package com.core136.service.party;

import com.core136.bean.party.PartyCleanReport;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.party.PartyCleanReportMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyCleanReportService {

    private PartyCleanReportMapper partyCleanReportMapper;

    @Autowired
    public void setPartyCleanReportMapper(PartyCleanReportMapper partyCleanReportMapper) {
        this.partyCleanReportMapper = partyCleanReportMapper;
    }

    public int insertPartyCleanReport(PartyCleanReport partyCleanReport) {
        return partyCleanReportMapper.insert(partyCleanReport);
    }

    public int deletePartyCleanReport(PartyCleanReport partyCleanReport) {
        return partyCleanReportMapper.delete(partyCleanReport);
    }

    public int updatePartyCleanReport(Example example, PartyCleanReport partyCleanReport) {
        return partyCleanReportMapper.updateByExampleSelective(partyCleanReport, example);
    }

    public PartyCleanReport selectOnePartyCleanReport(PartyCleanReport partyCleanReport) {
        return partyCleanReportMapper.selectOne(partyCleanReport);
    }

    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @param reportType
     * @param status
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getCleanReportList
     * @Description:  获取报告列表
     */
    public List<Map<String, String>> getCleanReportList(String orgId, String opFlag, String accountId, String reportType, String status, String beginTime, String endTime, String search) {
        return partyCleanReportMapper.getCleanReportList(orgId, opFlag, accountId, reportType, status, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param reportType
     * @param status
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getCleanReportList
     * @Description:  获取报告列表
     */
    public PageInfo<Map<String, String>> getCleanReportList(PageParam pageParam, String reportType, String status, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getCleanReportList(pageParam.getOrgId(), pageParam.getOpFlag(), pageParam.getAccountId(), reportType, status, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getMyCleanReportListForPortal
     * @Description:  获取门户领导报告列表
     */
    public List<Map<String, String>> getMyCleanReportListForPortal(String orgId) {
        return partyCleanReportMapper.getMyCleanReportListForPortal(orgId);
    }

    /**
     * @param orgId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getMyCleanReportList
     * @Description:  获取更多领导报告列表
     */
    public List<Map<String, String>> getMyCleanReportList(String orgId, String search) {
        return partyCleanReportMapper.getMyCleanReportList(orgId, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getMyCleanReportList
     * @Description:  获取更多领导报告列表
     */
    public PageInfo<Map<String, String>> getMyCleanReportList(PageParam pageParam) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyCleanReportList(pageParam.getOrgId(), pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }
}
