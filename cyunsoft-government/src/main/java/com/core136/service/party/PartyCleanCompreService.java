package com.core136.service.party;

import com.core136.bean.party.PartyCleanCompre;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.party.PartyCleanCompreMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyCleanCompreService {

    private PartyCleanCompreMapper partyCleanCompreMapper;

    @Autowired
    public void setPartyCleanCompreMapper(PartyCleanCompreMapper partyCleanCompreMapper) {
        this.partyCleanCompreMapper = partyCleanCompreMapper;
    }

    public int insertPartyCleanCompre(PartyCleanCompre partyCleanCompre) {
        return partyCleanCompreMapper.insert(partyCleanCompre);
    }

    public int deletePartyCleanCompre(PartyCleanCompre partyCleanCompre) {
        return partyCleanCompreMapper.delete(partyCleanCompre);
    }

    public int updatePartyCleanCompre(Example example, PartyCleanCompre partyCleanCompre) {
        return partyCleanCompreMapper.updateByExampleSelective(partyCleanCompre, example);
    }

    public PartyCleanCompre selectOnePartyCleanCompre(PartyCleanCompre partyCleanCompre) {
        return partyCleanCompreMapper.selectOne(partyCleanCompre);
    }

    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @param comprehensioType
     * @param status
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getCleanComprehensioList
     * @Description:  获取感悟列表
     */
    public List<Map<String, String>> getCleanComprehensionList(String orgId, String opFlag, String accountId, String comprehensionType, String status, String beginTime, String endTime, String search) {
        return partyCleanCompreMapper.getCleanComprehensionList(orgId, opFlag, accountId, comprehensionType, status, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param comprehensioType
     * @param status
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getCleanComprehensioList
     * @Description:  获取感悟列表
     */
    public PageInfo<Map<String, String>> getCleanComprehensionList(PageParam pageParam, String comprehensionType, String status, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getCleanComprehensionList(pageParam.getOrgId(), pageParam.getOpFlag(), pageParam.getAccountId(), comprehensionType, status, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }


    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getMyCleanComprehensionListForPortal
     * @Description:  获取门户学习感悟列表
     */
    public List<Map<String, String>> getMyCleanComprehensionListForPortal(String orgId) {
        return partyCleanCompreMapper.getMyCleanComprehensionListForPortal(orgId);
    }

    /**
     * @param orgId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getMyCleanComprehensionList
     * @Description:  获取更多学习感悟记录
     */
    public List<Map<String, String>> getMyCleanComprehensionList(String orgId, String search) {
        return partyCleanCompreMapper.getMyCleanComprehensionList(orgId, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getMyCleanComprehensionList
     * @Description:  获取更多学习感悟记录
     */
    public PageInfo<Map<String, String>> getMyCleanComprehensionList(PageParam pageParam) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyCleanComprehensionList(pageParam.getOrgId(), pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }
}
