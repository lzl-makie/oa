package com.core136.service.party;

import com.core136.bean.account.Account;
import com.core136.bean.party.PartyIntegral;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.party.PartyIntegralMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyIntegralService {

    private PartyIntegralMapper partyIntegralMapper;

    @Autowired
    public void setPartyIntegralMapper(PartyIntegralMapper partyIntegralMapper) {
        this.partyIntegralMapper = partyIntegralMapper;
    }

    public int insertPartyIntegral(PartyIntegral partyIntegral) {
        return partyIntegralMapper.insert(partyIntegral);
    }

    public int deletePartyIntegral(PartyIntegral partyIntegral) {
        return partyIntegralMapper.delete(partyIntegral);
    }

    public int updatePartyIntegral(Example example, PartyIntegral partyIntegral) {
        return partyIntegralMapper.updateByExampleSelective(partyIntegral, example);
    }

    public PartyIntegral selectOnePartyIntegral(PartyIntegral partyIntegral) {
        return partyIntegralMapper.selectOne(partyIntegral);
    }

    /**
     * @param account
     * @param memberIds
     * @param year
     * @return RetDataBean
     * @Title: addMemberIntegrals
     * @Description:  加入党员考核计划
     */
    @Transactional(value = "generalTM")
    public RetDataBean addMemberIntegrals(Account account, String memberIds, String year) {
        if (StringUtils.isBlank(year)) {
            return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
        }
        if (StringUtils.isBlank(memberIds)) {
            return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
        }
        String[] memberIdArr = memberIds.split(",");
        for (int i = 0; i < memberIdArr.length; i++) {
            PartyIntegral partyIntegral = new PartyIntegral();
            partyIntegral.setOrgId(account.getOrgId());
            partyIntegral.setMemberId(memberIdArr[i]);
            partyIntegral.setYear(year);
            partyIntegral = selectOnePartyIntegral(partyIntegral);
            if (partyIntegral == null) {
                PartyIntegral partyIntegralTemp = new PartyIntegral();
                partyIntegralTemp.setRecordId(SysTools.getGUID());
                partyIntegralTemp.setOrgId(account.getOrgId());
                partyIntegralTemp.setMemberId(memberIdArr[i]);
                partyIntegralTemp.setYear(year);
                partyIntegralTemp.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
                partyIntegralTemp.setCreateUser(account.getAccountId());
                insertPartyIntegral(partyIntegralTemp);
            }
        }
        return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS);
    }


    /**
     * @param orgId
     * @param partyOrgId
     * @param year
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPartyVerifyAllList
     * @Description:  获取所有考核记录
     */
    public List<Map<String, String>> getPartyVerifyAllList(String orgId, String partyOrgId, String year, String search) {
        return partyIntegralMapper.getPartyVerifyAllList(orgId, partyOrgId, year, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param partyOrgId
     * @param year
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getPartyVerifyAllList
     * @Description:  获取所有考核记录
     */
    public PageInfo<Map<String, String>> getPartyVerifyAllList(PageParam pageParam, String partyOrgId, String year) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getPartyVerifyAllList(pageParam.getOrgId(), partyOrgId, year, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }
}
