package com.core136.service.party;

import com.core136.bean.party.PartyRulesRecord;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.party.PartyRulesRecordMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyRulesRecordService {
    private PartyRulesRecordMapper partyRulesRecordMapper;

    @Autowired
    public void setPartyRulesRecordMapper(PartyRulesRecordMapper partyRulesRecordMapper) {
        this.partyRulesRecordMapper = partyRulesRecordMapper;
    }

    public int insertPartyRulesRecord(PartyRulesRecord partyRulesRecord) {
        return partyRulesRecordMapper.insert(partyRulesRecord);
    }

    public int deletePartyRulesRecord(PartyRulesRecord partyRulesRecord) {
        return partyRulesRecordMapper.delete(partyRulesRecord);
    }

    public int updatePartyRulesRecord(Example example, PartyRulesRecord partyRulesRecord) {
        return partyRulesRecordMapper.updateByExampleSelective(partyRulesRecord, example);
    }

    public PartyRulesRecord selectOnePartyRulesRecord(PartyRulesRecord partyRulesRecord) {
        return partyRulesRecordMapper.selectOne(partyRulesRecord);
    }

    /**
     * @param orgId
     * @param sortId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPartyRulesRecordList
     * @Description:  获取规章制度列表
     */
    public List<Map<String, String>> getPartyRulesRecordList(String orgId, String sortId, String beginTime, String endTime, String search) {
        return partyRulesRecordMapper.getPartyRulesRecordList(orgId, sortId, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param sortId
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getPartyRulesRecordList
     * @Description:  获取规章制度列表
     */
    public PageInfo<Map<String, String>> getPartyRulesRecordList(PageParam pageParam, String sortId, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getPartyRulesRecordList(pageParam.getOrgId(), sortId, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }


}
