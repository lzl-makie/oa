package com.core136.service.party;

import com.core136.bean.party.PartyOrgPoints;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.party.PartyOrgPointsMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyOrgPointsService {
    private PartyOrgPointsMapper partyOrgPointsMapper;

    @Autowired
    public void setPartyOrgPointsMapper(PartyOrgPointsMapper partyOrgPointsMapper) {
        this.partyOrgPointsMapper = partyOrgPointsMapper;
    }

    public int insertPartyOrgPoints(PartyOrgPoints partyOrgPoints) {
        return partyOrgPointsMapper.insert(partyOrgPoints);
    }

    public int deletePartyOrgPoints(PartyOrgPoints partyOrgPoints) {
        return partyOrgPointsMapper.delete(partyOrgPoints);
    }

    public int updatePartyOrgPoints(Example example, PartyOrgPoints partyOrgPoints) {
        return partyOrgPointsMapper.updateByExampleSelective(partyOrgPoints, example);
    }

    public PartyOrgPoints selectOnePartyOrgPoints(PartyOrgPoints partyOrgPoints) {
        return partyOrgPointsMapper.selectOne(partyOrgPoints);
    }

    /**
     * 获取党组织积分列表
     *
     * @param orgId
     * @param pointsYear
     * @param partyOrgId
     * @return
     */
    public List<Map<String, String>> getOrgPointsList(String orgId, String pointsYear, String partyOrgId) {
        return partyOrgPointsMapper.getOrgPointsList(orgId, pointsYear, partyOrgId);
    }

    /**
     * 获取党组织积分列表
     *
     * @param pageParam
     * @param pointsYear
     * @param partyOrgId
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getOrgPointsList(PageParam pageParam, String pointsYear, String partyOrgId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getOrgPointsList(pageParam.getOrgId(), pointsYear, partyOrgId);
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }


    /**
     * 获取党组织汇总积分列表
     *
     * @param orgId
     * @param pointsYear
     * @param partyOrgId
     * @return
     */
    public List<Map<String, String>> getOrgPointsTotalList(String orgId, String pointsYear, String partyOrgId) {
        return partyOrgPointsMapper.getOrgPointsTotalList(orgId, pointsYear, partyOrgId);
    }

    /**
     * 获取党组织汇总列表
     *
     * @param pageParam
     * @param pointsYear
     * @param partyOrgId
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getOrgPointsTotalList(PageParam pageParam, String pointsYear, String partyOrgId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getOrgPointsTotalList(pageParam.getOrgId(), pointsYear, partyOrgId);
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
