package com.core136.service.party;

import com.core136.bean.account.Account;
import com.core136.bean.party.PartyAlbumSubscribe;
import com.core136.common.utils.SysTools;
import com.core136.mapper.party.PartyAlbumSubscribeMapper;
import com.core136.service.account.UserInfoService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.*;

@Service
public class PartyAlbumSubscribeService {
    private PartyAlbumSubscribeMapper partyAlbumSubscribeMapper;

    @Autowired
    public void setPartyAlbumSubscribeMapper(PartyAlbumSubscribeMapper partyAlbumSubscribeMapper) {
        this.partyAlbumSubscribeMapper = partyAlbumSubscribeMapper;
    }

    private UserInfoService userInfoService;

    @Autowired
    public void setUserInfoService(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }

    public int insertPartyAlbumSubscribe(PartyAlbumSubscribe partyAlbumSubscribe) {
        return partyAlbumSubscribeMapper.insert(partyAlbumSubscribe);
    }

    public int deletePartyAlbumSubscribe(PartyAlbumSubscribe partyAlbumSubscribe) {
        return partyAlbumSubscribeMapper.delete(partyAlbumSubscribe);
    }

    public int updatePartyAlbumSubscribe(Example example, PartyAlbumSubscribe partyAlbumSubscribe) {
        return partyAlbumSubscribeMapper.updateByExampleSelective(partyAlbumSubscribe, example);
    }

    public PartyAlbumSubscribe selectOnePartyAlbumSubscribe(PartyAlbumSubscribe partyAlbumSubscribe) {
        return partyAlbumSubscribeMapper.selectOne(partyAlbumSubscribe);
    }

    /**
     * @param orgId
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getSubscribeMyUserList
     * @Description:  获取关注我的人员列表
     */
    public List<Map<String, String>> getSubscribeMyUserList(String orgId, String accountId) {
        return partyAlbumSubscribeMapper.getSubscribeMyUserList(orgId, accountId);
    }

    /**
     * @param account
     * @return List<Map < String, String>>
     * @Title: getSubscribeUserListAll
     * @Description:  获取所有关注人员
     */
    public List<Map<String, String>> getSubscribeUserListAll(Account account) {
        PartyAlbumSubscribe partyAlbumSubscribeTemp = new PartyAlbumSubscribe();
        partyAlbumSubscribeTemp.setCreateUser(account.getAccountId());
        partyAlbumSubscribeTemp.setOrgId(account.getOrgId());
        partyAlbumSubscribeTemp = selectOnePartyAlbumSubscribe(partyAlbumSubscribeTemp);
        String subscribeUser = partyAlbumSubscribeTemp.getSubscribeUser();
        List<String> list = new ArrayList<String>();
        if (StringUtils.isNotBlank(subscribeUser)) {
            String[] arr = subscribeUser.split(",");
            list = new ArrayList<String>(Arrays.asList(arr));
            HashSet<String> set = new HashSet<String>(list.size());
            List<String> result = new ArrayList<String>(list.size());
            for (String str : list) {
                if (set.add(str)) {
                    result.add(str);
                }
            }
            list.clear();
            list.addAll(result);
            return userInfoService.getAllUserInfoByAccountList(account.getOrgId(), list);
        } else {
            return null;
        }
    }


    /**
     * @param account
     * @return List<Map < String, String>>
     * @Title: getSubscribeUserList
     * @Description:  获取订阅人员列表
     */
    public List<Map<String, String>> getSubscribeUserList(Account account) {
        PartyAlbumSubscribe partyAlbumSubscribeTemp = new PartyAlbumSubscribe();
        partyAlbumSubscribeTemp.setCreateUser(account.getAccountId());
        partyAlbumSubscribeTemp.setOrgId(account.getOrgId());
        partyAlbumSubscribeTemp = selectOnePartyAlbumSubscribe(partyAlbumSubscribeTemp);
        String subscribeUser = "";
        if (partyAlbumSubscribeTemp != null) {
            subscribeUser = partyAlbumSubscribeTemp.getSubscribeUser();
        }
        List<String> list = new ArrayList<String>();
        if (StringUtils.isNotBlank(subscribeUser)) {
            String[] arr = subscribeUser.split(",");
            list = new ArrayList<String>(Arrays.asList(arr));
            HashSet<String> set = new HashSet<String>(list.size());
            List<String> result = new ArrayList<String>(list.size());
            for (String str : list) {
                if (set.add(str)) {
                    result.add(str);
                }
            }
            list.clear();
            list.addAll(result);
            List<String> tempList = new ArrayList<String>();
            if (list.size() > 10) {
                for (int i = 0; i < 10; i++) {
                    tempList.add(list.get(i));
                }
            } else {
                tempList = list;
            }

            return userInfoService.getAllUserInfoByAccountList(account.getOrgId(), tempList);
        } else {
            return null;
        }
    }


    /**
     * @param partyAlbumSubscribe void
     * @Title: setSubscribeUser
     * @Description:  关注
     */
    public void setSubscribeUser(PartyAlbumSubscribe partyAlbumSubscribe) {
        Example example = new Example(PartyAlbumSubscribe.class);
        example.createCriteria().andEqualTo("orgId", partyAlbumSubscribe.getOrgId()).andEqualTo("createUser", partyAlbumSubscribe.getCreateUser());
        PartyAlbumSubscribe partyAlbumSubscribeTemp = partyAlbumSubscribeMapper.selectOneByExample(example);
        if (partyAlbumSubscribeTemp == null) {
            partyAlbumSubscribe.setSubscribeId(SysTools.getGUID());
            insertPartyAlbumSubscribe(partyAlbumSubscribe);
        } else {
            String subscribeUser = partyAlbumSubscribeTemp.getSubscribeUser();
            List<String> list = new ArrayList<String>();
            if (StringUtils.isNotBlank(subscribeUser)) {
                String[] arr = subscribeUser.split(",");
                list = new ArrayList<String>(Arrays.asList(arr));
            }
            list.add(partyAlbumSubscribe.getSubscribeUser());
            HashSet<String> set = new HashSet<String>(list.size());
            List<String> result = new ArrayList<String>(list.size());
            for (String str : list) {
                if (set.add(str)) {
                    result.add(str);
                }
            }
            list.clear();
            list.addAll(result);
            partyAlbumSubscribe.setSubscribeUser(StringUtils.join(list, ","));
            Example example1 = new Example(PartyAlbumSubscribe.class);
            example1.createCriteria().andEqualTo("orgId", partyAlbumSubscribe.getOrgId()).andEqualTo("createUser", partyAlbumSubscribe.getCreateUser());
            updatePartyAlbumSubscribe(example1, partyAlbumSubscribe);
        }
    }

    /**
     * @param account
     * @param accountId void
     * @Title: setCancelSubscribeUser
     * @Description:  取消关注
     */
    public void setCancelSubscribeUser(Account account, String accountId) {
        PartyAlbumSubscribe partyAlbumSubscribeTemp = new PartyAlbumSubscribe();
        partyAlbumSubscribeTemp.setCreateUser(account.getAccountId());
        partyAlbumSubscribeTemp.setOrgId(account.getOrgId());
        partyAlbumSubscribeTemp = selectOnePartyAlbumSubscribe(partyAlbumSubscribeTemp);
        String subscribeUser = partyAlbumSubscribeTemp.getSubscribeUser();
        List<String> list = new ArrayList<String>();
        if (StringUtils.isNotBlank(subscribeUser)) {
            String[] arr = subscribeUser.split(",");
            list = new ArrayList<String>(Arrays.asList(arr));
        }
        HashSet<String> set = new HashSet<String>(list.size());
        List<String> result = new ArrayList<String>(list.size());
        for (String str : list) {
            if (set.add(str)) {
                result.add(str);
            }
        }
        list.clear();
        list.addAll(result);
        String tempStr = "," + StringUtils.join(list, ",") + ",";
        tempStr = tempStr.replace("," + accountId + ",", ",");
        tempStr = tempStr.substring(1, tempStr.length() - 1);
        partyAlbumSubscribeTemp.setSubscribeUser(tempStr);
        Example example1 = new Example(PartyAlbumSubscribe.class);
        example1.createCriteria().andEqualTo("orgId", partyAlbumSubscribeTemp.getOrgId()).andEqualTo("createUser", partyAlbumSubscribeTemp.getCreateUser());
        updatePartyAlbumSubscribe(example1, partyAlbumSubscribeTemp);
    }

}
