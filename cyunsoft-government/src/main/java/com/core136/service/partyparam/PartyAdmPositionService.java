package com.core136.service.partyparam;

import com.core136.bean.partyparam.PartyAdmPosition;
import com.core136.mapper.partyparam.PartyAdmPositionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyAdmPositionService {
    private PartyAdmPositionMapper partyAdmPositionMapper;

    @Autowired
    public void setPartyAdmPositionMapper(PartyAdmPositionMapper partyAdmPositionMapper) {
        this.partyAdmPositionMapper = partyAdmPositionMapper;
    }

    public int insertPartyAdmPosition(PartyAdmPosition partyAdmPosition) {
        return partyAdmPositionMapper.insert(partyAdmPosition);
    }

    public int deletePartyAdmPosition(PartyAdmPosition partyAdmPosition) {
        return partyAdmPositionMapper.delete(partyAdmPosition);
    }

    public int updatePartyAdmPosition(Example example, PartyAdmPosition partyAdmPosition) {
        return partyAdmPositionMapper.updateByExampleSelective(partyAdmPosition, example);
    }

    public PartyAdmPosition selectOnePartyAdmPosition(PartyAdmPosition partyAdmPosition) {
        return partyAdmPositionMapper.selectOne(partyAdmPosition);
    }

    /**
     * @param orgId
     * @param levelId
     * @return List<Map < String, String>>
     * @Title: getAdmPositionTree
     * @Description:  获取行政职务分类树
     */
    public List<Map<String, String>> getAdmPositionTree(String orgId, String levelId) {
        return partyAdmPositionMapper.getAdmPositionTree(orgId, levelId);
    }

    /**
     * 判断是否还有子集
     */

    public int isExistChild(String orgId, String sortId) {
        return partyAdmPositionMapper.isExistChild(orgId, sortId);
    }

}
