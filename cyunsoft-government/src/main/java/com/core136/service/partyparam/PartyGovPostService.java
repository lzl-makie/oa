package com.core136.service.partyparam;

import com.core136.bean.partyparam.PartyGovPost;
import com.core136.mapper.partyparam.PartyGovPostMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyGovPostService {
    private PartyGovPostMapper partyGovPostMapper;

    @Autowired
    public void setPartyGovPostMapper(PartyGovPostMapper partyGovPostMapper) {
        this.partyGovPostMapper = partyGovPostMapper;
    }

    public int insertPartyGovPost(PartyGovPost partyGovPost) {
        return partyGovPostMapper.insert(partyGovPost);
    }

    public int deletePartyGovPost(PartyGovPost partyGovPost) {
        return partyGovPostMapper.delete(partyGovPost);
    }

    public int updatePartyGovPost(Example example, PartyGovPost partyGovPost) {
        return partyGovPostMapper.updateByExampleSelective(partyGovPost, example);
    }

    public PartyGovPost selectOnePartyGovPost(PartyGovPost partyGovPost) {
        return partyGovPostMapper.selectOne(partyGovPost);
    }

    /**
     * @param orgId
     * @param levelId
     * @return List<Map < String, String>>
     * @Title: getGovPostTree
     * @Description:  获取党内职务分类树
     */
    public List<Map<String, String>> getGovPostTree(String orgId, String levelId) {
        return partyGovPostMapper.getGovPostTree(orgId, levelId);
    }

    /**
     * 判断是否还有子集
     */

    public int isExistChild(String orgId, String sortId) {
        return partyGovPostMapper.isExistChild(orgId, sortId);
    }
}
