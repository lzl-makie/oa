package com.core136.service.partyparam;

import com.core136.bean.partyparam.PartyNativePlace;
import com.core136.mapper.partyparam.PartyNativePlaceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyNativePlaceService {
    private PartyNativePlaceMapper partyNativePlaceMapper;

    @Autowired
    public void setPartyNativePlaceMapper(PartyNativePlaceMapper partyNativePlaceMapper) {
        this.partyNativePlaceMapper = partyNativePlaceMapper;
    }

    public int insertPartyNativePlace(PartyNativePlace partyNativePlace) {
        return partyNativePlaceMapper.insert(partyNativePlace);
    }

    public int deletePartyNativePlace(PartyNativePlace partyNativePlace) {
        return partyNativePlaceMapper.delete(partyNativePlace);
    }

    public int updatePartyNativePlace(Example example, PartyNativePlace partyNativePlace) {
        return partyNativePlaceMapper.updateByExampleSelective(partyNativePlace, example);
    }

    public PartyNativePlace selectOnePartyNativePlace(PartyNativePlace partyNativePlace) {
        return partyNativePlaceMapper.selectOne(partyNativePlace);
    }

    /**
     * @param orgId
     * @param levelId
     * @return List<Map < String, String>>
     * @Title: getNativePlaceTree
     * @Description:  获取籍贯分类树
     */
    public List<Map<String, String>> getNativePlaceTree(String orgId, String levelId) {
        return partyNativePlaceMapper.getNativePlaceTree(orgId, levelId);
    }

    /**
     * 判断是否还有子集
     */

    public int isExistChild(String orgId, String sortId) {
        return partyNativePlaceMapper.isExistChild(orgId, sortId);
    }
}
