package com.core136.service.partyparam;

import com.core136.bean.partyparam.PartyUnitService;
import com.core136.mapper.partyparam.PartyUnitServiceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyUnitServiceService {
    private PartyUnitServiceMapper partyUnitServiceMapper;

    @Autowired
    public void setPartyUnitServiceMapper(PartyUnitServiceMapper partyUnitServiceMapper) {
        this.partyUnitServiceMapper = partyUnitServiceMapper;
    }

    public int insertPartyUnitService(PartyUnitService partyUnitService) {
        return partyUnitServiceMapper.insert(partyUnitService);
    }

    public int deletePartyUnitService(PartyUnitService partyUnitService) {
        return partyUnitServiceMapper.delete(partyUnitService);
    }

    public int updatePartyUnitService(Example example, PartyUnitService partyUnitService) {
        return partyUnitServiceMapper.updateByExampleSelective(partyUnitService, example);
    }

    public PartyUnitService selectOnePartyUnitService(PartyUnitService partyUnitService) {
        return partyUnitServiceMapper.selectOne(partyUnitService);
    }

    /**
     * @param orgId
     * @param levelId
     * @return List<Map < String, String>>
     * @Title: getUnitServiceTree
     * @Description:  获取单位服务行业类型树
     */
    public List<Map<String, String>> getUnitServiceTree(String orgId, String levelId) {
        return partyUnitServiceMapper.getUnitServiceTree(orgId, levelId);
    }

    /**
     * 判断是否还有子集
     */

    public int isExistChild(String orgId, String sortId) {
        //  Auto-generated method stub
        return partyUnitServiceMapper.isExistChild(orgId, sortId);
    }

}
