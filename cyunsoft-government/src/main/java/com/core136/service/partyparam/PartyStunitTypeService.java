package com.core136.service.partyparam;

import com.core136.bean.partyparam.PartyStunitType;
import com.core136.mapper.partyparam.PartyStunitTypeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyStunitTypeService {
    private PartyStunitTypeMapper partyStunitTypeMapper;

    @Autowired
    public void setPartyStunitTypeMapper(PartyStunitTypeMapper partyStunitTypeMapper) {
        this.partyStunitTypeMapper = partyStunitTypeMapper;
    }

    public int insertPartyStunitType(PartyStunitType partyStunitType) {
        return partyStunitTypeMapper.insert(partyStunitType);
    }

    public int deletePartyStunitType(PartyStunitType partyStunitType) {
        return partyStunitTypeMapper.delete(partyStunitType);
    }

    public int updatePartyStunitType(Example example, PartyStunitType partyStunitType) {
        return partyStunitTypeMapper.updateByExampleSelective(partyStunitType, example);
    }

    public PartyStunitType selectOnePartyStunitType(PartyStunitType partyStunitType) {
        return partyStunitTypeMapper.selectOne(partyStunitType);
    }

    /**
     * @param orgId
     * @param levelId
     * @return List<Map < String, String>>
     * @Title: getStunitTypeTree
     * @Description:  获取从学单位类别树
     */
    public List<Map<String, String>> getStunitTypeTree(String orgId, String levelId) {
        return partyStunitTypeMapper.getStunitTypeTree(orgId, levelId);
    }

    /**
     * 判断是否还有子集
     */

    public int isExistChild(String orgId, String sortId) {
        return partyStunitTypeMapper.isExistChild(orgId, sortId);
    }

}
