package com.core136.service.partyparam;

import com.core136.bean.partyparam.PartyStratum;
import com.core136.mapper.partyparam.PartyStratumMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyStratumService {
    private PartyStratumMapper partyStratumMapper;

    @Autowired
    public void setPartyStratumMapper(PartyStratumMapper partyStratumMapper) {
        this.partyStratumMapper = partyStratumMapper;
    }

    public int insertPartyStratum(PartyStratum partyStratum) {
        return partyStratumMapper.insert(partyStratum);
    }

    public int deletePartyStratum(PartyStratum partyStratum) {
        return partyStratumMapper.delete(partyStratum);
    }

    public int updatePartyStratum(Example example, PartyStratum partyStratum) {
        return partyStratumMapper.updateByExampleSelective(partyStratum, example);
    }

    public PartyStratum selectOnePartyStratum(PartyStratum partyStratum) {
        return partyStratumMapper.selectOne(partyStratum);
    }

    /**
     * @param orgId
     * @param levelId
     * @return List<Map < String, String>>
     * @Title: getStratumTree
     * @Description:  获取社会阶层分类树
     */
    public List<Map<String, String>> getStratumTree(String orgId, String levelId) {
        return partyStratumMapper.getStratumTree(orgId, levelId);
    }

    /**
     * 判断是否还有子集
     */

    public int isExistChild(String orgId, String sortId) {
        return partyStratumMapper.isExistChild(orgId, sortId);
    }
}
