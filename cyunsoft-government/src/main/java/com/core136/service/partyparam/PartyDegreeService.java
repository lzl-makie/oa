package com.core136.service.partyparam;

import com.core136.bean.partyparam.PartyDegree;
import com.core136.mapper.partyparam.PartyDegreeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyDegreeService {
    private PartyDegreeMapper partyDegreeMapper;

    @Autowired
    public void setPartyDegreeMapper(PartyDegreeMapper partyDegreeMapper) {
        this.partyDegreeMapper = partyDegreeMapper;
    }

    public int insertPartyDegree(PartyDegree partyDegree) {
        return partyDegreeMapper.insert(partyDegree);
    }

    public int deletePartyDegree(PartyDegree partyDegree) {
        return partyDegreeMapper.delete(partyDegree);
    }

    public int updatePartyDegree(Example example, PartyDegree partyDegree) {
        return partyDegreeMapper.updateByExampleSelective(partyDegree, example);
    }

    public PartyDegree selectOnePartyDegree(PartyDegree partyDegree) {
        return partyDegreeMapper.selectOne(partyDegree);
    }

    /**
     * @param orgId
     * @param levelId
     * @return List<Map < String, String>>
     * @Title: getDegreeTree
     * @Description:  获取学位分类树
     */
    public List<Map<String, String>> getDegreeTree(String orgId, String levelId) {
        return partyDegreeMapper.getDegreeTree(orgId, levelId);
    }

    /**
     * 判断是否还有子集
     */

    public int isExistChild(String orgId, String sortId) {
        return partyDegreeMapper.isExistChild(orgId, sortId);
    }

}
