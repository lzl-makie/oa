package com.core136.service.partyunion;

import com.core136.bean.account.Account;
import com.core136.bean.partyunion.PartyUnionDept;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.ExcelUtil;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partyunion.PartyUnionDeptMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PartyUnionDeptService {
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private PartyUnionDeptMapper partyUnionDeptMapper;

    @Autowired
    public void setPartyUnionDeptMapper(PartyUnionDeptMapper partyUnionDeptMapper) {
        this.partyUnionDeptMapper = partyUnionDeptMapper;
    }

    public int insertPartyUnionDept(PartyUnionDept partyUnionDept) {
        return partyUnionDeptMapper.insert(partyUnionDept);
    }

    public int deletePartyUnionDept(PartyUnionDept partyUnionDept) {
        return partyUnionDeptMapper.delete(partyUnionDept);
    }

    public int updatePartyUnionDept(Example example, PartyUnionDept partyUnionDept) {
        return partyUnionDeptMapper.updateByExampleSelective(partyUnionDept, example);
    }

    public PartyUnionDept selectOnePartyUnionDept(PartyUnionDept partyUnionDept) {
        return partyUnionDeptMapper.selectOne(partyUnionDept);
    }

    /**
     * 获取工会部门
     *
     * @param orgId
     * @param orgLevelId
     * @return
     */
    public List<Map<String, String>> getPartyUnionDeptTree(String orgId, String orgLevelId) {
        return partyUnionDeptMapper.getPartyUnionDeptTree(orgId, orgLevelId);
    }

    /**
     * 导入工会部门
     *
     * @param account
     * @param file
     * @return
     * @throws IOException
     */
    @Transactional(value = "generalTM")
    public RetDataBean importPartyUnionDept(Account account, MultipartFile file) throws IOException {
        List<String> list = new ArrayList<String>();
        Map<String, String> fieldMap = new HashMap<String, String>();
        fieldMap.put("排序号", "sort_no");
        fieldMap.put("部门名称", "dept_name");
        fieldMap.put("上级部门", "org_level_id");
        fieldMap.put("电话", "dept_tel");
        fieldMap.put("电子邮件", "dept_email");
        fieldMap.put("传真", "dept_fax");
        fieldMap.put("部门领导", "dept_lead");
        fieldMap.put("部门职责", "dept_function");
        List<String> fieldList = new ArrayList<String>();
        List<String> titleList = new ArrayList<String>();
        for (Map.Entry<String, String> entry : fieldMap.entrySet()) {
            fieldList.add(entry.getValue());
            titleList.add(entry.getKey());
        }
        String[] fieldArr = new String[fieldList.size()];
        fieldList.toArray(fieldArr);
        String fieldString = StringUtils.join(fieldArr, ",");
        List<Map<String, String>> recordList = ExcelUtil.readExcel(file);
        int level = getDeptLevel(recordList);
        for (int s = 0; s <= level; s++) {
            int length = 0;
            List<Map<String, String>> tempRecordList = new ArrayList<Map<String, String>>();
            for (int i = 0; i < recordList.size(); i++) {
                Map<String, String> tempMap = recordList.get(i);
                String orgLevelId = tempMap.get("上级部门");
                if (StringUtils.isNotBlank(orgLevelId)) {
                    String[] tempArr = orgLevelId.split("/");
                    length = tempArr.length;
                } else {
                    length = 0;
                }
                if (length == s) {
                    tempRecordList.add(tempMap);
                }
            }
            for (int i = 0; i < tempRecordList.size(); i++) {
                String deptName = "";
                Map<String, String> tempMap = tempRecordList.get(i);
                String valueString = "'" + SysTools.getGUID() + "',";
                for (int k = 0; k < titleList.size(); k++) {
                    if (titleList.get(k).equals("上级部门")) {
                        String orgLevelStr = tempMap.get(titleList.get(k));
                        if (StringUtils.isNotBlank(orgLevelStr)) {
                            String[] orgLevel = orgLevelStr.split("/");
                            deptName = orgLevel[orgLevel.length - 1];
                            String sql = "select dept_id from party_union_dept where dept_name='" + deptName + "' and org_id='" + account.getOrgId() + "'";
                            String deptId = jdbcTemplate.queryForObject(sql, String.class);
                            valueString += "'" + deptId + "',";
                        } else {
                            valueString += "'0',";
                        }
                    } else {
                        valueString += "'" + tempMap.get(titleList.get(k)) + "',";
                    }
                }
                valueString += "'" + account.getOrgId() + "'";
                String insertSql = "insert into party_union_dept(dept_id," + fieldString + ",org_id) values" + "(" + valueString + ")";
                jdbcTemplate.execute(insertSql);
            }
        }
        return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
    }


    public int getDeptLevel(List<Map<String, String>> recordList) {
        int returnInt = 0;
        Map<String, String> tempMap = new HashMap<String, String>();
        for (int i = 0; i < recordList.size(); i++) {
            tempMap = recordList.get(i);
            String tempStr = tempMap.get("上级部门");
            if (StringUtils.isNotBlank(tempStr)) {
                String[] tempArr = tempStr.split("/");
                if (returnInt < tempArr.length) {
                    returnInt = tempArr.length;
                }
            }
        }
        return returnInt;
    }

}
