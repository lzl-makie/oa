package com.core136.service.partyunion;

import com.core136.bean.partyunion.PartyUnionParty;
import com.core136.mapper.partyunion.PartyUnionPartyMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

@Service
public class PartyUnionPartyService {
    private PartyUnionPartyMapper partyUnionPartyMapper;

    @Autowired
    public void setPartyUnionPartyMapper(PartyUnionPartyMapper partyUnionPartyMapper) {
        this.partyUnionPartyMapper = partyUnionPartyMapper;
    }

    public int insertPartyUnionParty(PartyUnionParty partyUnionParty) {
        return partyUnionPartyMapper.insert(partyUnionParty);
    }

    public int deletePartyUnionParty(PartyUnionParty partyUnionParty) {
        return partyUnionPartyMapper.delete(partyUnionParty);
    }

    public int updatePartyUnionParty(Example example, PartyUnionParty partyUnionParty) {
        return partyUnionPartyMapper.updateByExampleSelective(partyUnionParty, example);
    }

    public PartyUnionParty selectOnePartyUnionParty(PartyUnionParty partyUnionParty) {
        return partyUnionPartyMapper.selectOne(partyUnionParty);
    }


}
