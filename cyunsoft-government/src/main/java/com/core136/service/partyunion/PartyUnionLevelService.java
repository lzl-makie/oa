package com.core136.service.partyunion;

import com.core136.bean.partyunion.PartyUnionLevel;
import com.core136.mapper.partyunion.PartyUnionLevelMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyUnionLevelService {

    private PartyUnionLevelMapper partyUnionLevelMapper;

    @Autowired
    public void setpartyUnionLevelMapper(PartyUnionLevelMapper partyUnionLevelMapper) {
        this.partyUnionLevelMapper = partyUnionLevelMapper;
    }


    public int insertPartyUnionLevel(PartyUnionLevel partyUnionLevel) {
        return partyUnionLevelMapper.insert(partyUnionLevel);
    }

    public int deletePartyUnionLevel(PartyUnionLevel partyUnionLevel) {
        return partyUnionLevelMapper.delete(partyUnionLevel);
    }

    public int updatePartyUnionLevel(Example example, PartyUnionLevel partyUnionLevel) {
        return partyUnionLevelMapper.updateByExampleSelective(partyUnionLevel, example);
    }

    public int deletePartyUnionLevelByIds(Example example) {
        return partyUnionLevelMapper.deleteByExample(example);
    }


    public PartyUnionLevel selectOnePartyUnionLevel(PartyUnionLevel partyUnionLevel) {
        return partyUnionLevelMapper.selectOne(partyUnionLevel);
    }


    public PageInfo<PartyUnionLevel> getPartyUnionLevelList(Example example, int pageNumber, int pageSize) {
        PageHelper.startPage(pageNumber, pageSize);
        List<PartyUnionLevel> datalist = getPartyUnionLevelList(example);
        PageInfo<PartyUnionLevel> pageInfo = new PageInfo<PartyUnionLevel>(datalist);
        return pageInfo;
    }

    public List<PartyUnionLevel> getPartyUnionLevelList(Example example) {
        return partyUnionLevelMapper.selectByExample(example);
    }

    public List<Map<String, String>> getPartyUnionLevelForList(String orgId) {
        return partyUnionLevelMapper.getPartyUnionLevelForList(orgId);
    }
}
