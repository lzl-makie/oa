package com.core136.service.partyunion;

import com.core136.bean.partyunion.PartyUnionWorker;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partyunion.PartyUnionWorkerMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyUnionWorkerService {
    private PartyUnionWorkerMapper partyUnionWorkerMapper;

    @Autowired
    public void setPartyUnionWorkerMapper(PartyUnionWorkerMapper partyUnionWorkerMapper) {
        this.partyUnionWorkerMapper = partyUnionWorkerMapper;
    }

    public int insertPartyUnionWorker(PartyUnionWorker partyUnionWorker) {
        return partyUnionWorkerMapper.insert(partyUnionWorker);
    }

    public int deletePartyUnionWorker(PartyUnionWorker partyUnionWorker) {
        return partyUnionWorkerMapper.delete(partyUnionWorker);
    }

    public int updatePartyUnionWorker(Example example, PartyUnionWorker partyUnionWorker) {
        return partyUnionWorkerMapper.updateByExampleSelective(partyUnionWorker, example);
    }

    public PartyUnionWorker selectOnePartyUnionWorker(PartyUnionWorker partyUnionWorker) {
        return partyUnionWorkerMapper.selectOne(partyUnionWorker);
    }

    /**
     * 获取农民工列表
     *
     * @param orgId
     * @param beginTime
     * @param endTime
     * @param beginTimeBirthday
     * @param endTimeBirthday
     * @param search
     * @return
     */
    public List<Map<String, String>> getPartyUnionWorkersList(String orgId, String beginTime, String endTime, String beginTimeBirthday, String endTimeBirthday, String search) {
        return partyUnionWorkerMapper.getPartyUnionWorkersList(orgId, beginTime, endTime, beginTimeBirthday, endTimeBirthday, "%" + search + "%");
    }

    /**
     * 获取农民工列表
     *
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @param beginTimeBirthday
     * @param endTimeBirthday
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getPartyUnionWorkersList(PageParam pageParam, String beginTime, String endTime, String beginTimeBirthday, String endTimeBirthday) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getPartyUnionWorkersList(pageParam.getOrgId(), beginTime, endTime, beginTimeBirthday, endTimeBirthday, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }


}
