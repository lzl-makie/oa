package com.core136.service.partyunion;

import com.core136.bean.partyunion.PartyUnionInst;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partyunion.PartyUnionInstMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyUnionInstService {
    private PartyUnionInstMapper partyUnionInstMapper;

    @Autowired
    public void setPartyUnionInstMapper(PartyUnionInstMapper partyUnionInstMapper) {
        this.partyUnionInstMapper = partyUnionInstMapper;
    }

    public int insertPartyUnionInst(PartyUnionInst partyUnionInst) {
        return partyUnionInstMapper.insert(partyUnionInst);
    }

    public int deletePartyUnionInst(PartyUnionInst partyUnionInst) {
        return partyUnionInstMapper.delete(partyUnionInst);
    }

    public int updatePartyUnionInst(Example example, PartyUnionInst partyUnionInst) {
        return partyUnionInstMapper.updateByExampleSelective(partyUnionInst, example);
    }

    public PartyUnionInst selectOnePartyUnionInst(PartyUnionInst partyUnionInst) {
        return partyUnionInstMapper.selectOne(partyUnionInst);
    }

    /**
     * 获取法律法规列表
     *
     * @param orgId
     * @param sortId
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getPartyUnionInstList(String orgId, String sortId, String beginTime, String endTime, String search) {
        return partyUnionInstMapper.getPartyUnionInstList(orgId, sortId, beginTime, endTime, "%" + search + "%");
    }

    /**
     * 获取法律法规列表
     *
     * @param pageParam
     * @param sortId
     * @param beginTime
     * @param endTime
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getPartyUnionInstList(PageParam pageParam, String sortId, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getPartyUnionInstList(pageParam.getOrgId(), sortId, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
