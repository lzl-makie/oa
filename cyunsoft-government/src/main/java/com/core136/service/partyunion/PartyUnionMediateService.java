package com.core136.service.partyunion;

import com.core136.bean.partyunion.PartyUnionMediate;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partyunion.PartyUnionMediateMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyUnionMediateService {
    private PartyUnionMediateMapper partyUnionMediateMapper;

    @Autowired
    public void setPartyUnionMediateMapper(PartyUnionMediateMapper partyUnionMediateMapper) {
        this.partyUnionMediateMapper = partyUnionMediateMapper;
    }

    public int insertPartyUnionMediate(PartyUnionMediate partyUnionMediatex) {
        return partyUnionMediateMapper.insert(partyUnionMediatex);
    }

    public int deletePartyUnionMediate(PartyUnionMediate partyUnionMediate) {
        return partyUnionMediateMapper.delete(partyUnionMediate);
    }

    public int updatePartyUnionMediate(Example example, PartyUnionMediate partyUnionMediate) {
        return partyUnionMediateMapper.updateByExampleSelective(partyUnionMediate, example);
    }

    public PartyUnionMediate selectOnePartyUnionMediate(PartyUnionMediate partyUnionMediate) {
        return partyUnionMediateMapper.selectOne(partyUnionMediate);
    }

    /**
     * 获取调解事件列表
     *
     * @param orgId
     * @param mediateType
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getPartyUnionMediateList(String orgId, String mediateType, String beginTime, String endTime, String search) {
        return partyUnionMediateMapper.getPartyUnionMediateList(orgId, mediateType, beginTime, endTime, "%" + search + "%");
    }

    /**
     * 获取调解事件列表
     *
     * @param pageParam
     * @param mediateType
     * @param beginTime
     * @param endTime
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getPartyUnionMediateList(PageParam pageParam, String mediateType, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getPartyUnionMediateList(pageParam.getOrgId(), mediateType, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }


}
