package com.core136.service.partyunion;

import com.core136.bean.partyunion.PartyUnionEssence;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partyunion.PartyUnionEssenceMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyUnionEssenceService {

    private PartyUnionEssenceMapper partyUnionEssenceMapper;

    @Autowired
    public void setPartyUnionEssenceMapper(PartyUnionEssenceMapper partyUnionEssenceMapper) {
        this.partyUnionEssenceMapper = partyUnionEssenceMapper;
    }

    public int insertPartyUnionEssence(PartyUnionEssence partyUnionEssence) {
        return partyUnionEssenceMapper.insert(partyUnionEssence);
    }

    public int deletePartyUnionEssence(PartyUnionEssence partyUnionEssence) {
        return partyUnionEssenceMapper.delete(partyUnionEssence);
    }

    public int updatePartyUnionEssence(Example example, PartyUnionEssence partyUnionEssence) {
        return partyUnionEssenceMapper.updateByExampleSelective(partyUnionEssence, example);
    }

    public PartyUnionEssence selectOnePartyUnionEssence(PartyUnionEssence partyUnionEssence) {
        return partyUnionEssenceMapper.selectOne(partyUnionEssence);
    }

    /**
     * 获取职场精英列表
     *
     * @param orgId
     * @param essenceType
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getPartyUnionEssenceList(String orgId, String essenceType, String beginTime, String endTime, String search) {
        return partyUnionEssenceMapper.getPartyUnionEssenceList(orgId, essenceType, beginTime, endTime, "%" + search + "%");
    }

    /**
     * 获取职场精英列表
     *
     * @param pageParam
     * @param essenceType
     * @param beginTime
     * @param endTime
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getPartyUnionEssenceList(PageParam pageParam, String essenceType, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getPartyUnionEssenceList(pageParam.getOrgId(), essenceType, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }


}
