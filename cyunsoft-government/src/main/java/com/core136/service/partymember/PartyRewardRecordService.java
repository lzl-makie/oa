package com.core136.service.partymember;

import com.core136.bean.partymember.PartyRewardRecord;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partymember.PartyRewardRecordMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyRewardRecordService {

    private PartyRewardRecordMapper partyRewardRecordMapper;

    @Autowired
    public void setPartyRewardRecordMapper(PartyRewardRecordMapper partyRewardRecordMapper) {
        this.partyRewardRecordMapper = partyRewardRecordMapper;
    }

    public int insertPartyRewardRecord(PartyRewardRecord partyRewardRecord) {
        return partyRewardRecordMapper.insert(partyRewardRecord);
    }

    public int deletePartyRewardRecord(PartyRewardRecord partyRewardRecord) {
        return partyRewardRecordMapper.delete(partyRewardRecord);
    }

    public int updatePartyRewardRecord(Example example, PartyRewardRecord partyRewardRecord) {
        return partyRewardRecordMapper.updateByExampleSelective(partyRewardRecord, example);
    }

    public PartyRewardRecord selectOnePartyRewardRecord(PartyRewardRecord partyRewardRecord) {
        return partyRewardRecordMapper.selectOne(partyRewardRecord);
    }

    /**
     * @param orgId
     * @param beginTime
     * @param rewardLevel
     * @param memberId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getMemberRewardList
     * @Description:  获取党员奖励列表
     */
    public List<Map<String, String>> getMemberRewardList(String orgId, String beginTime, String endTime, String rewardLevel, String memberId, String search) {
        return partyRewardRecordMapper.getMemberRewardList(orgId, beginTime, endTime, rewardLevel, memberId, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @param rewardLevel
     * @param memberId
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getMemberRewardList
     * @Description:  获取党员奖励列表
     */
    public PageInfo<Map<String, String>> getMemberRewardList(PageParam pageParam, String beginTime, String endTime, String rewardLevel, String memberId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMemberRewardList(pageParam.getOrgId(), beginTime, endTime, rewardLevel, memberId, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param memberId
     * @return List<Map < String, String>>
     * @Title: getMyMemberRewardList
     * @Description:  获取个人奖励列表
     */
    public List<Map<String, String>> getMyMemberRewardList(String orgId, String memberId) {
        return partyRewardRecordMapper.getMyMemberRewardList(orgId, memberId);
    }


}
