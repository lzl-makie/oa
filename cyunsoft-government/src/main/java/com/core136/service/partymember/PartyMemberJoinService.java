package com.core136.service.partymember;

import com.core136.bean.partymember.PartyMemberJoin;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partymember.PartyMemberJoinMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyMemberJoinService {

    private PartyMemberJoinMapper partyMemberJoinMapper;

    @Autowired
    public void setPartyMemberJoinMapper(PartyMemberJoinMapper partyMemberJoinMapper) {
        this.partyMemberJoinMapper = partyMemberJoinMapper;
    }

    public int insertPartyMemberJoin(PartyMemberJoin partyMemberJoin) {
        return partyMemberJoinMapper.insert(partyMemberJoin);
    }

    public int deletePartyMemberJoin(PartyMemberJoin partyMemberJoin) {
        return partyMemberJoinMapper.delete(partyMemberJoin);
    }

    public int updatePartyMemberJoin(Example example, PartyMemberJoin partyMemberJoin) {
        return partyMemberJoinMapper.updateByExampleSelective(partyMemberJoin, example);
    }

    public PartyMemberJoin selectOnePartyMemberJoin(PartyMemberJoin partyMemberJoin) {
        return partyMemberJoinMapper.selectOne(partyMemberJoin);
    }

    /**
     * @param orgId
     * @param partyOrgId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getMemberJoinApplyList
     * @Description:  获取入党申请列表
     */
    public List<Map<String, String>> getMemberJoinApplyList(String orgId, String statusFlag, String partyOrgId, String beginTime, String endTime, String search) {
        return partyMemberJoinMapper.getMemberJoinApplyList(orgId, statusFlag, partyOrgId, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param partyOrgId
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getMemberJoinApplyList
     * @Description:  获取入党申请列表
     */
    public PageInfo<Map<String, String>> getMemberJoinApplyList(PageParam pageParam, String statusFlag, String partyOrgId, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMemberJoinApplyList(pageParam.getOrgId(), statusFlag, partyOrgId, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getActivistsMemberIdList
     * @Description:  获取积极份子人员列表
     */
    public List<Map<String, String>> getActivistsMemberIdList(String orgId) {
        return partyMemberJoinMapper.getActivistsMemberIdList(orgId);
    }

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getTargetMemberIdList
     * @Description:  获取发展对象人员列表
     */
    public List<Map<String, String>> getTargetMemberIdList(String orgId) {
        return partyMemberJoinMapper.getTargetMemberIdList(orgId);
    }

}
