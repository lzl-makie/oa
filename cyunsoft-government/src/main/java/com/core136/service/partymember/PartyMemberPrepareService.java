package com.core136.service.partymember;

import com.core136.bean.partymember.PartyMember;
import com.core136.bean.partymember.PartyMemberJoin;
import com.core136.bean.partymember.PartyMemberPrepare;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partymember.PartyMemberPrepareMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyMemberPrepareService {
    private PartyMemberPrepareMapper partyMemberPrepareMapper;

    @Autowired
    public void setPartyMemberPrepareMapper(PartyMemberPrepareMapper partyMemberPrepareMapper) {
        this.partyMemberPrepareMapper = partyMemberPrepareMapper;
    }

    private PartyMemberJoinService partyMemberJoinService;

    @Autowired
    public void setPartyMemberJoinService(PartyMemberJoinService partyMemberJoinService) {
        this.partyMemberJoinService = partyMemberJoinService;
    }

    private PartyMemberService partyMemberService;

    @Autowired
    public void setPartyMemberService(PartyMemberService partyMemberService) {
        this.partyMemberService = partyMemberService;
    }

    public int insertPartyMemberPrepare(PartyMemberPrepare partyMemberPrepare) {
        return partyMemberPrepareMapper.insert(partyMemberPrepare);
    }

    public int deletePartyMemberPrepare(PartyMemberPrepare partyMemberPrepare) {
        return partyMemberPrepareMapper.delete(partyMemberPrepare);
    }

    public int updatePartyMemberPrepare(Example example, PartyMemberPrepare partyMemberPrepare) {
        return partyMemberPrepareMapper.updateByExample(partyMemberPrepare, example);
    }

    public PartyMemberPrepare selectOnePartyMemberPrepare(PartyMemberPrepare partyMemberPrepare) {
        return partyMemberPrepareMapper.selectOne(partyMemberPrepare);
    }

    /**
     * @param partyMemberPrepare
     * @return RetDataBean
     * @Title: setPartyMemberPrepare
     * @Description:  设置入党申请人为预备党员
     */
    @Transactional(value = "generalTM")
    public RetDataBean setPartyMemberPrepare(PartyMemberPrepare partyMemberPrepare) {
        if (StringUtils.isBlank(partyMemberPrepare.getJoinRecordId())) {
            return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
        } else {
            if (partyMemberPrepare.getStatus().equals("1")) {
                String memberId = SysTools.getGUID();
                PartyMemberJoin partyMemberJoin = new PartyMemberJoin();
                partyMemberJoin.setJoinTime2(partyMemberPrepare.getJoinTime());
                partyMemberJoin.setStatusFlag(3);
                partyMemberJoin.setMemberId(memberId);
                Example example = new Example(PartyMemberJoin.class);
                example.createCriteria().andEqualTo("orgId", partyMemberPrepare.getOrgId()).andEqualTo("recordId", partyMemberPrepare.getJoinRecordId());
                partyMemberJoinService.updatePartyMemberJoin(example, partyMemberJoin);
                PartyMemberJoin partyMemberJoin1 = new PartyMemberJoin();
                partyMemberJoin1.setRecordId(partyMemberPrepare.getJoinRecordId());
                partyMemberJoin1.setOrgId(partyMemberPrepare.getOrgId());
                partyMemberJoin1 = partyMemberJoinService.selectOnePartyMemberJoin(partyMemberJoin1);
                PartyMember partyMember = new PartyMember();
                partyMember.setMemberId(memberId);
                partyMember.setPartyOrgId(partyMemberJoin1.getPartyOrgId());
                partyMember.setUserName(partyMemberJoin1.getUserName());
                partyMember.setUserSex(partyMemberJoin1.getUserSex());
                partyMember.setJoinTime(partyMemberPrepare.getJoinTime());
                partyMember.setPartyStatus("2");
                partyMember.setCardId(partyMemberJoin1.getCardId());
                partyMember.setTel(partyMemberJoin1.getTel());
                partyMember.setCreateTime(partyMemberPrepare.getCreateTime());
                partyMember.setCreateUser(partyMemberPrepare.getCreateUser());
                partyMember.setOrgId(partyMemberJoin1.getOrgId());
                partyMemberService.insertPartyMember(partyMember);
                return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, insertPartyMemberPrepare(partyMemberPrepare));
            } else {
                PartyMemberJoin partyMemberJoin = new PartyMemberJoin();
                partyMemberJoin.setJoinTime2(partyMemberPrepare.getJoinTime());
                partyMemberJoin.setStatusFlag(100);
                Example example = new Example(PartyMemberJoin.class);
                example.createCriteria().andEqualTo("orgId", partyMemberPrepare.getOrgId()).andEqualTo("recordId", partyMemberPrepare.getJoinRecordId());
                partyMemberJoinService.updatePartyMemberJoin(example, partyMemberJoin);
                return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, insertPartyMemberPrepare(partyMemberPrepare));
            }
        }
    }

    /**
     * @param orgId
     * @param status
     * @param partyOrgId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPrepareRecordList
     * @Description:  获取预备党员审批记录
     */
    public List<Map<String, String>> getPrepareRecordList(String orgId, String status, String partyOrgId, String beginTime, String endTime, String search) {
        return partyMemberPrepareMapper.getPrepareRecordList(orgId, status, partyOrgId, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param status
     * @param partyOrgId
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getPrepareRecordList
     * @Description:  获取预备党员审批记录
     */
    public PageInfo<Map<String, String>> getPrepareRecordList(PageParam pageParam, String status, String partyOrgId, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getPrepareRecordList(pageParam.getOrgId(), status, partyOrgId, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
