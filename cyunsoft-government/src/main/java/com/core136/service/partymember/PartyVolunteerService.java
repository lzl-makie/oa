package com.core136.service.partymember;

import com.core136.bean.partymember.PartyVolunteer;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partymember.PartyVolunteerMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyVolunteerService {
    private PartyVolunteerMapper partyVolunteerMapper;

    @Autowired
    public void setPartyVolunteerMapper(PartyVolunteerMapper partyVolunteerMapper) {
        this.partyVolunteerMapper = partyVolunteerMapper;
    }

    public int insertPartyVolunteer(PartyVolunteer partyVolunteer) {
        return partyVolunteerMapper.insert(partyVolunteer);
    }

    public int deletePartyVolunteer(PartyVolunteer partyVolunteer) {
        return partyVolunteerMapper.delete(partyVolunteer);
    }

    public int updatePartyVolunteer(Example example, PartyVolunteer partyVolunteer) {
        return partyVolunteerMapper.updateByExampleSelective(partyVolunteer, example);
    }

    public PartyVolunteer selectOnePartyVolunteer(PartyVolunteer partyVolunteer) {
        return partyVolunteerMapper.selectOne(partyVolunteer);
    }

    /**
     * @param orgId
     * @param volType
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getVolunteerList
     * @Description:  获取志愿者活动列表
     */
    public List<Map<String, String>> getVolunteerList(String orgId, String volType, String beginTime, String endTime, String search) {
        return partyVolunteerMapper.getVolunteerList(orgId, volType, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param volType
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getGovVolunteerList
     * @Description:  获取志愿者活动列表
     */
    public PageInfo<Map<String, String>> getVolunteerList(PageParam pageParam, String volType, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getVolunteerList(pageParam.getOrgId(), volType, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }


}
