package com.core136.service.partycommission;

import com.core136.bean.partycommission.PartyEfficiency;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partycommission.PartyEfficiencyMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyEfficiencyService {
    private PartyEfficiencyMapper partyEfficiencyMapper;

    @Autowired
    public void setPartyEfficiencyMapper(PartyEfficiencyMapper partyEfficiencyMapper) {
        this.partyEfficiencyMapper = partyEfficiencyMapper;
    }

    public int insertPartyEfficiency(PartyEfficiency partyEfficiency) {
        return partyEfficiencyMapper.insert(partyEfficiency);
    }

    public int deletePartyEfficiency(PartyEfficiency partyEfficiency) {
        return partyEfficiencyMapper.delete(partyEfficiency);
    }

    public int updatePartyEfficiency(Example example, PartyEfficiency partyEfficiency) {
        return partyEfficiencyMapper.updateByExampleSelective(partyEfficiency, example);
    }

    public PartyEfficiency selectOnePartyEfficiency(PartyEfficiency partyEfficiency) {
        return partyEfficiencyMapper.selectOne(partyEfficiency);
    }

    /**
     * @param orgId
     * @param eventType
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPartyEfficiencyList
     * @Description:  效能监察工作记录列表
     */
    public List<Map<String, String>> getPartyEfficiencyList(String orgId, String eventType, String beginTime, String endTime, String search) {
        return partyEfficiencyMapper.getPartyEfficiencyList(orgId, eventType, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param eventType
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getPartyBuildCleanList
     * @Description:  效能监察工作记录列表
     */
    public PageInfo<Map<String, String>> getPartyEfficiencyList(PageParam pageParam, String eventType, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getPartyEfficiencyList(pageParam.getOrgId(), eventType, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }


}
