package com.core136.service.partyorg;

import com.core136.bean.partyorg.PartyCheckMeet;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partyorg.PartyCheckMeetMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyCheckMeetService {
    private PartyCheckMeetMapper partyCheckMeetMapper;

    @Autowired
    public void setPartyCheckMeetMapper(PartyCheckMeetMapper partyCheckMeetMapper) {
        this.partyCheckMeetMapper = partyCheckMeetMapper;
    }

    public int insertPartyCheckMeet(PartyCheckMeet partyCheckMeet) {
        return partyCheckMeetMapper.insert(partyCheckMeet);
    }

    public int deletePartyCheckMeet(PartyCheckMeet partyCheckMeet) {
        return partyCheckMeetMapper.delete(partyCheckMeet);
    }

    public int updatePartyCheckMeet(Example example, PartyCheckMeet partyCheckMeet) {
        return partyCheckMeetMapper.updateByExampleSelective(partyCheckMeet, example);
    }

    public PartyCheckMeet selectOnePartyCheckMeet(PartyCheckMeet partyCheckMeet) {
        return partyCheckMeetMapper.selectOne(partyCheckMeet);
    }

    /**
     * @param orgId
     * @param checkYear
     * @param checkStatus
     * @param checkLevel
     * @param partyOrgId
     * @return List<Map < String, String>>
     * @Title: getCheckMeetList
     * @Description:  获取述评考记录列表
     */
    public List<Map<String, String>> getCheckMeetList(String orgId, String checkYear, String checkStatus, String checkLevel, String partyOrgId) {
        return partyCheckMeetMapper.getCheckMeetList(orgId, checkYear, checkStatus, checkLevel, partyOrgId);
    }

    /**
     * @param pageParam
     * @param checkYear
     * @param checkStatus
     * @param checkLevel
     * @param partyOrgId
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getCheckMeetList
     * @Description:  获取述评考记录列表
     */
    public PageInfo<Map<String, String>> getCheckMeetList(PageParam pageParam, String checkYear, String checkStatus, String checkLevel, String partyOrgId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getCheckMeetList(pageParam.getOrgId(), checkYear, checkStatus, checkLevel, partyOrgId);
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
