package com.core136.service.partyorg;

import com.core136.bean.partyorg.PartyTalk;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partyorg.PartyTalkMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyTalkService {
    private PartyTalkMapper partyTalkMapper;

    @Autowired
    public void setPartyTalkMapper(PartyTalkMapper partyTalkMapper) {
        this.partyTalkMapper = partyTalkMapper;
    }

    public int insertPartyTalk(PartyTalk partyTalk) {
        return partyTalkMapper.insert(partyTalk);
    }

    public int deletePartyTalk(PartyTalk partyTalk) {
        return partyTalkMapper.delete(partyTalk);
    }

    public int updatePartyTalk(Example example, PartyTalk partyTalk) {
        return partyTalkMapper.updateByExampleSelective(partyTalk, example);
    }

    public PartyTalk selectOnePartyTalk(PartyTalk partyTalk) {
        return partyTalkMapper.selectOne(partyTalk);
    }

    /**
     * @param orgId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPartyTalkList
     * @Description:  获取书记谈话记录
     */
    public List<Map<String, String>> getPartyTalkList(String orgId, String beginTime, String endTime, String search) {
        return partyTalkMapper.getPartyTalkList(orgId, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getPartyTalkList
     * @Description:  获取书记谈话记录
     */
    public PageInfo<Map<String, String>> getPartyTalkList(PageParam pageParam, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getPartyTalkList(pageParam.getOrgId(), beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
