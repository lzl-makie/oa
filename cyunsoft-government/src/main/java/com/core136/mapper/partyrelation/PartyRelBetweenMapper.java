package com.core136.mapper.partyrelation;

import com.core136.bean.partyrelation.PartyRelBetween;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyRelBetweenMapper extends MyMapper<PartyRelBetween> {

    /**
     * @param orgId
     * @param relType
     * @param beginTime
     * @param endTime
     * @param oldPartyOrgId
     * @param relPartyOrgId
     * @return List<Map < String, String>>
     * @Title: getRelBetweenList
     * @Description:  获取支部间调转记录列表
     */
    public List<Map<String, String>> getRelBetweenList(@Param(value = "orgId") String orgId, @Param(value = "relType") String relType,
                                                       @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime,
                                                       @Param(value = "oldPartyOrgId") String oldPartyOrgId, @Param(value = "relPartyOrgId") String relPartyOrgId);


}
