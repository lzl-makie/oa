package com.core136.mapper.partyparam;

import com.core136.bean.partyparam.PartyUnitService;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyUnitServiceMapper extends MyMapper<PartyUnitService> {
    /**
     * @param orgId
     * @param levelId
     * @return List<Map < String, String>>
     * @Title: getUnitServiceTree
     * @Description:  获取单位服务行业类型树
     */
    public List<Map<String, String>> getUnitServiceTree(@Param(value = "orgId") String orgId, @Param(value = "levelId") String levelId);

    /**
     * @param @param  orgId
     * @param @param  sortId
     * @param @return 设定文件
     * @return int 返回类型
     * @Title: isExistChild
     * @Description:  判断是否还有子集
     */
    public int isExistChild(@Param(value = "orgId") String orgId, @Param(value = "sortId") String sortId);
}
