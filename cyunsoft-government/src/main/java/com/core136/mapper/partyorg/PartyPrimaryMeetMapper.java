package com.core136.mapper.partyorg;

import com.core136.bean.partyorg.PartyPrimaryMeet;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyPrimaryMeetMapper extends MyMapper<PartyPrimaryMeet> {
    /**
     * @param orgId
     * @param beginTime
     * @param endTime
     * @param meetType
     * @param partyOrgId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPrimaryMeetList
     * @Description:  获取主题党日活动列表
     */
    public List<Map<String, String>> getPrimaryMeetList(@Param(value = "orgId") String orgId, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime,
                                                        @Param(value = "meetType") String meetType, @Param(value = "partyOrgId") String partyOrgId, @Param(value = "search") String search);
}
