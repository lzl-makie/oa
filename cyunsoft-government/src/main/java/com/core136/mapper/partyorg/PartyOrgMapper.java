package com.core136.mapper.partyorg;

import com.core136.bean.partyorg.PartyOrg;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyOrgMapper extends MyMapper<PartyOrg> {

    /**
     * @param orgId
     * @param levelId
     * @return List<Map < String, String>>
     * @Title: getPartyOrgTree
     * @Description:  获取党组织树形结构
     */
    public List<Map<String, String>> getPartyOrgTree(@Param(value = "orgId") String orgId, @Param(value = "levelId") String levelId);

    /**
     * @param orgId
     * @param partyOrgId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPartyOrgListById
     * @Description:  获取党组织下组织单位列表
     */
    public List<Map<String, String>> getPartyOrgListById(@Param(value = "orgId") String orgId, @Param(value = "partyOrgId") String partyOrgId, @Param(value = "search") String search);

    public List<Map<String, String>> getPartyOrgAllParentTree(@Param(value = "orgId") String orgId, @Param(value = "levelId") String levelId);

}
