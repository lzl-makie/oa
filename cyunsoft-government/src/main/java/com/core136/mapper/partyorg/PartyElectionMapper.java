package com.core136.mapper.partyorg;

import com.core136.bean.partyorg.PartyElection;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Map;


@Mapper
public interface PartyElectionMapper extends MyMapper<PartyElection> {

    /**
     * @param orgId
     * @param nowTime
     * @return Map<String, String>
     * @Title: electionIsExist
     * @Description:  当前时间是否存在届次
     */
    public Map<String, String> electionIsExist(@Param(value = "orgId") String orgId, @Param(value = "partyOrgId") String partyOrgId, @Param(value = "nowTime") String nowTime);

}
