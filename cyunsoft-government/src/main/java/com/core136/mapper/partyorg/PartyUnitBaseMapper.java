package com.core136.mapper.partyorg;

import com.core136.bean.partyorg.PartyUnitBase;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyUnitBaseMapper extends MyMapper<PartyUnitBase> {

    /**
     * @param orgId
     * @param partyStatus
     * @param unitAffiliation
     * @param serviceIndustry
     * @param search
     * @return List<Map < String, String>>
     * @Title: getUnitBaseInfoList
     * @Description:  获取单位基本信息列表
     */
    public List<Map<String, String>> getUnitBaseInfoList(@Param(value = "orgId") String orgId, @Param(value = "partyStatus") String partyStatus, @Param(value = "unitAffiliation") String unitAffiliation,
                                                         @Param(value = "serviceIndustry") String serviceIndustry, @Param(value = "search") String search);

    /**
     * @param orgId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getSelect2UnitBaseList
     * @Description:  获取select2下拉列表
     */
    public List<Map<String, String>> getSelect2UnitBaseList(@Param(value = "orgId") String orgId, @Param(value = "search") String search);
}
