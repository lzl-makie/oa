package com.core136.mapper.partyorg;

import com.core136.bean.partyorg.PartyLifeMeet;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyLifeMeetMapper extends MyMapper<PartyLifeMeet> {
    /**
     * @param orgId
     * @param beginTime
     * @param endTime
     * @param meetType
     * @param partyOrgId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getLifeMeetList
     * @Description:  获取三会一课记录列表
     */
    public List<Map<String, String>> getLifeMeetList(@Param(value = "orgId") String orgId, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime,
                                                     @Param(value = "meetType") String meetType, @Param(value = "partyOrgId") String partyOrgId, @Param(value = "search") String search);
}
