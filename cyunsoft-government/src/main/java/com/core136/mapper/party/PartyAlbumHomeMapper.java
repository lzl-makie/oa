package com.core136.mapper.party;

import com.core136.bean.party.PartyAlbumHome;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface PartyAlbumHomeMapper extends MyMapper<PartyAlbumHome> {

}
