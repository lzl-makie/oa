package com.core136.mapper.party;

import com.core136.bean.party.PartyCleanExpose;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyCleanExposeMapper extends MyMapper<PartyCleanExpose> {

    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @param exposeType
     * @param status
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getCleanExposeList
     * @Description:  获取曝光台列表
     */
    public List<Map<String, String>> getCleanExposeList(@Param(value = "orgId") String orgId, @Param(value = "opFlag") String opFlag, @Param(value = "accountId") String accountId, @Param(value = "exposeType") String exposeType,
                                                        @Param(value = "status") String status, @Param("beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);

    /**
     * 获取门户曝光记录列表
     * @param orgId
     * @param memberId
     * @return
     */
    public List<Map<String, String>> getMyCleanExposeListForPortal(@Param(value = "orgId") String orgId, @Param(value = "memberId") String memberId);

    /**
     * 获取更多曝光记录
     * @param orgId
     * @param memberId
     * @param search
     * @return
     */
    public List<Map<String, String>> getMyCleanExposeList(@Param(value = "orgId") String orgId, @Param(value = "memberId") String memberId, @Param(value = "search") String search);


}
