package com.core136.mapper.party;

import com.core136.bean.party.PartyBusinessGuide;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface PartyBusinessGuideMapper extends MyMapper<PartyBusinessGuide> {

    /**
     * @param orgId
     * @param guide
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPartyBusinessGuideList
     * @Description:  获取业务指南列表
     */
    public List<Map<String, String>> getPartyBusinessGuideList(@Param(value = "orgId") String orgId, @Param(value = "guideType") String guideType,
                                                               @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);


}
