package com.core136.mapper.party;

import com.core136.bean.party.PartyAlbumVideo;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface PartyAlbumVideoMapper extends MyMapper<PartyAlbumVideo> {
    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getSendVideoUserList
     * @Description:  获取所有发布视频的人员列表前10
     */
    public List<Map<String, String>> getSendVideoUserList(@Param(value = "orgId") String orgId);

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getAlbumVideoForHomeTop
     * @Description:  获取热榜TOP10
     */
    public List<Map<String, String>> getAlbumVideoForHomeTop(@Param(value = "orgId") String orgId, @Param(value = "memberId") String memberId);

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getAlbumVideoByAlbumType
     * @Description:  获了按分类视频
     */
    public List<Map<String, String>> getAlbumVideoByAlbumType(@Param(value = "orgId") String orgId, @Param(value = "albumType") String albumType, @Param(value = "memberId") String memberId);


    /**
     * @param orgId
     * @param albumType
     * @return List<Map < String, String>>
     * @Title: getAlbumVideoForHome
     * @Description:  获取专辑首页视频列表
     */
    public List<Map<String, String>> getAlbumVideoForHome(@Param(value = "orgId") String orgId, @Param(value = "albumType") String albumType, @Param(value = "memberId") String memberId);

    /**
     * @param orgId
     * @param accountId
     * @param beginTime
     * @param endTime
     * @param albumType
     * @param createUser
     * @param search
     * @return List<Map < String, String>>
     * @Title: getAlbumVideoForApprovel
     * @Description:  获取专辑待审批列表
     */
    public List<Map<String, String>> getAlbumVideoForApprovel(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId, @Param(value = "beginTime") String beginTime,
                                                              @Param(value = "endTime") String endTime, @Param(value = "albumType") String albumType, @Param(value = "createUser") String createUser, @Param(value = "search") String search);

    /**
     * 获取专辑列表
     * @param orgId
     * @param accountId
     * @param beginTime
     * @param endTime
     * @param albumType
     * @param createUser
     * @param status
     * @param search
     * @return
     */
    public List<Map<String, String>> getAlbumVideoForManage(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,
                                                            @Param(value = "beginTime") String beginTime,@Param(value = "endTime") String endTime,
                                                            @Param(value = "albumType") String albumType, @Param(value = "createUser") String createUser,
                                                            @Param(value="status")String status,@Param(value = "search") String search);


    /**
     * @param orgId
     * @param accountId
     * @param status
     * @param beginTime
     * @param endTime
     * @param albumType
     * @param createUser
     * @param search
     * @return List<Map < String, String>>
     * @Title: getAlbumVideoForApprovelOld
     * @Description:  获取历史审批记录
     */
    public List<Map<String, String>> getAlbumVideoForApprovelOld(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId, @Param(value = "status") String status, @Param(value = "beginTime") String beginTime,
                                                                 @Param(value = "endTime") String endTime, @Param(value = "albumType") String albumType, @Param(value = "createUser") String createUser, @Param(value = "search") String search);


    /**
     * @param orgId
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getAlbumVideoByCreateUser
     * @Description:  获取当前用户最近10条视频
     */
    public List<Map<String, String>> getAlbumVideoByCreateUser(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId);

    /**
     * @param orgId
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getAlbumVideoByHistory
     * @Description:  获取当前用户历史记录
     */
    public List<Map<String, String>> getAlbumVideoByHistory(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId);


}
