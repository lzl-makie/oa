package com.core136.mapper.party;

import com.core136.bean.party.PartyRulesLearn;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyRulesLearnMapper extends MyMapper<PartyRulesLearn> {

    /**
     * @param orgId
     * @param accountId
     * @param rulesRecordId
     * @return List<Map < String, String>>
     * @Title: getMyLearnRecordList
     * @Description:  获取当前用户的历史学习记录
     */
    public List<Map<String, String>> getMyLearnRecordList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId, @Param(value = "rulesRecordId") String rulesRecordId);

    /**
     * @param orgId
     * @param accountId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getLearnRecordList
     * @Description:  查询学习记录
     */
    public List<Map<String, String>> getLearnRecordList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,
                                                        @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);

}
