package com.core136.mapper.party;

import com.core136.bean.party.PartyAlbumComments;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyAlbumCommentsMapper extends MyMapper<PartyAlbumComments> {

    /**
     * @param orgId
     * @param videoId
     * @return List<Map < String, String>>
     * @Title: getAlbumCommentsList
     * @Description:  获取专辑评论
     */
    public List<Map<String, String>> getAlbumCommentsList(@Param(value = "orgId") String orgId, @Param(value = "videoId") String videoId);
}
