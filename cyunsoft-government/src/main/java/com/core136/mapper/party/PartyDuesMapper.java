package com.core136.mapper.party;

import com.core136.bean.party.PartyDues;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface PartyDuesMapper extends MyMapper<PartyDues> {

    /**
     * @param orgId
     * @param onTime
     * @param duesType
     * @param year
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPartyDeusList
     * @Description:  获取党员缴费列表
     */
    public List<Map<String, String>> getPartyDeusList(@Param(value = "orgId") String orgId, @Param(value = "onTime") String onTime, @Param(value = "duesType") String duesType, @Param(value = "year") String year, @Param(value = "search") String search);

    /**
     * @param orgId
     * @param memberId
     * @param year
     * @return List<Map < String, String>>
     * @Title: getMyPartyDeusList
     * @Description:  获取个人党费缴纳记录
     */
    public List<Map<String, String>> getMyPartyDeusList(@Param(value = "orgId") String orgId, @Param(value = "memberId") String memberId, @Param(value = "year") String year);

}
