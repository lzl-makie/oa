package com.core136.mapper.partymember;

import com.core136.bean.partymember.PartyMemberOut;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyMemberOutMapper extends MyMapper<PartyMemberOut> {

    /**
     * @param orgId
     * @param outType
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getMemberOutList
     * @Description:  获取出党人员列表
     */
    public List<Map<String, String>> getMemberOutList(@Param(value = "orgId") String orgId, @Param(value = "outType") String outType, @Param(value = "beginTime") String beginTime,
                                                      @Param(value = "endTime") String endTime, @Param(value = "search") String search);


}
