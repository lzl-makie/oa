package com.core136.mapper.partymember;

import com.core136.bean.partymember.PartyPunishRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyPunishRecordMapper extends MyMapper<PartyPunishRecord> {
    /**
     * @param orgId
     * @param beginTime
     * @param endTime
     * @param punishLevel
     * @param memberId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getMemberPunishList
     * @Description:  获取党员惩戒记录列表
     */
    public List<Map<String, String>> getMemberPunishList(@Param(value = "orgId") String orgId,
                                                         @Param(value = "beginTime") String beginTime,
                                                         @Param(value = "endTime") String endTime,
                                                         @Param(value = "punishLevel") String punishLevel,
                                                         @Param(value = "memberId") String memberId,
                                                         @Param(value = "search") String search);

    /**
     * @param orgId
     * @param memberId
     * @return List<Map < String, String>>
     * @Title: getMyMemberPunishList
     * @Description:  获取个人惩戒记录列表
     */
    public List<Map<String, String>> getMyMemberPunishList(@Param(value = "orgId") String orgId, @Param(value = "memberId") String memberId);
}
