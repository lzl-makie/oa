package com.core136.mapper.partymember;

import com.core136.bean.partymember.PartyTargetTrain;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyTargetTrainMapper extends MyMapper<PartyTargetTrain> {
    /**
     * @param orgId
     * @param beginTime
     * @param endTime
     * @param partyOrgId
     * @param jobStatus
     * @param search
     * @return List<Map < String, String>>
     * @Title: getTargetTrainList
     * @Description:  获取发展对象培训记录
     */
    public List<Map<String, String>> getTargetTrainList(@Param(value = "orgId") String orgId, @Param(value = "beginTime") String beginTime,
                                                        @Param(value = "endTime") String endTime, @Param(value = "partyOrgId") String partyOrgId, @Param(value = "jobStatus") String jobStatus,
                                                        @Param(value = "search") String search);
}
