package com.core136.mapper.partymember;

import com.core136.bean.partymember.PartyMemberLose;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface PartyMemberLoseMapper extends MyMapper<PartyMemberLose> {

    public List<Map<String, String>> getMemberLoseList(@Param(value = "orgId") String orgId, @Param(value = "loseType") String loseType,
                                                       @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);

}
