package com.core136.mapper.partymember;

import com.core136.bean.partymember.PartyMemberDifficulty;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface PartyMemberDifficultyMapper extends MyMapper<PartyMemberDifficulty> {
    /**
     * @param orgId
     * @param difType
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getMemberDifficultyList
     * @Description:  获取困难党员列表
     */
    public List<Map<String, String>> getMemberDifficultyList(@Param(value = "orgId") String orgId, @Param(value = "difType") String difType,
                                                             @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);

}
