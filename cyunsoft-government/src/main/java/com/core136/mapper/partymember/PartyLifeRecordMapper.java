package com.core136.mapper.partymember;

import com.core136.bean.partymember.PartyLifeRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyLifeRecordMapper extends MyMapper<PartyLifeRecord> {

    /**
     * @param orgId
     * @param reason
     * @param lifeContent
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPartyLifeList
     * @Description:  获取组织生活列表
     */
    public List<Map<String, String>> getPartyLifeList(@Param(value = "orgId") String orgId, @Param(value = "reason") String reason, @Param(value = "lifeContent") String lifeContent,
                                                      @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);

    /**
     * @param orgId
     * @param memberId
     * @return List<Map < String, String>>
     * @Title: getMyPartyLifeList
     * @Description:  获取个人组织生活列表
     */
    public List<Map<String, String>> getMyPartyLifeList(@Param(value = "orgId") String orgId, @Param(value = "memberId") String memberId);


}
