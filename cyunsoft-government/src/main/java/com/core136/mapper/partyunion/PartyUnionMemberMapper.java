package com.core136.mapper.partyunion;

import com.core136.bean.partyunion.PartyUnionMember;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface PartyUnionMemberMapper extends MyMapper<PartyUnionMember> {
    /**
     * 按基层获取成员列表
     *
     * @param orgId
     * @param deptId
     * @param search
     * @return
     */
    public List<Map<String, String>> getPartyUnionMemberListByDeptId(@Param(value = "orgId") String orgId, @Param(value = "deptId") String deptId, @Param(value = "search") String search);
}
