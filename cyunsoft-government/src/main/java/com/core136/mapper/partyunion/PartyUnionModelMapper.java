package com.core136.mapper.partyunion;

import com.core136.bean.partyunion.PartyUnionModel;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface PartyUnionModelMapper extends MyMapper<PartyUnionModel> {

    /**
     * 获取劳模记录列表
     *
     * @param orgId
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getPartyUnionModelList(@Param(value = "orgId") String orgId, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);

}
