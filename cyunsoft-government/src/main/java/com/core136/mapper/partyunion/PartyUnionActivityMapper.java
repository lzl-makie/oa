package com.core136.mapper.partyunion;

import com.core136.bean.partyunion.PartyUnionActivity;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface PartyUnionActivityMapper extends MyMapper<PartyUnionActivity> {
    /**
     * 获取工会活动列表
     *
     * @param orgId
     * @param activityType
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getPartyUnionActivityList(@Param(value = "orgId") String orgId, @Param(value = "activityType") String activityType,
                                                               @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);
}
