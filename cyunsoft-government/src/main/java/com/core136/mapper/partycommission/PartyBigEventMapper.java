package com.core136.mapper.partycommission;

import com.core136.bean.partycommission.PartyBigEvent;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface PartyBigEventMapper extends MyMapper<PartyBigEvent> {
    /**
     * @param orgId
     * @param eventType
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPartyBigEventList
     * @Description:  获取重大事件记录列表
     */
    public List<Map<String, String>> getPartyBigEventList(@Param(value = "orgId") String orgId, @Param(value = "eventType") String eventType,
                                                          @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);
}
