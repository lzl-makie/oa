package com.core136.mapper.partycommission;

import com.core136.bean.partycommission.PartyCommission;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface PartyCommissionMapper extends MyMapper<PartyCommission> {

    /**
     * @param orgId
     * @param nowTime
     * @return PartyCommission
     * @Title: getPartyCommission
     * @Description:  获取当前届次信息
     */
    public PartyCommission getPartyCommission(@Param(value = "orgId") String orgId, @Param(value = "nowTime") String nowTime);


}
