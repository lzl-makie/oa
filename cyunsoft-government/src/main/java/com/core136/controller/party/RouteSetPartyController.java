package com.core136.controller.party;

import com.core136.bean.account.Account;
import com.core136.bean.party.*;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.party.*;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/set/partyset")
public class RouteSetPartyController {
    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private PartyRulesLearnService partyRulesLearnService;

    @Autowired
    public void setPartyRulesLearnService(PartyRulesLearnService partyRulesLearnService) {
        this.partyRulesLearnService = partyRulesLearnService;
    }

    private PartyRulesRecordService partyRulesRecordService;

    @Autowired
    public void setPartyRulesRecordService(PartyRulesRecordService partyRulesRecordService) {
        this.partyRulesRecordService = partyRulesRecordService;
    }

    private PartyRulesSortService partyRulesSortService;

    @Autowired
    public void setPartyRulesSortService(PartyRulesSortService partyRulesSortService) {
        this.partyRulesSortService = partyRulesSortService;
    }

    private PartyAlbumTypeService partyAlbumTypeService;

    @Autowired
    public void setPartyAlbumTypeService(PartyAlbumTypeService partyAlbumTypeService) {
        this.partyAlbumTypeService = partyAlbumTypeService;
    }

    private PartyAlbumVideoService partyAlbumVideoService;

    @Autowired
    public void setPartyAlbumVideoService(PartyAlbumVideoService partyAlbumVideoService) {
        this.partyAlbumVideoService = partyAlbumVideoService;
    }

    private PartyAlbumHomeService partyAlbumHomeService;

    @Autowired
    public void setPartyAlbumHomeService(PartyAlbumHomeService partyAlbumHomeService) {
        this.partyAlbumHomeService = partyAlbumHomeService;
    }

    private PartyAlbumConfigService partyAlbumConfigService;

    @Autowired
    public void setPartyAlbumConfigService(PartyAlbumConfigService partyAlbumConfigService) {
        this.partyAlbumConfigService = partyAlbumConfigService;
    }

    private PartyAlbumSubscribeService partyAlbumSubscribeService;

    @Autowired
    public void setPartyAlbumSubscribeService(PartyAlbumSubscribeService partyAlbumSubscribeService) {
        this.partyAlbumSubscribeService = partyAlbumSubscribeService;
    }

    private PartyAlbumCommentsService partyAlbumCommentsService;

    @Autowired
    public void setPartyAlbumCommentsService(PartyAlbumCommentsService partyAlbumCommentsService) {
        this.partyAlbumCommentsService = partyAlbumCommentsService;
    }

    private PartyAlbumHistoryService partyAlbumHistoryService;

    @Autowired
    public void setPartyAlbumHistoryService(PartyAlbumHistoryService partyAlbumHistoryService) {
        this.partyAlbumHistoryService = partyAlbumHistoryService;
    }

    private PartyDuesService partyDuesService;

    @Autowired
    public void setPartyDuesService(PartyDuesService partyDuesService) {
        this.partyDuesService = partyDuesService;
    }

    private PartyCleanExposeService partyCleanExposeService;

    @Autowired
    public void setPartyCleanExposeService(PartyCleanExposeService partyCleanExposeService) {
        this.partyCleanExposeService = partyCleanExposeService;
    }

    private PartyIntegralService partyIntegralService;

    @Autowired
    public void setPartyIntegralService(PartyIntegralService partyIntegralService) {
        this.partyIntegralService = partyIntegralService;
    }

    private PartyCleanMeritoriousService partyCleanMeritoriousService;

    @Autowired
    public void setPartyCleanMeritoriousService(PartyCleanMeritoriousService partyCleanMeritoriousService) {
        this.partyCleanMeritoriousService = partyCleanMeritoriousService;
    }

    private PartyCleanSuggestionsService partyCleanSuggestionsService;

    @Autowired
    public void setPartyCleanSuggestionsService(PartyCleanSuggestionsService partyCleanSuggestionsService) {
        this.partyCleanSuggestionsService = partyCleanSuggestionsService;
    }

    private PartyCleanTipoffsService partyCleanTipoffsService;

    @Autowired
    public void setPartyCleanTipoffsService(PartyCleanTipoffsService partyCleanTipoffsService) {
        this.partyCleanTipoffsService = partyCleanTipoffsService;
    }

    private PartyCleanReportService partyCleanReportService;

    @Autowired
    public void setPartyCleanReportService(PartyCleanReportService partyCleanReportService) {
        this.partyCleanReportService = partyCleanReportService;
    }

    private PartyCleanCompreService partyCleanCompreService;

    @Autowired
    public void setPartyCleanCompreService(PartyCleanCompreService partyCleanCompreService) {
        this.partyCleanCompreService = partyCleanCompreService;
    }

    private PartyPayStandardService partyPayStandardService;

    @Autowired
    public void setPartyPayStandardService(PartyPayStandardService partyPayStandardService) {
        this.partyPayStandardService = partyPayStandardService;
    }

    private PartyFundsService partyFundsService;

    @Autowired
    public void setPartyFundsService(PartyFundsService partyFundsService) {
        this.partyFundsService = partyFundsService;
    }

    private PartyBusinessGuideService partyBusinessGuideService;

    @Autowired
    public void setPartyBusinessGuideService(PartyBusinessGuideService partyBusinessGuideService) {
        this.partyBusinessGuideService = partyBusinessGuideService;
    }

    private PartyNewsService partyNewsService;

    @Autowired
    public void setPartyNewsService(PartyNewsService partyNewsService) {
        this.partyNewsService = partyNewsService;
    }

    private PartyOrgPointsService partyOrgPointsService;

    @Autowired
    public void setPartyOrgPointsService(PartyOrgPointsService partyOrgPointsService) {
        this.partyOrgPointsService = partyOrgPointsService;
    }

    /**
     * 添加组织积分
     *
     * @return
     */
    @RequestMapping(value = "/insertPartyOrgPoints", method = RequestMethod.POST)
    public RetDataBean insertPartyOrgPoints(PartyOrgPoints partyOrgPoints) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyOrgPoints.setRecordId(SysTools.getGUID());
            partyOrgPoints.setCreateUser(account.getAccountId());
            partyOrgPoints.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyOrgPoints.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyOrgPointsService.insertPartyOrgPoints(partyOrgPoints));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除组织积分
     *
     * @param partyOrgPoints
     * @return
     */
    @RequestMapping(value = "/deletePartyOrgPoints", method = RequestMethod.POST)
    public RetDataBean deletePartyOrgPoints(PartyOrgPoints partyOrgPoints) {
        try {
            if (StringUtils.isBlank(partyOrgPoints.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyOrgPoints.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyOrgPointsService.deletePartyOrgPoints(partyOrgPoints));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新组织积分
     *
     * @param partyOrgPoints
     * @return
     */
    @RequestMapping(value = "/updatePartyOrgPoints", method = RequestMethod.POST)
    public RetDataBean updatePartyOrgPoints(PartyOrgPoints partyOrgPoints) {
        try {
            if (StringUtils.isBlank(partyOrgPoints.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyOrgPoints.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyOrgPoints.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyOrgPointsService.updatePartyOrgPoints(example, partyOrgPoints));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyNews
     * @return RetDataBean
     * @Title: insertPartyNews
     * @Description:  创建党建新闻
     */
    @RequestMapping(value = "/insertPartyNews", method = RequestMethod.POST)
    public RetDataBean insertPartyNews(PartyNews partyNews) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyNews.setRecordId(SysTools.getGUID());
            if (StringUtils.isNotBlank(partyNews.getContent())) {
                Document htmlDoc = Jsoup.parse(partyNews.getContent());
                String subheading = htmlDoc.text();
                if (subheading.length() > 50) {
                    subheading = subheading.substring(0, 50) + "...";
                }
                partyNews.setSubheading(subheading);
            } else {
                partyNews.setSubheading("");
            }
            partyNews.setCreateUser(account.getAccountId());
            partyNews.setDelFlag("0");
            partyNews.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyNews.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyNewsService.insertPartyNews(partyNews));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyNews
     * @return RetDataBean
     * @Title: deletePartyNews
     * @Description:  删除党建新闻
     */
    @RequestMapping(value = "/deletePartyNews", method = RequestMethod.POST)
    public RetDataBean deletePartyNews(PartyNews partyNews) {
        try {
            if (StringUtils.isBlank(partyNews.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyNews.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyNewsService.deletePartyNews(partyNews));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyNews
     * @return RetDataBean
     * @Title: updatePartyNews
     * @Description:  更新党建新闻
     */
    @RequestMapping(value = "/updatePartyNews", method = RequestMethod.POST)
    public RetDataBean updatePartyNews(PartyNews partyNews) {
        try {
            if (StringUtils.isBlank(partyNews.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isNotBlank(partyNews.getContent())) {
                Document htmlDoc = Jsoup.parse(partyNews.getContent());
                String subheading = htmlDoc.text();
                if (subheading.length() > 50) {
                    subheading = subheading.substring(0, 50) + "...";
                }
                partyNews.setSubheading(subheading);
            } else {
                partyNews.setSubheading("");
            }
            Example example = new Example(PartyNews.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyNews.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyNewsService.updatePartyNews(example, partyNews));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyBusinessGuide
     * @return RetDataBean
     * @Title: insertPartyBusinessGuide
     * @Description:  添加业务指南
     */
    @RequestMapping(value = "/insertPartyBusinessGuide", method = RequestMethod.POST)
    public RetDataBean insertPartyBusinessGuide(PartyBusinessGuide partyBusinessGuide) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyBusinessGuide.setRecordId(SysTools.getGUID());
            if (StringUtils.isNotBlank(partyBusinessGuide.getContent())) {
                Document htmlDoc = Jsoup.parse(partyBusinessGuide.getContent());
                String subheading = htmlDoc.text();
                if (subheading.length() > 50) {
                    subheading = subheading.substring(0, 50) + "...";
                }
                partyBusinessGuide.setSubheading(subheading);
            } else {
                partyBusinessGuide.setSubheading("");
            }
            partyBusinessGuide.setCreateUser(account.getAccountId());
            partyBusinessGuide.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyBusinessGuide.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyBusinessGuideService.insertPartyBusinessGuide(partyBusinessGuide));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyBusinessGuide
     * @return RetDataBean
     * @Title: deletePartyBusinessGuide
     * @Description:  删除业务指南
     */
    @RequestMapping(value = "/deletePartyBusinessGuide", method = RequestMethod.POST)
    public RetDataBean deletePartyBusinessGuide(PartyBusinessGuide partyBusinessGuide) {
        try {
            if (StringUtils.isBlank(partyBusinessGuide.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyBusinessGuide.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyBusinessGuideService.deletePartyBusinessGuide(partyBusinessGuide));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyBusinessGuide
     * @return RetDataBean
     * @Title: updatePartyBusinessGuide
     * @Description:  更新业务指南
     */
    @RequestMapping(value = "/updatePartyBusinessGuide", method = RequestMethod.POST)
    public RetDataBean updatePartyBusinessGuide(PartyBusinessGuide partyBusinessGuide) {
        try {
            if (StringUtils.isBlank(partyBusinessGuide.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isNotBlank(partyBusinessGuide.getContent())) {
                Document htmlDoc = Jsoup.parse(partyBusinessGuide.getContent());
                String subheading = htmlDoc.text();
                if (subheading.length() > 50) {
                    subheading = subheading.substring(0, 50) + "...";
                }
                partyBusinessGuide.setSubheading(subheading);
            } else {
                partyBusinessGuide.setSubheading("");
            }
            Example example = new Example(PartyBusinessGuide.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyBusinessGuide.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyBusinessGuideService.updatePartyBusinessGuide(example, partyBusinessGuide));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyFunds
     * @return RetDataBean
     * @Title: insertPartyFunds
     * @Description:  发起经费申请
     */
    @RequestMapping(value = "/insertPartyFunds", method = RequestMethod.POST)
    public RetDataBean insertPartyFunds(PartyFunds partyFunds) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyFunds.setRecordId(SysTools.getGUID());
            partyFunds.setCreateUser(account.getAccountId());
            partyFunds.setStatus("0");
            if (StringUtils.isNotBlank(partyFunds.getNeedTime())) {
                String needMonth = partyFunds.getNeedTime().substring(0, 7);
                partyFunds.setNeedMonth(needMonth);
            }
            partyFunds.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyFunds.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyFundsService.insertPartyFunds(partyFunds));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyFunds
     * @return RetDataBean
     * @Title: deletePartyFunds
     * @Description:  删除经费申请
     */
    @RequestMapping(value = "/deletePartyFunds", method = RequestMethod.POST)
    public RetDataBean deletePartyFunds(PartyFunds partyFunds) {
        try {
            if (StringUtils.isBlank(partyFunds.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyFunds.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyFundsService.deletePartyFunds(partyFunds));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyFunds
     * @return RetDataBean
     * @Title: updatePartyFunds
     * @Description:  更新经费申请
     */
    @RequestMapping(value = "/updatePartyFunds", method = RequestMethod.POST)
    public RetDataBean updatePartyFunds(PartyFunds partyFunds) {
        try {
            if (StringUtils.isBlank(partyFunds.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isNotBlank(partyFunds.getNeedTime())) {
                String needMonth = partyFunds.getNeedTime().substring(0, 7);
                partyFunds.setNeedMonth(needMonth);
            }
            Example example = new Example(PartyFunds.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyFunds.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyFundsService.updatePartyFunds(example, partyFunds));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyPayStandard
     * @return RetDataBean
     * @Title: insertPartyPayStandard
     * @Description:  添加党费缴纳标准
     */
    @RequestMapping(value = "/insertPartyPayStandard", method = RequestMethod.POST)
    public RetDataBean insertPartyPayStandard(PartyPayStandard partyPayStandard) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyPayStandard.setRecordId(SysTools.getGUID());
            partyPayStandard.setCreateUser(account.getAccountId());
            partyPayStandard.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyPayStandard.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyPayStandardService.insertPartyPayStandard(partyPayStandard));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param file
     * @return ModelAndView
     * @Title: importPayStandard
     * @Description:  批量导入党费缴纳标准
     */
    @RequestMapping(value = "/importPayStandard", method = RequestMethod.POST)
    public ModelAndView importPayStandard(MultipartFile file) {
        ModelAndView mv = null;
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            mv = new ModelAndView("app/core/party/paystandard/payimport");
            RetDataBean retDataBean = partyPayStandardService.importPayStandard(account, file);
            mv.addObject("retDataBean", retDataBean);
            return mv;
        } catch (Exception e) {
            mv.addObject("retDataBean", RetDataTools.NotOk(MessageCode.MESSAGE_FAILED));
            return mv;
        }
    }

    /**
     * @param partyPayStandard
     * @return RetDataBean
     * @Title: deletePartyPayStandard
     * @Description:  删除党费缴纳标准
     */
    @RequestMapping(value = "/deletePartyPayStandard", method = RequestMethod.POST)
    public RetDataBean deletePartyPayStandard(PartyPayStandard partyPayStandard) {
        try {
            if (StringUtils.isBlank(partyPayStandard.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyPayStandard.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyPayStandardService.deletePartyPayStandard(partyPayStandard));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyPayStandard
     * @return RetDataBean
     * @Title: updatePartyPayStandard
     * @Description:  更新党费缴纳标准
     */
    @RequestMapping(value = "/updatePartyPayStandard", method = RequestMethod.POST)
    public RetDataBean updatePartyPayStandard(PartyPayStandard partyPayStandard) {
        try {
            if (StringUtils.isBlank(partyPayStandard.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyPayStandard.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyPayStandard.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyPayStandardService.updatePartyPayStandard(example, partyPayStandard));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param partyCleanCompre
     * @return RetDataBean
     * @Title: insertPartyCleanCompre
     * @Description:  添加感悟记录
     */
    @RequestMapping(value = "/insertPartyCleanCompre", method = RequestMethod.POST)
    public RetDataBean insertPartyCleanCompre(PartyCleanCompre partyCleanCompre) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyCleanCompre.setRecordId(SysTools.getGUID());
            Document htmlDoc = Jsoup.parse(partyCleanCompre.getContent());
            String subheading = htmlDoc.text();
            if (subheading.length() > 50) {
                subheading = subheading.substring(0, 50) + "...";
            }
            partyCleanCompre.setSubheading(subheading);
            partyCleanCompre.setStatus("0");
            partyCleanCompre.setCreateUser(account.getAccountId());
            partyCleanCompre.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyCleanCompre.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyCleanCompreService.insertPartyCleanCompre(partyCleanCompre));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyCleanCompre
     * @return RetDataBean
     * @Title: deletePartyCleanCompre
     * @Description:  删除感悟记录
     */
    @RequestMapping(value = "/deletePartyCleanCompre", method = RequestMethod.POST)
    public RetDataBean deletePartyCleanCompre(PartyCleanCompre partyCleanCompre) {
        try {
            if (StringUtils.isBlank(partyCleanCompre.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyCleanCompre.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyCleanCompreService.deletePartyCleanCompre(partyCleanCompre));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyCleanCompre
     * @return RetDataBean
     * @Title: updatePartyCleanCompreStatus
     * @Description:  更改感悟记录状态
     */
    @RequestMapping(value = "/updatePartyCleanCompreStatus", method = RequestMethod.POST)
    public RetDataBean updatePartyCleanCompreStatus(PartyCleanCompre partyCleanCompre) {
        try {
            if (StringUtils.isBlank(partyCleanCompre.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyCleanCompre.class);
            partyCleanCompre.setApprovalTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyCleanCompre.setApprovalUser(account.getAccountId());
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyCleanCompre.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyCleanCompreService.updatePartyCleanCompre(example, partyCleanCompre));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyCleanCompre
     * @return RetDataBean
     * @Title: updatePartyCleanCompre
     * @Description:  更新感悟记录
     */
    @RequestMapping(value = "/updatePartyCleanCompre", method = RequestMethod.POST)
    public RetDataBean updatePartyCleanCompre(PartyCleanCompre partyCleanCompre) {
        try {
            if (StringUtils.isBlank(partyCleanCompre.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Document htmlDoc = Jsoup.parse(partyCleanCompre.getContent());
            String subheading = htmlDoc.text();
            if (subheading.length() > 50) {
                subheading = subheading.substring(0, 50) + "...";
            }
            partyCleanCompre.setSubheading(subheading);
            Example example = new Example(PartyCleanCompre.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyCleanCompre.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyCleanCompreService.updatePartyCleanCompre(example, partyCleanCompre));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param partyCleanReport
     * @return RetDataBean
     * @Title: insertPartyCleanReport
     * @Description:  添加报告
     */
    @RequestMapping(value = "/insertPartyCleanReport", method = RequestMethod.POST)
    public RetDataBean insertPartyCleanReport(PartyCleanReport partyCleanReport) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyCleanReport.setRecordId(SysTools.getGUID());
            Document htmlDoc = Jsoup.parse(partyCleanReport.getContent());
            String subheading = htmlDoc.text();
            if (subheading.length() > 50) {
                subheading = subheading.substring(0, 50) + "...";
            }
            partyCleanReport.setSubheading(subheading);
            partyCleanReport.setStatus("0");
            partyCleanReport.setCreateUser(account.getAccountId());
            partyCleanReport.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyCleanReport.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyCleanReportService.insertPartyCleanReport(partyCleanReport));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyCleanReport
     * @return RetDataBean
     * @Title: deletePartyCleanReport
     * @Description:  删除报告记录
     */
    @RequestMapping(value = "/deletePartyCleanReport", method = RequestMethod.POST)
    public RetDataBean deletePartyCleanReport(PartyCleanReport partyCleanReport) {
        try {
            if (StringUtils.isBlank(partyCleanReport.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyCleanReport.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyCleanReportService.deletePartyCleanReport(partyCleanReport));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyCleanReport
     * @return RetDataBean
     * @Title: updatePartyCleanReportStatus
     * @Description:  更改报告记录状态
     */
    @RequestMapping(value = "/updatePartyCleanReportStatus", method = RequestMethod.POST)
    public RetDataBean updatePartyCleanReportStatus(PartyCleanReport partyCleanReport) {
        try {
            if (StringUtils.isBlank(partyCleanReport.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyCleanReport.class);
            partyCleanReport.setApprovalTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyCleanReport.setApprovalUser(account.getAccountId());
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyCleanReport.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyCleanReportService.updatePartyCleanReport(example, partyCleanReport));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyCleanReport
     * @return RetDataBean
     * @Title: updatePartyCleanReport
     * @Description:  更新报告记录
     */
    @RequestMapping(value = "/updatePartyCleanReport", method = RequestMethod.POST)
    public RetDataBean updatePartyCleanReport(PartyCleanReport partyCleanReport) {
        try {
            if (StringUtils.isBlank(partyCleanReport.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Document htmlDoc = Jsoup.parse(partyCleanReport.getContent());
            String subheading = htmlDoc.text();
            if (subheading.length() > 50) {
                subheading = subheading.substring(0, 50) + "...";
            }
            partyCleanReport.setSubheading(subheading);
            Example example = new Example(PartyCleanReport.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyCleanReport.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyCleanReportService.updatePartyCleanReport(example, partyCleanReport));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param partyCleanTipoffs
     * @return RetDataBean
     * @Title: insertPartyCleanTipoffs
     * @Description:  添加举报记录
     */
    @RequestMapping(value = "/insertPartyCleanTipoffs", method = RequestMethod.POST)
    public RetDataBean insertPartyCleanTipoffs(PartyCleanTipoffs partyCleanTipoffs) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyCleanTipoffs.setRecordId(SysTools.getGUID());
            Document htmlDoc = Jsoup.parse(partyCleanTipoffs.getContent());
            String subheading = htmlDoc.text();
            if (subheading.length() > 50) {
                subheading = subheading.substring(0, 50) + "...";
            }
            partyCleanTipoffs.setSubheading(subheading);
            partyCleanTipoffs.setStatus("0");
            partyCleanTipoffs.setCreateUser(account.getAccountId());
            partyCleanTipoffs.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyCleanTipoffs.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyCleanTipoffsService.insertPartyCleanTipoffs(partyCleanTipoffs));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyCleanTipoffs
     * @return RetDataBean
     * @Title: deletePartyCleanTipoffs
     * @Description:  删除举报记录
     */
    @RequestMapping(value = "/deletePartyCleanTipoffs", method = RequestMethod.POST)
    public RetDataBean deletePartyCleanTipoffs(PartyCleanTipoffs partyCleanTipoffs) {
        try {
            if (StringUtils.isBlank(partyCleanTipoffs.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyCleanTipoffs.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyCleanTipoffsService.deletePartyCleanTipoffs(partyCleanTipoffs));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyCleanTipoffs
     * @return RetDataBean
     * @Title: updatePartyCleanTipoffsStatus
     * @Description:  更改举报记录状态
     */
    @RequestMapping(value = "/updatePartyCleanTipoffsStatus", method = RequestMethod.POST)
    public RetDataBean updatePartyCleanTipoffsStatus(PartyCleanTipoffs partyCleanTipoffs) {
        try {
            if (StringUtils.isBlank(partyCleanTipoffs.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyCleanTipoffs.class);
            partyCleanTipoffs.setApprovalTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyCleanTipoffs.setApprovalUser(account.getAccountId());
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyCleanTipoffs.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyCleanTipoffsService.updatePartyCleanTipoffs(example, partyCleanTipoffs));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyCleanTipoffs
     * @return RetDataBean
     * @Title: updatePartyCleanTipoffs
     * @Description:  更新举报记录
     */
    @RequestMapping(value = "/updatePartyCleanTipoffs", method = RequestMethod.POST)
    public RetDataBean updatePartyCleanTipoffs(PartyCleanTipoffs partyCleanTipoffs) {
        try {
            if (StringUtils.isBlank(partyCleanTipoffs.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Document htmlDoc = Jsoup.parse(partyCleanTipoffs.getContent());
            String subheading = htmlDoc.text();
            if (subheading.length() > 50) {
                subheading = subheading.substring(0, 50) + "...";
            }
            partyCleanTipoffs.setSubheading(subheading);
            Example example = new Example(PartyCleanTipoffs.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyCleanTipoffs.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyCleanTipoffsService.updatePartyCleanTipoffs(example, partyCleanTipoffs));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyCleanSuggestions
     * @return RetDataBean
     * @Title: insertPartyCleanSuggestions
     * @Description:  添加献计献策
     */
    @RequestMapping(value = "/insertPartyCleanSuggestions", method = RequestMethod.POST)
    public RetDataBean insertPartyCleanSuggestions(PartyCleanSuggestions partyCleanSuggestions) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyCleanSuggestions.setRecordId(SysTools.getGUID());
            Document htmlDoc = Jsoup.parse(partyCleanSuggestions.getContent());
            String subheading = htmlDoc.text();
            if (subheading.length() > 50) {
                subheading = subheading.substring(0, 50) + "...";
            }
            partyCleanSuggestions.setSubheading(subheading);
            partyCleanSuggestions.setStatus("0");
            partyCleanSuggestions.setCreateUser(account.getAccountId());
            partyCleanSuggestions.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyCleanSuggestions.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyCleanSuggestionsService.insertPartyCleanSuggestions(partyCleanSuggestions));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyCleanSuggestions
     * @return RetDataBean
     * @Title: deletePartyCleanSuggestions
     * @Description:  删除献计献策
     */
    @RequestMapping(value = "/deletePartyCleanSuggestions", method = RequestMethod.POST)
    public RetDataBean deletePartyCleanSuggestions(PartyCleanSuggestions partyCleanSuggestions) {
        try {
            if (StringUtils.isBlank(partyCleanSuggestions.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyCleanSuggestions.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyCleanSuggestionsService.deletePartyCleanSuggestions(partyCleanSuggestions));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyCleanSuggestions
     * @return RetDataBean
     * @Title: updatePartyCleanSuggestionsStatus
     * @Description:  更改献计献策状态
     */
    @RequestMapping(value = "/updatePartyCleanSuggestionsStatus", method = RequestMethod.POST)
    public RetDataBean updatePartyCleanSuggestionsStatus(PartyCleanSuggestions partyCleanSuggestions) {
        try {
            if (StringUtils.isBlank(partyCleanSuggestions.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyCleanSuggestions.setApprovalTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyCleanSuggestions.setApprovalUser(account.getAccountId());
            Example example = new Example(PartyCleanSuggestions.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyCleanSuggestions.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyCleanSuggestionsService.updatePartyCleanSuggestions(example, partyCleanSuggestions));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyCleanSuggestions
     * @return RetDataBean
     * @Title: updatePartyCleanSuggestions
     * @Description:  更新献计献策
     */
    @RequestMapping(value = "/updatePartyCleanSuggestions", method = RequestMethod.POST)
    public RetDataBean updatePartyCleanSuggestions(PartyCleanSuggestions partyCleanSuggestions) {
        try {
            if (StringUtils.isBlank(partyCleanSuggestions.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Document htmlDoc = Jsoup.parse(partyCleanSuggestions.getContent());
            String subheading = htmlDoc.text();
            if (subheading.length() > 50) {
                subheading = subheading.substring(0, 50) + "...";
            }
            partyCleanSuggestions.setSubheading(subheading);
            Example example = new Example(PartyCleanSuggestions.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyCleanSuggestions.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyCleanSuggestionsService.updatePartyCleanSuggestions(example, partyCleanSuggestions));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param partyCleanMeritorious
     * @return RetDataBean
     * @Title: insertPartyCleanMeritorious
     * @Description:  添加先进事迹
     */
    @RequestMapping(value = "/insertPartyCleanMeritorious", method = RequestMethod.POST)
    public RetDataBean insertPartyCleanMeritorious(PartyCleanMeritorious partyCleanMeritorious) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyCleanMeritorious.setRecordId(SysTools.getGUID());
            Document htmlDoc = Jsoup.parse(partyCleanMeritorious.getContent());
            String subheading = htmlDoc.text();
            if (subheading.length() > 50) {
                subheading = subheading.substring(0, 50) + "...";
            }
            partyCleanMeritorious.setSubheading(subheading);
            partyCleanMeritorious.setStatus("0");
            partyCleanMeritorious.setCreateUser(account.getAccountId());
            partyCleanMeritorious.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyCleanMeritorious.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyCleanMeritoriousService.insertPartyCleanMeritorious(partyCleanMeritorious));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyCleanMeritorious
     * @return RetDataBean
     * @Title: deletePartyCleanMeritorious
     * @Description:  删除先进事迹
     */
    @RequestMapping(value = "/deletePartyCleanMeritorious", method = RequestMethod.POST)
    public RetDataBean deletePartyCleanMeritorious(PartyCleanMeritorious partyCleanMeritorious) {
        try {
            if (StringUtils.isBlank(partyCleanMeritorious.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyCleanMeritorious.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyCleanMeritoriousService.deletePartyCleanMeritorious(partyCleanMeritorious));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyCleanMeritorious
     * @return RetDataBean
     * @Title: updatePartyCleanMeritoriousStatus
     * @Description:  更改记录状态
     */
    @RequestMapping(value = "/updatePartyCleanMeritoriousStatus", method = RequestMethod.POST)
    public RetDataBean updatePartyCleanMeritoriousStatus(PartyCleanMeritorious partyCleanMeritorious) {
        try {
            if (StringUtils.isBlank(partyCleanMeritorious.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyCleanMeritorious.class);
            partyCleanMeritorious.setApprovalTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyCleanMeritorious.setApprovalUser(account.getAccountId());
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyCleanMeritorious.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyCleanMeritoriousService.updatePartyCleanMeritorious(example, partyCleanMeritorious));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyCleanMeritorious
     * @return RetDataBean
     * @Title: updatePartyCleanMeritorious
     * @Description:  更新先进事迹记录
     */
    @RequestMapping(value = "/updatePartyCleanMeritorious", method = RequestMethod.POST)
    public RetDataBean updatePartyCleanExpose(PartyCleanMeritorious partyCleanMeritorious) {
        try {
            if (StringUtils.isBlank(partyCleanMeritorious.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Document htmlDoc = Jsoup.parse(partyCleanMeritorious.getContent());
            String subheading = htmlDoc.text();
            if (subheading.length() > 50) {
                subheading = subheading.substring(0, 50) + "...";
            }
            partyCleanMeritorious.setSubheading(subheading);
            Example example = new Example(PartyCleanMeritorious.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyCleanMeritorious.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyCleanMeritoriousService.updatePartyCleanMeritorious(example, partyCleanMeritorious));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param memberIds
     * @param year
     * @return RetDataBean
     * @Title: addMemberIntegrals
     * @Description:  添加党员考核计划
     */
    @RequestMapping(value = "/addMemberIntegrals", method = RequestMethod.POST)
    public RetDataBean addMemberIntegrals(String memberIds, String year) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return partyIntegralService.addMemberIntegrals(account, memberIds, year);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyIntegral
     * @return RetDataBean
     * @Title: deletePartyIntegral
     * @Description:  删除党员考核记录
     */
    @RequestMapping(value = "/deletePartyIntegral", method = RequestMethod.POST)
    public RetDataBean deletePartyIntegral(PartyIntegral partyIntegral) {
        try {
            if (StringUtils.isBlank(partyIntegral.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyIntegral.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyIntegralService.deletePartyIntegral(partyIntegral));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyIntegral
     * @return RetDataBean
     * @Title: updatePartyIntegral
     * @Description:  更新党员考核记录
     */
    @RequestMapping(value = "/updatePartyIntegral", method = RequestMethod.POST)
    public RetDataBean updatePartyIntegral(PartyIntegral partyIntegral) {
        try {
            if (StringUtils.isBlank(partyIntegral.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyIntegral.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyIntegral.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyIntegralService.updatePartyIntegral(example, partyIntegral));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyCleanExpose
     * @return RetDataBean
     * @Title: insertPartyCleanExpose
     * @Description:  添加曝光记录
     */
    @RequestMapping(value = "/insertPartyCleanExpose", method = RequestMethod.POST)
    public RetDataBean insertPartyCleanExpose(PartyCleanExpose partyCleanExpose) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyCleanExpose.setRecordId(SysTools.getGUID());
            Document htmlDoc = Jsoup.parse(partyCleanExpose.getContent());
            String subheading = htmlDoc.text();
            if (subheading.length() > 50) {
                subheading = subheading.substring(0, 50) + "...";
            }
            partyCleanExpose.setSubheading(subheading);
            partyCleanExpose.setStatus("0");
            partyCleanExpose.setCreateUser(account.getAccountId());
            partyCleanExpose.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyCleanExpose.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyCleanExposeService.insertPartyCleanExpose(partyCleanExpose));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyCleanExpose
     * @return RetDataBean
     * @Title: deletePartyCleanExpose
     * @Description:  删除曝光记录
     */
    @RequestMapping(value = "/deletePartyCleanExpose", method = RequestMethod.POST)
    public RetDataBean deletePartyCleanExpose(PartyCleanExpose partyCleanExpose) {
        try {
            if (StringUtils.isBlank(partyCleanExpose.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyCleanExpose.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyCleanExposeService.deletePartyCleanExpose(partyCleanExpose));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyCleanExpose
     * @return RetDataBean
     * @Title: updatePartyCleanExposeStatus
     * @Description:  更改记录状态
     */
    @RequestMapping(value = "/updatePartyCleanExposeStatus", method = RequestMethod.POST)
    public RetDataBean updatePartyCleanExposeStatus(PartyCleanExpose partyCleanExpose) {
        try {
            if (StringUtils.isBlank(partyCleanExpose.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyCleanExpose.setApprovalTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyCleanExpose.setApprovalUser(account.getAccountId());
            Example example = new Example(PartyCleanExpose.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyCleanExpose.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyCleanExposeService.updatePartyCleanExpose(example, partyCleanExpose));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyCleanExpose
     * @return RetDataBean
     * @Title: updatePartyCleanExpose
     * @Description:  更新曝光记录
     */
    @RequestMapping(value = "/updatePartyCleanExpose", method = RequestMethod.POST)
    public RetDataBean updatePartyCleanExpose(PartyCleanExpose partyCleanExpose) {
        try {
            if (StringUtils.isBlank(partyCleanExpose.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Document htmlDoc = Jsoup.parse(partyCleanExpose.getContent());
            String subheading = htmlDoc.text();
            if (subheading.length() > 50) {
                subheading = subheading.substring(0, 50) + "...";
            }
            partyCleanExpose.setSubheading(subheading);
            Example example = new Example(PartyCleanExpose.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyCleanExpose.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyCleanExposeService.updatePartyCleanExpose(example, partyCleanExpose));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param partyDues
     * @return RetDataBean
     * @Title: insertPartyDues
     * @Description:  添加党费
     */
    @RequestMapping(value = "/insertPartyDues", method = RequestMethod.POST)
    public RetDataBean insertPartyDues(PartyDues partyDues) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyDues.setRecordId(SysTools.getGUID());
            partyDues.setCreateUser(account.getAccountId());
            partyDues.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyDues.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyDuesService.insertPartyDues(partyDues));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyDues
     * @return RetDataBean
     * @Title: deletePartyDues
     * @Description:  删除党费信息
     */
    @RequestMapping(value = "/deletePartyDues", method = RequestMethod.POST)
    public RetDataBean deletePartyDues(PartyDues partyDues) {
        try {
            if (StringUtils.isBlank(partyDues.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyDues.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyDuesService.deletePartyDues(partyDues));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyDues
     * @return RetDataBean
     * @Title: updatePartyDues
     * @Description:  更新党费信息
     */
    @RequestMapping(value = "/updatePartyDues", method = RequestMethod.POST)
    public RetDataBean updatePartyDues(PartyDues partyDues) {
        try {
            if (StringUtils.isBlank(partyDues.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyDues.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyDues.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyDuesService.updatePartyDues(example, partyDues));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param partyAlbumHistory
     * @return RetDataBean
     * @Title: setAlbumClearCache
     * @Description:  清除浏览记录
     */
    @RequestMapping(value = "/setAlbumClearCache", method = RequestMethod.POST)
    public RetDataBean setAlbumClearCache(PartyAlbumHistory partyAlbumHistory) {
        try {
            if (StringUtils.isBlank(partyAlbumHistory.getVideoId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyAlbumHistory.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyAlbumHistoryService.deletePartyAlbumHistory(partyAlbumHistory));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyAlbumVideo
     * @return RetDataBean
     * @Title: setAlbumVideoClickCount
     * @Description:  设置播放次数
     */
    @RequestMapping(value = "/setAlbumVideoClickCount", method = RequestMethod.POST)
    public RetDataBean setAlbumVideoClickCount(PartyAlbumVideo partyAlbumVideo) {
        try {
            if (StringUtils.isBlank(partyAlbumVideo.getVideoId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyAlbumVideo.setOrgId(account.getOrgId());
            partyAlbumHistoryService.addAlbumHistory(account, partyAlbumVideo.getVideoId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyAlbumVideoService.setAlbumVideoClickCount(partyAlbumVideo));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyAlbumComments
     * @return RetDataBean
     * @Title: insertPartyAlbumComments
     * @Description:  发表评论
     */
    @RequestMapping(value = "/insertPartyAlbumComments", method = RequestMethod.POST)
    public RetDataBean insertPartyAlbumComments(PartyAlbumComments partyAlbumComments) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyAlbumComments.setCommentsId(SysTools.getGUID());
            partyAlbumComments.setCreateUser(account.getAccountId());
            partyAlbumComments.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyAlbumComments.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyAlbumCommentsService.insertPartyAlbumComments(partyAlbumComments));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyAlbumVideo
     * @return RetDataBean
     * @Title: setAlbumApproval
     * @Description:  专辑审核
     */
    @RequestMapping(value = "/setAlbumApproval", method = RequestMethod.POST)
    public RetDataBean setAlbumApproval(PartyAlbumVideo partyAlbumVideo) {
        try {
            if (StringUtils.isBlank(partyAlbumVideo.getVideoId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyAlbumVideo.setApprovelTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            Example example = new Example(PartyAlbumVideo.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("videoId", partyAlbumVideo.getVideoId());
            partyAlbumVideoService.updatePartyAlbumVideo(example, partyAlbumVideo);
            return RetDataTools.Ok(MessageCode.MESSAGE_APPROVAL_SUCCESS);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param subscribeUser
     * @return RetDataBean
     * @Title: setCancelSubscribeUser
     * @Description:  取消关注
     */
    @RequestMapping(value = "/setCancelSubscribeUser", method = RequestMethod.POST)
    public RetDataBean setCancelSubscribeUser(String subscribeUser) {
        try {
            if (StringUtils.isBlank(subscribeUser)) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyAlbumSubscribeService.setCancelSubscribeUser(account, subscribeUser);
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param partyAlbumSubscribe
     * @return RetDataBean
     * @Title: setSubscribeUser
     * @Description:  订阅
     */
    @RequestMapping(value = "/setSubscribeUser", method = RequestMethod.POST)
    public RetDataBean setSubscribeUser(PartyAlbumSubscribe partyAlbumSubscribe) {
        try {
            if (StringUtils.isBlank(partyAlbumSubscribe.getSubscribeUser())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyAlbumSubscribe.setOrgId(account.getOrgId());
            partyAlbumSubscribe.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyAlbumSubscribe.setCreateUser(account.getAccountId());
            partyAlbumSubscribeService.setSubscribeUser(partyAlbumSubscribe);
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyAlbumConfig
     * @return RetDataBean
     * @Title: setAlbumConfig
     * @Description:  设置专辑参数
     */
    @RequestMapping(value = "/setAlbumConfig", method = RequestMethod.POST)
    public RetDataBean setAlbumConfig(PartyAlbumConfig partyAlbumConfig) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyAlbumConfig.setConfigId(SysTools.getGUID());
            partyAlbumConfig.setOrgId(account.getOrgId());
            partyAlbumConfig.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyAlbumConfig.setCreateUser(account.getAccountId());
            return partyAlbumConfigService.setAlbumConfig(partyAlbumConfig);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyAlbumHome
     * @return RetDataBean
     * @Title: setAlbumHomeInfo
     * @Description:  设置首页面
     */
    @RequestMapping(value = "/setAlbumHomeInfo", method = RequestMethod.POST)
    public RetDataBean setAlbumHomeInfo(PartyAlbumHome partyAlbumHome) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyAlbumHome.setHomePageId(SysTools.getGUID());
            partyAlbumHome.setOrgId(account.getOrgId());
            partyAlbumHome.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyAlbumHome.setCreateUser(account.getAccountId());
            return partyAlbumHomeService.setAlbumHomeInfo(partyAlbumHome);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: deletePartyAlbumTypeBatch
     * @Description:  批量删除分类码
     * @param: request
     * @param: codeClass
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/deletePartyAlbumTypeBatch", method = RequestMethod.POST)
    public RetDataBean deletePartyAlbumTypeBatch(@RequestParam(value = "typeIdArr[]") String[] typeIdArr) {
        try {
            if (typeIdArr.length == 0) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            }
            List<String> list = new ArrayList<String>(Arrays.asList(typeIdArr));
            Example example = new Example(PartyAlbumType.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andIn("albumTypeId", list);
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyAlbumTypeService.deletePartyAlbumType(example));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyAlbumVideo
     * @return RetDataBean
     * @Title: insertPartyAlbumVideo
     * @Description:  上传视频专辑
     */
    @RequestMapping(value = "/insertPartyAlbumVideo", method = RequestMethod.POST)
    public RetDataBean insertPartyAlbumVideo(PartyAlbumVideo partyAlbumVideo) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyAlbumVideo.setVideoId(SysTools.getGUID());
            Document htmlDoc = Jsoup.parse(partyAlbumVideo.getRemark());
            String subheading = htmlDoc.text();
            if (subheading.length() > 50) {
                subheading = subheading.substring(0, 50) + "...";
            }
            if (StringUtils.isBlank(partyAlbumVideo.getSendTime())) {
                partyAlbumVideo.setSendTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            }
            partyAlbumVideo.setStatus("0");
            partyAlbumVideo.setClickCount(0);
            partyAlbumVideo.setSubheading(subheading);
            partyAlbumVideo.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyAlbumVideo.setCreateUser(account.getAccountId());
            partyAlbumVideo.setOrgId(account.getOrgId());
            return partyAlbumVideoService.upLoadVideo(partyAlbumVideo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyAlbumVideo
     * @return RetDataBean
     * @Title: deletePartyAlbumVideo
     * @Description:  删除视频专辑
     */
    @RequestMapping(value = "/deletePartyAlbumVideo", method = RequestMethod.POST)
    public RetDataBean deletePartyAlbumVideo(PartyAlbumVideo partyAlbumVideo) {
        try {
            if (StringUtils.isBlank(partyAlbumVideo.getVideoId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyAlbumVideo.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyAlbumVideoService.deletePartyAlbumVideo(partyAlbumVideo));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyAlbumVideo
     * @return RetDataBean
     * @Title: updatePartyAlbumVideo
     * @Description:  专辑更新
     */
    @RequestMapping(value = "/updatePartyAlbumVideo", method = RequestMethod.POST)
    public RetDataBean updatePartyAlbumVideo(PartyAlbumVideo partyAlbumVideo) {
        try {
            if (StringUtils.isBlank(partyAlbumVideo.getVideoId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Document htmlDoc = Jsoup.parse(partyAlbumVideo.getRemark());
            String subheading = htmlDoc.text();
            if (subheading.length() > 50) {
                subheading = subheading.substring(0, 50) + "...";
            }
            if (StringUtils.isBlank(partyAlbumVideo.getSendTime())) {
                partyAlbumVideo.setSendTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            }
            partyAlbumVideo.setStatus("0");
            partyAlbumVideo.setClickCount(0);
            partyAlbumVideo.setSubheading(subheading);
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyAlbumVideo.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("videoId", partyAlbumVideo.getVideoId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyAlbumVideoService.updatePartyAlbumVideo(example, partyAlbumVideo));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyAlbumType
     * @return RetDataBean
     * @Title: insertPartyAlbumType
     * @Description:  添加专辑类型
     */
    @RequestMapping(value = "/insertPartyAlbumType", method = RequestMethod.POST)
    public RetDataBean insertPartyAlbumType(PartyAlbumType partyAlbumType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyAlbumType.setAlbumTypeId(SysTools.getGUID());
            partyAlbumType.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyAlbumType.setCreateUser(account.getAccountId());
            partyAlbumType.setOrgId(account.getOrgId());
            partyAlbumType.setStatus("0");
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyAlbumTypeService.insertPartyAlbumType(partyAlbumType));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyAlbumType
     * @return RetDataBean
     * @Title: deletePartyAlbumType
     * @Description:  删除专辑类型
     */
    @RequestMapping(value = "/deletePartyAlbumType", method = RequestMethod.POST)
    public RetDataBean deletePartyAlbumType(PartyAlbumType partyAlbumType) {
        try {
            if (StringUtils.isBlank(partyAlbumType.getAlbumTypeId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyAlbumType.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyAlbumTypeService.deletePartyAlbumType(partyAlbumType));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyAlbumType
     * @return RetDataBean
     * @Title: updatePartyAlbumType
     * @Description:  更新专辑类型
     */
    @RequestMapping(value = "/updatePartyAlbumType", method = RequestMethod.POST)
    public RetDataBean updatePartyAlbumType(PartyAlbumType partyAlbumType) {
        try {
            if (StringUtils.isBlank(partyAlbumType.getAlbumTypeId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyAlbumType.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("albumTypeId", partyAlbumType.getAlbumTypeId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyAlbumTypeService.updatePartyAlbumType(example, partyAlbumType));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyRulesLearn
     * @return RetDataBean
     * @Title: insertPartyRulesLearn
     * @Description:  添加学习记录
     */
    @RequestMapping(value = "/insertPartyRulesLearn", method = RequestMethod.POST)
    public RetDataBean insertPartyRulesLearn(PartyRulesLearn partyRulesLearn) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyRulesLearn.setRecordId(SysTools.getGUID());
            partyRulesLearn.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyRulesLearn.setCreateUser(account.getAccountId());
            partyRulesLearn.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyRulesLearnService.insertPartyRulesLearn(partyRulesLearn));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyRulesLearn
     * @return RetDataBean
     * @Title: deletePartyRulesRecord
     * @Description:  删除学习记录
     */
    @RequestMapping(value = "/deletePartyRulesLearn", method = RequestMethod.POST)
    public RetDataBean deletePartyRulesLearn(PartyRulesLearn partyRulesLearn) {
        try {
            if (StringUtils.isBlank(partyRulesLearn.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyRulesLearn.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyRulesLearnService.deletePartyRulesLearn(partyRulesLearn));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyRulesLearn
     * @return RetDataBean
     * @Title: updatePartyRulesLearn
     * @Description:  更新学习记录
     */
    @RequestMapping(value = "/updatePartyRulesLearn", method = RequestMethod.POST)
    public RetDataBean updatePartyRulesLearn(PartyRulesLearn partyRulesLearn) {
        try {
            if (StringUtils.isBlank(partyRulesLearn.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyRulesLearn.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyRulesLearn.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyRulesLearnService.updatePartyRulesLearn(example, partyRulesLearn));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyRulesRecord
     * @return RetDataBean
     * @Title: insertPartyRulesRecord
     * @Description:  创建规章制度
     */
    @RequestMapping(value = "/insertPartyRulesRecord", method = RequestMethod.POST)
    public RetDataBean insertPartyRulesRecord(PartyRulesRecord partyRulesRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            Document htmlDoc = Jsoup.parse(partyRulesRecord.getContent());
            String subheading = htmlDoc.text();
            if (subheading.length() > 50) {
                subheading = subheading.substring(0, 50) + "...";
            }
            partyRulesRecord.setSubheading(subheading);
            partyRulesRecord.setRecordId(SysTools.getGUID());
            partyRulesRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyRulesRecord.setCreateUser(account.getAccountId());
            partyRulesRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyRulesRecordService.insertPartyRulesRecord(partyRulesRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyRulesRecord
     * @return RetDataBean
     * @Title: deletePartyRulesRecord
     * @Description:  删除规章制度
     */
    @RequestMapping(value = "/deletePartyRulesRecord", method = RequestMethod.POST)
    public RetDataBean deletePartyRulesRecord(PartyRulesRecord partyRulesRecord) {
        try {
            if (StringUtils.isBlank(partyRulesRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyRulesRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyRulesRecordService.deletePartyRulesRecord(partyRulesRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param @param  request
     * @param @param  contractSort
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: updatePartyRulesRecord
     * @Description:  更新规章制度信息
     */
    @RequestMapping(value = "/updatePartyRulesRecord", method = RequestMethod.POST)
    public RetDataBean updatePartyRulesRecord(PartyRulesRecord partyRulesRecord) {
        try {
            if (StringUtils.isBlank(partyRulesRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Document htmlDoc = Jsoup.parse(partyRulesRecord.getContent());
            String subheading = htmlDoc.text();
            if (subheading.length() > 50) {
                subheading = subheading.substring(0, 50) + "...";
            }
            partyRulesRecord.setSubheading(subheading);
            Example example = new Example(PartyRulesRecord.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyRulesRecord.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyRulesRecordService.updatePartyRulesRecord(example, partyRulesRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  contractSort
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: deletePartyRulesSort
     * @Description:  删除分类
     */
    @RequestMapping(value = "/deletePartyRulesSort", method = RequestMethod.POST)
    public RetDataBean deletePartyRulesSort(PartyRulesSort partyRulesSort) {
        try {
            if (StringUtils.isBlank(partyRulesSort.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyRulesSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyRulesSortService.deletePartyRulesSort(partyRulesSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  contractSort
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: insertPartyRulesSort
     * @Description:  创建规章制度分类
     */
    @RequestMapping(value = "/insertPartyRulesSort", method = RequestMethod.POST)
    public RetDataBean insertPartyRulesSort(PartyRulesSort partyRulesSort) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyRulesSort.setSortId(SysTools.getGUID());
            partyRulesSort.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyRulesSort.setCreateUser(account.getAccountId());
            if (StringUtils.isBlank(partyRulesSort.getSortLevel())) {
                partyRulesSort.setSortLevel("0");
            }
            partyRulesSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyRulesSortService.insertPartyRulesSort(partyRulesSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  contractSort
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: updatePartyRulesSort
     * @Description:  更新规章制度分类信息
     */
    @RequestMapping(value = "/updatePartyRulesSort", method = RequestMethod.POST)
    public RetDataBean updatePartyRulesSort(PartyRulesSort partyRulesSort) {
        try {
            if (StringUtils.isBlank(partyRulesSort.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyRulesSort.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("sortId", partyRulesSort.getSortId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyRulesSortService.updatePartyRulesSort(example, partyRulesSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }
}
