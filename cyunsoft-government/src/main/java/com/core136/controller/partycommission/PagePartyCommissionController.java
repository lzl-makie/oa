package com.core136.controller.partycommission;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/app/core/partycommission")
public class PagePartyCommissionController {
    /**
     * @return ModelAndView
     * @Title: goPartyEfficiency
     * @Description:  效能监察工作
     */
    @RequestMapping("/partyefficiency")
    @RequiresPermissions("/app/core/partycommission/partyefficiency")
    public ModelAndView goPartyEfficiency() {
        try {
            return new ModelAndView("app/core/partycommission/partyefficiency");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goBuildClean
     * @Description:  党风廉政建设
     */
    @RequestMapping("/buildclean")
    @RequiresPermissions("/app/core/partycommission/buildclean")
    public ModelAndView goBuildClean() {
        try {
            return new ModelAndView("app/core/partycommission/buildclean");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goBigEvent
     * @Description:  重大事件
     */
    @RequestMapping("/bigevent")
    @RequiresPermissions("/app/core/partycommission/bigevent")
    public ModelAndView goBigEvent(String view) {
        try {
            return new ModelAndView("app/core/partycommission/bigevent");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goInFlow
     * @Description:  纪委管理
     */
    @RequestMapping("/commission")
    @RequiresPermissions("/app/core/partycommission/commission")
    public ModelAndView goInFlow(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partycommission/commission");
            } else {
                if (view.equals("info")) {
                    mv = new ModelAndView("app/core/partycommission/commissioninfo");
                } else if (view.equals("list")) {
                    mv = new ModelAndView("app/core/partycommission/commissionlist");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }
}
