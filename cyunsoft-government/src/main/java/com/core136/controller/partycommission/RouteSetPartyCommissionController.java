package com.core136.controller.partycommission;

import com.core136.bean.account.Account;
import com.core136.bean.partycommission.PartyBigEvent;
import com.core136.bean.partycommission.PartyBuildClean;
import com.core136.bean.partycommission.PartyCommission;
import com.core136.bean.partycommission.PartyEfficiency;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.partycommission.PartyBigEventService;
import com.core136.service.partycommission.PartyBuildCleanService;
import com.core136.service.partycommission.PartyCommissionService;
import com.core136.service.partycommission.PartyEfficiencyService;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

@RestController
@RequestMapping("/set/partycommissionset")
public class RouteSetPartyCommissionController {
    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private PartyCommissionService partyCommissionService;

    @Autowired
    public void setPartyCommissionService(PartyCommissionService partyCommissionService) {
        this.partyCommissionService = partyCommissionService;
    }

    private PartyBigEventService partyBigEventService;

    @Autowired
    public void setPartyBigEventService(PartyBigEventService partyBigEventService) {
        this.partyBigEventService = partyBigEventService;
    }

    private PartyEfficiencyService partyEfficiencyService;

    @Autowired
    public void setPartyEfficiencyService(PartyEfficiencyService partyEfficiencyService) {
        this.partyEfficiencyService = partyEfficiencyService;
    }

    private PartyBuildCleanService partyBuildCleanService;

    @Autowired
    public void setPartyBuildCleanService(PartyBuildCleanService partyBuildCleanService) {
        this.partyBuildCleanService = partyBuildCleanService;
    }

    /**
     * @param partyEfficiency
     * @return RetDataBean
     * @Title: insertPartyEfficiency
     * @Description:  添加效能监察工作
     */
    @RequestMapping(value = "/insertPartyEfficiency", method = RequestMethod.POST)
    public RetDataBean insertPartyEfficiency(PartyEfficiency partyEfficiency) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyEfficiency.setRecordId(SysTools.getGUID());
            if (StringUtils.isNotBlank(partyEfficiency.getRemark())) {
                Document htmlDoc = Jsoup.parse(partyEfficiency.getRemark());
                String subheading = htmlDoc.text();
                if (subheading.length() > 50) {
                    subheading = subheading.substring(0, 50) + "...";
                }
                partyEfficiency.setSubheading(subheading);
            } else {
                partyEfficiency.setSubheading("");
            }
            partyEfficiency.setCreateUser(account.getAccountId());
            partyEfficiency.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyEfficiency.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyEfficiencyService.insertPartyEfficiency(partyEfficiency));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyEfficiency
     * @return RetDataBean
     * @Title: deletePartyEfficiency
     * @Description:  删除效能监察工作
     */
    @RequestMapping(value = "/deletePartyEfficiency", method = RequestMethod.POST)
    public RetDataBean deletePartyEfficiency(PartyEfficiency partyEfficiency) {
        try {
            if (StringUtils.isBlank(partyEfficiency.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyEfficiency.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyEfficiencyService.deletePartyEfficiency(partyEfficiency));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyEfficiency
     * @return RetDataBean
     * @Title: updatePartyEfficiency
     * @Description:  更新效能监察工作
     */
    @RequestMapping(value = "/updatePartyEfficiency", method = RequestMethod.POST)
    public RetDataBean updatePartyEfficiency(PartyEfficiency partyEfficiency) {
        try {
            if (StringUtils.isBlank(partyEfficiency.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isNotBlank(partyEfficiency.getRemark())) {
                Document htmlDoc = Jsoup.parse(partyEfficiency.getRemark());
                String subheading = htmlDoc.text();
                if (subheading.length() > 50) {
                    subheading = subheading.substring(0, 50) + "...";
                }
                partyEfficiency.setSubheading(subheading);
            } else {
                partyEfficiency.setSubheading("");
            }
            Example example = new Example(PartyEfficiency.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyEfficiency.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyEfficiencyService.updatePartyEfficiency(example, partyEfficiency));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyBuildClean
     * @return RetDataBean
     * @Title: insertPartyBuildClean
     * @Description:  添加党风廉政建设记录
     */
    @RequestMapping(value = "/insertPartyBuildClean", method = RequestMethod.POST)
    public RetDataBean insertPartyBuildClean(PartyBuildClean partyBuildClean) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyBuildClean.setRecordId(SysTools.getGUID());
            if (StringUtils.isNotBlank(partyBuildClean.getRemark())) {
                Document htmlDoc = Jsoup.parse(partyBuildClean.getRemark());
                String subheading = htmlDoc.text();
                if (subheading.length() > 50) {
                    subheading = subheading.substring(0, 50) + "...";
                }
                partyBuildClean.setSubheading(subheading);
            } else {
                partyBuildClean.setSubheading("");
            }
            partyBuildClean.setCreateUser(account.getAccountId());
            partyBuildClean.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyBuildClean.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyBuildCleanService.insertPartyBuildClean(partyBuildClean));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyBuildClean
     * @return RetDataBean
     * @Title: deletePartyBuildClean
     * @Description:  删除党风廉政建设
     */
    @RequestMapping(value = "/deletePartyBuildClean", method = RequestMethod.POST)
    public RetDataBean deletePartyBuildClean(PartyBuildClean partyBuildClean) {
        try {
            if (StringUtils.isBlank(partyBuildClean.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyBuildClean.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyBuildCleanService.deletePartyBuildClean(partyBuildClean));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyBuildClean
     * @return RetDataBean
     * @Title: updatePartyBuildClean
     * @Description:  更新党风廉政建设记录
     */
    @RequestMapping(value = "/updatePartyBuildClean", method = RequestMethod.POST)
    public RetDataBean updatePartyBuildClean(PartyBuildClean partyBuildClean) {
        try {
            if (StringUtils.isBlank(partyBuildClean.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isNotBlank(partyBuildClean.getRemark())) {
                Document htmlDoc = Jsoup.parse(partyBuildClean.getRemark());
                String subheading = htmlDoc.text();
                if (subheading.length() > 50) {
                    subheading = subheading.substring(0, 50) + "...";
                }
                partyBuildClean.setSubheading(subheading);
            } else {
                partyBuildClean.setSubheading("");
            }
            Example example = new Example(PartyBuildClean.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyBuildClean.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyBuildCleanService.updatePartyBuildClean(example, partyBuildClean));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyCommission
     * @return RetDataBean
     * @Title: insertPartyCommission
     * @Description:  添加纪委届次信息
     */
    @RequestMapping(value = "/insertPartyCommission", method = RequestMethod.POST)
    public RetDataBean insertPartyCommission(PartyCommission partyCommission) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyCommission.setRecordId(SysTools.getGUID());
            partyCommission.setCreateUser(account.getAccountId());
            partyCommission.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyCommission.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyCommissionService.insertPartyCommission(partyCommission));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyCommission
     * @return RetDataBean
     * @Title: deletePartyCommission
     * @Description:  删除纪委届次信息
     */
    @RequestMapping(value = "/deletePartyCommission", method = RequestMethod.POST)
    public RetDataBean deletePartyCommission(PartyCommission partyCommission) {
        try {
            if (StringUtils.isBlank(partyCommission.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyCommission.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyCommissionService.deletePartyCommission(partyCommission));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyCommission
     * @return RetDataBean
     * @Title: updatePartyCommission
     * @Description:  更新纪委届次信息
     */
    @RequestMapping(value = "/updatePartyCommission", method = RequestMethod.POST)
    public RetDataBean updatePartyCommission(PartyCommission partyCommission) {
        try {
            if (StringUtils.isBlank(partyCommission.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyCommission.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyCommission.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyCommissionService.updatePartyCommission(example, partyCommission));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyBigEvent
     * @return RetDataBean
     * @Title: insertPartyBigEvent
     * @Description:  添加重大事件
     */
    @RequestMapping(value = "/insertPartyBigEvent", method = RequestMethod.POST)
    public RetDataBean insertPartyBigEvent(PartyBigEvent partyBigEvent) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyBigEvent.setRecordId(SysTools.getGUID());
            if (StringUtils.isNotBlank(partyBigEvent.getRemark())) {
                Document htmlDoc = Jsoup.parse(partyBigEvent.getRemark());
                String subheading = htmlDoc.text();
                if (subheading.length() > 50) {
                    subheading = subheading.substring(0, 50) + "...";
                }
                partyBigEvent.setSubheading(subheading);
            } else {
                partyBigEvent.setSubheading("");
            }
            partyBigEvent.setCreateUser(account.getAccountId());
            partyBigEvent.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyBigEvent.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyBigEventService.insertPartyBigEvent(partyBigEvent));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyBigEvent
     * @return RetDataBean
     * @Title: deletePartyBigEvent
     * @Description:  删除重大事件
     */
    @RequestMapping(value = "/deletePartyBigEvent", method = RequestMethod.POST)
    public RetDataBean deletePartyBigEvent(PartyBigEvent partyBigEvent) {
        try {
            if (StringUtils.isBlank(partyBigEvent.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isNotBlank(partyBigEvent.getRemark())) {
                Document htmlDoc = Jsoup.parse(partyBigEvent.getRemark());
                String subheading = htmlDoc.text();
                if (subheading.length() > 50) {
                    subheading = subheading.substring(0, 50) + "...";
                }
                partyBigEvent.setSubheading(subheading);
            } else {
                partyBigEvent.setSubheading("");
            }
            partyBigEvent.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyBigEventService.deletePartyBigEvent(partyBigEvent));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyBigEvent
     * @return RetDataBean
     * @Title: updatePartyBigEvent
     * @Description:  更新重大事件
     */
    @RequestMapping(value = "/updatePartyBigEvent", method = RequestMethod.POST)
    public RetDataBean updatePartyBigEvent(PartyBigEvent partyBigEvent) {
        try {
            if (StringUtils.isBlank(partyBigEvent.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyBigEvent.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyBigEvent.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyBigEventService.updatePartyBigEvent(example, partyBigEvent));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


}
