package com.core136.controller.partyparam;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/app/core/partyparam")
public class PagePartyParamController {
    /**
     * @param request
     * @return ModelAndView
     * @Title: goBindAccount
     * @Description:  账号绑定
     */
    @RequestMapping("/bindaccount")
    @RequiresPermissions("/app/core/partyparam/bindaccount")
    public ModelAndView goBindAccount(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/partyparam/bindaccount");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param request
     * @return ModelAndView
     * @Title: goStratumSet
     * @Description: 社会阶层设置
     */
    @RequestMapping("/stratumSet")
    @RequiresPermissions("/app/core/partyparam/stratumSet")
    public ModelAndView goStratumSet(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/partyparam/stratum");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


    /**
     * @param request
     * @return ModelAndView
     * @Title: goAppraisalReasonSet
     * @Description:  评议奖惩原因
     */
    @RequestMapping("/appraisalreasonSet")
    @RequiresPermissions("/app/core/partyparam/appraisalreasonSet")
    public ModelAndView goAppraisalReasonSet(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/partyparam/appraisalreason");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


    /**
     * @param request
     * @return ModelAndView
     * @Title: goAppraisalSituationSet
     * @Description:  评议奖惩情况
     */
    @RequestMapping("/appraisalsituationSet")
    @RequiresPermissions("/app/core/partyparam/appraisalsituationSet")
    public ModelAndView goAppraisalSituationSet(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/partyparam/appraisalsituation");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param request
     * @return ModelAndView
     * @Title: goUnitServiceSet
     * @Description:  服务行业设置
     */
    @RequestMapping("/unitServiceSet")
    @RequiresPermissions("/app/core/partyparam/unitServiceSet")
    public ModelAndView goUnitServiceSet(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/partyparam/service");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param request
     * @return ModelAndView
     * @Title: goUnitIndustrySet
     * @Description:  所属行业设置
     */
    @RequestMapping("/unitIndustrySet")
    @RequiresPermissions("/app/core/partyparam/unitIndustrySet")
    public ModelAndView goUnitIndustrySet(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/partyparam/industry");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param request
     * @return ModelAndView
     * @Title: goGovPostSet
     * @Description:  党内职务设置
     */
    @RequestMapping("/govpostSet")
    @RequiresPermissions("/app/core/partyparam/govpostSet")
    public ModelAndView goGovPostSet(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/partyparam/govpost");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param request
     * @return ModelAndView
     * @Title: goAdmPositionSet
     * @Description:  行政职务设置
     */
    @RequestMapping("/admpositionSet")
    @RequiresPermissions("/app/core/partyparam/admpositionSet")
    public ModelAndView goAdmPositionSet(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/partyparam/admposition");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param request
     * @return ModelAndView
     * @Title: goTitleSet
     * @Description:  职称设置
     */
    @RequestMapping("/titleSet")
    @RequiresPermissions("/app/core/partyparam/titleSet")
    public ModelAndView goTitleSet(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/partyparam/title");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param request
     * @return ModelAndView
     * @Title: goPostLevelSet
     * @Description:  职务级别设置
     */
    @RequestMapping("/postlevelSet")
    @RequiresPermissions("/app/core/partyparam/postlevelSet")
    public ModelAndView goPostLevelSet(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/partyparam/postlevel");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param request
     * @return ModelAndView
     * @Title: goPostSet
     * @Description:  工作岗位设置
     */
    @RequestMapping("/postSet")
    @RequiresPermissions("/app/core/partyparam/postSet")
    public ModelAndView goPostSet(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/partyparam/post");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param request
     * @return ModelAndView
     * @Title: goDegreeSet
     * @Description:  学位设置
     */
    @RequestMapping("/degreeSet")
    public ModelAndView goDegreeSet(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/partyparam/degree");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param request
     * @return ModelAndView
     * @Title: goEducationSet
     * @Description:  学历设置
     */
    @RequestMapping("/educationSet")
    public ModelAndView goEducationSet(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/partyparam/education");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param request
     * @return ModelAndView
     * @Title: goNativePlaceSet
     * @Description:  籍贯设置
     */
    @RequestMapping("/nativePalceSet")
    @RequiresPermissions("/app/core/partyparam/nativePalceSet")
    public ModelAndView goNativePlaceSet(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/partyparam/nativeplace");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param request
     * @return ModelAndView
     * @Title: goStUnitTypeSet
     * @Description:  从学单位类别设置
     */
    @RequestMapping("/stunittypeSet")
    @RequiresPermissions("/app/core/partyparam/stunittypeSet")
    public ModelAndView goStUnitTypeSet(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/partyparam/stunittype");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param request
     * @return ModelAndView
     * @Title: goUnitSuboSet
     * @Description:  单位隶属设置
     */
    @RequestMapping("/unitSuboSet")
    @RequiresPermissions("/app/core/partyparam/unitSuboSet")
    public ModelAndView goUnitSuboSet(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/partyparam/unitsubo");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param request
     * @return ModelAndView
     * @Title: goPartySuboSet
     * @Description:  党组织隶属关系设置
     */
    @RequestMapping("/partysuboSet")
    @RequiresPermissions("/app/core/partyparam/partysuboSet")
    public ModelAndView goPartySuboSet(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/partyparam/partysubo");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param request
     * @return ModelAndView
     * @Title: goWorkTypeSet
     * @Description:  党组织工作类型设置
     */
    @RequestMapping("/workTypeSet")
    @RequiresPermissions("/app/core/partyparam/workTypeSet")
    public ModelAndView goWorkTypeSet(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/partyparam/worktype");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param request
     * @return ModelAndView
     * @Title: goUnitTypeSet
     * @Description:  单位类别设置
     */
    @RequestMapping("/unitTypeSet")
    @RequiresPermissions("/app/core/partyparam/unitTypeSet")
    public ModelAndView goUnitTypeSet(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/partyparam/unittype");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param request
     * @return ModelAndView
     * @Title: goPartyTypeSet
     * @Description:  组织类别设置
     */
    @RequestMapping("/partyTypeSet")
    @RequiresPermissions("/app/core/partyparam/partyTypeSet")
    public ModelAndView goPartyTypeSet(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/partyparam/partytype");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }
}
