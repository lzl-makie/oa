package com.core136.controller.partyrelation;

import com.core136.bean.account.Account;
import com.core136.bean.partyrelation.*;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.partyrelation.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

@RestController
@RequestMapping("/set/partyrelationset")
public class RouteSetPartyRelationController {
    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private PartyRelOutService partyRelOutService;

    @Autowired
    public void setPartyRelOutService(PartyRelOutService partyRelOutService) {
        this.partyRelOutService = partyRelOutService;
    }

    private PartyRelBetweenService partyRelBetweenService;

    @Autowired
    public void setPartyRelBetweenService(PartyRelBetweenService partyRelBetweenService) {
        this.partyRelBetweenService = partyRelBetweenService;
    }

    private PartyIntroduceService partyIntroduceService;

    @Autowired
    public void setPartyIntroduceService(PartyIntroduceService partyIntroduceService) {
        this.partyIntroduceService = partyIntroduceService;
    }

    private PartyRelInService partyRelInService;

    @Autowired
    public void setPartyRelInService(PartyRelInService partyRelInService) {
        this.partyRelInService = partyRelInService;
    }

    private PartyRelAllOutService partyRelAllOutService;

    @Autowired
    public void setPartyRelAllOutService(PartyRelAllOutService partyRelAllOutService) {
        this.partyRelAllOutService = partyRelAllOutService;
    }

    private PartyOutFlowService partyOutFlowService;

    @Autowired
    public void setPartyOutFlowService(PartyOutFlowService partyOutFlowService) {
        this.partyOutFlowService = partyOutFlowService;
    }

    /**
    
     * @param partyOutFlow
     * @return RetDataBean
     * @Title: setPartyOutFlow
     * @Description:  添加党员流出记录
     */
    @RequestMapping(value = "/setPartyOutFlow", method = RequestMethod.POST)
    public RetDataBean setPartyOutFlow( PartyOutFlow partyOutFlow) {
        Account account = accountService.getRedisAUserInfoToAccount();
        partyOutFlow.setRecordId(SysTools.getGUID());
        partyOutFlow.setCreateUser(account.getAccountId());
        partyOutFlow.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
        partyOutFlow.setOrgId(account.getOrgId());
        return partyOutFlowService.setPartyOutFlow(partyOutFlow);
    }

    /**
    
     * @param partyOutFlow
     * @return RetDataBean
     * @Title: deletePartyOutFlow
     * @Description:  删除党员流出记录
     */
    @RequestMapping(value = "/deletePartyOutFlow", method = RequestMethod.POST)
    public RetDataBean deletePartyOutFlow( PartyOutFlow partyOutFlow) {
        try {
            if (StringUtils.isBlank(partyOutFlow.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyOutFlow.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyOutFlowService.deletePartyOutFlow(partyOutFlow));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyOutFlow
     * @return RetDataBean
     * @Title: updatePartyOutFlow
     * @Description:  更新党员流出记录
     */
    @RequestMapping(value = "/updatePartyOutFlow", method = RequestMethod.POST)
    public RetDataBean updatePartyOutFlow( PartyOutFlow partyOutFlow) {
        try {
            if (StringUtils.isBlank(partyOutFlow.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyOutFlow.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyOutFlow.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyOutFlowService.updatePartyOutFlow(example, partyOutFlow));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
    
     * @param partyRelAllOut
     * @return RetDataBean
     * @Title: setRelAllToOutOtherOrg
     * @Description:  整建制转出
     */
    @RequestMapping(value = "/setRelAllToOutOtherOrg", method = RequestMethod.POST)
    public RetDataBean setRelAllToOutOtherOrg( PartyRelAllOut partyRelAllOut) {
        Account account = accountService.getRedisAUserInfoToAccount();
        partyRelAllOut.setRecordId(SysTools.getGUID());
        partyRelAllOut.setCreateUser(account.getAccountId());
        partyRelAllOut.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
        partyRelAllOut.setOrgId(account.getOrgId());
        return partyRelAllOutService.setRelAllToOutOtherOrg(partyRelAllOut);

    }

    /**
    
     * @param partyRelIn
     * @return RetDataBean
     * @Title: setRelIn
     * @Description:  创建关转入记录
     */
    @RequestMapping(value = "/setRelIn", method = RequestMethod.POST)
    public RetDataBean setRelIn( PartyRelIn partyRelIn) {
        Account account = accountService.getRedisAUserInfoToAccount();
        partyRelIn.setRecordId(SysTools.getGUID());
        partyRelIn.setCreateUser(account.getAccountId());
        partyRelIn.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
        partyRelIn.setOrgId(account.getOrgId());
        return partyRelInService.setRelIn(partyRelIn);

    }

    /**
    
     * @param partyRelIn
     * @return RetDataBean
     * @Title: deletePartyRelIn
     * @Description:  删除关系转入记录
     */
    @RequestMapping(value = "/deletePartyRelIn", method = RequestMethod.POST)
    public RetDataBean deletePartyRelIn( PartyRelIn partyRelIn) {
        try {
            if (StringUtils.isBlank(partyRelIn.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyRelIn.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyRelInService.deletePartyRelIn(partyRelIn));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyRelIn
     * @return RetDataBean
     * @Title: updatePartyRelIn
     * @Description:  更新关系转入记录
     */
    @RequestMapping(value = "/updatePartyRelIn", method = RequestMethod.POST)
    public RetDataBean updatePartyRelIn( PartyRelIn partyRelIn) {
        try {
            if (StringUtils.isBlank(partyRelIn.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyRelIn.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyRelIn.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyRelInService.updatePartyRelIn(example, partyRelIn));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyIntroduce
     * @return RetDataBean
     * @Title: insertPartyIntroduce
     * @Description:  创建介绍介记录
     */
    @RequestMapping(value = "/insertPartyIntroduce", method = RequestMethod.POST)
    public RetDataBean insertPartyIntroduce( PartyIntroduce partyIntroduce) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyIntroduce.setRecordId(SysTools.getGUID());
            partyIntroduce.setCreateUser(account.getAccountId());
            partyIntroduce.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyIntroduce.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyIntroduceService.insertPartyIntroduce(partyIntroduce));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyIntroduce
     * @return RetDataBean
     * @Title: deletePartyIntroduce
     * @Description:  删除介绍信记录
     */
    @RequestMapping(value = "/deletePartyIntroduce", method = RequestMethod.POST)
    public RetDataBean deletePartyIntroduce( PartyIntroduce partyIntroduce) {
        try {
            if (StringUtils.isBlank(partyIntroduce.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyIntroduce.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyIntroduceService.deletePartyIntroduce(partyIntroduce));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyIntroduce
     * @return RetDataBean
     * @Title: updatePartyIntroduce
     * @Description:  更新介绍信记录
     */
    @RequestMapping(value = "/updatePartyIntroduce", method = RequestMethod.POST)
    public RetDataBean updatePartyIntroduce( PartyIntroduce partyIntroduce) {
        try {
            if (StringUtils.isBlank(partyIntroduce.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyIntroduce.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyIntroduce.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyIntroduceService.updatePartyIntroduce(example, partyIntroduce));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyRelBetween
     * @return RetDataBean
     * @Title: setRelMemberToOtherOrg
     * @Description:  党组织调转记录
     */
    @RequestMapping(value = "/setRelMemberToOtherOrg", method = RequestMethod.POST)
    public RetDataBean setRelMemberToOtherOrg( PartyRelBetween partyRelBetween) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyRelBetween.setRecordId(SysTools.getGUID());
            partyRelBetween.setCreateUser(account.getAccountId());
            partyRelBetween.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyRelBetween.setOrgId(account.getOrgId());
            return partyRelBetweenService.setRelMemberToOtherOrg(partyRelBetween);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyRelBetween
     * @return RetDataBean
     * @Title: deletePartyRelBetween
     * @Description:  删除党组织调转记录
     */
    @RequestMapping(value = "/deletePartyRelBetween", method = RequestMethod.POST)
    public RetDataBean deletePartyRelBetween( PartyRelBetween partyRelBetween) {
        try {
            if (StringUtils.isBlank(partyRelBetween.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyRelBetween.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyRelBetweenService.deletePartyRelBetween(partyRelBetween));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyRelBetween
     * @return RetDataBean
     * @Title: updatePartyRelBetween
     * @Description:  删除党组织调转记录
     */
    @RequestMapping(value = "/updatePartyRelBetween", method = RequestMethod.POST)
    public RetDataBean updatePartyRelBetween( PartyRelBetween partyRelBetween) {
        try {
            if (StringUtils.isBlank(partyRelBetween.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyRelBetween.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyRelBetween.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyRelBetweenService.updatePartyRelBetween(example, partyRelBetween));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyRelOut
     * @return RetDataBean
     * @Title: setRelMemberToOutOtherOrg
     * @Description:  添加党组织关系转出记录
     */
    @RequestMapping(value = "/setRelMemberToOutOtherOrg", method = RequestMethod.POST)
    public RetDataBean setRelMemberToOutOtherOrg( PartyRelOut partyRelOut) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyRelOut.setRecordId(SysTools.getGUID());
            partyRelOut.setCreateUser(account.getAccountId());
            partyRelOut.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyRelOut.setOrgId(account.getOrgId());
            return partyRelOutService.setRelMemberToOutOtherOrg(partyRelOut);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }
}
