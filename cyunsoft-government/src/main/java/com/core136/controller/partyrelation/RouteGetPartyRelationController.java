package com.core136.controller.partyrelation;

import com.core136.bean.account.Account;
import com.core136.bean.partyrelation.*;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.service.account.AccountService;
import com.core136.service.partyrelation.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/ret/partyrelationget")
public class RouteGetPartyRelationController {
    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private PartyRelOutService partyRelOutService;

    @Autowired
    public void setPartyRelOutService(PartyRelOutService partyRelOutService) {
        this.partyRelOutService = partyRelOutService;
    }

    private PartyRelBetweenService partyRelBetweenService;

    @Autowired
    public void setPartyRelBetweenService(PartyRelBetweenService partyRelBetweenService) {
        this.partyRelBetweenService = partyRelBetweenService;
    }

    private PartyIntroduceService partyIntroduceService;

    @Autowired
    public void setPartyIntroduceService(PartyIntroduceService partyIntroduceService) {
        this.partyIntroduceService = partyIntroduceService;
    }

    private PartyRelInService partyRelInService;

    @Autowired
    public void setPartyRelInService(PartyRelInService partyRelInService) {
        this.partyRelInService = partyRelInService;
    }

    private PartyRelAllOutService partyRelAllOutService;

    @Autowired
    public void setPartyRelAllOutService(PartyRelAllOutService partyRelAllOutService) {
        this.partyRelAllOutService = partyRelAllOutService;
    }

    private PartyOutFlowService partyOutFlowService;

    @Autowired
    public void setPartyOutFlowService(PartyOutFlowService partyOutFlowService) {
        this.partyOutFlowService = partyOutFlowService;
    }

    /**
    
     * @param pageParam
     * @param partyOrgId
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getPartyOutFlowList
     * @Description:  党员流出管理
     */
    @RequestMapping(value = "/getPartyOutFlowList", method = RequestMethod.POST)
    public RetDataBean getPartyOutFlowList( PageParam pageParam, String partyOrgId, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("o.out_time");
            } else {
                pageParam.setSort("o." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyOutFlowService.getPartyOutFlowList(pageParam, partyOrgId, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
    
     * @param partyOutFlow
     * @return RetDataBean
     * @Title: getPartyOutFlowById
     * @Description:  党员流出详情
     */
    @RequestMapping(value = "/getPartyOutFlowById", method = RequestMethod.POST)
    public RetDataBean getPartyOutFlowById( PartyOutFlow partyOutFlow) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyOutFlow.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyOutFlowService.selectOnePartyOutFlow(partyOutFlow));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param pageParam
     * @param isInSys
     * @param beginTime
     * @param endTime
     * @param oldPartyOrgId
     * @param outPartyOrgId
     * @return RetDataBean
     * @Title: getRelAllOutList
     * @Description:  获取整建制关系转出记录
     */
    @RequestMapping(value = "/getRelAllOutList", method = RequestMethod.POST)
    public RetDataBean getRelAllOutList( PageParam pageParam, String isInSys, String beginTime, String endTime, String oldPartyOrgId, String outPartyOrgId) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("o.create_time");
            } else {
                pageParam.setSort("o." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyRelAllOutService.getRelAllOutList(pageParam, isInSys, beginTime, endTime, oldPartyOrgId, outPartyOrgId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
    
     * @param pageParam
     * @param inType
     * @param beginTime
     * @param endTime
     * @param partyOrgId
     * @return RetDataBean
     * @Title: getRelInList
     * @Description:  获取关系转入列表
     */
    @RequestMapping(value = "/getRelInList", method = RequestMethod.POST)
    public RetDataBean getRelInList( PageParam pageParam, String inType, String beginTime, String endTime, String partyOrgId) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("o.in_time");
            } else {
                pageParam.setSort("o." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyRelInService.getRelInList(pageParam, inType, beginTime, endTime, partyOrgId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyRelIn
     * @return RetDataBean
     * @Title: getPartyRelInById
     * @Description:  获取关系转入详情
     */
    @RequestMapping(value = "/getPartyRelInById", method = RequestMethod.POST)
    public RetDataBean getPartyRelInById( PartyRelIn partyRelIn) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyRelIn.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyRelInService.selectOnePartyRelIn(partyRelIn));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @param memberId
     * @return RetDataBean
     * @Title: getIntroduceList
     * @Description:  获取介绍信列表
     */
    @RequestMapping(value = "/getIntroduceList", method = RequestMethod.POST)
    public RetDataBean getIntroduceList( PageParam pageParam, String beginTime, String endTime, String memberId) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("j.create_time");
            } else {
                pageParam.setSort("j." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyIntroduceService.getIntroduceList(pageParam, beginTime, endTime, memberId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
    
     * @param partyIntroduce
     * @return RetDataBean
     * @Title: getPartyIntroduceById
     * @Description:  获取介绍信详情
     */
    @RequestMapping(value = "/getPartyIntroduceById", method = RequestMethod.POST)
    public RetDataBean getPartyIntroduceById( PartyIntroduce partyIntroduce) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyIntroduce.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyIntroduceService.selectOnePartyIntroduce(partyIntroduce));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param pageParam
     * @param isInSys
     * @param outType
     * @param beginTime
     * @param endTime
     * @param oldPartyOrgId
     * @param outPartyOrgId
     * @return RetDataBean
     * @Title: getRelOutList
     * @Description:  获取党员党组织关系转出记录
     */
    @RequestMapping(value = "/getRelOutList", method = RequestMethod.POST)
    public RetDataBean getRelOutList( PageParam pageParam, String isInSys, String outType, String beginTime, String endTime, String oldPartyOrgId, String outPartyOrgId) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("o.create_time");
            } else {
                pageParam.setSort("o." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyRelOutService.getRelOutList(pageParam, isInSys, outType, beginTime, endTime, oldPartyOrgId, outPartyOrgId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyRelOut
     * @return RetDataBean
     * @Title: getPartyRelOutById
     * @Description:  获取党组织关系转出记录详情
     */
    @RequestMapping(value = "/getPartyRelOutById", method = RequestMethod.POST)
    public RetDataBean getPartyRelOutById( PartyRelOut partyRelOut) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyRelOut.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyRelOutService.selectOnePartyRelOut(partyRelOut));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param pageParam
     * @param relType
     * @param beginTime
     * @param endTime
     * @param oldPartyOrgId
     * @param relPartyOrgId
     * @return RetDataBean
     * @Title: getRelBetweenList
     * @Description:  获取支部间调转记录列表
     */
    @RequestMapping(value = "/getRelBetweenList", method = RequestMethod.POST)
    public RetDataBean getRelBetweenList( PageParam pageParam, String relType, String beginTime, String endTime, String oldPartyOrgId, String relPartyOrgId) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("o.create_time");
            } else {
                pageParam.setSort("o." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("as");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyRelBetweenService.getRelBetweenList(pageParam, relType, beginTime, endTime, oldPartyOrgId, relPartyOrgId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
    
     * @param partyRelBetween
     * @return RetDataBean
     * @Title: getPartyRelBetweenById
     * @Description:  支部间调转
     */
    @RequestMapping(value = "/getPartyRelBetweenById", method = RequestMethod.POST)
    public RetDataBean getPartyRelBetweenById( PartyRelBetween partyRelBetween) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyRelBetween.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyRelBetweenService.selectOnePartyRelBetween(partyRelBetween));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

}
