package com.core136.controller.partyunion;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping("/app/core/partyunion")
public class PagePartyUnionController {

    /**
     * 农民工管理
     *
     * @param view
     * @return
     */
    @RequestMapping("/workers")
    @RequiresPermissions("/app/core/partyunion/workers")
    public ModelAndView goUnionWorkers(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partyunion/workers/manage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/partyunion/workers/index");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 农民工详情
     *
     * @return
     */
    @RequestMapping("/workersdetails")
    @RequiresPermissions("/app/core/partyunion/workersdetails")
    public ModelAndView goWorkersDetails() {
        try {
            return new ModelAndView("app/core/partyunion/workers/details");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 就业培训详情
     *
     * @return
     */
    @RequestMapping("/trainingdetails")
    @RequiresPermissions("/app/core/partyunion/trainingdetails")
    public ModelAndView goTrainingDetails() {
        try {
            return new ModelAndView("app/core/partyunion/training/details");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 就业培训
     *
     * @param view
     * @return
     */
    @RequestMapping("/training")
    @RequiresPermissions("/app/core/partyunion/training")
    public ModelAndView goUnionTraining(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partyunion/training/manage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/partyunion/training/index");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 困难帮护管理
     *
     * @param view
     * @return
     */
    @RequestMapping("/help")
    @RequiresPermissions("/app/core/partyunion/help")
    public ModelAndView goUnionHelp(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partyunion/help/manage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/partyunion/help/index");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 困难帮护详情
     *
     * @return
     */
    @RequestMapping("/helpdetails")
    @RequiresPermissions("/app/core/partyunion/helpdetails")
    public ModelAndView goHelpDetails() {
        try {
            return new ModelAndView("app/core/partyunion/help/details");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 法律法规分类
     *
     * @return
     */
    @RequestMapping("/instsort")
    @RequiresPermissions("/app/core/partyunion/instsort")
    public ModelAndView goInstSort() {
        try {
            return new ModelAndView("app/core/partyunion/inst/instsort");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 规章制度
     *
     * @param view
     * @return
     */
    @RequestMapping("/inst")
    @RequiresPermissions("/app/core/partyunion/inst")
    public ModelAndView goUnionInst(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partyunion/inst/manage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/partyunion/inst/index");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 规章制度详情
     *
     * @return
     */
    @RequestMapping("/instdetails")
    @RequiresPermissions("/app/core/partyunion/instdetails")
    public ModelAndView goInstDetails() {
        try {
            return new ModelAndView("app/core/partyunion/inst/details");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


    /**
     * 维权调解
     *
     * @param view
     * @return
     */
    @RequestMapping("/mediate")
    @RequiresPermissions("/app/core/partyunion/mediate")
    public ModelAndView goUnionMediate(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partyunion/mediate/manage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/partyunion/mediate/index");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 维权调解详情
     *
     * @return
     */
    @RequestMapping("/mediatedetails")
    @RequiresPermissions("/app/core/partyunion/mediatedetails")
    public ModelAndView goMediateDetails() {
        try {
            return new ModelAndView("app/core/partyunion/mediate/details");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 投诉举报详情
     *
     * @return
     */
    @RequestMapping("/tipdetails")
    @RequiresPermissions("/app/core/partyunion/tipdetails")
    public ModelAndView goTipDetails() {
        try {
            return new ModelAndView("app/core/partyunion/tip/details");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 投诉举报
     *
     * @param view
     * @return
     */
    @RequestMapping("/tip")
    @RequiresPermissions("/app/core/partyunion/tip")
    public ModelAndView goUnionTip(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partyunion/tip/manage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/partyunion/tip/index");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 劳模管理
     *
     * @param view
     * @return
     */
    @RequestMapping("/model")
    @RequiresPermissions("/app/core/partyunion/model")
    public ModelAndView goUnionModel(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partyunion/model/manage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/partyunion/model/index");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 劳模管理详情
     *
     * @return
     */
    @RequestMapping("/modeldetails")
    @RequiresPermissions("/app/core/partyunion/modeldetails")
    public ModelAndView goModelDetails() {
        try {
            return new ModelAndView("app/core/partyunion/model/details");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


    /**
     * 三八红旗手管理
     *
     * @param view
     * @return
     */
    @RequestMapping("/woman")
    @RequiresPermissions("/app/core/partyunion/woman")
    public ModelAndView goUnionWoman(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partyunion/woman/manage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/partyunion/woman/index");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 三八红旗手详情
     *
     * @return
     */
    @RequestMapping("/womandetails")
    @RequiresPermissions("/app/core/partyunion/womandetails")
    public ModelAndView goWomanDetails() {
        try {
            return new ModelAndView("app/core/partyunion/woman/details");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 职工信息详情
     *
     * @return
     */
    @RequestMapping("/msgboxdetails")
    @RequiresPermissions("/app/core/partyunion/msgboxdetails")
    public ModelAndView goMsgboxDetails() {
        try {
            return new ModelAndView("app/core/partyunion/msgbox/details");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


    /**
     * 职工信息反馈
     *
     * @return
     */
    @RequestMapping("/msgbox")
    @RequiresPermissions("/app/core/partyunion/msgbox")
    public ModelAndView goMsgbox() {
        try {
            return new ModelAndView("app/core/partyunion/msgbox/index");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 职工信箱意见回复
     *
     * @return
     */
    @RequestMapping("/managemsgbox")
    @RequiresPermissions("/app/core/partyunion/managemsgbox")
    public ModelAndView goManageMsgbox() {
        try {
            return new ModelAndView("app/core/partyunion/msgbox/manage");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


    /**
     * 职场故事
     *
     * @param view
     * @return
     */
    @RequestMapping("/story")
    @RequiresPermissions("/app/core/partyunion/story")
    public ModelAndView goUnionStory(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partyunion/story/manage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/partyunion/story/index");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 职场故事详情
     *
     * @return
     */
    @RequestMapping("/storydetails")
    @RequiresPermissions("/app/core/partyunion/storydetails")
    public ModelAndView goStroyDetails() {
        try {
            return new ModelAndView("app/core/partyunion/story/details");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 职场英精
     *
     * @param view
     * @return
     */
    @RequestMapping("/essence")
    @RequiresPermissions("/app/core/partyunion/essence")
    public ModelAndView goUnionEssence(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partyunion/essence/manage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/partyunion/essence/index");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 职场英精详情
     *
     * @return
     */
    @RequestMapping("/essencedetails")
    @RequiresPermissions("/app/core/partyunion/essencedetails")
    public ModelAndView goEssenceDetails() {
        try {
            return new ModelAndView("app/core/partyunion/essence/details");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 工会活动
     *
     * @param view
     * @return
     */
    @RequestMapping("/activity")
    @RequiresPermissions("/app/core/partyunion/activity")
    public ModelAndView goUnionActivity(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partyunion/activity/manage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/partyunion/activity/index");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 工会活动详情
     *
     * @return
     */
    @RequestMapping("/activitydetails")
    @RequiresPermissions("/app/core/partyunion/activitydetails")
    public ModelAndView goActiovityDetails() {
        try {
            return new ModelAndView("app/core/partyunion/activity/details");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 工会管理
     *
     * @return
     */
    @RequestMapping("/setorg")
    @RequiresPermissions("/app/core/partyunion/setorg")
    public ModelAndView goUnionSetOrg(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partyunion/org/setorg");
            } else {
                if (view.equals("info")) {
                    mv = new ModelAndView("app/core/partyunion/org/setorginfo");
                } else if (view.equals("list")) {
                    mv = new ModelAndView("app/core/partyunion/org/setorglist");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 工会机构管理
     *
     * @return
     */
    @RequestMapping("/setdept")
    @RequiresPermissions("/app/core/partyunion/setdept")
    public ModelAndView goUnionSetDept() {
        try {
            return new ModelAndView("app/core/partyunion/org/setdept");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 工会职务
     *
     * @return
     */
    @RequestMapping("/setlevel")
    @RequiresPermissions("/app/core/partyunion/setlevel")
    public ModelAndView goUnionSetLevel() {
        try {
            return new ModelAndView("app/core/partyunion/org/setlevel");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 工会成员管理
     *
     * @return
     */
    @RequestMapping("/setmember")
    @RequiresPermissions("/app/core/partyunion/setmember")
    public ModelAndView goUnionSetMember() {
        try {
            return new ModelAndView("app/core/partyunion/org/setmember");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 工会成员详情
     *
     * @return
     */
    @RequestMapping("/setmemberdetails")
    @RequiresPermissions("/app/core/partyunion/setmemberdetails")
    public ModelAndView goSetMemberDetails() {
        try {
            return new ModelAndView("app/core/partyunion/org/setmemberdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

}
