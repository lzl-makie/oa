package com.core136.controller.partymember;

import com.core136.bean.account.Account;
import com.core136.bean.account.UserInfo;
import com.core136.bean.partymember.*;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partyparam.PartyWorkRecordService;
import com.core136.service.account.AccountService;
import com.core136.service.account.UserInfoService;
import com.core136.service.partymember.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;


@RestController
@RequestMapping("/ret/partymemberget")
public class RouteGetPartyMemberController {
    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private UserInfoService userInfoService;

    @Autowired
    public void setUserInfoService(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }

    private PartyMemberService partyMemberService;

    @Autowired
    public void setPartyMemberService(PartyMemberService partyMemberService) {
        this.partyMemberService = partyMemberService;
    }

    private PartyWorkRecordService partyWorkRecordService;

    @Autowired
    public void setPartyWorkRecordService(PartyWorkRecordService partyWorkRecordService) {
        this.partyWorkRecordService = partyWorkRecordService;
    }

    private PartyMemberOutService partyMemberOutService;

    @Autowired
    public void setPartyMemberOutService(PartyMemberOutService partyMemberOutService) {
        this.partyMemberOutService = partyMemberOutService;
    }

    private PartyMemberDeathService partyMemberDeathService;

    @Autowired
    public void setPartyMemberDeathService(PartyMemberDeathService partyMemberDeathService) {
        this.partyMemberDeathService = partyMemberDeathService;
    }

    private PartyPostRecordService partyPostRecordService;

    @Autowired
    public void setPartyPostRecordService(PartyPostRecordService partyPostRecordService) {
        this.partyPostRecordService = partyPostRecordService;
    }

    private PartyTrainRecordService partyTrainRecordService;

    @Autowired
    public void setPartyTrainRecordService(PartyTrainRecordService partyTrainRecordService) {
        this.partyTrainRecordService = partyTrainRecordService;
    }

    private PartyLifeRecordService partyLifeRecordService;

    @Autowired
    public void setPartyLifeRecordService(PartyLifeRecordService partyLifeRecordService) {
        this.partyLifeRecordService = partyLifeRecordService;
    }

    private PartyAppraisalRecordService partyAppraisalRecordService;

    @Autowired
    public void setPartyAppraisalRecordService(PartyAppraisalRecordService partyAppraisalRecordService) {
        this.partyAppraisalRecordService = partyAppraisalRecordService;
    }

    private PartyAdmRecordService partyAdmRecordService;

    @Autowired
    public void setPartyAdmRecordService(PartyAdmRecordService partyAdmRecordService) {
        this.partyAdmRecordService = partyAdmRecordService;
    }

    private PartyPunishRecordService partyPunishRecordService;

    @Autowired
    public void setPartyPunishRecordService(PartyPunishRecordService partyPunishRecordService) {
        this.partyPunishRecordService = partyPunishRecordService;
    }

    private PartyRewardRecordService partyRewardRecordService;

    @Autowired
    public void setPartyRewardRecordService(PartyRewardRecordService partyRewardRecordService) {
        this.partyRewardRecordService = partyRewardRecordService;
    }

    private PartyFamilyRecordService partyFamilyRecordService;

    @Autowired
    public void setPartyFamilyRecordService(PartyFamilyRecordService partyFamilyRecordService) {
        this.partyFamilyRecordService = partyFamilyRecordService;
    }

    private PartyMemberJoinService partyMemberJoinService;

    @Autowired
    public void setPartyMemberJoinService(PartyMemberJoinService partyMemberJoinService) {
        this.partyMemberJoinService = partyMemberJoinService;
    }

    private PartyMemberFormalService partyMemberFormalService;

    @Autowired
    public void setPartyMemberFormalService(PartyMemberFormalService partyMemberFormalService) {
        this.partyMemberFormalService = partyMemberFormalService;
    }

    private PartyMemberPrepareService partyMemberPrepareService;

    @Autowired
    public void setPartyMemberPrepareService(PartyMemberPrepareService partyMemberPrepareService) {
        this.partyMemberPrepareService = partyMemberPrepareService;
    }

    private PartyMemberTargetService partyMemberTargetService;

    @Autowired
    public void setPartyMemberTargetService(PartyMemberTargetService partyMemberTargetService) {
        this.partyMemberTargetService = partyMemberTargetService;
    }

    private PartyMemberActivistsService partyMemberActivistsService;

    @Autowired
    public void setPartyMemberActivistsService(PartyMemberActivistsService partyMemberActivistsService) {
        this.partyMemberActivistsService = partyMemberActivistsService;
    }

    private PartyTargetTrainService partyTargetTrainService;

    @Autowired
    public void setPartyTargetTrainService(PartyTargetTrainService partyTargetTrainService) {
        this.partyTargetTrainService = partyTargetTrainService;
    }

    private PartyActivistsTrainService partyActivistsTrainService;

    @Autowired
    public void setPartyActivistsTrainService(PartyActivistsTrainService partyActivistsTrainService) {
        this.partyActivistsTrainService = partyActivistsTrainService;
    }

    private PartyVolunteerService partyVolunteerService;

    @Autowired
    public void setPartyVolunteerService(PartyVolunteerService partyVolunteerService) {
        this.partyVolunteerService = partyVolunteerService;
    }

    private PartyArmyService partyArmyService;

    @Autowired
    public void setPartyArmyService(PartyArmyService partyArmyService) {
        this.partyArmyService = partyArmyService;
    }

    private PartyMemberDifficultyService partyMemberDifficultyService;

    @Autowired
    public void setPartyMemberDifficultyService(PartyMemberDifficultyService partyMemberDifficultyService) {
        this.partyMemberDifficultyService = partyMemberDifficultyService;
    }

    private PartyMemberPointsService partyMemberPointsService;

    @Autowired
    public void setPartyMemberPointsService(PartyMemberPointsService partyMemberPointsService) {
        this.partyMemberPointsService = partyMemberPointsService;
    }

    private PartyMemberLoseService partyMemberLoseService;

    @Autowired
    public void setPartyMemberLoseService(PartyMemberLoseService partyMemberLoseService) {
        this.partyMemberLoseService = partyMemberLoseService;
    }

    /**
     * @param pageParam
     * @param loseType
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getMemberLoseList
     * @Description:  获取失联党员信息列表
     */
    @RequestMapping(value = "/getMemberLoseList", method = RequestMethod.POST)
    public RetDataBean getMemberLoseList(PageParam pageParam, String loseType, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("o.lose_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyMemberLoseService.getMemberLoseList(pageParam, loseType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyMemberLose
     * @return RetDataBean
     * @Title: getPartyMemberLoseById
     * @Description:  获取失联党员详情
     */
    @RequestMapping(value = "/getPartyMemberLoseById", method = RequestMethod.POST)
    public RetDataBean getPartyMemberLoseById(PartyMemberLose partyMemberLose) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyMemberLose.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyMemberLoseService.selectOnePartyMemberLose(partyMemberLose));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getMyPointsInfo
     * @Description:  获取演员员积分总和信息
     */
    @RequestMapping(value = "/getMyPointsInfo", method = RequestMethod.POST)
    public RetDataBean getMyPointsInfo() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyMemberPointsService.getMyPointsInfo(account));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @return RetDataBean
     * @Title: getMyMemberInfoByMemberId
     * @Description:  获取个人党员信息
     */
    @RequestMapping(value = "/getMyMemberInfoByMemberId", method = RequestMethod.POST)
    public RetDataBean getMyMemberInfoByMemberId() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyMemberService.getMyMemberInfoByMemberId(account.getOrgId(), account.getMemberId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyMemberPoints
     * @return RetDataBean
     * @Title: getPartyOrgPointsById
     * @Description:  获取党员积分详情
     */
    @RequestMapping(value = "/getPartyMemberPointsById", method = RequestMethod.POST)
    public RetDataBean getPartyOrgPointsById(PartyMemberPoints partyMemberPoints) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyMemberPoints.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyMemberPointsService.selectOnePartyMemberPoints(partyMemberPoints));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @return RetDataBean
     * @Title: getMyMemberPointsList
     * @Description:  获取党员个人历史积分明细
     */
    @RequestMapping(value = "/getMyMemberPointsList", method = RequestMethod.POST)
    public RetDataBean getMyMemberPointsList(PageParam pageParam) {
        String pointsYear = SysTools.getTime("yyyy");
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("o.points_year");
            } else {
                pageParam.setSort("o." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyMemberPointsService.getMemberPointsList(pageParam, pointsYear, account.getMemberId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param pointsYear
     * @param memberId
     * @return RetDataBean
     * @Title: getMemberPointsList
     * @Description: 获取党员积分列表
     */
    @RequestMapping(value = "/getMemberPointsList", method = RequestMethod.POST)
    public RetDataBean getMemberPointsList(PageParam pageParam, String pointsYear, String memberId) {
        if (StringUtils.isBlank(pointsYear)) {
            pointsYear = SysTools.getTime("yyyy");
        }
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("o.points_year");
            } else {
                pageParam.setSort("o." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyMemberPointsService.getMemberPointsList(pageParam, pointsYear, memberId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取党员汇总积分列表
     *
     * @param pageParam
     * @param pointsYear
     * @return
     */
    @RequestMapping(value = "/getMemberPointsTotalList", method = RequestMethod.POST)
    public RetDataBean getMemberPointsTotalList(PageParam pageParam, String pointsYear, String memberId) {
        if (StringUtils.isBlank(pointsYear)) {
            pointsYear = SysTools.getTime("yyyy");
        }
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("o.points_year");
            } else {
                pageParam.setSort("o." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyMemberPointsService.getMemberPointsTotalList(pageParam, pointsYear, memberId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getMyMemberFamilyList
     * @Description:  获取个人家庭成员记录
     */
    @RequestMapping(value = "/getMyMemberFamilyList", method = RequestMethod.POST)
    public RetDataBean getMyMemberFamilyList() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(account.getMemberId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyFamilyRecordService.getMyMemberFamilyList(account.getOrgId(), account.getMemberId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getMyMemberPunishList
     * @Description:  获取个人惩戒记录列表
     */
    @RequestMapping(value = "/getMyMemberPunishList", method = RequestMethod.POST)
    public RetDataBean getMyMemberPunishList() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(account.getMemberId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyPunishRecordService.getMyMemberPunishList(account.getOrgId(), account.getMemberId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getMyMemberRewardList
     * @Description:  获取个人奖励列表
     */
    @RequestMapping(value = "/getMyMemberRewardList", method = RequestMethod.POST)
    public RetDataBean getMyMemberRewardList() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(account.getMemberId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyRewardRecordService.getMyMemberRewardList(account.getOrgId(), account.getMemberId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getMyAdmRecordList
     * @Description:  获取个人行政任职记录列表
     */
    @RequestMapping(value = "/getMyAdmRecordList", method = RequestMethod.POST)
    public RetDataBean getMyAdmRecordList() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(account.getMemberId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyAdmRecordService.getMyAdmRecordList(account.getOrgId(), account.getMemberId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getMyPartyLifeList
     * @Description:  获取个人组织生活列表
     */
    @RequestMapping(value = "/getMyPartyLifeList", method = RequestMethod.POST)
    public RetDataBean getMyPartyLifeList() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(account.getMemberId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyLifeRecordService.getMyPartyLifeList(account.getOrgId(), account.getMemberId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getMyAppraisalList
     * @Description:  获取个人民主评议记录
     */
    @RequestMapping(value = "/getMyAppraisalList", method = RequestMethod.POST)
    public RetDataBean getMyAppraisalList() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(account.getMemberId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyAppraisalRecordService.getMyAppraisalList(account.getOrgId(), account.getMemberId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getMyPartyPostList
     * @Description:
     */
    @RequestMapping(value = "/getMyPartyPostList", method = RequestMethod.POST)
    public RetDataBean getMyPartyPostList() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(account.getMemberId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyPostRecordService.getMyPartyPostList(account.getOrgId(), account.getMemberId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getMyMemberTrainList
     * @Description:  获取党员自己的培训记录
     */
    @RequestMapping(value = "/getMyMemberTrainList", method = RequestMethod.POST)
    public RetDataBean getMyMemberTrainList() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(account.getMemberId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyTrainRecordService.getMyMemberTrainList(account.getOrgId(), account.getMemberId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getMyPartyMemberById
     * @Description:  获取个人党信息
     */
    @RequestMapping(value = "/getMyPartyMemberById", method = RequestMethod.POST)
    public RetDataBean getMyPartyMemberById() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(account.getMemberId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            PartyMember partyMember = new PartyMember();
            partyMember.setMemberId(account.getMemberId());
            partyMember.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyMemberService.selectOnePartyMember(partyMember));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param difType
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getMemberDifficultyList
     * @Description:  获取困难党员列表
     */
    @RequestMapping(value = "/getMemberDifficultyList", method = RequestMethod.POST)
    public RetDataBean getMemberDifficultyList(PageParam pageParam, String difType, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("o.create_time");
            } else {
                pageParam.setSort("o." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyMemberDifficultyService.getMemberDifficultyList(pageParam, difType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyMemberDifficulty
     * @return RetDataBean
     * @Title: getPartyMemberDifficultyById
     * @Description:  困难党员详情
     */
    @RequestMapping(value = "/getPartyMemberDifficultyById", method = RequestMethod.POST)
    public RetDataBean getPartyMemberDifficultyById(PartyMemberDifficulty partyMemberDifficulty) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyMemberDifficulty.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyMemberDifficultyService.selectOnePartyMemberDifficulty(partyMemberDifficulty));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param userType
     * @param education
     * @param partyMember
     * @param armyRank
     * @return RetDataBean
     * @Title: getArmyUserList
     * @Description:  获取统战人员列表
     */
    @RequestMapping(value = "/getArmyUserList", method = RequestMethod.POST)
    public RetDataBean getArmyUserList(PageParam pageParam, String userType, String education, String partyMember, String armyRank) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("p.sort_no");
            } else {
                pageParam.setSort("p." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyArmyService.getArmyUserList(pageParam, userType, education, partyMember, armyRank);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param volType
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getVolunteerList
     * @Description:  获取志愿者活动列表
     */
    @RequestMapping(value = "/getVolunteerList", method = RequestMethod.POST)
    public RetDataBean getVolunteerList(PageParam pageParam, String volType, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("sort_no");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyVolunteerService.getVolunteerList(pageParam, volType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @param partyOrgId
     * @param jobStatus
     * @return RetDataBean
     * @Title: getTargetTrainList
     * @Description:  发展对象培训记录列表
     */
    @RequestMapping(value = "/getTargetTrainList", method = RequestMethod.POST)
    public RetDataBean getTargetTrainList(PageParam pageParam, String beginTime, String endTime, String partyOrgId, String jobStatus) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("r.create_time");
            } else {
                pageParam.setSort("r." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyTargetTrainService.getTargetTrainList(pageParam, beginTime, endTime, partyOrgId, jobStatus);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyTargetTrain
     * @return RetDataBean
     * @Title: getPartyTargetTrainById
     * @Description:  获取发展对象培训详情
     */
    @RequestMapping(value = "/getPartyTargetTrainById", method = RequestMethod.POST)
    public RetDataBean getPartyTargetTrainById(PartyTargetTrain partyTargetTrain) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyTargetTrain.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyTargetTrainService.selectOnePartyTargetTrain(partyTargetTrain));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @param partyOrgId
     * @param jobStatus
     * @return RetDataBean
     * @Title: getActivistsTrainList
     * @Description:  getActivistsTrainList
     */
    @RequestMapping(value = "/getActivistsTrainList", method = RequestMethod.POST)
    public RetDataBean getActivistsTrainList(PageParam pageParam, String beginTime, String endTime, String partyOrgId, String jobStatus) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("r.create_time");
            } else {
                pageParam.setSort("r." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyActivistsTrainService.getActivistsTrainList(pageParam, beginTime, endTime, partyOrgId, jobStatus);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyActivistsTrain
     * @return RetDataBean
     * @Title: getPartyActivistsTrainById
     * @Description:  获取积极份子培训记录详情
     */
    @RequestMapping(value = "/getPartyActivistsTrainById", method = RequestMethod.POST)
    public RetDataBean getPartyActivistsTrainById(PartyActivistsTrain partyActivistsTrain) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyActivistsTrain.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyActivistsTrainService.selectOnePartyActivistsTrain(partyActivistsTrain));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyVolunteer
     * @return RetDataBean
     * @Title: getPartyVolunteerById
     * @Description:  志愿者服务记录详情
     */
    @RequestMapping(value = "/getPartyVolunteerById", method = RequestMethod.POST)
    public RetDataBean getPartyVolunteerById(PartyVolunteer partyVolunteer) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyVolunteer.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyVolunteerService.selectOnePartyVolunteer(partyVolunteer));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyArmy
     * @return RetDataBean
     * @Title: getPartyArmyById
     * @Description:  统战记录详情
     */
    @RequestMapping(value = "/getPartyArmyById", method = RequestMethod.POST)
    public RetDataBean getPartyArmyById(PartyArmy partyArmy) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyArmy.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyArmyService.selectOnePartyArmy(partyArmy));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param status
     * @param partyOrgId
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getActivistsRecordList
     * @Description:  获取积极份子审批记录
     */
    @RequestMapping(value = "/getActivistsRecordList", method = RequestMethod.POST)
    public RetDataBean getActivistsRecordList(PageParam pageParam, String status, String partyOrgId, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("r.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyMemberActivistsService.getActivistsRecordList(pageParam, status, partyOrgId, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyMemberTarget
     * @return RetDataBean
     * @Title: getPartyMemberTargetById
     * @Description:  获取发展为积极份子详情
     */
    @RequestMapping(value = "/getPartyMemberTargetById", method = RequestMethod.POST)
    public RetDataBean getPartyMemberTargetById(PartyMemberTarget partyMemberTarget) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyMemberTarget.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyMemberTargetService.selectOnePartyMemberTarget(partyMemberTarget));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param status
     * @param partyOrgId
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getFormalRecordList
     * @Description:  获取党员转正记录
     */
    @RequestMapping(value = "/getFormalRecordList", method = RequestMethod.POST)
    public RetDataBean getFormalRecordList(PageParam pageParam, String status, String partyOrgId, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("r.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyMemberFormalService.getFormalRecordList(pageParam, status, partyOrgId, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param pageParam
     * @param status
     * @param partyOrgId
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getPrepareRecordList
     * @Description:  获取预备党员审批记录
     */
    @RequestMapping(value = "/getPrepareRecordList", method = RequestMethod.POST)
    public RetDataBean getPrepareRecordList(PageParam pageParam, String status, String partyOrgId, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("r.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyMemberPrepareService.getPrepareRecordList(pageParam, status, partyOrgId, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param status
     * @param partyOrgId
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getTargetRecordList
     * @Description:  获取发展对象审批列表
     */
    @RequestMapping(value = "/getTargetRecordList", method = RequestMethod.POST)
    public RetDataBean getTargetRecordList(PageParam pageParam, String status, String partyOrgId, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("r.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyMemberTargetService.getTargetRecordList(pageParam, status, partyOrgId, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param partyOrgId
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getMemberJoinApplyList
     * @Description:  获取入党申请列表
     */
    @RequestMapping(value = "/getMemberJoinApplyList", method = RequestMethod.POST)
    public RetDataBean getMemberJoinApplyList(PageParam pageParam, String statusFlag, String partyOrgId, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("j.join_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyMemberJoinService.getMemberJoinApplyList(pageParam, statusFlag, partyOrgId, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getActivistsMemberIdList
     * @Description:  获取积极份子人员列表
     */
    @RequestMapping(value = "/getActivistsMemberIdList", method = RequestMethod.POST)
    public RetDataBean getActivistsMemberIdList() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyMemberJoinService.getActivistsMemberIdList(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyMemberJoin
     * @return RetDataBean
     * @Title: getPartyMemberJoinById
     * @Description:  获取入党申请详情
     */
    @RequestMapping(value = "/getPartyMemberJoinById", method = RequestMethod.POST)
    public RetDataBean getPartyMemberJoinById(PartyMemberJoin partyMemberJoin) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyMemberJoin.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyMemberJoinService.selectOnePartyMemberJoin(partyMemberJoin));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getActivistsMemberIdList
     * @Description:  获取发展对象人员列表
     */
    @RequestMapping(value = "/getTargetMemberIdList", method = RequestMethod.POST)
    public RetDataBean getTargetMemberIdList() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyMemberJoinService.getTargetMemberIdList(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyPunishRecord
     * @return RetDataBean
     * @Title: getPartyPunishRecordById
     * @Description:  获取党员惩戒记录详情
     */
    @RequestMapping(value = "/getPartyPunishRecordById", method = RequestMethod.POST)
    public RetDataBean getPartyPunishRecordById(PartyPunishRecord partyPunishRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyPunishRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyPunishRecordService.selectOnePartyPunishRecord(partyPunishRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @param memberId
     * @param rewardLevel
     * @return RetDataBean
     * @Title: getMemberRewardList
     * @Description:  获取党员奖励列表
     */
    @RequestMapping(value = "/getMemberRewardList", method = RequestMethod.POST)
    public RetDataBean getMemberRewardList(PageParam pageParam, String beginTime, String endTime, String memberId, String rewardLevel) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("r.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyRewardRecordService.getMemberRewardList(pageParam, beginTime, endTime, memberId, rewardLevel);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyRewardRecord
     * @return RetDataBean
     * @Title: getPartyRewardRecordById
     * @Description:  获取党员奖励记录详情
     */
    @RequestMapping(value = "/getPartyRewardRecordById", method = RequestMethod.POST)
    public RetDataBean getPartyRewardRecordById(PartyRewardRecord partyRewardRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyRewardRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyRewardRecordService.selectOnePartyRewardRecord(partyRewardRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param userName
     * @param memberId
     * @return RetDataBean
     * @Title: getMemberFamilyList
     * @Description:  获取党员家庭成员列表
     */
    @RequestMapping(value = "/getMemberFamilyList", method = RequestMethod.POST)
    public RetDataBean getMemberFamilyList(PageParam pageParam, String userName, String memberId) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("r.sort_no");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyFamilyRecordService.getMemberFamilyList(pageParam, userName, memberId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyFamilyRecord
     * @return RetDataBean
     * @Title: getPartyFamilyRecordById
     * @Description:  获取家庭成员记录详情
     */
    @RequestMapping(value = "/getPartyFamilyRecordById", method = RequestMethod.POST)
    public RetDataBean getPartyFamilyRecordById(PartyFamilyRecord partyFamilyRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyFamilyRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyFamilyRecordService.selectOnePartyFamilyRecord(partyFamilyRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @param memberId
     * @param punishLevel
     * @return RetDataBean
     * @Title: getMemberPunishList
     * @Description:  获取党员惩戒记录列表
     */
    @RequestMapping(value = "/getMemberPunishList", method = RequestMethod.POST)
    public RetDataBean getMemberPunishList(PageParam pageParam, String beginTime, String endTime, String memberId, String punishLevel) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("r.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyPunishRecordService.getMemberPunishList(pageParam, beginTime, endTime, memberId, punishLevel);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param reason
     * @param lifeContent
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getPartyLifeList
     * @Description:  获取组织生活列表
     */
    @RequestMapping(value = "/getPartyLifeList", method = RequestMethod.POST)
    public RetDataBean getPartyLifeList(PageParam pageParam, String reason, String lifeContent, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("o.join_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyLifeRecordService.getPartyLifeList(pageParam, reason, lifeContent, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param appResult
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getAppraisalList
     * @Description:  获取民主评议列表
     */
    @RequestMapping(value = "/getAppraisalList", method = RequestMethod.POST)
    public RetDataBean getAppraisalList(PageParam pageParam, String appResult, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("o.begin_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyAppraisalRecordService.getAppraisalList(pageParam, appResult, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyAppraisalRecord
     * @return RetDataBean
     * @Title: getPartyAppraisalRecordById
     * @Description:  获取民主评议记录详情
     */
    @RequestMapping(value = "/getPartyAppraisalRecordById", method = RequestMethod.POST)
    public RetDataBean getPartyAppraisalRecordById(PartyAppraisalRecord partyAppraisalRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyAppraisalRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyAppraisalRecordService.selectOnePartyAppraisalRecord(partyAppraisalRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyLifeRecord
     * @return RetDataBean
     * @Title: getPartyLifeRecordById
     * @Description:  获取组织生活记录详情
     */
    @RequestMapping(value = "/getPartyLifeRecordById", method = RequestMethod.POST)
    public RetDataBean getPartyLifeRecordById(PartyLifeRecord partyLifeRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyLifeRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyLifeRecordService.selectOnePartyLifeRecord(partyLifeRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param postType
     * @param admPost
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getAdmRecordList
     * @Description:  获取行政任职记录列表
     */
    @RequestMapping(value = "/getAdmRecordList", method = RequestMethod.POST)
    public RetDataBean getAdmRecordList(PageParam pageParam, String postType, String admPost, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("o.sort_no");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyAdmRecordService.getAdmRecordList(pageParam, postType, admPost, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyAdmRecord
     * @return RetDataBean
     * @Title: getPartyAdmRecordById
     * @Description:  获取行政任职记录详情
     */
    @RequestMapping(value = "/getPartyAdmRecordById", method = RequestMethod.POST)
    public RetDataBean getPartyAdmRecordById(PartyAdmRecord partyAdmRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyAdmRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyAdmRecordService.selectOnePartyAdmRecord(partyAdmRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyPostRecord
     * @return RetDataBean
     * @Title: getPartyPostRecordById
     * @Description:  获取党内任职记录详情
     */
    @RequestMapping(value = "/getPartyPostRecordById", method = RequestMethod.POST)
    public RetDataBean getPartyPostRecordById(PartyPostRecord partyPostRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyPostRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyPostRecordService.selectOnePartyPostRecord(partyPostRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param postType
     * @param takeType
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getPartyPostList
     * @Description:   获取党内任职记录列表
     */
    @RequestMapping(value = "/getPartyPostList", method = RequestMethod.POST)
    public RetDataBean getPartyPostList(PageParam pageParam, String postType, String takeType, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("o.begin_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyPostRecordService.getPartyPostList(pageParam, postType, takeType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param classType
     * @param trainType
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getMemberTrainList
     * @Description:  获取党员培训记录列表
     */
    @RequestMapping(value = "/getMemberTrainList", method = RequestMethod.POST)
    public RetDataBean getMemberTrainList(PageParam pageParam, String classType, String trainType, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("o.begin_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyTrainRecordService.getMemberTrainList(pageParam, classType, trainType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyTrainRecord
     * @return RetDataBean
     * @Title: getPartyTrainRecordById
     * @Description:  获取党员培训记录详情
     */
    @RequestMapping(value = "/getPartyTrainRecordById", method = RequestMethod.POST)
    public RetDataBean getPartyTrainRecordById(PartyTrainRecord partyTrainRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyTrainRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyTrainRecordService.selectOnePartyTrainRecord(partyTrainRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyMemberDeath
     * @return RetDataBean
     * @Title: getPartyMemberDeathById
     * @Description:  获取党员去世记录详情
     */
    @RequestMapping(value = "/getPartyMemberDeathById", method = RequestMethod.POST)
    public RetDataBean getPartyMemberDeathById(PartyMemberDeath partyMemberDeath) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyMemberDeath.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyMemberDeathService.selectOnePartyMemberDeath(partyMemberDeath));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getMemberDeathList
     * @Description:  获取出党人员列表
     */
    @RequestMapping(value = "/getMemberDeathList", method = RequestMethod.POST)
    public RetDataBean getMemberDeathList(PageParam pageParam, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("o.death_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyMemberDeathService.getMemberDeathList(pageParam, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyMemberOut
     * @return RetDataBean
     * @Title: getPartyMemberOutById
     * @Description:  获取离党记录详情
     */
    @RequestMapping(value = "/getPartyMemberOutById", method = RequestMethod.POST)
    public RetDataBean getPartyMemberOutById(PartyMemberOut partyMemberOut) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyMemberOut.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyMemberOutService.selectOnePartyMemberOut(partyMemberOut));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取出党人员列表
     *
     * @param pageParam
     * @param outType
     * @param beginTime
     * @param endTime
     * @return
     */
    @RequestMapping(value = "/getMemberOutList", method = RequestMethod.POST)
    public RetDataBean getMemberOutList(PageParam pageParam, String outType, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("o.out_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyMemberOutService.getMemberOutList(pageParam, outType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param workType
     * @return RetDataBean
     * @Title: getMypartyWorkRecordList
     * @Description:  获取需我参加的党员活动列表
     */
    @RequestMapping(value = "/getMypartyWorkRecordList", method = RequestMethod.POST)
    public RetDataBean getMypartyWorkRecordList(PageParam pageParam, String workType) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("w.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            String nowTime = SysTools.getTime("yyyy-MM-dd HH:mm:ss");
            PageInfo<Map<String, String>> pageInfo = partyWorkRecordService.getMyPartyWorkRecordList(pageParam, workType, nowTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @param workType
     * @return RetDataBean
     * @Title: getpartyWorkRecordList
     * @Description:  历史活动列表
     */
    @RequestMapping(value = "/getpartyWorkRecordList", method = RequestMethod.POST)
    public RetDataBean getpartyWorkRecordList(PageParam pageParam, String beginTime, String endTime, String workType) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("w.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            String joinUser = "";
            if (StringUtils.isNotBlank(pageParam.getSearch())) {

                UserInfo userInfo = new UserInfo();
                userInfo.setOrgId(account.getOrgId());
                userInfo.setUserName(pageParam.getSearch());
                userInfo = userInfoService.selectOneUserInfo(userInfo);
                if (userInfo != null) {
                    joinUser = userInfo.getAccountId();
                }
            }
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyWorkRecordService.getPartyWorkRecordList(pageParam, workType, joinUser, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    @RequestMapping(value = "/getHistoryPartyWorkRecordList", method = RequestMethod.POST)
    public RetDataBean getHistoryPartyWorkRecordList(PageParam pageParam, String beginTime, String endTime, String workType) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("w.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyWorkRecordService.getHistoryPartyWorkRecordList(pageParam, workType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyWorkRecord
     * @return RetDataBean
     * @Title: getPartyWorkRecordById
     * @Description:  获取党员活动详情
     */
    @RequestMapping(value = "/getPartyWorkRecordById", method = RequestMethod.POST)
    public RetDataBean getPartyWorkRecordById(PartyWorkRecord partyWorkRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyWorkRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyWorkRecordService.selectOnePartyWorkRecord(partyWorkRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param partyOrgId
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getMemberInFlowList
     * @Description:  获取流入党员列表
     */
    @RequestMapping(value = "/getMemberInFlowList", method = RequestMethod.POST)
    public RetDataBean getMemberInFlowList(PageParam pageParam, String partyOrgId, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("m.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyMemberService.getMemberInFlowList(pageParam, partyOrgId, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param memberIds
     * @return RetDataBean
     * @Title: getPartyMemeberByIds
     * @Description:  获取党员姓名列表
     */
    @RequestMapping(value = "/getPartyMemeberByIds", method = RequestMethod.POST)
    public RetDataBean getPartyMemeberByIds(String memberIds) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyMemberService.getPartyMemeberByIds(account.getOrgId(), memberIds));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param searchuser
     * @return RetDataBean
     * @Title: getPartyMemberBySearchuser
     * @Description:  党员选择时查询
     */
    @RequestMapping(value = "/getPartyMemberBySearchuser", method = RequestMethod.POST)
    public RetDataBean getPartyMemberBySearchuser(String searchuser) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isNotBlank(searchuser)) {
                return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyMemberService.getPartyMemberBySearchuser(account.getOrgId(), searchuser));
            } else {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyOrgId
     * @return RetDataBean
     * @Title: getSelectPartyMemberByPartyId
     * @Description:  按党支部获取党员列表
     */
    @RequestMapping(value = "/getSelectPartyMemberByPartyId", method = RequestMethod.POST)
    public RetDataBean getSelectPartyMemberByPartyId(String partyOrgId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyMemberService.getSelectPartyMemberByPartyId(account.getOrgId(), partyOrgId));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param year
     * @param partyOrgId
     * @return RetDataBean
     * @Title: getPartyVerifyList
     * @Description:  获取考核党员列表
     */
    @RequestMapping(value = "/getPartyVerifyList", method = RequestMethod.POST)
    public RetDataBean getPartyVerifyList(PageParam pageParam, String year, String partyOrgId) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("i.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyMemberService.getPartyVerifyList(pageParam, partyOrgId, year);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param partyOrgId
     * @return RetDataBean
     * @Title: getPartyMemberListByPartyOrgId
     * @Description:  按党组织获取党员列表
     */
    @RequestMapping(value = "/getPartyMemberListByPartyOrgId", method = RequestMethod.POST)
    public RetDataBean getPartyMemberListByPartyOrgId(PageParam pageParam, String partyOrgId) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("m.sort_no");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyMemberService.getPartyMemberListByPartyOrgId(pageParam, partyOrgId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param partyOrgId
     * @return RetDataBean
     * @Title: getPartyMemberAccountListByPartyOrgId
     * @Description:  获取账号绑定党员列表
     */
    @RequestMapping(value = "/getPartyMemberAccountListByPartyOrgId", method = RequestMethod.POST)
    public RetDataBean getPartyMemberAccountListByPartyOrgId(PageParam pageParam, String partyOrgId) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("m.sort_no");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyMemberService.getPartyMemberAccountListByPartyOrgId(pageParam, partyOrgId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyMember
     * @return RetDataBean
     * @Title: getPartyMemberById
     * @Description:  党员信息详情
     */
    @RequestMapping(value = "/getPartyMemberById", method = RequestMethod.POST)
    public RetDataBean getPartyMemberById(PartyMember partyMember) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyMember.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyMemberService.selectOnePartyMember(partyMember));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }
}
