package com.core136.controller.partymember;

import com.core136.bean.account.Account;
import com.core136.bean.partymember.*;
import com.core136.common.SysRunConfig;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partyparam.PartyWorkRecordService;
import com.core136.service.account.AccountService;
import com.core136.service.account.UserInfoService;
import com.core136.service.partymember.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/set/partymemberset")
public class RouteSetPartyMemberController {
    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private UserInfoService userInfoService;

    @Autowired
    public void setUserInfoService(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }

    private PartyMemberService partyMemberService;

    @Autowired
    public void setPartyMemberService(PartyMemberService partyMemberService) {
        this.partyMemberService = partyMemberService;
    }

    private PartyWorkRecordService partyWorkRecordService;

    @Autowired
    public void setPartyWorkRecordService(PartyWorkRecordService partyWorkRecordService) {
        this.partyWorkRecordService = partyWorkRecordService;
    }

    private PartyMemberOutService partyMemberOutService;

    @Autowired
    public void setPartyMemberOutService(PartyMemberOutService partyMemberOutService) {
        this.partyMemberOutService = partyMemberOutService;
    }

    private PartyMemberDeathService partyMemberDeathService;

    @Autowired
    public void setPartyMemberDeathService(PartyMemberDeathService partyMemberDeathService) {
        this.partyMemberDeathService = partyMemberDeathService;
    }

    private PartyPostRecordService partyPostRecordService;

    @Autowired
    public void setPartyPostRecordService(PartyPostRecordService partyPostRecordService) {
        this.partyPostRecordService = partyPostRecordService;
    }

    private PartyTrainRecordService partyTrainRecordService;

    @Autowired
    public void setPartyTrainRecordService(PartyTrainRecordService partyTrainRecordService) {
        this.partyTrainRecordService = partyTrainRecordService;
    }

    private PartyLifeRecordService partyLifeRecordService;

    @Autowired
    public void setPartyLifeRecordService(PartyLifeRecordService partyLifeRecordService) {
        this.partyLifeRecordService = partyLifeRecordService;
    }

    private PartyAppraisalRecordService partyAppraisalRecordService;

    @Autowired
    public void setPartyAppraisalRecordService(PartyAppraisalRecordService partyAppraisalRecordService) {
        this.partyAppraisalRecordService = partyAppraisalRecordService;
    }

    private PartyAdmRecordService partyAdmRecordService;

    @Autowired
    public void setPartyAdmRecordService(PartyAdmRecordService partyAdmRecordService) {
        this.partyAdmRecordService = partyAdmRecordService;
    }

    private PartyPunishRecordService partyPunishRecordService;

    @Autowired
    public void setPartyPunishRecordService(PartyPunishRecordService partyPunishRecordService) {
        this.partyPunishRecordService = partyPunishRecordService;
    }

    private PartyRewardRecordService partyRewardRecordService;

    @Autowired
    public void setPartyRewardRecordService(PartyRewardRecordService partyRewardRecordService) {
        this.partyRewardRecordService = partyRewardRecordService;
    }

    private PartyFamilyRecordService partyFamilyRecordService;

    @Autowired
    public void setPartyFamilyRecordService(PartyFamilyRecordService partyFamilyRecordService) {
        this.partyFamilyRecordService = partyFamilyRecordService;
    }

    private PartyMemberJoinService partyMemberJoinService;

    @Autowired
    public void setPartyMemberJoinService(PartyMemberJoinService partyMemberJoinService) {
        this.partyMemberJoinService = partyMemberJoinService;
    }

    private PartyMemberFormalService partyMemberFormalService;

    @Autowired
    public void setPartyMemberFormalService(PartyMemberFormalService partyMemberFormalService) {
        this.partyMemberFormalService = partyMemberFormalService;
    }

    private PartyMemberPrepareService partyMemberPrepareService;

    @Autowired
    public void setPartyMemberPrepareService(PartyMemberPrepareService partyMemberPrepareService) {
        this.partyMemberPrepareService = partyMemberPrepareService;
    }

    private PartyMemberTargetService partyMemberTargetService;

    @Autowired
    public void setPartyMemberTargetService(PartyMemberTargetService partyMemberTargetService) {
        this.partyMemberTargetService = partyMemberTargetService;
    }

    private PartyMemberActivistsService partyMemberActivistsService;

    @Autowired
    public void setPartyMemberActivistsService(PartyMemberActivistsService partyMemberActivistsService) {
        this.partyMemberActivistsService = partyMemberActivistsService;
    }

    private PartyTargetTrainService partyTargetTrainService;

    @Autowired
    public void setPartyTargetTrainService(PartyTargetTrainService partyTargetTrainService) {
        this.partyTargetTrainService = partyTargetTrainService;
    }

    private PartyActivistsTrainService partyActivistsTrainService;

    @Autowired
    public void setPartyActivistsTrainService(PartyActivistsTrainService partyActivistsTrainService) {
        this.partyActivistsTrainService = partyActivistsTrainService;
    }

    private PartyVolunteerService partyVolunteerService;

    @Autowired
    public void setPartyVolunteerService(PartyVolunteerService partyVolunteerService) {
        this.partyVolunteerService = partyVolunteerService;
    }

    private PartyArmyService partyArmyService;

    @Autowired
    public void setPartyArmyService(PartyArmyService partyArmyService) {
        this.partyArmyService = partyArmyService;
    }

    private PartyMemberDifficultyService partyMemberDifficultyService;

    @Autowired
    public void setPartyMemberDifficultyService(PartyMemberDifficultyService partyMemberDifficultyService) {
        this.partyMemberDifficultyService = partyMemberDifficultyService;
    }

    private PartyMemberPointsService partyMemberPointsService;

    @Autowired
    public void setPartyMemberPointsService(PartyMemberPointsService partyMemberPointsService) {
        this.partyMemberPointsService = partyMemberPointsService;
    }

    private PartyMemberLoseService partyMemberLoseService;

    @Autowired
    public void setPartyMemberLoseService(PartyMemberLoseService partyMemberLoseService) {
        this.partyMemberLoseService = partyMemberLoseService;
    }

    /**
     * @param partyMemberLose
     * @return RetDataBean
     * @Title: insertPartyMemberLose
     * @Description:  添加失联党员信息
     */
    @RequestMapping(value = "/insertPartyMemberLose", method = RequestMethod.POST)
    public RetDataBean insertPartyMemberLose(PartyMemberLose partyMemberLose) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyMemberLose.setRecordId(SysTools.getGUID());
            partyMemberLose.setCreateUser(account.getAccountId());
            partyMemberLose.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyMemberLose.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyMemberLoseService.insertPartyMemberLose(partyMemberLose));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyMemberLose
     * @return RetDataBean
     * @Title: deletePartyMemberLose
     * @Description:  删除失联党员信息
     */
    @RequestMapping(value = "/deletePartyMemberLose", method = RequestMethod.POST)
    public RetDataBean deletePartyMemberLose(PartyMemberLose partyMemberLose) {
        try {
            if (StringUtils.isBlank(partyMemberLose.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyMemberLose.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyMemberLoseService.deletePartyMemberLose(partyMemberLose));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyMemberLose
     * @return RetDataBean
     * @Title: updatePartyMemberLose
     * @Description:  更新失联党员信息
     */
    @RequestMapping(value = "/updatePartyMemberLose", method = RequestMethod.POST)
    public RetDataBean updatePartyMemberLose(PartyMemberLose partyMemberLose) {
        try {
            if (StringUtils.isBlank(partyMemberLose.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyMemberLose.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyMemberLose.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyMemberLoseService.updatePartyMemberLose(example, partyMemberLose));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 添加党员积分
     *
     * @param partyMemberPoints
     * @return
     */
    @RequestMapping(value = "/insertPartyMemberPoints", method = RequestMethod.POST)
    public RetDataBean insertPartyMemberPoints(PartyMemberPoints partyMemberPoints) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyMemberPoints.setRecordId(SysTools.getGUID());
            partyMemberPoints.setCreateUser(account.getAccountId());
            partyMemberPoints.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyMemberPoints.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyMemberPointsService.insertPartyMemberPoints(partyMemberPoints));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除党员积分
     *
     * @param partyMemberPoints
     * @return
     */
    @RequestMapping(value = "/deletePartyMemberPoints", method = RequestMethod.POST)
    public RetDataBean deletePartyMemberPoints(PartyMemberPoints partyMemberPoints) {
        try {
            if (StringUtils.isBlank(partyMemberPoints.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyMemberPoints.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyMemberPointsService.deletePartyMemberPoints(partyMemberPoints));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新党员积分
     *
     * @param partyMemberPoints
     * @return
     */
    @RequestMapping(value = "/updatePartyMemberPoints", method = RequestMethod.POST)
    public RetDataBean updatePartyMemberPoints(PartyMemberPoints partyMemberPoints) {
        try {
            if (StringUtils.isBlank(partyMemberPoints.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyMemberPoints.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyMemberPoints.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyMemberPointsService.updatePartyMemberPoints(example, partyMemberPoints));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param memberIds
     * @return RetDataBean
     * @Title: setUnbindAccount
     * @Description:  解除账号
     */
    @RequestMapping(value = "/setUnbindAccount", method = RequestMethod.POST)
    public RetDataBean setUnbindAccount(String memberIds) {
        Account account = accountService.getRedisAUserInfoToAccount();
        return partyMemberService.setUnbindAccount(account.getOrgId(), memberIds);
    }


    /**
     * @param accountId
     * @param memberId
     * @return RetDataBean
     * @Title: setMemberAccount
     * @Description:  党员信息与系统账号绑定
     */
    @RequestMapping(value = "/setMemberAccount", method = RequestMethod.POST)
    public RetDataBean setMemberAccount(String accountId, String memberId) {
        Account account = accountService.getRedisAUserInfoToAccount();
        return partyMemberService.setMemberAccount(account.getOrgId(), memberId, accountId);
    }

    /**
     * @param partyMemberDifficulty
     * @return RetDataBean
     * @Title: insertPartyMemberDifficulty
     * @Description:  添加困难党员记录
     */
    @RequestMapping(value = "/insertPartyMemberDifficulty", method = RequestMethod.POST)
    public RetDataBean insertPartyMemberDifficulty(PartyMemberDifficulty partyMemberDifficulty) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyMemberDifficulty.setRecordId(SysTools.getGUID());
            partyMemberDifficulty.setCreateUser(account.getAccountId());
            partyMemberDifficulty.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyMemberDifficulty.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyMemberDifficultyService.insertPartyMemberDifficulty(partyMemberDifficulty));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyMemberDifficulty
     * @return RetDataBean
     * @Title: deletePartyMemberDifficulty
     * @Description:  删除困难党员记录
     */
    @RequestMapping(value = "/deletePartyMemberDifficulty", method = RequestMethod.POST)
    public RetDataBean deletePartyMemberDifficulty(PartyMemberDifficulty partyMemberDifficulty) {
        try {
            if (StringUtils.isBlank(partyMemberDifficulty.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyMemberDifficulty.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyMemberDifficultyService.deletePartyMemberDifficulty(partyMemberDifficulty));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyMemberDifficulty
     * @return RetDataBean
     * @Title: updatePartyMemberDifficulty
     * @Description:  更新困难党员记录
     */
    @RequestMapping(value = "/updatePartyMemberDifficulty", method = RequestMethod.POST)
    public RetDataBean updatePartyMemberDifficulty(PartyMemberDifficulty partyMemberDifficulty) {
        try {
            if (StringUtils.isBlank(partyMemberDifficulty.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyMemberDifficulty.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyMemberDifficulty.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyMemberDifficultyService.updatePartyMemberDifficulty(example, partyMemberDifficulty));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyTargetTrain
     * @return RetDataBean
     * @Title: insertPartyTargetTrain
     * @Description:  创建发展对象培训记录
     */
    @RequestMapping(value = "/insertPartyTargetTrain", method = RequestMethod.POST)
    public RetDataBean insertPartyTargetTrain(PartyTargetTrain partyTargetTrain) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyTargetTrain.setRecordId(SysTools.getGUID());
            partyTargetTrain.setCreateUser(account.getAccountId());
            partyTargetTrain.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyTargetTrain.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyTargetTrainService.insertPartyTargetTrain(partyTargetTrain));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyTargetTrain
     * @return RetDataBean
     * @Title: deletePartyTargetTrain
     * @Description:  删除发展对象培训记录
     */
    @RequestMapping(value = "/deletePartyTargetTrain", method = RequestMethod.POST)
    public RetDataBean deletePartyTargetTrain(PartyTargetTrain partyTargetTrain) {
        try {
            if (StringUtils.isBlank(partyTargetTrain.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyTargetTrain.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyTargetTrainService.deletePartyTargetTrain(partyTargetTrain));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyTargetTrain
     * @return RetDataBean
     * @Title: updatePartyTargetTrain
     * @Description:  更新发展对象培训记录
     */
    @RequestMapping(value = "/updatePartyTargetTrain", method = RequestMethod.POST)
    public RetDataBean updatePartyTargetTrain(PartyTargetTrain partyTargetTrain) {
        try {
            if (StringUtils.isBlank(partyTargetTrain.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyTargetTrain.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyTargetTrain.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyTargetTrainService.updatePartyTargetTrain(example, partyTargetTrain));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param partyActivistsTrain
     * @return RetDataBean
     * @Title: insertPartyActivistsTrain
     * @Description:  添加积极份子培训记录
     */
    @RequestMapping(value = "/insertPartyActivistsTrain", method = RequestMethod.POST)
    public RetDataBean insertPartyActivistsTrain(PartyActivistsTrain partyActivistsTrain) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyActivistsTrain.setRecordId(SysTools.getGUID());
            partyActivistsTrain.setCreateUser(account.getAccountId());
            partyActivistsTrain.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyActivistsTrain.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyActivistsTrainService.insertPartyActivistsTrain(partyActivistsTrain));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyActivistsTrain
     * @return RetDataBean
     * @Title: deletePartyActivistsTrain
     * @Description:  删除积极份子培训记录
     */
    @RequestMapping(value = "/deletePartyActivistsTrain", method = RequestMethod.POST)
    public RetDataBean deletePartyActivistsTrain(PartyActivistsTrain partyActivistsTrain) {
        try {
            if (StringUtils.isBlank(partyActivistsTrain.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyActivistsTrain.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyActivistsTrainService.deletePartyActivistsTrain(partyActivistsTrain));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyActivistsTrain
     * @return RetDataBean
     * @Title: updatePartyActivistsTrain
     * @Description:  更新积极份子培训记录
     */
    @RequestMapping(value = "/updatePartyActivistsTrain", method = RequestMethod.POST)
    public RetDataBean updatePartyActivistsTrain(PartyActivistsTrain partyActivistsTrain) {
        try {
            if (StringUtils.isBlank(partyActivistsTrain.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyActivistsTrain.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyActivistsTrain.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyActivistsTrainService.updatePartyActivistsTrain(example, partyActivistsTrain));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyVolunteer
     * @return RetDataBean
     * @Title: insertPartyVolunteer
     * @Description:  添加志愿者服务记录
     */
    @RequestMapping(value = "/insertPartyVolunteer", method = RequestMethod.POST)
    public RetDataBean insertPartyVolunteer(PartyVolunteer partyVolunteer) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyVolunteer.setRecordId(SysTools.getGUID());
            partyVolunteer.setCreateUser(account.getAccountId());
            partyVolunteer.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyVolunteer.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyVolunteerService.insertPartyVolunteer(partyVolunteer));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyVolunteer
     * @return RetDataBean
     * @Title: deletePartyVolunteer
     * @Description:  删除志愿者服务记录
     */
    @RequestMapping(value = "/deletePartyVolunteer", method = RequestMethod.POST)
    public RetDataBean deletePartyVolunteer(PartyVolunteer partyVolunteer) {
        try {
            if (StringUtils.isBlank(partyVolunteer.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyVolunteer.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyVolunteerService.deletePartyVolunteer(partyVolunteer));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyVolunteer
     * @return RetDataBean
     * @Title: updatePartyVolunteer
     * @Description:  更新志愿者服务记录
     */
    @RequestMapping(value = "/updatePartyVolunteer", method = RequestMethod.POST)
    public RetDataBean updatePartyVolunteer(PartyVolunteer partyVolunteer) {
        try {
            if (StringUtils.isBlank(partyVolunteer.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyVolunteer.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyVolunteer.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyVolunteerService.updatePartyVolunteer(example, partyVolunteer));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyArmy
     * @return RetDataBean
     * @Title: insertPartyArmy
     * @Description:  添加统战记录
     */
    @RequestMapping(value = "/insertPartyArmy", method = RequestMethod.POST)
    public RetDataBean insertPartyArmy(PartyArmy partyArmy) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyArmy.setRecordId(SysTools.getGUID());
            partyArmy.setCreateUser(account.getAccountId());
            partyArmy.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyArmy.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyArmyService.insertPartyArmy(partyArmy));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyArmy
     * @return RetDataBean
     * @Title: deletePartyArmy
     * @Description:  删除统战记录
     */
    @RequestMapping(value = "/deletePartyArmy", method = RequestMethod.POST)
    public RetDataBean deletePartyArmy(PartyArmy partyArmy) {
        try {
            if (StringUtils.isBlank(partyArmy.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyArmy.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyArmyService.deletePartyArmy(partyArmy));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyArmy
     * @return RetDataBean
     * @Title: updatePartyArmy
     * @Description:  更新统战记录
     */
    @RequestMapping(value = "/updatePartyArmy", method = RequestMethod.POST)
    public RetDataBean updatePartyArmy(PartyArmy partyArmy) {
        try {
            if (StringUtils.isBlank(partyArmy.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyArmy.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyArmy.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyArmyService.updatePartyArmy(example, partyArmy));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyMemberActivists
     * @return RetDataBean
     * @Title: setPartyMemberActivists
     * @Description:  设置申请党员为积极份子
     */
    @RequestMapping(value = "/setPartyMemberActivists", method = RequestMethod.POST)
    public RetDataBean setPartyMemberActivists(PartyMemberActivists partyMemberActivists) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyMemberActivists.setRecordId(SysTools.getGUID());
            partyMemberActivists.setCreateUser(account.getAccountId());
            partyMemberActivists.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyMemberActivists.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyMemberActivistsService.setPartyMemberActivists(partyMemberActivists));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyMemberFormal
     * @return RetDataBean
     * @Title: setPartyMemberFormal
     * @Description:  预备党员转正
     */
    @RequestMapping(value = "/setPartyMemberFormal", method = RequestMethod.POST)
    public RetDataBean setPartyMemberFormal(PartyMemberFormal partyMemberFormal) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyMemberFormal.setRecordId(SysTools.getGUID());
            partyMemberFormal.setCreateUser(account.getAccountId());
            partyMemberFormal.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyMemberFormal.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyMemberFormalService.setPartyMemberFormal(partyMemberFormal));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param partyMemberPrepare
     * @return RetDataBean
     * @Title: setPartyMemberPrepare
     * @Description:  设置入党申请人为预备党员
     */
    @RequestMapping(value = "/setPartyMemberPrepare", method = RequestMethod.POST)
    public RetDataBean setPartyMemberPrepare(PartyMemberPrepare partyMemberPrepare) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyMemberPrepare.setRecordId(SysTools.getGUID());
            partyMemberPrepare.setCreateUser(account.getAccountId());
            partyMemberPrepare.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyMemberPrepare.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyMemberPrepareService.setPartyMemberPrepare(partyMemberPrepare));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyMemberTarget
     * @return RetDataBean
     * @Title: setPartyMemberTarget
     * @Description:  设置入党申请人为发展对象
     */
    @RequestMapping(value = "/setPartyMemberTarget", method = RequestMethod.POST)
    public RetDataBean setPartyMemberTarget(PartyMemberTarget partyMemberTarget) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyMemberTarget.setRecordId(SysTools.getGUID());
            partyMemberTarget.setCreateUser(account.getAccountId());
            partyMemberTarget.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyMemberTarget.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyMemberTargetService.setPartyMemberTarget(partyMemberTarget));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyMemberTarget
     * @return RetDataBean
     * @Title: insertPartyMemberTarget
     * @Description:  添加发展对象记录
     */
    @RequestMapping(value = "/insertPartyMemberTarget", method = RequestMethod.POST)
    public RetDataBean insertPartyMemberTarget(PartyMemberTarget partyMemberTarget) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyMemberTarget.setRecordId(SysTools.getGUID());
            partyMemberTarget.setCreateUser(account.getAccountId());
            partyMemberTarget.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyMemberTarget.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyMemberTargetService.insertPartyMemberTarget(partyMemberTarget));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyMemberTarget
     * @return RetDataBean
     * @Title: deletePartyMemberTarget
     * @Description:  删除发展对象记录
     */
    @RequestMapping(value = "/deletePartyMemberTarget", method = RequestMethod.POST)
    public RetDataBean deletePartyMemberTarget(PartyMemberTarget partyMemberTarget) {
        try {
            if (StringUtils.isBlank(partyMemberTarget.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyMemberTarget.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyMemberTargetService.deletePartyMemberTarget(partyMemberTarget));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyMemberTarget
     * @return RetDataBean
     * @Title: updatePartyMemberTarget
     * @Description:  更新发展对象记录
     */
    @RequestMapping(value = "/updatePartyMemberTarget", method = RequestMethod.POST)
    public RetDataBean updatePartyMemberTarget(PartyMemberTarget partyMemberTarget) {
        try {
            if (StringUtils.isBlank(partyMemberTarget.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyMemberTarget.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyMemberTarget.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyMemberTargetService.updatePartyMemberTarget(example, partyMemberTarget));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyMemberJoin
     * @return RetDataBean
     * @Title: insertPartyMemberJoin
     * @Description:  添加入党申请
     */
    @RequestMapping(value = "/insertPartyMemberJoin", method = RequestMethod.POST)
    public RetDataBean insertPartyMemberJoin(PartyMemberJoin partyMemberJoin) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyMemberJoin.setRecordId(SysTools.getGUID());
            partyMemberJoin.setCreateUser(account.getAccountId());
            partyMemberJoin.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyMemberJoin.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyMemberJoinService.insertPartyMemberJoin(partyMemberJoin));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyMemberJoin
     * @return RetDataBean
     * @Title: deletePartyMemberJoin
     * @Description:  删除入党申请
     */
    @RequestMapping(value = "/deletePartyMemberJoin", method = RequestMethod.POST)
    public RetDataBean deletePartyMemberJoin(PartyMemberJoin partyMemberJoin) {
        try {
            if (StringUtils.isBlank(partyMemberJoin.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyMemberJoin.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyMemberJoinService.deletePartyMemberJoin(partyMemberJoin));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyMemberJoin
     * @return RetDataBean
     * @Title: updatePartyMemberJoin
     * @Description:  更新入党申请
     */
    @RequestMapping(value = "/updatePartyMemberJoin", method = RequestMethod.POST)
    public RetDataBean updatePartyMemberJoin(PartyMemberJoin partyMemberJoin) {
        try {
            if (StringUtils.isBlank(partyMemberJoin.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyMemberJoin.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyMemberJoin.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyMemberJoinService.updatePartyMemberJoin(example, partyMemberJoin));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param partyFamilyRecord
     * @return RetDataBean
     * @Title: insertPartyFamilyRecord
     * @Description:  添加党员家庭成员记录
     */
    @RequestMapping(value = "/insertPartyFamilyRecord", method = RequestMethod.POST)
    public RetDataBean insertPartyFamilyRecord(PartyFamilyRecord partyFamilyRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyFamilyRecord.setRecordId(SysTools.getGUID());
            partyFamilyRecord.setCreateUser(account.getAccountId());
            partyFamilyRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyFamilyRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyFamilyRecordService.insertPartyFamilyRecord(partyFamilyRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyFamilyRecord
     * @return RetDataBean
     * @Title: deletePartyFamilyRecord
     * @Description:  删除家庭成员记录
     */
    @RequestMapping(value = "/deletePartyFamilyRecord", method = RequestMethod.POST)
    public RetDataBean deletePartyFamilyRecord(PartyFamilyRecord partyFamilyRecord) {
        try {
            if (StringUtils.isBlank(partyFamilyRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyFamilyRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyFamilyRecordService.deletePartyFamilyRecord(partyFamilyRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyFamilyRecord
     * @return RetDataBean
     * @Title: updatePartyFamilyRecord
     * @Description:  更新党员家庭成员
     */
    @RequestMapping(value = "/updatePartyFamilyRecord", method = RequestMethod.POST)
    public RetDataBean updatePartyFamilyRecord(PartyFamilyRecord partyFamilyRecord) {
        try {
            if (StringUtils.isBlank(partyFamilyRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyFamilyRecord.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyFamilyRecord.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyFamilyRecordService.updatePartyFamilyRecord(example, partyFamilyRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyPunishRecord
     * @return RetDataBean
     * @Title: insertPartyPunishRecord
     * @Description:  创建党员惩戒记录
     */
    @RequestMapping(value = "/insertPartyPunishRecord", method = RequestMethod.POST)
    public RetDataBean insertPartyPunishRecord(PartyPunishRecord partyPunishRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyPunishRecord.setRecordId(SysTools.getGUID());
            partyPunishRecord.setCreateUser(account.getAccountId());
            partyPunishRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyPunishRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyPunishRecordService.insertPartyPunishRecord(partyPunishRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyPunishRecord
     * @return RetDataBean
     * @Title: deletePartyPunishRecord
     * @Description:  党员惩戒记录删除
     */
    @RequestMapping(value = "/deletePartyPunishRecord", method = RequestMethod.POST)
    public RetDataBean deletePartyPunishRecord(PartyPunishRecord partyPunishRecord) {
        try {
            if (StringUtils.isBlank(partyPunishRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyPunishRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyPunishRecordService.deletePartyPunishRecord(partyPunishRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyPunishRecord
     * @return RetDataBean
     * @Title: updatePartyPunishRecord
     * @Description:  党员惩戒记录更新
     */
    @RequestMapping(value = "/updatePartyPunishRecord", method = RequestMethod.POST)
    public RetDataBean updatePartyPunishRecord(PartyPunishRecord partyPunishRecord) {
        try {
            if (StringUtils.isBlank(partyPunishRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyPunishRecord.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyPunishRecord.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyPunishRecordService.updatePartyPunishRecord(example, partyPunishRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyRewardRecord
     * @return RetDataBean
     * @Title: insertPartyRewardRecord
     * @Description:  添加党员奖励记录
     */
    @RequestMapping(value = "/insertPartyRewardRecord", method = RequestMethod.POST)
    public RetDataBean insertPartyRewardRecord(PartyRewardRecord partyRewardRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyRewardRecord.setRecordId(SysTools.getGUID());
            partyRewardRecord.setCreateUser(account.getAccountId());
            partyRewardRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyRewardRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyRewardRecordService.insertPartyRewardRecord(partyRewardRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyRewardRecord
     * @return RetDataBean
     * @Title: deletePartyRewardRecord
     * @Description:  党员奖励记录删除
     */
    @RequestMapping(value = "/deletePartyRewardRecord", method = RequestMethod.POST)
    public RetDataBean deletePartyRewardRecord(PartyRewardRecord partyRewardRecord) {
        try {
            if (StringUtils.isBlank(partyRewardRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyRewardRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyRewardRecordService.deletePartyRewardRecord(partyRewardRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyRewardRecord
     * @return RetDataBean
     * @Title: updatePartyRewardRecord
     * @Description:  党员奖励记录更新
     */
    @RequestMapping(value = "/updatePartyRewardRecord", method = RequestMethod.POST)
    public RetDataBean updatePartyRewardRecord(PartyRewardRecord partyRewardRecord) {
        try {
            if (StringUtils.isBlank(partyRewardRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyRewardRecord.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyRewardRecord.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyRewardRecordService.updatePartyRewardRecord(example, partyRewardRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyAdmRecord
     * @return RetDataBean
     * @Title: insertPartyAdmRecord
     * @Description:  添加行政任职记录
     */
    @RequestMapping(value = "/insertPartyAdmRecord", method = RequestMethod.POST)
    public RetDataBean insertPartyAdmRecord(PartyAdmRecord partyAdmRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyAdmRecord.setRecordId(SysTools.getGUID());
            partyAdmRecord.setCreateUser(account.getAccountId());
            partyAdmRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyAdmRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyAdmRecordService.insertPartyAdmRecord(partyAdmRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyAdmRecord
     * @return RetDataBean
     * @Title: deletePartyAdmRecord
     * @Description:  删除行政任职记录
     */
    @RequestMapping(value = "/deletePartyAdmRecord", method = RequestMethod.POST)
    public RetDataBean deletePartyAdmRecord(PartyAdmRecord partyAdmRecord) {
        try {
            if (StringUtils.isBlank(partyAdmRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyAdmRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyAdmRecordService.deletePartyAdmRecord(partyAdmRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyAdmRecord
     * @return RetDataBean
     * @Title: updatePartyAdmRecord
     * @Description:  更新行政任职记录
     */
    @RequestMapping(value = "/updatePartyAdmRecord", method = RequestMethod.POST)
    public RetDataBean updatePartyAdmRecord(PartyAdmRecord partyAdmRecord) {
        try {
            if (StringUtils.isBlank(partyAdmRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyAdmRecord.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyAdmRecord.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyAdmRecordService.updatePartyAdmRecord(example, partyAdmRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyLifeRecord
     * @return RetDataBean
     * @Title: insertPartyLifeRecord
     * @Description:  添加组织生活
     */
    @RequestMapping(value = "/insertPartyLifeRecord", method = RequestMethod.POST)
    public RetDataBean insertPartyLifeRecord(PartyLifeRecord partyLifeRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyLifeRecord.setRecordId(SysTools.getGUID());
            partyLifeRecord.setCreateUser(account.getAccountId());
            partyLifeRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyLifeRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyLifeRecordService.insertPartyLifeRecord(partyLifeRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyLifeRecord
     * @return RetDataBean
     * @Title: deletePartyLifeRecord
     * @Description:  删除组织生活记录
     */
    @RequestMapping(value = "/deletePartyLifeRecord", method = RequestMethod.POST)
    public RetDataBean deletePartyLifeRecord(PartyLifeRecord partyLifeRecord) {
        try {
            if (StringUtils.isBlank(partyLifeRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyLifeRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyLifeRecordService.deletePartyLifeRecord(partyLifeRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyLifeRecord
     * @return RetDataBean
     * @Title: updatePartyLifeRecord
     * @Description:  更新组织生活记录
     */
    @RequestMapping(value = "/updatePartyLifeRecord", method = RequestMethod.POST)
    public RetDataBean updatePartyLifeRecord(PartyLifeRecord partyLifeRecord) {
        try {
            if (StringUtils.isBlank(partyLifeRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyLifeRecord.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyLifeRecord.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyLifeRecordService.updatePartyLifeRecord(example, partyLifeRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyAppraisalRecord
     * @return RetDataBean
     * @Title: insertPartyTrain
     * @Description:  添加民主评议
     */
    @RequestMapping(value = "/insertPartyAppraisalRecord", method = RequestMethod.POST)
    public RetDataBean insertPartyAppraisalRecord(PartyAppraisalRecord partyAppraisalRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyAppraisalRecord.setRecordId(SysTools.getGUID());
            partyAppraisalRecord.setCreateUser(account.getAccountId());
            partyAppraisalRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyAppraisalRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyAppraisalRecordService.insertPartyAppraisalRecord(partyAppraisalRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyAppraisalRecord
     * @return RetDataBean
     * @Title: deletePartyTrain
     * @Description:  删除民主评议
     */
    @RequestMapping(value = "/deletePartyAppraisalRecord", method = RequestMethod.POST)
    public RetDataBean deletePartyAppraisalRecord(PartyAppraisalRecord partyAppraisalRecord) {
        try {
            if (StringUtils.isBlank(partyAppraisalRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyAppraisalRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyAppraisalRecordService.deletePartyAppraisalRecord(partyAppraisalRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyAppraisalRecord
     * @return RetDataBean
     * @Title: updatePartyAppraisalRecord
     * @Description:  更新民主评议
     */
    @RequestMapping(value = "/updatePartyAppraisalRecord", method = RequestMethod.POST)
    public RetDataBean updatePartyAppraisalRecord(PartyAppraisalRecord partyAppraisalRecord) {
        try {
            if (StringUtils.isBlank(partyAppraisalRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyAppraisalRecord.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyAppraisalRecord.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyAppraisalRecordService.updatePartyAppraisalRecord(example, partyAppraisalRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyPostRecord
     * @return RetDataBean
     * @Title: insertPartyPostRecord
     * @Description:  创建党内任职记录
     */
    @RequestMapping(value = "/insertPartyPostRecord", method = RequestMethod.POST)
    public RetDataBean insertPartyPostRecord(PartyPostRecord partyPostRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyPostRecord.setRecordId(SysTools.getGUID());
            partyPostRecord.setCreateUser(account.getAccountId());
            partyPostRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyPostRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyPostRecordService.insertPartyPostRecord(partyPostRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyPostRecord
     * @return RetDataBean
     * @Title: deletePartyPostRecord
     * @Description:  删除党内任职记录
     */
    @RequestMapping(value = "/deletePartyPostRecord", method = RequestMethod.POST)
    public RetDataBean deletePartyPostRecord(PartyPostRecord partyPostRecord) {
        try {
            if (StringUtils.isBlank(partyPostRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyPostRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyPostRecordService.deletePartyPostRecord(partyPostRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyPostRecord
     * @return RetDataBean
     * @Title: updatePartyPostRecord
     * @Description:  更新党内任职记录
     */
    @RequestMapping(value = "/updatePartyPostRecord", method = RequestMethod.POST)
    public RetDataBean updatePartyPostRecord(PartyPostRecord partyPostRecord) {
        try {
            if (StringUtils.isBlank(partyPostRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyPostRecord.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyPostRecord.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyPostRecordService.updatePartyPostRecord(example, partyPostRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyTrainRecord
     * @return RetDataBean
     * @Title: insertPartyTrainRecord
     * @Description:  添加党员培训记录
     */
    @RequestMapping(value = "/insertPartyTrainRecord", method = RequestMethod.POST)
    public RetDataBean insertPartyTrainRecord(PartyTrainRecord partyTrainRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyTrainRecord.setRecordId(SysTools.getGUID());
            partyTrainRecord.setCreateUser(account.getAccountId());
            partyTrainRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyTrainRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyTrainRecordService.insertPartyTrainRecord(partyTrainRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyTrainRecord
     * @return RetDataBean
     * @Title: deletePartyTrainRecord
     * @Description:  党员培训记录删除
     */
    @RequestMapping(value = "/deletePartyTrainRecord", method = RequestMethod.POST)
    public RetDataBean deletePartyTrainRecord(PartyTrainRecord partyTrainRecord) {
        try {
            if (StringUtils.isBlank(partyTrainRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyTrainRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyTrainRecordService.deletePartyTrainRecord(partyTrainRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyTrainRecord
     * @return RetDataBean
     * @Title: updatePartyTrainRecord
     * @Description:  更新党员培训记录
     */
    @RequestMapping(value = "/updatePartyTrainRecord", method = RequestMethod.POST)
    public RetDataBean updatePartyTrainRecord(PartyTrainRecord partyTrainRecord) {
        try {
            if (StringUtils.isBlank(partyTrainRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyTrainRecord.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyTrainRecord.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyTrainRecordService.updatePartyTrainRecord(example, partyTrainRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyMemberDeath
     * @return RetDataBean
     * @Title: insertPartyMemberDeath
     * @Description:  添加党员去世记录
     */
    @RequestMapping(value = "/insertPartyMemberDeath", method = RequestMethod.POST)
    public RetDataBean insertPartyMemberDeath(PartyMemberDeath partyMemberDeath) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyMemberDeath.setRecordId(SysTools.getGUID());
            partyMemberDeath.setCreateUser(account.getAccountId());
            partyMemberDeath.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyMemberDeath.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyMemberDeathService.insertPartyMemberDeath(partyMemberDeath));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyMemberDeath
     * @return RetDataBean
     * @Title: deletePartyMemberDeath
     * @Description:  删除党员离世记录
     */
    @RequestMapping(value = "/deletePartyMemberDeath", method = RequestMethod.POST)
    public RetDataBean deletePartyMemberDeath(PartyMemberDeath partyMemberDeath) {
        try {
            if (StringUtils.isBlank(partyMemberDeath.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyMemberDeath.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyMemberDeathService.deletePartyMemberDeath(partyMemberDeath));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyMemberDeath
     * @return RetDataBean
     * @Title: updatePartyMemberDeath
     * @Description:  更新党员去世记录
     */
    @RequestMapping(value = "/updatePartyMemberDeath", method = RequestMethod.POST)
    public RetDataBean updatePartyMemberDeath(PartyMemberDeath partyMemberDeath) {
        try {
            if (StringUtils.isBlank(partyMemberDeath.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyMemberDeath.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyMemberDeath.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyMemberDeathService.updatePartyMemberDeath(example, partyMemberDeath));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyMemberOut
     * @return RetDataBean
     * @Title: insertPartyMemberOut
     * @Description:  创建离党记录
     */
    @RequestMapping(value = "/insertPartyMemberOut", method = RequestMethod.POST)
    public RetDataBean insertPartyMemberOut(PartyMemberOut partyMemberOut) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyMemberOut.setRecordId(SysTools.getGUID());
            partyMemberOut.setCreateUser(account.getAccountId());
            partyMemberOut.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyMemberOut.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyMemberOutService.insertPartyMemberOut(partyMemberOut));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyMemberOut
     * @return RetDataBean
     * @Title: deletePartyMemberOut
     * @Description:  删除离党记录
     */
    @RequestMapping(value = "/deletePartyMemberOut", method = RequestMethod.POST)
    public RetDataBean deletePartyMemberOut(PartyMemberOut partyMemberOut) {
        try {
            if (StringUtils.isBlank(partyMemberOut.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyMemberOut.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyMemberOutService.deletePartyMemberOut(partyMemberOut));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyMemberOut
     * @return RetDataBean
     * @Title: updatePartyMemberOut
     * @Description:  更新离党记录
     */
    @RequestMapping(value = "/updatePartyMemberOut", method = RequestMethod.POST)
    public RetDataBean updatePartyMemberOut(PartyMemberOut partyMemberOut) {
        try {
            if (StringUtils.isBlank(partyMemberOut.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyMemberOut.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyMemberOut.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyMemberOutService.updatePartyMemberOut(example, partyMemberOut));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyWorkRecord
     * @return RetDataBean
     * @Title: insertPartyWorkRecord
     * @Description:  创建党员活动
     */
    @RequestMapping(value = "/insertPartyWorkRecord", method = RequestMethod.POST)
    public RetDataBean insertPartyWorkRecord(PartyWorkRecord partyWorkRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyWorkRecord.setWorkId(SysTools.getGUID());
            partyWorkRecord.setCreateUser(account.getAccountId());
            partyWorkRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyWorkRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyWorkRecordService.insertPartyWorkRecord(partyWorkRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyWorkRecord
     * @return RetDataBean
     * @Title: deletePartyWorkRecord
     * @Description:  删除党员活动
     */
    @RequestMapping(value = "/deletePartyWorkRecord", method = RequestMethod.POST)
    public RetDataBean deletePartyPartyMember(PartyWorkRecord partyWorkRecord) {
        try {
            if (StringUtils.isBlank(partyWorkRecord.getWorkId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyWorkRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyWorkRecordService.deletePartyWorkRecord(partyWorkRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyWorkRecord
     * @return RetDataBean
     * @Title: updatePartyWorkRecord
     * @Description:  党员活动信息更新
     */
    @RequestMapping(value = "/updatePartyWorkRecord", method = RequestMethod.POST)
    public RetDataBean updatePartyWorkRecord(PartyWorkRecord partyWorkRecord) {
        try {
            if (StringUtils.isBlank(partyWorkRecord.getWorkId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyWorkRecord.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("workId", partyWorkRecord.getWorkId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyWorkRecordService.updatePartyWorkRecord(example, partyWorkRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param recordIds
     * @return RetDataBean
     * @Title: deletePartyMenbers
     * @Description:  批量删除党员信息
     */
    @RequestMapping(value = "/deletePartyMenbers", method = RequestMethod.POST)
    public RetDataBean deletePartyMenbers(String recordIds) {
        try {
            if (StringUtils.isBlank(recordIds)) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            String[] redcordArr = recordIds.split(",");
            List<String> redcordList = Arrays.asList(redcordArr);
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            } else {
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyMemberService.deletePartyMenbers(account.getOrgId(), redcordList));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyPartyMember
     * @return RetDataBean
     * @Title: insertPartyMember
     * @Description:  党员录入
     */
    @RequestMapping(value = "/insertPartyMember", method = RequestMethod.POST)
    public RetDataBean insertPartyMember(PartyMember partyPartyMember) {
        try {
            if (SysRunConfig.getModelMap().get("PARTY") == null) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_SYSTEM_NOT_REGEIST);
            } else {
                if (!SysRunConfig.getModelMap().get("PARTY")) {
                    return RetDataTools.NotOk(MessageCode.MESSAGE_SYSTEM_NOT_REGEIST);
                }
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyPartyMember.setDelFlag("0");
            partyPartyMember.setMemberId(SysTools.getGUID());
            partyPartyMember.setCreateUser(account.getAccountId());
            partyPartyMember.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyPartyMember.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyMemberService.insertPartyMember(partyPartyMember));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyPartyMember
     * @return RetDataBean
     * @Title: deletePartyMember
     * @Description:  党员删除
     */
    @RequestMapping(value = "/deletePartyMember", method = RequestMethod.POST)
    public RetDataBean deletePartyMember(PartyMember partyPartyMember) {
        try {
            if (StringUtils.isBlank(partyPartyMember.getMemberId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyPartyMember.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyMemberService.deletePartyMember(partyPartyMember));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyPartyMember
     * @return RetDataBean
     * @Title: updatePartyMember
     * @Description:  党员信息更新
     */
    @RequestMapping(value = "/updatePartyMember", method = RequestMethod.POST)
    public RetDataBean updatePartyMember(PartyMember partyPartyMember) {
        try {
            if (StringUtils.isBlank(partyPartyMember.getMemberId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyMember.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("memberId", partyPartyMember.getMemberId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyMemberService.updatePartyMember(example, partyPartyMember));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }
}
