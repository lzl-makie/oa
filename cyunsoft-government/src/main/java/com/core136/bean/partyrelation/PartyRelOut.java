package com.core136.bean.partyrelation;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: PartyRelOut
 * @Description: 党组织关系调出
 * @author: 稠云技术
 * @date: 2021年3月3日 下午1:40:49
 * @Copyright: 2021 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "party_rel_out")
public class PartyRelOut implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String recordId;
    private String isInSys;
    private String oldPartyOrgId;
    private String outPartyOrgId;
    private String otherPartyOrg;
    private String outType;
    private String memberId;
    private String remark;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getIsInSys() {
        return isInSys;
    }

    public void setIsInSys(String isInSys) {
        this.isInSys = isInSys;
    }

    public String getOldPartyOrgId() {
        return oldPartyOrgId;
    }

    public void setOldPartyOrgId(String oldPartyOrgId) {
        this.oldPartyOrgId = oldPartyOrgId;
    }

    public String getOutPartyOrgId() {
        return outPartyOrgId;
    }

    public void setOutPartyOrgId(String outPartyOrgId) {
        this.outPartyOrgId = outPartyOrgId;
    }

    public String getOtherPartyOrg() {
        return otherPartyOrg;
    }

    public void setOtherPartyOrg(String otherPartyOrg) {
        this.otherPartyOrg = otherPartyOrg;
    }

    public String getOutType() {
        return outType;
    }

    public void setOutType(String outType) {
        this.outType = outType;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }


}
