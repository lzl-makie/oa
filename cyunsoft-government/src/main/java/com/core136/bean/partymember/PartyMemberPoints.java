package com.core136.bean.partymember;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: PartyMemberPoints
 * @Description: 党员积分
 * @author: 稠云技术
 * @date: 2021年4月10日 下午12:51:29
 * @Copyright: 2021 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "party_member_points")
public class PartyMemberPoints implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String recordId;
    private String memberId;
    private String item;
    private String pointsYear;
    private Double points;
    private String remark;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getPointsYear() {
        return pointsYear;
    }

    public void setPointsYear(String pointsYear) {
        this.pointsYear = pointsYear;
    }

    public Double getPoints() {
        return points;
    }

    public void setPoints(Double points) {
        this.points = points;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }


}
