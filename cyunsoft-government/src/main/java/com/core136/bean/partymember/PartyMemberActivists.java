package com.core136.bean.partymember;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: PartyMemberActivists
 * @Description: 确定为积极份子记录
 * @author: 稠云技术
 * @date: 2021年3月6日 下午5:50:39
 * @Copyright: 2021 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "party_member_activists")
public class PartyMemberActivists implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String recordId;
    private String joinRecordId;
    private String trainUser1;
    private String trainUser2;
    private String attach1;
    private String attach2;
    private String remark;
    private String status;
    private String joinTime;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getJoinRecordId() {
        return joinRecordId;
    }

    public void setJoinRecordId(String joinRecordId) {
        this.joinRecordId = joinRecordId;
    }

    public String getAttach1() {
        return attach1;
    }

    public void setAttach1(String attach1) {
        this.attach1 = attach1;
    }

    public String getAttach2() {
        return attach2;
    }

    public void setAttach2(String attach2) {
        this.attach2 = attach2;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getJoinTime() {
        return joinTime;
    }

    public void setJoinTime(String joinTime) {
        this.joinTime = joinTime;
    }

    public String getTrainUser1() {
        return trainUser1;
    }

    public void setTrainUser1(String trainUser1) {
        this.trainUser1 = trainUser1;
    }

    public String getTrainUser2() {
        return trainUser2;
    }

    public void setTrainUser2(String trainUser2) {
        this.trainUser2 = trainUser2;
    }


}
