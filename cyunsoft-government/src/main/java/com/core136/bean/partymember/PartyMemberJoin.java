package com.core136.bean.partymember;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: PartyMemberJoin
 * @Description: 人员入党申请
 * @author: 稠云技术
 * @date: 2020年11月17日 下午12:10:00
 * @Copyright: 2020 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "party_member_join")
public class PartyMemberJoin implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String recordId;
    private String title;
    private Integer statusFlag;
    private String userSex;
    private String userName;
    private String applyTime;
    private String partyOrgId;
    private String cardId;
    private String tel;
    private String isAdm1;
    private String isAdm2;
    private String isAdm3;
    private String maturity;
    private String joinTime;
    private String joinTime1;
    private String joinTime2;
    private String joinTime3;
    private String checkSitu;
    private String trainUser1;
    private String trainUser2;
    private String linkMan1;
    private String linkMan2;
    private String trainTime;
    private String trainResult;
    private String trainRemark;
    private String applyRemark;
    private String attach;
    private String memberId;
    private String createUser;
    private String createTime;
    private String orgId;

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(String applyTime) {
        this.applyTime = applyTime;
    }

    public String getUserSex() {
        return userSex;
    }

    public void setUserSex(String userSex) {
        this.userSex = userSex;
    }

    public String getPartyOrgId() {
        return partyOrgId;
    }

    public void setPartyOrgId(String partyOrgId) {
        this.partyOrgId = partyOrgId;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getIsAdm1() {
        return isAdm1;
    }

    public void setIsAdm1(String isAdm1) {
        this.isAdm1 = isAdm1;
    }

    public String getIsAdm2() {
        return isAdm2;
    }

    public void setIsAdm2(String isAdm2) {
        this.isAdm2 = isAdm2;
    }

    public String getIsAdm3() {
        return isAdm3;
    }

    public void setIsAdm3(String isAdm3) {
        this.isAdm3 = isAdm3;
    }

    public String getMaturity() {
        return maturity;
    }

    public void setMaturity(String maturity) {
        this.maturity = maturity;
    }

    public String getJoinTime() {
        return joinTime;
    }

    public void setJoinTime(String joinTime) {
        this.joinTime = joinTime;
    }

    public String getJoinTime1() {
        return joinTime1;
    }

    public void setJoinTime1(String joinTime1) {
        this.joinTime1 = joinTime1;
    }

    public String getTrainUser1() {
        return trainUser1;
    }

    public void setTrainUser1(String trainUser1) {
        this.trainUser1 = trainUser1;
    }

    public String getTrainUser2() {
        return trainUser2;
    }

    public void setTrainUser2(String trainUser2) {
        this.trainUser2 = trainUser2;
    }

    public String getLinkMan1() {
        return linkMan1;
    }

    public void setLinkMan1(String linkMan1) {
        this.linkMan1 = linkMan1;
    }

    public String getLinkMan2() {
        return linkMan2;
    }

    public void setLinkMan2(String linkMan2) {
        this.linkMan2 = linkMan2;
    }

    public String getTrainTime() {
        return trainTime;
    }

    public void setTrainTime(String trainTime) {
        this.trainTime = trainTime;
    }

    public String getTrainResult() {
        return trainResult;
    }

    public void setTrainResult(String trainResult) {
        this.trainResult = trainResult;
    }

    public String getTrainRemark() {
        return trainRemark;
    }

    public void setTrainRemark(String trainRemark) {
        this.trainRemark = trainRemark;
    }

    public String getApplyRemark() {
        return applyRemark;
    }

    public void setApplyRemark(String applyRemark) {
        this.applyRemark = applyRemark;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public Integer getStatusFlag() {
        return statusFlag;
    }

    public void setStatusFlag(Integer statusFlag) {
        this.statusFlag = statusFlag;
    }

    public String getCheckSitu() {
        return checkSitu;
    }

    public void setCheckSitu(String checkSitu) {
        this.checkSitu = checkSitu;
    }

    public String getJoinTime2() {
        return joinTime2;
    }

    public void setJoinTime2(String joinTime2) {
        this.joinTime2 = joinTime2;
    }

    public String getJoinTime3() {
        return joinTime3;
    }

    public void setJoinTime3(String joinTime3) {
        this.joinTime3 = joinTime3;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

}
