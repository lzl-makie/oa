package com.core136.bean.partyorg;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: PartyMzMeet
 * @Description: 民主生活会
 * @author: 稠云技术
 * @date: 2021年3月3日 上午12:04:52
 * @Copyright: 2021 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "party_mz_meet")
public class PartyMzMeet implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String recordId;
    private String title;
    private String partyOrgId;
    private String meetYear;
    private String meetType;
    private Integer joinUserCount;
    private String beginTime;
    private String endTime;
    private String address;
    private String joinMember;
    private String noStaff;
    private String remark;
    private String attach;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPartyOrgId() {
        return partyOrgId;
    }

    public void setPartyOrgId(String partyOrgId) {
        this.partyOrgId = partyOrgId;
    }

    public String getMeetYear() {
        return meetYear;
    }

    public void setMeetYear(String meetYear) {
        this.meetYear = meetYear;
    }

    public String getMeetType() {
        return meetType;
    }

    public void setMeetType(String meetType) {
        this.meetType = meetType;
    }

    public Integer getJoinUserCount() {
        return joinUserCount;
    }

    public void setJoinUserCount(Integer joinUserCount) {
        this.joinUserCount = joinUserCount;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getJoinMember() {
        return joinMember;
    }

    public void setJoinMember(String joinMember) {
        this.joinMember = joinMember;
    }

    public String getNoStaff() {
        return noStaff;
    }

    public void setNoStaff(String noStaff) {
        this.noStaff = noStaff;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

}
