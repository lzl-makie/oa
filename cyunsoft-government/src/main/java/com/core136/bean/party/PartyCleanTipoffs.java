package com.core136.bean.party;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: PartyCleanTipoffs
 * @Description: 群众举报
 * @author: 稠云技术
 * @date: 2020年11月25日 下午2:40:19
 * @Copyright: 2020 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "party_clean_tipoffs")
public class PartyCleanTipoffs implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String recordId;
    private Integer sortNo;
    private String title;
    private String content;
    private String subheading;
    private String tipoffsType;
    private String attach;
    private String status;
    private String tipoffsUserName;
    private String tipoffsTime;
    private String canal;
    private String approvalUser;
    private String approvalTime;
    private String ApprovalIdea;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSubheading() {
        return subheading;
    }

    public void setSubheading(String subheading) {
        this.subheading = subheading;
    }

    public String getTipoffsType() {
        return tipoffsType;
    }

    public void setTipoffsType(String tipoffsType) {
        this.tipoffsType = tipoffsType;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTipoffsUserName() {
        return tipoffsUserName;
    }

    public void setTipoffsUserName(String tipoffsUserName) {
        this.tipoffsUserName = tipoffsUserName;
    }

    public String getTipoffsTime() {
        return tipoffsTime;
    }

    public void setTipoffsTime(String tipoffsTime) {
        this.tipoffsTime = tipoffsTime;
    }

    public String getCanal() {
        return canal;
    }

    public void setCanal(String canal) {
        this.canal = canal;
    }

    public String getApprovalUser() {
        return approvalUser;
    }

    public void setApprovalUser(String approvalUser) {
        this.approvalUser = approvalUser;
    }

    public String getApprovalTime() {
        return approvalTime;
    }

    public void setApprovalTime(String approvalTime) {
        this.approvalTime = approvalTime;
    }

    public String getApprovalIdea() {
        return ApprovalIdea;
    }

    public void setApprovalIdea(String approvalIdea) {
        ApprovalIdea = approvalIdea;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

}
