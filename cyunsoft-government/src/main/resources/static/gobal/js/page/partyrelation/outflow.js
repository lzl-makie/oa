$(function () {
    $('#remark').summernote({height: 300});
    jeDate("#outTime", {
        format: "YYYY-MM-DD",
        maxDate: getSysDate(),
        isinitVal: true
    });
    $(".js-add-save").unbind("click").click(function () {
        setPartyOutFlow();
    })
})

function setPartyOutFlow() {
    if($("#memberId").attr("data-value")=="")
    {
        layer.msg("党员不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partyrelationset/setPartyOutFlow",
        type: "post",
        dataType: "json",
        data: {
            memberId: $("#memberId").attr("data-value"),
            outTime: $("#outTime").val(),
            attach: $("#attach").attr("data_value"),
            remark: $("#remark").code()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}
