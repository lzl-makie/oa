$(function () {
    $.ajax({
        url: "/ret/partyunionget/getPartyUnionStoryById",
        type: "post",
        dataType: "json",
        data: {recordId: recordId},
        success: function (data) {
            if (data.status == "200") {
                var info = data.list;
                for (var id in info) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", info.attach);
                        createAttach("attach", 1);
                    } else if (id == "content") {
                        $("#content").html(info[id]);
                    } else if (id == "mainImg") {
                        $("#file_img").attr("src", "/sys/file/getStaticImg?r=" + Math.random() + "&module=party&fileName=" + info[id] + "")
                    } else if (id == "storyType") {
                        $("#" + id).html(getCodeClassName(info[id], "partyunion_story"));
                    } else {
                        $("#" + id).html(info[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})
