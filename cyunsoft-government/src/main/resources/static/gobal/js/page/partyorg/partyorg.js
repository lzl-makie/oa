var zTree;
var setting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyorgget/getPartyOrgTree",// Ajax 获取数据的 URL 地址
        autoParam: ["partyOrgId"],// 异步加载时需要自动提交父节点属性的参数
    },
    callback: {
        onClick: zTreeOnClick
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "partyOrgId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "partyOrgName"
        }
    }
};

var setting1 = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyorgget/getPartyOrgTree",// Ajax 获取数据的 URL 地址
        autoParam: ["partyOrgId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        // 禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "partyOrgId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "partyOrgName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("menuTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.partyOrgId;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].partyOrgName + ",";
                vid += nodes[i].partyOrgId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#levelId");
            idem.val(v);
            idem.attr("data-value", vid);
        }
    }
};
var partyTypesetting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyparamget/getPartyTypeTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("partyTypeTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.id;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].sortName + ",";
                vid += nodes[i].sortId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#partyType");
            idem.attr("data-value", vid);
            idem.val(v);
        }
    }
};

var subseting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyparamget/getPartySuboTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("subordinationTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.id;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].sortName + ",";
                vid += nodes[i].sortId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#subordination");
            idem.attr("data-value", vid);
            idem.val(v);
        }
    }
};
var worktypesetting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyparamget/getWorkTypeTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("workTypeTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.id;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].sortName + ",";
                vid += nodes[i].sortId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#workType");
            idem.attr("data-value", vid);
            idem.val(v);
        }
    }
};
var select2;
$(function () {
    $('#remark').summernote({
        height: 300
    });
    select2 = $("#unitId").select2({
        ajax: {
            url: "/ret/partyorgget/getSelect2UnitBaseList",
            dataType: 'json',
            type: "POST",
            delay: 250,
            data: function (params) {
                return {
                    search: params.term
                };
            },
            processResults: function (datas) {
                var datares = {
                    results: []
                };
                var datalist = datas.list;
                datares.results.push({
                    id: "0",
                    text: "无"
                });
                for (var i = 0; i < datalist.length; i++) {
                    var record = datalist[i];
                    var name = record.unitName != '' ? record.unitName : record.shortName;
                    var option = {
                        "id": record.unitId,
                        "text": name
                    };
                    datares.results.push(option);
                }
                return datares;
            },
            cache: true
        },
        language: 'zh-CN',
        escapeMarkup: function (markup) {
            return markup;
        },
        minimumInputLength: 2,
        placeholder: "请输入单位名称",
    })
    var topNode = [{
        partyOrgName: orgName,
        levelId: '',
        isParent: "true",
        partyOrgId: "0",
        icon: "/gobal/img/org/org.png"
    }];
    var topNode1 = [{
        partyOrgName: orgName,
        levelId: '',
        isParent: "true",
        partyOrgId: "0",
        icon: "/gobal/img/org/org.png"
    }];
    zTree = $.fn.zTree.init($("#tree"), setting, topNode);// 初始化树节点时，添加同步获取的数据
    var zTreeObj = $.fn.zTree.init($("#menuTree"), setting1, topNode1);
    var nodes = zTree.getNodes();
    var nodes1 = zTreeObj.getNodes();
    for (var i = 0; i < nodes.length; i++) {
        zTree.expandNode(nodes[i], true, false, false);// 默认展开第一级节点
    }
    for (var i = 0; i < nodes1.length; i++) {
        zTreeObj.expandNode(nodes1[i], true, false, false);// 默认展开第一级节点
    }
    $("#createbut").unbind("click").click(function () {
        insertPartyOrg();
    });
    $("#cbut").unbind("click").click(function () {
        document.getElementById("form").reset();
        $("#remark").code("");
        $("#attach").attr("data_value", "");
        $("#levelId").attr("data-value");
        $("#partyType").attr("data-value");
        $("#workType").attr("data-value");
        $("#subordination").attr("data-value");
        $("#show_attach").html("");
        $("#unitId").empty();
        $("#createbut").show();
        $("#updatabut").hide();
        $("#delbut").hide();
    });
    $("#delbut").unbind("click").click(function () {
        delPartyOrg();
    });
    $("#levelId").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#menuContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });
    $.ajax({
        url: "/ret/partyparamget/getPartyTypeTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            $.fn.zTree.init($("#partyTypeTree"), partyTypesetting, data);
        }
    });
    $("#partyType").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#partyTypeContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });

    $.ajax({
        url: "/ret/partyparamget/getPartySuboTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            $.fn.zTree.init($("#subordinationTree"), subseting, data);
        }
    });
    $("#subordination").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#subordinationContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });

    $.ajax({
        url: "/ret/partyparamget/getWorkTypeTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            $.fn.zTree.init($("#workTypeTree"), worktypesetting, data);
        }
    });
    $("#workType").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#workTypeContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });

    $("body").unbind("click").click(function () {
        $(".menuContent").hide();
    });

    $(".menuContent").unbind("click").click(function (e) {
        e.stopPropagation();
    });
    jeDate("#buildTime", {
        format: "YYYY-MM-DD",
        maxDate: getSysDate(),
        isinitVal: true
    });
});

function zTreeOnClick(event, treeId, treeNode) {
    document.getElementById("form").reset();
    $("#attach").attr("data_value", "");
    $("#levelId").attr("data-value");
    $("#partyType").attr("data-value");
    $("#workType").attr("data-value");
    $("#subordination").attr("data-value");
    $("#show_attach").html("");
    $("#unitId").empty();
    $.ajax({
        url: "/ret/partyorgget/getPartyOrgById",
        type: "post",
        dataType: "json",
        data: {
            partyOrgId: treeNode.partyOrgId
        },
        success: function (data) {
            if (data.status == 200) {
                var v = data.list;
                for (name in v) {
                    if (name == "levelId") {
                        $("#levelId").attr("data-value", v["levelId"]);
                        $.ajax({
                            url: "/ret/partyorgget/getPartyOrgById",
                            type: "post",
                            dataType: "json",
                            data: {
                                partyOrgId: v["levelId"]
                            },
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#levelId").val(res.list.partyOrgName);
                                        $("#levelId").attr("data-value", res.list.partyOrgId);
                                    } else {
                                        $("#levelId").val("");
                                    }
                                } else {
                                    console.log(data.msg);
                                }
                            }
                        });
                    } else if (name == "partyType") {
                        $("#partyType").attr("data-value", v["partyType"]);
                        $.ajax({
                            url: "/ret/partyparamget/getPartyTypeById",
                            type: "post",
                            dataType: "json",
                            data: {
                                sortId: v["partyType"]
                            },
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#partyType").val(res.list.sortName);
                                        $("#partyType").attr("data-value", res.list.sortId);
                                    } else {
                                        $("#partyType").val("");
                                    }
                                } else {
                                    console.log(data.msg);
                                }
                            }
                        });
                    } else if (name == "workType") {
                        $("#workType").attr("data-value", v["workType"]);
                        $.ajax({
                            url: "/ret/partyparamget/getPartyWorkTypeById",
                            type: "post",
                            dataType: "json",
                            data: {
                                sortId: v["workType"]
                            },
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#workType").val(res.list.sortName);
                                        $("#workType").attr("data-value", res.list.sortId);
                                    } else {
                                        $("#workType").val("");
                                    }
                                } else {
                                    console.log(data.msg);
                                }
                            }
                        });
                    } else if (name == "subordination") {
                        $("#subordination").attr("data-value", v["subordination"]);
                        $.ajax({
                            url: "/ret/partyparamget/getPartySuboById",
                            type: "post",
                            dataType: "json",
                            data: {
                                sortId: v["subordination"]
                            },
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#subordination").val(res.list.sortName);
                                        $("#subordination").attr("data-value", res.list.sortId);
                                    } else {
                                        $("#subordination").val("");
                                    }
                                } else {
                                    console.log(data.msg);
                                }
                            }
                        });
                    } else if (name == "remark") {
                        $("#remark").code(v[name]);
                    } else if (name == "attach") {
                        $("#attach").attr("data_value", v[name]);
                        createAttach("attach", 4);
                    } else if (name == "unitId") {
                        $.ajax({
                            url: "/ret/partyorgget/getPartyUnitBaseListByIds",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {
                                unitIds: v[name]
                            },
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        var recrodInfo = res.list
                                        for (var k = 0; k < recrodInfo.length; k++) {
                                            $("#unitId").append(new Option(recrodInfo[k].unitName, recrodInfo[k].unitId, true, true));//第一个参数时id,第二个参数是text
                                            $("#unitId").trigger("change");
                                        }
                                    }
                                } else if (res.status == "100") {
                                    layer.msg(sysmsg[res.msg]);
                                } else if (res.status == "500") {
                                    console.log(res.msg);
                                }
                            }
                        })

                    } else {
                        $("#" + name).val(v[name]);
                    }
                }
                $("#updatabut").unbind("click").click(function () {
                    updatePartyOrg(treeNode.partyOrgId);
                });

            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }

        }
    });
    $("#createbut").hide();
    $("#updatabut").show();
    $("#delbut").show();
}

function delPartyOrg() {
    if (confirm("确定删除当前党组织吗？")) {
        $.ajax({
            url: "/set/partyorgset/deletePartyOrg",
            type: "post",
            dataType: "json",
            data: {
                partyOrgId: $("#partyOrgId").val()
            },
            success: function (data) {
                if (data.status == 200) {
                    layer.msg(sysmsg[data.msg]);
                    location.reload();
                } else if (data.status == 100) {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    } else {
        return;
    }
}

function insertPartyOrg() {
    var unitArr = $("#unitId").val();
    var unitIds = "";
    if (unitArr != null) {
        unitIds = unitArr.join(",");
    }
    $.ajax({
        url: "/set/partyorgset/insertPartyOrg",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            orgCodeNo: $("#orgCodeNo").val(),
            partyOrgName: $("#partyOrgName").val(),
            shortName: $("#shortName").val(),
            orgType: $("#orgType").val(),
            isOnly: $("#isOnly").val(),
            secretary: $("#secretary").val(),
            levelId: $("#levelId").attr("data-value"),
            linkMan: $("#linkMan").val(),
            linkTel: $("#linkTel").val(),
            partyType: $("#partyType").attr("data-value"),
            subordination: $("#subordination").attr("data-value"),
            workType: $("#workType").attr("data-value"),
            inUnit: $("#inUnit").val(),
            buildTime: $("#buildTime").val(),
            docNum: $("#docNum").val(),
            address: $("#address").val(),
            unitId: unitIds,
            remark: $("#remark").code()
        },
        success: function (data) {
            if (data.status == 200) {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function updatePartyOrg(partyOrgId) {
    if ($("#partyOrgId").val() == $("#levelId").val()) {
        layer.msg("父级不能为本身,请重新选择！");
        return;
    }
    var unitArr = $("#unitId").val();
    var unitIds = "";
    if (unitArr != null) {
        unitIds = unitArr.join(",");
    }
    $.ajax({
        url: "/set/partyorgset/updatePartyOrg",
        type: "post",
        dataType: "json",
        data: {
            partyOrgId: partyOrgId,
            sortNo: $("#sortNo").val(),
            orgCodeNo: $("#orgCodeNo").val(),
            partyOrgName: $("#partyOrgName").val(),
            shortName: $("#shortName").val(),
            orgType: $("#orgType").val(),
            isOnly: $("#isOnly").val(),
            secretary: $("#secretary").val(),
            levelId: $("#levelId").attr("data-value"),
            linkMan: $("#linkMan").val(),
            linkTel: $("#linkTel").val(),
            partyType: $("#partyType").attr("data-value"),
            subordination: $("#subordination").attr("data-value"),
            workType: $("#workType").attr("data-value"),
            inUnit: $("#inUnit").val(),
            buildTime: $("#buildTime").val(),
            docNum: $("#docNum").val(),
            address: $("#address").val(),
            unitId: unitIds,
            remark: $("#remark").code()
        },
        success: function (data) {
            if (data.status == 200) {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}
