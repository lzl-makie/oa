$(function () {
    $.ajax({
        url: "/ret/partyunionget/getPartyUnionOrg",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "memberIds") {
                        $("#memberIds").html(getUserNameByStr(recordInfo[id]));
                    } else if (id == "chairmanFir") {
                        $("#chairmanFir").html(getUserNameByStr(recordInfo[id]));
                    } else if (id == "chairmanSec") {
                        $("#chairmanSec").html(getUserNameByStr(recordInfo[id]));
                    } else if (id == "createType") {
                        if (recordInfo[id] == "1") {
                            $("#createType").html("差额")
                        } else if (recordInfo[id] == "2") {
                            $("#createType").html("等额")
                        } else if (recordInfo[id] == "3") {
                            $("#createType").html("其他")
                        } else {
                            $("#createType").html("未知")
                        }
                    } else if (id == "linkMan") {
                        $("#linkMan").html(getUserNameByStr(recordInfo[id]));
                    } else if (id == "attach") {
                        $("#attach").attr("data_value", recordInfo.attach);
                        createAttach("attach", 1);
                    } else {
                        $("#" + id).html(recordInfo[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})
