$(function () {
    jeDate("#rewardTime", {
        format: "YYYY-MM-DD"
    });
    jeDate("#loseTime", {
        format: "YYYY-MM-DD"
    });
    $('#remark').summernote({height: 300});
    $("#createbut").unbind("click").click(function () {
        addPartyRewardRecord();
    })
})

function addPartyRewardRecord() {
    if ($("#memberId").attr("data-value") == "") {
        layer.msg("党员不能为空！");
        return;
    }
    if ($("#title").val() == "") {
        layer.msg("标题不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partymemberset/insertPartyRewardRecord",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            memberId: $("#memberId").attr("data-value"),
            title: $("#title").val(),
            otherTitle: $("#otherTitle").val(),
            rewardTime: $("#rewardTime").val(),
            honoraryLevel: $("#honoraryLevel").val(),
            unitName: $("#unitName").val(),
            rewardLevel: $("#rewardLevel").val(),
            rewardReasons: $("#rewardReasons").val(),
            isInTime: getCheckBoxValue("isInTime"),
            loseTime: $("#loseTime").val(),
            loseReasons: $("#loseReasons").val(),
            remark: $("#remark").code()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}
