$(function () {
    $.ajax({
        url: "/ret/partyunionget/getPartyUnionMsgBoxById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == 200) {
                for (var name in data.list) {
                    if (name == "attach") {
                        $("#attach").attr("data_value", data.list[name]);
                        createAttach("attach", 1);
                    } else if (name == "mailType") {
                        if (data.list[name] == "1") {
                            $("#" + name).html("部门意见");
                        } else if (data.list[name] == "2") {
                            $("#" + name).html("个人意见");
                        } else if (data.list[name] == "3") {
                            $("#" + name).html("其它意见");
                        } else {
                            $("#" + name).html("未知");
                        }
                    } else if (name == "status") {
                        if (data.list[name] == "0") {
                            $("#" + name).html("未查看");
                        } else if (data.list[name] == "1") {
                            $("#" + name).html("已查看");
                        }
                    } else {
                        $("#" + name).html(data.list[name]);
                    }
                }
                $(".js-send").unbind("click").click(function () {
                    updateLeaderMailbox(recordId);
                })
            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    })
})
