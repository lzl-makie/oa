$(function () {
    $.ajax({
        url: "/ret/partymemberget/getPartyWorkRecordById",
        type: "post",
        dataType: "json",
        data: {
            workId: workId
        },
        success: function (data) {
            if (data.status == "200") {
                var workInfo = data.list;
                for (var id in workInfo) {
                    if (id == "attach") {
                        $("#show_attach").html("");
                        $("#attach").attr("data_value", workInfo.attach);
                        createAttach("attach", 1);
                    } else if (id == "remark") {
                        $("#remark").html(workInfo[id]);
                    } else if (id == "joinUser") {
                        $.ajax({
                            url: "/ret/partymemberget/getPartyMemberById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {memberId: workInfo[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#" + id).html(res.list.userName);
                                    } else {
                                        $("#" + id).html("");
                                    }
                                }
                            }
                        });
                    } else if (id == "workType") {
                        if (workInfo[id] == "1") {
                            $("#workType").html("主题教育");
                        } else if (workInfo[id] == "2") {
                            $("#workType").html("社区活动");
                        } else if (workInfo[id] == "3") {
                            $("#workType").html("红色宣传");
                        }
                    } else {
                        $("#" + id).html(workInfo[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})
