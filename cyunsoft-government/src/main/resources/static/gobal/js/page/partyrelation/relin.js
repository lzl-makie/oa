$(function () {
    getCodeClass("nation", "nation");
    jeDate("#joinTime", {
        format: "YYYY-MM-DD",
        maxDate: getSysDate()
    });
    jeDate("#birthday", {
        format: "YYYY-MM-DD",
        maxDate: getSysDate()
    });
    jeDate("#duesEndTime", {
        format: "YYYY-MM-DD",
        maxDate: getSysDate()
    });
    jeDate("#inTime", {
        format: "YYYY-MM-DD",
        maxDate: getSysDate()
    });
    $.ajax({
        url: "/ret/partyparamget/getNativePlaceTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            var topNode = [{
                sortName: "空",
                isParent: "false",
                sortId: ""
            }];
            var newTreeNodes = topNode.concat(data);
            $.fn.zTree.init($("#nativePlaceTree"), nativePlacesetting, newTreeNodes);
        }
    });
    $('#remark').summernote({height: 300});
    $("#nativePlace").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#nativePlaceContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });
    $.ajax({
        url: "/ret/partyparamget/getGovEducationTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            var topNode = [{
                sortName: "空",
                isParent: "false",
                sortId: ""
            }];
            var newTreeNodes = topNode.concat(data);
            $.fn.zTree.init($("#educationTree"), educationsetting, newTreeNodes);
        }
    });

    $("#education").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#educationContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });

    $.ajax({
        url: "/ret/partyparamget/getDegreeTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            var topNode = [{
                sortName: "空",
                isParent: "false",
                sortId: ""
            }];
            var newTreeNodes = topNode.concat(data);
            $.fn.zTree.init($("#degreeTree"), degreesetting, newTreeNodes);
        }
    });

    $("#degree").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#degreeContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });

    $.ajax({
        url: "/ret/partyparamget/getPostTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            var topNode = [{
                sortName: "空",
                isParent: "false",
                sortId: ""
            }];
            var newTreeNodes = topNode.concat(data);
            $.fn.zTree.init($("#userPostTree"), userpostsetting, newTreeNodes);
        }
    });
    $("#userPost").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#userPostContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });
    $.ajax({
        url: "/ret/partyorgget/getPartyOrgTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            var topNode = [{
                partyOrgName: "空",
                isParent: "false",
                partyOrgId: ""
            }];
            var newTreeNodes = topNode.concat(data);
            $.fn.zTree.init($("#partyOrgIdTree"), partyOrgIdsetting, newTreeNodes);
        }
    });

    $("#partyOrgId").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#partyOrgIdContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });

    $("body").unbind("click").click(function () {
        $(".menuContent").hide();
    });
    $(".menuContent").unbind("click").click(function (e) {
        e.stopPropagation();
    });
    $("#createbut").unbind("click").click(function () {
        setRelIn();
    });
})

function setRelIn() {
    $.ajax({
        url: "/set/partyrelationset/setRelIn",
        type: "post",
        dataType: "json",
        data: {
            inType: $("#inType").val(),
            userName: $("#userName").val(),
            photo: $("#file").attr("data-value"),
            userSex: $("input[name='userSex']:checked").val(),
            birthday: $("#birthday").val(),
            nation: $("#nation").val(),
            nativePlace: $("#nativePlace").attr("data-value"),
            userType: $("#userType").val(),
            cardId: $("#cardId").val(),
            education: $("#education").attr("data-value"),
            degree: $("#degree").attr("data-value"),
            userPost: $("#userPost").attr("data-value"),
            joinTime: $("#joinTime").val(),
            oldPartyOrg: $("#oldPartyOrg").val(),
            partyOrgId: $("#partyOrgId").attr("data-value"),
            duesBase: $("#duesBase").val(),
            dues: $("#dues").val(),
            duesEndTime: $("#duesEndTime").val(),
            inTime: $("#inTime").val(),
            remark: $("#remark").code()
        },
        success: function (data) {
            if (data.status == 200) {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}


function delPhotos() {
    $("#file_img").attr("src", "/assets/img/avatars/adam-jansen.jpg");
    $("#file").attr("data-value", "");
}

var educationsetting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyparamget/getGovEducationTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("educationTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.id;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].sortName + ",";
                vid += nodes[i].sortId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#education");
            idem.attr("data-value", vid);
            idem.val(v);
        }
    }
};
var nativePlacesetting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyparamget/getNativePlaceTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("nativePlaceTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.id;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].sortName + ",";
                vid += nodes[i].sortId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#nativePlace");
            idem.attr("data-value", vid);
            idem.val(v);
        }
    }
};

var degreesetting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyparamget/getDegreeTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("degreeTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.id;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].sortName + ",";
                vid += nodes[i].sortId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#degree");
            idem.attr("data-value", vid);
            idem.val(v);
        }
    }
};
var userpostsetting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyparamget/getPostTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("userPostTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.id;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].sortName + ",";
                vid += nodes[i].sortId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#userPost");
            idem.attr("data-value", vid);
            idem.val(v);
        }
    }
};

var partyOrgIdsetting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyorgget/getPartyOrgTree",// Ajax 获取数据的 URL 地址
        autoParam: ["partyOrgId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        // 禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "partyOrgId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "partyOrgName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("partyOrgIdTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.partyOrgId;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].partyOrgName + ",";
                vid += nodes[i].partyOrgId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#partyOrgId");
            idem.val(v);
            idem.attr("data-value", vid);
        }
    }
};
