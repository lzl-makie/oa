$(function () {
    jeDate("#needTime", {
        format: "YYYY-MM-DD",
        minDate: getSysDate(),
        isinitVal: true
    });
    $('#remark').summernote({height: 300});
    getCodeClass("fundsType", "gov_funds_type");
    $(".js-add-save").unbind("click").click(function () {
        addFunds();
    })
})

function addFunds() {
    if($("#title").val()=="")
    {
        layer.msg("标题不能为空！")
        return;
    }
    $.ajax({
        url: "/set/partyset/insertPartyFunds",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("sortNo").val(),
            fundsType: $("#fundsType").val(),
            title: $("#title").val(),
            applyUser: $("#applyUser").attr("data-value"),
            needTime: $("#needTime").val(),
            funds: $("#funds").val(),
            approvalUser: $("#approvalUser").attr("data-value"),
            remark: $("#remark").code()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}
