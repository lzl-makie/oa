$(function () {
    jeDate("#beginTimeQuery", {
        format: "YYYY-MM-DD hh:mm:ss"
    });
    jeDate("#endTimeQuery", {
        format: "YYYY-MM-DD hh:mm:ss"
    });
    $('#remark').summernote({height: 300});
    query();
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })
    jeDate("#endTime", {
        format: "YYYY-MM-DD",
    });
    jeDate("#beginTime", {
        format: "YYYY-MM-DD"
    });

})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/partymemberget/getPartyWorkRecordList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',// 工具列
        striped: true,// 隔行换色
        cache: false,// 禁用缓存
        pagination: true,// 启动分页
        sidePagination: 'server',// 分页方式
        pageNumber: 1,// 初始化table时显示的页码
        pageSize: 10,// 每页条目
        showFooter: false,// 是否显示列脚
        showPaginationSwitch: true,// 是否显示 数据条数选择框
        sortable: true,// 排序
        search: true,// 启用搜索
        sortOrder: "asc",
        showColumns: true,// 是否显示 内容列下拉框
        showRefresh: true,// 显示刷新按钮
        idField: 'workId',// key值栏位
        clickToSelect: true,// 点击选中checkbox
        pageList: [10, 20, 30, 50],// 可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',// 标题 可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'title',
            title: '党员活动标题',
            width: '150px',
            formatter:function (value,row,index)
            {
                return "<a href=\"javascript:void(0);details('" + row.workId + "')\" style='cursor: pointer'>"+value+"</a>";
            }
        }, {
            field: 'workType',
            width: '100px',
            title: '活动类型',
            formatter: function (value, row, index) {
                if (value == "1") {
                    return "主题教育";
                } else if (value == "2") {
                    return "社区活动";
                } else if (value == "3") {
                    return "红色宣传";
                }
            }
        }, {
            field: 'sponsor',
            width: '150px',
            title: '活动地点'
        }, {
            field: 'beginTime',
            width: '100px',
            title: '开始时间'
        }, {
            field: 'endTime',
            width: '100px',
            title: '截止时间'
        }, {
            field: 'createTime',
            title: '创建时间',
            width: '100px'
        }, {
            field: 'createUserName',
            title: '创建人',
            width: '100px'
        }, {
            field: 'opt',
            title: '操作',
            align: 'center',
            width: '120px',
            formatter: function (value, row, index) {
                return createOptBtn(row.workId);
            }
        }],
        onClickCell: function (field, value, row, $element) {
            // alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, // 总页数,前面的key必须为"total"
                    rows: res.list.list
                    // 行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        workType: $("#workTypeQuery").val(),
        endTime: $("#endTimeQuery").val(),
        beginTime: $("#beginTimeQuery").val()
    };
    return temp;
};

function createOptBtn(workId) {
    var html = "<a href=\"javascript:void(0);edit('" + workId + "')\" class=\"btn btn-primary btn-xs\">编辑</a>&nbsp;&nbsp;" +
        "<a href=\"javascript:void(0);deleteWork('" + workId + "')\" class=\"btn btn-darkorange btn-xs\" >删除</a>";
    return html;
}

function deleteWork(workId) {
    if (confirm("确定删除当前活动吗？")) {
        $.ajax({
            url: "/set/partymemberset/deletePartyWorkRecord",
            type: "post",
            dataType: "json",
            data: {
                workId: workId
            },
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable("refresh");
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    }
}

function edit(workId) {
    $("#worklistdiv").hide();
    $("#workdiv").show();
    $("#joinUser").attr("data-value", "");
    $("#show_attach").empty();
    $("#attach").attr("data_value", "");
    $(".js-back-btn").unbind("click").click(function () {
        goback();
    })
    $.ajax({
        url: "/ret/partymemberget/getPartyWorkRecordById",
        type: "post",
        dataType: "json",
        data: {
            workId: workId
        },
        success: function (data) {
            if (data.status == "200") {
                var workInfo = data.list;
                for (var id in workInfo) {
                    if (id == "attach") {
                        $("#show_attach").html("");
                        $("#attach").attr("data_value", workInfo.attach);
                        createAttach("attach", 4);
                    } else if (id == "remark") {
                        $("#remark").code(workInfo[id]);
                    } else if (id == "joinUser") {
                        $("#joinUser").attr("data-value", workInfo[id]);
                        $.ajax({
                            url: "/ret/partymemberget/getPartyMemberById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {memberId: workInfo[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#" + id).val(res.list.userName);
                                    } else {
                                        $("#" + id).val("");
                                    }
                                }
                            }
                        });
                    } else {
                        $("#" + id).val(workInfo[id]);
                    }
                }
                $(".js-update-save").unbind("click").click(function () {
                    updatePartyWork(workId);
                })
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function details(workId) {
    window.open("/app/core/party/partyworkdetails?workId=" + workId);
}

function updatePartyWork(workId) {
    if($("#title").val()=="")
    {
        layer.msg("标题不能为空");
        return;
    }
    $.ajax({
        url: "/set/partymemberset/updatePartyWorkRecord",
        type: "post",
        dataType: "json",
        data: {
            workId: workId,
            title: $("#title").val(),
            joinUser: $("#joinUser").attr("data-value"),
            sponsor: $("#sponsor").val(),
            beginTime: $("#beginTime").val(),
            endTime: $("#endTime").val(),
            workType: $("#workType").val(),
            address: $("#address").val(),
            attach: $("#attach").attr("data_value"),
            remark: $("#remark").code(),
            msgType: getCheckBoxValue("msgType")
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function goback() {
    $("#workdiv").hide();
    $("#worklistdiv").show();
}
