$(function () {
    jeDate("#beginTime", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTime", {
        format: "YYYY-MM-DD"
    });
    $('#remark').summernote({height: 200});
    $(".js-setcomm").unbind("click").click(function () {
        $("#setcommmodal").modal("show");
        $(".js-setcommmodal").unbind("click").click(function () {
            insertPartyUnionOrg();
        })
    })
    getPartyUnionOrg();
    $(".js-appointment").unbind("click").click(function () {
        var unionOrgId = $("#unionOrgId").val();
        if (unionOrgId == "") {
            layer.msg("当前届期信息不存在，不可任命！")
            return;
        } else {
            appointment();
            $("#setappointment").modal("show");
            $(".js-appointmentmodal").unbind("click").click(function () {
                $.ajax({
                    url: "/set/partyunionset/updatePartyUnionOrg",
                    type: "post",
                    dataType: "json",
                    data: {
                        unionOrgId: unionOrgId,
                        memberIds: $("#appMemberIds1").attr("data-value"),
                        chairmanFir: $("#chairmanFir1").attr("data-value"),
                        chairmanSec: $("#chairmanSec1").attr("data-value")
                    },
                    success: function (data) {
                        if (data.status == "500") {
                            console.log(data.msg);
                        } else if (data.status == "100") {
                            layer.msg(sysmsg[data.msg]);
                        } else {
                            layer.msg(sysmsg[data.msg]);
                            $("#setappointment").modal("hide");
                        }
                    }
                })
            })
        }
    })
})

function appointment() {
    $.ajax({
        url: "/ret/partyunionget/getPartyUnionOrg",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "memberIds") {
                        $("#memberIds1").attr("data-value", recordInfo[id]);
                        $("#memberIds1").val(getUserNameByStr(recordInfo[id]));
                    } else if (id == "chairmanFir") {
                        $("#chairmanFir1").attr("data-value", recordInfo[id]);
                        $("#chairmanFir1").val(getUserNameByStr(recordInfo[id]));
                    } else if (id == "chairmanSec") {
                        $("#chairmanSec1").attr("data-value", recordInfo[id]);
                        $("#chairmanSec1").val(getUserNameByStr(recordInfo[id]));
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function getPartyUnionOrg() {
    $.ajax({
        url: "/ret/partyunionget/getPartyUnionOrg",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                let recordInfo = data.list;
                for (let id in recordInfo) {
                    if (id == "memberIds") {
                        $("#spanMemberIds").html(getUserNameByStr(recordInfo[id]));
                    } else if (id == "chairmanFir") {
                        $("#spanChairmanFir").html(getUserNameByStr(recordInfo[id]));
                    } else if (id == "chairmanSec") {
                        $("#spanChairmanSec").html(getUserNameByStr(recordInfo[id]));
                    } else if (id == "beginTime") {
                        $("#spanBeinTime").html(recordInfo.beginTime + "&nbsp;&nbsp;&nbsp;&nbsp;至&nbsp;&nbsp;&nbsp;&nbsp;" + recordInfo.endTime)
                    } else if (id == "deadLine") {
                        $("#spanDeadLine").html(recordInfo[id]);
                    } else if (id == "unionOrgId") {
                        $("#unionOrgId").val(recordInfo.unionOrgId)
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function insertPartyUnionOrg() {
    $.ajax({
        url: "/set/partyunionset/insertPartyUnionOrg",
        type: "post",
        dataType: "json",
        data: {
            deadline: $("#deadline").val(),
            createType: $("#createType").val(),
            beginTime: $("#beginTime").val(),
            endTime: $("#endTime").val(),
            chairmanFir: $("#chairmanFir").attr("data-value"),
            chairmanSec: $("#chairmanSec").attr("data-value"),
            memberIds: $("#memberIds").attr("data-value"),
            linkMan: $("#linkMan").attr("data-value"),
            linkTel: $("#linkTel").val(),
            address: $("#address").val(),
            attach: $("#attach").attr("data_value"),
            remark: $("#remark").code()
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                layer.msg(sysmsg[data.msg]);
                $("#setcommmodal").modal("hide");
            }
        }
    })
}
