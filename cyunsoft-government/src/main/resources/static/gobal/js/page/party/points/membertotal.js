var zTree;
var setting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyorgget/getPartyOrgAllParentTree",// Ajax 获取数据的 URL 地址
        autoParam: ["partyOrgId"],// 异步加载时需要自动提交父节点属性的参数
    },
    callback: {
        onExpand: function (event, treeId, treeNode) {
            var partyOrgId = treeNode.partyOrgId;
            if (treeNode.isParent) {
                $.ajax({
                    url: "/ret/partymemberget/getSelectPartyMemberByPartyId",
                    type: "post",
                    data: {
                        partyOrgId: partyOrgId
                    },
                    dataType: "json",
                    success: function (data) {
                        var appNode = [];
                        if (data.list.length > 0) {
                            for (var i = 0; i < data.list.length; i++) {
                                var newnode = {};
                                newnode.partyOrgId = data.list[i].memberId;
                                newnode.partyOrgName = data.list[i].userName;
                                newnode.isParent = false;
                                if (data.list[i].userSex == '男') {
                                    newnode.icon = '/gobal/img/org/U01.png';
                                } else if (data.list[i].userSex == '女') {
                                    newnode.icon = '/gobal/img/org/U11.png';
                                }
                                appNode.push(newnode);
                            }
                            zTree.reAsyncChildNodes(treeNode, "refresh");
                            zTree.addNodes(treeNode, appNode);
                        }
                    }
                });
            }
        },
        onClick: zTreeOnClick
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "partyOrgId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "partyOrgName"
        }
    }
};
$(function () {
    jeDate("#pointsYearQuery", {
        format: "YYYY"
    });
    var topNode = [{
        partyOrgName: orgName,
        levelId: '',
        isParent: "true",
        partyOrgId: "0",
        icon: "/gobal/img/org/org.png"
    }];

    zTree = $.fn.zTree.init($("#tree"), setting, topNode);// 初始化树节点时，添加同步获取的数据
    var nodes = zTree.getNodes();
    for (var i = 0; i < nodes.length; i++) {
        zTree.expandNode(nodes[i], true, false, false);//默认展开第一级节点
    }
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })
    query("")
    $("#cquery").unbind("click").click(function () {
        $("#myTable").bootstrapTable('destroy');
        query("");
    })
})

function zTreeOnClick(event, treeId, treeNode) {
    if (treeNode.isParent == false) {
        $("#myTable").bootstrapTable('destroy');
        if (treeNode.partyOrgId == "0") {
            query("");
        } else {
            query(treeNode.partyOrgId);
        }
    }
}


function query(memberId) {
    $("#myTable").bootstrapTable({
        url: '/ret/partymemberget/getMemberPointsTotalList?memberId=' + memberId,
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: false,//是否显示 数据条数选择框
        sortable: true,//排序
        search: false,//启用搜索
        showColumns: false,//是否显示 内容列下拉框
        showRefresh: false,//显示刷新按钮
        idField: 'recordId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',//标题  可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'partyOrgName',
            width: '100px',
            title: '组织名称'
        }, {
            field: 'userName',
            width: '100px',
            title: '党员姓名'
        },
            {
                field: 'pointsYear',
                title: '年度',
                width: '100px',
                formatter: function (value, row, index) {
                    return "【" + value + "】年度"
                }
            }, {
                field: 'points',
                width: '100px',
                title: '年度积分',
                formatter: function (value, row, index) {
                    return value + "分"
                }
            }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list
                    //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

// 创建表格行操作按钮
function createOptBtn(memberId) {
    var html = "<a href=\"javascript:void(0);details('" + memberId + "')\" class=\"btn btn-primary btn-xs\">明细</a>";
    return html;
}

// 查询参数
function queryParams(params) {
    var temp = {
        search: "",
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        deleted: 0,
        pointsYear: $("#pointsYearQuery").val()
    };
    return temp;
}
