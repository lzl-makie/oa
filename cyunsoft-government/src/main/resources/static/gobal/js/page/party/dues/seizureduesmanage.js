$(function () {
    query();
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })
    jeDate("#yearQuery", {
        format: "YYYY"
    })
})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/partyget/getPartyDeusList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',// 工具列
        striped: true,// 隔行换色
        cache: false,// 禁用缓存
        pagination: true,// 启动分页
        sidePagination: 'server',// 分页方式
        pageNumber: 1,// 初始化table时显示的页码
        pageSize: 10,// 每页条目
        showFooter: false,// 是否显示列脚
        showPaginationSwitch: true,// 是否显示 数据条数选择框
        sortable: true,// 排序
        search: true,// 启用搜索
        sortOrder: "asc",
        showColumns: true,// 是否显示 内容列下拉框
        showRefresh: true,// 显示刷新按钮
        idField: 'recordId',// key值栏位
        clickToSelect: true,// 点击选中checkbox
        pageList: [10, 20, 30, 50],// 可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',// 标题 可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'userName',
            title: '党员姓名',
            width: '50'
        }, {
            field: 'year',
            title: '年度',
            width: '100',
            formatter: function (value, row, index) {
                return "【" + value + "年度】";
            }
        }, {
            field: 'month',
            title: '月份',
            width: '100',
            formatter: function (value, row, index) {
                return "【" + value + "月】";
            }
        }, {
            field: 'duesType',
            width: '100px',
            title: '党费类型',
            formatter: function (value, row, index) {
                if (value == "1") {
                    return "个体经营党员党费";
                } else if (value == "2") {
                    return "离退休党员党费";
                } else if (value == "3") {
                    return "农民党员党费";
                } else if (value == "4") {
                    return "学生党员党费";
                } else if (value == "5") {
                    return "下岗失业党员党费";
                } else if (value == "6") {
                    return "生活有困难党员党费";
                } else if (value == "7") {
                    return "预备党员党费";
                } else if (value == "8") {
                    return "流动党员党费";
                } else if (value == "9") {
                    return "自愿多交党费";
                } else if (value == "0") {
                    return "其它";
                }
            }
        }, {
            field: 'dues',
            title: '应缴金额',
            width: '50px'
        }, {
            field: 'realDues',
            title: '实缴金额',
            width: '50px'
        }, {
            field: 'onTime',
            title: '是否按时',
            width: '50px',
            formatter: function (value, row, index) {
                if (value == "0") {
                    return "拖延";
                } else if (value == "1") {
                    return "按时";
                }
            }
        }, {
            field: 'duesTime',
            width: '100px',
            title: '缴费日期'
        }, {
            field: 'remark',
            title: '备注',
            width: '200px'
        }, {
            field: 'opt',
            title: '操作',
            align: 'center',
            width: '100px',
            formatter: function (value, row, index) {
                return createOptBtn(row.recordId);
            }
        }],
        onClickCell: function (field, value, row, $element) {
            // alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, // 总页数,前面的key必须为"total"
                    rows: res.list.list
                    // 行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        duesType: $("#duesTypeQuery").val(),
        year: $("#yearQuery").val(),
        onTime: $("#onTimeQuery").val()
    };
    return temp;
};

function createOptBtn(recordId) {
    var html = "<a href=\"javascript:void(0);deleteDues('" + recordId + "')\" class=\"btn btn-darkorange btn-xs\" >删除</a>&nbsp;&nbsp;";
    html += "<a href=\"javascript:void(0);details('" + recordId + "')\" class=\"btn btn-sky btn-xs\" >详情</a>";
    return html;
}

function details(recordId) {
    window.open("/app/core/party/seizureduesdetails?recordId=" + recordId);
}

function deleteDues(recordId) {
    if (confirm("确定删除当前缴费记录吗？")) {
        $.ajax({
            url: "/set/partyset/deletePartyDues",
            type: "post",
            dataType: "json",
            data: {
                recordId: recordId
            },
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable("refresh");
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    }
}

