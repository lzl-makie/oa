$(function () {
    getCodeClass("suggestionsType", "party_suggestions");
    $("#createbut").unbind("click").click(function () {
        addSuggestionsRecord();
    })
    $('#content').summernote({height: 300});
    jeDate("#contributorsTime", {
        format: "YYYY-MM-DD"
    });

})

function addSuggestionsRecord() {
    if($("#title").val()=="")
    {
        layer.msg("标题不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partyset/insertPartyCleanSuggestions",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            title: $("#title").val(),
            suggestionsType: $("#suggestionsType").val(),
            memberId: $("#memberId").attr("data-value"),
            contributorsTime: $("#contributorsTime").val(),
            content: $("#content").code(),
            attach: $("#attach").attr("data_value")
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                window.location.reload();
                layer.msg(sysmsg[data.msg]);
            }
        }
    })
}
