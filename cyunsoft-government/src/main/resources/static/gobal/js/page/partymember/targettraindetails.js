$(function () {
    $.ajax({
        url: "/ret/partyget/getPartyTargetTrainById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "jobStatus") {
                        if (recordInfo[id] == "1") {
                            $("#" + id).html("全脱产（离岗）");
                        } else if (recordInfo[id] == "2") {
                            $("#" + id).html("半脱产（半离岗）");
                        } else if (recordInfo[id] == "3") {
                            $("#" + id).html("不脱产（不离岗）");
                        } else {
                            $("#" + id).html("未知");
                        }
                    } else if (id == "joinRecordId") {
                        $.ajax({
                            url: "/ret/partyget/getPartyJoinById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {recordId: recordInfo[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#" + id).html(res.list.userName);
                                    } else {
                                        $("#" + id).html("");
                                    }
                                }
                            }
                        });
                    } else if (id == "partyOrgId") {
                        $("#" + id).attr("data-value", recordInfo[id]);
                        $.ajax({
                            url: "/ret/partyget/getPartyOrgById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {partyOrgId: recordInfo[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#" + id).html(res.list.partyOrgName);
                                    } else {
                                        $("#" + id).html("");
                                    }
                                }
                            }
                        });
                    } else {
                        $("#" + id).html(recordInfo[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})
