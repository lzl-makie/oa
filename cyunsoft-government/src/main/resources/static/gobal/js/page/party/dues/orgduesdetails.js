$(function () {
    $.ajax({
        url: "/ret/partyorgget/getPartyDuesOrgById",
        type: "post",
        dataType: "json",
        data: {recordId: recordId},
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "remark") {
                        $("#remark").html(recordInfo[id]);
                    } else if (id == "onTime") {
                        if (recordInfo[id] == "1") {
                            $("#" + id).html("缴纳");
                        } else if (recordInfo[id] == "2") {
                            $("#" + id).html("未缴");
                        } else if (recordInfo[id] == "0") {
                            $("#" + id).html("补缴");
                        } else {
                            $("#" + id).html("未知");
                        }
                    } else if (id == "partyOrgId") {
                        $.ajax({
                            url: "/ret/partyorgget/getPartyOrgById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {partyOrgId: recordInfo[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#" + id).html(res.list.partyOrgName);
                                    } else {
                                        $("#" + id).html("");
                                    }
                                }
                            }
                        });
                    } else if (id == "year") {
                        $("#" + id).html(recordInfo[id] + "年度");
                    } else if (id == "month") {
                        $("#" + id).html(recordInfo[id] + "月份");
                    } else {
                        $("#" + id).html(recordInfo[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})
