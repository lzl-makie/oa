$(function () {
    jeDate("#beginTimeQuery", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTimeQuery", {
        format: "YYYY-MM-DD"
    });
    $('#content').summernote({height: 200});
    $('#resContent').summernote({height: 200});
    query();
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })
    getCodeClass("mediateType", "partyunion_mediate");
    getCodeClass("mediateTypeQuery", "partyunion_mediate");
})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/partyunionget/getPartyUnionMediateList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        search: true,//启用搜索
        sortOrder: "asc",
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'recordId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',//标题  可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'title',
            width: '100px',
            title: '调解标题',
            formatter:function (value,row,index)
            {
                return "<a href=\"javascript:void(0);details('" + row.recordId + "')\" style='cursor: pointer'>"+value+"</a>";
            }
        },{
            field: 'partyUser',
            width: '100px',
            title: '被调解方'
        },  {
            field: 'mediateType',
            width: '100px',
            title: '调解类型',
            formatter: function (value, row, index) {
                return getCodeClassName(value, 'partyunion_mediate');
            }
        }, {
            field: 'mediateTime',
            width: '100px',
            title: '调解时间'
        }, {
            field: 'partyUser',
            title: '调解人',
            width: '100px'
        }, {
            field: 'mediateModel',
            width: '100px',
            title: '调解方式',
            formatter: function (value, row, index) {
                if (value == "1") {
                    return "约谈";
                } else if (value == "2") {
                    return "双方协商";
                } else if (value == "3") {
                    return "法院调解";
                } else if (value == "4") {
                    return "人民调解";
                } else if (value == "5") {
                    return "行政调解";
                } else if (value == "6") {
                    return "仲裁调解";
                }
            }
        }, {
            field: 'createUserName',
            width: '100px',
            title: '创建时间'
        }, {
            field: 'opt',
            title: '操作',
            align: 'center',
            width: '120px',
            formatter: function (value, row, index) {
                return createOptBtn(row.recordId);
            }
        }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list
                    //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        mediateType: $("#mediateTypeQuery").val(),
        endTime: $("#endTimeQuery").val(),
        beginTime: $("#beginTimeQuery").val()
    };
    return temp;
};

function createOptBtn(recordId) {
    var html = "<a href=\"javascript:void(0);edit('" + recordId + "')\" class=\"btn btn-primary btn-xs\">编辑</a>&nbsp;&nbsp;"
        + "<a href=\"javascript:void(0);deleteRecord('" + recordId + "')\" class=\"btn btn-darkorange btn-xs\" >删除</a>";
    return html;
}

function deleteRecord(recordId) {
    if (confirm("确定删除当前记录吗？")) {
        $.ajax({
            url: "/set/partyunitonset/deletePartyUnionMediate",
            type: "post",
            dataType: "json",
            data: {recordId: recordId},
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable("refresh");
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    }
}

function edit(recordId) {
    $("#content").code("");
    $("#resContent").code("");
    $("#baseinfolistdiv").hide();
    $("#baseinfodiv").show();
    $(".js-back-btn").unbind("click").click(function () {
        goback();
    })
    document.getElementById("form1").reset();
    $("#attach").attr("data_value", "");
    $("#content").code("");
    $("#resContent").code("");
    $.ajax({
        url: "/ret/partyunionget/getPartyUnionMediateById",
        type: "post",
        dataType: "json",
        data: {recordId: recordId},
        success: function (data) {
            if (data.status == "200") {
                var info = data.list;
                for (var id in info) {
                    if (id == "attach") {
                        $("#show_attach").html("");
                        $("#attach").attr("data_value", info.attach);
                        createAttach("attach", 4);
                    } else if (id == "content") {
                        $("#content").code(info[id]);
                    } else if (id == "resContent") {
                        $("#resContent").code(info[id]);
                    } else {
                        $("#" + id).val(info[id]);
                    }
                }
                $(".js-update-save").unbind("click").click(function () {
                    updatePartyUnionMediate(recordId);
                })
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function details(recordId) {
    window.open("/app/core/partyunion/mediatedetails?recordId=" + recordId);
}

function updatePartyUnionMediate(recordId) {
    if($("#title").val()=="")
    {
        layer.msg("标题不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partyunionset/updatePartyUnionMediate",
        type: "post",
        dataType: "json",
        data: {
            recordId:recordId,
            title: $("#title").val(),
            mediateType: $("#mediateType").val(),
            mediateTime: $("#mediateTime").val(),
            partyUser: $("#partyUser").val(),
            mediateUser: $("#mediateUser").val(),
            content: $("#content").code(),
            mediateModel: $("#mediateModel").val(),
            address: $("#address").val(),
            resContent: $("#resContent").code(),
            attach: $("#attach").attr("data_value")
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                $("#baseinfodiv").hide();
                $("#baseinfolistdiv").show();
                $("#myTable").bootstrapTable("refresh");
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function goback() {
    $("#baseinfodiv").hide();
    $("#baseinfolistdiv").show();
}
