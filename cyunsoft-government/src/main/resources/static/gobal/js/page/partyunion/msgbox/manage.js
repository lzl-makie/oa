$(function () {
    query();
})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/partyunionget/getLeaderMsgBoxList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        sortOrder: "desc",
        search: true,//启用搜索
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'recordId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',//标题  可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'title',
            title: '标题',
            width: '200px',
            formatter:function (value,row,index)
            {
                return "<a href=\"javascript:void(0);readdetails('" + row.recordId + "')\" style='cursor: pointer'>"+value+"</a>";
            }
        }, {
            field: 'mailType',
            width: '50px',
            title: '意见类型',
            formatter: function (value, row, index) {
                if (value == "1") {
                    return "部门意见";
                } else if (value == "2") {
                    return "个人意见";
                } else if (value == "3") {
                    return "其它意见";
                } else {
                    return "未知";
                }

            }
        }, {
            field: 'status',
            title: '状态',
            width: '50px',
            align: 'center',
            formatter: function (value, row, index) {
                if (value == "0") {
                    return "<a href=\"javascript:void(0);\" class=\"btn btn-magenta shiny btn-xs\">未查看</a>";
                } else if (value == "1") {
                    return "<a href=\"javascript:void(0);\" class=\"btn btn-success shiny btn-xs\">已查看</a>";
                }

            }
        }, {
            field: 'userName',
            title: '反馈人',
            width: '100px',
            formatter: function (value, row, index) {
                if (value) {
                    return value;
                } else {
                    return "匿名";
                }

            }
        }, {
            field: 'createTime',
            width: '100px',
            title: '创建时间'
        }, {
            field: 'remark',
            width: '250px',
            title: '内容'
        }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list
                    //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order
    };
    return temp;
};


function readdetails(recordId) {
    window.open("/app/core/partyunion/msgboxdetails?flag=read&recordId=" + recordId)
}
