$(function () {
    $.ajax({
        url: "/ret/partyget/getPartyDuesById",
        type: "post",
        dataType: "json",
        data: {recordId: recordId},
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "memberId") {
                        $.ajax({
                            url: "/ret/partymemberget/getPartyMemberById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {memberId: recordInfo[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#" + id).html(res.list.userName);
                                    } else {
                                        $("#" + id).html("");
                                    }
                                }
                            }
                        });
                    } else if (id == "remark") {
                        $("#remark").html(recordInfo[id]);
                    } else if (id == "duesType") {
                        if (recordInfo[id] == "1") {
                            $("#" + id).html("个体经营党员党费");
                        } else if (recordInfo[id] == "2") {
                            $("#" + id).html("离退休党员党费");
                        } else if (recordInfo[id] == "3") {
                            $("#" + id).html("农民党员党费");
                        } else if (recordInfo[id] == "4") {
                            $("#" + id).html("学生党员党费");
                        } else if (recordInfo[id] == "5") {
                            $("#" + id).html("下岗失业党员党费");
                        } else if (recordInfo[id] == "6") {
                            $("#" + id).html("生活有困难党员党费");
                        } else if (recordInfo[id] == "7") {
                            $("#" + id).html("预备党员党费");
                        } else if (recordInfo[id] == "8") {
                            $("#" + id).html("流动党员党费");
                        } else if (recordInfo[id] == "9") {
                            $("#" + id).html("自愿多交党费");
                        } else if (recordInfo[id] == "0") {
                            $("#" + id).html("其它");
                        }
                    } else if (id == "onTime") {
                        if (recordInfo[id] == "1") {
                            $("#" + id).html("按时");
                        } else if (recordInfo[id] == "2") {
                            $("#" + id).html("拖延");
                        }
                    } else {
                        $("#" + id).html(recordInfo[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})
