$(function () {
    $('#remark').summernote({height: 200});
    $(".js-add-save").unbind("click").click(function () {
        insertPartyUnionHelp();
    })
})

function insertPartyUnionHelp() {
    if($("#title").val()=="")
    {
        layer.msg("标题不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partyunionset/insertPartyUnionHelp",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            helpType: $("#helpType").val(),
            title: $("#title").val(),
            userName: $("#userName").val(),
            accountId: $("#accountId").attr("data-value"),
            deptId: $("#deptId").attr("data-value"),
            job: $("#job").val(),
            income: $("#income").val(),
            familyIncome: $("#familyIncome").val(),
            loverName: $("#loverName").val(),
            workOrgName: $("#workOrgName").val(),
            homeTel: $("#homeTel").val(),
            address: $("#address").val(),
            userCount: $("#userCount").val(),
            familyPay: $("#familyPay").val(),
            remark: $("#remark").code(),
            attach: $("#attach").attr("data_value")
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                window.location.reload();
                layer.msg(sysmsg[data.msg]);
            }
        }
    })
}
