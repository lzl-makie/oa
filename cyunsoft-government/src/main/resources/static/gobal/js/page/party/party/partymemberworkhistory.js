$(function () {
    jeDate("#beginTimeQuery", {
        format: "YYYY-MM-DD hh:mm:ss"
    });
    jeDate("#endTimeQuery", {
        format: "YYYY-MM-DD hh:mm:ss"
    });
    query();
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })

})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/partymemberget/getHistoryPartyWorkRecordList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',// 工具列
        striped: true,// 隔行换色
        cache: false,// 禁用缓存
        pagination: true,// 启动分页
        sidePagination: 'server',// 分页方式
        pageNumber: 1,// 初始化table时显示的页码
        pageSize: 10,// 每页条目
        showFooter: false,// 是否显示列脚
        showPaginationSwitch: true,// 是否显示 数据条数选择框
        sortable: true,// 排序
        search: true,// 启用搜索
        sortOrder: "asc",
        showColumns: true,// 是否显示 内容列下拉框
        showRefresh: true,// 显示刷新按钮
        idField: 'workId',// key值栏位
        clickToSelect: true,// 点击选中checkbox
        pageList: [10, 20, 30, 50],// 可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',// 标题 可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'title',
            title: '党员活动标题',
            width: '150px',
            formatter:function (value,row,index)
            {
                return "<a href=\"javascript:void(0);details('" + row.workId + "')\" style='cursor: pointer' >"+value+"</a>";
            }
        }, {
            field: 'workType',
            width: '100px',
            title: '活动类型',
            formatter: function (value, row, index) {
                if (value == "1") {
                    return "主题教育";
                } else if (value == "2") {
                    return "社区活动";
                } else if (value == "3") {
                    return "红色宣传";
                }
            }
        }, {
            field: 'sponsor',
            width: '150px',
            title: '活动地点'
        }, {
            field: 'beginTime',
            width: '100px',
            title: '开始时间'
        }, {
            field: 'endTime',
            width: '100px',
            title: '截止时间'
        }, {
            field: 'createTime',
            title: '创建时间',
            width: '100px'
        }, {
            field: 'createUserName',
            title: '创建人',
            width: '100px'
        }],
        onClickCell: function (field, value, row, $element) {
            // alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, // 总页数,前面的key必须为"total"
                    rows: res.list.list
                    // 行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        workType: $("#workTypeQuery").val(),
        endTime: $("#endTimeQuery").val(),
        beginTime: $("#beginTimeQuery").val()
    };
    return temp;
};

function details(workId) {
    window.open("/app/core/partymember/partyworkdetails?workId=" + workId);
}
