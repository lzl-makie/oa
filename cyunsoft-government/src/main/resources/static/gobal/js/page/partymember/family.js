$(function () {
    jeDate("#birthday", {
        format: "YYYY-MM-DD"
    });
    $("#createbut").unbind("click").click(function () {
        insertFamily();
    })
})

function insertFamily() {
    if($("#memberId").attr("data-value")==""&&$("#userName").val()=="")
    {
        layer.msg("党员不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partymemberset/insertPartyFamilyRecord",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            memberId: $("#memberId").attr("data-value"),
            userName: $("#userName").val(),
            userSex: $("input[name='userSex']:checked").val(),
            tel: $("#tel").val(),
            relation: $("#relation").val(),
            birthday: $("#birthday").val(),
            positionStatus: $("#positionStatus").val(),
            workUnit: $("#workUnit").val(),
            userPost: $("#userPost").val(),
            workAddress: $("#workAddress").val(),
            homeAddress: $("#homeAddress").val(),
            remark: $("#remark").val()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}
