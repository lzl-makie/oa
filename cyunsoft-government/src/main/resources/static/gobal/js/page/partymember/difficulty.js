$(function () {
    getCodeClass("difType", "party_dif_type");
    jeDate("#difTime", {
        format: "YYYY-MM-DD",
        maxDate: getSysDate(),
        isinitVal: true
    });
    $('#reason').summernote({height: 300});
    $(".js-add-save").unbind("click").click(function () {
        addDifficultyRecord();
    })
})

function addDifficultyRecord() {
    if ($("#memberId").attr("data-value") == "") {
        layer.msg("党员不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partymemberset/insertPartyMemberDifficulty",
        type: "post",
        dataType: "json",
        data: {
            memberId: $("#memberId").attr("data-value"),
            difType: $("#difType").val(),
            difTime: $("#difTime").val(),
            remark: $("#remark").val(),
            attach: $("#attach").attr("data_value"),
            reason: $("#reason").code()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}
