$(function () {
    jeDate("#beginTime", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTime", {
        format: "YYYY-MM-DD"
    });
    getCodeClass("trainType", "partyunion_train");
    $('#remark').summernote({height: 200});
    $(".js-add-save").unbind("click").click(function () {
        insertPartyUnionTraining();
    })
})

function insertPartyUnionTraining() {
    if($("#title").val()=="")
    {
        layer.msg("标题不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partyunionset/insertPartyUnionTraining",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            trainType: $("#trainType").val(),
            title: $("#title").val(),
            beginTime: $("#beginTime").val(),
            endTime: $("#endTime").val(),
            memberIds: $("#memberIds").attr("data-value"),
            sponsor: $("#sponsor").val(),
            address: $("#address").val(),
            linkMan: $("#linkMan").val(),
            linkTel: $("#linkTel").val(),
            remark: $("#remark").code(),
            attach: $("#attach").attr("data_value")
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                window.location.reload();
                layer.msg(sysmsg[data.msg]);
            }
        }
    })
}
