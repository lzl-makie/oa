$(function () {
    $.ajax({
        url: "/ret/partyorgget/getPartyPrimaryMeetById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", recordInfo.attach);
                        createAttach("attach", 1);
                    } else if (id == "joinMember") {
                        $("#" + id).html(getPartyMemberName(recordInfo.joinMember));
                    } else if (id == "meetType") {
                        if (recordInfo[id] == "1") {
                            $("#" + id).html("重大活劢(纨念日)重温入党誓词。讥全体党员丌忘初心、牢记使命");
                        } else if (recordInfo[id] == "2") {
                            $("#" + id).html("相关有演出比赛，特别是体现“党”味的活劢，讥更多的党员同志参不迚行");
                        } else if (recordInfo[id] == "3") {
                            $("#" + id).html("走访慰问老同志、老党员，送教上门，戒请老党员为我们讲一埻历史的党课");
                        } else if (recordInfo[id] == "4") {
                            $("#" + id).html("搞一次服务人民群众的志愿服务活劢");
                        } else if (recordInfo[id] == "5") {
                            $("#" + id).html("交心谈心，走访调研，民情恳谈都是很好的形式");
                        } else if (recordInfo[id] == "6") {
                            $("#" + id).html("参观警示教育基地，戒看一场警示教育电影");
                        } else if (recordInfo[id] == "7") {
                            $("#" + id).html("来一次大学习、大认论、大辩论都是丌错的选择");
                        } else if (recordInfo[id] == "8") {
                            $("#" + id).html("深入基层、深入农村，不民共商脱贫攻坚大计");
                        } else if (recordInfo[id] == "9") {
                            $("#" + id).html("认论分析党员队伍建设的现状，提出加强和改迚的措施和办法");
                        } else if (recordInfo[id] == "10") {
                            $("#" + id).html("结合当前的政治形式和工作重点，来一次主题宣传和政策宣讲");
                        } else {
                            $("#" + id).html("未知");
                        }
                    } else if (id == "partyOrgId") {
                        $.ajax({
                            url: "/ret/partyorgget/getPartyOrgById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {partyOrgId: recordInfo[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#" + id).html(res.list.partyOrgName);
                                    } else {
                                        $("#" + id).val("");
                                    }
                                }
                            }
                        });
                    } else if (id == "meetYear") {
                        $("#" + id).html(recordInfo[id] + "年度");
                    } else {
                        $("#" + id).html(recordInfo[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})


function getPartyMemberName(memberIds) {
    var userNameStr = "";
    $.ajax({
        url: "/ret/partymemberget/getPartyMemeberByIds",
        type: "post",
        dataType: "json",
        async: false,
        data: {
            memberIds: memberIds
        },
        success: function (data) {
            if (data.status == "200") {
                var userNameArr = [];
                if (data.list) {
                    for (var i = 0; i < data.list.length; i++) {
                        userNameArr.push(data.list[i].userName);
                    }
                }
                userNameStr = userNameArr.join(",");
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
    return userNameStr;
}
