$(function () {
    $.ajax({
        url: "/ret/partymemberget/getPartyArmyById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "education") {
                        if (recordInfo[id] == "01") {
                            $("#" + id).html("小学");
                        } else if (recordInfo[id] == "02") {
                            $("#" + id).html("初中");
                        } else if (recordInfo[id] == "03") {
                            $("#" + id).html("高中");
                        } else if (recordInfo[id] == "04") {
                            $("#" + id).html("中专");
                        } else if (recordInfo[id] == "05") {
                            $("#" + id).html("技校");
                        } else if (recordInfo[id] == "06") {
                            $("#" + id).html("大专");
                        } else if (recordInfo[id] == "07") {
                            $("#" + id).html("本科");
                        } else if (recordInfo[id] == "08") {
                            $("#" + id).html("研究生");
                        } else if (recordInfo[id] == "09") {
                            $("#" + id).html("博士");
                        } else if (recordInfo[id] == "10") {
                            $("#" + id).html("博士后");
                        } else {
                            $("#" + id).html("未知");
                        }
                    } else if (id == "armyRank") {
                        if (recordInfo[id] == "01") {
                            $("#" + id).html("列兵");
                        } else if (recordInfo[id] == "02") {
                            $("#" + id).html("上等兵");
                        } else if (recordInfo[id] == "03") {
                            $("#" + id).html("下士");
                        } else if (recordInfo[id] == "04") {
                            $("#" + id).html("中士");
                        } else if (recordInfo[id] == "05") {
                            $("#" + id).html("上士");
                        } else if (recordInfo[id] == "06") {
                            $("#" + id).html("四级军士长");
                        } else if (recordInfo[id] == "07") {
                            $("#" + id).html("三级军士长");
                        } else if (recordInfo[id] == "08") {
                            $("#" + id).html("二级军士长");
                        } else if (recordInfo[id] == "09") {
                            $("#" + id).html("一级军士长");
                        } else if (recordInfo[id] == "10") {
                            $("#" + id).html("少尉");
                        }
                        if (recordInfo[id] == "11") {
                            $("#" + id).html("中尉");
                        } else if (recordInfo[id] == "12") {
                            $("#" + id).html("上尉");
                        } else if (recordInfo[id] == "13") {
                            $("#" + id).html("少校");
                        } else if (recordInfo[id] == "14") {
                            $("#" + id).html("中校");
                        } else if (recordInfo[id] == "15") {
                            $("#" + id).html("上校");
                        } else if (recordInfo[id] == "16") {
                            $("#" + id).html("大校");
                        } else if (recordInfo[id] == "17") {
                            $("#" + id).html("少将");
                        } else if (recordInfo[id] == "18") {
                            $("#" + id).html("中将");
                        } else if (recordInfo[id] == "19") {
                            $("#" + id).html("上将");
                        } else {
                            $("#" + id).html("未知");
                        }
                    } else if (id == "partyMember") {
                        if (recordInfo[id] == "0") {
                            $("#" + id).html("无党籍");
                        } else if (recordInfo[id] == "1") {
                            $("#" + id).html("正式党员");
                        } else if (recordInfo[id] == "2") {
                            $("#" + id).html("预备党员");
                        } else {
                            $("#" + id).html("未知");
                        }
                    } else if (id == "memberId") {
                        $.ajax({
                            url: "/ret/partymemberget/getPartyMemberById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {memberId: recordInfo[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#" + id).html(res.list.userName);
                                    } else {
                                        $("#" + id).html("");
                                    }
                                }
                            }
                        });
                    } else if (id == "userType") {
                        if (recordInfo[id] == "1") {
                            $("#" + id).html("民主党派人士");
                        } else if (recordInfo[id] == "2") {
                            $("#" + id).html("无党派人士");
                        } else if (recordInfo[id] == "3") {
                            $("#" + id).html("党外知识分子");
                        } else if (recordInfo[id] == "4") {
                            $("#" + id).html("少数民族人士");
                        } else if (recordInfo[id] == "5") {
                            $("#" + id).html("宗教界人士");
                        } else if (recordInfo[id] == "6") {
                            $("#" + id).html("非公有制经济人士");
                        } else if (recordInfo[id] == "7") {
                            $("#" + id).html("新的社会阶层人士");
                        } else if (recordInfo[id] == "8") {
                            $("#" + id).html("出国和归国留学人员");
                        } else if (recordInfo[id] == "9") {
                            $("#" + id).html("香港同胞、澳门同胞");
                        } else if (recordInfo[id] == "10") {
                            $("#" + id).html("台湾同胞及其在大陆的亲属");
                        }
                        if (recordInfo[id] == "11") {
                            $("#" + id).html("华侨、归侨及侨眷");
                        } else if (recordInfo[id] == "12") {
                            $("#" + id).html("其他需要联系和团结的人员");
                        } else if (recordInfo[id] == "13") {
                            $("#" + id).html("其他");
                        } else {
                            $("#" + id).html("未知");
                        }
                    } else {
                        $("#" + id).html(recordInfo[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})
