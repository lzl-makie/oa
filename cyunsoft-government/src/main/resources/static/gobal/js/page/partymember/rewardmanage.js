$(function () {
    jeDate("#beginTimeQuery", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTimeQuery", {
        format: "YYYY-MM-DD"
    });
    jeDate("#rewardTime", {
        format: "YYYY-MM-DD"
    });
    jeDate("#loseTime", {
        format: "YYYY-MM-DD"
    });
    $('#remark').summernote({height: 300});
    query();
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })
})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/partymemberget/getMemberRewardList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',// 工具列
        striped: true,// 隔行换色
        cache: false,// 禁用缓存
        pagination: true,// 启动分页
        sidePagination: 'server',// 分页方式
        pageNumber: 1,// 初始化table时显示的页码
        pageSize: 10,// 每页条目
        showFooter: false,// 是否显示列脚
        showPaginationSwitch: true,// 是否显示 数据条数选择框
        sortable: true,// 排序
        search: true,// 启用搜索
        sortOrder: "asc",
        showColumns: true,// 是否显示 内容列下拉框
        showRefresh: true,// 显示刷新按钮
        idField: 'recordId',// key值栏位
        clickToSelect: true,// 点击选中checkbox
        pageList: [10, 20, 30, 50],// 可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',// 标题 可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'userName',
            width: '50px',
            title: '党员姓名',
            formatter:function (value,row,index)
            {
                return "<a href=\"javascript:void(0);details('" + row.recordId + "')\" style='cursor: pointer'>"+value+"</a>";
            }
        }, {
            field: 'partyOrgName',
            width: '100px',
            title: '组织名称'
        }, {
            field: 'title',
            title: '奖励名称',
            width: '100px',
            formatter: function (value, row, index) {
                if (row.otherTitle != "") {
                    return value + "/" + row.otherTitle;
                } else {
                    return value;
                }
            }
        }, {
            field: 'rewardTime',
            width: '100px',
            title: '奖励批准日期'
        }, {
            field: 'rewardReasons',
            width: '100px',
            title: '奖励原因',
            formatter: function (value, row, index) {
                if (value == "1") {
                    return "忠于职守，积极工作，成绩显著";
                } else if (value == "2") {
                    return "遵守纪律，廉洁奉公，作风正派，办事公道，模范作用突出";
                } else if (value == "3") {
                    return "在工作中有发明创造或者提出合理化建议，取得显著经济效益或者社会效益";
                } else if (value == "4") {
                    return "为增进民族团结、维护社会稳定做出突出贡献";
                } else if (value == "5") {
                    return "爱护公共财产，节约国家资财有突出成绩";
                } else if (value == "6") {
                    return "防止或者消除事故有功，使国家和人民群众利益免受或者减少损失";
                } else if (value == "7") {
                    return "在抢险、救灾等特定环境中奋不顾身，做出贡献";
                } else if (value == "8") {
                    return "同违法违纪行为作斗争有功绩";
                } else if (value == "9") {
                    return "在对外交往中为国家争得荣誉和利益";
                } else if (value == "10") {
                    return "其他突出功绩";
                } else {
                    return "未知";
                }
            }
        }, {
            field: 'isInTime',
            width: '50px',
            title: '及时性表彰',
            formatter: function (value, row, index) {
                if (value == "1") {
                    return "是";
                } else {
                    return "否";
                }
            }
        }, {
            field: 'rewardTime',
            width: '100px',
            title: '奖励批准日期'
        }, {
            field: 'honoraryLevel',
            width: '120px',
            title: '授予荣誉称号级别',
            formatter: function (value, row, index) {
                if (value == "1") {
                    return "国家级荣誉称号";
                } else if (value == "2") {
                    return "省（自治区、直辖市）级荣誉称号";
                } else if (value == "3") {
                    return "部（委）级荣誉称号";
                } else if (value == "4") {
                    return "区（地、厅、司、局）级";
                } else if (value == "5") {
                    return "县、处级";
                } else if (value == "6") {
                    return "系统内部荣誉称号";
                } else if (value == "7") {
                    return "基层企事业单位荣誉称号";
                } else if (value == "8") {
                    return "国外授予的荣誉称号";
                } else if (value == "9") {
                    return "其他";
                } else {
                    return "未知";
                }
            }
        }, {
            field: 'opt',
            title: '操作',
            align: 'center',
            width: '100px',
            formatter: function (value, row, index) {
                return createOptBtn(row.recordId);
            }
        }],
        onClickCell: function (field, value, row, $element) {
            // alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, // 总页数,前面的key必须为"total"
                    rows: res.list.list
                    // 行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        rewardLevel: $("#rewardLevelQuery").val(),
        memberId: $("#takeTypeQuery").attr("data-value"),
        endTime: $("#endTimeQuery").val(),
        beginTime: $("#beginTimeQuery").val()
    };
    return temp;
};

function createOptBtn(recordId) {
    var html = "<a href=\"javascript:void(0);edit('" + recordId + "')\" class=\"btn btn-primary btn-xs\">编辑</a>&nbsp;&nbsp;" +
        "<a href=\"javascript:void(0);deleterecord('" + recordId + "')\" class=\"btn btn-darkorange btn-xs\" >删除</a>";
    return html;
}

function edit(recordId) {
    $("#rewardlistdiv").hide();
    $("#rewarddiv").show();
    $("#memberId").attr("data_value", "");
    document.getElementById("form1").reset();
    $(".js-back-btn").unbind("click").click(function () {
        goback();
    })
    $.ajax({
        url: "/ret/partymemberget/getPartyRewardRecordById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "remark") {
                        $("#remark").code(recordInfo[id]);
                    } else if (id == "memberId") {
                        $("#memberId").attr("data-value", recordInfo[id]);
                        $.ajax({
                            url: "/ret/partymemberget/getPartyMemberById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {memberId: recordInfo[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#" + id).val(res.list.userName);
                                    } else {
                                        $("#" + id).val("");
                                    }
                                }
                            }
                        });
                    } else if (id == "isInTime") {
                        $("input:checkbox[name='isInTime'][value='" + recordInfo[id] + "']").attr("checked", "checked");
                    } else {
                        $("#" + id).val(recordInfo[id]);
                    }
                }
                $(".js-update-save").unbind("click").click(function () {
                    updateGPartyRewardRecord(recordId);
                })
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function deleterecord(recordId) {
    if (confirm(sysmsg['OPT_CONFIRM_DELETE'])) {
        $.ajax({
            url: "/set/partymemberset/deletePartyRewardRecord",
            type: "post",
            dataType: "json",
            data: {
                recordId: recordId
            },
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable("refresh");
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    }
}


function goback() {
    $("#rewarddiv").hide();
    $("#rewardlistdiv").show();
}

function details(recordId) {
    window.open("/app/core/partymember/rewarddetails?recordId=" + recordId);
}


function updateGPartyRewardRecord(recordId) {
    if ($("#memberId").attr("data-value") == "") {
        layer.msg("党员不能为空！");
        return;
    }
    if ($("#title").val() == "") {
        layer.msg("标题不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partymemberset/updatePartyRewardRecord",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId,
            sortNo: $("#sortNo").val(),
            memberId: $("#memberId").attr("data-value"),
            title: $("#title").val(),
            otherTitle: $("#otherTitle").val(),
            rewardTime: $("#rewardTime").val(),
            honoraryLevel: $("#honoraryLevel").val(),
            unitName: $("#unitName").val(),
            rewardLevel: $("#rewardLevel").val(),
            rewardReasons: $("#rewardReasons").val(),
            isInTime: getCheckBoxValue("isInTime"),
            loseTime: $("#loseTime").val(),
            loseReasons: $("#loseReasons").val(),
            remark: $("#remark").code()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}
