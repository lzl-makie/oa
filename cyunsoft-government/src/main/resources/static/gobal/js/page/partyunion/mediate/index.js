$(function () {
    jeDate("#mediateTime", {
        format: "YYYY-MM-DD"
    });
    $('#content').summernote({height: 200});
    $('#resContent').summernote({height: 200});
    $(".js-add-save").unbind("click").click(function () {
        insertPartyUnionMediate();
    })
    getCodeClass("mediateType", "partyunion_mediate");
})

function insertPartyUnionMediate() {
    if($("#title").val()=="")
    {
        layer.msg("标题不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partyunionset/insertPartyUnionMediate",
        type: "post",
        dataType: "json",
        data: {
            title: $("#title").val(),
            mediateType: $("#mediateType").val(),
            mediateTime: $("#mediateTime").val(),
            partyUser: $("#partyUser").val(),
            mediateUser: $("#mediateUser").val(),
            content: $("#content").code(),
            mediateModel: $("#mediateModel").val(),
            address: $("#address").val(),
            resContent: $("#resContent").code(),
            attach: $("#attach").attr("data_value")
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                window.location.reload();
                layer.msg(sysmsg[data.msg]);
            }
        }
    })
}
