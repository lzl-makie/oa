$(function(){
    $.ajax({
        url: "/ret/partyget/getPartyAlbumVideoById",
        type: "post",
        dataType: "json",
        data: {videoId: videoId},
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                let info = data.list;
                console.log(info);
                for(let id in info)
                {
                    if(id=="attach")
                    {
                    }else if(id=="albumType")
                    {
                        $.ajax({
                            url: "/ret/partyget/getAlbumTypeName",
                            type: "post",
                            dataType: "json",
                            data:{albumTypeId:info[id]},
                            success: function (res) {
                                if (res.status == "500") {
                                    console.log(res.msg);
                                } else if (res.status == "100") {
                                    layer.msg(sysmsg[res.msg]);
                                } else {
                                    $("#albumType").append(res.list.typeName);
                                }
                            }
                        })

                    }else if(id=="approvelUser"||id=="memberId")
                    {
                        $("#"+id).html(getUserNameByStr(info[id]));
                    }else
                    {
                        $("#"+id).html(info[id]);
                    }

                }
            }
        }
    })
})
