$(function () {
    getCodeClass("reportType", "party_report");
    $("#createbut").unbind("click").click(function () {
        addComprehensionRecord();
    })
    $('#content').summernote({height: 300});
    jeDate("#comprehensionTime", {
        format: "YYYY-MM-DD"
    });
})

function addComprehensionRecord() {
    if($("#title").val()=="")
    {
        layer.msg("标题不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partyset/insertPartyCleanComprehension",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            title: $("#title").val(),
            mainPic: $("#file").attr("data-value"),
            comprehensionType: $("#comprehensionType").val(),
            memberId: $("#memberId").attr("data-value"),
            comprehensionTime: $("#comprehensionTime").val(),
            content: $("#content").code(),
            attach: $("#attach").attr("data_value")
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                window.location.reload();
                layer.msg(sysmsg[data.msg]);
            }
        }
    })
}
