$(function () {
    getCodeClass("meritoriousType", "party_meritorious");
    $("#createbut").unbind("click").click(function () {
        addMeritoriousRecord();
    })
    $('#content').summernote({height: 300});
})

function addMeritoriousRecord() {
    if($("#title").val()=="")
    {
        layer.msg("标题不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partyset/insertPartyCleanMeritorious",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            title: $("#title").val(),
            mainPic: $("#file").attr("data-value"),
            meritoriousType: $("#meritoriousType").val(),
            memberId: $("#memberId").attr("data-value"),
            content: $("#content").code(),
            attach: $("#attach").attr("data_value")
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                window.location.reload();
                layer.msg(sysmsg[data.msg]);
            }
        }
    })
}
