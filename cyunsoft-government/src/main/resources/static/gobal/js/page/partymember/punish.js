$(function () {
    jeDate("#punishTime", {
        format: "YYYY-MM-DD"
    });
    jeDate("#loseTime", {
        format: "YYYY-MM-DD"
    });
    $('#remark').summernote({height: 300});
    $("#createbut").unbind("click").click(function () {
        addPartyPunishRecord();
    })
    $.ajax({
        url: "/ret/partyparamget/getPartyTypeTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            var topNode = [{
                sortName: "空",
                isParent: "false",
                sortId: ""
            }];
            var newTreeNodes = topNode.concat(data);
            $.fn.zTree.init($("#partyTypeTree"), partytypesetting, newTreeNodes);
        }
    });
    $("#partyType").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#partyTypeContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });
    $("body").unbind("click").click(function () {
        $(".menuContent").hide();
    });

    $(".menuContent").unbind("click").click(function (e) {
        e.stopPropagation();
    });
})

function addPartyPunishRecord() {
    if ($("#memberId").attr("data-value") == "") {
        layer.msg("党员不能为空！");
        return;
    }
    if ($("#title").val() == "") {
        layer.msg("标题不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partymemberset/insertPartyPunishRecord",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            memberId: $("#memberId").attr("data-value"),
            title: $("#title").val(),
            otherTitle: $("#otherTitle").val(),
            punishTime: $("#punishTime").val(),
            unitName: $("#unitName").val(),
            punishLevel: $("#punishLevel").val(),
            punishReasons: $("#punishReasons").val(),
            partyType: $("#partyType").attr("data-value"),
            loseTime: $("#loseTime").val(),
            loseResons: $("#loseResons").val(),
            remark: $("#remark").code()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

var partytypesetting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyparamget/getPartyTypeTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("partyTypeTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.id;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].sortName + ",";
                vid += nodes[i].sortId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#partyType");
            idem.attr("data-value", vid);
            idem.val(v);
        }
    }
};
