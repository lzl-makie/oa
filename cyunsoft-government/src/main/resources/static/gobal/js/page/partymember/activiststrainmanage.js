$(function () {
    jeDate("#beginTime", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTime", {
        format: "YYYY-MM-DD"
    });
    $.ajax({
        url: "/ret/partyorgget/getPartyOrgTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            var topNode = [{
                partyOrgName: "空",
                isParent: "false",
                partyOrgId: ""
            }];
            var newTreeNodes = topNode.concat(data);
            $.fn.zTree.init($("#partyOrgIdTree"), partyOrgIdsetting, newTreeNodes);
            $.fn.zTree.init($("#partyOrgIdQueryTree"), partyOrgIdQuerysetting, newTreeNodes);
        }
    });
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })
    $("#partyOrgId").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#partyOrgIdContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });
    $("#partyOrgIdQuery").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#partyOrgIdQueryContent").css({
            "width": $(this).outerWidth() + "px",
            "left": $(this).offset().left + "px"
        }).slideDown(200);
    });
    $("body").unbind("click").click(function () {
        $(".menuContent").hide();
    });
    $(".menuContent").unbind("click").click(function (e) {
        e.stopPropagation();
    });
    query();
})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/partymemberget/getActivistsTrainList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',// 工具列
        striped: true,// 隔行换色
        cache: false,// 禁用缓存
        pagination: true,// 启动分页
        sidePagination: 'server',// 分页方式
        pageNumber: 1,// 初始化table时显示的页码
        pageSize: 10,// 每页条目
        showFooter: false,// 是否显示列脚
        showPaginationSwitch: true,// 是否显示 数据条数选择框
        sortable: true,// 排序
        search: true,// 启用搜索
        sortOrder: "asc",
        showColumns: true,// 是否显示 内容列下拉框
        showRefresh: true,// 显示刷新按钮
        idField: 'recordId',// key值栏位
        clickToSelect: true,// 点击选中checkbox
        pageList: [10, 20, 30, 50],// 可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',// 标题 可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'userName',
            title: '积极份子姓名',
            width: '100px',
            formatter:function (value,row,index)
            {
                return "<a href=\"javascript:void(0);details('" + row.recordId + "')\" style='cursor: pointer'>"+value+"</a>";
            }
        }, {
            field: 'partyOrgName',
            width: '100px',
            title: '培训主办单位名称'
        },
            {
                field: 'otherPartyOrgName',
                width: '100px',
                title: '培训主办单位名称补充'
            },
            {
                field: 'beginTime',
                width: '100px',
                title: '开始日期'
            }, {
                field: 'endTime',
                width: '100px',
                title: '结束时间'
            }, {
                field: 'remark',
                title: '备注',
                width: '200px'
            }, {
                field: 'opt',
                title: '操作',
                align: 'center',
                width: '150px',
                formatter: function (value, row, index) {
                    return createOptBtn(row.recordId);
                }
            }],
        onClickCell: function (field, value, row, $element) {
            // alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, // 总页数,前面的key必须为"total"
                    rows: res.list.list
                    // 行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        jobStatus: $("#jobStatusQuery").val(),
        endTime: $("#endTimeQuery").val(),
        beginTime: $("#beginTimeQuery").val(),
        partyOrgId: $("#partyOrgIdQuery").val()
    };
    return temp;
};

function createOptBtn(recordId) {
    var html = "<a href=\"javascript:void(0);edit('" + recordId + "')\" class=\"btn btn-primary btn-xs\">编辑</a>&nbsp;&nbsp;" +
        "<a href=\"javascript:void(0);deleterecord('" + recordId + "')\" class=\"btn btn-darkorange btn-xs\" >删除</a>";
    return html;
}

function deleterecord(recordId) {
    if (confirm(sysmsg['OPT_CONFIRM_DELETE'])) {
        $.ajax({
            url: "/set/partymemberset/deletePartyActivistsTrain",
            type: "post",
            dataType: "json",
            data: {
                recordId: recordId
            },
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable("refresh");
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    }
}

function edit(recordId) {
    $("#trainlistdiv").hide();
    $("#traindiv").show();
    $(".js-back-btn").unbind("click").click(function () {
        goback();
    })
    $.ajax({
        url: "/ret/partymemberget/getPartyActivistsTrainById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "partyOrgId") {
                        $("#" + id).attr("data-value", recordInfo[id]);
                        $.ajax({
                            url: "/ret/partyorgget/getPartyOrgById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {partyOrgId: recordInfo[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#" + id).val(res.list.partyOrgName);
                                    } else {
                                        $("#" + id).val(orgName);
                                    }
                                }
                            }
                        });
                    } else if (id == "joinRecordId") {
                        $("#joinRecordId").empty();
                        $.ajax({
                            url: "/ret/partymemberget/getPartyMemberJoinById",
                            type: "post",
                            dataType: "json",
                            data: {recordId: recordInfo[id]},
                            success: function (data) {
                                if (data.status == "200") {
                                    $("#joinRecordId").append("<option value='" + data.list.recordId + "'>" + data.list.userName + "</option>")
                                } else if (data.status == "100") {
                                    layer.msg(sysmsg[data.msg]);
                                } else if (data.status == "500") {
                                    console.log(data.msg);
                                }
                            }
                        })
                    } else {
                        $("#" + id).val(recordInfo[id]);
                    }
                }
                $(".js-update-save").unbind("click").click(function () {
                    updatePartyActivistsTrain(recordId);
                })
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function details(recordId) {
    window.open("/app/core/partymember/activiststraindetails?recordId=" + recordId);
}

function goback() {
    $("#traindiv").hide();
    $("#trainlistdiv").show();
}

var partyOrgIdQuerysetting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyorgget/getPartyOrgTree",// Ajax 获取数据的 URL 地址
        autoParam: ["partyOrgId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        // 禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "partyOrgId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "partyOrgName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("partyOrgIdQueryTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.partyOrgId;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].partyOrgName + ",";
                vid += nodes[i].partyOrgId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#partyOrgIdQuery");
            idem.val(v);
            idem.attr("data-value", vid);
        }
    }
};

function updatePartyActivistsTrain(recordId) {
    if ($("#joinRecordId").val() == "") {
        layer.msg("请选择受培积极份子！");
    } else {
        $.ajax({
            url: "/set/partymemberset/updatePartyActivistsTrain",
            type: "post",
            dataType: "json",
            data: {
                recordId: recordId,
                joinRecordId: $("#joinRecordId").val(),
                beginTime: $("#beginTime").val(),
                endTime: $("#endTime").val(),
                jobStatus: $("#jobStatus").val(),
                partyOrgId: $("#partyOrgId").attr("data-value"),
                otherPartyOrgName: $("#otherPartyOrgName").val(),
                remark: $("#remark").val()
            },
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    location.reload();
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else if (data.status == "500") {
                    console.log(data.msg);
                }
            }
        })
    }
}

var partyOrgIdsetting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyorgget/getPartyOrgTree",// Ajax 获取数据的 URL 地址
        autoParam: ["partyOrgId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        // 禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "partyOrgId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "partyOrgName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("partyOrgIdTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.partyOrgId;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].partyOrgName + ",";
                vid += nodes[i].partyOrgId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#partyOrgId");
            idem.val(v);
            idem.attr("data-value", vid);
        }
    }
};
