$(function () {
    getCodeClass("exposeTypeQuery", "party_expose");
    jeDate("#beginTimeQuery", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTimeQuery", {
        format: "YYYY-MM-DD"
    });
    query();
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })

})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/partyget/getCleanExposeList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',// 工具列
        striped: true,// 隔行换色
        cache: false,// 禁用缓存
        pagination: true,// 启动分页
        sidePagination: 'server',// 分页方式
        pageNumber: 1,// 初始化table时显示的页码
        pageSize: 10,// 每页条目
        showFooter: false,// 是否显示列脚
        showPaginationSwitch: true,// 是否显示 数据条数选择框
        sortable: true,// 排序
        search: true,// 启用搜索
        sortOrder: "asc",
        showColumns: true,// 是否显示 内容列下拉框
        showRefresh: true,// 显示刷新按钮
        idField: 'recordId',// key值栏位
        clickToSelect: true,// 点击选中checkbox
        pageList: [10, 20, 30, 50],// 可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',// 标题 可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'title',
            title: '曝光标题',
            width: '150px',
            formatter: function (value, row, index) {
                return "<a href=\"javascript:void(0);details('" + row.recordId + "')\" style='cursor: pointer'>" + value + "</a>";
            }
        }, {
            field: 'exposeType',
            width: '50px',
            title: '事件类型',
            formatter: function (value, row, index) {
                return getCodeClassName(value, "party_expose");
            }
        }, {
            field: 'subheading',
            width: '200px',
            title: '副标题'
        }, {
            field: 'status',
            width: '50px',
            title: '状态',
            formatter: function (value, row, index) {
                if (value == "0") {
                    return "审批中";
                } else if (value == "1") {
                    return "通过审批";
                } else if (value == "2") {
                    return "未通过审批";
                }
            }
        }, {
            field: 'createTime',
            width: '100px',
            title: '创建时间'
        }, {
            field: 'createUser',
            title: '创建人',
            width: '100px',
            formatter: function (value, row, index) {
                return getUserNameByStr(value);
            }
        }, {
            field: 'opt',
            title: '操作',
            align: 'center',
            width: '120px',
            formatter: function (value, row, index) {
                return createOptBtn(row.recordId, row.status);
            }
        }],
        onClickCell: function (field, value, row, $element) {
            // alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, // 总页数,前面的key必须为"total"
                    rows: res.list.list
                    // 行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        status: $("#statusQuery").val(),
        exposeType: $("#exposeTypeQuery").val(),
        endTime: $("#endTimeQuery").val(),
        beginTime: $("#beginTimeQuery").val()
    };
    return temp;
};

function createOptBtn(recordId, status) {
    var html = "";
    if (status == "0") {
        html += "<a href=\"javascript:void(0);approval('" + recordId + "')\" class=\"btn btn-primary btn-xs\">审核</a>&nbsp;&nbsp;";
    }
    html += "<a href=\"javascript:void(0);del('" + recordId + "')\" class=\"btn btn-darkorange btn-xs\" >删除</a>";
    return html;
}

function del(recordId) {
    if (confirm("确定删除当前记录吗？")) {
        $.ajax({
            url: "/set/partyset/deletePartyCleanExpose",
            type: "post",
            dataType: "json",
            data: {
                recordId: recordId
            },
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable("refresh");
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    }
}

function details(recordId) {
    window.open("/app/core/party/exposedetails?recordId=" + recordId);
}

function approval(recordId) {
    $("#approvalModal").modal("show");
    $(".js-save").unbind("click").click(function () {
        $.ajax({
            url: "/set/partyset/updatePartyCleanExposeStatus",
            type: "post",
            dataType: "json",
            data: {
                recordId: recordId,
                status: $("#status").val(),
                approvalIdea: $("#approvalIdea").val()
            },
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable("refresh");
                    $("#approvalModal").modal("hide");
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else if (data.status == "500") {
                    console.log(data.msg);
                }
            }
        })
    })
}
