package com.core136.bean.notice;

import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "notice")
public class Notice implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String noticeId;
    private String noticeTitle;
    private String subheading;
    private String noticeType;
    private String sendTime;
    private String msgType;
    private String userPriv;
    private String deptPriv;
    private String LevelPriv;
    private String delFlag;
    private String isTop;
    private String redHead;
    private String status;
    private String attach;
    private String attachPriv;
    private String approvalStatus;
    private String reader;
    private String onclickCount;
    private String approvalStaff;
    private String content;
    private String endTime;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getNoticeId() {
        return noticeId;
    }

    public void setNoticeId(String noticeId) {
        this.noticeId = noticeId;
    }

    public String getNoticeTitle() {
        return noticeTitle;
    }

    public void setNoticeTitle(String noticeTitle) {
        this.noticeTitle = noticeTitle;
    }

    public String getNoticeType() {
        return noticeType;
    }

    public String getSubheading() {
        return subheading;
    }

    public void setSubheading(String subheading) {
        this.subheading = subheading;
    }

    public void setNoticeType(String noticeType) {
        this.noticeType = noticeType;
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public String getUserPriv() {
        return userPriv;
    }

    public void setUserPriv(String userPriv) {
        this.userPriv = userPriv;
    }

    public String getDeptPriv() {
        return deptPriv;
    }

    public void setDeptPriv(String deptPriv) {
        this.deptPriv = deptPriv;
    }

    public String getLevelPriv() {
        return LevelPriv;
    }

    public void setLevelPriv(String levelPriv) {
        LevelPriv = levelPriv;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAttachPriv() {
        return attachPriv;
    }

    public void setAttachPriv(String attachPriv) {
        this.attachPriv = attachPriv;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getReader() {
        return reader;
    }

    public void setReader(String reader) {
        this.reader = reader;
    }

    public String getOnclickCount() {
        return onclickCount;
    }

    public void setOnclickCount(String onclickCount) {
        this.onclickCount = onclickCount;
    }

    public String getRedHead() {
        return redHead;
    }

    public void setRedHead(String redHead) {
        this.redHead = redHead;
    }

    public String getIsTop() {
        return isTop;
    }

    public void setIsTop(String isTop) {
        this.isTop = isTop;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getApprovalStaff() {
        return approvalStaff;
    }

    public void setApprovalStaff(String approvalStaff) {
        this.approvalStaff = approvalStaff;
    }

}
