/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: BpmAdvTableQueryParam.java
 * @Package com.core136.bean.bpm
 * @Description: 描述
 * @author: lsq
 * @date: 2019年12月23日 上午10:19:23
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.bean.document;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 *
 */
public class DocumentAdvTableQueryParam {
    private String id;
    private String flowId;
    private String queryType;
    private String flowTitle;
    private String createTime;
    private String endTime;
    private String createUser;
    private String queryRange;
    private String status;
    private String publicAttach;
    private String sendOrg;
    private String orgId;
    private List<Map<String, String>> whereListMap;
    private List<Map<String, String>> resFields;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSendOrg() {
        return sendOrg;
    }

    public void setSendOrg(String sendOrg) {
        this.sendOrg = sendOrg;
    }

    /**
     * @return the flowTitle
     */
    public String getFlowTitle() {
        return flowTitle;
    }

    /**
     * @param flowTitle the flowTitle to set
     */
    public void setFlowTitle(String flowTitle) {
        this.flowTitle = flowTitle;
    }

    /**
     * @return the createTime
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime the createTime to set
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /**
     * @return the endTime
     */
    public String getEndTime() {
        return endTime;
    }

    /**
     * @param endTime the endTime to set
     */
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    /**
     * @return the createUser
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * @param createUser the createUser to set
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * @return the queryRange
     */
    public String getQueryRange() {
        return queryRange;
    }

    /**
     * @param queryRange the queryRange to set
     */
    public void setQueryRange(String queryRange) {
        this.queryRange = queryRange;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the publicAttach
     */
    public String getPublicAttach() {
        return publicAttach;
    }

    /**
     * @param publicAttach the publicAttach to set
     */
    public void setPublicAttach(String publicAttach) {
        this.publicAttach = publicAttach;
    }

    /**
     * @return the whereListMap
     */
    public List<Map<String, String>> getWhereListMap() {
        return whereListMap;
    }

    /**
     * @param whereListMap the whereListMap to set
     */
    public void setWhereListMap(List<Map<String, String>> whereListMap) {
        this.whereListMap = whereListMap;
    }

    /**
     * @return the resFields
     */
    public List<Map<String, String>> getResFields() {
        return resFields;
    }

    /**
     * @param resFields the resFields to set
     */
    public void setResFields(List<Map<String, String>> resFields) {
        this.resFields = resFields;
    }

    /**
     * @return the queryType
     */
    public String getQueryType() {
        return queryType;
    }

    /**
     * @param queryType the queryType to set
     */
    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    /**
     * @return the flowId
     */
    public String getFlowId() {
        return flowId;
    }

    /**
     * @param flowId the flowId to set
     */
    public void setFlowId(String flowId) {
        this.flowId = flowId;
    }

    /**
     * @return the orgId
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * @param orgId the orgId to set
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }


}
