package com.core136.bean.document;

import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "document_run_process")
public class DocumentRunProcess implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String runProcessId;
    private String runId;
    private String processId;
    private String opFlag;
    private String accountId;
    private String gatherParentId;
    private String passStatus;
    private String status;
    private String ideaText;
    private String attach;
    private String sendId;
    private String recTime;
    private String endTime;
    private String follow;
    private String waitType;
    private String waitProcess;
    private String createType;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getFollow() {
        return follow;
    }

    public void setFollow(String follow) {
        this.follow = follow;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getRunProcessId() {
        return runProcessId;
    }

    public void setRunProcessId(String runProcessId) {
        this.runProcessId = runProcessId;
    }

    public String getRunId() {
        return runId;
    }

    public void setRunId(String runId) {
        this.runId = runId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getOpFlag() {
        return opFlag;
    }

    public void setOpFlag(String opFlag) {
        this.opFlag = opFlag;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getPassStatus() {
        return passStatus;
    }

    public void setPassStatus(String passStatus) {
        this.passStatus = passStatus;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIdeaText() {
        return ideaText;
    }

    public void setIdeaText(String ideaText) {
        this.ideaText = ideaText;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getSendId() {
        return sendId;
    }

    public void setSendId(String sendId) {
        this.sendId = sendId;
    }

    public String getRecTime() {
        return recTime;
    }

    public void setRecTime(String recTime) {
        this.recTime = recTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    /**
     * @return the waitType
     */
    public String getWaitType() {
        return waitType;
    }

    /**
     * @param waitType the waitType to set
     */
    public void setWaitType(String waitType) {
        this.waitType = waitType;
    }

    /**
     * @return the waitProcess
     */
    public String getWaitProcess() {
        return waitProcess;
    }

    /**
     * @param waitProcess the waitProcess to set
     */
    public void setWaitProcess(String waitProcess) {
        this.waitProcess = waitProcess;
    }

    public String getGatherParentId() {
        return gatherParentId;
    }

    public void setGatherParentId(String gatherParentId) {
        this.gatherParentId = gatherParentId;
    }

    public String getCreateType() {
        return createType;
    }

    public void setCreateType(String createType) {
        this.createType = createType;
    }
}
