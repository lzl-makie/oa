package com.core136.bean.education;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: EduCourse
 * @Description: 课程管理
 * @author: 稠云技术
 * @date: 2020年10月18日 下午10:31:10
 * @Copyright: 2020 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "edu_course")
public class EduCourse implements Serializable {

    private static final long serialVersionUID = 1L;
    private String courseId;
    private Integer sortNo;
    private String courseName;
    private String shortName;
    private String status;
    private String remark;
    private String isElecative;
    private String courseCode;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getIsElecative() {
        return isElecative;
    }

    public void setIsElecative(String isElecative) {
        this.isElecative = isElecative;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }
}
