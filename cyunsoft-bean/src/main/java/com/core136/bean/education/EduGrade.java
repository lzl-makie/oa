package com.core136.bean.education;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * 年级设置
 */
@Table(name = "edu_grade")
public class EduGrade implements Serializable {
    private String gradeId;
    private Integer sortNo;
    private String title;
    private String nextGrade;
    private String remark;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getGradeId() {
        return gradeId;
    }

    public void setGradeId(String gradeId) {
        this.gradeId = gradeId;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNextGrade() {
        return nextGrade;
    }

    public void setNextGrade(String nextGrade) {
        this.nextGrade = nextGrade;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }
}
