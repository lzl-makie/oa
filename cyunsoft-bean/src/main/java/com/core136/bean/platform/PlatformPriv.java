package com.core136.bean.platform;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: PlatformPriv
 * @Description: 智能业务平台权限组
 * @author: 稠云技术
 * @date: 2020年12月30日 下午2:10:23
 * @Copyright: 2020 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "platform_priv")
public class PlatformPriv implements Serializable {

    /**
     * 系统角色权限
     */
    private static final long serialVersionUID = 1L;
    private String userPrivId;
    private Integer sortNo;
    private String userPrivName;
    private String userPrivStr;
    private String mobilePriv;
    private String appPriv;
    private String remark;
    private String accounts;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getUserPrivId() {
        return userPrivId;
    }

    public void setUserPrivId(String userPrivId) {
        this.userPrivId = userPrivId;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getUserPrivName() {
        return userPrivName;
    }

    public void setUserPrivName(String userPrivName) {
        this.userPrivName = userPrivName;
    }

    public String getUserPrivStr() {
        return userPrivStr;
    }

    public void setUserPrivStr(String userPrivStr) {
        this.userPrivStr = userPrivStr;
    }

    public String getAppPriv() {
        return appPriv;
    }

    public void setAppPriv(String appPriv) {
        this.appPriv = appPriv;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getMobilePriv() {
        return mobilePriv;
    }

    public void setMobilePriv(String mobilePriv) {
        this.mobilePriv = mobilePriv;
    }

    public String getAccounts() {
        return accounts;
    }

    public void setAccounts(String accounts) {
        this.accounts = accounts;
    }

}
