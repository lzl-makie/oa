package com.core136.bean.email;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: EmailTags
 * @Description: Email 邮件标签
 * @author: 稠云信息
 * @date: 2019年3月8日 下午3:52:29
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "email_tags")
public class EmailTags implements Serializable {
    private static final long serialVersionUID = 1L;
    private String tagId;
    private String tagName;
    private String color;
    private String accountId;
    private String orgId;

    public String getTagId() {
        return tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

}
