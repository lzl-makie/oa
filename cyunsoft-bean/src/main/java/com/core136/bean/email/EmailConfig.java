/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: EmailConfig.java
 * @Package com.core136.bean.oa
 * @Description: (用一句话描述该文件做什么)
 * @author: 刘绍全
 * @date: 2019年1月14日 下午7:35:33
 * @version V1.0
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
package com.core136.bean.email;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: EmailConfig
 * @Description: WEB_EMAIL 配置
 * @author: 刘绍全
 * @date: 2019年1月14日 下午7:35:33
 *
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云科信息技术股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "email_config")
public class EmailConfig implements Serializable {
    private static final long serialVersionUID = 1L;
    private String configId;
    private String accountId;
    private String pop3;
    private String smtp;
    private String email;
    private String passWord;
    private String type;
    private String port;
    private String sign;

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    private String orgId;

    public String getConfigId() {
        return configId;
    }

    public void setConfigId(String configId) {
        this.configId = configId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getPop3() {
        return pop3;
    }

    public void setPop3(String pop3) {
        this.pop3 = pop3;
    }

    public String getSmtp() {
        return smtp;
    }

    public void setSmtp(String smtp) {
        this.smtp = smtp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

}
