package com.core136.bean.vehicle;

import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "vehicle_repair_record")
public class VehicleRepairRecord implements Serializable {


    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String recordId;
    private String vehicleId;
    private String repairTime;
    private String repairUser;
    private String repairType;
    private String reason;
    private Double repairPay;
    private String remark;
    private String repairSupplier;
    private String attach;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getRepairTime() {
        return repairTime;
    }

    public void setRepairTime(String repairTime) {
        this.repairTime = repairTime;
    }

    public String getRepairUser() {
        return repairUser;
    }

    public void setRepairUser(String repairUser) {
        this.repairUser = repairUser;
    }

    public String getRepairType() {
        return repairType;
    }

    public void setRepairType(String repairType) {
        this.repairType = repairType;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Double getRepairPay() {
        return repairPay;
    }

    public void setRepairPay(Double repairPay) {
        this.repairPay = repairPay;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getRepairSupplier() {
        return repairSupplier;
    }

    public void setRepairSupplier(String repairSupplier) {
        this.repairSupplier = repairSupplier;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

}
