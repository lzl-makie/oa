package com.core136.bean.personnel;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * 干部管理单位基本信息
 */
@Table(name = "per_dept")
public class PerDept implements Serializable {
    private static final long serialVersionUID = 1L;
    private String deptId;
    //排序号
    private Integer sortNo;
    //单位名称
    private String deptName;
    //单位名称简称
    private String deptShortName;
    //单位行政编码
    private String deptCode;
    //所在行政区域
    private String place;
    //单位性质
    private String nature;
    //单位类型
    private String deptType;
    //法人单位标识
    private String affiliation;
    //正职领导数量
    private Integer leadersCount1;
    //副职领导数量
    private Integer leadersCount2;
    //编制机构数
    private Integer establishment;
    //编制人数
    private Integer formalCount1;
    //编制外人数
    private Integer formalCount2;

    private String remark;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getDeptShortName() {
        return deptShortName;
    }

    public void setDeptShortName(String deptShortName) {
        this.deptShortName = deptShortName;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public String getDeptType() {
        return deptType;
    }

    public void setDeptType(String deptType) {
        this.deptType = deptType;
    }

    public String getAffiliation() {
        return affiliation;
    }

    public void setAffiliation(String affiliation) {
        this.affiliation = affiliation;
    }

    public Integer getLeadersCount1() {
        return leadersCount1;
    }

    public void setLeadersCount1(Integer leadersCount1) {
        this.leadersCount1 = leadersCount1;
    }

    public Integer getLeadersCount2() {
        return leadersCount2;
    }

    public void setLeadersCount2(Integer leadersCount2) {
        this.leadersCount2 = leadersCount2;
    }

    public Integer getEstablishment() {
        return establishment;
    }

    public void setEstablishment(Integer establishment) {
        this.establishment = establishment;
    }

    public Integer getFormalCount1() {
        return formalCount1;
    }

    public void setFormalCount1(Integer formalCount1) {
        this.formalCount1 = formalCount1;
    }

    public Integer getFormalCount2() {
        return formalCount2;
    }

    public void setFormalCount2(Integer formalCount2) {
        this.formalCount2 = formalCount2;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }
}
