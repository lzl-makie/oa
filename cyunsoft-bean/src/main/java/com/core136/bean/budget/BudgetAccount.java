package com.core136.bean.budget;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: BudgetAccount
 * @Description: 预算科目
 * @author: 稠云技术
 * @date: 2020年9月9日 下午9:56:02
 * @Copyright: 2020 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "budget_account")
public class BudgetAccount implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String budgetAccountId;
    private Integer sortNo;
    private String title;
    private String levelId;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getBudgetAccountId() {
        return budgetAccountId;
    }

    public void setBudgetAccountId(String budgetAccountId) {
        this.budgetAccountId = budgetAccountId;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLevelId() {
        return levelId;
    }

    public void setLevelId(String levelId) {
        this.levelId = levelId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }


}
