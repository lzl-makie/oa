package com.core136.bean.bpm;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: BpmProcess
 * @Description: 步骤的设置
 * @author: 稠云信息
 * @date: 2019年1月22日 下午3:49:05
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "bpm_process")
public class BpmProcess implements Serializable {
    private static final long serialVersionUID = 1L;

    private String processId;
    private Integer sortNo;
    private String flowId;
    private String prcsName;
    private String nextPrcs;
    private String prcsType;
    private Integer pointX;
    private Integer pointY;
    private String parentWait;
    private String addProcess;
    private String waitPrcsId;
    private String concurrentFlag;
    private String gatherFlag;
    private String attachPriv;
    private String remark;
    private String publicFile;
    private String mustField;
    private String hideField;
    private String numField;
    private Double passTime;
    private String userPriv;
    private String deptPriv;
    private String levelPriv;
    private String writerField;
    private String childTablePriv;
    private String childTableRowPriv;
    private String prcsCondition;
    private String goBack;
    private String afterClass;
    private String afterParam;
    private String autoUserModel;
    private String sPrcsAuto;
    private String sPrcsAutoOther;
    private String opRule;
    private String beforeClass;
    private String beforeParam;
    private String changeTitle;
    private String autoSendType;
    private String autoSendUser;
    private String smsConfig;
    private String approval;
    private String approvalFlag;
    private String userLevel;
    private String follow;
    private String remindCreateUser;
    private String remindParticipant;
    private String remindNextUser;
    private String passEndFlag;
    private String changeUser;
    private String sameJump;
    private String xUploadPriv;
    private String doRule;
    private String createUser;
    private String createTime;
    private String orgId;

    public String getPassEndFlag() {
        return passEndFlag;
    }

    public void setPassEndFlag(String passEndFlag) {
        this.passEndFlag = passEndFlag;
    }

    /**
     * @return the addProcess
     */
    public String getAddProcess() {
        return addProcess;
    }

    /**
     * @param addProcess the addProcess to set
     */
    public void setAddProcess(String addProcess) {
        this.addProcess = addProcess;
    }


    public String getAutoSendType() {
        return autoSendType;
    }

    public void setAutoSendType(String autoSendType) {
        this.autoSendType = autoSendType;
    }

    public String getAutoSendUser() {
        return autoSendUser;
    }

    public void setAutoSendUser(String autoSendUser) {
        this.autoSendUser = autoSendUser;
    }

    public Double getPassTime() {
        return passTime;
    }

    public void setPassTime(Double passTime) {
        this.passTime = passTime;
    }

    public String getPublicFile() {
        return publicFile;
    }

    public void setPublicFile(String publicFile) {
        this.publicFile = publicFile;
    }

    public String getConcurrentFlag() {
        return concurrentFlag;
    }

    public String getsPrcsAuto() {
        return sPrcsAuto;
    }

    public void setsPrcsAuto(String sPrcsAuto) {
        this.sPrcsAuto = sPrcsAuto;
    }

    public void setConcurrentFlag(String concurrentFlag) {
        this.concurrentFlag = concurrentFlag;
    }

    public String getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(String userLevel) {
        this.userLevel = userLevel;
    }

    public String getFollow() {
        return follow;
    }

    public void setFollow(String follow) {
        this.follow = follow;
    }

    public String getFlowId() {
        return flowId;
    }

    public void setFlowId(String flowId) {
        this.flowId = flowId;
    }

    public String getApproval() {
        return approval;
    }

    public void setApproval(String approval) {
        this.approval = approval;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getGoBack() {
        return goBack;
    }

    public void setGoBack(String goBack) {
        this.goBack = goBack;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getPrcsName() {
        return prcsName;
    }

    public void setPrcsName(String prcsName) {
        this.prcsName = prcsName;
    }

    public String getNextPrcs() {
        return nextPrcs;
    }

    public void setNextPrcs(String nextPrcs) {
        this.nextPrcs = nextPrcs;
    }

    public String getPrcsType() {
        return prcsType;
    }

    public void setPrcsType(String prcsType) {
        this.prcsType = prcsType;
    }

    public Integer getPointX() {
        return pointX;
    }

    public void setPointX(Integer pointX) {
        this.pointX = pointX;
    }

    public Integer getPointY() {
        return pointY;
    }

    public void setPointY(Integer pointY) {
        this.pointY = pointY;
    }

    public String getAttachPriv() {
        return attachPriv;
    }

    public void setAttachPriv(String attachPriv) {
        this.attachPriv = attachPriv;
    }

    public String getMustField() {
        return mustField;
    }

    public void setMustField(String mustField) {
        this.mustField = mustField;
    }

    public String getHideField() {
        return hideField;
    }

    public void setHideField(String hideField) {
        this.hideField = hideField;
    }

    public String getNumField() {
        return numField;
    }

    public void setNumField(String numField) {
        this.numField = numField;
    }

    public String getSmsConfig() {
        return smsConfig;
    }

    public void setSmsConfig(String smsConfig) {
        this.smsConfig = smsConfig;
    }

    public String getUserPriv() {
        return userPriv;
    }

    public void setUserPriv(String userPriv) {
        this.userPriv = userPriv;
    }

    public String getDeptPriv() {
        return deptPriv;
    }

    public void setDeptPriv(String deptPriv) {
        this.deptPriv = deptPriv;
    }

    public String getLevelPriv() {
        return levelPriv;
    }

    public void setLevelPriv(String levelPriv) {
        this.levelPriv = levelPriv;
    }

    public String getWriterField() {
        return writerField;
    }

    public void setWriterField(String writerField) {
        this.writerField = writerField;
    }

    public String getChildTablePriv() {
        return childTablePriv;
    }

    public void setChildTablePriv(String childTablePriv) {
        this.childTablePriv = childTablePriv;
    }

    public String getPrcsCondition() {
        return prcsCondition;
    }

    public void setPrcsCondition(String prcsCondition) {
        this.prcsCondition = prcsCondition;
    }

    public String getAutoUserModel() {
        return autoUserModel;
    }

    public void setAutoUserModel(String autoUserModel) {
        this.autoUserModel = autoUserModel;
    }

    public String getAfterClass() {
        return afterClass;
    }

    public void setAfterClass(String afterClass) {
        this.afterClass = afterClass;
    }

    public String getOpRule() {
        return opRule;
    }

    public void setOpRule(String opRule) {
        this.opRule = opRule;
    }

    public String getAfterParam() {
        return afterParam;
    }

    public void setAfterParam(String afterParam) {
        this.afterParam = afterParam;
    }

    public String getBeforeClass() {
        return beforeClass;
    }

    public void setBeforeClass(String beforeClass) {
        this.beforeClass = beforeClass;
    }

    public String getBeforeParam() {
        return beforeParam;
    }

    public void setBeforeParam(String beforeParam) {
        this.beforeParam = beforeParam;
    }

    public String getChangeTitle() {
        return changeTitle;
    }

    public void setChangeTitle(String changeTitle) {
        this.changeTitle = changeTitle;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    /**
     * @return the parentWait
     */
    public String getParentWait() {
        return parentWait;
    }

    /**
     * @param parentWait the parentWait to set
     */
    public void setParentWait(String parentWait) {
        this.parentWait = parentWait;
    }

    /**
     * @return the waitPrcsId
     */
    public String getWaitPrcsId() {
        return waitPrcsId;
    }

    /**
     * @param waitPrcsId the waitPrcsId to set
     */
    public void setWaitPrcsId(String waitPrcsId) {
        this.waitPrcsId = waitPrcsId;
    }

    public String getGatherFlag() {
        return gatherFlag;
    }

    public void setGatherFlag(String gatherFlag) {
        this.gatherFlag = gatherFlag;
    }

    public String getApprovalFlag() {
        return approvalFlag;
    }

    public void setApprovalFlag(String approvalFlag) {
        this.approvalFlag = approvalFlag;
    }

    public String getChildTableRowPriv() {
        return childTableRowPriv;
    }

    public void setChildTableRowPriv(String childTableRowPriv) {
        this.childTableRowPriv = childTableRowPriv;
    }

    public String getRemindCreateUser() {
        return remindCreateUser;
    }

    public void setRemindCreateUser(String remindCreateUser) {
        this.remindCreateUser = remindCreateUser;
    }

    public String getRemindParticipant() {
        return remindParticipant;
    }

    public void setRemindParticipant(String remindParticipant) {
        this.remindParticipant = remindParticipant;
    }

    public String getRemindNextUser() {
        return remindNextUser;
    }

    public void setRemindNextUser(String remindNextUser) {
        this.remindNextUser = remindNextUser;
    }

    public String getsPrcsAutoOther() {
        return sPrcsAutoOther;
    }

    public void setsPrcsAutoOther(String sPrcsAutoOther) {
        this.sPrcsAutoOther = sPrcsAutoOther;
    }

    public String getChangeUser() {
        return changeUser;
    }

    public void setChangeUser(String changeUser) {
        this.changeUser = changeUser;
    }

    public String getSameJump() {
        return sameJump;
    }

    public void setSameJump(String sameJump) {
        this.sameJump = sameJump;
    }

    public String getxUploadPriv() {
        return xUploadPriv;
    }

    public void setxUploadPriv(String xUploadPriv) {
        this.xUploadPriv = xUploadPriv;
    }

    public String getDoRule() {
        return doRule;
    }

    public void setDoRule(String doRule) {
        this.doRule = doRule;
    }
}
