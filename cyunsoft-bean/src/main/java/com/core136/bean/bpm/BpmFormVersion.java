package com.core136.bean.bpm;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: BpmFormVersion
 * @Description: 表单版本
 * @author: 稠云技术
 * @date: 2020年3月31日 下午12:46:46
 * @Copyright: 2020 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "bpm_form_version")
public class BpmFormVersion implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String versionId;
    private String title;
    private String formId;
    private String htmlCode;
    private String style;
    private String script;
    private String mobileHtmlCode;
    private String mobileStyle;
    private String mobileScript;
    private String createUser;
    private String createTime;
    private String remark;
    private String orgId;

    public String getVersionId() {
        return versionId;
    }

    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFormId() {
        return formId;
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }

    public String getHtmlCode() {
        return htmlCode;
    }

    public void setHtmlCode(String htmlCode) {
        this.htmlCode = htmlCode;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public String getMobileHtmlCode() {
        return mobileHtmlCode;
    }

    public void setMobileHtmlCode(String mobileHtmlCode) {
        this.mobileHtmlCode = mobileHtmlCode;
    }

    public String getMobileStyle() {
        return mobileStyle;
    }

    public void setMobileStyle(String mobileStyle) {
        this.mobileStyle = mobileStyle;
    }

    public String getMobileScript() {
        return mobileScript;
    }

    public void setMobileScript(String mobileScript) {
        this.mobileScript = mobileScript;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

}
