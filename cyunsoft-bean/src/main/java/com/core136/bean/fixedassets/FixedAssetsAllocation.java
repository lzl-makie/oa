package com.core136.bean.fixedassets;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: FixedAssetsAllocation
 * @Description: 资固资产调拨记录
 * @author: 稠云技术
 * @date: 2020年10月8日 下午8:33:32
 * @Copyright: 2020 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "fixed_assets_allocation")
public class FixedAssetsAllocation implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String recordId;
    private String assetsId;
    private String oldOwnDept;
    private String newOwnDept;
    private String remark;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getAssetsId() {
        return assetsId;
    }

    public void setAssetsId(String assetsId) {
        this.assetsId = assetsId;
    }

    public String getOldOwnDept() {
        return oldOwnDept;
    }

    public void setOldOwnDept(String oldOwnDept) {
        this.oldOwnDept = oldOwnDept;
    }

    public String getNewOwnDept() {
        return newOwnDept;
    }

    public void setNewOwnDept(String newOwnDept) {
        this.newOwnDept = newOwnDept;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

}
