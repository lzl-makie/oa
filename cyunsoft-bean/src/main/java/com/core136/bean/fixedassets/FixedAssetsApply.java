/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: FixedAssetsApply.java
 * @Package com.core136.bean.fixedassets
 * @Description: 描述
 * @author: lsq
 * @date: 2019年12月26日 下午2:46:45
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.bean.fixedassets;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author lsq
 *
 */
@Table(name = "fixed_assets_apply")
public class FixedAssetsApply implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String applyId;
    private String assetsId;
    private String usedUser;
    private String usedDept;
    private String remark;
    private String status;
    private String createUser;
    private String createTime;
    private String orgId;

    /**
     * @return the applyId
     */
    public String getApplyId() {
        return applyId;
    }

    /**
     * @param applyId the applyId to set
     */
    public void setApplyId(String applyId) {
        this.applyId = applyId;
    }

    /**
     * @return the assetsId
     */
    public String getAssetsId() {
        return assetsId;
    }

    /**
     * @param assetsId the assetsId to set
     */
    public void setAssetsId(String assetsId) {
        this.assetsId = assetsId;
    }

    /**
     * @return the usedUser
     */
    public String getUsedUser() {
        return usedUser;
    }

    /**
     * @param usedUser the usedUser to set
     */
    public void setUsedUser(String usedUser) {
        this.usedUser = usedUser;
    }

    /**
     * @return the usedDept
     */
    public String getUsedDept() {
        return usedDept;
    }

    /**
     * @param usedDept the usedDept to set
     */
    public void setUsedDept(String usedDept) {
        this.usedDept = usedDept;
    }

    /**
     * @return the remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark the remark to set
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * @return the createUser
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * @param createUser the createUser to set
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * @return the createTime
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime the createTime to set
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /**
     * @return the orgId
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * @param orgId the orgId to set
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
