/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: FixedAssets.java
 * @Package com.core136.bean.fixedassets
 * @Description: 描述
 * @author: lsq
 * @date: 2019年11月4日 下午4:04:18
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.bean.fixedassets;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author lsq
 * 固定资产明细
 */
@Table(name = "fixed_assets")
public class FixedAssets implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private Integer sortNo;
    private String assetsId;
    private String assetsCode;
    private String sortId;
    private String assetsName;
    private String attach;
    private String remark;
    //使用部门
    private String ownDept;
    private String model;
    private String brand;
    private String purchasePrice;
    //折旧方式
    private String depreciation;
    //采购日期
    private String purchaseTime;
    private String status;
    private String storageId;
    private String createTime;
    private String createUser;
    private String orgId;

    /**
     * @return the assetsId
     */
    public String getAssetsId() {
        return assetsId;
    }

    /**
     * @param assetsId the assetsId to set
     */
    public void setAssetsId(String assetsId) {
        this.assetsId = assetsId;
    }

    /**
     * @return the assetsCode
     */
    public String getAssetsCode() {
        return assetsCode;
    }

    /**
     * @param assetsCode the assetsCode to set
     */
    public void setAssetsCode(String assetsCode) {
        this.assetsCode = assetsCode;
    }

    /**
     * @return the attach
     */
    public String getAttach() {
        return attach;
    }

    /**
     * @param attach the attach to set
     */
    public void setAttach(String attach) {
        this.attach = attach;
    }

    /**
     * @return the remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark the remark to set
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * @return the ownDept
     */
    public String getOwnDept() {
        return ownDept;
    }

    /**
     * @param ownDept the ownDept to set
     */
    public void setOwnDept(String ownDept) {
        this.ownDept = ownDept;
    }

    /**
     * @return the model
     */
    public String getModel() {
        return model;
    }

    /**
     * @param model the model to set
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * @return the brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     * @param brand the brand to set
     */
    public void setBrand(String brand) {
        this.brand = brand;
    }

    /**
     * @return the purchasePrice
     */
    public String getPurchasePrice() {
        return purchasePrice;
    }

    /**
     * @param purchasePrice the purchasePrice to set
     */
    public void setPurchasePrice(String purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    /**
     * @return the depreciation
     */
    public String getDepreciation() {
        return depreciation;
    }

    /**
     * @param depreciation the depreciation to set
     */
    public void setDepreciation(String depreciation) {
        this.depreciation = depreciation;
    }

    /**
     * @return the purchaseTime
     */
    public String getPurchaseTime() {
        return purchaseTime;
    }

    /**
     * @param purchaseTime the purchaseTime to set
     */
    public void setPurchaseTime(String purchaseTime) {
        this.purchaseTime = purchaseTime;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the createTime
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime the createTime to set
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /**
     * @return the createUser
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * @param createUser the createUser to set
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * @return the orgId
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * @param orgId the orgId to set
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    /**
     * @return the sortNo
     */
    public Integer getSortNo() {
        return sortNo;
    }

    /**
     * @param sortNo the sortNo to set
     */
    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    /**
     * @return the sortId
     */
    public String getSortId() {
        return sortId;
    }

    /**
     * @param sortId the sortId to set
     */
    public void setSortId(String sortId) {
        this.sortId = sortId;
    }

    /**
     * @return the assetsName
     */
    public String getAssetsName() {
        return assetsName;
    }

    /**
     * @param assetsName the assetsName to set
     */
    public void setAssetsName(String assetsName) {
        this.assetsName = assetsName;
    }

    /**
     * @return the storageId
     */
    public String getStorageId() {
        return storageId;
    }

    /**
     * @param storageId the storageId to set
     */
    public void setStorageId(String storageId) {
        this.storageId = storageId;
    }


}
