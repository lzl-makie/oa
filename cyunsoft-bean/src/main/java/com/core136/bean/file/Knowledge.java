/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: KonwLeadge.java
 * @Package com.core136.bean.file
 * @Description: (用一句话描述该文件做什么)
 * @author: 稠云信息
 * @date: 2019年9月10日 上午10:43:39
 * @version V1.0
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
package com.core136.bean.file;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: KonwLeadge
 * @Description:(这里用一句话描述这个类的作用)
 * @author: 稠云信息
 * @date: 2019年9月10日 上午10:43:39
 * @author lsq
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "knowledge")
public class Knowledge implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String knowledgeId;
    private Integer sortNo;
    private String title;
    private String keywords;
    private String content;
    private String attach;
    private String sortId;
    private String version;
    //几星
    private String levelStar;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getKnowledgeId() {
        return knowledgeId;
    }

    public void setKnowledgeId(String knowledgeId) {
        this.knowledgeId = knowledgeId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getSortId() {
        return sortId;
    }

    public void setSortId(String sortId) {
        this.sortId = sortId;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getLevelStar() {
        return levelStar;
    }

    public void setLevelStar(String levelStar) {
        this.levelStar = levelStar;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    /**
     * @return the content
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content the content to set
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * @return the sortNo
     */
    public Integer getSortNo() {
        return sortNo;
    }

    /**
     * @param sortNo the sortNo to set
     */
    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }


}
