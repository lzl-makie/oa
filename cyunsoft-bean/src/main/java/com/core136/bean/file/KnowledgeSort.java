/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: KnowledgeSort.java
 * @Package com.core136.bean.file
 * @Description: (用一句话描述该文件做什么)
 * @author: 稠云信息
 * @date: 2019年9月10日 上午10:32:05
 * @version V1.0
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
package com.core136.bean.file;

import javax.persistence.Table;
import java.io.Serializable;


/**
 * @ClassName: KnowledgeSort
 * @Description:(这里用一句话描述这个类的作用)
 * @author: 稠云信息
 * @date: 2019年9月10日 上午10:32:05   
 * @author lsq
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved. 
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目 
 */
@Table(name = "knowledge_sort")
public class KnowledgeSort implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String sortId;
    private Integer sortNo;
    private String sortName;
    private String sortRemark;
    private String createUser;
    private String createTime;
    private String userPriv;
    private String deptPriv;
    private String levelPriv;
    private String orgId;
    private String sortLevel;

    public String getSortId() {
        return sortId;
    }

    public void setSortId(String sortId) {
        this.sortId = sortId;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getSortName() {
        return sortName;
    }

    public void setSortName(String sortName) {
        this.sortName = sortName;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUserPriv() {
        return userPriv;
    }

    public void setUserPriv(String userPriv) {
        this.userPriv = userPriv;
    }

    public String getDeptPriv() {
        return deptPriv;
    }

    public void setDeptPriv(String deptPriv) {
        this.deptPriv = deptPriv;
    }

    public String getLevelPriv() {
        return levelPriv;
    }

    public void setLevelPriv(String levelPriv) {
        this.levelPriv = levelPriv;
    }

    public String getSortRemark() {
        return sortRemark;
    }

    public void setSortRemark(String sortRemark) {
        this.sortRemark = sortRemark;
    }

    public String getSortLevel() {
        return sortLevel;
    }

    public void setSortLevel(String sortLevel) {
        this.sortLevel = sortLevel;
    }


}
