/**
 * All rights Reserved, Designed By www.tydic.com
 *
 * @Title: Photo.java
 * @Package com.core136.bean.file
 * @Description: 描述
 * @author: lsq
 * @date: 2019年10月17日 上午11:02:49
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.bean.file;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author lsq
 *
 */
@Table(name = "photo")
public class Photo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String photoId;
    private Integer sortNo;
    private String photoTitle;
    private String rootPath;
    private String accessUser;
    private String accessDept;
    private String accessLevel;
    private String manageUser;
    private String createTime;
    private String createUser;
    private String orgId;

    /**
     * @return the sortNo
     */
    public Integer getSortNo() {
        return sortNo;
    }

    /**
     * @param sortNo the sortNo to set
     */
    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    /**
     * @return the photoId
     */
    public String getPhotoId() {
        return photoId;
    }

    /**
     * @param photoId the photoId to set
     */
    public void setPhotoId(String photoId) {
        this.photoId = photoId;
    }

    /**
     * @return the rootPath
     */
    public String getRootPath() {
        return rootPath;
    }

    /**
     * @param rootPath the rootPath to set
     */
    public void setRootPath(String rootPath) {
        this.rootPath = rootPath;
    }

    /**
     * @return the photoTitle
     */
    public String getPhotoTitle() {
        return photoTitle;
    }

    /**
     * @param photoTitle the photoTitle to set
     */
    public void setPhotoTitle(String photoTitle) {
        this.photoTitle = photoTitle;
    }


    /**
     * @return the accessUser
     */
    public String getAccessUser() {
        return accessUser;
    }

    /**
     * @param accessUser the accessUser to set
     */
    public void setAccessUser(String accessUser) {
        this.accessUser = accessUser;
    }

    /**
     * @return the accessDept
     */
    public String getAccessDept() {
        return accessDept;
    }

    /**
     * @param accessDept the accessDept to set
     */
    public void setAccessDept(String accessDept) {
        this.accessDept = accessDept;
    }

    /**
     * @return the accessLevel
     */
    public String getAccessLevel() {
        return accessLevel;
    }

    /**
     * @param accessLevel the accessLevel to set
     */
    public void setAccessLevel(String accessLevel) {
        this.accessLevel = accessLevel;
    }

    /**
     * @return the manageUser
     */
    public String getManageUser() {
        return manageUser;
    }

    /**
     * @param manageUser the manageUser to set
     */
    public void setManageUser(String manageUser) {
        this.manageUser = manageUser;
    }
    /**
     * @return the manageDeptPriv
     */
    /**
     * @return the createTime
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime the createTime to set
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /**
     * @return the createUser
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * @param createUser the createUser to set
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * @return the orgId
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * @param orgId the orgId to set
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }


}
