/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: NetDisk.java
 * @Package com.core136.bean.file
 * @Description: (用一句话描述该文件做什么)
 * @author: 刘绍全
 * @date: 2019年1月14日 下午9:45:37
 * @version V1.0
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
package com.core136.bean.file;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: NetDisk
 * @Description:(这里用一句话描述这个类的作用)
 * @author: 刘绍全
 * @date: 2019年1月14日 下午9:45:37
 *
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云科信息技术股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "net_disk")
public class NetDisk implements Serializable {
    private static final long serialVersionUID = 1L;
    private String netDiskId;
    private Integer sortNo;
    private String netDiskName;
    private String rootPath;
    private String createUser;
    private String manageUser;
    private String downUser;
    private String accessUser;
    private String createDept;
    private String manageDept;
    private String downDept;
    private String accessDept;
    private String createLevel;
    private String manageLevel;
    private String downLevel;
    private String accessLevel;
    private Integer spaceLimit;
    private String orderBy;
    private String ascOrDesc;
    private String diskCreateUser;
    private String diskCreateTime;
    private String orgId;

    public String getNetDiskId() {
        return netDiskId;
    }

    public void setNetDiskId(String netDiskId) {
        this.netDiskId = netDiskId;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getNetDiskName() {
        return netDiskName;
    }

    public void setNetDiskName(String netDiskName) {
        this.netDiskName = netDiskName;
    }

    public String getRootPath() {
        return rootPath;
    }

    public void setRootPath(String rootPath) {
        this.rootPath = rootPath;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getManageUser() {
        return manageUser;
    }

    public void setManageUser(String manageUser) {
        this.manageUser = manageUser;
    }

    public String getDownUser() {
        return downUser;
    }

    public void setDownUser(String downUser) {
        this.downUser = downUser;
    }

    public String getAccessUser() {
        return accessUser;
    }

    public void setAccessUser(String accessUser) {
        this.accessUser = accessUser;
    }

    public String getCreateDept() {
        return createDept;
    }

    public void setCreateDept(String createDept) {
        this.createDept = createDept;
    }

    public String getManageDept() {
        return manageDept;
    }

    public void setManageDept(String manageDept) {
        this.manageDept = manageDept;
    }

    public String getDownDept() {
        return downDept;
    }

    public void setDownDept(String downDept) {
        this.downDept = downDept;
    }

    public String getAccessDept() {
        return accessDept;
    }

    public void setAccessDept(String accessDept) {
        this.accessDept = accessDept;
    }

    public String getCreateLevel() {
        return createLevel;
    }

    public void setCreateLevel(String createLevel) {
        this.createLevel = createLevel;
    }

    public String getManageLevel() {
        return manageLevel;
    }

    public void setManageLevel(String manageLevel) {
        this.manageLevel = manageLevel;
    }

    public String getDownLevel() {
        return downLevel;
    }

    public void setDownLevel(String downLevel) {
        this.downLevel = downLevel;
    }

    public String getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(String accessLevel) {
        this.accessLevel = accessLevel;
    }

    public Integer getSpaceLimit() {
        return spaceLimit;
    }

    public void setSpaceLimit(Integer spaceLimit) {
        this.spaceLimit = spaceLimit;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getAscOrDesc() {
        return ascOrDesc;
    }

    public void setAscOrDesc(String ascOrDesc) {
        this.ascOrDesc = ascOrDesc;
    }

    public String getDiskCreateUser() {
        return diskCreateUser;
    }

    public void setDiskCreateUser(String diskCreateUser) {
        this.diskCreateUser = diskCreateUser;
    }

    public String getDiskCreateTime() {
        return diskCreateTime;
    }

    public void setDiskCreateTime(String diskCreateTime) {
        this.diskCreateTime = diskCreateTime;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }


}
