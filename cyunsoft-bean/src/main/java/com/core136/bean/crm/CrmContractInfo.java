/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: CrmContractInfo.java
 * @Package com.core136.bean.crm
 * @Description: (用一句话描述该文件做什么)
 * @author: 稠云信息
 * @date: 2019年4月24日 下午3:24:17
 * @version V1.0
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
package com.core136.bean.crm;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: CrmContractInfo
 * @Description:(这里用一句话描述这个类的作用)
 * @author: 稠云信息
 * @date: 2019年4月24日 下午3:24:17
 * @author lsq
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "crm_contract_info")
public class CrmContractInfo implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String contractInfoId;
    private String customerId;
    private String bank;
    private String bankAccount;
    private String taxNo;
    private String createTime;
    private String createUser;
    private Integer sortNo;
    private String orgId;

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getContractInfoId() {
        return contractInfoId;
    }

    public void setContractInfoId(String contractInfoId) {
        this.contractInfoId = contractInfoId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getTaxNo() {
        return taxNo;
    }

    public void setTaxNo(String taxNo) {
        this.taxNo = taxNo;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

}
