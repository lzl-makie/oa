/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: ContractPriv.java
 * @Package com.core136.bean.contract
 * @Description: (用一句话描述该文件做什么)
 * @author: 稠云信息
 * @date: 2019年4月26日 上午9:00:42
 * @version V1.0
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
package com.core136.bean.contract;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: ContractPriv
 * @Description:(这里用一句话描述这个类的作用)
 * @author: 稠云信息
 * @date: 2019年4月26日 上午9:00:42
 * @author lsq
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "contract_priv")
public class ContractPriv implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String privId;
    private String financialStaff;
    private String shipper;
    private String lendingExaminer;
    private String borrowPerson;
    private String orgId;

    public String getPrivId() {
        return privId;
    }

    public void setPrivId(String privId) {
        this.privId = privId;
    }

    public String getFinancialStaff() {
        return financialStaff;
    }

    public void setFinancialStaff(String financialStaff) {
        this.financialStaff = financialStaff;
    }

    public String getShipper() {
        return shipper;
    }

    public void setShipper(String shipper) {
        this.shipper = shipper;
    }

    public String getLendingExaminer() {
        return lendingExaminer;
    }

    public void setLendingExaminer(String lendingExaminer) {
        this.lendingExaminer = lendingExaminer;
    }

    public String getBorrowPerson() {
        return borrowPerson;
    }

    public void setBorrowPerson(String borrowPerson) {
        this.borrowPerson = borrowPerson;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

}
