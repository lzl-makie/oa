/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: Meeting.java
 * @Package com.core136.bean.meeting
 * @Description: (用一句话描述该文件做什么)
 * @author: 稠云信息
 * @date: 2019年9月11日 上午9:46:24
 * @version V1.0
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
package com.core136.bean.meeting;

import java.io.Serializable;

/**
 * @ClassName: Meeting
 * @Description:(这里用一句话描述这个类的作用)
 * @author: 稠云信息
 * @date: 2019年9月11日 上午9:46:24
 * @author lsq
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
public class Meeting implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String meetingId;
    private String meetingType;
    private String msgType;
    private String subject;
    private String roomId;
    private String beginTime;
    private String endTime;
    //参会人员
    private String userJoin;
    private String deptJoin;
    private String levelJoin;
    private String otherJoin;
    private String remark;
    private String attach;
    private String notesUser;
    private String chair;
    private String status;
    private String deviceId;
    private String attachPriv;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getMeetingId() {
        return meetingId;
    }

    public void setMeetingId(String meetingId) {
        this.meetingId = meetingId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getOtherJoin() {
        return otherJoin;
    }

    public void setOtherJoin(String otherJoin) {
        this.otherJoin = otherJoin;
    }

    public String getNotesUser() {
        return notesUser;
    }

    public void setNotesUser(String notesUser) {
        this.notesUser = notesUser;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getUserJoin() {
        return userJoin;
    }

    public void setUserJoin(String userJoin) {
        this.userJoin = userJoin;
    }

    public String getDeptJoin() {
        return deptJoin;
    }

    public void setDeptJoin(String deptJoin) {
        this.deptJoin = deptJoin;
    }

    public String getLevelJoin() {
        return levelJoin;
    }

    public void setLevelJoin(String levelJoin) {
        this.levelJoin = levelJoin;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getChair() {
        return chair;
    }

    public void setChair(String chair) {
        this.chair = chair;
    }

    public String getMeetingType() {
        return meetingType;
    }

    public void setMeetingType(String meetingType) {
        this.meetingType = meetingType;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getAttachPriv() {
        return attachPriv;
    }

    public void setAttachPriv(String attachPriv) {
        this.attachPriv = attachPriv;
    }

}
