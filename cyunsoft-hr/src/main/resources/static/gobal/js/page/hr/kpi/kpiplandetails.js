$(function () {
    $.ajax({
        url: "/ret/hrget/getHrKpiPlanById",
        type: "post",
        dataType: "json",
        data: {planId: planId},
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "attach") {
                        $("#hrattach").attr("data_value", recordInfo.attach);
                        createAttach("hrattach", 1);
                    } else if (id == "chargeUser") {
                        $("#" + id).html(getUserNameByStr(recordInfo[id]));
                    } else if (id == "userPriv") {
                        $("#" + id).html(getUserNameByStr(recordInfo[id]));
                    } else if (id == "deptPriv") {
                        $("#" + id).html(getDeptNameByStr(recordInfo[id]));
                    } else if (id == "levelPriv") {
                        $("#" + id).html(getHrUserLevelByStr(recordInfo[id]));
                    } else if (id == "status") {
                        if (recordInfo[id] == "0") {
                            $("#" + id).html("未生效");
                        } else if (recordInfo[id] == "1") {
                            $("#" + id).html("生效中");
                        } else if (recordInfo[id] == "2") {
                            $("#" + id).html("已终止");
                        }
                    } else if (id == "kpiRule") {
                        if (recordInfo[id] == "1") {
                            $("#" + id).html("指定考核人");
                        } else if (recordInfo[id] == "2") {
                            $("#" + id).html("部门主管考核");
                        } else if (recordInfo[id] == "3") {
                            $("#" + id).html("逐级考核");
                        }
                    } else {
                        $("#" + id).html(recordInfo[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    });
    getKpiItemList();
})

function getKpiItemList() {
    $.ajax({
        url: "/ret/hrget/getHrKpiPlanItemList",
        type: "post",
        dataType: "json",
        data: {planId: planId},
        success: function (data) {
            if (data.status == "200") {
                var childItemArr = data.list;
                for (var i = 0; i < childItemArr.length; i++) {
                    var html = "<tr><td>" + (i + 1) + "</td>" +
                        "<td>" + childItemArr[i].title + "</td>";
                    html += "<td>";
                    var zjArr = JSON.parse(childItemArr[i].childItem);
                    var chtml = "<table class=\"table table-hover table-striped table-bordered\"><tr><td width='50px'>序号</td><td>子项</td><td width='100px'>分值</td></tr>"
                    for (var j = 0; j < zjArr.length; j++) {
                        chtml += "<tr><td>" + zjArr[j].sortNo + "</td><td>" + zjArr[j].childTitle + "</td><td>" + zjArr[j].childScore + "</td></tr>"
                    }
                    chtml += "</table>";
                    html += chtml;
                    html + "</td>";
                    html += "</tr>";
                    $("#child-item-table").append(html);
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}
