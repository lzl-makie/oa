let ue = UE.getEditor("remark");
$(function () {
    jeDate("#year", {
        format: "YYYY"
    });
    $("#createbut").unbind("click").click(function () {
        addWelfareRecord()
    });
})

function addWelfareRecord() {
    if($("#title").val()=="")
    {
        layer.msg("福利标题不能为空！");
        return;
    }
    $.ajax({
        url: "/set/hrset/insertHrWelfareRecord",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            userId: $("#userId").attr("data-value"),
            title: $("#title").val(),
            year: $("#year").val(),
            month: $("#month").val(),
            type: $("#type").val(),
            amount: $("#amount").val(),
            remark: ue.getContent()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}
