$(function () {
    query();
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })
    $.ajax({
        url: "/ret/hrget/getKpiPlanForListForSelect",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var planList = data.list;
                for (var i = 0; i < planList.length; i++) {
                    $("#planIdQuery").append("<option value=\"" + planList[i].planId + "\">" + planList[i].title + "</option>")
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/hrget/getKpiPlanForUserQueryList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        search: true,//启用搜索
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'planId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        },
            {
                field: 'num',
                title: '序号',//标题  可不加
                width: '50px',
                formatter: function (value, row, index) {
                    return index + 1;
                }
            },
            {
                field: 'title',
                title: '考核标题',
                width: '150px'
            },
            {
                field: 'accountId',
                width: '100px',
                title: '被考核人',
                formatter: function (value, row, index) {
                    return getUserNameByStr(value);
                }
            },
            {
                field: 'beginTime',
                width: '50px',
                title: '开始时间'
            },
            {
                field: 'endTime',
                width: '50px',
                title: '结束时间'
            },
            {
                field: 'totalScore',
                width: '50px',
                title: '考核总分'
            },
            {
                field: 'chargeUser',
                width: '100px',
                title: '考核人',
                formatter: function (value, row, index) {
                    return getUserNameByStr(value);
                }
            }, {
                field: 'createTime',
                width: '100px',
                title: '考核时间'
            },
            {
                field: 'opt',
                width: '50px',
                align: 'center',
                title: '操作',
                formatter: function (value, row, index) {
                    return createOptBtn(row.planId, row.accountId);
                }
            }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        planId: $("#planIdQuery").val(),
        chargeUser: $("#chargeUserQuery").attr("data-value"),
        accountId: $("#accountIdQuery").attr("data-value")
    };
    return temp;
};

function createOptBtn(planId, accountId) {
    var html = "<a href=\"javascript:void(0);getresult('" + planId + "','" + accountId + "')\" class=\"btn btn-sky btn-xs\" >详情</a>";
    return html;
}

function getresult(planId, accountId) {
    window.open("/app/core/hr/startkpi?view=result&accountId=" + accountId + "&planId=" + planId);
}
