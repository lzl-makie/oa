let ue = UE.getEditor("remark");
$(function () {
    jeDate("#beginTimeQuery", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTimeQuery", {
        format: "YYYY-MM-DD"
    });
    query();
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })
    jeDate("#beginTime", {
        format: "YYYY-MM",
        minDate: getSysDate()
    });
    jeDate("#endTime", {
        format: "YYYY-MM-DD"
    });
    $(".js-auto-select").each(function () {
        var module = $(this).attr("module");
        createAutoSelect(module);
    })

})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/hrget/getHrLearnRecordList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        search: true,//启用搜索
        sortOrder: "asc",
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'recordId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',//标题  可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'userId',
            title: '学生姓名',
            sortable: true,
            width: '80px',
            formatter: function (value, row, index) {
                if (row.userName == "" || row.userName == null) {
                    return "<a href=\"javascript:void(0);details('" + row.recordId + "')\" style='cursor: pointer'>"+getHrUserNameByStr(row.userId)+"</a>";
                } else {
                    return "<a href=\"javascript:void(0);details('" + row.recordId + "')\" style='cursor: pointer'>"+row.userName+"</a>";
                }
            }
        }, {
            field: 'shoolName',
            width: '100px',
            title: '学校名称'
        }, {
            field: 'major',
            width: '100px',
            title: '所学专业'
        }, {
            field: 'beginTime',
            width: '50px',
            title: '入学日期'
        }, {
            field: 'endTime',
            width: '50px',
            title: '毕业日期'
        }, {
            field: 'cerifier',
            title: '证明人',
            width: '100px'
        }, {
            field: 'opt',
            title: '操作',
            align: 'center',
            width: '120px',
            formatter: function (value, row, index) {
                return createOptBtn(row.recordId);
            }
        }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list
                    //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        userId: $("#userIdQuery").attr("data-value"),
        endTime: $("#endTimeQuery").val(),
        beginTime: $("#beginTimeQuery").val()
    };
    return temp;
};

function createOptBtn(recordId) {
    var html = "<a href=\"javascript:void(0);edit('" + recordId + "')\" class=\"btn btn-primary btn-xs\">编辑</a>&nbsp;&nbsp;"
        + "<a href=\"javascript:void(0);deleteReocrd('" + recordId + "')\" class=\"btn btn-darkorange btn-xs\" >删除</a>";
    return html;
}

function deleteReocrd(recordId) {
    if (confirm("确定删除当前学习记录吗？")) {
        $.ajax({
            url: "/set/hrset/deleteHrLearnRecord",
            type: "post",
            dataType: "json",
            data: {recordId: recordId},
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable("refresh");
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    }
}

function edit(recordId) {
    $("#learnlistdiv").hide();
    $("#learndiv").show();
    $(".js-back-btn").unbind("click").click(function () {
        goback();
    })
    $("#hrattach").attr("data_value","");
    $("#hrattach_show").empty();
    $("#userId").attr("data-value","");
    ue.setContent("")
    document.getElementById("form1").reset();
    $.ajax({
        url: "/ret/hrget/getHrLearnRecordById",
        type: "post",
        dataType: "json",
        data: {recordId: recordId},
        success: function (data) {
            if (data.status == "200") {
                var incentiveInfo = data.list;
                for (var id in incentiveInfo) {
                    if (id == "attach") {
                        $("#show_hrattach").html("");
                        $("#hrattach").attr("data_value", incentiveInfo.attach);
                        createAttach("hrattach", 4);
                    } else if (id == "userId") {
                        $("#" + id).val(getHrUserNameByStr(incentiveInfo[id]));
                        $("#" + id).attr("data-value", incentiveInfo[id]);
                    } else if (id == "remark") {
                        ue.setContent(incentiveInfo[id])
                    } else if (id == "reminder") {
                        $("input:radio[name='reminder'][value='" + incentiveInfo[id] + "']").prop("checked", "checked");
                    } else {
                        $("#" + id).val(incentiveInfo[id]);
                    }
                }
                $(".js-update-save").unbind("click").click(function () {
                    updateHrLearnRecord(recordId);
                })
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function details(recordId) {
    window.open("/app/core/hr/learndetails?recordId=" + recordId);
}

function updateHrLearnRecord(recordId) {
    if($("#userId").attr("data-value")=="")
    {
        layer.msg("相关人员不能为空!");
        return;
    }
    $.ajax({
        url: "/set/hrset/updateHrLearnRecord",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId,
            sortNo: $("#sortNo").val(),
            shoolName: $("#shoolName").val(),
            userId: $("#userId").attr("data-value"),
            major: $("#major").val(),
            beginTime: $("#beginTime").val(),
            endTime: $("#endTime").val(),
            highsetDegree: $("#highsetDegree").val(),
            cerifier: $("#cerifier").val(),
            cerificate: $("#cerificate").val(),
            honor: $("#honor").val(),
            attach: $("#hrattach").attr("data_value"),
            remark: ue.getContent()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function goback() {
    $("#learndiv").hide();
    $("#learnlistdiv").show();
}
