let ue = UE.getEditor("remark");
$(function () {
    jeDate("#salaryMonth", {
        format: "YYYY-MM",
        minDate: getSysDate(),
    });
    jeDate("#incentiveTime", {
        format: "YYYY-MM-DD"
    });
    $(".js-add-save").unbind("click").click(function () {
        addIncentive();
    })
    getSmsConfig("msgType", "hr");
    $(".js-auto-select").each(function () {
        var module = $(this).attr("module");
        createAutoSelect(module);
    })

})

function addIncentive() {
    if($("#incentiveItem").val()=="")
    {
        layer.msg("相关人员不能为空!");
        return;
    }
    $.ajax({
        url: "/set/hrset/insertHrIncentive",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            incentiveType: $("#incentiveType").val(),
            userId: $("#userId").attr("data-value"),
            incentiveItem: $("#incentiveItem").val(),
            incentiveTime: $("#incentiveTime").val(),
            incentiveAmount: $("#incentiveAmount").val(),
            salaryMonth: $("#salaryMonth").val(),
            msgType: getCheckBoxValue("msgType"),
            attach: $("#hrattach").attr("data_value"),
            remark: ue.getContent()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}
