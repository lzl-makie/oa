let ue = UE.getEditor("remark");
$(function () {
    jeDate("#workDate", {
        format: "YYYY-MM-DD",
        minDate: getSysDate()
    });
    $(".js-add-save").unbind("click").click(function () {
        addRecruitTask();
    })
    $(".js-auto-select").each(function () {
        var module = $(this).attr("module");
        createAutoSelect(module);
    })
    getHrRecruitPlanForSelect();
})

function addRecruitTask() {
    if($("#userName").val()=="")
    {
        layer.msg("人员姓名不能为空！");
        return;
    }
    $.ajax({
        url: "/set/hrset/insertHrRecruitTask",
        type: "post",
        dataType: "json",
        data: {
            workDate: $("#workDate").val(),
            userName: $("#userName").val(),
            linkTel: $("#linkTel").val(),
            salary: $("#salary").val(),
            workType: $("#workType").val(),
            planId: $("#planId").val(),
            attach: $("#hrattach").attr("data_value"),
            remark: ue.getContent()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function getHrRecruitPlanForSelect() {
    $.ajax({
        url: "/ret/hrget/getHrRecruitPlanForSelect",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var html = "<option value=''>请选择</option>";
                for (var i = 0; i < data.list.length; i++) {
                    html += "<option value='" + data.list[i].planId + "'>" + data.list[i].title + "</option>";
                }
                $("#planId").html(html);
            } else if (data.status == "100") {
                layer.msg(data.msg)
            } else if (data.statsu == "500") {
                console.log(data.msg);
            }
        }
    });
}
