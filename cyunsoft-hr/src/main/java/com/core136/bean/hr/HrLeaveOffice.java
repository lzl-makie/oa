package com.core136.bean.hr;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: HrLevelOffice
 * @Description: 人员离职
 * @author: 稠云技术
 * @date: 2020年4月27日 下午11:34:56
 * @Copyright: 2020 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "hr_level_office")
public class HrLeaveOffice implements Serializable {

}
