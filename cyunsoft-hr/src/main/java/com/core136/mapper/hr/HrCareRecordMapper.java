package com.core136.mapper.hr;

import com.core136.bean.hr.HrCareRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface HrCareRecordMapper extends MyMapper<HrCareRecord> {
    /**
     * @param orgId
     * @param userId
     * @param beginTime
     * @param endTime
     * @param careType
     * @param search
     * @return List<Map < String, String>>
     * @Title: getHrCareRecordList
     * @Description:  获取人员关怀列表
     */
    public List<Map<String, String>> getHrCareRecordList(@Param(value = "orgId") String orgId,
                                                         @Param(value = "userId") String userId, @Param(value = "beginTime") String beginTime,
                                                         @Param(value = "endTime") String endTime, @Param(value = "careType") String careType,
                                                         @Param(value = "search") String search);
}
