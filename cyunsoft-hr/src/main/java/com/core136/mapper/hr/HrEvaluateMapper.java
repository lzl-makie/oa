package com.core136.mapper.hr;

import com.core136.bean.hr.HrEvaluate;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface HrEvaluateMapper extends MyMapper<HrEvaluate> {

    /**
     * @param orgId
     * @param userId
     * @return List<Map < String, String>>
     * @Title: getHrEvaluateByUserIdList
     * @Description:  获取人员评价列表
     */
    public List<Map<String, String>> getHrEvaluateByUserIdList(@Param(value = "orgId") String orgId, @Param(value = "userId") String userId);

    /**
     * @param orgId
     * @param userId
     * @param beginTime
     * @param endTime
     * @param status
     * @param search
     * @return List<Map < String, String>>
     * @Title: getHrEvaluateQueryList
     * @Description:  获取查询人员评价列表
     */
    public List<Map<String, String>> getHrEvaluateQueryList(@Param(value = "orgId") String orgId, @Param(value = "userId") String userId,
                                                            @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "status") String status,
                                                            @Param(value = "search") String search
    );


}
