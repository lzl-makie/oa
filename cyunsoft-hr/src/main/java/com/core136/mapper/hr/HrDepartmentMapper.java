/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: HrDepartmentMapper.java
 * @Package com.core136.mapper.hr
 * @Description: 描述
 * @author: lsq
 * @date: 2019年10月29日 上午9:29:47
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.mapper.hr;

import com.core136.bean.hr.HrDepartment;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 *
 */
@Mapper
public interface HrDepartmentMapper extends MyMapper<HrDepartment> {

    /**
     *
     * @Title: getHrDepartmentTree
     * @Description:  获取HR部门树
     * @param: orgLevelId
     * @param: orgId
     * @param: @return
     * @return: List<Map < String, Object>>

     */
    public List<Map<String, String>> getHrDepartmentTree(@Param(value = "orgId") String orgId, @Param(value = "orgLevelId") String orgLevelId);

    /**
     *
     * @Title: getHrUserInfoDepartmentTree
     * @Description:  人员基本信息树结构
     * @param: orgId
     * @param: orgLevelId
     * @param: @return
     * @return: List<Map < String, String>>

     */
    public List<Map<String, String>> getHrUserInfoDepartmentTree(@Param(value = "orgId") String orgId, @Param(value = "orgLevelId") String orgLevelId);

    /**
     *
     * @Title: getHrDeptNameByStr
     * @Description:  获取部门名称列表
     * @param orgId
     * @param list
     * @return
     * List<Map < String, String>>

     */
    public List<Map<String, String>> getHrDeptNameByStr(@Param(value = "orgId") String orgId, @Param(value = "list") List<String> list);

    /**
     *
     * @Title: getHrDeptBySearchdept
     * @Description:  查询部门
     * @param orgId
     * @param searchdept
     * @return
     * List<Map < String, String>>

     */
    public List<Map<String, String>> getHrDeptBySearchdept(@Param(value = "orgId") String orgId, @Param(value = "searchdept") String searchdept);

}
