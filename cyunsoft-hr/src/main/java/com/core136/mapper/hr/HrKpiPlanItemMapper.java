package com.core136.mapper.hr;

import com.core136.bean.hr.HrKpiPlanItem;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface HrKpiPlanItemMapper extends MyMapper<HrKpiPlanItem> {

    /**
     * @param orgId
     * @param planId
     * @return List<Map < String, String>>
     * @Title: getHrKpiPlanItemList
     * @Description:  获取考核计划的指标集
     */
    public List<Map<String, String>> getHrKpiPlanItemList(@Param(value = "orgId") String orgId, @Param(value = "planId") String planId);

}
