/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: HrUserInfoMapper.java
 * @Package com.core136.mapper.hr
 * @Description: 描述
 * @author: lsq
 * @date: 2019年10月29日 上午9:34:31
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.mapper.hr;

import com.core136.bean.hr.HrUserInfo;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 *
 */
@Mapper
public interface HrUserInfoMapper extends MyMapper<HrUserInfo> {
    /**
     *
     * @Title: getHrUserInfoByDeptId
     * @Description:  获取部门下的人员列表
     * @param: orgId
     * @param: deptId
     * @param: search
     * @param: @return
     * @return: List<Map < String, String>>

     */
    public List<Map<String, String>> getHrUserInfoByDeptId(@Param(value = "orgId") String orgId, @Param(value = "deptId") String deptId);

    /**
     *
     * @Title: getHrUserInfoByBeptIdInWorkList
     * @Description:  获取部门下的人员
     * @param orgId
     * @param deptId
     * @return
     * List<Map < String, String>>

     */
    public List<Map<String, String>> getHrUserInfoByBeptIdInWorkList(@Param(value = "orgId") String orgId,
                                                                     @Param(value = "deptId") String deptId, @Param(value = "workStatus") String workStatus,
                                                                     @Param(value = "employedTime") String employedTime, @Param(value = "staffCardNo") String staffCardNo,
                                                                     @Param(value = "search") String search);

    /**
     *
     * @Title: getHrUserInfoForTree
     * @Description:  按部门获取人员列表
     * @param: orgId
     * @param: deptId
     * @param: @return
     * @return: List<Map < String, String>>

     */
    public List<Map<String, String>> getHrUserInfoForTree(@Param(value = "orgId") String orgId, @Param(value = "deptId") String deptId);

    /**
     *
     * @Title: getUserNamesByUserIds
     * @Description:  获取HR人员列表
     * @param orgId
     * @param list
     * @return
     * List<Map < String, String>>

     */
    public List<Map<String, String>> getUserNamesByUserIds(@Param(value = "orgId") String orgId, @Param(value = "list") List<String> list);

    /**
     *
     * @Title: getHrUserInfoBySearchuser
     * @Description:  查询HR人员
     * @param orgId
     * @param searchuser
     * @return
     * List<Map < String, String>>

     */
    public List<Map<String, String>> getHrUserInfoBySearchuser(@Param(value = "orgId") String orgId, @Param(value = "searchuser") String searchuser);

    /**
     *
     * @Title: getHrUserInfoListByDeptId
     * @Description:  获取部门下的人员列表
     * @param orgId
     * @param deptId
     * @param search
     * @return
     * List<Map < String, String>>

     */
    public List<Map<String, String>> getHrUserInfoListByDeptId(@Param(value = "orgId") String orgId, @Param(value = "deptId") String deptId, @Param(value = "search") String search);

    /**
     *
     * @Title: getDeskHrUserInfo
     * @Description:  获取人力资源门户人员信息
     * @param orgId
     * @return
     * Map<String, String>

     */
    public Map<String, String> getDeskHrUserInfo(@Param(value = "orgId") String orgId);

}
