package com.core136.service.hr;

import com.core136.bean.hr.HrRecruitTask;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.hr.HrRecruitTaskMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class HrRecruitTaskService {
    private HrRecruitTaskMapper hrRecruitTaskMapper;

    @Autowired
    public void setHrRecruitTaskMapper(HrRecruitTaskMapper hrRecruitTaskMapper) {
        this.hrRecruitTaskMapper = hrRecruitTaskMapper;
    }

    public int insertHrRecruitTask(HrRecruitTask hrRecruitTask) {
        return hrRecruitTaskMapper.insert(hrRecruitTask);
    }

    public int deleteHrRecruitTask(HrRecruitTask hrRecruitTask) {
        return hrRecruitTaskMapper.delete(hrRecruitTask);
    }

    public int updateHrRecruitTask(Example example, HrRecruitTask hrRecruitTask) {
        return hrRecruitTaskMapper.updateByExampleSelective(hrRecruitTask, example);
    }

    public HrRecruitTask selectOneHrRecruitTask(HrRecruitTask hrRecruitTask) {
        return hrRecruitTaskMapper.selectOne(hrRecruitTask);
    }

    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getHrRecruitTaskList
     * @Description:  获取招聘任务列表
     */
    public List<Map<String, String>> getHrRecruitTaskList(String orgId, String opFlag, String accountId, String beginTime, String endTime, String search) {
        return hrRecruitTaskMapper.getHrRecruitTaskList(orgId, opFlag, accountId, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getHrRecruitTaskList
     * @Description:  获取招聘任务列表
     */
    public PageInfo<Map<String, String>> getHrRecruitTaskList(PageParam pageParam, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHrRecruitTaskList(pageParam.getOrgId(), pageParam.getOpFlag(), pageParam.getAccountId(), beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }


}
