package com.core136.service.hr;

import com.alibaba.fastjson.JSONArray;
import com.core136.bean.account.Account;
import com.core136.bean.hr.HrKpiPlanRecord;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.mapper.hr.HrKpiPlanRecordMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class HrKpiPlanRecordService {
    private HrKpiPlanRecordMapper hrKpiPlanRecordMapper;

    @Autowired
    public void setHrKpiPlanRecordMapper(HrKpiPlanRecordMapper hrKpiPlanRecordMapper) {
        this.hrKpiPlanRecordMapper = hrKpiPlanRecordMapper;
    }

    public int insertHrKpiPlanRecord(HrKpiPlanRecord hrKpiPlanRecord) {
        return hrKpiPlanRecordMapper.insert(hrKpiPlanRecord);
    }

    public int deleteHrKpiPlanRecord(HrKpiPlanRecord hrKpiPlanRecord) {
        return hrKpiPlanRecordMapper.delete(hrKpiPlanRecord);
    }

    public int updateHrKpiPlanRecord(Example example, HrKpiPlanRecord hrKpiPlanRecord) {
        return hrKpiPlanRecordMapper.updateByExampleSelective(hrKpiPlanRecord, example);
    }

    public HrKpiPlanRecord selectOneHrKpiPlanRecord(HrKpiPlanRecord hrKpiPlanRecord) {
        return hrKpiPlanRecordMapper.selectOne(hrKpiPlanRecord);
    }

    /**
     * @param orgId
     * @param planId
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getMyHrKpiScoreList
     * @Description:  获取个人考核分数明细
     */
    public List<Map<String, String>> getMyHrKpiScoreList(String orgId, String planId, String accountId) {
        return hrKpiPlanRecordMapper.getMyHrKpiScoreList(orgId, planId, accountId);
    }


    /**
     * @param hrKpiPlanRecord
     * @return int
     * @Title: getHrKpiPlanRecordConnt
     * @Description:  获取总数
     */
    public int getHrKpiPlanRecordConnt(HrKpiPlanRecord hrKpiPlanRecord) {
        return hrKpiPlanRecordMapper.selectCount(hrKpiPlanRecord);
    }

    /**
     * @param account
     * @param planId
     * @param accountId
     * @param scoreList
     * @return RetDataBean
     * @Title: setScoreToUser
     * @Description:  设置考核分
     */
    @Transactional(value = "generalTM")
    public RetDataBean setScoreToUser(Account account, String planId, String accountId, String scoreList) {
        try {
            JSONArray jsonArray = JSONArray.parseArray(scoreList);
            for (int i = 0; i < jsonArray.size(); i++) {
                String itemId = jsonArray.getJSONObject(i).getString("itemId");
                if (StringUtils.isNotBlank(itemId)) {
                    HrKpiPlanRecord hrKpiPlanRecord = new HrKpiPlanRecord();
                    hrKpiPlanRecord.setScore(jsonArray.getJSONObject(i).getDouble("score"));
                    hrKpiPlanRecord.setComment(jsonArray.getJSONObject(i).getString("comment"));
                    Example example = new Example(HrKpiPlanRecord.class);
                    example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("planId", planId)
                            .andEqualTo("accountId", accountId).andEqualTo("itemId", itemId);
                    updateHrKpiPlanRecord(example, hrKpiPlanRecord);
                }
            }
            HrKpiPlanRecord hrKpiPlanRecord1 = new HrKpiPlanRecord();
            hrKpiPlanRecord1.setStatus("1");
            Example example1 = new Example(HrKpiPlanRecord.class);
            example1.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("planId", planId)
                    .andEqualTo("accountId", accountId);
            updateHrKpiPlanRecord(example1, hrKpiPlanRecord1);
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }

    }


}
