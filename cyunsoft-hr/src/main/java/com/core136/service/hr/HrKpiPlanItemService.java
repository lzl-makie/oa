package com.core136.service.hr;

import com.core136.bean.hr.HrKpiPlanItem;
import com.core136.mapper.hr.HrKpiPlanItemMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class HrKpiPlanItemService {
    private HrKpiPlanItemMapper hrKpiPlanItemMapper;

    @Autowired
    public void setHrKpiPlanItemMapper(HrKpiPlanItemMapper hrKpiPlanItemMapper) {
        this.hrKpiPlanItemMapper = hrKpiPlanItemMapper;
    }

    public int insertHrKpiPlanItem(HrKpiPlanItem hrKpiPlanItem) {
        return hrKpiPlanItemMapper.insert(hrKpiPlanItem);
    }

    public int deleteHrKpiPlanItem(HrKpiPlanItem hrKpiPlanItem) {
        return hrKpiPlanItemMapper.delete(hrKpiPlanItem);
    }

    public int updateHrKpiPlanItem(Example example, HrKpiPlanItem hrKpiPlanItem) {
        return hrKpiPlanItemMapper.updateByExampleSelective(hrKpiPlanItem, example);
    }

    public HrKpiPlanItem selectOneHrKpiPlanItem(HrKpiPlanItem hrKpiPlanItem) {
        return hrKpiPlanItemMapper.selectOne(hrKpiPlanItem);
    }

    public int getHrKpiPlanItemCount(HrKpiPlanItem hrKpiPlanItem) {
        return hrKpiPlanItemMapper.selectCount(hrKpiPlanItem);
    }


    public List<HrKpiPlanItem> getHrKpiPlanItemList(HrKpiPlanItem hrKpiPlanItem) {
        return hrKpiPlanItemMapper.select(hrKpiPlanItem);
    }

    /**
     * @param orgId
     * @param planId
     * @return List<Map < String, String>>
     * @Title: getHrKpiPlanItemList
     * @Description:  获取考核计划的指标集
     */
    public List<Map<String, String>> getHrKpiPlanItemList(String orgId, String planId) {
        return hrKpiPlanItemMapper.getHrKpiPlanItemList(orgId, planId);
    }

}
