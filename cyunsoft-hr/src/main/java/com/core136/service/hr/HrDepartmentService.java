/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: HrDepartmentService.java
 * @Package com.core136.service.hr
 * @Description: 描述
 * @author: lsq
 * @date: 2019年10月29日 上午9:30:40
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.service.hr;

import com.core136.bean.account.Account;
import com.core136.bean.hr.HrDepartment;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.ExcelUtil;
import com.core136.common.utils.SysTools;
import com.core136.mapper.hr.HrDepartmentMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import java.io.IOException;
import java.util.*;

/**
 * @author lsq
 */
@Service
public class HrDepartmentService {
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private HrDepartmentMapper hrDepartmentMapper;

    @Autowired
    public void setHrDepartmentMapper(HrDepartmentMapper hrDepartmentMapper) {
        this.hrDepartmentMapper = hrDepartmentMapper;
    }

    public int insertHrDepartment(HrDepartment hrDepartment) {
        return hrDepartmentMapper.insert(hrDepartment);
    }

    public int deleteHrDepartment(HrDepartment hrDepartment) {
        return hrDepartmentMapper.delete(hrDepartment);
    }

    public int updateHrDepartment(Example example, HrDepartment hrDepartment) {
        return hrDepartmentMapper.updateByExampleSelective(hrDepartment, example);
    }

    public HrDepartment selectOneHrDepartment(HrDepartment hrDepartment) {
        return hrDepartmentMapper.selectOne(hrDepartment);
    }

    /**
     * @Title: getHrDepartmentTree
     * @Description: 获取HR部门树
     * @param: orgId
     * @param: orgLevelId
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getHrDepartmentTree(String orgId, String orgLevelId) {
        return hrDepartmentMapper.getHrDepartmentTree(orgId, orgLevelId);
    }

    /**
     * @Title: getHrUserInfoDepartmentTree
     * @Description:  人员基本信息树结构
     * @param: orgId
     * @param: orgLevelId
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getHrUserInfoDepartmentTree(String orgId, String orgLevelId) {
        return hrDepartmentMapper.getHrUserInfoDepartmentTree(orgId, orgLevelId);
    }

    /**
     * @param example
     * @return List<HrDepartment>
     * @Title: getHrDeptList
     * @Description:  获取子部门列表
     */
    public List<HrDepartment> getHrDeptList(Example example) {
        return hrDepartmentMapper.selectByExample(example);
    }

    /**
     * @param orgId
     * @param deptStrs
     * @return List<Map < String, String>>
     * @Title: getHrDeptNameByStr
     * @Description:  获取部门名称列表
     */
    public List<Map<String, String>> getHrDeptNameByStr(String orgId, String deptStrs) {
        if (StringUtils.isNotBlank(deptStrs)) {
            String[] deptArr = deptStrs.split(",");
            List<String> list = Arrays.asList(deptArr);
            return hrDepartmentMapper.getHrDeptNameByStr(orgId, list);
        } else {
            return null;
        }
    }

    /**
     * @param orgId
     * @param searchdept
     * @return List<Map < String, String>>
     * @Title: getHrDeptBySearchdept
     * @Description:  查询HR部门
     */
    public List<Map<String, String>> getHrDeptBySearchdept(String orgId, String searchdept) {
        return hrDepartmentMapper.getHrDeptBySearchdept(orgId, "%" + searchdept + "%");
    }


    /**
     * 导入工会部门
     *
     * @param account
     * @param file
     * @return
     * @throws IOException
     */
    @Transactional(value = "generalTM")
    public RetDataBean importHrDepartment(Account account, MultipartFile file) throws IOException {
        Map<String, String> fieldMap = new HashMap<String, String>();
        fieldMap.put("排序号", "sort_no");
        fieldMap.put("部门名称", "dept_name");
        fieldMap.put("上级部门", "org_level_id");
        fieldMap.put("电话", "dept_tel");
        fieldMap.put("电子邮件", "dept_email");
        fieldMap.put("传真", "dept_fax");
        fieldMap.put("部门领导", "dept_lead");
        fieldMap.put("部门职责", "dept_function");
        List<String> fieldList = new ArrayList<String>();
        List<String> titleList = new ArrayList<String>();
        for (Map.Entry<String, String> entry : fieldMap.entrySet()) {
            fieldList.add(entry.getValue());
            titleList.add(entry.getKey());
        }
        String[] fieldArr = new String[fieldList.size()];
        fieldList.toArray(fieldArr);
        String fieldString = StringUtils.join(fieldArr, ",");
        List<Map<String, String>> recordList = ExcelUtil.readExcel(file);
        List<Map<String, String>> oneList = getOneDeptListMap(recordList);
        if (oneList.size() > 0) {
            for (int i = 0; i < oneList.size(); i++) {
                Map<String, String> tempMap = oneList.get(i);
                String valueString = "'" + SysTools.getGUID() + "',";
                for (int k = 0; k < titleList.size(); k++) {
                    if (titleList.get(k).equals("上级部门")) {
                        valueString += "'0',";
                    } else {
                        valueString += "'" + tempMap.get(titleList.get(k)) + "',";
                    }
                }
                valueString += "'/','0','" + account.getOrgId() + "'";
                String insertSql = "insert into hr_department(dept_id," + fieldString + ",level_id,is_revoke,org_id) values" + "(" + valueString + ")";
                jdbcTemplate.execute(insertSql);
            }

        }
        List<Map<String, String>> twoList = getTwoDeptListMap(recordList);
        if (twoList.size() > 0) {
            for (int i = 0; i < twoList.size(); i++) {
                String levelId = "";
                Map<String, String> tempMap = twoList.get(i);
                String valueString = "'" + SysTools.getGUID() + "',";
                for (int k = 0; k < titleList.size(); k++) {
                    if (titleList.get(k).equals("上级部门")) {
                        String deptName = tempMap.get(titleList.get(k));
                        String sql = "select dept_id from hr_department where dept_name='" + deptName + "' and org_id='" + account.getOrgId() + "'";
                        String deptId = jdbcTemplate.queryForObject(sql, String.class);
                        valueString += "'" + deptId + "',";
                        levelId = "/" + deptId;
                    } else {
                        valueString += "'" + tempMap.get(titleList.get(k)) + "',";
                    }
                }
                valueString += "'" + levelId + "','0','" + account.getOrgId() + "'";
                String insertSql = "insert into hr_department(dept_id," + fieldString + ",level_id,is_revoke,org_id) values" + "(" + valueString + ")";
                jdbcTemplate.execute(insertSql);
            }
        }

        List<Map<String, String>> threeList = getThreeDeptListMap(recordList);
        if (threeList.size() > 0) {
            for (int i = 0; i < threeList.size(); i++) {
                String levelId = "";
                Map<String, String> tempMap = threeList.get(i);
                String valueString = "'" + SysTools.getGUID() + "',";
                for (int k = 0; k < titleList.size(); k++) {
                    if (titleList.get(k).equals("上级部门")) {

                        String deptName = tempMap.get(titleList.get(k)).split("/")[1];
                        String sql = "select dept_id from hr_department where dept_name='" + deptName + "' and org_id='" + account.getOrgId() + "'";
                        String deptId = jdbcTemplate.queryForObject(sql, String.class);
                        valueString += "'" + deptId + "',";
                        HrDepartment parentUnitDept = new HrDepartment();
                        parentUnitDept.setDeptId(deptId);
                        parentUnitDept.setOrgId(account.getOrgId());
                        parentUnitDept = selectOneHrDepartment(parentUnitDept);
                        levelId = parentUnitDept.getLevelId() + "/" + deptId;
                    } else {
                        valueString += "'" + tempMap.get(titleList.get(k)) + "',";
                    }
                }
                valueString += "'" + levelId + "','0','" + account.getOrgId() + "'";
                String insertSql = "insert into hr_department(dept_id," + fieldString + ",level_id,is_revoke,org_id) values" + "(" + valueString + ")";
                jdbcTemplate.execute(insertSql);
            }
        }

        List<Map<String, String>> fourList = getFourDeptListMap(recordList);
        if (fourList.size() > 0) {
            for (int i = 0; i < fourList.size(); i++) {
                String levelId = "";
                Map<String, String> tempMap = fourList.get(i);
                String valueString = "'" + SysTools.getGUID() + "',";
                for (int k = 0; k < titleList.size(); k++) {
                    if (titleList.get(k).equals("上级部门")) {
                        String deptName = tempMap.get(titleList.get(k)).split("/")[2];
                        String sql = "select dept_id from hr_department where dept_name='" + deptName + "' and org_id='" + account.getOrgId() + "'";
                        String deptId = jdbcTemplate.queryForObject(sql, String.class);
                        valueString += "'" + deptId + "',";
                        HrDepartment parentUnitDept = new HrDepartment();
                        parentUnitDept.setDeptId(deptId);
                        parentUnitDept.setOrgId(account.getOrgId());
                        parentUnitDept = selectOneHrDepartment(parentUnitDept);
                        levelId = parentUnitDept.getLevelId() + "/" + deptId;
                    } else {
                        valueString += "'" + tempMap.get(titleList.get(k)) + "',";
                    }
                }
                valueString += "'" + levelId + "','0','" + account.getOrgId() + "'";
                String insertSql = "insert into hr_department(dept_id," + fieldString + ",level_id,is_revoke,org_id) values" + "(" + valueString + ")";
                jdbcTemplate.execute(insertSql);
            }
        }
        return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
    }

    /**
     * 获取第一层部门
     *
     * @param recordList
     * @return
     */
    public List<Map<String, String>> getOneDeptListMap(List<Map<String, String>> recordList) {
        List<Map<String, String>> retList = new ArrayList<Map<String, String>>();
        for (int i = 0; i < recordList.size(); i++) {
            Map<String, String> tempMap = recordList.get(i);
            String tempStr = tempMap.get("上级部门");
            if (StringUtils.isBlank(tempStr)) {
                retList.add(tempMap);
            }
        }
        return retList;
    }

    /**
     * 获取第二层部门列表
     *
     * @param recordList
     * @return
     */
    public List<Map<String, String>> getTwoDeptListMap(List<Map<String, String>> recordList) {
        List<Map<String, String>> retList = new ArrayList<Map<String, String>>();
        for (int i = 0; i < recordList.size(); i++) {
            Map<String, String> tempMap = recordList.get(i);
            String tempStr = tempMap.get("上级部门");
            if (StringUtils.isNotBlank(tempStr)) {
                if (SysTools.countStr(tempStr, "/") == 0) {
                    retList.add(tempMap);
                }
            }
        }
        return retList;
    }

    /**
     * 获取第三层部门列表
     *
     * @param recordList
     * @return
     */
    public List<Map<String, String>> getThreeDeptListMap(List<Map<String, String>> recordList) {
        List<Map<String, String>> retList = new ArrayList<Map<String, String>>();
        for (int i = 0; i < recordList.size(); i++) {
            Map<String, String> tempMap = recordList.get(i);
            String tempStr = tempMap.get("上级部门");
            if (StringUtils.isNotBlank(tempStr)) {
                if (SysTools.countStr(tempStr, "/") == 1) {
                    retList.add(tempMap);
                }
            }
        }
        return retList;
    }

    /**
     * 第四级部门列表
     *
     * @param recordList
     * @return
     */
    public List<Map<String, String>> getFourDeptListMap(List<Map<String, String>> recordList) {
        List<Map<String, String>> retList = new ArrayList<Map<String, String>>();
        for (int i = 0; i < recordList.size(); i++) {
            Map<String, String> tempMap = recordList.get(i);
            String tempStr = tempMap.get("上级部门");
            if (StringUtils.isNotBlank(tempStr)) {
                if (SysTools.countStr(tempStr, "/") == 2) {
                    retList.add(tempMap);
                }
            }
        }
        return retList;
    }

}
