/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: HrLicenceService.java
 * @Package com.core136.service.hr
 * @Description: 描述
 * @author: lsq
 * @date: 2019年12月31日 上午10:09:25
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.service.hr;

import com.core136.bean.account.Account;
import com.core136.bean.hr.HrLicence;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.ExcelUtil;
import com.core136.common.utils.SysTools;
import com.core136.mapper.hr.HrLicenceMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lsq
 */
@Service
public class HrLicenceService {
    private HrLicenceMapper hrLicenceMapper;

    @Autowired
    public void setHrLicenceMapper(HrLicenceMapper hrLicenceMapper) {
        this.hrLicenceMapper = hrLicenceMapper;
    }

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public int insertHrLicence(HrLicence hrLicence) {
        return hrLicenceMapper.insert(hrLicence);
    }

    public int deleteHrLicence(HrLicence hrLicence) {
        return hrLicenceMapper.delete(hrLicence);
    }

    public int updateHrLicence(Example example, HrLicence hrLicence) {
        return hrLicenceMapper.updateByExampleSelective(hrLicence, example);
    }

    public HrLicence selectOneHrLicence(HrLicence hrLicence) {
        return hrLicenceMapper.selectOne(hrLicence);
    }

    /**
     * @param orgId
     * @param userId
     * @param beginTime
     * @param endTime
     * @param licenceType
     * @param search
     * @return List<Map < String, String>>
     * @Title: getHrLicenceList
     * @Description:  获取证照列表
     */
    public List<Map<String, String>> getHrLicenceList(String orgId, String userId, String beginTime, String endTime, String licenceType, String search) {
        return hrLicenceMapper.getHrLicenceList(orgId, userId, beginTime, endTime, licenceType, "%" + search + "%");
    }

    /**
     * @param orgId
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getMyHrLicenceList
     * @Description:  查询个人证照信息
     */
    public List<Map<String, String>> getMyHrLicenceList(String orgId, String accountId) {
        return hrLicenceMapper.getMyHrLicenceList(orgId, accountId);
    }

    /**
     * @param pageParam
     * @param userId
     * @param beginTime
     * @param endTime
     * @param licenceType
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getHrLicenceList
     * @Description:  获取证照列表
     */
    public PageInfo<Map<String, String>> getHrLicenceList(PageParam pageParam, String userId, String beginTime, String endTime, String licenceType) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHrLicenceList(pageParam.getOrgId(), userId, beginTime, endTime, licenceType, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param pageParam
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getMyHrLicenceList
     * @Description:  查询个人证照信息
     */
    public PageInfo<Map<String, String>> getMyHrLicenceList(PageParam pageParam) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyHrLicenceList(pageParam.getOrgId(), pageParam.getAccountId());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param account
     * @param file
     * @return IOException
     * RetDataBean
     * @Title: importHrLicence
     * @Description:  证照记录导入
     */
    @Transactional(value = "generalTM")
    public RetDataBean importHrLicence(Account account, MultipartFile file) throws IOException {
        Map<String, String> fieldMap = new HashMap<String, String>();
        fieldMap.put("排序号", "sort_no");
        fieldMap.put("持证人员", "user_id");
        fieldMap.put("证照名称", "name");
        fieldMap.put("证照编号", "licence_code");
        fieldMap.put("生效日期", "begin_time");
        fieldMap.put("截止日期", "end_time");
        fieldMap.put("发证机构", "notified_body");
        fieldMap.put("证照类型", "licence_type");
        fieldMap.put("备注", "remark");
        List<String> fieldList = new ArrayList<String>();
        List<String> titleList = new ArrayList<String>();
        for (Map.Entry<String, String> entry : fieldMap.entrySet()) {
            fieldList.add(entry.getValue());
            titleList.add(entry.getKey());
        }
        String[] fieldArr = new String[fieldList.size()];
        fieldList.toArray(fieldArr);
        String fieldString = StringUtils.join(fieldArr, ",");
        List<Map<String, String>> recordList = ExcelUtil.readExcel(file);
        for (int i = 0; i < recordList.size(); i++) {
            Map<String, String> tempMap = recordList.get(i);
            String valueString = "'" + SysTools.getGUID() + "',";
            for (int k = 0; k < titleList.size(); k++) {
                if (titleList.get(k).equals("持证人员")) {
                    if (StringUtils.isNotBlank(tempMap.get(titleList.get(k)))) {
                        String sql1 = "select user_id from hr_user_info where user_name='" + tempMap.get(titleList.get(k)) + "' and org_id='" + account.getOrgId() + "'";
                        String userId = jdbcTemplate.queryForObject(sql1, String.class);
                        valueString += "'" + userId + "',";
                    } else {
                        valueString += "'',";
                    }
                } else {
                    valueString += "'" + tempMap.get(titleList.get(k)) + "',";
                }
            }
            valueString += "'" + SysTools.getTime("yyyy-MM-dd HH:mm:ss") + "',";
            valueString += "'" + account.getAccountId() + "',";
            valueString += "'" + account.getOrgId() + "'";
            String insertSql = "insert into hr_licence(licence_id," + fieldString + ",create_time,create_user,org_id) values" + "(" + valueString + ")";
            jdbcTemplate.execute(insertSql);
        }
        return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
    }

}
