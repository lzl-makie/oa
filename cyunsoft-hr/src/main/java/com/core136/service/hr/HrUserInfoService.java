/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: HrUserInfoService.java
 * @Package com.core136.service.hr
 * @Description: 描述
 * @author: lsq
 * @date: 2019年10月29日 上午9:35:19
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.service.hr;

import com.core136.bean.account.Account;
import com.core136.bean.hr.HrUserInfo;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.ExcelUtil;
import com.core136.common.utils.SysTools;
import com.core136.mapper.hr.HrUserInfoMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import java.io.IOException;
import java.util.*;

/**
 * @author lsq
 */

@Service
public class HrUserInfoService {
    private HrUserInfoMapper hrUserInfoMapper;

    @Autowired
    public void setHrUserInfoMapper(HrUserInfoMapper hrUserInfoMapper) {
        this.hrUserInfoMapper = hrUserInfoMapper;
    }

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public int insertHrUserInfo(HrUserInfo hrUserInfo) {
        return hrUserInfoMapper.insert(hrUserInfo);
    }

    public int deleteHrUserInfo(HrUserInfo hrUserInfo) {
        return hrUserInfoMapper.delete(hrUserInfo);
    }

    public int updateHrUserInfo(Example example, HrUserInfo hrUserInfo) {
        return hrUserInfoMapper.updateByExampleSelective(hrUserInfo, example);
    }

    public HrUserInfo selectOneHrUserInfo(HrUserInfo hrUserInfo) {
        return hrUserInfoMapper.selectOne(hrUserInfo);
    }

    /**
     * @Title: getHrUserInfoByDeptId
     * @Description:  获取部门下的人员列表
     * @param: orgId
     * @param: deptId
     * @param: search
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getHrUserInfoByDeptId(String orgId, String deptId) {
        return hrUserInfoMapper.getHrUserInfoByDeptId(orgId, deptId);
    }

    /**
     * @param orgId
     * @param deptId
     * @return List<Map < String, String>>
     * @Title: getHrUserInfoByBeptIdInWorkList
     * @Description:  获取部门下的人员列表
     */
    public List<Map<String, String>> getHrUserInfoByBeptIdInWorkList(String orgId, String deptId, String workStatus, String employedTime, String staffCardNo, String search) {
        return hrUserInfoMapper.getHrUserInfoByBeptIdInWorkList(orgId, deptId, workStatus, employedTime, staffCardNo, "%" + search + "%");
    }


    /**
     * @param pageParam
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getHrUserInfoByBeptIdInWorkList
     * @Description:  获取部门下的人员列表
     */
    public PageInfo<Map<String, String>> getHrUserInfoByBeptIdInWorkList(PageParam pageParam, String workStatus, String employedTime, String staffCardNo) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHrUserInfoByBeptIdInWorkList(pageParam.getOrgId(), pageParam.getDeptId(), workStatus, employedTime, staffCardNo, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param deptId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getHrUserInfoListByDeptId
     * @Description:  获取部门下的人员列表
     */
    public List<Map<String, String>> getHrUserInfoListByDeptId(String orgId, String deptId, String search) {
        return hrUserInfoMapper.getHrUserInfoListByDeptId(orgId, deptId, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getHrUserInfoListByDeptId
     * @Description:  获取部门下的人员列表
     */
    public PageInfo<Map<String, String>> getHrUserInfoListByDeptId(PageParam pageParam) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHrUserInfoListByDeptId(pageParam.getOrgId(), pageParam.getDeptId(), pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @Title: getHrUserInfoForTree
     * @Description:  按部门获取人员列表
     * @param: orgId
     * @param: deptId
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getHrUserInfoForTree(String orgId, String deptId) {
        return hrUserInfoMapper.getHrUserInfoForTree(orgId, deptId);
    }

    /**
     * @param orgId
     * @param userIds
     * @return List<Map < String, String>>
     * @Title: getUserNamesByUserIds
     * @Description:  获取HR人员列表
     */
    public List<Map<String, String>> getUserNamesByUserIds(String orgId, String userIds) {
        if (StringUtils.isNotBlank(userIds)) {
            String[] userIdArr = userIds.split(",");
            List<String> list = Arrays.asList(userIdArr);
            return hrUserInfoMapper.getUserNamesByUserIds(orgId, list);
        } else {
            return null;
        }
    }

    /**
     * @param orgId
     * @param searchuser
     * @return List<Map < String, String>>
     * @Title: getHrUserInfoBySearchuser
     * @Description:  查询HR人员
     */
    public List<Map<String, String>> getHrUserInfoBySearchuser(String orgId, String searchuser) {
        return hrUserInfoMapper.getHrUserInfoBySearchuser(orgId, "%" + searchuser + "%");
    }

    /**
     * @param orgId
     * @return Map<String, String>
     * @Title: getDeskHrUserInfo
     * @Description:  获取人力资源门户人员信息
     */
    public Map<String, String> getDeskHrUserInfo(String orgId) {
        return hrUserInfoMapper.getDeskHrUserInfo(orgId);
    }


    /**
     * @param account
     * @param file
     * @return IOException
     * RetDataBean
     * @Title: importHrUserInfo
     * @Description:  人事档案信息导入
     */
    @Transactional(value = "generalTM")
    public RetDataBean importHrUserInfo(Account account, MultipartFile file) throws IOException {
        Map<String, String> fieldMap = new HashMap<String, String>();
        fieldMap.put("排序号", "sort_no");
        fieldMap.put("姓名", "user_name");
        fieldMap.put("OA用户名", "account_id");
        fieldMap.put("部门", "dept_id");
        fieldMap.put("职务", "level_id");
        fieldMap.put("英文名", "user_name_en");
        fieldMap.put("曾用名", "before_user_name");
        fieldMap.put("性别", "sex");
        fieldMap.put("工号", "work_no");
        fieldMap.put("编号", "staff_no");
        fieldMap.put("在职状态", "work_status");
        fieldMap.put("身份证号", "staff_card_no");
        fieldMap.put("出生日期", "birth_day");
        fieldMap.put("籍贯", "native_place");
        fieldMap.put("民族", "nationalty");
        fieldMap.put("婚姻状况", "marital_status");
        fieldMap.put("政治面貌", "political_status");
        fieldMap.put("入党时间", "join_party_time");
        fieldMap.put("岗位", "work_job");
        fieldMap.put("入职时间", "employed_time");
        fieldMap.put("手机号码", "mobile_no");
        fieldMap.put("学历", "highset_shool");
        fieldMap.put("毕业时间", "graduation_time");
        fieldMap.put("毕业学校", "graduation_shool");
        fieldMap.put("专业", "marjor");
        List<String> fieldList = new ArrayList<String>();
        List<String> titleList = new ArrayList<String>();
        for (Map.Entry<String, String> entry : fieldMap.entrySet()) {
            fieldList.add(entry.getValue());
            titleList.add(entry.getKey());
        }
        String[] fieldArr = new String[fieldList.size()];
        fieldList.toArray(fieldArr);
        String fieldString = StringUtils.join(fieldArr, ",");
        List<Map<String, String>> recordList = ExcelUtil.readExcel(file);
        for (int i = 0; i < recordList.size(); i++) {
            Map<String, String> tempMap = recordList.get(i);
            String valueString = "'" + SysTools.getGUID() + "',";
            for (int k = 0; k < titleList.size(); k++) {
                if (titleList.get(k).equals("部门")) {
                    if (StringUtils.isNotBlank(tempMap.get(titleList.get(k)))) {
                        String sql1 = "select dept_id from hr_department where dept_name ='" + tempMap.get(titleList.get(k)) + "' and org_id='" + account.getOrgId() + "'";
                        String deptId = jdbcTemplate.queryForObject(sql1, String.class);
                        valueString += "'" + deptId + "',";
                    } else {
                        valueString += "'',";
                    }
                } else if (titleList.get(k).equals("职务")) {
                    if (StringUtils.isNotBlank(tempMap.get(titleList.get(k)))) {
                        String sql2 = "select level_id from hr_user_level where level_name ='" + tempMap.get(titleList.get(k)) + "' and org_id='" + account.getOrgId() + "'";
                        String levelId = jdbcTemplate.queryForObject(sql2, String.class);
                        valueString += "'" + levelId + "',";
                    } else {
                        valueString += "'',";
                    }
                } else if (titleList.get(k).equals("姓名")) {
                    if (StringUtils.isNotBlank(tempMap.get(titleList.get(k)))) {
                        String sql1 = "select user_id from hr_user_info where user_name ='" + tempMap.get(titleList.get(k)) + "' and org_id='" + account.getOrgId() + "'";
                        String userId = jdbcTemplate.queryForObject(sql1, String.class);
                        valueString += "'" + userId + "',";
                    } else {
                        valueString += "'',";
                    }
                } else {
                    valueString += "'" + tempMap.get(titleList.get(k)) + "',";
                }
            }
            valueString += "'" + SysTools.getTime("yyyy-MM-dd HH:mm:ss") + "',";
            valueString += "'" + account.getAccountId() + "',";
            valueString += "'" + account.getOrgId() + "'";
            String insertSql = "insert into hr_user_info(user_id," + fieldString + ",create_time,create_user,org_id) values" + "(" + valueString + ")";
            jdbcTemplate.execute(insertSql);
        }
        return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
    }


}
