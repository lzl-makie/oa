/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: HrPageController.java
 * @Package com.core136.controller.hr
 * @Description: 描述
 * @author: lsq
 * @date: 2019年11月14日 下午1:21:33
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.controller.hr;


import com.core136.bean.account.Account;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.service.account.AccountService;
import com.core136.service.account.UserInfoService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;


/**
 * @author lsq
 */
@Controller
@RequestMapping("/app/core/hr")
public class PageHrController {
    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private UserInfoService userInfoService;

    @Autowired
    public void setUserInfoService(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }

    /**
     * @return ModelAndView
     * @Title: goHrHecruitTaskdetails
     * @Description:  招聘任务详情
     */
    @RequestMapping("/hrrecruittaskdetails")
    @RequiresPermissions("/app/core/hr/hrrecruittaskdetails")
    public ModelAndView goHrHecruitTaskdetails() {
        try {
            return new ModelAndView("app/core/hr/requirements/hrrecruittaskdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goDorecruitplan
     * @Description:  招聘任务
     */
    @RequestMapping("/dorecruitplan")
    @RequiresPermissions("/app/core/hr/dorecruitplan")
    public ModelAndView goDorecruitplan(String view) {
        try {
            if (StringUtils.isBlank(view)) {
                return new ModelAndView("app/core/hr/requirements/droecruitplanmanage");
            } else {
                return new ModelAndView("app/core/hr/requirements/dorecruitplan");
            }
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


    /**
     * @return ModelAndView
     * @Title: goStartkpi
     * @Description:  人员考核
     */
    @RequestMapping("/startkpi")
    @RequiresPermissions("/app/core/hr/startkpi")
    public ModelAndView goStartkpi(String view, String accountId) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/hr/kpi/kpistart");
            } else {
                if (view.equals("result")) {
                    mv = new ModelAndView("app/core/hr/kpi/kpiresult");
                    Account account = accountService.getRedisAUserInfoToAccount();
                    Map<String, String> userInfo = userInfoService.getAccountAndUserInfo(accountId, account.getOrgId());
                    mv.addObject("userInfo", userInfo);
                } else {
                    mv = new ModelAndView("app/core/hr/kpi/kpiforuser");
                    Account account = accountService.getRedisAUserInfoToAccount();
                    Map<String, String> userInfo = userInfoService.getAccountAndUserInfo(accountId, account.getOrgId());
                    mv.addObject("userInfo", userInfo);
                }
            }
            return mv;
        } catch (Exception e) {
            mv = new ModelAndView("titps");
            return mv;
        }
    }

    /**
     * @return ModelAndView
     * @Title: goKpiquery
     * @Description:  考核统计
     */
    @RequestMapping("/kpiquery")
    @RequiresPermissions("/app/core/hr/kpiquery")
    public ModelAndView goKpiquery() {
        ModelAndView mv = null;
        try {
            mv = new ModelAndView("app/core/hr/kpi/kpiquery");
            return mv;
        } catch (Exception e) {
            mv = new ModelAndView("titps");
            return mv;
        }
    }

    /**
     * @return ModelAndView
     * @Title: goKpiplandetails
     * @Description:  考核计划详情
     */
    @RequestMapping("/kpiplandetails")
    @RequiresPermissions("/app/core/hr/kpiplandetails")
    public ModelAndView goKpiplandetails() {
        ModelAndView mv = null;
        try {
            mv = new ModelAndView("app/core/hr/kpi/kpiplandetails");
            return mv;
        } catch (Exception e) {
            mv = new ModelAndView("titps");
            return mv;
        }
    }

    /**
     * @return ModelAndView
     * @Title: goBaseQuery
     * @Description:  基本档案查询
     */
    @RequestMapping("/query")
    @RequiresPermissions("/app/core/hr/query")
    public ModelAndView goBaseQuery() {
        ModelAndView mv = null;
        try {
            mv = new ModelAndView("app/core/hr/baseinfo/querybaseinfo");
            return mv;
        } catch (Exception e) {
            mv = new ModelAndView("titps");
            return mv;
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goAnalysis
     * @Description:  人事数据分析
     */
    @RequestMapping("/analysis")
    @RequiresPermissions("/app/core/hr/analysis")
    public ModelAndView goAnalysis(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/hr/analysis/analysisbaseinfo");
            } else {
                if (view.equals("contract")) {
                    mv = new ModelAndView("app/core/hr/analysis/analysiscontract");
                } else if (view.equals("incentive")) {
                    mv = new ModelAndView("app/core/hr/analysis/analysisincentive");
                } else if (view.equals("licence")) {
                    mv = new ModelAndView("app/core/hr/analysis/analysislicence");
                } else if (view.equals("learn")) {
                    mv = new ModelAndView("app/core/hr/analysis/analysislearn");
                } else if (view.equals("skills")) {
                    mv = new ModelAndView("app/core/hr/analysis/analysisskills");
                } else if (view.equals("transfer")) {
                    mv = new ModelAndView("app/core/hr/analysis/analysistransfer");
                } else if (view.equals("level")) {
                    mv = new ModelAndView("app/core/hr/analysis/analysislevel");
                } else if (view.equals("reinstat")) {
                    mv = new ModelAndView("app/core/hr/analysis/analysisreinstat");
                } else if (view.equals("evaluation")) {
                    mv = new ModelAndView("app/core/hr/analysis/analysisevaluation");
                } else if (view.equals("care")) {
                    mv = new ModelAndView("app/core/hr/analysis/analysiscare");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goEvaluateQuery
     * @Description:  人员评价查询
     */
    @RequestMapping("/evaluatequery")
    @RequiresPermissions("/app/core/hr/evaluatequery")
    public ModelAndView goEvaluateQuery() {
        try {
            return new ModelAndView("app/core/hr/evaluate/evaluatequery");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: evaluate
     * @Description:  人员评价
     */
    @RequestMapping("/evaluate")
    @RequiresPermissions("/app/core/hr/evaluate")
    public ModelAndView goEvaluate() {
        try {
            return new ModelAndView("app/core/hr/evaluate/evaluate");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goWelfare
     * @Description:  人员福利
     */
    @RequestMapping("/welfare")
    @RequiresPermissions("/app/core/hr/welfare")
    public ModelAndView goWelfare(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/hr/salary/welfaremanage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/hr/salary/welfare");
                } else if (view.equals("import")) {
                    mv = new ModelAndView("app/core/hr/salary/welfareimport");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goSalary
     * @Description:  人员薪资管理
     */
    @RequestMapping("/salary")
    @RequiresPermissions("/app/core/hr/salary")
    public ModelAndView goSalary(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/hr/salary/salarymanage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/hr/salary/salary");
                } else if (view.equals("import")) {
                    mv = new ModelAndView("app/core/hr/salary/salaryimport");
                    RetDataBean retDataBean = new RetDataBean();
                    mv.addObject("retDataBean", retDataBean);
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goKpiPlan
     * @Description:  考核计划
     */
    @RequestMapping("/kpi/kpiplan")
    @RequiresPermissions("/app/core/hr/kpi/kpiplan")
    public ModelAndView goKpiPlan(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/hr/kpi/kpiplanmanage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/hr/kpi/kpiplan");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goPlanApproved
     * @Description:  招聘需求审批
     */
    @RequestMapping("/planapproved")
    @RequiresPermissions("/app/core/hr/planapproved")
    public ModelAndView goPlanApproved(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/hr/requirements/planapproved");
            } else {
                if (view.equals("manage")) {
                    mv = new ModelAndView("app/core/hr/requirements/planapprovedmanage");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goKpiItem
     * @Description:  KPI 考核指标
     */
    @RequestMapping("/kpi/kpiitem")
    @RequiresPermissions("/app/core/hr/kpi/kpiitem")
    public ModelAndView goKpiItem(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/hr/kpi/kpiitemmanage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/hr/kpi/kpiitem");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goRecruitplan
     * @Description:  招聘计划管理
     */
    @RequestMapping("/recruitplan")
    @RequiresPermissions("/app/core/hr/recruitplan")
    public ModelAndView goRecruitplan(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/hr/requirements/recruitplanmanage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/hr/requirements/recruitplan");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goRequirements
     * @Description:  招聘需求
     */
    @RequestMapping("/requirements")
    public ModelAndView goRequirements(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/hr/requirements/requirementsmanage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/hr/requirements/requirements");
                } else if (view.equals("queryplan")) {
                    mv = new ModelAndView("app/core/hr/requirements/queryplan");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goTrainApproved
     * @Description:  培训审批
     */
    @RequestMapping("/trainapproved")
    @RequiresPermissions("/app/core/hr/trainapproved")
    public ModelAndView goTrainApproved(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/hr/train/approved");
            } else {
                if (view.equals("manage")) {
                    mv = new ModelAndView("app/core/hr/train/approvedmanage");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goTrain
     * @Description:  培训管理
     */
    @RequestMapping("/train")
    @RequiresPermissions("/app/core/hr/train")
    public ModelAndView goTrain(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/hr/train/manage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/hr/train/input");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goCare
     * @Description:  员工关怀
     */
    @RequestMapping("/care")
    @RequiresPermissions("/app/core/hr/care")
    public ModelAndView goCare(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/hr/care/manage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/hr/care/input");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goTitleEvaluation
     * @Description:  人员职称评定
     */
    @RequestMapping("/titleevaluation")
    @RequiresPermissions("/app/core/hr/titleevaluation")
    public ModelAndView goTitleEvaluation(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/hr/evaluation/manage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/hr/evaluation/input");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


    /**
     * @param view
     * @return ModelAndView
     * @Title: goReinstatement
     * @Description:  复职管理
     */
    @RequestMapping("/reinstatement")
    @RequiresPermissions("/app/core/hr/reinstatement")
    public ModelAndView goReinstatement(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/hr/reinstatement/manage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/hr/reinstatement/input");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goLevel
     * @Description:  人员离职
     */
    @RequestMapping("/leave")
    @RequiresPermissions("/app/core/hr/leave")
    public ModelAndView goLevel(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/hr/leave/manage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/hr/leave/input");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goTranSfer
     * @Description:  人事调动
     */
    @RequestMapping("/transfer")
    @RequiresPermissions("/app/core/hr/transfer")
    public ModelAndView goTranSfer(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/hr/transfer/manage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/hr/transfer/input");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goLaborSkills
     * @Description:  劳动技能管理
     */
    @RequestMapping("/laborskills")
    @RequiresPermissions("/app/core/hr/laborskills")
    public ModelAndView goLaborSkills(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/hr/skills/manage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/hr/skills/input");
                } else if (view.equals("import")) {
                    mv = new ModelAndView("app/core/hr/skills/import");
                    RetDataBean retDataBean = new RetDataBean();
                    mv.addObject("retDataBean", retDataBean);
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goWorkExperience
     * @Description:  工作经历
     */
    @RequestMapping("/workexperience")
    @RequiresPermissions("/app/core/hr/workexperience")
    public ModelAndView goWorkExperience(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/hr/work/manage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/hr/work/input");
                } else if (view.equals("import")) {
                    mv = new ModelAndView("app/core/hr/work/import");
                    RetDataBean retDataBean = new RetDataBean();
                    mv.addObject("retDataBean", retDataBean);
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goLearn
     * @Description:  学习经历
     */
    @RequestMapping("/learn")
    @RequiresPermissions("/app/core/hr/learn")
    public ModelAndView goLearn(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/hr/learn/manage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/hr/learn/input");
                } else if (view.equals("import")) {
                    mv = new ModelAndView("app/core/hr/learn/import");
                    RetDataBean retDataBean = new RetDataBean();
                    mv.addObject("retDataBean", retDataBean);
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goLicense
     * @Description:  证照管理
     */
    @RequestMapping("/licence")
    @RequiresPermissions("/app/core/hr/licence")
    public ModelAndView goLicense(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/hr/licence/manage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/hr/licence/input");
                } else if (view.equals("import")) {
                    mv = new ModelAndView("app/core/hr/licence/import");
                    RetDataBean retDataBean = new RetDataBean();
                    mv.addObject("retDataBean", retDataBean);
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: contract
     * @Description:  HR合同管理
     */
    @RequestMapping("/contract")
    @RequiresPermissions("/app/core/hr/contract")
    public ModelAndView contract(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/hr/contract/manage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/hr/contract/input");
                } else if (view.equals("import")) {
                    mv = new ModelAndView("app/core/hr/contract/import");
                    RetDataBean retDataBean = new RetDataBean();
                    mv.addObject("retDataBean", retDataBean);
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: incentive
     * @Description:  奖惩管理
     */
    @RequestMapping("/incentive")
    @RequiresPermissions("/app/core/hr/incentive")
    public ModelAndView incentive(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/hr/incentive/manage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/hr/incentive/input");
                } else if (view.equals("import")) {
                    mv = new ModelAndView("app/core/hr/incentive/import");
                    RetDataBean retDataBean = new RetDataBean();
                    mv.addObject("retDataBean", retDataBean);
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: setBaseinfo
     * @Description:  人员基本信息
     * @param: view
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/baseinfo")
    @RequiresPermissions("/app/core/hr/baseinfo")
    public ModelAndView setBaseinfo(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/hr/baseinfo/baseinfo");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/hr/baseinfo/baseinfoinput");
                } else if (view.equals("import")) {
                    mv = new ModelAndView("app/core/hr/baseinfo/baseinfoimport");
                    RetDataBean retDataBean = new RetDataBean();
                    mv.addObject("retDataBean", retDataBean);
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: goUserInofDetails
     * @Description:  HR用户信息详情
     * @param: view
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/userinfodetails")
    @RequiresPermissions("/app/core/hr/userinfodetails")
    public ModelAndView goUserInfoDetails(String view) {
        try {
            return new ModelAndView("app/core/hr/baseinfo/userinfodetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goReinstatementDetails
     * @Description:  复职详情
     */
    @RequestMapping("/reinstatementdetails")
    @RequiresPermissions("/app/core/hr/reinstatementdetails")
    public ModelAndView goReinstatementDetails(String view) {
        try {
            return new ModelAndView("app/core/hr/reinstatement/reinstatementdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goHrRecruitPlandetails
     * @Description:  招聘计划详情
     */
    @RequestMapping("/hrrecruitplandetails")
    @RequiresPermissions("/app/core/hr/hrrecruitplandetails")
    public ModelAndView goHrRecruitPlandetails(String view) {
        try {
            return new ModelAndView("app/core/hr/requirements/hrrecruitplandetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goLevelDetails
     * @Description:  人员离职详情
     */
    @RequestMapping("/leveldetails")
    @RequiresPermissions("/app/core/hr/leveldetails")
    public ModelAndView goLevelDetails(String view) {
        try {
            return new ModelAndView("app/core/hr/level/leveldetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goKpiItemDetails
     * @Description:  KPI考核指标详情
     */
    @RequestMapping("/kpi/kpiitemdetails")
    @RequiresPermissions("/app/core/hr/kpi/kpiitemdetails")
    public ModelAndView goKpiItemDetails(String view) {
        try {
            return new ModelAndView("app/core/hr/kpi/kpiitemdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goTrainDetails
     * @Description:  培训详情
     */
    @RequestMapping("/traindetails")
    @RequiresPermissions("/app/core/hr/traindetails")
    public ModelAndView goTrainDetails(String view) {
        try {
            return new ModelAndView("app/core/hr/train/traindetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goTransferDetails
     * @Description:  人事调动详情
     */
    @RequestMapping("/transferdetails")
    @RequiresPermissions("/app/core/hr/transferdetails")
    public ModelAndView goTransferDetails(String view) {
        try {
            return new ModelAndView("app/core/hr/transfer/transferdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goLearnetails
     * @Description:  学习经历详情
     */
    @RequestMapping("/learndetails")
    @RequiresPermissions("/app/core/hr/learndetails")
    public ModelAndView goLearnetails(String view) {
        try {
            return new ModelAndView("app/core/hr/learn/learndetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goLicencedetails
     * @Description:  证照详情
     */
    @RequestMapping("/licencedetails")
    @RequiresPermissions("/app/core/hr/licencedetails")
    public ModelAndView goLicencedetails(String view) {
        try {
            return new ModelAndView("app/core/hr/licence/licencedetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goIncentivedetails
     * @Description:  奖惩详情
     */
    @RequestMapping("/incentivedetails")
    @RequiresPermissions("/app/core/hr/incentivedetails")
    public ModelAndView goIncentivedetails(String view) {
        try {
            return new ModelAndView("app/core/hr/incentive/incentivedetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goContractDetails
     * @Description:  合同详情
     */
    @RequestMapping("/contractdetails")
    @RequiresPermissions("/app/core/hr/contractdetails")
    public ModelAndView goContractDetails() {
        try {
            return new ModelAndView("app/core/hr/contract/contractdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goCareDetails
     * @Description:  员工关怀详情页
     */
    @RequestMapping("/caredetails")
    @RequiresPermissions("/app/core/hr/caredetails")
    public ModelAndView goCareDetails() {
        try {
            return new ModelAndView("app/core/hr/care/caredetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goWorkDetails
     * @Description:  工作经历详情
     */
    @RequestMapping("/workdetails")
    @RequiresPermissions("/app/core/hr/workdetails")
    public ModelAndView goWorkDetails() {
        try {
            return new ModelAndView("app/core/hr/work/workdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goSkillsDetails
     * @Description:  劳动技能详情
     */
    @RequestMapping("/skillsdetails")
    @RequiresPermissions("/app/core/hr/skillsdetails")
    public ModelAndView goSkillsDetails(String view) {
        try {
            return new ModelAndView("app/core/hr/skills/skillsdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goEvaluationDetails
     * @Description:  人员职称评定详情
     */
    @RequestMapping("/evaluationdetails")
    @RequiresPermissions("/app/core/hr/evaluationdetails")
    public ModelAndView goEvaluationDetails(String view) {
        try {
            return new ModelAndView("app/core/hr/evaluation/evaluationdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goRequirementsDetails
     * @Description:  招聘需求详情
     */
    @RequestMapping("/requirementsdetails")
    @RequiresPermissions("/app/core/hr/requirementsdetails")
    public ModelAndView goRequirementsDetails(String view) {
        try {
            return new ModelAndView("app/core/hr/requirements/requirementsdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


    /**
     * @Title: goWagesLevel
     * @Description:  工资级别设置
     * @param: view
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/wageslevel")
    @RequiresPermissions("/app/core/hr/wageslevel")
    public ModelAndView goWagesLevel(String view) {
        try {
            return new ModelAndView("app/core/hr/set/wageslevel");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: addhrdept
     * @Description:  添加HR人员部门
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/hrdept")
    @RequiresPermissions("/app/core/hr/hrdept")
    public ModelAndView addhrdept() {
        try {
            return new ModelAndView("app/core/hr/set/hrdept");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: addhruserinfo
     * @Description:  人员录入
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/hruserinfo")
    @RequiresPermissions("/app/core/hr/hruserinfo")
    public ModelAndView addhruserinfo() {
        try {
            return new ModelAndView("app/core/hr/set/hruserinfo");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goSalarydetails
     * @Description:  人员薪资详情
     */
    @RequestMapping("/salarydetails")
    @RequiresPermissions("/app/core/hr/salarydetails")
    public ModelAndView goSalarydetails() {
        try {
            return new ModelAndView("app/core/hr/salary/salarydetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goWelfaredetails
     * @Description:  人员福利详情
     */
    @RequestMapping("/welfaredetails")
    @RequiresPermissions("/app/core/hr/welfaredetails")
    public ModelAndView goWelfaredetails() {
        try {
            return new ModelAndView("app/core/hr/salary/welfaredetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: addhruserlevel
     * @Description:  行政职务
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/hruserlevel")
    @RequiresPermissions("/app/core/hr/hruserlevel")
    public ModelAndView addhruserlevel() {
        try {
            return new ModelAndView("app/core/hr/set/hruserlevel");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


    /**
     * @return ModelAndView
     * @Title: setClassCode
     * @Description:  设置HR系统编码
     */
    @RequestMapping("/hrclasscode")
    @RequiresPermissions("/app/core/hr/hrclasscode")
    public ModelAndView setClassCode() {
        try {
            return new ModelAndView("app/core/hr/set/classcode");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goMyHrInfo
     * @Description:  员工自助查询
     */
    @RequestMapping("/myhrinfo")
    @RequiresPermissions("/app/core/hr/myhrinfo")
    public ModelAndView goMyHrInfo(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/hr/query/myhrinfo");
            } else {
                if (view.equals("contract")) {
                    mv = new ModelAndView("app/core/hr/query/mycontract");
                } else if (view.equals("incentive")) {
                    mv = new ModelAndView("app/core/hr/query/myincentive");
                } else if (view.equals("licence")) {
                    mv = new ModelAndView("app/core/hr/query/mylicence");
                } else if (view.equals("learn")) {
                    mv = new ModelAndView("app/core/hr/query/mylearn");
                } else if (view.equals("work")) {
                    mv = new ModelAndView("app/core/hr/query/mywork");
                } else if (view.equals("transfer")) {
                    mv = new ModelAndView("app/core/hr/query/mytransfer");
                } else if (view.equals("salary")) {
                    mv = new ModelAndView("app/core/hr/query/mysalary");
                } else if (view.equals("kpi")) {
                    mv = new ModelAndView("app/core/hr/query/mykpi");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }
}
