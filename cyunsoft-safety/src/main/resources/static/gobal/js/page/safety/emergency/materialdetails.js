$(function () {
    $.ajax({
        url: "/ret/safetyget/getSafetyMaterialById",
        type: "post",
        dataType: "json",
        data: {
            materialId: materialId
        },
        success: function (data) {
            if (data.status == "200") {
                let info = data.list;
                for (let id in info) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", info.attach);
                        createAttach("attach", 1);
                    } else if (id == "materialType") {
                        $("#materialType").html(getSafetyCodeClassName(info[id], "materialType"));
                    } else {
                        $("#" + id).html(info[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})
