$(function () {
    $('#remark').summernote({height: 200});
    $(".js-add-save").unbind("click").click(function () {
        createPlan();
    })
    jeDate("#planYear", {
        format: "YYYY"
    });
    getSafetyCodeClass("planType", "planType");
    $("#entId").select2({
        theme: "bootstrap",
        allowClear: true,
        placeholder: "请输入企业名称或法人姓名",
        query: function (query) {
            var url = "/ret/safetyget/selectEntInfoByName";
            var param = {search: query.term}; // 查询参数，query.term为用户在select2中的输入内容.
            var type = "json";
            var data = {results: []};
            $.post(
                url,
                param,
                function (datas) {
                    var datalist = datas.list;
                    for (var i = 0, len = datalist.length; i < len; i++) {
                        var land = datalist[i];
                        var option = {
                            "id": land.entId,
                            "text": land.entName
                        };
                        data.results.push(option);
                    }
                    query.callback(data);
                }, type);

        },
        escapeMarkup: function (markup) {
            return markup;
        },
        minimumInputLength: 2,
        formatResult: function (data) {
            return '<div class="select2-user-result">' + data.text + '</div>'
        },
        formatSelection: function (data) {
            return data.text;
        },
        initSelection: function (data, cb) {
            cb(data);
        }
    });
})

function createPlan() {
    if($("#title").val()=="")
    {
        layer.msg("标题不能为空！");
        return;
    }
    $.ajax({
        url: "/set/safetyset/insertSafetyPlan",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            planType: $("#planType").val(),
            title: $("#title").val(),
            planYear:$("#planYear").val(),
            entId:$("#entId").val(),
            version: $("#version").val(),
            remark: $("#remark").code(),
            attach: $("#attach").attr("data_value")
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}
