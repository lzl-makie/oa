package com.core136.controller.safety;

import com.core136.bean.account.Account;
import com.core136.bean.safety.*;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.safety.*;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/set/safetyset")
public class RuoteSetSafetyController {

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private SafetyEntInfoService safetyEntInfoService;

    @Autowired
    public void setSafetyEntInfoService(SafetyEntInfoService safetyEntInfoService) {
        this.safetyEntInfoService = safetyEntInfoService;
    }

    private SafetyTeamService safetyTeamService;

    @Autowired
    public void setSafetyTeamService(SafetyTeamService safetyTeamService) {
        this.safetyTeamService = safetyTeamService;
    }

    private SafetyEventService safetyEventService;

    @Autowired
    public void setSafetyEventService(SafetyEventService safetyEventService) {
        this.safetyEventService = safetyEventService;
    }

    private SafetyLicenceService safetyLicenceService;

    @Autowired
    public void setSafetyLicenceService(SafetyLicenceService safetyLicenceService) {
        this.safetyLicenceService = safetyLicenceService;
    }

    private SafetyCodeClassService safetyCodeClassService;

    @Autowired
    public void setSafetyCodeClassService(SafetyCodeClassService safetyCodeClassService) {
        this.safetyCodeClassService = safetyCodeClassService;
    }

    private SafetyEntMemberService safetyEntMemberService;

    @Autowired
    public void setSafetyEntMemberService(SafetyEntMemberService safetyEntMemberService) {
        this.safetyEntMemberService = safetyEntMemberService;
    }

    private SafetyConfigService safetyConfigService;

    @Autowired
    public void setSafetyConfigService(SafetyConfigService safetyConfigService) {
        this.safetyConfigService = safetyConfigService;
    }

    private SafetyTrainService safetyTrainService;

    @Autowired
    private void setSafetyTrainService(SafetyTrainService safetyTrainService) {
        this.safetyTrainService = safetyTrainService;
    }

    private SafetyDiaryService safetyDiaryService;

    @Autowired
    public void setSafetyDiaryService(SafetyDiaryService safetyDiaryService) {
        this.safetyDiaryService = safetyDiaryService;
    }

    private SafetyTeamMemberService safetyTeamMemberService;

    @Autowired
    public void setSafetyTeamMemberService(SafetyTeamMemberService safetyTeamMemberService) {
        this.safetyTeamMemberService = safetyTeamMemberService;
    }

    public SafetyPlanService safetyPlanService;

    @Autowired
    public void setSafetyPlanService(SafetyPlanService safetyPlanService) {
        this.safetyPlanService = safetyPlanService;
    }

    private SafetyDrillService safetyDrillService;

    @Autowired
    public void setSafetyDrillService(SafetyDrillService safetyDrillService) {
        this.safetyDrillService = safetyDrillService;
    }

    private SafetyMaterialService safetyMaterialService;

    @Autowired
    public void setSafetyMaterialService(SafetyMaterialService safetyMaterialService) {
        this.safetyMaterialService = safetyMaterialService;
    }

    private SafetyTargetService safetyTargetService;

    @Autowired
    public void setSafetyTargetService(SafetyTargetService safetyTargetService) {
        this.safetyTargetService = safetyTargetService;
    }

    private SafetyTargetSortService safetyTargetSortService;

    @Autowired
    public void setSafetyTargetSortService(SafetyTargetSortService safetyTargetSortService) {
        this.safetyTargetSortService = safetyTargetSortService;
    }

    private SafetyRegulatorService safetyRegulatorService;

    @Autowired
    public void setSafetyRegulatorService(SafetyRegulatorService safetyRegulatorService) {
        this.safetyRegulatorService = safetyRegulatorService;
    }

    /**
     * 创建监管机构
     * @param safetyRegulator
     * @return
     */
    @RequestMapping(value = "/insertSafetyRegulator", method = RequestMethod.POST)
    public RetDataBean insertSafetyRegulator(SafetyRegulator safetyRegulator) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyRegulator.setRecordId(SysTools.getGUID());
            safetyRegulator.setCreateUser(account.getAccountId());
            safetyRegulator.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            safetyRegulator.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, safetyRegulatorService.insertSafetyRegulator(safetyRegulator));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除监管机构
     *
     * @param safetyRegulator
     * @return
     */
    @RequestMapping(value = "/deleteSafetyRegulator", method = RequestMethod.POST)
    public RetDataBean deleteSafetyRegulator(SafetyRegulator safetyRegulator) {
        try {
            if (StringUtils.isBlank(safetyRegulator.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyRegulator.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, safetyRegulatorService.deleteSafetyRegulator(safetyRegulator));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新监管机构名称
     * @param safetyRegulator
     * @return
     */
    @RequestMapping(value = "/updateSafetyRegulator", method = RequestMethod.POST)
    public RetDataBean updateSafetyRegulator(SafetyRegulator safetyRegulator) {
        try {
            if (StringUtils.isBlank(safetyRegulator.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(SafetyRegulator.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", safetyRegulator.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, safetyRegulatorService.updateSafetyRegulator(example, safetyRegulator));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 创建管理目标分类
     *
     * @param safetyTargetSort
     * @return
     */
    @RequestMapping(value = "/insertSafetyTargetSort", method = RequestMethod.POST)
    public RetDataBean insertSafetyTargetSort(SafetyTargetSort safetyTargetSort) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyTargetSort.setSortId(SysTools.getGUID());
            if (StringUtils.isBlank(safetyTargetSort.getSortLevel())) {
                safetyTargetSort.setSortLevel("0");
            }
            safetyTargetSort.setCreateUser(account.getAccountId());
            safetyTargetSort.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            safetyTargetSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, safetyTargetSortService.insertSafetyTargetSort(safetyTargetSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除管理目标分类
     *
     * @param safetyTargetSort
     * @return
     */
    @RequestMapping(value = "/deleteSafetyTargetSort", method = RequestMethod.POST)
    public RetDataBean deleteSafetyTargetSort(SafetyTargetSort safetyTargetSort) {
        try {
            if (StringUtils.isBlank(safetyTargetSort.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyTargetSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, safetyTargetSortService.deleteSafetyTargetSort(safetyTargetSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新管理目标分类
     *
     * @param safetyTargetSort
     * @return
     */
    @RequestMapping(value = "/updateSafetyTargetSort", method = RequestMethod.POST)
    public RetDataBean updateSafetyTargetSort(SafetyTargetSort safetyTargetSort) {
        try {
            if (StringUtils.isBlank(safetyTargetSort.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            if (StringUtils.isBlank(safetyTargetSort.getSortLevel())) {
                safetyTargetSort.setSortLevel("0");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(SafetyTargetSort.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("sortId", safetyTargetSort.getSortId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, safetyTargetSortService.updateSafetyTargetSort(example, safetyTargetSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 创建管理目标
     *
     * @param safetyTarget
     * @return
     */
    @RequestMapping(value = "/insertSafetyTarget", method = RequestMethod.POST)
    public RetDataBean insertSafetyTarget(SafetyTarget safetyTarget) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyTarget.setTargetId(SysTools.getGUID());
            safetyTarget.setCreateUser(account.getAccountId());
            safetyTarget.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            safetyTarget.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, safetyTargetService.insertSafetyTarget(safetyTarget));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除管理目标
     *
     * @param safetyTarget
     * @return
     */
    @RequestMapping(value = "/deleteSafetyTarget", method = RequestMethod.POST)
    public RetDataBean deleteSafetyTarget(SafetyTarget safetyTarget) {
        try {
            if (StringUtils.isBlank(safetyTarget.getTargetId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyTarget.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, safetyTargetService.deleteSafetyTarget(safetyTarget));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新管理目标
     *
     * @param safetyTarget
     * @return
     */
    @RequestMapping(value = "/updateSafetyTarget", method = RequestMethod.POST)
    public RetDataBean updateSafetyTarget(SafetyTarget safetyTarget) {
        try {
            if (StringUtils.isBlank(safetyTarget.getTargetId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(SafetyTarget.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("targetId", safetyTarget.getTargetId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, safetyTargetService.updateSafetyTarget(example, safetyTarget));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 创建应急物资
     *
     * @param safetyMaterial
     * @return
     */
    @RequestMapping(value = "/insertSafetyMaterial", method = RequestMethod.POST)
    public RetDataBean insertSafetyMaterial(SafetyMaterial safetyMaterial) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyMaterial.setMaterialId(SysTools.getGUID());
            safetyMaterial.setCreateUser(account.getAccountId());
            safetyMaterial.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            safetyMaterial.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, safetyMaterialService.insertSafetyMaterial(safetyMaterial));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除应急物资
     *
     * @param safetyMaterial
     * @return
     */
    @RequestMapping(value = "/deleteSafetyMaterial", method = RequestMethod.POST)
    public RetDataBean deleteSafetyMaterial(SafetyMaterial safetyMaterial) {
        try {
            if (StringUtils.isBlank(safetyMaterial.getMaterialId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyMaterial.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, safetyMaterialService.deleteSafetyMaterial(safetyMaterial));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新应急物资信息
     *
     * @param safetyMaterial
     * @return
     */
    @RequestMapping(value = "/updateSafetyMaterial", method = RequestMethod.POST)
    public RetDataBean updateSafetyMaterial(SafetyMaterial safetyMaterial) {
        try {
            if (StringUtils.isBlank(safetyMaterial.getMaterialId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(SafetyMaterial.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("materialId", safetyMaterial.getMaterialId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, safetyMaterialService.updateSafetyMaterial(example, safetyMaterial));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 日常安全培训
     *
     * @param safetyTrain
     * @return
     */
    @RequestMapping(value = "/insertSafetyTrain", method = RequestMethod.POST)
    public RetDataBean insertSafetyTrain(SafetyTrain safetyTrain) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyTrain.setTrainId(SysTools.getGUID());
            Document htmlDoc = Jsoup.parse(safetyTrain.getRemark());
            String subheading = htmlDoc.text();
            if (subheading.length() > 50) {
                subheading = subheading.substring(0, 50) + "...";
            }
            safetyTrain.setSubheading(subheading);
            safetyTrain.setCreateUser(account.getAccountId());
            safetyTrain.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            safetyTrain.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, safetyTrainService.insertSafetyTrain(safetyTrain));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除日常安全培训记录
     *
     * @param safetyTrain
     * @return
     */
    @RequestMapping(value = "/deleteSafetyTrain", method = RequestMethod.POST)
    public RetDataBean deleteSafetyTrain(SafetyTrain safetyTrain) {
        try {
            if (StringUtils.isBlank(safetyTrain.getTrainId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyTrain.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, safetyTrainService.deleteSafetyTrain(safetyTrain));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新日常安全培训记录
     *
     * @param safetyTrain
     * @return
     */
    @RequestMapping(value = "/updateSafetyTrain", method = RequestMethod.POST)
    public RetDataBean updateSafetyTrain(SafetyTrain safetyTrain) {
        try {
            if (StringUtils.isBlank(safetyTrain.getTrainId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Document htmlDoc = Jsoup.parse(safetyTrain.getRemark());
            String subheading = htmlDoc.text();
            if (subheading.length() > 50) {
                subheading = subheading.substring(0, 50) + "...";
            }
            safetyTrain.setSubheading(subheading);
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(SafetyTrain.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("trainId", safetyTrain.getTrainId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, safetyTrainService.updateSafetyTrain(example, safetyTrain));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 添加应急演练
     *
     * @param safetyDrill
     * @return
     */
    @RequestMapping(value = "/insertSafetyDrill", method = RequestMethod.POST)
    public RetDataBean insertSafetyDrill(SafetyDrill safetyDrill) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyDrill.setDrillId(SysTools.getGUID());
            safetyDrill.setCreateUser(account.getAccountId());
            safetyDrill.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            safetyDrill.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, safetyDrillService.insertSafetyDrill(safetyDrill));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除应急演练
     *
     * @param safetyDrill
     * @return
     */
    @RequestMapping(value = "/deleteSafetyDrill", method = RequestMethod.POST)
    public RetDataBean deleteSafetyDrill(SafetyDrill safetyDrill) {
        try {
            if (StringUtils.isBlank(safetyDrill.getDrillId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyDrill.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, safetyDrillService.deleteSafetyDrill(safetyDrill));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新应急演练
     *
     * @param safetyDrill
     * @return
     */
    @RequestMapping(value = "/updateSafetyDrill", method = RequestMethod.POST)
    public RetDataBean updateSafetyDrill(SafetyDrill safetyDrill) {
        try {
            if (StringUtils.isBlank(safetyDrill.getDrillId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(SafetyDrill.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("drillId", safetyDrill.getDrillId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, safetyDrillService.updateSafetyDrill(example, safetyDrill));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 添加应急方案
     *
     * @param safetyPlan
     * @return
     */
    @RequestMapping(value = "/insertSafetyPlan", method = RequestMethod.POST)
    public RetDataBean insertSafetyPlan(SafetyPlan safetyPlan) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyPlan.setPlanId(SysTools.getGUID());
            safetyPlan.setCreateUser(account.getAccountId());
            safetyPlan.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            safetyPlan.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, safetyPlanService.insertSafetyPlan(safetyPlan));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除应急方案
     *
     * @param safetyPlan
     * @return
     */
    @RequestMapping(value = "/deleteSafetyPlan", method = RequestMethod.POST)
    public RetDataBean deleteSafetyPlan(SafetyPlan safetyPlan) {
        try {
            if (StringUtils.isBlank(safetyPlan.getPlanId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyPlan.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, safetyPlanService.deleteSafetyPlan(safetyPlan));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新应急方案
     *
     * @param safetyPlan
     * @return
     */
    @RequestMapping(value = "/updateSafetyPlan", method = RequestMethod.POST)
    public RetDataBean updateSafetyPlan(SafetyPlan safetyPlan) {
        try {
            if (StringUtils.isBlank(safetyPlan.getPlanId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(SafetyPlan.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("planId", safetyPlan.getPlanId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, safetyPlanService.updateSafetyPlan(example, safetyPlan));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 设置监管参数
     *
     * @param safetyConfig
     * @return
     */
    @RequestMapping(value = "/setSafetyConfig", method = RequestMethod.POST)
    public RetDataBean setSafetyConfig(SafetyConfig safetyConfig) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, safetyConfigService.setSafetyConfig(account, safetyConfig));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 添加企业人员
     *
     * @param safetyEntMember
     * @return
     */
    @RequestMapping(value = "/insertSafetyEntMember", method = RequestMethod.POST)
    public RetDataBean insertSafetyEntMember(SafetyEntMember safetyEntMember) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyEntMember.setMemberId(SysTools.getGUID());
            safetyEntMember.setCreateUser(account.getAccountId());
            safetyEntMember.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            safetyEntMember.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, safetyEntMemberService.insertSafetyEntMember(safetyEntMember));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除企业人员
     *
     * @param safetyEntMember
     * @return
     */
    @RequestMapping(value = "/deleteSafetyEntMember", method = RequestMethod.POST)
    public RetDataBean deleteSafetyEntMember(SafetyEntMember safetyEntMember) {
        try {
            if (StringUtils.isBlank(safetyEntMember.getMemberId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyEntMember.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, safetyEntMemberService.deleteSafetyEntMember(safetyEntMember));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新企业人员
     *
     * @param safetyEntMember
     * @return
     */
    @RequestMapping(value = "/updateSafetyEntMember", method = RequestMethod.POST)
    public RetDataBean updateSafetyEntMember(SafetyEntMember safetyEntMember) {
        try {
            if (StringUtils.isBlank(safetyEntMember.getMemberId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(SafetyEntMember.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("memberId", safetyEntMember.getMemberId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, safetyEntMemberService.updateSafetyEntMember(example, safetyEntMember));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 创建分类码
     *
     * @param safetyCodeClass
     * @return
     */
    @RequestMapping(value = "/insertSafetyCodeClass", method = RequestMethod.POST)
    public RetDataBean insertSafetyCodeClass(SafetyCodeClass safetyCodeClass) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyCodeClass.setCodeClassId(SysTools.getGUID());
            safetyCodeClass.setCreateUser(account.getAccountId());
            safetyCodeClass.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            safetyCodeClass.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, safetyCodeClassService.insertSafetyCodeClass(safetyCodeClass));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除分类码
     *
     * @param safetyCodeClass
     * @return
     */
    @RequestMapping(value = "/deleteSafetyCodeClass", method = RequestMethod.POST)
    public RetDataBean deleteSafetyCodeClass(SafetyCodeClass safetyCodeClass) {
        try {
            if (StringUtils.isBlank(safetyCodeClass.getCodeClassId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyCodeClass.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, safetyCodeClassService.deleteSafetyCodeClass(safetyCodeClass));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 批量删除系统编码
     *
     * @param classCodeArr
     * @return
     */
    @RequestMapping(value = "/deleteSafetyCodeClassBatch", method = RequestMethod.POST)
    public RetDataBean deleteSafetyCodeClassBatch(@RequestParam(value = "classCodeArr[]") String[] classCodeArr) {
        try {
            if (classCodeArr.length == 0) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            }
            List<String> list = new ArrayList<String>(Arrays.asList(classCodeArr));
            Example example = new Example(SafetyCodeClass.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andIn("classCodeId", list);
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, safetyCodeClassService.deleteCodeClass(example));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新分类码
     *
     * @param safetyCodeClass
     * @return
     */
    @RequestMapping(value = "/updateSafetyCodeClass", method = RequestMethod.POST)
    public RetDataBean updateSafetyCodeClass(SafetyCodeClass safetyCodeClass) {
        try {
            if (StringUtils.isBlank(safetyCodeClass.getCodeClassId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(SafetyCodeClass.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("codeClassId", safetyCodeClass.getCodeClassId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, safetyCodeClassService.updateSafetyCodeClass(example, safetyCodeClass));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 添加相关证照
     *
     * @param safetyLicence
     * @return
     */
    @RequestMapping(value = "/insertSafetyLicence", method = RequestMethod.POST)
    public RetDataBean insertSafetyLicence(SafetyLicence safetyLicence) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyLicence.setLicenceId(SysTools.getGUID());
            safetyLicence.setCreateUser(account.getAccountId());
            safetyLicence.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            safetyLicence.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, safetyLicenceService.insertSafetyLicence(safetyLicence));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除相关证照
     *
     * @param safetyLicence
     * @return
     */
    @RequestMapping(value = "/deleteSafetyLicence", method = RequestMethod.POST)
    public RetDataBean deleteSafetyLicence(SafetyLicence safetyLicence) {
        try {
            if (StringUtils.isBlank(safetyLicence.getLicenceId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyLicence.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, safetyLicenceService.deleteSafetyLicence(safetyLicence));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新证照信息
     *
     * @param safetyLicence
     * @return
     */
    @RequestMapping(value = "/updateSafetyLicence", method = RequestMethod.POST)
    public RetDataBean updateSafetyLicence(SafetyLicence safetyLicence) {
        try {
            if (StringUtils.isBlank(safetyLicence.getLicenceId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(SafetyLicence.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("licenceId", safetyLicence.getLicenceId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, safetyLicenceService.updateSafetyLicence(example, safetyLicence));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 添加应急小组
     *
     * @param safetyTeam
     * @return
     */
    @RequestMapping(value = "/insertSafetyTeam", method = RequestMethod.POST)
    public RetDataBean insertSafetyTeam(SafetyTeam safetyTeam) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyTeam.setTeamId(SysTools.getGUID());
            safetyTeam.setCreateUser(account.getAccountId());
            safetyTeam.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            safetyTeam.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, safetyTeamService.insertSafetyTeam(safetyTeam));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除应急小组
     *
     * @param safetyTeam
     * @return
     */
    @RequestMapping(value = "/deleteSafetyTeam", method = RequestMethod.POST)
    public RetDataBean deleteSafetyTeam(SafetyTeam safetyTeam) {
        try {
            if (StringUtils.isBlank(safetyTeam.getTeamId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyTeam.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, safetyTeamService.deleteSafetyTeam(safetyTeam));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 应急小组更新
     *
     * @param safetyTeam
     * @return
     */
    @RequestMapping(value = "/updateSafetyTeam", method = RequestMethod.POST)
    public RetDataBean updateSafetyTeam(SafetyTeam safetyTeam) {
        try {
            if (StringUtils.isBlank(safetyTeam.getTeamId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(SafetyTeam.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("teamId", safetyTeam.getTeamId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, safetyTeamService.updateSafetyTeam(example, safetyTeam));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 添加应急小组成员
     *
     * @param safetyTeamMember
     * @return
     */
    @RequestMapping(value = "/insertSafetyTeamMember", method = RequestMethod.POST)
    public RetDataBean insertSafetyTeamMember(SafetyTeamMember safetyTeamMember) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyTeamMember.setRecordId(SysTools.getGUID());
            safetyTeamMember.setCreateUser(account.getAccountId());
            safetyTeamMember.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            safetyTeamMember.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, safetyTeamMemberService.insertSafetyTeamMember(safetyTeamMember));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除应急小组成员
     *
     * @param safetyTeamMember
     * @return
     */
    @RequestMapping(value = "/deleteSafetyTeamMember", method = RequestMethod.POST)
    public RetDataBean deleteSafetyTeamMember(SafetyTeamMember safetyTeamMember) {
        try {
            if (StringUtils.isBlank(safetyTeamMember.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyTeamMember.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, safetyTeamMemberService.deleteSafetyTeamMember(safetyTeamMember));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 应急小组成员更新
     *
     * @param safetyTeamMember
     * @return
     */
    @RequestMapping(value = "/updateSafetyTeamMember", method = RequestMethod.POST)
    public RetDataBean updateSafetyTeamMember(SafetyTeamMember safetyTeamMember) {
        try {
            if (StringUtils.isBlank(safetyTeamMember.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(SafetyTeamMember.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", safetyTeamMember.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, safetyTeamMemberService.updateSafetyTeamMember(example, safetyTeamMember));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 添加监管事件
     *
     * @param safetyEvent
     * @return
     */
    @RequestMapping(value = "/insertSafetyEvent", method = RequestMethod.POST)
    public RetDataBean insertSafetyEvent(SafetyEvent safetyEvent) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyEvent.setEventId(SysTools.getGUID());
            safetyEvent.setCreateUser(account.getAccountId());
            safetyEvent.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            safetyEvent.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, safetyEventService.insertSafetyEvent(safetyEvent));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除监管事件
     *
     * @param safetyEvent
     * @return
     */
    @RequestMapping(value = "/deleteSafetyEvent", method = RequestMethod.POST)
    public RetDataBean deleteSafetyEvent(SafetyEvent safetyEvent) {
        try {
            if (StringUtils.isBlank(safetyEvent.getEventId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyEvent.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, safetyEventService.deleteSafetyEvent(safetyEvent));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 监管事件更新
     *
     * @param safetyEvent
     * @return
     */
    @RequestMapping(value = "/updateSafetyEvent", method = RequestMethod.POST)
    public RetDataBean updateSafetyEvent(SafetyEvent safetyEvent) {
        try {
            if (StringUtils.isBlank(safetyEvent.getEventId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(SafetyEvent.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("eventId", safetyEvent.getEventId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, safetyEventService.updateSafetyEvent(example, safetyEvent));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 创建企来信息信息
     *
     * @param safetyEntInfo
     * @return
     */
    @RequestMapping(value = "/insertSafetyEntInfo", method = RequestMethod.POST)
    public RetDataBean insertSafetyEntInfo(SafetyEntInfo safetyEntInfo) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyEntInfo.setEntId(SysTools.getGUID());
            safetyEntInfo.setCreateUser(account.getAccountId());
            safetyEntInfo.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            safetyEntInfo.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, safetyEntInfoService.insertSafetyEntInfo(safetyEntInfo));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除企业信息
     *
     * @param safetyEntInfo
     * @return
     */
    @RequestMapping(value = "/deleteSafetyEntInfo", method = RequestMethod.POST)
    public RetDataBean deleteSafetyEntInfo(SafetyEntInfo safetyEntInfo) {
        try {
            if (StringUtils.isBlank(safetyEntInfo.getEntId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            safetyEntInfo.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, safetyEntInfoService.deleteSafetyEntInfo(safetyEntInfo));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新企业信息
     *
     * @param safetyEntInfo
     * @return
     */
    @RequestMapping(value = "/updateSafetyEntInfo", method = RequestMethod.POST)
    public RetDataBean updateSafetyEntInfo(SafetyEntInfo safetyEntInfo) {
        try {
            if (StringUtils.isBlank(safetyEntInfo.getEntId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(SafetyEntInfo.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("entId", safetyEntInfo.getEntId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, safetyEntInfoService.updateSafetyEntInfo(example, safetyEntInfo));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

}
