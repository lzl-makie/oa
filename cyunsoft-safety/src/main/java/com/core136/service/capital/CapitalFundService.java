package com.core136.service.capital;

import com.core136.bean.capital.CapitalFund;
import com.core136.mapper.capital.CapitalFundMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

@Service
public class CapitalFundService {
    private CapitalFundMapper capitalFundMapper;
    @Autowired
    public void setCapitalFundMapper(CapitalFundMapper capitalFundMapper)
    {
        this.capitalFundMapper = capitalFundMapper;
    }

    public int insertCapitalFund(CapitalFund capitalFund)
    {
        return capitalFundMapper.insert(capitalFund);
    }
    public int deleteCapitalFund(CapitalFund capitalFund)
    {
        return  capitalFundMapper.delete(capitalFund);
    }
    public CapitalFund selectOneCapitalFund(CapitalFund capitalFund)
    {
        return capitalFundMapper.selectOne(capitalFund);
    }
    public int updateCapitalFund(Example example,CapitalFund capitalFund)
    {
        return capitalFundMapper.updateByExampleSelective(capitalFund,example);
    }
}
