package com.core136.service.safety;

import com.core136.bean.safety.SafetyTrain;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.safety.SafetyTrainMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class SafetyTrainService {
    private SafetyTrainMapper safetyTrainMapper;

    @Autowired
    public void setSafetyTrainMapper(SafetyTrainMapper safetyTrainMapper) {
        this.safetyTrainMapper = safetyTrainMapper;
    }

    public int insertSafetyTrain(SafetyTrain safetyTrain) {
        return safetyTrainMapper.insert(safetyTrain);
    }

    public int deleteSafetyTrain(SafetyTrain safetyTrain) {
        return safetyTrainMapper.delete(safetyTrain);
    }

    public SafetyTrain selectOneSafetyTrain(SafetyTrain safetyTrain) {
        return safetyTrainMapper.selectOne(safetyTrain);
    }

    public int updateSafetyTrain(Example example, SafetyTrain safetyTrain) {
        return safetyTrainMapper.updateByExampleSelective(safetyTrain, example);
    }

    /**
     * 获取培训列表
     * @param orgId
     * @param trainType
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String,String>> getSafetyTrainList(String orgId, String trainType, String beginTime, String endTime, String search)
    {
        return safetyTrainMapper.getSafetyTrainList(orgId,trainType,beginTime,endTime,"%"+search+"%");
    }

    /**
     * 获取培训列表
     * @param pageParam
     * @param trainType
     * @param beginTime
     * @param endTime
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getSafetyTrainList(PageParam pageParam, String trainType, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getSafetyTrainList(pageParam.getOrgId(), trainType, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
