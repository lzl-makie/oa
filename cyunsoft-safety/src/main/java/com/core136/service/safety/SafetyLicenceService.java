package com.core136.service.safety;

import com.core136.bean.safety.SafetyLicence;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.safety.SafetyLicenceMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class SafetyLicenceService {
    private SafetyLicenceMapper safetyLicenceMapper;

    @Autowired
    public void setSafetyLicenceMapper(SafetyLicenceMapper safetyLicenceMapper) {
        this.safetyLicenceMapper = safetyLicenceMapper;
    }

    public int insertSafetyLicence(SafetyLicence safetyLicence) {
        return safetyLicenceMapper.insert(safetyLicence);
    }

    public int deleteSafetyLicence(SafetyLicence safetyLicence) {
        return safetyLicenceMapper.delete(safetyLicence);
    }

    public SafetyLicence selectOneSafetyLicence(SafetyLicence safetyLicence) {
        return safetyLicenceMapper.selectOne(safetyLicence);
    }

    public int updateSafetyLicence(Example example, SafetyLicence safetyLicence) {
        return safetyLicenceMapper.updateByExampleSelective(safetyLicence, example);
    }

    /**
     * 获取证照管理列表
     *
     * @param orgId
     * @param licenceType
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getSafetyLicenceList(String orgId, String licenceType, String beginTime, String endTime, String search) {
        return safetyLicenceMapper.getSafetyLicenceList(orgId, licenceType, beginTime, endTime, "%" + search + "%");
    }

    /**
     * 获取证照管理列表
     *
     * @param pageParam
     * @param licenceType
     * @param beginTime
     * @param endTime
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getSafetyLicenceList(PageParam pageParam, String licenceType, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getSafetyLicenceList(pageParam.getOrgId(), licenceType, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
