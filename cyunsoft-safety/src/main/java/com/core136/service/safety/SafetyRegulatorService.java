package com.core136.service.safety;

import com.core136.bean.safety.SafetyRegulator;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.safety.SafetyRegulatorMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;


@Service
public class SafetyRegulatorService {
    private SafetyRegulatorMapper safetyRegulatorMapper;
    @Autowired
    public void setSafetyRegulatorMapper(SafetyRegulatorMapper safetyRegulatorMapper)
    {
        this.safetyRegulatorMapper = safetyRegulatorMapper;
    }

    public int insertSafetyRegulator(SafetyRegulator safetyRegulator)
    {
        return safetyRegulatorMapper.insert(safetyRegulator);
    }

    public int deleteSafetyRegulator(SafetyRegulator safetyRegulator)
    {
        return safetyRegulatorMapper.delete(safetyRegulator);
    }

    public SafetyRegulator selectOneSafetyRegulator(SafetyRegulator safetyRegulator)
    {
        return safetyRegulatorMapper.selectOne(safetyRegulator);
    }

    public int updateSafetyRegulator(Example example,SafetyRegulator safetyRegulator)
    {
        return safetyRegulatorMapper.updateByExampleSelective(safetyRegulator,example);
    }

    /**
     * 获取监管机构列表
     * @param orgId
     * @param safetyType
     * @param search
     * @return
     */
    public List<Map<String,String>> getSafetyRegulatorList(String orgId, String safetyType,String search)
    {
        return safetyRegulatorMapper.getSafetyRegulatorList(orgId,safetyType,"%"+search+"%");
    }

    /**
     * 获取监管机构列表
     * @param pageParam
     * @param safetyType
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getSafetyRegulatorList(PageParam pageParam, String safetyType) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getSafetyRegulatorList(pageParam.getOrgId(), safetyType, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
