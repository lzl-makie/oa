package com.core136.mapper.capital;

import com.core136.bean.capital.CapitalFund;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CapitalFundMapper extends MyMapper<CapitalFund> {
}
