package com.core136.mapper.safety;

import com.core136.bean.safety.SafetyMaterial;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface SafetyMaterialMapper extends MyMapper<SafetyMaterial> {
    /**
     * 获取急应物资列表
     * @param orgId
     * @param entId
     * @param codeNo
     * @param search
     * @return
     */
    public List<Map<String,String>> getSafetyMaterialList(@Param(value="orgId")String orgId,@Param(value="materialType") String materialType,
                                                          @Param(value="entId")String entId,@Param(value="codeNo")String codeNo,@Param(value="search")String search);
}
