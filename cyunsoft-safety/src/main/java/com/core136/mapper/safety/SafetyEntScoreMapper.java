package com.core136.mapper.safety;

import com.core136.bean.safety.SafetyEntScore;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SafetyEntScoreMapper extends MyMapper<SafetyEntScore> {
}
