package com.core136.mapper.safety;

import com.core136.bean.safety.SafetyTrain;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface SafetyTrainMapper extends MyMapper<SafetyTrain> {
    /**
     * 获取培训列表
     * @param orgId
     * @param trainType
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String,String>> getSafetyTrainList(@Param(value="orgId")String orgId,@Param(value="trainType")String trainType,
                                                       @Param(value="beginTime")String beginTime,@Param(value="endTime")String endTime,@Param(value="search")String search);
}
