package com.core136.mapper.safety;

import com.core136.bean.safety.SafetyTargetSort;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface SafetyTargetSortMapper extends MyMapper<SafetyTargetSort> {
    /**
     *
     * @Title: getTargetSortTree
     * @Description:  获取目标分类树结构
     * @param @param orgId
     * @param @param sortLevel
     * @param @return 设定文件
     * @return List<Map < String, String>> 返回类型

     */
    public List<Map<String, String>> getTargetSortTree(@Param(value = "orgId") String orgId, @Param(value = "sortLevel") String sortLevel);

    /**
     *
     * @Title: isExistChild
     * @Description:  判断是否还有子集
     * @param @param orgId
     * @param @param sortId
     * @param @return 设定文件
     * @return int 返回类型

     */
    public int isExistChild(@Param(value = "orgId") String orgId, @Param(value = "sortId") String sortId);
}
