package com.core136.service.personnel;

import com.core136.bean.personnel.PerLevel;
import com.core136.mapper.personnel.PerLevelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

@Service
public class PerLevelService {
    private PerLevelMapper perLevelMapper;

    @Autowired
    public void setPerLevelMapper(PerLevelMapper perLevelMapper) {
        this.perLevelMapper = perLevelMapper;
    }

    public int insertPerLevel(PerLevel perLevel) {
        return perLevelMapper.insert(perLevel);
    }

    public int deletePerLevel(PerLevel perLevel) {
        return perLevelMapper.delete(perLevel);
    }

    public int updatePerLevel(Example example, PerLevel perLevel) {
        return perLevelMapper.updateByExampleSelective(perLevel, example);
    }

    public PerLevel selectOnePerLevel(PerLevel perLevel) {
        return perLevelMapper.selectOne(perLevel);
    }
}
