package com.core136.service.personnel;

import com.core136.bean.personnel.PerUser;
import com.core136.mapper.personnel.PerUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

@Service
public class PerUserService {
    private PerUserMapper perUserMapper;

    @Autowired
    public void setPerUserMapper(PerUserMapper perUserMapper) {
        this.perUserMapper = perUserMapper;
    }

    public int insertPerUser(PerUser perUser) {
        return perUserMapper.insert(perUser);
    }

    public int deletePerUser(PerUser perUser) {
        return perUserMapper.delete(perUser);
    }

    public int updatePerUser(Example example, PerUser perUser) {
        return perUserMapper.updateByExampleSelective(perUser, example);
    }

    public PerUser selectOnePerUser(PerUser perUser) {
        return perUserMapper.selectOne(perUser);
    }
}
