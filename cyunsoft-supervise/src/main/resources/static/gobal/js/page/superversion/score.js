let integralConfig;
$(function () {
    $.ajax({
        url: "/ret/superversionget/getSuperversionIntegralById",
        type: "post",
        dataType: "json",
        async: false,
        success: function (data) {
            if (data.status == "200") {
                integralConfig = data.list;
                query();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
})


function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/superversionget/getSuperversionScoreList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        sortOrder: "DESC",
        search: true,//启用搜索
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'processId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [
            {
                field: 'num',
                title: '序号',//标题  可不加
                width: '50px',
                formatter: function (value, row, index) {
                    return index + 1;
                }
            },
            {
                field: 'title',
                title: '督查督办事件名称',
                sortable: true,
                width: '150px',
                formatter: function (value, row, index) {
                    return "<a href=\"javascript:void(0);readdetails('" + row.superversionId + "')\">" + value + "</a>";
                }
            },
            {
                field: 'leadUserName',
                width: '50px',
                title: '督查领导'
            },
            {
                field: 'sysScore',
                title: '扣分',
                width: '50px',
                formatter: function (value, row, index) {
                    return value + "分";
                }
            },
            {
                field: 'userScore',
                width: '50px',
                title: '领导评分',
                formatter: function (value, row, index) {
                    let userScore = (integralConfig.userWeight / integralConfig.totalPoints) * value;
                    return value + "*" + (integralConfig.userWeight / integralConfig.totalPoints) + "=" + userScore + "分";
                }
            },
            {
                field: '',
                width: '50px',
                title: '总得分',
                formatter: function (value, row, index) {
                    let userScore = (integralConfig.userWeight / integralConfig.totalPoints) * row.userScore;
                    let sysScore = integralConfig.sysWeight - row.sysScore;
                    return sysScore + userScore + "分";
                }
            }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order
    };
    return temp;
};

function readdetails(superversionId) {
    window.open("/app/core/superversion/superversiondetails?superversionId=" + superversionId);
}
