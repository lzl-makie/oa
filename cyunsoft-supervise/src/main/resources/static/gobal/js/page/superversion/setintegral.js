$(function () {
    getIntegralInfo();
    $(".js-save").unbind("click").click(function () {
        setIntegral();
    })
})

function setIntegral() {
    $.ajax({
        url: "/set/superversionset/setIntegralRule",
        type: "post",
        dataType: "json",
        data: {
            passTime: $("#passTime").val(),
            passTimePoint: $("#passTimePoint").val(),
            delay: $("#delay").val(),
            delayPoint: $("#delayPoint").val(),
            sysWeight: $("#sysWeight").val(),
            userWeight: $("#userWeight").val(),
            totalPoints: $("#totalPoints").val(),
            leadId: $("#leadId").attr("data-value")
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.list);
            }
        }
    })
}

function getIntegralInfo() {
    $.ajax({
        url: "/ret/superversionget/getSuperversionIntegralById",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                for (let id in data.list) {
                    if (id == "leadId") {
                        $("#" + id).attr("data-value", data.list[id]);
                        $("#" + id).val(getUserNameByStr(data.list[id]));
                    } else {
                        $("#" + id).val(data.list[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.list);
            }
        }
    })
}
