let ue=UE.getEditor('content');
$(function () {
    getSmsConfig("msgType", "supperversion");
    jeDate("#beginTime", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTime", {
        format: "YYYY-MM-DD"
    });
    $("#createbut").unbind("click").click(function () {
        addSupperversion();
    });
    getsuperversionLead()
});

function addSupperversion() {
    if ($("#joinUser").attr("data-value") == "@all") {
        layer.msg("督查参与人员不能为全体！");
        return;
    } else {
        $.ajax({
            url: "/set/superversionset/insertSuperversion",
            type: "post",
            dataType: "json",
            data: {
                title: $("#title").val(),
                type: configId,
                leadId: $("#leadId").val(),
                beginTime: $("#beginTime").val(),
                endTime: $("#endTime").val(),
                handedUser: $("#handedUser").attr("data-value"),
                joinUser: $("#joinUser").attr("data-value"),
                content:ue.getContent(),
                attach: $("#superversionattach").attr("data_value"),
                msgType: getCheckBoxValue("msgType")
            },
            success: function (data) {
                if (data.status == "500") {
                    console.log(data.msg);
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    layer.msg(sysmsg[data.msg]);
                    window.location.reload();
                }
            }
        })
    }
}

function getsuperversionLead() {
    $.ajax({
        url: "/ret/superversionget/getSuperversionLeadList",
        type: "post",
        dataType: "json",
        data: {configId: configId},
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
                $("#leadId").html("");
            } else {
                var html = "";
                for (var i = 0; i < data.list.length; i++) {
                    html += "<option value='" + data.list[i].accountId + "'>" + data.list[i].userName + "</option>"
                }
                $("#leadId").html(html);
            }
        }
    })
}
