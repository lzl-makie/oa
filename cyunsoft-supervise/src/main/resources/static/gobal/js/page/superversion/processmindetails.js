$(function () {
    $.ajax({
        url: "/ret/superversionget/getSuperversionProcessById",
        type: "post",
        dataType: "json",
        data: {processId: processId},
        success: function (data) {
            console.log(data);
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                for (var name in data.list) {
                    if (name == "attach") {
                        $("#zsuperversionattach").attr("data_value", data.list.attach);
                        createAttach("zsuperversionattach", "1");
                    } else if (name == "holder" || name == "operator") {
                        $("#z" + name).html(getUserNameByStr(data.list[name]));
                    } else {
                        $("#z" + name).html(data.list[name]);
                    }
                }
            }
        }
    })
});
