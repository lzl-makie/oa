/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: SuperverionConfig.java
 * @Package com.core136.bean.superversion
 * @Description: 描述
 * @author: lsq
 * @date: 2019年11月14日 下午3:09:17
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.bean.superversion;

import javax.persistence.Table;
import java.io.Serializable;


/**
 * @author lsq
 *
 */
@Table(name = "superversion_config")
public class SuperversionConfig implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String configId;
    private Integer sortNo;
    /**
     * 类型名称
     */
    private String typeName;
    private String leadId;
    private String remark;
    private String createUser;
    private String createTime;
    private String orgId;

    public String getConfigId() {
        return configId;
    }

    public void setConfigId(String configId) {
        this.configId = configId;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getLeadId() {
        return leadId;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }


}
