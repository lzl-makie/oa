package com.core136.bean.superversion;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * 积分管理
 */
@Table(name = "superversion_integral")
public class SuperversionIntegral implements Serializable {
    private static final long serialVersionUID = 1L;
    private String configId;
    private Double passTime;
    private Double passTimePoint;
    private Double delay;
    private Double delayPoint;
    private Double sysWeight;
    private Double userWeight;
    private Double totalPoints;
    private String leadId;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getConfigId() {
        return configId;
    }

    public void setConfigId(String configId) {
        this.configId = configId;
    }

    public Double getPassTime() {
        return passTime;
    }

    public void setPassTime(Double passTime) {
        this.passTime = passTime;
    }

    public Double getPassTimePoint() {
        return passTimePoint;
    }

    public void setPassTimePoint(Double passTimePoint) {
        this.passTimePoint = passTimePoint;
    }

    public Double getDelay() {
        return delay;
    }

    public void setDelay(Double delay) {
        this.delay = delay;
    }

    public Double getDelayPoint() {
        return delayPoint;
    }

    public void setDelayPoint(Double delayPoint) {
        this.delayPoint = delayPoint;
    }

    public Double getSysWeight() {
        return sysWeight;
    }

    public void setSysWeight(Double sysWeight) {
        this.sysWeight = sysWeight;
    }

    public Double getUserWeight() {
        return userWeight;
    }

    public void setUserWeight(Double userWeight) {
        this.userWeight = userWeight;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public Double getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(Double totalPoints) {
        this.totalPoints = totalPoints;
    }

    public String getLeadId() {
        return leadId;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }
}
