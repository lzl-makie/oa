package com.core136.service.superversion;

import com.core136.bean.superversion.SuperversionBpmList;
import com.core136.mapper.superversion.SuperversionBpmListMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class SuperversionBpmListService {
    private SuperversionBpmListMapper superversionBpmListMapper;

    @Autowired
    public void setSuperversionBpmListMapper(SuperversionBpmListMapper superversionBpmListMapper) {
        this.superversionBpmListMapper = superversionBpmListMapper;
    }

    public int insertSuperversionBpmList(SuperversionBpmList superversionBpmList) {
        return superversionBpmListMapper.insert(superversionBpmList);
    }

    public int deleteSuperversionBpmList(SuperversionBpmList superversionBpmList) {
        return superversionBpmListMapper.delete(superversionBpmList);
    }

    public int updateSuperversionBpmList(Example example, SuperversionBpmList superversionBpmList) {
        return superversionBpmListMapper.updateByExampleSelective(superversionBpmList, example);
    }

    public SuperversionBpmList selectOneSuperversionBpmList(SuperversionBpmList superversionBpmList) {
        return superversionBpmListMapper.selectOne(superversionBpmList);
    }

    /**
     * 督查督办历史流程
     *
     * @param orgId
     * @param processId
     * @return
     */
    public List<Map<String, String>> getSupversionBpmRecordList(String orgId, String processId) {
        return superversionBpmListMapper.getSupversionBpmRecordList(orgId, processId);
    }

}
