package com.core136.service.superversion;

import com.core136.bean.superversion.SuperversionConfig;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.superversion.SuperversionConfigMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class SuperversionConfigService {
    private SuperversionConfigMapper superversionConfigMapper;

    @Autowired
    public void setSuperversionConfigMapper(SuperversionConfigMapper superversionConfigMapper) {
        this.superversionConfigMapper = superversionConfigMapper;
    }

    public int insertSuperversionConfig(SuperversionConfig superversionConfig) {
        return superversionConfigMapper.insert(superversionConfig);
    }

    public int deleteSuperversionConfig(SuperversionConfig superversionConfig) {
        return superversionConfigMapper.delete(superversionConfig);
    }

    public int updateSuperversionConfig(Example example, SuperversionConfig superversionConfig) {
        return superversionConfigMapper.updateByExampleSelective(superversionConfig, example);
    }

    public SuperversionConfig selectOneSuperversionConfig(SuperversionConfig superversionConfig) {
        return superversionConfigMapper.selectOne(superversionConfig);
    }

    public List<SuperversionConfig> getAllConfig(Example example) {
        return superversionConfigMapper.selectByExample(example);
    }

    public PageInfo<SuperversionConfig> getSuperversionConfigList(PageParam pageParam) throws Exception {
        Example example = new Example(SuperversionConfig.class);
        example.createCriteria().andEqualTo("orgId", pageParam.getOrgId());
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<SuperversionConfig> datalist = getAllConfig(example);
        PageInfo<SuperversionConfig> pageInfo = new PageInfo<SuperversionConfig>(datalist);
        return pageInfo;
    }

    /**
     * @Title: getAllSuperversionConfigList
     * @Description:  获取类型与领导列表
     * @param: orgId
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getAllSuperversionConfigList(String orgId) {
        return superversionConfigMapper.getAllSuperversionConfigList(orgId);
    }

    /**
     * @Title: getMySuperversionConfigList
     * @Description: 与我有关的分类
     * @param: orgId
     * @param: accountId
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getMySuperversionConfigList(String orgId, String accountId) {
        return superversionConfigMapper.getMySuperversionConfigList(orgId, accountId);
    }

    /**
     * @Title: getQuerySuperversionForType
     * @Description:  按类型汇总
     * @param: orgId
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getQuerySuperversionForType(String orgId) {
        return superversionConfigMapper.getQuerySuperversionForType(orgId);
    }

    /**
     * 获取分类树结构
     *
     * @param orgId
     * @return
     */
    public List<Map<String, String>> getSuperverionConfigTree(String orgId) {
        return superversionConfigMapper.getSuperverionConfigTree(orgId);
    }

}
