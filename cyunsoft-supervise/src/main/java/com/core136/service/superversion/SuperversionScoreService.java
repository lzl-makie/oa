/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: SuperversionScoreService.java
 * @Package com.core136.service.superversion
 * @Description: 描述
 * @author: lsq
 * @date: 2019年11月18日 上午9:22:07
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.service.superversion;

import com.core136.bean.account.Account;
import com.core136.bean.superversion.SuperversionIntegral;
import com.core136.bean.superversion.SuperversionScore;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.superversion.SuperversionScoreMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 */
@Service
public class SuperversionScoreService {
    private SuperversionScoreMapper superversionScoreMapper;

    @Autowired
    public void setSuperversionScoreMapper(SuperversionScoreMapper superversionScoreMapper) {
        this.superversionScoreMapper = superversionScoreMapper;
    }

    public int insertSuperversionScore(SuperversionScore superversionScore) {
        return superversionScoreMapper.insert(superversionScore);
    }

    public int deleteSuperversionScore(SuperversionScore superversionScore) {
        return superversionScoreMapper.delete(superversionScore);
    }

    public int updateSuperversionScore(Example example, SuperversionScore superversionScore) {
        return superversionScoreMapper.updateByExampleSelective(superversionScore, example);
    }

    public SuperversionScore selectOneSuperversionScore(SuperversionScore superversionScore) {
        return superversionScoreMapper.selectOne(superversionScore);
    }

    public RetDataBean setSuperversionScore(Account account, SuperversionScore superversionScore) {
        SuperversionScore tmpSuperversionScore = new SuperversionScore();
        tmpSuperversionScore.setOrgId(account.getOrgId());
        tmpSuperversionScore.setSuperversionId(superversionScore.getSuperversionId());
        tmpSuperversionScore = selectOneSuperversionScore(tmpSuperversionScore);
        if (tmpSuperversionScore == null) {
            superversionScore.setScoreId(SysTools.getGUID());
            superversionScore.setCreateUser(account.getAccountId());
            superversionScore.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            insertSuperversionScore(superversionScore);
        } else {
            Example example = new Example(SuperversionScore.class);
            example.createCriteria().andEqualTo("superversionId", superversionScore.getSuperversionId()).andEqualTo("orgId", account.getOrgId());
            updateSuperversionScore(example, superversionScore);
        }
        return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
    }


    /**
     * 设置延期积分
     *
     * @param account
     * @param superversionId
     * @param superversionIntegral
     * @param delay
     * @return
     */
    public RetDataBean setDelayScore(Account account, String superversionId, SuperversionIntegral superversionIntegral, Double delay) {
        SuperversionScore superversionScore = new SuperversionScore();
        superversionScore.setOrgId(account.getOrgId());
        superversionScore.setSuperversionId(superversionId);
        superversionScore = selectOneSuperversionScore(superversionScore);
        SuperversionScore tmpSuperversionScore = new SuperversionScore();
        if (superversionScore == null) {
            tmpSuperversionScore.setScoreId(SysTools.getGUID());
            tmpSuperversionScore.setSuperversionId(superversionId);
            tmpSuperversionScore.setOrgId(account.getOrgId());
            if (superversionIntegral.getDelayPoint() == null) {
                superversionIntegral.setDelay(0.0);
            }
            tmpSuperversionScore.setSysScore(superversionIntegral.getDelayPoint() * delay);
            tmpSuperversionScore.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            tmpSuperversionScore.setCreateUser(account.getAccountId());
            insertSuperversionScore(tmpSuperversionScore);
        } else {
            if (superversionIntegral.getDelayPoint() == null) {
                superversionIntegral.setDelay(0.0);
            }
            tmpSuperversionScore.setSysScore(tmpSuperversionScore.getSysScore() + superversionIntegral.getDelayPoint() * delay);
            tmpSuperversionScore.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            tmpSuperversionScore.setCreateUser(account.getAccountId());
            Example example = new Example(SuperversionScore.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("superversionId", superversionId);
            updateSuperversionScore(example, tmpSuperversionScore);
        }
        return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS);
    }


    /**
     * 设置超时积分
     *
     * @param account
     * @param superversionId
     * @param superversionIntegral
     * @param passTime
     * @return
     */
    public RetDataBean setPassTimeScore(Account account, String superversionId, SuperversionIntegral superversionIntegral, Double passTime) {
        SuperversionScore superversionScore = new SuperversionScore();
        superversionScore.setOrgId(account.getOrgId());
        superversionScore.setSuperversionId(superversionId);
        superversionScore = selectOneSuperversionScore(superversionScore);
        SuperversionScore tmpSuperversionScore = new SuperversionScore();
        if (superversionScore == null) {
            tmpSuperversionScore.setScoreId(SysTools.getGUID());
            tmpSuperversionScore.setSuperversionId(superversionId);
            tmpSuperversionScore.setOrgId(account.getOrgId());
            if (superversionIntegral.getPassTimePoint() == null) {
                superversionIntegral.setPassTimePoint(0.0);
            }
            tmpSuperversionScore.setSysScore(superversionIntegral.getPassTimePoint() * passTime);
            tmpSuperversionScore.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            tmpSuperversionScore.setCreateUser(account.getAccountId());
            insertSuperversionScore(tmpSuperversionScore);
        } else {
            if (superversionIntegral.getPassTimePoint() == null) {
                superversionIntegral.setPassTimePoint(0.0);
            }
            tmpSuperversionScore.setSysScore(tmpSuperversionScore.getSysScore() + superversionIntegral.getPassTimePoint() * passTime);
            tmpSuperversionScore.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            tmpSuperversionScore.setCreateUser(account.getAccountId());
            Example example = new Example(SuperversionScore.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("superversionId", superversionId);
            updateSuperversionScore(example, tmpSuperversionScore);
        }
        return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS);
    }


    public List<Map<String, String>> getSuperversionScoreList(String orgId, String search) {
        return superversionScoreMapper.getSuperversionScoreList(orgId, "%" + search + "%");
    }

    public PageInfo<Map<String, String>> getSuperversionScoreList(PageParam pageParam) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getSuperversionScoreList(pageParam.getOrgId(), pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
