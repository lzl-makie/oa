/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: SuperversionProcessMapper.java
 * @Package com.core136.mapper.superversion
 * @Description: 描述
 * @author: lsq
 * @date: 2019年11月18日 上午9:16:59
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.mapper.superversion;

import com.core136.bean.superversion.SuperversionProcess;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 *
 */
@Mapper
public interface SuperversionProcessMapper extends MyMapper<SuperversionProcess> {


    public List<Map<String, String>> getSuperversionProcessListForSelect(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId);

    /**
     *
     * @Title: getOldProcessList
     * @Description:  获取事件处理过程列表
     * @param: orgId
     * @param: @return
     * @return: List<Map < String, String>>

     */
    public List<Map<String, String>> getOldProcessList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId, @Param(value = "beginTime") String beginTime,
                                                       @Param(value = "endTime") String endTime, @Param(value = "type") String type, @Param(value = "search") String search);

    /**
     *
     * @Title: getControlProcessList
     * @Description:  获取我所管控的任务列表
     * @param: orgId
     * @param: accountId
     * @param: beginTime
     * @param: endTime
     * @param: superversionId
     * @param: search
     * @param: @return
     * @return: List<Map < String, String>>

     */
    public List<Map<String, String>> getControlProcessList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId, @Param(value = "beginTime") String beginTime,
                                                           @Param(value = "endTime") String endTime, @Param(value = "superversionId") String superversionId, @Param(value = "search") String search);


    /**
     *
     * @Title: getSupperversionPorcessList
     * @Description:  获取待处理的督查列表
     * @param: orgId
     * @param: accountId
     * @param: type
     * @param: handedUser
     * @param: beginTime
     * @param: endTime
     * @param: search
     * @param: @return
     * @return: List<Map < String, String>>

     */
    public List<Map<String, String>> getSupperversionPorcessList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,
                                                                 @Param(value = "type") String type, @Param(value = "beginTime") String beginTime,
                                                                 @Param(value = "endTime") String endTime, @Param(value = "search") String search);


    /**
     * 获取转办给我的列表
     * @param orgId
     * @param accountId
     * @param type
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getSupperversionPorcessForChangeList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,
                                                                          @Param(value = "type") String type, @Param(value = "beginTime") String beginTime,
                                                                          @Param(value = "endTime") String endTime, @Param(value = "search") String search);


    /**
     *
     * @Title: getQuerySuperversionForDept
     * @Description:  按部门汇总
     * @param: orgId
     * @param: @return
     * @return: List<Map < String, String>>

     */
    public List<Map<String, String>> getQuerySuperversionForDept(@Param(value = "orgId") String orgId);


    /**
     *
     * @Title: getSupperversionPorcessListForDesk
     * @Description:  获取督查督办待办事件
     * @param orgId
     * @param accountId
     * @return
     * List<Map < String, String>>
     */
    public List<Map<String, String>> getSupperversionPorcessListForDesk(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId);

    /**
     * 获取延时任务列表
     * @param orgId
     * @return
     */
    public List<Map<String, String>> getAllSuperversionProcessForDesk(@Param(value = "orgId") String orgId);

}
