/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: SuperversionPageController.java
 * @Package com.core136.controller.superversion
 * @Description: 描述
 * @author: lsq
 * @date: 2019年11月14日 下午1:17:49
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.controller.superversion;

import com.core136.common.utils.SysTools;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;


/**
 * @author lsq
 */
@Controller
@RequestMapping("/app/core/superversion")
public class PageSuperversionController {
    /**
     * 查询历史督查督办事件
     *
     * @return
     */
    @RequestMapping("/querysuperversion")
    @RequiresPermissions("/app/core/superversion/querysuperversion")
    public ModelAndView goQuerySuperversion() {
        try {
            return new ModelAndView("/app/core/superversion/querysuperversion");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 积分查询
     *
     * @return
     */
    @RequestMapping("/score")
    @RequiresPermissions("/app/core/superversion/score")
    public ModelAndView goScore() {
        try {
            return new ModelAndView("/app/core/superversion/score");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 领导评分
     *
     * @return
     */
    @RequestMapping("/setscore")
    @RequiresPermissions("/app/core/superversion/setscore")
    public ModelAndView goSetScore() {
        try {
            return new ModelAndView("/app/core/superversion/setscore");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 设置积份规则
     *
     * @return
     */
    @RequestMapping("/setintegral")
    @RequiresPermissions("/app/core/superversion/setintegral")
    public ModelAndView goSetIntegral() {
        try {
            return new ModelAndView("/app/core/superversion/setintegral");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 问题回复
     *
     * @return
     */
    @RequestMapping("/answer")
    @RequiresPermissions("/app/core/superversion/answer")
    public ModelAndView goAnswer() {
        try {
            return new ModelAndView("/app/core/superversion/superversionanswer");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 问题反馈
     *
     * @return
     */
    @RequestMapping("/problem")
    @RequiresPermissions("/app/core/superversion/problem")
    public ModelAndView goProblem() {
        try {
            return new ModelAndView("/app/core/superversion/superversionproblem");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 任务处理详情
     *
     * @return
     */
    @RequestMapping("/processresultdetails")
    @RequiresPermissions("/app/core/superversion/processresultdetails")
    public ModelAndView goProcessResultDetails() {
        try {
            return new ModelAndView("/app/core/superversion/processresultdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 督查督办审批详情
     *
     * @return ModelAndView
     * @Title: goApplydetails
     * @Description:
     */
    @RequestMapping("/applydetails")
    @RequiresPermissions("/app/core/superversion/applydetails")
    public ModelAndView goApplydetails() {
        try {
            return new ModelAndView("/app/core/superversion/applydetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: goProcessDetails
     * @Description:  办理步骤详情
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/processdetails")
    @RequiresPermissions("/app/core/superversion/processdetails")
    public ModelAndView goProcessDetails(String superversionId) {
        try {
            if (StringUtils.isBlank(superversionId)) {
                return new ModelAndView("/app/core/superversion/processmindetails");
            } else {
                return new ModelAndView("/app/core/superversion/processdetails");
            }
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: goLeadmanage
     * @Description:  领导管控
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/leadmanage")
    @RequiresPermissions("/app/core/superversion/leadmanage")
    public ModelAndView goLeadmanage(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("/app/core/superversion/leadmanage");
            } else {
                if (view.equals("controlprocess")) {
                    mv = new ModelAndView("/app/core/superversion/controlprocess");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 延期审批
     *
     * @param view
     * @return
     */
    @RequestMapping("/delayapply")
    @RequiresPermissions("/app/core/superversion/delayapply")
    public ModelAndView goDelayapply(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("/app/core/superversion/delayapply");
            } else {
                if (view.equals("manage")) {
                    mv = new ModelAndView("/app/core/superversion/delayapplymanage");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: goQuery
     * @Description:  督查查询
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/query")
    @RequiresPermissions("/app/core/superversion/query")
    public ModelAndView goQuery(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("/app/core/superversion/querydept");
            } else {
                if (view.equals("type")) {
                    mv = new ModelAndView("/app/core/superversion/querytype");
                } else if (view.equals("bi")) {
                    mv = new ModelAndView("/app/core/superversion/querybi");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: setSuperversionConfig
     * @Description:  设置相关的领导
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/setsuperversionconfig")
    @RequiresPermissions("/app/core/superversion/setsuperversionconfig")
    public ModelAndView setSuperversionConfig(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("/app/core/superversion/setleadconfig");
            } else {
                if (view.equals("bpmset")) {
                    mv = new ModelAndView("/app/core/superversion/setbpmconfig");
                }
            }

            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: goCreatesuperverion
     * @Description:  发起督查项目
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/createsuperverion")
    @RequiresPermissions("/app/core/superversion/createsuperverion")
    public ModelAndView goCreatesuperverion(String view, String superversionId) {
        try {
            ModelAndView mv = null;
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("/app/core/superversion/createsuperverion");

            } else if (view.equals("mysuperverionmanage")) {
                mv = new ModelAndView("/app/core/superversion/mysuperverionmanage");
            } else if (view.equals("mysuperverionedit")) {
                mv = new ModelAndView("/app/core/superversion/mysuperverionedit");
                mv.addObject("superversionId", superversionId);
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: goProcess
     * @Description:  督查督办事件处理
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/process")
    @RequiresPermissions("/app/core/superversion/process")
    public ModelAndView goProcess() {
        try {
            return new ModelAndView("/app/core/superversion/process");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 转办
     *
     * @return
     */
    @RequestMapping("/processchange")
    @RequiresPermissions("/app/core/superversion/processchange")
    public ModelAndView goProcessChange() {
        try {
            return new ModelAndView("/app/core/superversion/processchange");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 已办结
     *
     * @return
     */
    @RequestMapping("/queryprocess")
    @RequiresPermissions("/app/core/superversion/queryprocess")
    public ModelAndView goQueryProcess() {
        try {
            return new ModelAndView("/app/core/superversion/queryprocess");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: goSuperversionDetails
     * @Description:  查看详情
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/superversiondetails")
    @RequiresPermissions("/app/core/superversion/superversiondetails")
    public ModelAndView goSuperversionDetails(HttpServletRequest request) {
        try {
            if (SysTools.isMobileDevice(request)) {
                return new ModelAndView("/app/mobile/main/superversion/details");
            } else {
                return new ModelAndView("/app/core/superversion/superversiondetails");
            }

        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

}
