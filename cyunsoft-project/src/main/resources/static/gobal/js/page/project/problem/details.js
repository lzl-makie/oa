$(function () {
    $.ajax({
        url: "/ret/proget/getProProblemById",
        type: "post",
        dataType: "json",
        data: {recordId: recordId},
        success: function (data) {
            if (data.status == "200") {
                for (let id in data.list) {
                    if (id == "problemType") {
                        $("#" + id).html(getCodeClassName(data.list[id], "pro_problem_type"));
                    } else if (id == "attach") {
                        $("#attach").attr("data_value", data.list[id]);
                        createAttach("attach", 1);
                    } else {
                        $("#" + id).html(data.list[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
    getProblemReplyList();
})

function getProblemReplyList() {
    $.ajax({
        url: "/ret/proget/getProblemReplyList",
        type: "post",
        dataType: "json",
        data: {problemId: recordId},
        success: function (data) {
            if (data.status == "200") {
                let info = data.list;
                console.log(info);
                for (let i = 0; i < info.length; i++) {
                    let attachhtml = "";
                    if (info[i].attach != null) {
                        attachhtml += "<div style='text-align: right;'>" + createTableAttach(info[i].attach) + "</div>";
                    }
                    $("#relpylist").append("<tr><td><div style='text-align: right;'><span>回复人:" + info[i].createUserName + "</span><span style='margin-left: 10px;'>回复时间：" + info[i].createTime + "</span></div><div>" + info[i].remark + "</div>" + attachhtml + "</td></tr>")
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}
