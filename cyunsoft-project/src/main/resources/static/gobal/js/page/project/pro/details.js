$(function () {
    $.ajax({
        url: "/ret/proget/getProRecordById",
        type: "post",
        dataType: "json",
        data: {proId: proId},
        success: function (data) {
            if (data.status == "200") {
                let info = data.list;
                for (let id in info) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", info.attach);
                        createAttach("attach", 1);
                    } else if (id == "manager" || id == "approvalUser") {
                        $("#" + id).html(getUserNameByStr(info[id]));
                    } else if (id == "joinDept") {
                        $("#" + id).html(getDeptNameByDeptIds(info[id]));
                    } else if (id == "taskUsers") {
                        $("#" + id).html(getUserNameByStr(info[id]));
                    } else if (id == "isCreateFile") {
                        if (info[id] == "1") {
                            $("#" + id).html("创建项目文档目录");
                        } else {
                            $("#" + id).html("不创建项目文档目录");
                        }
                    } else if (id == "proSort") {
                        $.ajax({
                            url: "/ret/proget/getProSortById",
                            type: "post",
                            dataType: "json",
                            data: {sortId: info[id]},
                            success: function (data) {
                                if (data.status = "200") {
                                    $("#" + id).html(data.list.sortName);
                                } else if (data.status == "100") {
                                    layer.msg(sysmsg[data.msg]);
                                } else {
                                    console.log(data.msg);
                                }
                            }
                        });
                    } else {
                        $("#" + id).html(info[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }

        }
    });
})
