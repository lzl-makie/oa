$(function () {
    $(".js-setcreatepriv").unbind("click").click(function () {
        setcreatepriv();
    })
    $.ajax({
        url: "/ret/proget/getProPrivById",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                for (let id in data.list) {
                    if (id == "createUserPriv") {
                        $("#createUserPriv").attr("data-value", data.list.createUserPriv);
                        $("#createUserPriv").val(getUserNameByStr(data.list.createUserPriv));
                    } else if (id == "createDeptPriv") {
                        $("#createDeptPriv").attr("data-value", data.list.createDeptPriv);
                        $("#createDeptPriv").val(getDeptNameByDeptIds(data.list.createDeptPriv));
                    } else if (id == "createLevelPriv") {
                        $("#createLevelPriv").attr("data-value", data.list.createLevelPriv);
                        $("#createLevelPriv").val(getUserLevelStr(data.list.createLevelPriv));
                    }
                }
            }
        }
    })
})

function setcreatepriv() {
    $.ajax({
        url: "/set/proset/setProPriv",
        type: "post",
        dataType: "json",
        data: {
            createUserPriv: $("#createUserPriv").attr("data-value"),
            createDeptPriv: $("#createDeptPriv").attr("data-value"),
            createLevelPriv: $("#createLevelPriv").attr("data-value")
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                window.location.reload();
                layer.msg(sysmsg[data.msg]);
            }
        }
    })
}
