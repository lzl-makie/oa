package com.core136.mapper.project;

import com.core136.bean.project.ProCost;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ProCostMapper extends MyMapper<ProCost> {
    /**
     * 获取费用类型列表
     *
     * @param orgId
     * @return
     */
    public List<Map<String, String>> getProCostForSelect(@Param(value = "orgId") String orgId);
}
