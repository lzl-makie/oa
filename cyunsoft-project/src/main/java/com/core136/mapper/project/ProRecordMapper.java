package com.core136.mapper.project;

import com.core136.bean.project.ProRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ProRecordMapper extends MyMapper<ProRecord> {
    /**
     * 按状态获取项目列表
     * @param orgId
     * @param proLevel
     * @param beginTime
     * @param endTime
     * @param status
     * @param search
     * @return
     */
    public List<Map<String, String>> getProRecordListByStatus(@Param(value = "orgId") String orgId, @Param(value = "proLevel") String proLevel,
                                                              @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime,
                                                              @Param(value = "status") String status, @Param(value = "search") String search);

    /**
     * 项目记录查询
     * @param orgId
     * @param proSort
     * @param proLevel
     * @param beginTime
     * @param endTime
     * @param status
     * @param search
     * @return
     */
    public List<Map<String, String>> getProRecordListByQuery(@Param(value = "orgId") String orgId, @Param(value = "proSort") String proSort, @Param(value = "proLevel") String proLevel,
                                                             @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime,
                                                             @Param(value = "status") String status, @Param(value = "search") String search);


    public List<Map<String, String>> getProRecordListForApproval(@Param(value = "orgId") String orgId, @Param(value = "proLevel") String proLevel,
                                                                 @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);


    public Map<String, String> getProRecordStatusCountList(@Param(value = "orgId") String orgId);

    public List<Map<String, String>> getProReocrdTree(@Param(value = "orgId") String orgId, @Param(value = "levelId") String levelId);
}
