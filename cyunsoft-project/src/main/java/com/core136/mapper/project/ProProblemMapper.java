package com.core136.mapper.project;

import com.core136.bean.project.ProProblem;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ProProblemMapper extends MyMapper<ProProblem> {
    /**
     * 获取项目任务
     *
     * @param orgId
     * @param proId
     * @param taskId
     * @param search
     * @return
     */
    public List<Map<String, String>> getProProblemListByPro(@Param(value = "orgId") String orgId, @Param(value = "proId") String proId, @Param(value = "taskId") String taskId, @Param(value = "search") String search);
}
