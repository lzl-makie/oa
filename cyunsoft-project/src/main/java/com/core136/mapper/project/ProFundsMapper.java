package com.core136.mapper.project;

import com.core136.bean.project.ProFunds;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ProFundsMapper extends MyMapper<ProFunds> {

    /**
     * 项目预算列表
     *
     * @param orgId
     * @param proId
     * @param search
     * @return
     */
    public List<Map<String, String>> getProFundsListByProId(@Param(value = "orgId") String orgId, @Param(value = "proId") String proId, @Param(value = "search") String search);
}
