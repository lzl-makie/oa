package com.core136.mapper.project;

import com.core136.bean.project.ProPriv;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface ProPrivMapper extends MyMapper<ProPriv> {

    /**
     * 判读人员是否在立项权限内
     *
     * @param orgId
     * @param accountId
     * @param deptId
     * @param levelId
     * @return
     */
    public Integer isInPriv(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId, @Param(value = "deptId") String deptId, @Param(value = "levelId") String levelId);
}
