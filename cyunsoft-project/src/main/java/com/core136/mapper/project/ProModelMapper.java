package com.core136.mapper.project;

import com.core136.bean.project.ProModel;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ProModelMapper extends MyMapper<ProModel> {
}
