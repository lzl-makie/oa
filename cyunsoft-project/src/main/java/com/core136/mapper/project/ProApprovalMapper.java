package com.core136.mapper.project;

import com.core136.bean.project.ProApproval;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ProApprovalMapper extends MyMapper<ProApproval> {

    /**
     * 获取项目历史审批记录
     *
     * @param orgId
     * @param status
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getOldProApprovalList(@Param(value = "orgId") String orgId, @Param(value = "status") String status, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);

}
