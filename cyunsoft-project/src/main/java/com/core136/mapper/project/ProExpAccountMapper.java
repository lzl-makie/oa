package com.core136.mapper.project;

import com.core136.bean.project.ProExpAccount;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ProExpAccountMapper extends MyMapper<ProExpAccount> {


    /**
     * 获取项目费用科目树结构
     *
     * @param orgId
     * @param levelId
     * @return
     */
    public List<Map<String, String>> getProExpAccountTree(@Param(value = "orgId") String orgId, @Param(value = "levelId") String levelId);

    /**
     * @param @param  orgId
     * @param @param  sortId
     * @param @return 设定文件
     * @return int 返回类型
     * @Title: isExistChild
     * @Description:  判断是否还有子集
     */
    public int isExistChild(@Param(value = "orgId") String orgId, @Param(value = "sortId") String sortId);
}
