package com.core136.mapper.project;

import com.core136.bean.project.ProFile;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ProFileMapper extends MyMapper<ProFile> {
    /**
     * 获取项目文档
     *
     * @param orgId
     * @param proId
     * @param search
     * @return
     */
    public List<Map<String, String>> getProFileListByProId(@Param(value = "orgId") String orgId, @Param(value = "proId") String proId, @Param(value = "search") String search);
}
