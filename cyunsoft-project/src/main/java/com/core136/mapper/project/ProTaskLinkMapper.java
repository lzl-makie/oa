package com.core136.mapper.project;

import com.core136.bean.project.ProTaskLink;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ProTaskLinkMapper extends MyMapper<ProTaskLink> {
    /**
     * 获取任务关联列表
     *
     * @param orgId
     * @param proId
     * @return
     */
    public List<Map<String, String>> getProTaskLinkList(@Param(value = "orgId") String orgId, @Param(value = "proId") String proId);
}
