package com.core136.controller.project;

import com.core136.bean.account.Account;
import com.core136.bean.account.UserInfo;
import com.core136.bean.project.*;
import com.core136.bean.sys.CodeClass;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.project.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/set/proset")
public class RouteSetProController {
    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private ProPrivService proPrivService;

    @Autowired
    public void setProPrivService(ProPrivService proPrivService) {
        this.proPrivService = proPrivService;
    }

    private ProCostService proCostService;

    @Autowired
    public void setProCostService(ProCostService proCostService) {
        this.proCostService = proCostService;
    }

    private ProExpAccountService proExpAccountService;

    @Autowired
    public void setProExpAccountService(ProExpAccountService proExpAccountService) {
        this.proExpAccountService = proExpAccountService;
    }

    private ProModelService proModelService;

    @Autowired
    public void setProModelService(ProModelService proModelService) {
        this.proModelService = proModelService;
    }

    private ProRecordService proRecordService;

    @Autowired
    public void setProRecordService(ProRecordService proRecordService) {
        this.proRecordService = proRecordService;
    }

    private ProRoleService proRoleService;

    @Autowired
    public void setProRoleService(ProRoleService proRoleService) {
        this.proRoleService = proRoleService;
    }

    private ProSortService proSortService;

    @Autowired
    public void setProSortService(ProSortService proSortService) {
        this.proSortService = proSortService;
    }

    private ProFileService proFileService;

    @Autowired
    public void setProFileService(ProFileService proFileService) {
        this.proFileService = proFileService;
    }

    private ProProblemService proProblemService;

    @Autowired
    public void setProProblemService(ProProblemService proProblemService) {
        this.proProblemService = proProblemService;
    }

    private ProBpmRecordService proBpmRecordService;

    @Autowired
    public void setProBpmRecordService(ProBpmRecordService proBpmRecordService) {
        this.proBpmRecordService = proBpmRecordService;
    }

    private ProTaskService proTaskService;

    @Autowired
    public void setProTaskService(ProTaskService proTaskService) {
        this.proTaskService = proTaskService;
    }

    private ProTaskLinkService proTaskLinkService;

    @Autowired
    public void setProTaskLinkService(ProTaskLinkService proTaskLinkService) {
        this.proTaskLinkService = proTaskLinkService;
    }

    private ProApprovalService proApprovalService;

    @Autowired
    public void setProApprovalService(ProApprovalService proApprovalService) {
        this.proApprovalService = proApprovalService;
    }

    private ProTaskProcessService proTaskProcessService;

    @Autowired
    private void setProTaskProcessService(ProTaskProcessService proTaskProcessService) {
        this.proTaskProcessService = proTaskProcessService;
    }

    private ProProblemReplyService proProblemReplyService;

    @Autowired
    public void setProProblemReplyService(ProProblemReplyService proProblemReplyService) {
        this.proProblemReplyService = proProblemReplyService;
    }

    private ProFundsService proFundsService;

    @Autowired
    public void setProFundsService(ProFundsService proFundsService) {
        this.proFundsService = proFundsService;
    }

    /**
     * 项目流程
     *
     * @param proBpmRecord
     * @return
     */
    @RequestMapping(value = "/startProBpmRecordBpm", method = RequestMethod.POST)
    public RetDataBean startProBpmRecordBpm(ProBpmRecord proBpmRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            proBpmRecord.setRecordId(SysTools.getGUID());
            proBpmRecord.setRunId(SysTools.getGUID());
            proBpmRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            proBpmRecord.setCreateUser(account.getAccountId());
            proBpmRecord.setOrgId(account.getOrgId());
            proBpmRecordService.insertProBpmRecord(proBpmRecord);
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, "/app/core/apibpmstart?isEmpty=1&runId=" + proBpmRecord.getRunId() + "&flowId=" + proBpmRecord.getFlowId() + "&title=" + proBpmRecord.getTitle());
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 创建项目经费
     *
     * @param proFunds
     * @return
     */
    @RequestMapping(value = "/insertProFunds", method = RequestMethod.POST)
    public RetDataBean insertProFunds(ProFunds proFunds) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            proFunds.setRecordId(SysTools.getGUID());
            proFunds.setCreateUser(account.getAccountId());
            proFunds.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            proFunds.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, proFundsService.insertProFunds(proFunds));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除项目经费
     *
     * @param proFunds
     * @return
     */
    @RequestMapping(value = "/deleteProFunds", method = RequestMethod.POST)
    public RetDataBean deleteProFunds(ProFunds proFunds) {
        try {
            if (StringUtils.isBlank(proFunds.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            proFunds.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, proFundsService.deleteProFunds(proFunds));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新项目经费
     *
     * @param proFunds
     * @return
     */
    @RequestMapping(value = "/updateProFunds", method = RequestMethod.POST)
    public RetDataBean updateProFunds(ProFunds proFunds) {
        try {
            if (StringUtils.isBlank(proFunds.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(ProFunds.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", proFunds.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, proFundsService.updateProFunds(example, proFunds));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 创建项目文档
     *
     * @param proFile
     * @return
     */
    @RequestMapping(value = "/insertProFile", method = RequestMethod.POST)
    public RetDataBean insertProFile(ProFile proFile) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            proFile.setFileId(SysTools.getGUID());
            proFile.setCreateUser(account.getAccountId());
            proFile.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            proFile.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, proFileService.insertProFile(proFile));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 删除项目文档
     *
     * @param proFile
     * @return
     */
    @RequestMapping(value = "/deleteProFile", method = RequestMethod.POST)
    public RetDataBean deleteProFile(ProFile proFile) {
        try {
            if (StringUtils.isBlank(proFile.getFileId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            proFile.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, proFileService.deleteProFile(proFile));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新项目文档
     *
     * @param proFile
     * @return
     */
    @RequestMapping(value = "/updateProFile", method = RequestMethod.POST)
    public RetDataBean updateProFile(ProFile proFile) {
        try {
            if (StringUtils.isBlank(proFile.getFileId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(ProFile.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("fileId", proFile.getFileId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, proFileService.updateProFile(example, proFile));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 回复项目问题
     *
     * @param proProblemReply
     * @return
     */
    @RequestMapping(value = "/insertProProblemReply", method = RequestMethod.POST)
    public RetDataBean insertProProblemReply(ProProblemReply proProblemReply) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            proProblemReply.setRecordId(SysTools.getGUID());
            proProblemReply.setCreateUser(account.getAccountId());
            proProblemReply.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            proProblemReply.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, proProblemReplyService.insertProProblemReply(proProblemReply));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除问题回复记录
     *
     * @param proProblemReply
     * @return
     */
    @RequestMapping(value = "/deleteProProblemReply", method = RequestMethod.POST)
    public RetDataBean deleteProProblemReply(ProProblemReply proProblemReply) {
        try {
            if (StringUtils.isBlank(proProblemReply.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            proProblemReply.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, proProblemReplyService.deleteProProblemReply(proProblemReply));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新问题回复
     *
     * @param proProblemReply
     * @return
     */
    @RequestMapping(value = "/updateProProblemReply", method = RequestMethod.POST)
    public RetDataBean updateProProblemReply(ProProblemReply proProblemReply) {
        try {
            if (StringUtils.isBlank(proProblemReply.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(ProProblemReply.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", proProblemReply.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, proProblemReplyService.updateProProblemReply(example, proProblemReply));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 添加项目问题反馈
     *
     * @param proProblem
     * @return
     */
    @RequestMapping(value = "/insertProProblem", method = RequestMethod.POST)
    public RetDataBean insertProProblem(ProProblem proProblem) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            proProblem.setRecordId(SysTools.getGUID());
            proProblem.setCreateUser(account.getAccountId());
            proProblem.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            proProblem.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, proProblemService.insertProProblem(proProblem));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除项目问题反馈
     *
     * @param proProblem
     * @return
     */
    @RequestMapping(value = "/deleteProProblem", method = RequestMethod.POST)
    public RetDataBean deleteProProblem(ProProblem proProblem) {
        try {
            if (StringUtils.isBlank(proProblem.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            proProblem.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, proProblemService.deleteProProblem(proProblem));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新项目问题
     *
     * @param proProblem
     * @return
     */
    @RequestMapping(value = "/updateProProblem", method = RequestMethod.POST)
    public RetDataBean updateProProblem(ProProblem proProblem) {
        try {
            if (StringUtils.isBlank(proProblem.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(ProProblem.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", proProblem.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, proProblemService.updateProProblem(example, proProblem));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 添加项目任务处理结果的同时更新任务的进度
     *
     * @param proTaskProcess
     * @return
     */
    @RequestMapping(value = "/createProcess", method = RequestMethod.POST)
    public RetDataBean createProcess(ProTaskProcess proTaskProcess) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            proTaskProcess.setProcessId(SysTools.getGUID());
            proTaskProcess.setCreateUser(account.getAccountId());
            proTaskProcess.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            proTaskProcess.setOrgId(account.getOrgId());
            return proTaskProcessService.createProcess(proTaskProcess);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 添加任务处理过程
     *
     * @param proTaskProcess
     * @return
     */
    @RequestMapping(value = "/insertProTaskProcess", method = RequestMethod.POST)
    public RetDataBean insertProTaskProcess(ProTaskProcess proTaskProcess) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            proTaskProcess.setProcessId(SysTools.getGUID());
            proTaskProcess.setCreateUser(account.getAccountId());
            proTaskProcess.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            proTaskProcess.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, proTaskProcessService.insertProTaskProcess(proTaskProcess));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除项目任务处理过程
     *
     * @param proTaskProcess
     * @return
     */
    @RequestMapping(value = "/deleteProTaskProcess", method = RequestMethod.POST)
    public RetDataBean deleteProTaskProcess(ProTaskProcess proTaskProcess) {
        try {
            if (StringUtils.isBlank(proTaskProcess.getProcessId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            proTaskProcess.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, proTaskProcessService.deleteProTaskProcess(proTaskProcess));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新项目处理过程
     *
     * @param proTaskProcess
     * @return
     */
    @RequestMapping(value = "/updateProTaskProcess", method = RequestMethod.POST)
    public RetDataBean updateProTaskProcess(ProTaskProcess proTaskProcess) {
        try {
            if (StringUtils.isBlank(proTaskProcess.getProcessId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(ProTaskProcess.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("processId", proTaskProcess.getProcessId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, proTaskProcessService.updateProTaskProcess(example, proTaskProcess));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 添加项目审批意见
     *
     * @param proApproval
     * @return
     */
    @RequestMapping(value = "/setApprovalProRecrod", method = RequestMethod.POST)
    public RetDataBean setApprovalProRecrod(ProApproval proApproval) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            proApproval.setRecordId(SysTools.getGUID());
            proApproval.setCreateUser(account.getAccountId());
            proApproval.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            proApproval.setOrgId(account.getOrgId());
            return proApprovalService.setApprovalProRecrod(proApproval);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除项目审批意见
     *
     * @param proApproval
     * @return
     */
    @RequestMapping(value = "/deleteProApproval", method = RequestMethod.POST)
    public RetDataBean deleteProApproval(ProApproval proApproval) {
        try {
            if (StringUtils.isBlank(proApproval.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            proApproval.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, proApprovalService.deleteProApproval(proApproval));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新项目审批意见
     *
     * @param proApproval
     * @return
     */
    @RequestMapping(value = "/updateProApproval", method = RequestMethod.POST)
    public RetDataBean updateProApproval(ProApproval proApproval) {
        try {
            if (StringUtils.isBlank(proApproval.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(ProApproval.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", proApproval.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, proApprovalService.updateProApproval(example, proApproval));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 创建任务关联
     *
     * @param proTaskLink
     * @returnp
     */
    @RequestMapping(value = "/insertProTaskLink", method = RequestMethod.POST)
    public RetDataBean insertProTaskLink(ProTaskLink proTaskLink) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            proTaskLink.setTaskLinkId(SysTools.getGUID());
            proTaskLink.setCreateUser(account.getAccountId());
            proTaskLink.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            proTaskLink.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, proTaskLinkService.insertProTaskLink(proTaskLink));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除任务关联记录
     *
     * @param proTaskLink
     * @return
     */
    @RequestMapping(value = "/deleteProTaskLink", method = RequestMethod.POST)
    public RetDataBean deleteProTaskLink(ProTaskLink proTaskLink) {
        try {
            if (StringUtils.isBlank(proTaskLink.getTaskLinkId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            proTaskLink.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, proTaskLinkService.deleteProTaskLink(proTaskLink));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新任务关联记录
     *
     * @param proTaskLink
     * @return
     */
    @RequestMapping(value = "/updateProTaskLink", method = RequestMethod.POST)
    public RetDataBean updateProTaskLink(ProTaskLink proTaskLink) {
        try {
            if (StringUtils.isBlank(proTaskLink.getTaskLinkId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(ProTaskLink.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("taskLinkId", proTaskLink.getTaskLinkId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, proTaskLinkService.updateProTaskLink(example, proTaskLink));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 创建子任务
     *
     * @param proTask
     * @return
     */
    @RequestMapping(value = "/insertProTask", method = RequestMethod.POST)
    public RetDataBean insertProTask(ProTask proTask) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            proTask.setTaskId(SysTools.getGUID());
            proTask.setCreateUser(account.getAccountId());
            proTask.setStatus("0");
            proTask.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            proTask.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, proTaskService.insertProTask(proTask));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除子任务
     *
     * @param proTask
     * @return
     */
    @RequestMapping(value = "/deleteProTask", method = RequestMethod.POST)
    public RetDataBean deleteProTask(ProTask proTask) {
        try {
            if (StringUtils.isBlank(proTask.getTaskId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            proTask.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, proTaskService.deleteProTask(proTask));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新子任务
     *
     * @param proTask
     * @return
     */
    @RequestMapping(value = "/updateProTask", method = RequestMethod.POST)
    public RetDataBean updateProTask(ProTask proTask) {
        try {
            if (StringUtils.isBlank(proTask.getTaskId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(ProTask.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("taskId", proTask.getTaskId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, proTaskService.updateProTask(example, proTask));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 创建项目流程
     *
     * @param proBpmRecord
     * @return
     */
    @RequestMapping(value = "/insertProBpmRecord", method = RequestMethod.POST)
    public RetDataBean insertProBpmRecord(ProBpmRecord proBpmRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            proBpmRecord.setRecordId(SysTools.getGUID());
            proBpmRecord.setCreateUser(account.getAccountId());
            proBpmRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            proBpmRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, proBpmRecordService.insertProBpmRecord(proBpmRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除项止流程记录
     *
     * @param proBpmRecord
     * @return
     */
    @RequestMapping(value = "/deleteProBpmRecord", method = RequestMethod.POST)
    public RetDataBean deleteProBpmRecord(ProBpmRecord proBpmRecord) {
        try {
            if (StringUtils.isBlank(proBpmRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            proBpmRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, proBpmRecordService.deleteProBpmRecord(proBpmRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新项目流程记录
     *
     * @param proBpmRecord
     * @return
     */
    @RequestMapping(value = "/updateProBpmRecord", method = RequestMethod.POST)
    public RetDataBean updateProBpmRecord(ProBpmRecord proBpmRecord) {
        try {
            if (StringUtils.isBlank(proBpmRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(ProBpmRecord.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", proBpmRecord.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, proBpmRecordService.updateProBpmRecord(example, proBpmRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 创建项目
     *
     * @param proRecord
     * @return
     */
    @RequestMapping(value = "/insertProRecord", method = RequestMethod.POST)
    public RetDataBean insertProRecord(ProRecord proRecord) {
        try {
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            ProPriv proPriv = new ProPriv();
            proPriv.setOrgId(userInfo.getOrgId());
            proPriv = proPrivService.selectOneProPriv(proPriv);
            if (proPrivService.isInPriv(userInfo.getOrgId(), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getLeadLevel()) > 0) {
                proRecord.setProId(SysTools.getGUID());
                proRecord.setCreateUser(userInfo.getAccountId());
                proRecord.setStatus("0");
                proRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
                proRecord.setOrgId(userInfo.getOrgId());
                return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, proRecordService.insertProRecord(proRecord));
            } else {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除项目
     *
     * @param proRecord
     * @return
     */
    @RequestMapping(value = "/deleteProRecord", method = RequestMethod.POST)
    public RetDataBean deleteProRecord(ProRecord proRecord) {
        try {
            if (StringUtils.isBlank(proRecord.getProId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            proRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, proRecordService.deleteProRecord(proRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新项目信息
     *
     * @param proRecord
     * @return
     */
    @RequestMapping(value = "/updateProRecord", method = RequestMethod.POST)
    public RetDataBean updateProRecord(ProRecord proRecord) {
        try {
            if (StringUtils.isBlank(proRecord.getProId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(ProRecord.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("proId", proRecord.getProId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, proRecordService.updateProRecord(example, proRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 提交审批
     *
     * @param proRecord
     * @return
     */
    @RequestMapping(value = "/setToApproval", method = RequestMethod.POST)
    public RetDataBean setToApproval(ProRecord proRecord) {
        try {
            if (StringUtils.isBlank(proRecord.getProId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            ProPriv proPriv = new ProPriv();
            proPriv.setOrgId(account.getOrgId());
            proPriv = proPrivService.selectOneProPriv(proPriv);
            String notNeedApprovalPriv = proPriv.getNotNeedApprovalPriv();
            if (StringUtils.isNotBlank(notNeedApprovalPriv)) {
                if (Arrays.asList(notNeedApprovalPriv.split(",")).contains(account.getAccountId())) {
                    proRecord.setStatus("3");
                }
            }
            Example example = new Example(ProRecord.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("proId", proRecord.getProId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, proRecordService.updateProRecord(example, proRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 添加项目费用科目
     *
     * @param proExpAccount
     * @return
     */
    @RequestMapping(value = "/insertProExpAccount", method = RequestMethod.POST)
    public RetDataBean insertProExpAccount(ProExpAccount proExpAccount) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            proExpAccount.setExpId(SysTools.getGUID());
            proExpAccount.setCreateUser(account.getAccountId());
            if (StringUtils.isBlank(proExpAccount.getLevelId())) {
                proExpAccount.setLevelId("0");
            }
            proExpAccount.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            proExpAccount.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, proExpAccountService.insertProExpAccount(proExpAccount));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除项目费用科目
     *
     * @param proExpAccount
     * @return
     */
    @RequestMapping(value = "/deleteProExpAccount", method = RequestMethod.POST)
    public RetDataBean deleteProExpAccount(ProExpAccount proExpAccount) {
        try {
            if (StringUtils.isBlank(proExpAccount.getExpId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            proExpAccount.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, proExpAccountService.deleteProExpAccount(proExpAccount));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新项目费用科目
     *
     * @param proExpAccount
     * @return
     */
    @RequestMapping(value = "/updateProExpAccount", method = RequestMethod.POST)
    public RetDataBean updateProExpAccount(ProExpAccount proExpAccount) {
        try {
            if (StringUtils.isBlank(proExpAccount.getExpId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            if (StringUtils.isBlank(proExpAccount.getLevelId())) {
                proExpAccount.setLevelId("0");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(ProExpAccount.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("expId", proExpAccount.getExpId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, proExpAccountService.updateProExpAccount(example, proExpAccount));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 添加项目费用分类
     *
     * @param proCost
     * @return
     */
    @RequestMapping(value = "/insertProCost", method = RequestMethod.POST)
    public RetDataBean insertProCost(ProCost proCost) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            proCost.setCostId(SysTools.getGUID());
            proCost.setCreateUser(account.getAccountId());
            proCost.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            proCost.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, proCostService.insertProCost(proCost));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除项目费用类型
     *
     * @param proCost
     * @return
     */
    @RequestMapping(value = "/deleteProCost", method = RequestMethod.POST)
    public RetDataBean deleteProCost(ProCost proCost) {
        try {
            if (StringUtils.isBlank(proCost.getCostId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            proCost.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, proCostService.deleteProCost(proCost));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新项目费用类型
     *
     * @param proCost
     * @return
     */
    @RequestMapping(value = "/updateProCost", method = RequestMethod.POST)
    public RetDataBean updateProCost(ProCost proCost) {
        try {
            if (StringUtils.isBlank(proCost.getCostId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(ProCost.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("costId", proCost.getCostId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, proCostService.updateProCost(example, proCost));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 批量删除项目费用类型列表
     *
     * @param costIddArr
     * @return
     */
    @RequestMapping(value = "/deleteProCostBatch", method = RequestMethod.POST)
    public RetDataBean deleteProCostBatch(@RequestParam(value = "costIddArr[]") String[] costIddArr) {
        try {
            if (costIddArr.length == 0) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            }
            List<String> list = new ArrayList<String>(Arrays.asList(costIddArr));
            Example example = new Example(CodeClass.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andIn("costId", list);
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, proCostService.deleteProCost(example));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 批量删除项目角色
     *
     * @param recordIdArr
     * @return
     */
    @RequestMapping(value = "/deleteProRoleBatch", method = RequestMethod.POST)
    public RetDataBean deleteProRoleBatch(@RequestParam(value = "recordIdArr[]") String[] recordIdArr) {
        try {
            if (recordIdArr.length == 0) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            }
            List<String> list = new ArrayList<String>(Arrays.asList(recordIdArr));
            Example example = new Example(CodeClass.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andIn("classCodeId", list);
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, proRoleService.deleteProRole(example));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 添加项目人员角色
     *
     * @param proRole
     * @return
     */
    @RequestMapping(value = "/insertProRole", method = RequestMethod.POST)
    public RetDataBean insertProRole(ProRole proRole) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            proRole.setRecordId(SysTools.getGUID());
            proRole.setCreateUser(account.getAccountId());
            proRole.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            proRole.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, proRoleService.insertProRole(proRole));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除项目人员角色
     *
     * @param proRole
     * @return
     */
    @RequestMapping(value = "/deleteProRole", method = RequestMethod.POST)
    public RetDataBean deleteProRole(ProRole proRole) {
        try {
            if (StringUtils.isBlank(proRole.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            proRole.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, proRoleService.deleteProRole(proRole));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新项目人员角色
     *
     * @param proRole
     * @return
     */
    @RequestMapping(value = "/updateProRole", method = RequestMethod.POST)
    public RetDataBean updateProRole(ProRole proRole) {
        try {
            if (StringUtils.isBlank(proRole.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(ProRole.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", proRole.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, proRoleService.updateProRole(example, proRole));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 设置项目人员权限
     *
     * @param proPriv
     * @return
     */
    @RequestMapping(value = "/setProPriv", method = RequestMethod.POST)
    public RetDataBean setProPriv(ProPriv proPriv) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, proPrivService.setProPriv(account, proPriv));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 添加项目分类
     *
     * @param proSort
     * @return
     */
    @RequestMapping(value = "/insertProSort", method = RequestMethod.POST)
    public RetDataBean insertProSort(ProSort proSort) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            proSort.setSortId(SysTools.getGUID());
            proSort.setCreateUser(account.getAccountId());
            if (StringUtils.isBlank(proSort.getLevelId())) {
                proSort.setLevelId("0");
            }
            proSort.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            proSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, proSortService.insertProSort(proSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除项目分类
     *
     * @param proSort
     * @return
     */
    @RequestMapping(value = "/deleteProSort", method = RequestMethod.POST)
    public RetDataBean deleteProSort(ProSort proSort) {
        try {
            if (StringUtils.isBlank(proSort.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            proSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, proSortService.deleteProSort(proSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新项目分类
     *
     * @param proSort
     * @return
     */
    @RequestMapping(value = "/updateProSort", method = RequestMethod.POST)
    public RetDataBean updateProSort(ProSort proSort) {
        try {
            if (StringUtils.isBlank(proSort.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(proSort.getLevelId())) {
                proSort.setLevelId("0");
            }
            Example example = new Example(ProSort.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("sortId", proSort.getSortId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, proSortService.updateProSort(example, proSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


}
