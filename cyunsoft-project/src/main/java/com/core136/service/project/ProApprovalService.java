package com.core136.service.project;

import com.core136.bean.project.ProApproval;
import com.core136.bean.project.ProRecord;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.project.ProApprovalMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;


@Service
public class ProApprovalService {
    private ProApprovalMapper proApprovalMapper;

    @Autowired
    public void setProApprovalMapper(ProApprovalMapper proApprovalMapper) {
        this.proApprovalMapper = proApprovalMapper;
    }

    private ProRecordService proRecordService;

    @Autowired
    public void setProRecordService(ProRecordService proRecordService) {
        this.proRecordService = proRecordService;
    }

    public int insertProApproval(ProApproval proApproval) {
        return proApprovalMapper.insert(proApproval);
    }

    public int deleteProApproval(ProApproval proApproval) {
        return proApprovalMapper.delete(proApproval);
    }

    public int updateProApproval(Example example, ProApproval proApproval) {
        return proApprovalMapper.updateByExampleSelective(proApproval, example);
    }

    public ProApproval selectOneProApproval(ProApproval proApproval) {
        return proApprovalMapper.selectOne(proApproval);
    }

    /**
     * 项目审批成功
     *
     * @param proApproval
     * @return
     */
    @Transactional(value = "generalTM")
    public RetDataBean setApprovalProRecrod(ProApproval proApproval) {
        ProRecord proRecord = new ProRecord();
        proRecord.setStatus(proApproval.getStatus());
        Example example = new Example(ProRecord.class);
        proRecordService.updateProRecord(example, proRecord);
        return RetDataTools.Ok(MessageCode.MESSAGE_APPROVAL_SUCCESS, insertProApproval(proApproval));
    }

    /**
     * 获取项目历史审批记录
     *
     * @param orgId
     * @param status
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getOldProApprovalList(String orgId, String status, String beginTime, String endTime, String search) {
        return proApprovalMapper.getOldProApprovalList(orgId, status, beginTime, endTime, "%" + search + "%");
    }

    /**
     * 获取项目历史审批记录
     *
     * @param pageParam
     * @param status
     * @param beginTime
     * @param endTime
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getOldProApprovalList(PageParam pageParam, String status, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getOldProApprovalList(pageParam.getOrgId(), status, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
