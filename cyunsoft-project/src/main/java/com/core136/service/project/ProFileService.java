package com.core136.service.project;

import com.core136.bean.project.ProFile;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.project.ProFileMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ProFileService {
    private ProFileMapper proFileMapper;

    @Autowired
    public void setProFileMapper(ProFileMapper proFileMapper) {
        this.proFileMapper = proFileMapper;
    }

    public int insertProFile(ProFile proFile) {
        return proFileMapper.insert(proFile);
    }

    public int deleteProFile(ProFile proFile) {
        return proFileMapper.delete(proFile);
    }

    public int updateProFile(Example example, ProFile proFile) {
        return proFileMapper.updateByExampleSelective(proFile, example);
    }

    public ProFile selectOneProFile(ProFile proFile) {
        return proFileMapper.selectOne(proFile);
    }

    /**
     * 获取项目文档
     *
     * @param orgId
     * @param proId
     * @param search
     * @return
     */
    public List<Map<String, String>> getProFileListByProId(String orgId, String proId, String search) {
        return proFileMapper.getProFileListByProId(orgId, proId, "%" + search + "%");
    }

    /**
     * 获取项目文档
     *
     * @param pageParam
     * @param proId
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getProFileListByProId(PageParam pageParam, String proId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getProFileListByProId(pageParam.getOrgId(), proId, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
