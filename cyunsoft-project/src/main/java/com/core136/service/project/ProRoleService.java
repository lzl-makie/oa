package com.core136.service.project;

import com.core136.bean.project.ProRole;
import com.core136.mapper.project.ProRoleMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service
public class ProRoleService {

    private ProRoleMapper proRoleMapper;

    @Autowired
    public void setProRoleMapper(ProRoleMapper proRoleMapper) {
        this.proRoleMapper = proRoleMapper;
    }

    public int insertProRole(ProRole proRole) {
        return proRoleMapper.insert(proRole);
    }

    public int deleteProRole(ProRole proRole) {
        return proRoleMapper.delete(proRole);
    }

    public int updateProRole(Example example, ProRole proRole) {
        return proRoleMapper.updateByExampleSelective(proRole, example);
    }

    public ProRole selectOneProRole(ProRole proRole) {
        return proRoleMapper.selectOne(proRole);
    }

    /**
     * 获取项目角色列表
     *
     * @param example
     * @param pageNumber
     * @param pageSize
     * @return
     */
    public PageInfo<ProRole> getProRoleList(Example example, int pageNumber, int pageSize) {
        PageHelper.startPage(pageNumber, pageSize);
        List<ProRole> datalist = getProRoleList(example);
        PageInfo<ProRole> pageInfo = new PageInfo<ProRole>(datalist);
        return pageInfo;
    }

    public List<ProRole> getProRoleList(Example example) {
        return proRoleMapper.selectByExample(example);
    }

    public int deleteProRole(Example example) {
        return proRoleMapper.deleteByExample(example);
    }
}
