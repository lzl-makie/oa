package com.core136.service.project;

import com.core136.bean.project.ProCost;
import com.core136.mapper.project.ProCostMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ProCostService {

    private ProCostMapper proCostMapper;

    @Autowired
    public void setProCostMapper(ProCostMapper proCostMapper) {
        this.proCostMapper = proCostMapper;
    }

    public int insertProCost(ProCost proCost) {
        return proCostMapper.insert(proCost);
    }

    public int deleteProCost(ProCost proCost) {
        return proCostMapper.delete(proCost);
    }

    public int updateProCost(Example example, ProCost proCost) {
        return proCostMapper.updateByExampleSelective(proCost, example);
    }

    public ProCost selectOneProCost(ProCost proCost) {
        return proCostMapper.selectOne(proCost);
    }

    /**
     * 获取项目费用类型列表
     *
     * @param example
     * @param pageNumber
     * @param pageSize
     * @return
     */
    public PageInfo<ProCost> getProCostList(Example example, int pageNumber, int pageSize) {
        PageHelper.startPage(pageNumber, pageSize);
        List<ProCost> datalist = getProCostList(example);
        PageInfo<ProCost> pageInfo = new PageInfo<ProCost>(datalist);
        return pageInfo;
    }

    public List<ProCost> getProCostList(Example example) {
        return proCostMapper.selectByExample(example);
    }

    public int deleteProCost(Example example) {
        return proCostMapper.deleteByExample(example);
    }

    /**
     * 获取费用类型列表
     *
     * @param orgId
     * @return
     */
    public List<Map<String, String>> getProCostForSelect(String orgId) {
        return proCostMapper.getProCostForSelect(orgId);
    }
}
