package com.core136.service.project;

import com.core136.bean.project.ProModel;
import com.core136.mapper.project.ProModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

@Service
public class ProModelService {
    private ProModelMapper proModelMapper;

    @Autowired
    public void setProModelMapper(ProModelMapper proModelMapper) {
        this.proModelMapper = proModelMapper;
    }

    public int insertProMode(ProModel proModel) {
        return proModelMapper.insert(proModel);
    }

    public int deleteProModel(ProModel proModel) {
        return proModelMapper.delete(proModel);
    }

    public int updateProModel(Example example, ProModel proModel) {
        return proModelMapper.updateByExampleSelective(proModel, example);
    }

    public ProModel selectOneProModel(ProModel proModel) {
        return proModelMapper.selectOne(proModel);
    }

}
