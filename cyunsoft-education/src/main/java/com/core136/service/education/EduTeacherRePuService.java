package com.core136.service.education;

import com.core136.bean.education.EduTeacherRePu;
import com.core136.mapper.education.EduTeacherRePuMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

@Service
public class EduTeacherRePuService {
    private EduTeacherRePuMapper eduTeacherRePuMapper;

    @Autowired
    public void setEduTeacherRePuMapper(EduTeacherRePuMapper eduTeacherRePuMapper) {
        this.eduTeacherRePuMapper = eduTeacherRePuMapper;
    }

    public int insertEduTeacherRePu(EduTeacherRePu eduTeacherRePu) {
        return eduTeacherRePuMapper.insert(eduTeacherRePu);
    }

    public int deleteEduTeacherRePu(EduTeacherRePu eduTeacherRePu) {
        return eduTeacherRePuMapper.delete(eduTeacherRePu);
    }

    public EduTeacherRePu selectOneEduTeacherRePu(EduTeacherRePu eduTeacherRePu) {
        return eduTeacherRePuMapper.selectOne(eduTeacherRePu);
    }

    public int updateEduTeacherRePu(Example example, EduTeacherRePu eduTeacherRePu) {
        return eduTeacherRePuMapper.updateByExampleSelective(eduTeacherRePu, example);
    }

}
