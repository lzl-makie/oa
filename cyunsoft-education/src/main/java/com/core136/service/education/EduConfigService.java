package com.core136.service.education;

import com.core136.bean.education.EduConfig;
import com.core136.mapper.education.EduConfigMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

@Service
public class EduConfigService {
    private EduConfigMapper eduConfigMapper;
    @Autowired
    public void setEduConfigMapper(EduConfigMapper eduConfigMapper)
    {
        this.eduConfigMapper = eduConfigMapper;
    }

    public int insertEduConfig(EduConfig eduConfig)
    {
        return eduConfigMapper.insert(eduConfig);
    }

    public int deleteEduConfig(EduConfig eduConfig)
    {
        return eduConfigMapper.delete(eduConfig);
    }

    public EduConfig selectOneEduConfig(EduConfig eduConfig)
    {
        return eduConfigMapper.selectOne(eduConfig);
    }

    public int updateEduConfig(Example example,EduConfig eduConfig)
    {
        return eduConfigMapper.updateByExampleSelective(eduConfig,example);
    }
}


