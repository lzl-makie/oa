package com.core136.service.education;

import com.core136.bean.education.EduCampus;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.education.EduCampusMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * @ClassName: EduCampusService
 * @Description: 学校校区
 * @author: 稠云技术
 * @date: 2020年10月14日 下午9:59:19
 * @Copyright: 2020 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Service
public class EduCampusService {
    private EduCampusMapper eduCampusMapper;

    @Autowired
    public void setEduCampusMapper(EduCampusMapper eduCampusMapper) {
        this.eduCampusMapper = eduCampusMapper;
    }

    public int insertEduCampus(EduCampus eduCampus) {
        return eduCampusMapper.insert(eduCampus);
    }

    public int deleteEduCampus(EduCampus eduCampus) {
        return eduCampusMapper.delete(eduCampus);
    }

    public int updateEduCampus(Example example, EduCampus eduCampus) {
        return eduCampusMapper.updateByExampleSelective(eduCampus, example);
    }

    public List<EduCampus> getAllEduCampus(EduCampus eduCampus) {
        return eduCampusMapper.select(eduCampus);
    }

    public EduCampus selectOneEduCampus(EduCampus eduCampus) {
        return eduCampusMapper.selectOne(eduCampus);
    }

    /**
     * @param orgId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getEduCampusList
     * @Description:  获取校区列表
     */
    public List<Map<String, String>> getEduCampusList(String orgId, String search) {
        return eduCampusMapper.getEduCampusList(orgId, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getEduCampusList
     * @Description:  获取校区列表
     */
    public PageInfo<Map<String, String>> getEduCampusList(PageParam pageParam) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getEduCampusList(pageParam.getOrgId(), pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
