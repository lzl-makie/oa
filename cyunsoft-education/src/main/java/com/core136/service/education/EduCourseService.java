package com.core136.service.education;

import com.core136.bean.education.EduCourse;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.education.EduCourseMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class EduCourseService {
    private EduCourseMapper eduCourseMapper;

    @Autowired
    public void setEduCourseMapper(EduCourseMapper eduCourseMapper) {
        this.eduCourseMapper = eduCourseMapper;
    }

    public int insertEduCourse(EduCourse eduCourse) {
        return eduCourseMapper.insert(eduCourse);
    }

    public int deleteEduCourse(EduCourse eduCourse) {
        return eduCourseMapper.delete(eduCourse);
    }

    public int deleteEduCourse(Example example) {
        return eduCourseMapper.deleteByExample(example);
    }

    public int updateEduCourse(Example example, EduCourse eduCourse) {
        return eduCourseMapper.updateByExampleSelective(eduCourse, example);
    }

    public EduCourse selectOneEduCourse(EduCourse eduCourse) {
        return eduCourseMapper.selectOne(eduCourse);
    }

    /**
     * @param orgId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getEduCourseLsit
     * @Description:  获取课程列表
     */
    public List<Map<String, String>> getEduCourseList(String orgId, String search) {
        return eduCourseMapper.getEduCourseList(orgId, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getEduCourseLsit
     * @Description:  获取课程列表
     */
    public PageInfo<Map<String, String>> getEduCourseList(PageParam pageParam) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getEduCourseList(pageParam.getOrgId(), pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * 获取课程名称
     *
     * @param orgId
     * @param list
     * @return
     */
    public List<Map<String, String>> getEduCourseNameByIds(String orgId, List<String> list) {
        return eduCourseMapper.getEduCourseNameByIds(orgId, list);
    }

    /**
     * 获取所有备选课程
     *
     * @param orgId
     * @return
     */
    public List<Map<String, String>> getEduCourseListForTags(String orgId) {
        return eduCourseMapper.getEduCourseListForTags(orgId);
    }

}
