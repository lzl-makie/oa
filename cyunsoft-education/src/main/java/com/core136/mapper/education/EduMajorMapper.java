package com.core136.mapper.education;

import com.core136.bean.education.EduMajor;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface EduMajorMapper extends MyMapper<EduMajor> {

    public List<Map<String, String>> getEduMajorListForTags(@Param(value = "orgId") String orgId);

    public List<Map<String, String>> getEduMajorNameByIds(@Param(value = "orgId") String orgId, @Param(value = "list") List<String> list);
}
