$(function () {
    query();
    $(".js-addsemester").unbind("click").click(function () {
        doadd();
    });
    jeDate("#beginTime", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTime", {
        format: "YYYY-MM-DD",
    });
});

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/eduget/getEduSemesterList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        sortOrder: 'desc',
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        search: true,//启用搜索
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'semesterId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        },
            {
                field: 'num',
                title: '序号',//标题  可不加
                width: '50px',
                formatter: function (value, row, index) {
                    return index + 1;
                }
            },
            {
                field: 'title',
                title: '学期名称',
                sortable: true,
                width: '100px'
            }, {
                field: 'schoolYear',
                title: '学年',
                sortable: true,
                width: '100px'
            },
            {
                field: 'beginTime',
                width: '100px',
                title: '开学日期'
            },
            {
                field: 'endTime',
                title: '放假日期',
                width: '100px',
            },
            {
                field: 'isLastOrNext',
                title: '学期类型',
                width: '100px',
                formatter: function (value, row, index) {
                    if (value == "1") {
                        return "上学期"
                    } else if (value == "2") {
                        return "下学期";
                    } else {
                        return "未知";
                    }
                }
            },
            {
                field: 'currentStatus',
                title: '当前学期',
                width: '100px',
                formatter: function (value, row, index) {
                    if (value == "0") {
                        return "否"
                    } else if (value == "1") {
                        return "是";
                    } else {
                        return "未知";
                    }
                }
            },
            {
                field: 'opt',
                title: '操作',
                align: 'center',
                width: '100px',
                formatter: function (value, row, index) {
                    return createOptBtn(row.campusId);
                }
            }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
                layer.msg(sysmsg[res.msg]);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function createOptBtn(semesterId) {
    var html = "<a href=\"javascript:void(0);edit('" + semesterId + "')\" class=\"btn btn-sky btn-xs\" >修改</a>&nbsp;&nbsp;<a href=\"javascript:void(0);del('" + semesterId + "')\" class=\"btn btn-darkorange btn-xs\" >删除</a>";
    return html;
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order
    };
    return temp;
};

function edit(semesterId) {
    document.getElementById("form1").reset();
    $.ajax({
        url: "/ret/eduget/getEduSemesterById",
        type: "post",
        dataType: "json",
        data: {
            semesterId: semesterId
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                for (var name in data.list) {
                    $("#" + name).val(data.list[name]);
                }
            }
        }
    });
    $("#setsemester").modal("show");
    $(".js-save").unbind("click").click(function () {
        $.ajax({
            url: "/set/eduset/updateEduSemester",
            type: "post",
            dataType: "json",
            data: {
                semesterId: semesterId,
                sortNo: $("#sortNo").val(),
                title: $("#title").val(),
                beginTime: $("#beginTime").val(),
                endTime: $("#endTime").val(),
                isLastOrNext: $("#isLastOrNext").val(),
                schoolYear: $("#schoolYear").val(),
                currentStatus: $("#currentStatus").val()
            },
            success: function (data) {
                if (data.status == "500") {
                    console.log(data.msg);
                } else {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable("refresh");
                }
            }
        });
        $("#setsemester").modal("hide");
    });

}

function del(semesterId) {
    if (confirm(sysmsg['OPT_CONFIRM_DELETE'])) {
        $.ajax({
            url: "/set/sysset/deleteEduSemester",
            type: "post",
            dataType: "json",
            data: {
                semesterId: semesterId
            },
            success: function (data) {
                if (data.status == "500") {
                    console.log(data.msg);
                } else {
                    $("#myTable").bootstrapTable("refresh");
                    layer.msg(sysmsg[data.msg]);
                }
            }
        });
    } else {
        return;
    }
}

function doadd() {
    document.getElementById("form1").reset();
    $("#setsemester").modal("show");
    $(".js-save").unbind("click").click(function () {
        $.ajax({
            url: "/set/eduset/insertEduSemester",
            type: "post",
            dataType: "json",
            data: {
                sortNo: $("#sortNo").val(),
                title: $("#title").val(),
                beginTime: $("#beginTime").val(),
                endTime: $("#endTime").val(),
                isLastOrNext: $("#isLastOrNext").val(),
                schoolYear: $("#schoolYear").val(),
                currentStatus: $("#currentStatus").val()
            },
            success: function (data) {
                if (data.status == "500") {
                    console.log(data.msg);
                } else {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable("refresh");
                }
            }
        });
        $("#setsemester").modal("hide");
    });
}
