$(function () {
    $.ajax({
        url: "/ret/eduget/getEduTeacherById",
        type: "post",
        dataType: "json",
        data: {teacherId: teacherId},
        success: function (data) {
            if (data.status == "200") {
                var info = data.list;
                for (var id in info) {
                    if (id == "headImg") {
                        $("#file_img").attr("src", "/sys/file/getStaticImg?module=eduphotos&fileName=" + info.headImg);
                    } else if (id == "sex") {
                        if (info[id] == "1") {
                            $("#" + id).html("男");
                        } else if (info[id] == "0") {
                            $("#" + id).html("女");
                        } else if (info[id] == "2") {
                            $("#" + id).html("其他");
                        }
                    } else if (id == "accountId") {
                        $("#" + id).html(getUserNameByStr(info[id]));
                    } else if (id == "userName") {
                        if (info[id] == "" || info[id] == null) {
                            $("#" + id).html(getUserNameByStr(info.accountId));
                        } else {
                            $("#" + id).html(info[id]);
                        }
                    } else if (id == "education") {
                        $("#" + id).html(getBaseEducationById(info[id]));
                    } else if (id == "nation") {
                        $("#" + id).html(getBaseNationById(info[id]));
                    } else if (id == "teacherType") {
                        if (info[id] == "0") {
                            $("#" + id).html("未知");
                        } else if (info[id] == "1") {
                            $("#" + id).html("幼儿园教师资格证");
                        } else if (info[id] == "2") {
                            $("#" + id).html("小学教师资格证");
                        } else if (info[id] == "3") {
                            $("#" + id).html("初级中学教师资格证");
                        } else if (info[id] == "4") {
                            $("#" + id).html("高级中学教师资格证");
                        } else if (info[id] == "5") {
                            $("#" + id).html("中等职业学校教师资格证");
                        } else if (info[id] == "6") {
                            $("#" + id).html("中等职业学校实习指导教师资格证");
                        } else if (info[id] == "7") {
                            $("#" + id).html("高等学校教师资格证");
                        } else if (info[id] == "8") {
                            $("#" + id).html("成人/大学教育的教师资格证");
                        }
                    } else {
                        $("#" + id).html(info[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})

function getBaseNationById(nationId) {
    var retStr = "";
    if (nationId == "") {
        return retStr;
    } else {
        $.ajax({
            url: "/ret/baseinfoget/getBaseNationById",
            type: "post",
            dataType: "json",
            async: false,
            data: {nationId: nationId},
            success: function (data) {
                if (data.status == "200") {
                    retStr = data.list.nationName;
                }
            }
        });
    }
    return retStr;
}


function getBaseEducationById(educationId) {
    var retStr = "";
    if (educationId == "") {
        return retStr;
    } else {
        $.ajax({
            url: "/ret/baseinfoget/getBaseEducationById",
            type: "post",
            dataType: "json",
            async: false,
            data: {educationId: educationId},
            success: function (data) {
                if (data.status == "200") {
                    retStr = data.list.educationName;
                }
            }
        });
    }
    return retStr;
}
