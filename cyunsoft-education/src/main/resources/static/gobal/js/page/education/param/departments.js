var zTree;
var setting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/eduget/getEduDepartmentsTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    callback: {
        onClick: zTreeOnClick
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "sortLevel",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    }
};

var setting1 = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/eduget/getEduDepartmentsTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "sortLevel",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            let zTree = $.fn.zTree.getZTreeObj("menuTree");
            let nodes = zTree.getSelectedNodes();
            let v = [];
            let vid = [];
            nodes.sort(function compare(a, b) {
                return a.id - b.id;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v.push(nodes[i].sortName);
                vid.push(nodes[i].sortId);
            }
            $("#levelId").val(v.join(","));
            $("#levelId").attr("data-value", vid.join(","));
        }
    }
};

$(function () {
    getAllEduCampusList();
    $.ajax({
        url: "/ret/eduget/getEduDepartmentsTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            zTree = $.fn.zTree.init($("#tree"), setting, data);// 初始化树节点时，添加同步获取的数据
            var topNode = [{
                sortName: "TOP分类",
                isParent: "false",
                sortId: ""
            }];
            var newTreeNodes = topNode.concat(data);
            $.fn.zTree.init($("#menuTree"), setting1, newTreeNodes);
        }
    });
    $("#createbut").unbind("click").click(function () {
        addEduDepartments();
    });
    $("#cbut").unbind("click").click(function () {
        document.getElementById("form").reset();
        $("#leadId").attr("data-value", "");
        $("#createbut").show();
        $("#updatabut").hide();
        $("#delbut").hide();
    });
    $("#levelId").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#menuContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });
    $("body").unbind("click").click(function () {
        $("#menuContent").hide();
    });

    $("#menuContent").unbind("click").click(function (e) {
        e.stopPropagation();
    });
});

function zTreeOnClick(event, treeId, treeNode) {
    document.getElementById("form").reset();
    $("#majors").attr("data-value", "");
    $.ajax({
        url: "/ret/eduget/getEduDepartmentsById",
        type: "post",
        dataType: "json",
        data: {
            departmentsId: treeNode.sortId
        },
        success: function (data) {
            if (data.status == "200") {
                var v = data.list;
                for (name in v) {
                    if (name == "levelId") {
                        $("#levelId").attr("data-value", v["levelId"]);
                        $.ajax({
                            url: "/ret/eduget/getEduDepartmentsById",
                            type: "post",
                            dataType: "json",
                            data: {
                                departmentsId: v["levelId"]
                            },
                            success: function (data) {
                                if (data.status == "200") {
                                    if (data.list) {
                                        $("#levelId").val(data.list.departmentsName);
                                    } else {
                                        $("#levelId").val("");
                                    }
                                }
                            }
                        });
                    } else if (name == "leadId") {
                        $("#leadId").val(getUserNameByStr(v["leadId"]));
                        $("#leadId").attr("data-value", v["leadId"]);
                    } else if (name == "majors") {
                        $("#majors").val(getMajorsName(v["majors"]));
                        $("#majors").attr("data-value", v["majors"]);
                    } else {
                        $("#" + name).val(v[name]);
                    }
                }
            } else {
                console.log(data.msg);
            }

        }
    });
    $("#createbut").hide();
    $("#updatabut").show();
    $("#delbut").show();
    $("#updatabut").unbind("click").click(function () {
        updateEduDepartments(treeNode.sortId);
    });
    $("#delbut").unbind("click").click(function () {
        delEduDepartments(treeNode.sortId);
    });

}

function delEduDepartments(departmentsId) {
    if (confirm("确定删除当前院系吗？")) {
        $.ajax({
            url: "/set/eduset/deleteEduDepartments",
            type: "post",
            dataType: "json",
            data: {
                departmentsId: departmentsId
            },
            success: function (data) {
                if (data.status == 200) {
                    layer.msg(sysmsg[data.msg]);
                    location.reload();
                } else {
                    console.log(data.msg);
                }
            }
        });
    } else {
        return;
    }
}

function addEduDepartments() {
    $.ajax({
        url: "/set/eduset/insertEduDepartments",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            departmentsName: $("#departmentsName").val(),
            campusId: $("#campusId").val(),
            majors: $("#majors").attr("data-value"),
            levelId: $("#levelId").attr("data-value"),
            leadId: $("#leadId").attr("data-value"),
            tel: $("#tel").val(),
            remark: $("#remark").val()
        },
        success: function (data) {
            if (data.status == 200) {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else {
                console.log(data.msg);
            }
        }
    });
}

function updateEduDepartments(departmentsId) {
    $.ajax({
        url: "/set/eduset/updateEduDepartments",
        type: "post",
        dataType: "json",
        data: {
            departmentsId: departmentsId,
            sortNo: $("#sortNo").val(),
            campusId: $("#campusId").val(),
            majors: $("#majors").attr("data-value"),
            departmentsName: $("#departmentsName").val(),
            levelId: $("#levelId").attr("data-value"),
            leadId: $("#leadId").attr("data-value"),
            tel: $("#tel").val(),
            remark: $("#remark").val()
        },
        success: function (data) {
            if (data.status == 200) {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else {
                console.log(data.msg);
            }
        }
    });
}

function getAllEduCampusList() {
    $.ajax({
        url: "/ret/eduget/getAllEduCampusList",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == 200) {
                $("#campusId").append("<option value='0'>请选择</option>");
                if (data.list) {
                    for (i = 0; i < data.list.length; i++) {
                        $("#campusId").append("<option value='" + data.list[i].campusId + "'>" + data.list[i].name + "</option>");
                    }
                }
            } else {
                console.log(data.msg);
            }
        }
    });
}
