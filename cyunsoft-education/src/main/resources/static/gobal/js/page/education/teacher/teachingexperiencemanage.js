$(function () {
    jeDate("#beginTimeQuery", {
        format: "YYYY-MM",
    });
    jeDate("#endTimeQuery", {
        format: "YYYY-MM"
    });
    jeDate("#beginTime", {
        format: "YYYY-MM",
    });
    jeDate("#endTime", {
        format: "YYYY-MM"
    });
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })
    $(".js-auto-select").each(function () {
        var module = $(this).attr("module");
        createAutoSelect(module);
    });
    $(".js-auto-select-query").each(function () {
        var module = $(this).attr("module");
        createAutoSelectQuery(module);
    });
    $('#remark').summernote({height: 300});
    query();
    $("#teacherId").select2({
        theme: "bootstrap",
        allowClear: true,
        placeholder: "请输入教师的姓名、手机号、身份证号",
        query: function (query) {
            var url = "/ret/eduget/getTeacherListForSelect2";
            var param = {search: query.term}; // 查询参数，query.term为用户在select2中的输入内容.
            var type = "json";
            var data = {results: []};
            $.post(
                url,
                param,
                function (datas) {
                    var datalist = datas.list;
                    for (var i = 0, len = datalist.length; i < len; i++) {
                        var info = datalist[i];
                        var option = {
                            "id": info.teacherId,
                            "text": info.userName
                        };
                        data.results.push(option);
                    }
                    query.callback(data);
                }, type);

        },
        escapeMarkup: function (markup) {
            return markup;
        },
        minimumInputLength: 2,
        formatResult: function (data) {
            return '<div class="select2-user-result">' + data.text + '</div>'
        },
        formatSelection: function (data) {
            return data.text;
        },
        initSelection: function (data, cb) {
            cb(data);
        }
    });
})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/eduget/getEduTeacherExperienceList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        search: true,//启用搜索
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'recordId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',//标题  可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'userName',
            width: '100px',
            title: '姓名',
            formatter: function (value, row, index) {
                return "<a href=\"javascript:void(0);details('" + row.recordId + "')\" style='cursor: pointer'>" + value + "</a>";
            }
        }, {
            field: 'beginTime',
            width: '100px',
            title: '任教开始时间'
        }, {
            field: 'endTime',
            width: '100px',
            title: '任教结束时间'
        }, {
            field: 'courseType',
            width: '100px',
            title: '任教课程',
            formatter: function (value, row, index) {
                return getEduClassCodeName("courseType", value);
            }
        }, {
            field: 'grade',
            title: '年级',
            width: '100px',
            formatter: function (value, row, index) {
                return getEduClassCodeName("grade", value);
            }
        }, {
            field: 'teachingSchool',
            title: '任教机构',
            width: '100px'
        }, {
            field: 'opt',
            title: '操作',
            align: 'center',
            width: '100px',
            formatter: function (value, row, index) {
                return createOptBtn(row.recordId);
            }
        }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list
                    //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function createOptBtn(recordId) {
    var html = "<a href=\"javascript:void(0);edit('" + recordId + "')\" class=\"btn btn-primary btn-xs\">编辑</a>&nbsp;&nbsp;" +
        "<a href=\"javascript:void(0);deleteRecord('" + recordId + "')\" class=\"btn btn-darkorange btn-xs\" >删除</a>";
    return html;
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        courseType: $("#courseTypeQuery").val(),
        grade: $("#gradeQuery").val(),
        beginTime: $("#beginTimeQuery").val(),
        endTime: $("#endTimeQuery").val()
    };
    return temp;
};

function details(recordId) {
    window.open("/app/core/education/teacher/experiencedetails?recordId=" + recordId);
}

function goback() {
    $("#infodiv").hide();
    $("#listdiv").show();
}

function edit(recordId) {
    $("#listdiv").hide();
    $("#infodiv").show();
    $(".js-back-btn").unbind("click").click(function () {
        goback();
    })
    $.ajax({
        url: "/ret/eduget/getEduTeacherExperienceById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "teacherId") {
                        $.ajax({
                            url: "/ret/eduget/getEduTeacherById",
                            type: "post",
                            dataType: "json",
                            data: {
                                teacherId: recordInfo[id]
                            },
                            success: function (res) {
                                let info = res.list;
                                if (info) {
                                    $('#teacherId').select2({
                                        data: [{
                                            "id": info.teacherId,
                                            "text": info.userName
                                        }]
                                    }).val(recordInfo.teacherId).trigger('change');
                                }
                            }
                        });
                    } else if (id == "remark") {
                        $("#remark").code(recordInfo[id]);
                    } else {
                        $("#" + id).val(recordInfo[id]);
                    }
                }
                $(".js-update-save").unbind("click").click(function () {
                    updateEduTeacherExperience(recordId);
                })
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function deleteRecord(recordId) {
    if (confirm(sysmsg['OPT_CONFIRM_DELETE'])) {
        $.ajax({
            url: "/set/eduset/deleteEduTeacherExperience",
            type: "post",
            dataType: "json",
            data: {
                recordId: recordId
            },
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable("refresh");
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    }
}

function updateEduTeacherExperience(recordId) {
    $.ajax({
        url: "/set/eduset/updateEduTeacherExperience",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId,
            sortNo: $("#sortNo").val(),
            courseType: $("#courseType").val(),
            grade: $("#grade").val(),
            beginTime: $("#beginTime").val(),
            endTime: $("#endTime").val(),
            teachingSchool: $("#teachingSchool").val(),
            remark: $("#remark").code()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                $("#infodiv").hide();
                $("#listdiv").show();
                $("#myTable").bootstrapTable("refresh");
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}
