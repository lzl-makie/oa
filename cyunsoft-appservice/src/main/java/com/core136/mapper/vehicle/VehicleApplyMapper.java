package com.core136.mapper.vehicle;

import com.core136.bean.vehicle.VehicleApply;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface VehicleApplyMapper extends MyMapper<VehicleApply> {
    /**
     * @param orgId
     * @param opFlag
     * @param status
     * @param accountId
     * @param createUser
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getVehicleApplyList
     * @Description:  获取申请记录
     */
    public List<Map<String, String>> getVehicleApplyList(
            @Param(value = "orgId") String orgId,
            @Param(value = "opFlag") String opFlag,
            @Param(value = "status") String status,
            @Param(value = "accountId") String accountId,
            @Param(value = "createUser") String createUser,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime,
            @Param(value = "search") String search
    );


    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @param createUser
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getVehicleApprovedList
     * @Description:  获取待审批列表
     */
    public List<Map<String, String>> getVehicleApprovedList(
            @Param(value = "orgId") String orgId,
            @Param(value = "opFlag") String opFlag,
            @Param(value = "accountId") String accountId,
            @Param(value = "createUser") String createUser,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime,
            @Param(value = "search") String search
    );

    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @param createUser
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getVehicleReturnList
     * @Description:  获取待还车记录
     */
    public List<Map<String, String>> getVehicleReturnList(
            @Param(value = "orgId") String orgId,
            @Param(value = "opFlag") String opFlag,
            @Param(value = "accountId") String accountId,
            @Param(value = "createUser") String createUser,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime,
            @Param(value = "search") String search
    );

    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @param createUser
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getVehicleReturnOldList
     * @Description:  历史还车记录
     */
    public List<Map<String, String>> getVehicleReturnOldList(
            @Param(value = "orgId") String orgId,
            @Param(value = "opFlag") String opFlag,
            @Param(value = "accountId") String accountId,
            @Param(value = "createUser") String createUser,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime,
            @Param(value = "search") String search
    );

}
