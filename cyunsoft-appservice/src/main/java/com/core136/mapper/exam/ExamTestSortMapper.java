package com.core136.mapper.exam;

import com.core136.bean.exam.ExamTestSort;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ExamTestSortMapper extends MyMapper<ExamTestSort> {

    /**
     * @param orgId
     * @param levelId
     * @return List<Map < String, String>>
     * @Title: getExamTestSortTree
     * @Description:  获取试卷分类
     */
    public List<Map<String, String>> getExamTestSortTree(@Param(value = "orgId") String orgId, @Param(value = "levelId") String levelId);

    /**
     * @param @param  orgId
     * @param @param  sortId
     * @param @return 设定文件
     * @return int 返回类型
     * @Title: isExistChild
     * @Description:  判断是否还有子集
     */
    public int isExistChild(@Param(value = "orgId") String orgId, @Param(value = "sortId") String sortId);


}
