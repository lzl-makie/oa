package com.core136.mapper.exam;

import com.core136.bean.exam.ExamTest;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ExamTestMapper extends MyMapper<ExamTest> {
    /**
     * 获取试卷管理列表
     *
     * @param orgId
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getExamTestManageList(@Param(value = "orgId") String orgId, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);

    /**
     * 获取个人考试表表
     *
     * @param orgId
     * @param accountId
     * @param deptId
     * @param levelId
     * @param nowTime
     * @return
     */
    public List<Map<String, String>> getMyExamTestList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,
                                                       @Param(value = "deptId") String deptId, @Param(value = "levelId") String levelId, @Param(value = "nowTime") String nowTime);

    public List<Map<String, String>> getExamTestListForSelect(@Param(value = "orgId") String orgId);

}
