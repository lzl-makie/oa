package com.core136.mapper.oa;

import com.core136.bean.oa.BigStory;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface BigStoryMapper extends MyMapper<BigStory> {

    /**
     * @param orgId
     * @param type
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getBigStoryListForManage
     * @Description:  获取大纪事管理列表
     */
    public List<Map<String, String>> getBigStoryListForManage(@Param(value = "orgId") String orgId, @Param(value = "type") String type, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getBigStoryList
     * @Description:  获取所有大记事
     */
    public List<Map<String, String>> getBigStoryList(@Param(value = "orgId") String orgId);

}
