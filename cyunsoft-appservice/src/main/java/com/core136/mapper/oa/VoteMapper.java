package com.core136.mapper.oa;

import com.core136.bean.oa.Vote;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface VoteMapper extends MyMapper<Vote> {

    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @param voteType
     * @param beginTime
     * @param endTime
     * @param status
     * @param search
     * @return List<Map < String, String>>
     * @Title: getVoteListForManage
     * @Description:  获取投票管理列表
     */
    public List<Map<String, String>> getVoteListForManage(@Param(value = "orgId") String orgId, @Param(value = "opFlag") String opFlag,
                                                          @Param(value = "accountId") String accountId, @Param(value = "voteType") String voteType, @Param(value = "beginTime") String beginTime,
                                                          @Param(value = "endTime") String endTime, @Param(value = "status") String status, @Param(value = "search") String search);


    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @param voteType
     * @param beginTime
     * @param endTime
     * @param status
     * @param search
     * @return List<Map < String, String>>
     * @Title: getMyOldVoteListForVote
     * @Description:  获取历史投票列表
     */
    public List<Map<String, String>> getMyOldVoteListForVote(@Param(value = "orgId") String orgId, @Param(value = "opFlag") String opFlag,
                                                             @Param(value = "accountId") String accountId, @Param(value = "voteType") String voteType, @Param(value = "beginTime") String beginTime,
                                                             @Param(value = "endTime") String endTime, @Param(value = "status") String status, @Param(value = "search") String search);


    /**
     * @param orgId
     * @param nowTime
     * @param accountId
     * @param deptId
     * @param levelId
     * @return List<Map < String, String>>
     * @Title: getMyVoteListForVote
     * @Description:  获取待我投票列表
     */
    public List<Map<String, String>> getMyVoteListForVote(@Param(value = "orgId") String orgId, @Param(value = "nowTime") String nowTime,
                                                          @Param(value = "accountId") String accountId, @Param(value = "deptId") String deptId, @Param(value = "levelId") String levelId);
}
