package com.core136.mapper.task;

import com.core136.bean.task.TaskProcess;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface TaskProcessMapper extends MyMapper<TaskProcess> {
    /**
     * @param orgId
     * @param accountId
     * @param createUser
     * @param taskType
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getMyTaskProcessList
     * @Description:  获取子任务的处理过程列表
     */
    public List<Map<String, String>> getMyTaskProcessList(
            @Param(value = "orgId") String orgId,
            @Param(value = "accountId") String accountId,
            @Param(value = "createUser") String createUser,
            @Param(value = "taskType") String taskType,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime,
            @Param(value = "search") String search
    );

    /**
     * @param orgId
     * @param processId
     * @return Map<String, String>
     * @Title: getProcessInfo
     * @Description:  获取处理事件详情
     */
    public Map<String, String> getProcessInfo(@Param(value = "orgId") String orgId, @Param(value = "processId") String processId, String accountId);
}
