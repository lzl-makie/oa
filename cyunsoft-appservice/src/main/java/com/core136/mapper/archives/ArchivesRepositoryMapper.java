package com.core136.mapper.archives;

import com.core136.bean.archives.ArchivesRepository;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ArchivesRepositoryMapper extends MyMapper<ArchivesRepository> {

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getArchivesRepositoryList
     * @Description:  获取卷库列表
     */
    public List<Map<String, String>> getArchivesRepositoryList(@Param(value = "orgId") String orgId, @Param(value = "opFlag") String opFlag, @Param(value = "accountId") String accountId);

}
