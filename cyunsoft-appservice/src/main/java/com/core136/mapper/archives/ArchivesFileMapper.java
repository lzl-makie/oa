package com.core136.mapper.archives;

import com.core136.bean.archives.ArchivesFile;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ArchivesFileMapper extends MyMapper<ArchivesFile> {

    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getArchivesFileList
     * @Description:  获取文件管理列表
     */
    public List<Map<String, String>> getArchivesFileList(
            @Param(value = "orgId") String orgId,
            @Param(value = "opFlag") String opFlag,
            @Param(value = "accountId") String accountId,
            @Param(value = "volumeId") String volumeId,
            @Param(value = "fileType") String fileType,
            @Param(value = "secretLevel") String secretLevel,
            @Param(value = "search") String search);

    /**
     * @param orgId
     * @param repositoryId
     * @param accountId
     * @param volumeId
     * @param fileType
     * @param secretLevel
     * @param search
     * @return List<Map < String, String>>
     * @Title: getArchivesFileQueryList
     * @Description:  档案查询列表
     */
    public List<Map<String, String>> getArchivesFileQueryList(
            @Param(value = "orgId") String orgId,
            @Param(value = "repositoryId") String repositoryId,
            @Param(value = "accountId") String accountId,
            @Param(value = "volumeId") String volumeId,
            @Param(value = "fileType") String fileType,
            @Param(value = "secretLevel") String secretLevel,
            @Param(value = "search") String search);

    /**
     * @param orgId
     * @param volumeId
     * @return List<Map < String, String>>
     * @Title: getBorrowArchivesFileList
     * @Description:  获取借阅的文件列表
     */
    public List<Map<String, String>> getBorrowArchivesFileList(@Param(value = "orgId") String orgId, @Param(value = "volumeId") String volumeId);
}
