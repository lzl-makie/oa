/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: FixedAssetsConfigMapper.java
 * @Package com.core136.mapper.fixedassets
 * @Description: 描述
 * @author: lsq
 * @date: 2019年12月10日 上午9:02:43
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.mapper.fixedassets;

import com.core136.bean.fixedassets.FixedAssetsConfig;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author lsq
 *
 */
@Mapper
public interface FixedAssetsConfigMapper extends MyMapper<FixedAssetsConfig> {

}
