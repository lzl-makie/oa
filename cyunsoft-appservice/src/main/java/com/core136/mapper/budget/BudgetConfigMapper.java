package com.core136.mapper.budget;

import com.core136.bean.budget.BudgetConfig;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BudgetConfigMapper extends MyMapper<BudgetConfig> {

}
