package com.core136.mapper.budget;

import com.core136.bean.budget.BudgetCost;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface BudgetCostMapper extends MyMapper<BudgetCost> {

    /**
     * @param orgId
     * @param projectId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getChildBudgetCostList
     * @Description:  获取子项目费用支出列表
     */
    public List<Map<String, String>> getChildBudgetCostList(@Param(value = "orgId") String orgId, @Param(value = "projectId") String projectId,
                                                            @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);


    /**
     * @param orgId
     * @param parentId
     * @return List<Map < String, String>>
     * @Title: getBudgetCostList
     * @Description:  获取项目预算费用支出列表
     */
    public List<Map<String, String>> getBudgetCostList(@Param(value = "orgId") String orgId, @Param(value = "parentId") String parentId);
}
