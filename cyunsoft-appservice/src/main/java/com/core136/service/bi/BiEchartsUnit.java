package com.core136.service.bi;

import com.core136.bean.account.Account;
import com.core136.bean.bi.BiTemplate;
import com.core136.bean.bi.BiType;
import com.core136.bi.option.bean.OptionConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BiEchartsUnit {
    private LineEchartsService lineEchartsService;

    @Autowired
    public void setLineEchartsService(LineEchartsService lineEchartsService) {
        this.lineEchartsService = lineEchartsService;
    }

    private BiTypeService biTypeService;

    @Autowired
    public void setBiTypeService(BiTypeService biTypeService) {
        this.biTypeService = biTypeService;
    }

    /**
     * @param biTemplate
     * @return OptionConfig
     * @Title: getEchartsOptConfig
     * @Description:  按模版信息获取相关图表配置
     */
    public OptionConfig getEchartsOptConfig(Account account, BiTemplate biTemplate) {
        OptionConfig optionConfig = new OptionConfig();
        BiType biType = new BiType();
        //biType.setBiTypeId(biTemplate.getBiType());
        biType.setOrgId(account.getOrgId());
        biType = biTypeService.selectOneBiType(biType);
        String key = biType.getBiFlag();
        switch (key) {
            case "pie":
                break;
            case "line":
                optionConfig = lineEchartsService.getLineOptionConfig(biTemplate, biType);
                break;
            default:
                break;
        }
        return optionConfig;
    }


}
