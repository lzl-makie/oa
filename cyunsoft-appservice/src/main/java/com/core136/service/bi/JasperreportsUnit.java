package com.core136.service.bi;

import com.core136.bean.bi.BiTemplate;
import com.core136.bean.file.Attach;
import com.core136.bean.sys.SysDbSource;
import com.core136.bi.jasper.JasperTools;
import com.core136.service.file.AttachService;
import com.core136.service.sys.SysDbSourceService;
import com.core136.unit.dbunit.DBInfoTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

@Service
public class JasperreportsUnit {
    @Value("${app.attachpath}")
    private String attachpath;
    private DataSource dataSource;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private BiTemplateService biTemplateService;

    @Autowired
    public void setBiTemplateService(BiTemplateService biTemplateService) {
        this.biTemplateService = biTemplateService;
    }

    private AttachService attachService;

    @Autowired
    public void setAttachService(AttachService attachService) {
        this.attachService = attachService;
    }

    private SysDbSourceService sysDbSourceService;

    @Autowired
    public void setSysDbSourceService(SysDbSourceService sysDbSourceService) {
        this.sysDbSourceService = sysDbSourceService;
    }

    /**
     * @param biTemplate
     * @return
     * @throws Exception String
     * @Title: getRepHtml
     * @Description:  获取HTML格式报表在线查看
     */
    public String getRepHtml(BiTemplate biTemplate) throws Exception {
        Connection conn = null;
        biTemplate = biTemplateService.selectOne(biTemplate);
        Attach attach = new Attach();
        attach.setOrgId(biTemplate.getOrgId());
        attach.setAttachId(biTemplate.getJasTemplate());
        attach = attachService.selectOne(attach);
        String jasPath = attachpath+attach.getPath();
        JasperTools jasperTools = new JasperTools();
        Map<String, String> pathMap = new HashMap<String, String>();
        pathMap = jasperTools.getJasPaths(jasPath, attach.getOldName(), attach.getNewName());
        if (biTemplate.getDbSource().equals("sys")) {
            conn = dataSource.getConnection();
        } else {
            String dbSourceId = biTemplate.getDbSource();
            SysDbSource sysDbSource = new SysDbSource();
            sysDbSource.setDbSourceId(dbSourceId);
            sysDbSource.setOrgId(biTemplate.getOrgId());
            sysDbSource = sysDbSourceService.selectOne(sysDbSource);
            conn = DBInfoTools.getConnByDbSource(sysDbSource);
        }
        String jrxmlPath = pathMap.get("jrxmlPath");
        String jasperPath = pathMap.get("jasperPath");
        String destFileName = pathMap.get("htmlPath");
        Map<String, Object> params = new HashMap<String, Object>();
        return jasperTools.reportHtml(conn, params, jrxmlPath, jasperPath, destFileName);
    }

    /**
     * @param request
     * @param response
     * @param biTemplate
     * @throws Exception void
     * @Title: getRepForPdf
     * @Description:  获取PDF格式报表
     */
    public void getRepForPdf(HttpServletRequest request, HttpServletResponse response, BiTemplate biTemplate) throws Exception {
        Connection conn = null;
        biTemplate = biTemplateService.selectOne(biTemplate);
        Attach attach = new Attach();
        attach.setOrgId(biTemplate.getOrgId());
        attach.setAttachId(biTemplate.getJasTemplate());
        attach = attachService.selectOne(attach);
        String jasPath = attachpath+attach.getPath();
        JasperTools jasperTools = new JasperTools();
        Map<String, String> pathMap = new HashMap<String, String>();
        pathMap = jasperTools.getJasPaths(jasPath, attach.getOldName(), attach.getNewName());
        if (biTemplate.getDbSource().equals("sys")) {
            conn = dataSource.getConnection();
        } else {
            String dbSourceId = biTemplate.getDbSource();
            SysDbSource sysDbSource = new SysDbSource();
            sysDbSource.setDbSourceId(dbSourceId);
            sysDbSource.setOrgId(biTemplate.getOrgId());
            sysDbSource = sysDbSourceService.selectOne(sysDbSource);
            conn = DBInfoTools.getConnByDbSource(sysDbSource);
        }
        String jrxmlPath = pathMap.get("jrxmlPath");
        String jasperPath = pathMap.get("jasperPath");
        Map<String, Object> params = new HashMap<String, Object>();
        jasperTools.pdfExport(request, response, conn, params, jrxmlPath, jasperPath);
    }


}
