package com.core136.service.budget;

import com.core136.bean.budget.BudgetType;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.budget.BudgetTypeMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class BudgetTypeService {
    private BudgetTypeMapper budgetTypeMapper;

    @Autowired
    public void setBudgetTypeMapper(BudgetTypeMapper budgetTypeMapper) {
        this.budgetTypeMapper = budgetTypeMapper;
    }

    public int insertBudgetType(BudgetType budgetType) {
        return budgetTypeMapper.insert(budgetType);
    }

    public int deleteBudgetType(BudgetType budgetType) {
        return budgetTypeMapper.delete(budgetType);
    }

    public int updateBudgetType(Example example, BudgetType budgetType) {
        return budgetTypeMapper.updateByExampleSelective(budgetType, example);
    }

    public BudgetType selectOneBudgetType(BudgetType budgetType) {
        return budgetTypeMapper.selectOne(budgetType);
    }

    public int deleteBudgetType(Example example) {
        return budgetTypeMapper.deleteByExample(example);
    }

    public List<Map<String, String>> getBudgetTypeList(String orgId) {
        return budgetTypeMapper.getBudgetTypeList(orgId);
    }

    public List<Map<String, String>> getBudgetTypeListForSelect(String orgId) {
        return budgetTypeMapper.getBudgetTypeListForSelect(orgId);
    }


    /**
     * @param pageParam
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getBudgetTypeList
     * @Description:  获取预算分类列表
     */
    public PageInfo<Map<String, String>> getBudgetTypeList(PageParam pageParam) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getBudgetTypeList(pageParam.getOrgId());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
