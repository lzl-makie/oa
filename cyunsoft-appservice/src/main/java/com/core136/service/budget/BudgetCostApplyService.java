package com.core136.service.budget;


import com.core136.bean.budget.BudgetConfig;
import com.core136.bean.budget.BudgetCostApply;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.budget.BudgetCostApplyMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class BudgetCostApplyService {
    private BudgetCostApplyMapper budgetCostApplyMapper;

    @Autowired
    public void setBudgetCostApplyMapper(BudgetCostApplyMapper budgetCostApplyMapper) {
        this.budgetCostApplyMapper = budgetCostApplyMapper;
    }

    private BudgetConfigService budgetConfigService;

    @Autowired
    public void setBudgetConfigService(BudgetConfigService budgetConfigService) {
        this.budgetConfigService = budgetConfigService;
    }

    public int insertBudgetCostApply(BudgetCostApply budgetCostApply) {
        return budgetCostApplyMapper.insert(budgetCostApply);
    }


    public int deleteBudgetCostApply(BudgetCostApply budgetCostApply) {
        return budgetCostApplyMapper.delete(budgetCostApply);
    }

    public int updateBudgetCostApply(Example example, BudgetCostApply budgetCostApply) {
        return budgetCostApplyMapper.updateByExampleSelective(budgetCostApply, example);
    }

    public BudgetCostApply selectOneBudgetCostApply(BudgetCostApply budgetCostApply) {
        return budgetCostApplyMapper.selectOne(budgetCostApply);
    }

    /**
     * @param budgetCostApply
     * @return RetDataBean
     * @Title: addBudgetCostApply
     * @Description:  预算费用申请
     */
    public RetDataBean addBudgetCostApply(BudgetCostApply budgetCostApply) {
        BudgetConfig budgetConfig = new BudgetConfig();
        budgetConfig.setOrgId(budgetConfig.getOrgId());
        budgetConfig = budgetConfigService.selectOneBudgetConfig(budgetConfig);
        if (budgetConfig.getCostApplayType().equals("1")) {
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, insertBudgetCostApply(budgetCostApply));
        }
        return RetDataTools.NotOk(MessageCode.MSG_00016);

    }

    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @param projectId
     * @param beginTime
     * @param endTime
     * @param status
     * @param search
     * @return List<Map < String, String>>
     * @Title: getBudgetCostApplyList
     * @Description:  获取申请记录列表
     */
    public List<Map<String, String>> getBudgetCostApplyList(String orgId, String opFlag, String accountId, String projectId, String beginTime, String endTime, String status, String search) {
        return budgetCostApplyMapper.getBudgetCostApplyList(orgId, opFlag, accountId, projectId, beginTime, endTime, status, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param projectId
     * @param beginTime
     * @param endTime
     * @param status
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getBudgetCostApplyList
     * @Description:  获取申请记录列表
     */
    public PageInfo<Map<String, String>> getBudgetCostApplyList(PageParam pageParam, String projectId, String beginTime, String endTime, String status) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getBudgetCostApplyList(pageParam.getOrgId(), pageParam.getOpFlag(), pageParam.getAccountId(), projectId, beginTime, endTime, status, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param accountId
     * @param projectId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getBudgetCostApprpvalList
     * @Description:  获取预算费用审批记录
     */
    public List<Map<String, String>> getBudgetCostApprpvalList(String orgId, String accountId, String projectId, String beginTime, String endTime, String search) {
        return budgetCostApplyMapper.getBudgetCostApprpvalList(orgId, accountId, projectId, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param projectId
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getBudgetCostApprpvalList
     * @Description:  获取预算费用审批记录
     */
    public PageInfo<Map<String, String>> getBudgetCostApprpvalList(PageParam pageParam, String projectId, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getBudgetCostApprpvalList(pageParam.getOrgId(), pageParam.getAccountId(), projectId, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param accountId
     * @param projectId
     * @param beginTime
     * @param endTime
     * @param status
     * @param search
     * @return List<Map < String, String>>
     * @Title: getBudgetCostApprpvalOldList
     * @Description:  获取预算费用历史审批记录
     */
    public List<Map<String, String>> getBudgetCostApprpvalOldList(String orgId, String accountId, String projectId, String beginTime, String endTime, String status, String search) {
        return budgetCostApplyMapper.getBudgetCostApprpvalOldList(orgId, accountId, projectId, beginTime, endTime, status, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param projectId
     * @param beginTime
     * @param endTime
     * @param status
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getBudgetCostApprpvalOldList
     * @Description:  获取预算费用历史审批记录
     */
    public PageInfo<Map<String, String>> getBudgetCostApprpvalOldList(PageParam pageParam, String projectId, String beginTime, String endTime, String status) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getBudgetCostApprpvalOldList(pageParam.getOrgId(), pageParam.getAccountId(), projectId, beginTime, endTime, status, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param budgetCostApply
     * @return int
     * @Title: setCostApprovalStatus
     * @Description:  预算费用申请审批
     */
    public int setCostApprovalStatus(BudgetCostApply budgetCostApply) {

        budgetCostApply.setApprovalTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
        Example example = new Example(BudgetCostApply.class);
        example.createCriteria().andEqualTo("orgId", budgetCostApply.getOrgId()).andEqualTo("recordId", budgetCostApply.getRecordId());
        return updateBudgetCostApply(example, budgetCostApply);
    }

}
