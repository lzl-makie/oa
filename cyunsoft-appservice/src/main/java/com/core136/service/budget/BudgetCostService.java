package com.core136.service.budget;

import com.core136.bean.budget.BudgetConfig;
import com.core136.bean.budget.BudgetCost;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.budget.BudgetCostMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class BudgetCostService {
    private BudgetConfigService budgetConfigService;

    @Autowired
    public void setBudgetConfigService(BudgetConfigService budgetConfigService) {
        this.budgetConfigService = budgetConfigService;
    }

    private BudgetCostMapper budgetCostMapper;

    @Autowired
    public void setBudgetCostMapper(BudgetCostMapper budgetCostMapper) {
        this.budgetCostMapper = budgetCostMapper;
    }

    public int insertBudgetCost(BudgetCost budgetCost) {
        return budgetCostMapper.insert(budgetCost);
    }

    public int deleteBudgetCost(BudgetCost budgetCost) {
        return budgetCostMapper.delete(budgetCost);
    }

    public int updateBudgetCost(Example example, BudgetCost budgetCost) {
        return budgetCostMapper.updateByExampleSelective(budgetCost, example);
    }

    public BudgetCost selectOneBudgetCost(BudgetCost budgetCost) {
        return budgetCostMapper.selectOne(budgetCost);
    }

    /**
     * @param budgetCost
     * @return RetDataBean
     * @Title: addBudgetCost
     * @Description:  添加费用支出记录
     */
    public RetDataBean addBudgetCost(BudgetCost budgetCost) {
        BudgetConfig budgetConfig = new BudgetConfig();
        budgetConfig.setOrgId(budgetCost.getOrgId());
        budgetConfig = budgetConfigService.selectOneBudgetConfig(budgetConfig);
        if (budgetConfig.getCostUpdateType().equals("1")) {
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, insertBudgetCost(budgetCost));
        }
        return RetDataTools.NotOk(MessageCode.MSG_00016);
    }


    /**
     * @param orgId
     * @param projectId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getChildBudgetCostList
     * @Description:  获取子项目费用支出列表
     */
    public List<Map<String, String>> getChildBudgetCostList(String orgId, String projectId, String beginTime, String endTime, String search) {
        return budgetCostMapper.getChildBudgetCostList(orgId, projectId, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param projectId
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getChildBudgetCostList
     * @Description:  获取子项目费用支出列表
     */
    public PageInfo<Map<String, String>> getChildBudgetCostList(PageParam pageParam, String projectId, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getChildBudgetCostList(pageParam.getOrgId(), projectId, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param parentId
     * @return List<Map < String, String>>
     * @Title: getBudgetCostList
     * @Description:  获取项目预算费用支出列表
     */
    public List<Map<String, String>> getBudgetCostList(String orgId, String parentId) {
        return budgetCostMapper.getBudgetCostList(orgId, "%" + parentId + "%");
    }

    /**
     * @param pageParam
     * @param parentId
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getBudgetCostList
     * @Description:  获取项目预算费用支出列表
     */
    public PageInfo<Map<String, String>> getBudgetCostList(PageParam pageParam, String parentId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getBudgetCostList(pageParam.getOrgId(), parentId);
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }


}
