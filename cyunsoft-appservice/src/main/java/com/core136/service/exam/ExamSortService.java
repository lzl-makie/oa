package com.core136.service.exam;

import com.core136.bean.exam.ExamSort;
import com.core136.mapper.exam.ExamSortMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ExamSortService {
    private ExamSortMapper examSortMapper;

    @Autowired
    public void setExamSortMapper(ExamSortMapper examSortMapper) {
        this.examSortMapper = examSortMapper;
    }

    public int insertExamSort(ExamSort examSort) {
        return examSortMapper.insert(examSort);
    }

    public int deleteExamSort(ExamSort examSort) {
        return examSortMapper.delete(examSort);
    }

    public int updateExamSort(Example example, ExamSort examSort) {
        return examSortMapper.updateByExampleSelective(examSort, example);
    }

    public ExamSort selectOneExamSort(ExamSort examSort) {
        return examSortMapper.selectOne(examSort);
    }

    public List<Map<String, String>> getExamSortTree(String orgId, String levelId) {
        return examSortMapper.getExamSortTree(orgId, levelId);
    }

    public int isExistChild(String orgId, String sortId) {
        return examSortMapper.isExistChild(orgId, sortId);
    }

}
