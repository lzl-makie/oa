package com.core136.service.news;

import com.core136.bean.news.NewsComments;
import com.core136.mapper.news.NewsCommentsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class NewsCommentsService {
    private NewsCommentsMapper newsCommentsMapper;

    @Autowired
    public void setNewsCommentsMapper(NewsCommentsMapper newsCommentsMapper) {
        this.newsCommentsMapper = newsCommentsMapper;
    }

    public int insertNewsComments(NewsComments newsComments) {
        return newsCommentsMapper.insert(newsComments);
    }

    public int deleteNewsComments(NewsComments newsComments) {
        return newsCommentsMapper.delete(newsComments);
    }

    public int updateNewsComments(Example example, NewsComments newsComments) {
        return newsCommentsMapper.updateByExampleSelective(newsComments, example);
    }

    public NewsComments selectOneNewsComments(NewsComments newsComments) {
        return newsCommentsMapper.selectOne(newsComments);
    }

    /**
     * @Title: getCommentsList
     * @Description:  获取新闻下的表有评论
     * @param: orgId
     * @param: newsId
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getCommentsList(String orgId, String newsId) {
        return newsCommentsMapper.getCommentsList(orgId, newsId);
    }

}
