package com.core136.service.weixin;

import com.core136.bean.account.Account;
import com.core136.bean.account.Unit;
import com.core136.common.SysRunConfig;
import com.core136.common.enums.AppGobalConstant;
import com.core136.common.enums.EventTypeEnums;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.account.UnitService;
import com.core136.service.account.UserInfoService;
import com.core136.service.account.UserPrivService;
import com.core136.service.sys.SysLogService;
import com.core136.service.sys.SysMenuService;
import com.core136.unit.RedisUtil;
import com.taobao.api.ApiException;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class WeiXinLoginService {
    @Value("${app.office.officetype}")
    private String officetype;
    @Value("${app.ofd.ofdtype}")
    private String ofdtype;
    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private UserInfoService userInfoService;

    @Autowired
    public void setUserInfoService(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }

    private SysLogService sysLogService;

    @Autowired
    public void setSysLogService(SysLogService sysLogService) {
        this.sysLogService = sysLogService;
    }

    private UnitService unitService;

    @Autowired
    public void setUnitService(UnitService unitService) {
        this.unitService = unitService;
    }

    private SysMenuService sysMenuService;

    @Autowired
    public void setSysMenuService(SysMenuService sysMenuService) {
        this.sysMenuService = sysMenuService;
    }

    private UserPrivService userPrivService;

    @Autowired
    public void setUserPrivService(UserPrivService userPrivService) {
        this.userPrivService = userPrivService;
    }

    private RedisUtil redisUtil;

    @Autowired
    public void setRedisUtil(RedisUtil redisUtil) {
        this.redisUtil = redisUtil;
    }

    /**
     * @param request
     * @param wAccountId
     * @param orgId      ApiException
     *                   void
     * @Title: weiXinLogin
     * @Description:  微信客户端登陆
     */
    public void weiXinLogin(HttpServletRequest request, String wAccountId, String orgId) throws ApiException {
        boolean isRegist = SysRunConfig.getIsRegist();
        String ip = SysTools.getIpAddress(request);
        if (isRegist) {
            Account account = accountService.wxLogin(orgId, wAccountId);
            if (account != null) {
                try {
                    SysTools.reloadAuthorizing(account.getAccountId());
                    UsernamePasswordToken userToken = new UsernamePasswordToken(account.getAccountId(), "#wx#:" + account.getPassWord());
                    Subject subject = SecurityUtils.getSubject();
                    subject.login(userToken);
                    account = (Account) SecurityUtils.getSubject().getPrincipal();
                    Session session = subject.getSession();
                    Unit unit = new Unit();
                    unit.setOrgId(orgId);
                    unit = unitService.selectOne(unit);
                    session.setAttribute("UNIT", unit);
                    session.setAttribute("officetype", officetype);
                    session.setAttribute("ofdtype", ofdtype);
                    session.setAttribute("accountId", account.getAccountId());
                    if (unit.getOrgName().equals("")) {
                        session.setAttribute("SOFT_NAME", AppGobalConstant.SOFT_NAME);
                    } else {
                        session.setAttribute("SOFT_NAME", unit.getOrgName());
                    }
                    accountService.updateLastLoginTime(account);
                    sysLogService.createLog(ip, account, EventTypeEnums.SYS_LOG_LOGIN, "微信登陆成功");
                } catch (Exception e) {
                    sysLogService.createLog(ip, account, EventTypeEnums.SYS_LOG_LOGIN, "微信登陆失败");
                }
            } else {
                sysLogService.createLog(ip, account, EventTypeEnums.SYS_LOG_LOGIN, "微信登陆成功");
            }
        }
    }

}
