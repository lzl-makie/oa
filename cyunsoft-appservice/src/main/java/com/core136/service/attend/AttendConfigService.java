/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: AttendConfigService.java
 * @Package com.core136.service.attend
 * @Description: 描述
 * @author: lsq
 * @date: 2019年11月14日 下午3:57:17
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.service.attend;

import com.core136.bean.attend.AttendConfig;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.attend.AttendConfigMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 */
@Service
public class AttendConfigService {
    private AttendConfigMapper attendConfigMapper;

    @Autowired
    public void setAttendConfigMapper(AttendConfigMapper attendConfigMapper) {
        this.attendConfigMapper = attendConfigMapper;
    }

    public int insertAttendConfig(AttendConfig attendConfig) {
        return attendConfigMapper.insert(attendConfig);
    }

    public int deleteAttendConfig(AttendConfig attendConfig) {
        return attendConfigMapper.delete(attendConfig);
    }

    public int updateAttendConfig(AttendConfig attendConfig, Example example) {
        return attendConfigMapper.updateByExampleSelective(attendConfig, example);
    }

    public AttendConfig selectOneAttendConfig(AttendConfig attendConfig) {
        return attendConfigMapper.selectOne(attendConfig);
    }

    public List<AttendConfig> getAllConfig(AttendConfig attendConfig) {
        return attendConfigMapper.select(attendConfig);
    }

    /**
     * @Title: getConfigList
     * @Description:  获取所有的考勤记录
     * @param: orgId
     * @param: @return
     * @return: List<AttendConfig>
     */
    public List<AttendConfig> getConfigList(String orgId) {
        AttendConfig attendConfig = new AttendConfig();
        attendConfig.setOrgId(orgId);
        return attendConfigMapper.select(attendConfig);
    }


    public PageInfo<AttendConfig> getAttendConfigList(PageParam pageParam, AttendConfig attendConfig) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<AttendConfig> datalist = getAllConfig(attendConfig);
        PageInfo<AttendConfig> pageInfo = new PageInfo<AttendConfig>(datalist);
        return pageInfo;
    }

    /**
     * @Title: getAttendConfigList
     * @Description:  获取考勤列表
     * @param: orgId
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getAllAttendConfigList(String orgId) {
        return attendConfigMapper.getAllAttendConfigList(orgId);
    }

    /**
     * @Title: getAttendConfigList
     * @Description:  获取考勤配置列表
     * @param: orgId
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getMyAttendConfigList(String orgId, String configId) {
        String nowTime = SysTools.getTime("yyyy-MM-dd");
        return attendConfigMapper.getMyAttendConfigList(orgId, configId, nowTime);
    }


}
