package com.core136.service.notice;

import com.core136.bean.account.Account;
import com.core136.bean.account.UserInfo;
import com.core136.bean.notice.Notice;
import com.core136.bean.notice.NoticeConfig;
import com.core136.bean.sys.MsgBody;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.GobalConstant;
import com.core136.common.utils.SysTools;
import com.core136.mapper.notice.NoticeMapper;
import com.core136.service.account.AccountService;
import com.core136.service.sys.MessageUnitService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

@Service
public class NoticeService {
    private NoticeMapper noticeMapper;

    @Autowired
    public void setNoticeMapper(NoticeMapper noticeMapper) {
        this.noticeMapper = noticeMapper;
    }

    private NoticeConfigService noticeConfigService;

    @Autowired
    public void setNoticeConfigService(NoticeConfigService noticeConfigService) {
        this.noticeConfigService = noticeConfigService;
    }

    private MessageUnitService messageUnitService;

    @Autowired
    public void setMessageUnitService(MessageUnitService messageUnitService) {
        this.messageUnitService = messageUnitService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    public int insertNotice(Notice notice) {
        return noticeMapper.insert(notice);
    }

    public int deleteNotice(Notice notice) {
        return noticeMapper.delete(notice);
    }

    public int updateNotice(Notice notice, Example example) {
        return noticeMapper.updateByExampleSelective(notice, example);
    }

    public int reEditNotice(UserInfo userinfo, Notice notice, Example example) {
        NoticeConfig noticeConfig0 = new NoticeConfig();
        noticeConfig0.setOrgId(notice.getOrgId());
        noticeConfig0.setApproverType("0");
        noticeConfig0.setNoticeType(notice.getNoticeType());
        noticeConfig0 = noticeConfigService.selectOneNoticeConfig(noticeConfig0);

        NoticeConfig noticeConfig1 = new NoticeConfig();
        noticeConfig1.setOrgId(notice.getOrgId());
        noticeConfig1.setApproverType("1");
        noticeConfig1.setNoticeType(notice.getNoticeType());
        noticeConfig1 = noticeConfigService.selectOneNoticeConfig(noticeConfig1);

        String createUser = userinfo.getAccountId();
        if (StringUtils.isNotBlank(noticeConfig1.getApprover())) {
            if (("," + noticeConfig1.getApprover() + ",").indexOf("," + createUser + ",") < 0) {
                if (StringUtils.isNotBlank(noticeConfig0.getApprover())) {
                    notice.setApprovalStatus("0");
                    notice.setStatus("0");
                } else {
                    notice.setStatus("1");
                }
            } else {
                notice.setStatus("1");
            }
        } else {
            if (StringUtils.isNotBlank(noticeConfig0.getApprover())) {
                notice.setApprovalStatus("0");
                notice.setStatus("0");
            } else {
                notice.setStatus("1");
            }
        }

        return noticeMapper.updateByExampleSelective(notice, example);
    }

    public Notice selectOneNotice(Notice notice) {
        return noticeMapper.selectOne(notice);
    }

    /**
     * @param @param  orgId
     * @param @param  opFlag
     * @param @param  accountId
     * @param @param  search
     * @param @return 设定文件
     * @return List<Map < String, Object>> 返回类型
     * @Title: getNoticeManageList
     * @Description:  获取通知公告的维护列表
     */
    public List<Map<String, Object>> getNoticeManageList(String orgId, String opFlag,
                                                         String accountId, String search, String notictType, String beginTime, String endTime) {
        return noticeMapper.getNoticeManageList(orgId, opFlag, accountId, search, notictType, beginTime, endTime);
    }


    /**
     * @param @param  orgId
     * @param @param  accountId
     * @param @param  search
     * @param @return 设定文件
     * @return List<Map < String, Object>> 返回类型
     * @Title: getNoticeApproverList
     * @Description:  获取审批列表
     */
    public List<Map<String, Object>> getNoticeApproverList(String orgId, String accountId, String search) {
        return noticeMapper.getNoticeApproverList(orgId, accountId, "%" + search + "%");
    }

    /**
     * @throws Exception
     * @Title: getNoticeApproverList
     * @Description:  获取审批列表
     * @param: pageParam
     * @param: @return
     * @return: PageInfo<Map < String, Object>>
     */
    public PageInfo<Map<String, Object>> getNoticeApproverList(PageParam pageParam) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, Object>> datalist = getNoticeApproverList(pageParam.getOrgId(), pageParam.getAccountId(), pageParam.getSearch());
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<Map<String, Object>>(datalist);
        return pageInfo;
    }

    /**
     * @throws Exception
     * @Title: getNoticeManageList
     * @Description:  获取通知公告的维护列表
     * @param: pageParam
     * @param: noticeType
     * @param: beginTime
     * @param: endTime
     * @param: @return
     * @return: PageInfo<Map < String, Object>>
     */
    public PageInfo<Map<String, Object>> getNoticeManageList(PageParam pageParam, String noticeType, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, Object>> datalist = getNoticeManageList(pageParam.getOrgId(), pageParam.getOpFlag(), pageParam.getAccountId(),
                "%" + pageParam.getSearch() + "%", noticeType, beginTime, endTime);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<Map<String, Object>>(datalist);
        return pageInfo;
    }


    /**
     * @param @param  notice
     * @param @return 设定文件
     * @return int 返回类型
     * @Title: sendNotice
     * @Description:  按权限发布通知公告
     */
    public int sendNotice(Notice notice, UserInfo userInfo) {
        NoticeConfig noticeConfig0 = new NoticeConfig();
        noticeConfig0.setOrgId(notice.getOrgId());
        noticeConfig0.setApproverType("0");
        noticeConfig0.setNoticeType(notice.getNoticeType());
        noticeConfig0 = noticeConfigService.selectOneNoticeConfig(noticeConfig0);

        NoticeConfig noticeConfig1 = new NoticeConfig();
        noticeConfig1.setOrgId(notice.getOrgId());
        noticeConfig1.setApproverType("1");
        noticeConfig1.setNoticeType(notice.getNoticeType());
        noticeConfig1 = noticeConfigService.selectOneNoticeConfig(noticeConfig1);

        String createUser = notice.getCreateUser();
        if (StringUtils.isNotBlank(noticeConfig1.getApprover())) {
            if (("," + noticeConfig1.getApprover() + ",").indexOf("," + createUser + ",") < 0) {
                if (StringUtils.isNotBlank(noticeConfig0.getApprover())) {
                    notice.setApprovalStatus("0");
                    notice.setStatus("0");
                } else {
                    notice.setStatus("1");
                }
            } else {
                notice.setStatus("1");
            }
        } else {
            if (StringUtils.isNotBlank(noticeConfig0.getApprover())) {
                notice.setApprovalStatus("0");
                notice.setStatus("0");
            } else {
                notice.setStatus("1");
            }
        }
        int flag = insertNotice(notice);
        if (flag > 0) {
            if (notice.getStatus().equals("1")) {
                if (StringUtils.isNotBlank(notice.getMsgType())) {
                    List<MsgBody> msgBodyList = new ArrayList<MsgBody>();
                    List<Account> accountList = accountService.getAccountInPriv(notice.getOrgId(), notice.getUserPriv(), notice.getDeptPriv(), notice.getLevelPriv());
                    for (int i = 0; i < accountList.size(); i++) {
                        MsgBody msgBody = new MsgBody();
                        msgBody.setFromAccountId(notice.getCreateUser());
                        msgBody.setFormUserName(userInfo.getUserName());
                        msgBody.setTitle(notice.getNoticeTitle());
                        msgBody.setContent(notice.getSubheading());
                        msgBody.setSendTime(notice.getCreateTime());
                        msgBody.setAccount(accountList.get(i));
                        msgBody.setRedirectUrl("/app/core/notice/details?noticeId=" + notice.getNoticeId());
                        msgBody.setAttach(notice.getAttach());
                        msgBody.setOrgId(notice.getOrgId());
                        msgBodyList.add(msgBody);
                    }
                    messageUnitService.sendMessage(notice.getMsgType(), GobalConstant.MSG_TYPE_NOTICE, msgBodyList);
                }
            }
        }
        return flag;
    }

    /**
     * @param @param account 设定文件
     * @return void 返回类型
     * @Title: addReadUser
     * @Description:  添加查看人
     */
    public void addReadUser(Account account, Notice notice) {
        String readuser = notice.getReader();
        List<String> list = new ArrayList<String>();
        if (StringUtils.isNotBlank(readuser)) {
            String[] userArr;
            if (readuser.indexOf(",") > 0) {
                userArr = readuser.split(",");
            } else {
                userArr = new String[]{readuser};
            }
            for (int i = 0; i < userArr.length; i++) {
                if (StringUtils.isNotBlank(userArr[i])) {
                    list.add(userArr[i]);
                }
            }
        }
        list.add(account.getAccountId());
        HashSet<String> h = new HashSet<String>(list);
        list.clear();
        list.addAll(h);
        String join = StringUtils.join(list, ",");
        notice.setReader(join);
        Example example = new Example(Notice.class);
        example.createCriteria().andEqualTo("orgId", notice.getOrgId()).andEqualTo("noticeId", notice.getNoticeId());
        updateNotice(notice, example);
    }

    /**
     * @param @param  orgId
     * @param @param  noticeId
     * @param @return 设定文件
     * @return int 返回类型
     * @Title: setOnClickCount
     * @Description:  设置查看次数
     */
    public int setOnClickCount(String orgId, String noticeId) {
        return noticeMapper.setOnClickCount(orgId, noticeId);
    }

    /**
     * @param @param  account
     * @param @param  notice
     * @param @return 设定文件
     * @return Notice 返回类型
     * @Title: getNoticeInfo
     * @Description:  获取通知公告内容
     */
    @Transactional(value = "generalTM")
    public Notice getNoticeInfo(Account account, Notice notice) {
        notice = selectOneNotice(notice);
        addReadUser(account, notice);
        setOnClickCount(account.getOrgId(), notice.getNoticeId());
        notice.setOrgId("");
        return notice;
    }

    /**
     * @param @param  orgId
     * @param @param  noticeId
     * @param @param  endTime
     * @param @return 设定文件
     * @return int 返回类型
     * @Title: isNoSendStatus
     * @Description:  判断是否有效
     */
    public int isNoSendStatus(String orgId, String noticeId, String endTime) {
        return noticeMapper.isNoSendStatus(orgId, noticeId, endTime);
    }

    /**
     * @param @param  notice
     * @param @return 设定文件
     * @return int 返回类型
     * @Title: deleteNoticeStrong
     * @Description:  强力删除通知公告
     */
    public int deleteNoticeStrong(Notice notice) {
        return noticeMapper.delete(notice);
    }

    /**
     * @param @param  orgId
     * @param @param  noticeId
     * @param @return 设定文件
     * @return int 返回类型
     * @Title: deleteNotice
     * @Description:  删除工作日志
     */
    public int deleteNotice(String orgId, String noticeId) {
        String endTime = SysTools.getTime("yyyy-MM-dd");
        if (isNoSendStatus(orgId, noticeId, endTime) == 1) {
            Notice notice = new Notice();
            notice.setOrgId(orgId);
            notice.setNoticeId(noticeId);
            return noticeMapper.delete(notice);
        } else {
            return 0;
        }
    }

    /**
     * @param @param  orgId
     * @param @param  accountId
     * @param @param  deptId
     * @param @param  levelId
     * @param @param  noticeType
     * @param @param  beginTime
     * @param @param  endTime
     * @param @param  search
     * @param @return 设定文件
     * @return List<Map < String, String>> 返回类型
     * @Title: getMyNoticeList
     * @Description:  获取个人的通知公告
     */
    public List<Map<String, String>> getMyNoticeList(String orgId, String accountId, String deptId, String levelId, String noticeType, String beginTime,
                                                     String endTime, String readStatus, String search
    ) {
        return noticeMapper.getMyNoticeList(orgId, accountId, deptId, levelId, noticeType, beginTime, endTime, readStatus, "%" + search + "%");
    }

    /**
     * 获取个人的通知公告
     *
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getMyNoticeList(PageParam pageParam, String noticeType, String beginTime, String endTime, String readStatus) throws Exception {

        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyNoticeList(pageParam.getOrgId(), pageParam.getAccountId(), pageParam.getDeptId(), pageParam.getLevelId(), noticeType, beginTime, endTime, readStatus, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @Title: getMyNoticeListForDesk
     * @Description:  获取桌面的通知消息
     * @param: orgId
     * @param: endTime
     * @param: accountId
     * @param: deptId
     * @param: levelId
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getMyNoticeListForDesk(String orgId, String endTime, String accountId, String deptId, String levelId) {
        return noticeMapper.getMyNoticeListForDesk(orgId, endTime, accountId, deptId, levelId);
    }

    /**
     * @Title: getMobileMyNoticeList
     * @Description:  移动端下滑加载更多
     * @param: orgId
     * @param: accountId
     * @param: deptId
     * @param: levelId
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getMobileMyNoticeList(String orgId, String accountId, String deptId, String levelId, Integer page) {
        return noticeMapper.getMobileMyNoticeList(orgId, accountId, deptId, levelId, page);
    }

    /**
     * 通知公告人员查看状态
     *
     * @param orgId
     * @param newsId
     * @param list
     * @return
     */
    public List<Map<String, String>> getNoticeReadStatus(String orgId, String newsId, List<String> list) {
        return noticeMapper.getNoticeReadStatus(orgId, newsId, list);
    }


}
