package com.core136.service.archives;

import com.core136.bean.archives.ArchivesRepository;
import com.core136.mapper.archives.ArchivesRepositoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ArchivesRepositoryService {
    private ArchivesRepositoryMapper archivesRepositoryMapper;

    @Autowired
    public void setArchivesRepositoryMapper(ArchivesRepositoryMapper archivesRepositoryMapper) {
        this.archivesRepositoryMapper = archivesRepositoryMapper;
    }

    public int insertArchivesRepository(ArchivesRepository archivesRepository) {
        return archivesRepositoryMapper.insert(archivesRepository);
    }

    public int deleteArchivesRepository(ArchivesRepository archivesRepository) {
        return archivesRepositoryMapper.delete(archivesRepository);
    }

    public int updateArchivesRepository(Example example, ArchivesRepository archivesRepository) {
        return archivesRepositoryMapper.updateByExampleSelective(archivesRepository, example);
    }

    public ArchivesRepository selectOneArchivesRepository(ArchivesRepository archivesRepository) {
        return archivesRepositoryMapper.selectOne(archivesRepository);
    }

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getArchivesRepositoryList
     * @Description:  获取卷库列表
     */
    public List<Map<String, String>> getArchivesRepositoryList(String orgId, String opFlag, String accountId) {
        return archivesRepositoryMapper.getArchivesRepositoryList(orgId, opFlag, accountId);
    }
}
