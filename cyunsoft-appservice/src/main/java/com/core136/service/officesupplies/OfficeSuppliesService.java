/**
 * All rights Reserved, Designed By www.tydic.com
 *
 * @Title: OfficeSuppliesService.java
 * @Package com.core136.service.officesupplies
 * @Description: 描述
 * @author: lsq
 * @date: 2019年10月18日 上午10:51:16
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.service.officesupplies;

import com.core136.bean.account.Account;
import com.core136.bean.officesupplies.OfficeSupplies;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.ExcelUtil;
import com.core136.common.utils.SysTools;
import com.core136.mapper.officesupplies.OfficeSuppliesMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lsq
 */
@Service
public class OfficeSuppliesService {
    private OfficeSuppliesMapper officeSuppliesMapper;

    @Autowired
    public void setOfficeSuppliesMapper(OfficeSuppliesMapper officeSuppliesMapper) {
        this.officeSuppliesMapper = officeSuppliesMapper;
    }

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public int insertOfficeSupplies(OfficeSupplies officeSupplies) {
        return officeSuppliesMapper.insert(officeSupplies);
    }

    public int deleteOfficeSupplies(OfficeSupplies officeSupplies) {
        return officeSuppliesMapper.delete(officeSupplies);
    }

    public int updateOfficeSupplies(Example example, OfficeSupplies officeSupplies) {
        return officeSuppliesMapper.updateByExampleSelective(officeSupplies, example);
    }

    public OfficeSupplies selectOneOfficeSupplies(OfficeSupplies officeSupplies) {
        return officeSuppliesMapper.selectOne(officeSupplies);
    }

    /**
     * @Title: getOfficeSupplieslistBySortId
     * @Description:  获取办公用品列表
     * @param: orgId
     * @param: sortId
     * @param: search
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getOfficeSupplieslistBySortId(String orgId, String sortId, String search) {
        return officeSuppliesMapper.getOfficeSupplieslistBySortId(orgId, sortId, "%" + search + "%");
    }

    public PageInfo<Map<String, String>> getOfficeSupplieslistBySortId(PageParam pageParam, String sortId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getOfficeSupplieslistBySortId(pageParam.getOrgId(), sortId, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @Title: getApplyOfficeSupplieslist
     * @Description:  获取可以领用的办公用品列表
     * @param: orgId
     * @param: sortId
     * @param: search
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getApplyOfficeSupplieslist(String orgId, String sortId, String deptId, String search) {
        return officeSuppliesMapper.getApplyOfficeSupplieslist(orgId, sortId, deptId, "%" + search + "%");
    }

    public PageInfo<Map<String, String>> getApplyOfficeSupplieslist(PageParam pageParam, String sortId, String deptId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getApplyOfficeSupplieslist(pageParam.getOrgId(), sortId, deptId, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param account
     * @param file
     * @return
     * @throws IOException RetDataBean
     * @Title: importOfficeSuppliesInfo
     * @Description:  办公用品导入
     */
    @Transactional(value = "generalTM")
    public RetDataBean importOfficeSuppliesInfo(Account account, MultipartFile file) throws IOException {
        Map<String, String> fieldMap = new HashMap<String, String>();
        fieldMap.put("排序号", "sort_no");
        fieldMap.put("办公用品名称", "supplies_name");
        fieldMap.put("办公用品编号", "supplies_code");
        fieldMap.put("所属分类", "sort_id");
        fieldMap.put("产品品牌", "brand");
        fieldMap.put("规格型号", "model");
        fieldMap.put("计量单位", "unit");
        fieldMap.put("物品数量", "quantity");
        fieldMap.put("可领用部门", "own_dept");
        fieldMap.put("备注", "remark");

        List<String> fieldList = new ArrayList<String>();
        List<String> titleList = new ArrayList<String>();
        for (Map.Entry<String, String> entry : fieldMap.entrySet()) {
            fieldList.add(entry.getValue());
            titleList.add(entry.getKey());
        }
        String[] fieldArr = new String[fieldList.size()];
        fieldList.toArray(fieldArr);
        String fieldString = StringUtils.join(fieldArr, ",");
        List<Map<String, String>> recordList = ExcelUtil.readExcel(file);
        for (int i = 0; i < recordList.size(); i++) {
            Map<String, String> tempMap = recordList.get(i);
            String valueString = "'" + SysTools.getGUID() + "','0',";
            for (int k = 0; k < titleList.size(); k++) {
                if (titleList.get(k).equals("可领用部门")) {
                    if (StringUtils.isNotBlank(tempMap.get(titleList.get(k)))) {
                        String[] arr = tempMap.get(titleList.get(k)).split(",");
                        String inString = "('";
                        for (int s = 0; s < arr.length; s++) {
                            inString += StringUtils.join(arr, "','");
                        }
                        inString += "')";
                        String sql1 = "select dept_id from unit_dept where dept_name in  " + inString + " and org_id='" + account.getOrgId() + "'";
                        List<Map<String, Object>> deptList = jdbcTemplate.queryForList(sql1);
                        List<String> tempList = new ArrayList<String>();
                        for (int s = 0; s < deptList.size(); s++) {
                            tempList.add(deptList.get(s).get("dept_id").toString());
                        }
                        valueString += "'" + StringUtils.join(tempList.toArray(), ",") + "',";
                    } else {
                        valueString += "'',";
                    }
                } else if (titleList.get(k).equals("所属分类")) {
                    if (StringUtils.isNotBlank(tempMap.get(titleList.get(k)))) {
                        String sql2 = "select sort_id from office_supplies_sort where sort_name ='" + tempMap.get(titleList.get(k)) + "' and org_id='" + account.getOrgId() + "'";
                        String sortId = jdbcTemplate.queryForObject(sql2, String.class);
                        valueString += "'" + sortId + "',";
                    } else {
                        valueString += "'',";
                    }
                } else if (titleList.get(k).equals("计量单位")) {
                    if (StringUtils.isNotBlank(tempMap.get(titleList.get(k)))) {
                        String sql1 = "select unit_id from office_supplies_unit where (cn_name ='" + tempMap.get(titleList.get(k)) + "' or en_name = '" + tempMap.get(titleList.get(k)) + "') and org_id='" + account.getOrgId() + "'";
                        String userId = jdbcTemplate.queryForObject(sql1, String.class);
                        valueString += "'" + userId + "',";
                    } else {
                        valueString += "'',";
                    }
                } else {
                    valueString += "'" + tempMap.get(titleList.get(k)) + "',";
                }
            }
            valueString += "'" + SysTools.getTime("yyyy-MM-dd HH:mm:ss") + "',";
            valueString += "'" + account.getAccountId() + "',";
            valueString += "'" + account.getOrgId() + "'";
            String insertSql = "insert into office_supplies(supplies_id,status," + fieldString + ",create_time,create_user,org_id) values" + "(" + valueString + ")";
            jdbcTemplate.execute(insertSql);
        }
        return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
    }
}
