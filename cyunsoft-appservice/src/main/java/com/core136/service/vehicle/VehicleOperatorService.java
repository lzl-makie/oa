package com.core136.service.vehicle;

import com.core136.bean.account.Account;
import com.core136.bean.vehicle.VehicleOperator;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.vehicle.VehicleOperatorMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

@Service
public class VehicleOperatorService {
    private VehicleOperatorMapper vehicleOperatorMapper;

    @Autowired
    public void setVehicleOperatorMapper(VehicleOperatorMapper vehicleOperatorMapper) {
        this.vehicleOperatorMapper = vehicleOperatorMapper;
    }

    public int insertVehicleOperator(VehicleOperator vehicleOperator) {
        return vehicleOperatorMapper.insert(vehicleOperator);
    }

    public int deleteVehicleOperator(VehicleOperator vehicleOperator) {
        return vehicleOperatorMapper.delete(vehicleOperator);
    }

    public int updateVehicleOperator(Example example, VehicleOperator vehicleOperator) {
        return vehicleOperatorMapper.updateByExampleSelective(vehicleOperator, example);
    }

    public VehicleOperator selectOneVehicleOperator(VehicleOperator vehicleOperator) {
        return vehicleOperatorMapper.selectOne(vehicleOperator);
    }

    /**
     * @param account
     * @param optUser
     * @return RetDataBean
     * @Title: setVehicleOperator
     * @Description:  设置调度员
     */
    public RetDataBean setVehicleOperator(Account account, String optUser) {
        if (account.getOpFlag().equals("1")) {
            VehicleOperator vehicleOperator = new VehicleOperator();
            vehicleOperator.setOrgId(account.getOrgId());
            vehicleOperator = selectOneVehicleOperator(vehicleOperator);
            if (vehicleOperator == null) {
                VehicleOperator newVehicleOperator = new VehicleOperator();
                newVehicleOperator.setConfigId(SysTools.getGUID());
                newVehicleOperator.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
                newVehicleOperator.setCreateUser(account.getAccountId());
                newVehicleOperator.setOptUser(optUser);
                newVehicleOperator.setOrgId(account.getOrgId());
                return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, insertVehicleOperator(newVehicleOperator));
            } else {
                Example example = new Example(VehicleOperator.class);
                example.createCriteria().andEqualTo("configId", vehicleOperator.getConfigId()).andEqualTo("orgId", account.getOrgId());
                vehicleOperator.setOptUser(optUser);
                return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, updateVehicleOperator(example, vehicleOperator));
            }
        } else {
            return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
        }
    }

}
