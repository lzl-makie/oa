package com.core136.service.discuss;

import com.core136.bean.discuss.DiscussRecord;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.discuss.DiscussRecordMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class DiscussRecordService {
    private DiscussRecordMapper discussRecordMapper;

    @Autowired
    public void setDiscussRecordMapper(DiscussRecordMapper discussRecordMapper) {
        this.discussRecordMapper = discussRecordMapper;
    }

    public int insertDiscussRecord(DiscussRecord discussRecord) {
        return discussRecordMapper.insert(discussRecord);
    }

    public int deleteDiscussRecord(DiscussRecord discussRecord) {
        return discussRecordMapper.delete(discussRecord);
    }

    public int updateDiscussRecord(Example example, DiscussRecord discussRecord) {
        return discussRecordMapper.updateByExampleSelective(discussRecord, example);
    }

    public DiscussRecord selectOneDiscussRecord(DiscussRecord discussRecord) {
        return discussRecordMapper.selectOne(discussRecord);
    }

    public int getDiscussRecordCount(DiscussRecord discussRecord) {
        return discussRecordMapper.selectCount(discussRecord);
    }

    /**
     * 获取帖子与子帖
     *
     * @param orgId
     * @param recordId
     * @return
     */
    public List<Map<String, String>> getDiscussRecordListById(String orgId, String accountId, String recordId) {
        return discussRecordMapper.getDiscussRecordListById(orgId, accountId, recordId);
    }


    /**
     * @param orgId
     * @param discussId
     * @return Map<String, String>
     * @Title: getLastDiscussRecordTime
     * @Description:   获取最后发帖时间
     */
    public Map<String, String> getLastDiscussRecordTime(String orgId, String discussId) {
        return discussRecordMapper.getLastDiscussRecordTime(orgId, discussId);
    }

    /**
     * @param orgId
     * @param discussId
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getTopDiscussRecordList
     * @Description:  获取讨论主题列表
     */
    public List<Map<String, String>> getTopDiscussRecordList(String orgId, String discussId, String accountId, String search) {
        return discussRecordMapper.getTopDiscussRecordList(orgId, discussId, accountId, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param discussId
     * @return
     * @throws Exception PageInfo<Map<String,String>>
     * @Title: getTopDiscussRecordList
     * @Description:  获取讨论主题列表
     */
    public PageInfo<Map<String, String>> getTopDiscussRecordList(PageParam pageParam, String discussId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getTopDiscussRecordList(pageParam.getOrgId(), discussId, pageParam.getAccountId(), pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getTopDiscussRecordApprovalList
     * @Description:   获取待审批讨论区主题列表
     */
    public List<Map<String, String>> getTopDiscussRecordApprovalList(String orgId, String accountId, String search) {
        return discussRecordMapper.getTopDiscussRecordApprovalList(orgId, accountId, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @return
     * @throws Exception PageInfo<Map<String,String>>
     * @Title: getTopDiscussApprovalRecordList
     * @Description:  获取待审批讨论区主题列表
     */
    public PageInfo<Map<String, String>> getTopDiscussRecordApprovalList(PageParam pageParam) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getTopDiscussRecordApprovalList(pageParam.getOrgId(), pageParam.getAccountId(), pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
