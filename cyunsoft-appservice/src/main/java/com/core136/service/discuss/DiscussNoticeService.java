package com.core136.service.discuss;

import com.core136.bean.account.Account;
import com.core136.bean.discuss.DiscussNotice;
import com.core136.mapper.discuss.DiscussNoticeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class DiscussNoticeService {
    private DiscussNoticeMapper discussNoticeMapper;

    @Autowired
    public void setDiscussNoticeMapper(DiscussNoticeMapper discussNoticeMapper) {
        this.discussNoticeMapper = discussNoticeMapper;
    }

    public int insertDiscussNotice(DiscussNotice discussNotice) {
        return discussNoticeMapper.insert(discussNotice);
    }

    public int deleteDiscussNotice(DiscussNotice discussNotice) {
        return discussNoticeMapper.delete(discussNotice);
    }

    public int updateDiscussNotice(Example example, DiscussNotice discussNotice) {
        return discussNoticeMapper.updateByExampleSelective(discussNotice, example);
    }

    public DiscussNotice selectOneDiscussNotice(DiscussNotice discussNotice) {
        return discussNoticeMapper.selectOne(discussNotice);
    }

    public List<Map<String, String>> getDiscussNoticeList(Account account) {
        return discussNoticeMapper.getDiscussNoticeList(account.getOrgId(), account.getAccountId());
    }


}
