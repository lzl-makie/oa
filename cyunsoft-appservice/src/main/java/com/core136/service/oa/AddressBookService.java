package com.core136.service.oa;

import com.core136.bean.account.Account;
import com.core136.bean.oa.AddressBook;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.ExcelUtil;
import com.core136.common.utils.StrTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.oa.AddressBookMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AddressBookService {
    private AddressBookMapper addressBookMapper;

    @Autowired
    public void setAddressBookMapper(AddressBookMapper addressBookMapper) {
        this.addressBookMapper = addressBookMapper;
    }

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public int insertAddressBook(AddressBook addressBook) {
        return addressBookMapper.insert(addressBook);
    }

    public int deleteAddressBook(AddressBook addressBook) {
        return addressBookMapper.delete(addressBook);
    }

    public int updateAddressBook(Example example, AddressBook addressBook) {
        return addressBookMapper.updateByExampleSelective(addressBook, example);
    }

    public AddressBook selectOneAddressBook(AddressBook addressBook) {
        return addressBookMapper.selectOne(addressBook);
    }

    /**
     * @param addressBook
     * @return List<AddressBook>
     * @Title: getAddressBook
     * @Description:  获取通讯录列表
     */
    public List<AddressBook> getAddressBook(AddressBook addressBook) {
        return addressBookMapper.select(addressBook);
    }

    /**
     * @param orgId
     * @param accountId
     * @param bookType
     * @return List<AddressBook>
     * @Title: getMyShareAddressBookList
     * @Description:  获取他人共享的人员列表
     */
    public List<AddressBook> getMyShareAddressBookList(String orgId, String accountId, String bookType) {
        return addressBookMapper.getMyShareAddressBookList(orgId, accountId, bookType);
    }

    /**
     * @param orgId
     * @param accountId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getQueryAddressBookList
     * @Description:  查询通讯录人员
     */
    public List<Map<String, String>> getQueryAddressBookList(String orgId, String accountId, String search) {
        return addressBookMapper.getQueryAddressBookList(orgId, accountId, "%" + search + "%");
    }

    /**
     * @param orgId
     * @param levelId
     * @return List<AddressBook>
     * @Title: getMyFriendsByLevel
     * @Description:   获取同事列表
     */
    public List<Map<String, String>> getMyFriendsByLevel(String orgId, String levelId) {
        return addressBookMapper.getMyFriendsByLevel(orgId, levelId);
    }

    /**
     * @param orgId
     * @param accountId
     * @return Map<String, String>
     * @Title: getMyFriendsInfo
     * @Description:  获取同事联系方式
     */
    public Map<String, String> getMyFriendsInfo(String orgId, String accountId) {
        return addressBookMapper.getMyFriendsInfo(orgId, accountId);
    }


    @Transactional(value = "generalTM")
    public RetDataBean importAddressBookForExcel(Account account, MultipartFile file) throws IOException {
        Map<String, String> fieldMap = new HashMap<String, String>();
        fieldMap.put("排序号", "sort_no");
        fieldMap.put("姓名", "user_name");
        fieldMap.put("性别", "user_sex");
        fieldMap.put("部门", "dept_name");
        fieldMap.put("职务", "user_post");
        fieldMap.put("电话", "tel");
        fieldMap.put("手机号", "mobile_no");
        fieldMap.put("邮件", "email");
        fieldMap.put("地址", "address");
        fieldMap.put("类型", "book_type");
        fieldMap.put("是否共享", "is_share");
        List<String> fieldList = new ArrayList<String>();
        List<String> titleList = new ArrayList<String>();
        for (Map.Entry<String, String> entry : fieldMap.entrySet()) {
            fieldList.add(entry.getValue());
            titleList.add(entry.getKey());
        }
        String[] fieldArr = new String[fieldList.size()];
        fieldList.toArray(fieldArr);
        String fieldString = StringUtils.join(fieldArr, ",");
        List<Map<String, String>> recordList = ExcelUtil.readExcel(file);
        for (int i = 0; i < recordList.size(); i++) {
            Map<String, String> tempMap = recordList.get(i);
            String valueString = "'" + SysTools.getGUID() + "',";
            String py = "";
            for (int k = 0; k < titleList.size(); k++) {

                if (titleList.get(k).equals("user_name")) {
                    py = StrTools.getPinYin(tempMap.get(titleList.get(k)));
                }
                valueString += "'" + tempMap.get(titleList.get(k)) + "',";
            }
            valueString += "'" + py + "'";
            valueString += "'" + py.substring(0, 1).toUpperCase() + "'";
            valueString += "'" + SysTools.getTime("yyyy-MM-dd HH:mm:ss") + "',";
            valueString += "'" + account.getAccountId() + "',";
            valueString += "'" + account.getOrgId() + "'";
            String insertSql = "insert into address_book(record_id," + fieldString + ",pin_yin,first_pin_yin,create_time,create_user,org_id) values" + "(" + valueString + ")";
            jdbcTemplate.execute(insertSql);
        }
        return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
    }

}
