package com.core136.service.oa;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.core136.bean.oa.VoteItem;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.oa.VoteItemMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class VoteItemService {
    private VoteItemMapper voteItemMapper;

    @Autowired
    public void setVoteItemMapper(VoteItemMapper voteItemMapper) {
        this.voteItemMapper = voteItemMapper;
    }

    public int insertVoteItem(VoteItem voteItem) {
        return voteItemMapper.insert(voteItem);
    }

    public int deleteVoteItem(VoteItem voteItem) {
        return voteItemMapper.delete(voteItem);
    }

    public int updateVoteItem(Example example, VoteItem voteItem) {
        return voteItemMapper.updateByExampleSelective(voteItem, example);
    }

    public VoteItem selectOneVoteItem(VoteItem voteItem) {
        return voteItemMapper.selectOne(voteItem);
    }


    /**
     * @param orgId
     * @param nowTime
     * @param accountId
     * @param deptId
     * @param levelId
     * @param voteId
     * @return List<Map < String, String>>
     * @Title: getVoteItemsList
     * @Description:  获取投票项
     */
    public List<Map<String, String>> getVoteItemsList(String orgId, String nowTime, String accountId, String deptId, String levelId, String voteId) {
        return voteItemMapper.getVoteItemsList(orgId, nowTime, accountId, deptId, levelId, voteId);
    }

    /**
     * @param orgId
     * @param voteId
     * @param recordId
     * @return List<Map < String, String>>
     * @Title: getVoteChildItemsList
     * @Description:  获取投票明细项列表
     */
    public List<Map<String, String>> getVoteChildItemsList(String orgId, String voteId, String recordId) {
        return voteItemMapper.getVoteChildItemsList(orgId, voteId, recordId);
    }

    /**
     * @param voteItem
     * @return int
     * @Title: deleteVoteItems
     * @Description:  删除投票项
     */
    public int deleteVoteItems(VoteItem voteItem) {
        return voteItemMapper.deleteVoteItems(voteItem.getOrgId(), voteItem.getVoteId(), voteItem.getRecordId());
    }


    /**
     * @param voteItem
     * @return RetDataBean
     * @Title: updateVoteItems
     * @Description:  更新投票项
     */
    @Transactional(value = "generalTM")
    public RetDataBean updateVoteItems(VoteItem voteItem) {
        JSONArray newArr = new JSONArray();
        if (StringUtils.isNotBlank(voteItem.getChildItem())) {
            JSONArray jsonArray = JSONArray.parseArray(voteItem.getChildItem());
            List<String> recordIdList = new ArrayList<String>();
            for (int i = 0; i < jsonArray.size(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                recordIdList.add(jsonObject.getString("recordId"));
            }
            Example example = new Example(VoteItem.class);
            example.createCriteria().andEqualTo("orgId", voteItem.getOrgId()).andEqualTo("voteId", voteItem.getVoteId()).andEqualTo("levelId", voteItem.getRecordId()).andNotIn("recordId", recordIdList);
            voteItemMapper.deleteByExample(example);
            for (int i = 0; i < jsonArray.size(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String childRecordId = "";
                try {
                    childRecordId = jsonObject.getString("recordId");
                } catch (Exception e) {
                }
                if (StringUtils.isNotBlank(childRecordId)) {
                    VoteItem childVoteItem = new VoteItem();
                    childVoteItem.setRecordId(childRecordId);
                    childVoteItem.setVoteId(voteItem.getVoteId());
                    childVoteItem.setLevelId(voteItem.getRecordId());
                    childVoteItem.setSortNo(jsonObject.getInteger("sortNo"));
                    childVoteItem.setTitle(jsonObject.getString("childTitle"));
                    childVoteItem.setRemark(jsonObject.getString("childRemark"));
                    childVoteItem.setCreateTime(voteItem.getCreateTime());
                    childVoteItem.setCreateUser(voteItem.getCreateUser());
                    childVoteItem.setOrgId(voteItem.getOrgId());
                    newArr.add(jsonObject);
                    Example childExample = new Example(VoteItem.class);
                    childExample.createCriteria().andEqualTo("orgId", voteItem.getOrgId()).andEqualTo("voteId", voteItem.getVoteId()).andEqualTo("recordId", childRecordId);
                    updateVoteItem(childExample, childVoteItem);
                } else {
                    String recordId = SysTools.getGUID();
                    jsonObject.put("recordId", recordId);
                    VoteItem childVoteItem = new VoteItem();
                    childVoteItem.setRecordId(recordId);
                    childVoteItem.setVoteId(voteItem.getVoteId());
                    childVoteItem.setLevelId(voteItem.getRecordId());
                    childVoteItem.setSortNo(jsonObject.getInteger("sortNo"));
                    childVoteItem.setTitle(jsonObject.getString("childTitle"));
                    childVoteItem.setRemark(jsonObject.getString("childRemark"));
                    childVoteItem.setCreateTime(voteItem.getCreateTime());
                    childVoteItem.setCreateUser(voteItem.getCreateUser());
                    childVoteItem.setOrgId(voteItem.getOrgId());
                    newArr.add(jsonObject);
                    insertVoteItem(childVoteItem);
                }
            }
        }
        voteItem.setChildItem(newArr.toString());
        Example voteExample = new Example(VoteItem.class);
        voteExample.createCriteria().andEqualTo("orgId", voteItem.getOrgId()).andEqualTo("voteId", voteItem.getVoteId()).andEqualTo("recordId", voteItem.getRecordId());
        updateVoteItem(voteExample, voteItem);
        return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS);
    }


    /**
     * @param voteItem
     * @return RetDataBean
     * @Title: addVoteItem
     * @Description:  创建投票项
     */
    @Transactional(value = "generalTM")
    public RetDataBean addVoteItem(VoteItem voteItem) {
        voteItem.setLevelId("0");
        JSONArray newArr = new JSONArray();
        if (StringUtils.isNotBlank(voteItem.getChildItem())) {
            JSONArray jsonArray = JSONArray.parseArray(voteItem.getChildItem());
            for (int i = 0; i < jsonArray.size(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String recordId = SysTools.getGUID();
                jsonObject.put("recordId", recordId);
                VoteItem childVoteItem = new VoteItem();
                childVoteItem.setRecordId(recordId);
                childVoteItem.setVoteId(voteItem.getVoteId());
                childVoteItem.setLevelId(voteItem.getRecordId());
                childVoteItem.setSortNo(jsonObject.getInteger("sortNo"));
                childVoteItem.setTitle(jsonObject.getString("childTitle"));
                childVoteItem.setRemark(jsonObject.getString("childRemark"));
                childVoteItem.setCreateTime(voteItem.getCreateTime());
                childVoteItem.setCreateUser(voteItem.getCreateUser());
                childVoteItem.setOrgId(voteItem.getOrgId());
                newArr.add(jsonObject);
                insertVoteItem(childVoteItem);
            }
        }
        voteItem.setChildItem(newArr.toString());
        insertVoteItem(voteItem);
        return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS);
    }

    /**
     * @param orgId
     * @param voteId
     * @return List<Map < String, String>>
     * @Title: getVoteItemListForManage
     * @Description:  获取投票管理列表
     */
    public List<Map<String, String>> getVoteItemListForManage(String orgId, String voteId) {
        return voteItemMapper.getVoteItemListForManage(orgId, voteId);
    }

    /**
     * @param pageParam
     * @param voteId
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getVoteItemListForManage
     * @Description:  获取投票管理列表
     */
    public PageInfo<Map<String, String>> getVoteItemListForManage(PageParam pageParam, String voteId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getVoteItemListForManage(pageParam.getOrgId(), voteId);
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }
}
