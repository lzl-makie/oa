package com.core136.service.oa;

import com.core136.bean.oa.LeadActivity;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.oa.LeadActivityMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class LeadActivityService {
    private LeadActivityMapper leadActivityMapper;

    @Autowired
    public void setLeadActivityMapper(LeadActivityMapper leadActivityMapper) {
        this.leadActivityMapper = leadActivityMapper;
    }

    public int insertLeadActivity(LeadActivity leadActivity) {
        return leadActivityMapper.insert(leadActivity);
    }

    public int deleteLeadActivity(LeadActivity leadActivity) {
        return leadActivityMapper.delete(leadActivity);
    }

    public int updateLeadActivity(Example example, LeadActivity leadActivity) {
        return leadActivityMapper.updateByExampleSelective(leadActivity, example);
    }

    public LeadActivity selectOneLeadActivity(LeadActivity leadActivity) {
        return leadActivityMapper.selectOne(leadActivity);
    }

    /**
     * @param orgId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getLeadActivityLsit
     * @Description:  获取领导行程列表
     */
    public List<Map<String, String>> getLeadActivityLsit(String orgId, String beginTime, String endTime, String leader, String search) {
        return leadActivityMapper.getLeadActivityLsit(orgId, beginTime, endTime, leader, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getLeadActivityLsit
     * @Description:  获取领导行程列表
     */
    public PageInfo<Map<String, String>> getLeadActivityLsit(PageParam pageParam, String beginTime, String endTime, String leader) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getLeadActivityLsit(pageParam.getOrgId(), beginTime, endTime, leader, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    public List<Map<String, String>> getLeadActivityQueryLsit(String orgId, String beginTime, String endTime, String leader, String accountId, String deptId, String levelId, String search) {
        return leadActivityMapper.getLeadActivityQueryLsit(orgId, beginTime, endTime, leader, accountId, deptId, levelId, "%" + search + "%");
    }

    public PageInfo<Map<String, String>> getLeadActivityQueryLsit(PageParam pageParam, String beginTime, String endTime, String leader) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getLeadActivityQueryLsit(pageParam.getOrgId(), beginTime, endTime, leader, pageParam.getAccountId(), pageParam.getDeptId(), pageParam.getLevelId(), pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    public List<Map<String, String>> getLeadActivityLsitForDesk(String orgId, String accountId, String deptId, String levelId) {
        String nowTime = SysTools.getTime("yyyy-MM-dd HH:mm:ss");
        return leadActivityMapper.getLeadActivityLsitForDesk(orgId, nowTime, accountId, deptId, levelId);
    }

    public List<Map<String, String>> getMobileMyLeadActivity(String orgId, String accountId, String deptId, String levelId, Integer page) {
        String nowTime = SysTools.getTime("yyyy-MM-dd HH:mm:ss");
        return leadActivityMapper.getMobileMyLeadActivity(orgId, nowTime, accountId, deptId, levelId, page);
    }

}
