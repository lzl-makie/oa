package com.core136.service.oa;

import com.core136.bean.account.UserInfo;
import com.core136.bean.oa.LeaderMailbox;
import com.core136.bean.oa.LeaderMailboxConfig;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.oa.LeaderMailboxMapper;
import com.core136.service.account.UserInfoService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class LeaderMailboxService {
    private LeaderMailboxMapper leaderMailboxMapper;

    @Autowired
    public void setLeaderMailboxMapper(LeaderMailboxMapper leaderMailboxMapper) {
        this.leaderMailboxMapper = leaderMailboxMapper;
    }

    private LeaderMailboxConfigService leaderMailboxConfigService;

    @Autowired
    public void setLeaderMailboxConfigService(LeaderMailboxConfigService leaderMailboxConfigService) {
        this.leaderMailboxConfigService = leaderMailboxConfigService;
    }

    private UserInfoService userInfoService;

    @Autowired
    public void setUserInfoService(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }

    public int insertLeaderMailbox(LeaderMailbox leaderMailbox) {
        return leaderMailboxMapper.insert(leaderMailbox);
    }

    public int deleteLeaderMailbox(LeaderMailbox leaderMailbox) {
        return leaderMailboxMapper.delete(leaderMailbox);
    }

    public int updateLeaderMailbox(Example example, LeaderMailbox leaderMailbox) {
        return leaderMailboxMapper.updateByExampleSelective(leaderMailbox, example);
    }

    public LeaderMailbox selectOneLeaderMailbox(LeaderMailbox leaderMailbox) {
        return leaderMailboxMapper.selectOne(leaderMailbox);
    }

    public List<Map<String, String>> getMyLeaderMailBoxList(String orgId, String accountId, String search) {
        return leaderMailboxMapper.getMyLeaderMailBoxList(orgId, accountId, "%" + search + "%");
    }

    public PageInfo<Map<String, String>> getMyLeaderMailBoxList(PageParam pageParam) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyLeaderMailBoxList(pageParam.getOrgId(), pageParam.getAccountId(), pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    public List<Map<String, String>> getLeaderMailBoxList(String orgId, String search) {
        return leaderMailboxMapper.getLeaderMailBoxList(orgId, "%" + search + "%");
    }

    public PageInfo<Map<String, String>> getLeaderMailBoxList(PageParam pageParam) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getLeaderMailBoxList(pageParam.getOrgId(), pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    public RetDataBean setLeaderMailBox(UserInfo userInfo, LeaderMailbox leaderMailbox) {
        LeaderMailboxConfig leaderMailboxConfig = new LeaderMailboxConfig();
        leaderMailboxConfig.setOrgId(leaderMailbox.getOrgId());
        leaderMailboxConfig = leaderMailboxConfigService.selectOneLeaderMailboxConfig(leaderMailboxConfig);
        int i = userInfoService.isInUserPriv(userInfo.getOrgId(), userInfo.getAccountId(), leaderMailboxConfig.getUserPriv(), leaderMailboxConfig.getDeptPriv(), leaderMailboxConfig.getLevelPriv());
        if (i == 0) {
            return RetDataTools.NotOk(MessageCode.MESSAGE_FAILED);
        } else {
            if (!leaderMailboxConfig.getIsAnonymous().equals("1")) {
                leaderMailbox.setAccountId(userInfo.getAccountId());
            } else {
                leaderMailbox.setAccountId("");
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, insertLeaderMailbox(leaderMailbox));
        }
    }


}
