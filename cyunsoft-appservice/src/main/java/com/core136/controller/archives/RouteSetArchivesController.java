package com.core136.controller.archives;

import com.core136.bean.account.Account;
import com.core136.bean.archives.*;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.archives.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

@RestController
@RequestMapping("/set/archivesset")
public class RouteSetArchivesController {
    private ArchivesRepositoryService archivesRepositoryService;

    @Autowired
    public void setArchivesRepositoryService(ArchivesRepositoryService archivesRepositoryService) {
        this.archivesRepositoryService = archivesRepositoryService;
    }

    private ArchivesVolumeService archivesVolumeService;

    @Autowired
    public void setArchivesVolumeService(ArchivesVolumeService archivesVolumeService) {
        this.archivesVolumeService = archivesVolumeService;
    }

    private ArchivesFileService archivesFileService;

    @Autowired
    public void setArchivesFileService(ArchivesFileService archivesFileService) {
        this.archivesFileService = archivesFileService;
    }

    private ArchivesBorrowFileService archivesBorrowFileService;

    @Autowired
    public void setArchivesBorrowFileService(ArchivesBorrowFileService archivesBorrowFileService) {
        this.archivesBorrowFileService = archivesBorrowFileService;
    }

    private ArchivesBorrowVolumeService archivesBorrowVolumeService;

    @Autowired
    public void setArchivesBorrowVolumeService(ArchivesBorrowVolumeService archivesBorrowVolumeService) {
        this.archivesBorrowVolumeService = archivesBorrowVolumeService;
    }

    private ArchivesDestroyRecordService archivesDestroyRecordService;

    @Autowired
    public void setArchivesDestroyRecordService(ArchivesDestroyRecordService archivesDestroyRecordService) {
        this.archivesDestroyRecordService = archivesDestroyRecordService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * @param archivesDestroyRecord
     * @return RetDataBean
     * @Description:  档案销毁
     */
    @RequestMapping(value = "/destroyArchives", method = RequestMethod.POST)
    public RetDataBean destroyArchives(ArchivesDestroyRecord archivesDestroyRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            archivesDestroyRecord.setRecordId(SysTools.getGUID());
            archivesDestroyRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            archivesDestroyRecord.setCreateUser(account.getAccountId());
            archivesDestroyRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, archivesDestroyRecordService.destroyArchives(archivesDestroyRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param archivesBorrowVolume
     * @param status
     * @return RetDataBean
     * @Title: doApprovalvolume
     * @Description: 档案案卷借阅审批
     */
    @RequestMapping(value = "/doApprovalvolume", method = RequestMethod.POST)
    public RetDataBean doApprovalvolume(ArchivesBorrowVolume archivesBorrowVolume, String status) {
        try {
            if (StringUtils.isBlank(archivesBorrowVolume.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                Account account = accountService.getRedisAUserInfoToAccount();
                Example example = new Example(ArchivesBorrowVolume.class);
                archivesBorrowVolume.setApprovalStatus(status);
                archivesBorrowVolume.setApprovalTime("yyyy-MM-dd HH:mm:ss");
                archivesBorrowVolume.setApprovalUser(account.getAccountId());
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", archivesBorrowVolume.getRecordId());
                return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, archivesBorrowVolumeService.updateArchivesBorrowVolume(example, archivesBorrowVolume));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param archivesBorrowFile
     * @param status
     * @return RetDataBean
     * @Title: doApprovalfile
     * @Description: 档案文件借阅 审批
     */
    @RequestMapping(value = "/doApprovalfile", method = RequestMethod.POST)
    public RetDataBean doApprovalfile(ArchivesBorrowFile archivesBorrowFile, String status) {
        try {
            if (StringUtils.isBlank(archivesBorrowFile.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                Account account = accountService.getRedisAUserInfoToAccount();
                Example example = new Example(ArchivesBorrowFile.class);
                archivesBorrowFile.setApprovalStatus(status);
                archivesBorrowFile.setApprovalTime("yyyy-MM-dd HH:mm:ss");
                archivesBorrowFile.setApprovalUser(account.getAccountId());
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", archivesBorrowFile.getRecordId());
                return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, archivesBorrowFileService.updateArchivesBorrowFile(example, archivesBorrowFile));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param archivesBorrowVolume
     * @return RetDataBean
     * @Title: insertArchivesBorrowVolume
     * @Description:  添加借阅记录
     */
    @RequestMapping(value = "/insertArchivesBorrowVolume", method = RequestMethod.POST)
    public RetDataBean insertArchivesBorrowVolume(ArchivesBorrowVolume archivesBorrowVolume) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            archivesBorrowVolume.setRecordId(SysTools.getGUID());
            archivesBorrowVolume.setApprovalStatus("0");
            archivesBorrowVolume.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            archivesBorrowVolume.setCreateUser(account.getAccountId());
            archivesBorrowVolume.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, archivesBorrowVolumeService.insertArchivesBorrowVolume(archivesBorrowVolume));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param archivesBorrowVolume
     * @return RetDataBean
     * @Title: deleteArchivesBorrowVolume
     * @Description:  删除借阅记录
     */
    @RequestMapping(value = "/deleteArchivesBorrowVolume", method = RequestMethod.POST)
    public RetDataBean deleteArchivesBorrowVolume(ArchivesBorrowVolume archivesBorrowVolume) {
        try {
            if (StringUtils.isBlank(archivesBorrowVolume.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                Account account = accountService.getRedisAUserInfoToAccount();
                archivesBorrowVolume.setOrgId(account.getOrgId());
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, archivesBorrowVolumeService.deleteArchivesBorrowVolume(archivesBorrowVolume));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param archivesBorrowVolume
     * @return RetDataBean
     * @Title: updateArchivesBorrowVolume
     * @Description:  更新借阅记录
     */
    @RequestMapping(value = "/updateArchivesBorrowVolume", method = RequestMethod.POST)
    public RetDataBean updateArchivesBorrowVolume(ArchivesBorrowVolume archivesBorrowVolume) {
        try {
            if (StringUtils.isBlank(archivesBorrowVolume.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                Account account = accountService.getRedisAUserInfoToAccount();
                Example example = new Example(ArchivesBorrowVolume.class);
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", archivesBorrowVolume.getRecordId());
                return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, archivesBorrowVolumeService.updateArchivesBorrowVolume(example, archivesBorrowVolume));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param archivesBorrowFile
     * @return RetDataBean
     * @Title: insertArchivesBorrowFile
     * @Description:  添加借阅记录
     */
    @RequestMapping(value = "/insertArchivesBorrowFile", method = RequestMethod.POST)
    public RetDataBean insertArchivesBorrowFile(ArchivesBorrowFile archivesBorrowFile) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            archivesBorrowFile.setRecordId(SysTools.getGUID());
            archivesBorrowFile.setApprovalStatus("0");
            archivesBorrowFile.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            archivesBorrowFile.setCreateUser(account.getAccountId());
            archivesBorrowFile.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, archivesBorrowFileService.insertArchivesBorrowFile(archivesBorrowFile));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param archivesBorrowFile
     * @return RetDataBean
     * @Title: deleteArchivesBorrowFile
     * @Description:  删除借阅记录
     */
    @RequestMapping(value = "/deleteArchivesBorrowFile", method = RequestMethod.POST)
    public RetDataBean deleteArchivesBorrowFile(ArchivesBorrowFile archivesBorrowFile) {
        try {
            if (StringUtils.isBlank(archivesBorrowFile.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                Account account = accountService.getRedisAUserInfoToAccount();
                archivesBorrowFile.setOrgId(account.getOrgId());
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, archivesBorrowFileService.deleteArchivesBorrowFile(archivesBorrowFile));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param archivesBorrowFile
     * @return RetDataBean
     * @Title: updateArchivesBorrowFile
     * @Description:  更新借阅记录
     */
    @RequestMapping(value = "/updateArchivesBorrowFile", method = RequestMethod.POST)
    public RetDataBean updateArchivesBorrowFile(ArchivesBorrowFile archivesBorrowFile) {
        try {
            if (StringUtils.isBlank(archivesBorrowFile.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                Account account = accountService.getRedisAUserInfoToAccount();
                Example example = new Example(ArchivesBorrowFile.class);
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", archivesBorrowFile.getRecordId());
                return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, archivesBorrowFileService.updateArchivesBorrowFile(example, archivesBorrowFile));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param archivesFile
     * @return RetDataBean
     * @Title: insertArchivesFile
     * @Description:  您无权添加档案文件
     */
    @RequestMapping(value = "/insertArchivesFile", method = RequestMethod.POST)
    public RetDataBean insertArchivesFile(ArchivesFile archivesFile) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("archives:insert")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            archivesFile.setFileId(SysTools.getGUID());
            archivesFile.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            archivesFile.setCreateUser(account.getAccountId());
            archivesFile.setOrgId(account.getOrgId());
            archivesFile.setDestoryFlag("0");
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, archivesFileService.insertArchivesFile(archivesFile));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param archivesFile
     * @return RetDataBean
     * @Title: deleteArchivesFile
     * @Description:  删除档案文件
     */
    @RequestMapping(value = "/deleteArchivesFile", method = RequestMethod.POST)
    public RetDataBean deleteArchivesFile(ArchivesFile archivesFile) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("archives:delete")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
            }
            if (StringUtils.isBlank(archivesFile.getFileId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                Account account = accountService.getRedisAUserInfoToAccount();
                archivesFile.setOrgId(account.getOrgId());
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, archivesFileService.deleteArchivesFile(archivesFile));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param archivesFile
     * @return RetDataBean
     * @Title: updateArchivesFile
     * @Description:  更新档案文件
     */
    @RequestMapping(value = "/updateArchivesFile", method = RequestMethod.POST)
    public RetDataBean updateArchivesFile(ArchivesFile archivesFile) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("archives:update")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
            if (StringUtils.isBlank(archivesFile.getFileId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                Account account = accountService.getRedisAUserInfoToAccount();
                Example example = new Example(ArchivesFile.class);
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("fileId", archivesFile.getFileId());
                return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, archivesFileService.updateArchivesFile(example, archivesFile));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param archivesVolume
     * @return RetDataBean
     * @Title: insertArchivesVolume
     * @Description:  添加案卷
     */
    @RequestMapping(value = "/insertArchivesVolume", method = RequestMethod.POST)
    public RetDataBean insertArchivesVolume(ArchivesVolume archivesVolume) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            archivesVolume.setVolumeId(SysTools.getGUID());
            archivesVolume.setCreateTime("yyyy-MM-dd HH:mm:ss");
            archivesVolume.setCreateUser(account.getAccountId());
            archivesVolume.setOrgId(account.getOrgId());
            archivesVolume.setDestoryFlag("0");
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, archivesVolumeService.insertArchivesVolume(archivesVolume));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param archivesVolume
     * @return RetDataBean
     * @Title: deleteArchivesVolume
     * @Description:  删除案卷
     */
    @RequestMapping(value = "/deleteArchivesVolume", method = RequestMethod.POST)
    public RetDataBean deleteArchivesVolume(ArchivesVolume archivesVolume) {
        try {
            if (StringUtils.isBlank(archivesVolume.getVolumeId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                Account account = accountService.getRedisAUserInfoToAccount();
                archivesVolume.setOrgId(account.getOrgId());
                ArchivesFile archivesFile = new ArchivesFile();
                archivesFile.setOrgId(account.getOrgId());
                archivesFile.setVolumeId(archivesVolume.getVolumeId());
                if (archivesFileService.isExist(archivesFile) == 0) {
                    return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, archivesVolumeService.deleteArchivesVolume(archivesVolume));
                } else {
                    return RetDataTools.NotOk(MessageCode.MESSAGE_HAVE_CHILDREN_ITEM);
                }
            }
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param archivesVolume
     * @return RetDataBean
     * @Title: updateArchivesVolume
     * @Description:  更新案卷
     */
    @RequestMapping(value = "/updateArchivesVolume", method = RequestMethod.POST)
    public RetDataBean updateArchivesVolume(ArchivesVolume archivesVolume) {
        try {
            if (StringUtils.isBlank(archivesVolume.getVolumeId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                Account account = accountService.getRedisAUserInfoToAccount();
                Example example = new Example(ArchivesVolume.class);
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("volumeId", archivesVolume.getVolumeId());
                return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, archivesVolumeService.updateArchivesVolume(example, archivesVolume));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param archivesRepository
     * @return RetDataBean
     * @Title: insertArchivesRepository
     * @Description:  添加卷库
     */
    @RequestMapping(value = "/insertArchivesRepository", method = RequestMethod.POST)
    public RetDataBean insertArchivesRepository(ArchivesRepository archivesRepository) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            archivesRepository.setRepositoryId(SysTools.getGUID());
            archivesRepository.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            archivesRepository.setCreateUser(account.getAccountId());
            archivesRepository.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, archivesRepositoryService.insertArchivesRepository(archivesRepository));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param archivesRepository
     * @return RetDataBean
     * @Title: deleteArchivesRepository
     * @Description:  删除卷库
     */
    @RequestMapping(value = "/deleteArchivesRepository", method = RequestMethod.POST)
    public RetDataBean deleteArchivesRepository(ArchivesRepository archivesRepository) {
        try {
            if (StringUtils.isBlank(archivesRepository.getRepositoryId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                Account account = accountService.getRedisAUserInfoToAccount();
                archivesRepository.setOrgId(account.getOrgId());
                ArchivesVolume archivesVolume = new ArchivesVolume();
                archivesVolume.setOrgId(account.getOrgId());
                archivesVolume.setRepositoryId(archivesRepository.getRepositoryId());
                if (archivesVolumeService.isExist(archivesVolume) == 0) {
                    return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, archivesRepositoryService.deleteArchivesRepository(archivesRepository));
                } else {
                    return RetDataTools.NotOk(MessageCode.MESSAGE_HAVE_CHILDREN_ITEM);
                }
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param archivesRepository
     * @return RetDataBean
     * @Title: updateArchivesRepository
     * @Description:  更新卷库信息
     */
    @RequestMapping(value = "/updateArchivesRepository", method = RequestMethod.POST)
    public RetDataBean updateArchivesRepository(ArchivesRepository archivesRepository) {
        try {
            if (StringUtils.isBlank(archivesRepository.getRepositoryId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                Account account = accountService.getRedisAUserInfoToAccount();
                Example example = new Example(ArchivesRepository.class);
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("repositoryId", archivesRepository.getRepositoryId());
                return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, archivesRepositoryService.updateArchivesRepository(example, archivesRepository));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

}
