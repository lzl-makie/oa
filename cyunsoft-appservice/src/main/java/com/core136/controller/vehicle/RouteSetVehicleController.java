package com.core136.controller.vehicle;

import com.core136.bean.account.Account;
import com.core136.bean.vehicle.*;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.vehicle.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

@RestController
@RequestMapping("/set/vehicleset")
public class RouteSetVehicleController {
    private VehicleInfoService vehicleInfoService;

    @Autowired
    public void setVehicleInfoService(VehicleInfoService vehicleInfoService) {
        this.vehicleInfoService = vehicleInfoService;
    }

    private VehicleApplyService vehicleApplyService;

    @Autowired
    public void setVehicleApplyService(VehicleApplyService vehicleApplyService) {
        this.vehicleApplyService = vehicleApplyService;
    }

    private VehicleOperatorService vehicleOperatorService;

    @Autowired
    public void setVehicleOperatorService(VehicleOperatorService vehicleOperatorService) {
        this.vehicleOperatorService = vehicleOperatorService;
    }

    private VehicleOilCardService vehicleOilCardService;

    @Autowired
    public void setVehicleOilCardService(VehicleOilCardService vehicleOilCardService) {
        this.vehicleOilCardService = vehicleOilCardService;
    }

    private VehicleRepairRecordService vehicleRepairRecordService;

    @Autowired
    public void setVehicleRepairRecordService(VehicleRepairRecordService vehicleRepairRecordService) {
        this.vehicleRepairRecordService = vehicleRepairRecordService;
    }

    private VehicleDriverService vehicleDriverService;

    @Autowired
    public void setVehicleDriverService(VehicleDriverService vehicleDriverService) {
        this.vehicleDriverService = vehicleDriverService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * @param vehicleDriver
     * @return RetDataBean
     * @Title: insertVehicleDriver
     * @Description:  添加司机
     */
    @RequestMapping(value = "/insertVehicleDriver", method = RequestMethod.POST)
    public RetDataBean insertVehicleDriver(VehicleDriver vehicleDriver) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            vehicleDriver.setDriverId(SysTools.getGUID());
            vehicleDriver.setStatus("0");
            vehicleDriver.setCreateUser(account.getAccountId());
            vehicleDriver.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            vehicleDriver.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, vehicleDriverService.insertVehicleDriver(vehicleDriver));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param vehicleDriver
     * @return RetDataBean
     * @Title: deleteVehicleDriver
     * @Description:  删除司机记录
     */
    @RequestMapping(value = "/deleteVehicleDriver", method = RequestMethod.POST)
    public RetDataBean deleteVehicleDriver(VehicleDriver vehicleDriver) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(vehicleDriver.getDriverId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            vehicleDriver.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, vehicleDriverService.deleteVehicleDriver(vehicleDriver));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param vehicleDriver
     * @return RetDataBean
     * @Title: updateVehicleDriver
     * @Description:  更新司机记录
     */
    @RequestMapping(value = "/updateVehicleDriver", method = RequestMethod.POST)
    public RetDataBean updateVehicleDriver(VehicleDriver vehicleDriver) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(vehicleDriver.getDriverId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Example example = new Example(VehicleDriver.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("driverId", vehicleDriver.getDriverId());
            vehicleDriver.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, vehicleDriverService.updateVehicleDriver(example, vehicleDriver));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param vehicleApply
     * @return RetDataBean
     * @Title: setReturnVehicle
     * @Description:  车辆归还
     */
    @RequestMapping(value = "/setReturnVehicle", method = RequestMethod.POST)
    public RetDataBean setReturnVehicle(VehicleApply vehicleApply) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(vehicleApply.getApplyId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            vehicleApply.setReturnTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            vehicleApply.setOrgId(account.getOrgId());
            return vehicleApplyService.setReturnVehicle(vehicleApply);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param vehicleRepairRecord
     * @return RetDataBean
     * @Title: insertVehicleRepairRecord
     * @Description:  添加维修记录
     */
    @RequestMapping(value = "/insertVehicleRepairRecord", method = RequestMethod.POST)
    public RetDataBean insertVehicleRepairRecord(VehicleRepairRecord vehicleRepairRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            vehicleRepairRecord.setRecordId(SysTools.getGUID());
            vehicleRepairRecord.setCreateUser(account.getAccountId());
            vehicleRepairRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            vehicleRepairRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, vehicleRepairRecordService.insertVehicleRepairRecord(vehicleRepairRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param vehicleRepairRecord
     * @return RetDataBean
     * @Title: deleteVehicleRepairRecord
     * @Description:  删除维修记录
     */
    @RequestMapping(value = "/deleteVehicleRepairRecord", method = RequestMethod.POST)
    public RetDataBean deleteVehicleRepairRecord(VehicleRepairRecord vehicleRepairRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(vehicleRepairRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            vehicleRepairRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, vehicleRepairRecordService.deleteVehicleRepairRecord(vehicleRepairRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param vehicleRepairRecord
     * @return RetDataBean
     * @Title: updateVehicleRepairRecord
     * @Description:  更新维修记录
     */
    @RequestMapping(value = "/updateVehicleRepairRecord", method = RequestMethod.POST)
    public RetDataBean updateVehicleRepairRecord(VehicleRepairRecord vehicleRepairRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(vehicleRepairRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Example example = new Example(VehicleRepairRecord.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", vehicleRepairRecord.getRecordId());
            vehicleRepairRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, vehicleRepairRecordService.updateVehicleRepairRecord(example, vehicleRepairRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param vehicleOilCard
     * @return RetDataBean
     * @Title: insertVehicleOilCard
     * @Description:  添加油卡信息
     */
    @RequestMapping(value = "/insertVehicleOilCard", method = RequestMethod.POST)
    public RetDataBean insertVehicleOilCard(VehicleOilCard vehicleOilCard) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            vehicleOilCard.setCardId(SysTools.getGUID());
            vehicleOilCard.setCreateUser(account.getAccountId());
            vehicleOilCard.setStatus("0");
            vehicleOilCard.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            vehicleOilCard.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, vehicleOilCardService.insertVehicleOilCard(vehicleOilCard));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param vehicleOilCard
     * @return RetDataBean
     * @Title: deleteVehicleOilCard
     * @Description:  删除油卡信息
     */
    @RequestMapping(value = "/deleteVehicleOilCard", method = RequestMethod.POST)
    public RetDataBean deleteVehicleOilCard(VehicleOilCard vehicleOilCard) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(vehicleOilCard.getCardId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            vehicleOilCard.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, vehicleOilCardService.deleteVehicleOilCard(vehicleOilCard));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param vehicleOilCard
     * @return RetDataBean
     * @Title: updateVehicleOilCard
     * @Description:  更新油卡信息
     */
    @RequestMapping(value = "/updateVehicleOilCard", method = RequestMethod.POST)
    public RetDataBean updateVehicleOilCard(VehicleOilCard vehicleOilCard) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(vehicleOilCard.getCardId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Example example = new Example(VehicleOilCard.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("cardId", vehicleOilCard.getCardId());
            vehicleOilCard.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, vehicleOilCardService.updateVehicleOilCard(example, vehicleOilCard));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param optUser
     * @return RetDataBean
     * @Title: setVehicleOperator
     * @Description:  设置调度员
     */
    @RequestMapping(value = "/setVehicleOperator", method = RequestMethod.POST)
    public RetDataBean setVehicleOperator(String optUser) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return vehicleOperatorService.setVehicleOperator(account, optUser);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param vehicleApply
     * @return RetDataBean
     * @Title: insertVehicleApply
     * @Description:  车辆使用申请
     */
    @RequestMapping(value = "/insertVehicleApply", method = RequestMethod.POST)
    public RetDataBean insertVehicleInfo(VehicleApply vehicleApply) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            vehicleApply.setApplyId(SysTools.getGUID());
            vehicleApply.setCreateUser(account.getAccountId());
            vehicleApply.setStatus("0");
            vehicleApply.setReturnFlag("0");
            vehicleApply.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            vehicleApply.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, vehicleApplyService.insertVehicleApply(vehicleApply));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param vehicleApply
     * @return RetDataBean
     * @Title: deleteVehicleApply
     * @Description:  删除车辆使用申请
     */
    @RequestMapping(value = "/deleteVehicleApply", method = RequestMethod.POST)
    public RetDataBean deleteVehicleApply(VehicleApply vehicleApply) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(vehicleApply.getApplyId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            vehicleApply.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, vehicleApplyService.deleteVehicleApply(vehicleApply));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param vehicleApply
     * @return RetDataBean
     * @Title: updateVehicleApply
     * @Description:  更新车辆使用申请
     */
    @RequestMapping(value = "/updateVehicleApply", method = RequestMethod.POST)
    public RetDataBean updateVehicleApply(VehicleApply vehicleApply) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(vehicleApply.getApplyId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Example example = new Example(VehicleApply.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("applyId", vehicleApply.getApplyId());
            vehicleApply.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, vehicleApplyService.updateVehicleApply(example, vehicleApply));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param vehicleInfo
     * @return RetDataBean
     * @Title: insertVehicleInfo
     * @Description:  添加车辆信息
     */
    @RequestMapping(value = "/insertVehicleInfo", method = RequestMethod.POST)
    public RetDataBean insertVehicleInfo(VehicleInfo vehicleInfo) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("vehicle:insert")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            vehicleInfo.setVehicleId(SysTools.getGUID());
            vehicleInfo.setCreateUser(account.getAccountId());
            vehicleInfo.setStatus("0");
            vehicleInfo.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            vehicleInfo.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, vehicleInfoService.insertVehicleInfo(vehicleInfo));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param vehicleApply
     * @return RetDataBean
     * @Title: setApprovalStatus
     * @Description:  调度员审批
     */
    @RequestMapping(value = "/setApprovalStatus", method = RequestMethod.POST)
    public RetDataBean setApprovalStatus(VehicleApply vehicleApply) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(vehicleApply.getVehicleId()) || StringUtils.isBlank(vehicleApply.getApplyId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            vehicleApply.setOrgId(account.getOrgId());
            return vehicleApplyService.setApprovalStatus(vehicleApply);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param vehicleInfo
     * @return RetDataBean
     * @Title: deleteVehicleInfo
     * @Description:  删除车辆信息
     */
    @RequestMapping(value = "/deleteVehicleInfo", method = RequestMethod.POST)
    public RetDataBean deleteVehicleInfo(VehicleInfo vehicleInfo) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("vehicle:delete")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(vehicleInfo.getVehicleId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            vehicleInfo.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, vehicleInfoService.deleteVehicleInfo(vehicleInfo));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param vehicleInfo
     * @return RetDataBean
     * @Title: updateVehicleInfo
     * @Description:  更新车辆信息
     */
    @RequestMapping(value = "/updateVehicleInfo", method = RequestMethod.POST)
    public RetDataBean updateVehicleInfo(VehicleInfo vehicleInfo) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("vehicle:update")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(vehicleInfo.getVehicleId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Example example = new Example(VehicleInfo.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("vehicleId", vehicleInfo.getVehicleId());
            vehicleInfo.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, vehicleInfoService.updateVehicleInfo(example, vehicleInfo));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }
}
