package com.core136.controller.file;

import com.core136.bean.account.Account;
import com.core136.bean.account.UserInfo;
import com.core136.bean.file.FileMapItem;
import com.core136.bean.file.FileMapRecord;
import com.core136.bean.file.FileMapSort;
import com.core136.bean.sys.MsgBody;
import com.core136.common.enums.GobalConstant;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.file.FileMapItemService;
import com.core136.service.file.FileMapLogService;
import com.core136.service.file.FileMapRecordService;
import com.core136.service.file.FileMapSortService;
import com.core136.service.sys.MessageUnitService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/set/filemapset")
public class RouteSetFileMapController {
    private FileMapSortService fileMapSortService;

    @Autowired
    public void setFileMapSortService(FileMapSortService fileMapSortService) {
        this.fileMapSortService = fileMapSortService;
    }

    private FileMapLogService fileMapLogService;

    @Autowired
    public void setFileMapLogService(FileMapLogService fileMapLogService) {
        this.fileMapLogService = fileMapLogService;
    }

    private FileMapItemService fileMapItemService;

    @Autowired
    public void setFileMapItemService(FileMapItemService fileMapItemService) {
        this.fileMapItemService = fileMapItemService;
    }

    private FileMapRecordService fileMapRecordService;

    @Autowired
    public void setFileMapRecordService(FileMapRecordService fileMapRecordService) {
        this.fileMapRecordService = fileMapRecordService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private MessageUnitService messageUnitService;

    @Autowired
    public void setMessageUnitService(MessageUnitService messageUnitService) {
        this.messageUnitService = messageUnitService;
    }

    /**
     * 更新查阅状态
     *
     * @param fileMapRecord
     * @return
     */
    @RequestMapping(value = "/setReadStatus", method = RequestMethod.POST)
    public RetDataBean setReadStatus(FileMapRecord fileMapRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            fileMapRecord.setOrgId(account.getOrgId());
            fileMapRecord = fileMapRecordService.selectOneFileMapRecord(fileMapRecord);
            return fileMapRecordService.setReadStatus(fileMapRecord, account);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 添加文件记录
     *
     * @param fileMapRecord
     * @param msgType
     * @return
     */
    @RequestMapping(value = "/insertFileMapRecord", method = RequestMethod.POST)
    public RetDataBean insertFileMapRecord(FileMapRecord fileMapRecord, String msgType) {
        try {
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            String createTime = SysTools.getTime("yyyy-MM-dd HH:mm:ss");
            fileMapRecord.setRecordId(SysTools.getGUID());
            fileMapRecord.setFileStatus("0");
            fileMapRecord.setClickCount(0);
            fileMapRecord.setCreateUser(userInfo.getAccountId());
            fileMapRecord.setCreateTime(createTime);
            fileMapRecord.setOrgId(userInfo.getOrgId());
            if (StringUtils.isNotBlank(msgType)) {
                FileMapItem fileMapItem = new FileMapItem();
                fileMapItem.setOrgId(userInfo.getOrgId());
                fileMapItem.setItemId(fileMapRecord.getItemId());
                fileMapItem = fileMapItemService.selectOneFileMapItem(fileMapItem);
                List<MsgBody> msgBodyList = new ArrayList<MsgBody>();
                List<Account> accountList = fileMapItemService.getMessageAccountList(fileMapItem);
                for (int i = 0; i < accountList.size(); i++) {
                    MsgBody msgBody = new MsgBody();
                    msgBody.setFromAccountId(userInfo.getAccountId());
                    msgBody.setFormUserName(userInfo.getUserName());
                    msgBody.setTitle("有新文件发布,请注意查阅!");
                    msgBody.setContent("文件版本:" + fileMapRecord.getVersion());
                    msgBody.setSendTime(createTime);
                    msgBody.setAccount(accountList.get(i));
                    msgBody.setRedirectUrl("/app/core/previewonline?attachId=" + fileMapRecord.getAttachId());
                    msgBody.setOrgId(userInfo.getOrgId());
                    msgBodyList.add(msgBody);
                }
                messageUnitService.sendMessage(msgType, GobalConstant.MSG_TYPE_FILE_MAP, msgBodyList);
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, fileMapRecordService.insertFileMapRecord(fileMapRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除文件
     *
     * @param fileMapRecord
     * @return
     */
    @RequestMapping(value = "/deleteFileMapRecord", method = RequestMethod.POST)
    public RetDataBean deleteFileMapRecord(FileMapRecord fileMapRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(fileMapRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            fileMapRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, fileMapRecordService.deleteFileMapRecord(fileMapRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新文件记录
     *
     * @param fileMapRecord
     * @return
     */
    @RequestMapping(value = "/updateFileMapRecord", method = RequestMethod.POST)
    public RetDataBean updateFileMapRecord(FileMapRecord fileMapRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(fileMapRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Example example = new Example(FileMapRecord.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", fileMapRecord.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, fileMapRecordService.updateFileMapRecord(example, fileMapRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 创建分类
     *
     * @param fileMapItem
     * @return
     */
    @RequestMapping(value = "/insertFileMapItem", method = RequestMethod.POST)
    public RetDataBean insertFileMapItem(FileMapItem fileMapItem) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            fileMapItem.setItemId(SysTools.getGUID());
            fileMapItem.setCreateUser(account.getAccountId());
            fileMapItem.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            fileMapItem.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, fileMapItemService.insertFileMapItem(fileMapItem));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除目录
     *
     * @param fileMapItem
     * @return
     */
    @RequestMapping(value = "/deleteFileMapItem", method = RequestMethod.POST)
    public RetDataBean deleteFileMapItem(FileMapItem fileMapItem) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(fileMapItem.getItemId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            fileMapItem.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, fileMapItemService.deleteFileMapItem(fileMapItem));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新分类
     *
     * @param fileMapItem
     * @return
     */
    @RequestMapping(value = "/updateFileMapItem", method = RequestMethod.POST)
    public RetDataBean updateFileMapItem(FileMapItem fileMapItem) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(fileMapItem.getItemId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Example example = new Example(FileMapItem.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("itemId", fileMapItem.getItemId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, fileMapItemService.updateFileMapItem(example, fileMapItem));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 创建目录
     *
     * @param fileMapSort
     * @return
     */
    @RequestMapping(value = "/insertFileMapSort", method = RequestMethod.POST)
    public RetDataBean insertFileMapSort(FileMapSort fileMapSort) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(fileMapSort.getLevelId())) {
                fileMapSort.setLevelId("0");
            }
            fileMapSort.setSortId(SysTools.getGUID());
            fileMapSort.setCreateUser(account.getAccountId());
            fileMapSort.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            fileMapSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, fileMapSortService.insertFileMapSort(fileMapSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除目录
     *
     * @param fileMapSort
     * @return
     */
    @RequestMapping(value = "/deleteFileMapSort", method = RequestMethod.POST)
    public RetDataBean deleteFileMapSort(FileMapSort fileMapSort) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(fileMapSort.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            fileMapSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, fileMapSortService.deleteFileMapSort(fileMapSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新目录
     *
     * @param fileMapSort
     * @return
     */
    @RequestMapping(value = "/updateFileMapSort", method = RequestMethod.POST)
    public RetDataBean updateFileMapSort(FileMapSort fileMapSort) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(fileMapSort.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            if(StringUtils.isBlank(fileMapSort.getLevelId()))
            {
                fileMapSort.setLevelId("0");
            }
            Example example = new Example(FileMapSort.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("sortId", fileMapSort.getSortId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, fileMapSortService.updateFileMapSort(example, fileMapSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

}
