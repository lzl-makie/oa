package com.core136.controller.file;

import com.core136.bean.account.Account;
import com.core136.bean.account.UserInfo;
import com.core136.bean.file.*;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.service.account.AccountService;
import com.core136.service.file.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/ret/knowledgeget")
public class RouteGetKnowledgeController {
    private KnowledgeSortService knowledgeSortService;

    @Autowired
    public void setKnowledgeSortService(KnowledgeSortService knowledgeSortService) {
        this.knowledgeSortService = knowledgeSortService;
    }

    private KnowledgeService knowledgeService;

    @Autowired
    public void setKnowledgeService(KnowledgeService knowledgeService) {
        this.knowledgeService = knowledgeService;
    }

    private KnowledgeLearnService knowledgeLearnService;

    @Autowired
    public void setKnowledgeLearnService(KnowledgeLearnService knowledgeLearnService) {
        this.knowledgeLearnService = knowledgeLearnService;
    }

    private KnowledgeSearchService knowledgeSearchService;

    @Autowired
    public void setKnowledgeSearchService(KnowledgeSearchService knowledgeSearchService) {
        this.knowledgeSearchService = knowledgeSearchService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private KnowledgeCommentService knowledgeCommentService;

    @Autowired
    public void setKnowledgeCommentService(KnowledgeCommentService knowledgeCommentService) {
        this.knowledgeCommentService = knowledgeCommentService;
    }


    @RequestMapping(value = "/getKnowledgeSearchList", method = RequestMethod.POST)
    public RetDataBean getKnowledgeSearchList(
            PageParam pageParam,
            String createUser
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("k.count");
            } else {
                pageParam.setSort("k." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(createUser);
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = knowledgeSearchService.getKnowledgeSearchList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getKnowledgeLearnList
     * @Description:  获取学习记录
     */
    @RequestMapping(value = "/getKnowledgeLearnList", method = RequestMethod.POST)
    public RetDataBean getKnowledgeLearnList(
            PageParam pageParam,
            String beginTime,
            String endTime,
            String createUser
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("l.create_time");
            } else {
                pageParam.setSort("l." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(createUser);
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = knowledgeLearnService.getKnowledgeLearnList(pageParam, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getAllKnowledgeInfo
     * @Description:  获取知识管理概要信息
     */
    @RequestMapping(value = "/getAllKnowledgeInfo", method = RequestMethod.POST)
    public RetDataBean getAllKnowledgeInfo() {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("knowledges:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, knowledgeService.getAllKnowledgeInfo(account));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param knowledgeComment
     * @return RetDataBean
     * @Title: getCommentsList
     * @Description:  获取评论列表
     */
    @RequestMapping(value = "/getCommentsList", method = RequestMethod.POST)
    public RetDataBean getCommentsList(KnowledgeComment knowledgeComment) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, knowledgeCommentService.getCommentsList(account.getOrgId(), knowledgeComment.getKnowledgeId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param knowledgeComment
     * @return RetDataBean
     * @Title: getKnowledgeCommentById
     * @Description:  获取评论详情
     */
    @RequestMapping(value = "/getKnowledgeCommentById", method = RequestMethod.POST)
    public RetDataBean getKnowledgeCommentById(KnowledgeComment knowledgeComment) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            knowledgeComment.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, knowledgeCommentService.selectOneKnowledgeComment(knowledgeComment));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param knowledgeLearn
     * @return RetDataBean
     * @Title: getLearnCount
     * @Description:  获取知识的学习次数
     */
    @RequestMapping(value = "/getLearnCount", method = RequestMethod.POST)
    public RetDataBean getLearnCount(KnowledgeLearn knowledgeLearn) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            knowledgeLearn.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, knowledgeLearnService.getLearnCount(knowledgeLearn));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @param sortId
     * @return RetDataBean
     * @Title: getAllKnowledgeList
     * @Description:  获取所有知识列表
     */
    @RequestMapping(value = "/getAllKnowledgeList", method = RequestMethod.POST)
    public RetDataBean getAllKnowledgeList(
            PageParam pageParam,
            String beginTime,
            String endTime,
            String sortId
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("knowledges:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("k.sort_no");
            } else {
                pageParam.setSort("k." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = knowledgeService.getAllKnowledgeList(pageParam, beginTime, endTime, sortId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getAllKnowledgeSortMap
     * @Description:  获取所有知识分类
     */
    @RequestMapping(value = "/getAllKnowledgeSortMap", method = RequestMethod.POST)
    public RetDataBean getAllKnowledgeSortMap() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, knowledgeSortService.getAllKnowledgeSortMap(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getHostKeyWords
     * @Description:  获取热门关键字
     */
    @RequestMapping(value = "/getHostKeyWords", method = RequestMethod.POST)
    public RetDataBean getHostKeyWords() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, knowledgeSearchService.getHostKeyWords(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: getMyCreateKnowledgeList
     * @Description:  获取我可管理的知识列表
     * @param: request
     * @param: pageParam
     * @param: beginTime
     * @param: endTime
     * @param: sortId
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getMyCreateKnowledgeList", method = RequestMethod.POST)
    public RetDataBean getMyCreateKnowledgeList(
            PageParam pageParam,
            String beginTime,
            String endTime,
            String sortId
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("knowledges:manage")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_MANAGE_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("k.sort_no");
            } else {
                pageParam.setSort("k." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setAccountId(userInfo.getAccountId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrgId(userInfo.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = knowledgeService.getMyCreateKnowledgeList(pageParam, beginTime, endTime, sortId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: getKnowledgeStudyList
     * @Description:  获取学习的知识列表
     * @param: request
     * @param: pageParam
     * @param: beginTime
     * @param: endTime
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getKnowledgeStudyList", method = RequestMethod.POST)
    public RetDataBean getKnowledgeStudyList(
            PageParam pageParam,
            String beginTime,
            String endTime,
            String sortId
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("knowledges:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("k.sort_no");
            } else {
                pageParam.setSort("k." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setAccountId(userInfo.getAccountId());
            pageParam.setDeptId(userInfo.getDeptId());
            pageParam.setLevelId(userInfo.getLeadLevel());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrgId(userInfo.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = knowledgeService.getKnowledgeStudyList(pageParam, beginTime, endTime, sortId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: searchIndex
     * @Description:  全文检索
     * @param: request
     * @param: keywords
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/searchIndex", method = RequestMethod.POST)
    public RetDataBean searchIndex(String keywords) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            KnowledgeSearch knowledgeSearch = new KnowledgeSearch();
            knowledgeSearch.setKeyWord(keywords);
            knowledgeSearch.setOrgId(account.getOrgId());
            knowledgeSearchService.addKnowledgeSearch(account, knowledgeSearch);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, knowledgeService.searchIndex(account.getOrgId(), keywords));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getKnowledgeById
     * @Description:  获取知识详情
     * @param: request
     * @param: knowledge
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getKnowledgeById", method = RequestMethod.POST)
    public RetDataBean getKnowledgeById(Knowledge knowledge) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("knowledges:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            knowledge.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, knowledgeService.selectOneKnowledge(knowledge));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: getknowledgeSortTree
     * @Description:  获取知识管理树
     * @param: request
     * @param: sortId
     * @param: @return
     * @return: List<Map < String, String>>
     */
    @RequestMapping(value = "/getknowledgeSortTree", method = RequestMethod.POST)
    public List<Map<String, String>> getknowledgeSortTree(String sortId) {
        try {
            String sortLevel = "0";
            if (StringUtils.isNotBlank(sortId)) {
                sortLevel = sortId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return knowledgeSortService.getKnowledgeSortTree(account.getOrgId(), sortLevel);
        } catch (Exception e) {
            return null;
        }
    }


    /**
     * @Title: getKnowledgeSortById
     * @Description:  获取知识管理数详情
     * @param: request
     * @param: knowledgeSort
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getKnowledgeSortById", method = RequestMethod.POST)
    public RetDataBean getKnowledgeSortById(KnowledgeSort knowledgeSort) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            knowledgeSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, knowledgeSortService.selectOneKnowledgeSort(knowledgeSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


}
