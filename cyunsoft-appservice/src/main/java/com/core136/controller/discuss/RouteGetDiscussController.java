package com.core136.controller.discuss;

import com.core136.bean.account.Account;
import com.core136.bean.account.UserInfo;
import com.core136.bean.discuss.Discuss;
import com.core136.bean.discuss.DiscussNotice;
import com.core136.bean.discuss.DiscussRecord;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.service.account.AccountService;
import com.core136.service.discuss.DiscussNoticeService;
import com.core136.service.discuss.DiscussRecordService;
import com.core136.service.discuss.DiscussService;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

import java.util.Map;

@RestController
@RequestMapping("/ret/oaget")
public class RouteGetDiscussController {
    private DiscussRecordService discussRecordService;

    @Autowired
    public void setDiscussRecordService(DiscussRecordService discussRecordService) {
        this.discussRecordService = discussRecordService;
    }

    private DiscussService discussService;

    @Autowired
    public void setDiscussService(DiscussService discussService) {
        this.discussService = discussService;
    }

    private DiscussNoticeService discussNoticeService;

    @Autowired
    public void setDiscussNoticeService(DiscussNoticeService discussNoticeService) {
        this.discussNoticeService = discussNoticeService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * 获取帖子与子帖
     *
     * @param recordId
     * @return
     */
    @RequestMapping(value = "/getDiscussRecordListById", method = RequestMethod.POST)
    public RetDataBean getDiscussRecordListById(String recordId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, discussRecordService.getDiscussRecordListById(account.getOrgId(), account.getAccountId(), recordId));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @return RetDataBean
     * @Title: getTopDiscussRecordApprovalList
     * @Description:  获取待审批讨论区主题列表
     */
    @RequestMapping(value = "/getTopDiscussRecordApprovalList", method = RequestMethod.POST)
    public RetDataBean getTopDiscussRecordApprovalList(PageParam pageParam) {
        Account account = accountService.getRedisAUserInfoToAccount();
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("d.create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            pageParam.setOrgId(account.getOrgId());
            if (!account.getOpFlag().equals("1")) {
                pageParam.setAccountId(account.getAccountId());
            }
            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(orderBy);
            PageInfo<Map<String, String>> pageInfo = discussRecordService.getTopDiscussRecordApprovalList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param discussId
     * @return RetDataBean
     * @Title: getTopDiscussRecordList
     * @Description:  获取讨论主题列表
     */
    @RequestMapping(value = "/getTopDiscussRecordList", method = RequestMethod.POST)
    public RetDataBean getTopDiscussRecordList(PageParam pageParam, String discussId) {
        Account account = accountService.getRedisAUserInfoToAccount();
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("d.create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            pageParam.setOrgId(account.getOrgId());
            if (!account.getOpFlag().equals("1")) {
                pageParam.setAccountId(account.getAccountId());
            }
            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            pageParam.setOrderBy(orderBy);
            PageInfo<Map<String, String>> pageInfo = discussRecordService.getTopDiscussRecordList(pageParam, discussId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getDiscussNoticeList
     * @Description:  获取通知公告列表
     */
    @RequestMapping(value = "/getDiscussNoticeList", method = RequestMethod.POST)
    public RetDataBean getDiscussNoticeList() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, discussNoticeService.getDiscussNoticeList(account));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param discussNotice
     * @return RetDataBean
     * @Title: getDiscussNoticeById
     * @Description:  获取讨论区通知公告
     */
    @RequestMapping(value = "/getDiscussNoticeById", method = RequestMethod.POST)
    public RetDataBean getDiscussNoticeById(DiscussNotice discussNotice) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            discussNotice.setOrgId(account.getOrgId());
            discussNotice = discussNoticeService.selectOneDiscussNotice(discussNotice);
            String reader = discussNotice.getReader();
            DiscussNotice discussNoticeTemp = new DiscussNotice();
            if (("," + reader + ",").indexOf("," + account.getAccountId() + ",") <= 0) {
                if (StringUtils.isBlank(reader)) {
                    reader = account.getAccountId();
                } else {
                    reader = reader + "," + account.getAccountId();
                }
            }
            discussNoticeTemp.setReader(reader);
            Example example = new Example(DiscussNotice.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", discussNotice.getRecordId());
            discussNoticeService.updateDiscussNotice(example, discussNoticeTemp);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, discussNotice);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param discussId
     * @return RetDataBean
     * @Title: getLastDiscussRecordTime
     * @Description:  获取最后发贴时间
     */
    @RequestMapping(value = "/getLastDiscussRecordTime", method = RequestMethod.POST)
    public RetDataBean getLastDiscussRecordTime(String discussId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, discussRecordService.getLastDiscussRecordTime(account.getOrgId(), discussId));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param discussId
     * @return RetDataBean
     * @Title: getDiscussRecordCount
     * @Description:  获取发怗数
     */
    @RequestMapping(value = "/getDiscussRecordCount", method = RequestMethod.POST)
    public RetDataBean getDiscussRecordCount(String discussId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            DiscussRecord discussRecord = new DiscussRecord();
            discussRecord.setDiscussId(discussId);
            discussRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, discussRecordService.getDiscussRecordCount(discussRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @return RetDataBean
     * @Title: getMyDiscussList
     * @Description:  获取讨论区列表
     */
    @RequestMapping(value = "/getMyDiscussList", method = RequestMethod.POST)
    public RetDataBean getMyDiscussList() {
        try {
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, discussService.getMyDiscussList(userInfo.getOrgId(), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getLeadLevel()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @return RetDataBean
     * @Title: getDiscussList
     * @Description:  版块列表
     */
    @RequestMapping(value = "/getDiscussList", method = RequestMethod.POST)
    public RetDataBean getDiscussList(PageParam pageParam) {
        Account account = accountService.getRedisAUserInfoToAccount();
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("d.sort_no");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            pageParam.setOrgId(account.getOrgId());
            if (!account.getOpFlag().equals("1")) {
                pageParam.setAccountId(account.getAccountId());
            }
            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            pageParam.setOrderBy(orderBy);
            PageInfo<Map<String, String>> pageInfo = discussService.getDiscussList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param discuss
     * @return RetDataBean
     * @Title: getDiscussById
     * @Description:  讨论区块版记录
     */
    @RequestMapping(value = "/getDiscussById", method = RequestMethod.POST)
    public RetDataBean getDiscussById(Discuss discuss) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            discuss.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, discussService.selectOneDiscuss(discuss));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param discussRecord
     * @return RetDataBean
     * @Title: getDiscussRecordById
     * @Description:  讨论区记录
     */
    @RequestMapping(value = "/getDiscussRecordById", method = RequestMethod.POST)
    public RetDataBean getDiscussRecordById(DiscussRecord discussRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            discussRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, discussRecordService.selectOneDiscussRecord(discussRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

}
