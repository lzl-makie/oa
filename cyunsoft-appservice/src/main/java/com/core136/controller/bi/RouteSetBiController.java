package com.core136.controller.bi;

import com.core136.bean.account.Account;
import com.core136.bean.bi.BiSort;
import com.core136.bean.bi.BiTemplate;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.bi.BiSortService;
import com.core136.service.bi.BiTemplateService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

@RestController
@RequestMapping("/set/biset")
public class RouteSetBiController {
    private BiSortService biSortService;

    @Autowired
    public void setBiSortService(BiSortService biSortService) {
        this.biSortService = biSortService;
    }

    private BiTemplateService biTemplateService;

    @Autowired
    public void setBiTemplateService(BiTemplateService biTemplateService) {
        this.biTemplateService = biTemplateService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * @param biSort
     * @return RetDataBean
     * @Title insertBiSort
     * @Description  创建BI分类
     */
    @RequestMapping(value = "/insertBiSort", method = RequestMethod.POST)
    public RetDataBean insertBiSort(BiSort biSort) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            biSort.setSortId(SysTools.getGUID());
            if (StringUtils.isBlank(biSort.getLevelId())) {
                biSort.setLevelId("0");
            }
            biSort.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            biSort.setCreateUser(account.getAccountId());
            biSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, biSortService.insertBiSort(biSort));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param biSort
     * @return RetDataBean
     * @Title deleteBiSort
     * @Description  删除BI分类
     */
    @RequestMapping(value = "/deleteBiSort", method = RequestMethod.POST)
    public RetDataBean deleteBiSort(BiSort biSort) {
        try {
            if (StringUtils.isBlank(biSort.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            biSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, biSortService.deleteBiSort(biSort));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param biSort
     * @return RetDataBean
     * @Title updateBiSort
     * @Description  更新分类
     */
    @RequestMapping(value = "/updateBiSort", method = RequestMethod.POST)
    public RetDataBean updateBiSort(BiSort biSort) {
        try {
            if (StringUtils.isBlank(biSort.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            if (biSort.getSortId().equals(biSort.getLevelId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_PARENT_ID_CANNOT_TO_USE);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            biSort.setOrgId(account.getOrgId());
            Example example = new Example(BiSort.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("sortId", biSort.getSortId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, biSortService.updateBiSort(biSort, example));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param biTemplate
     * @return RetDataBean
     * @Title deleteBiTemplate
     * @Description  删除BI模版
     */
    @RequestMapping(value = "/deleteBiTemplate", method = RequestMethod.POST)
    public RetDataBean deleteBiTemplate(BiTemplate biTemplate) {
        try {
            if (StringUtils.isBlank(biTemplate.getTemplateId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            biTemplate.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, biTemplateService.deleteBiTemplate(biTemplate));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param biTemplate
     * @return RetDataBean
     * @Title insertBiTemplate
     * @Description  添加BI模版
     */
    @RequestMapping(value = "/insertBiTemplate", method = RequestMethod.POST)
    public RetDataBean insertBiTemplate(BiTemplate biTemplate) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            biTemplate.setTemplateId(SysTools.getGUID());
            biTemplate.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            biTemplate.setCreateUser(account.getAccountId());
            biTemplate.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, biTemplateService.insertBiTemplate(biTemplate));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param biTemplate
     * @return RetDataBean
     * @Title updateBiTemplate
     * @Description  更新BI模版
     */
    @RequestMapping(value = "/updateBiTemplate", method = RequestMethod.POST)
    public RetDataBean updateBiTemplate(BiTemplate biTemplate) {
        try {
            if (StringUtils.isBlank(biTemplate.getTemplateId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            biTemplate.setOrgId(account.getOrgId());
            Example example = new Example(BiTemplate.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("templateId", biTemplate.getTemplateId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, biTemplateService.updateBiTemplate(biTemplate, example));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }
}
