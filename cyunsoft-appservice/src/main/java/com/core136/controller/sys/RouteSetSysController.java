package com.core136.controller.sys;

import com.alibaba.fastjson.JSON;
import com.core136.bean.account.Account;
import com.core136.bean.oa.Shortcut;
import com.core136.bean.sys.*;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.account.UserPrivService;
import com.core136.service.oa.ShortcutService;
import com.core136.service.sys.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

import java.util.*;

/**
 * @ClassName: RouteSetSysController
 * @Description: 系统
 * @author: 稠云信息
 * @date: 2019年1月3日 上午9:31:29
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@RestController
@RequestMapping("/set/sysset")
public class RouteSetSysController {
    private SmsService smsService;

    @Autowired
    public void setSmsService(SmsService smsService) {
        this.smsService = smsService;
    }

    private SysDbSourceService sysDbSourceService;

    @Autowired
    public void setSysDbSourceService(SysDbSourceService sysDbSourceService) {
        this.sysDbSourceService = sysDbSourceService;
    }

    private CodeClassService codeClassService;

    @Autowired
    public void setCodeClassService(CodeClassService codeClassService) {
        this.codeClassService = codeClassService;
    }

    private SmsConfigService smsConfigService;

    @Autowired
    public void setSmsConfigService(SmsConfigService smsConfigService) {
        this.smsConfigService = smsConfigService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private SysDeskConfigService sysDeskConfigService;

    @Autowired
    public void setSysDeskConfigService(SysDeskConfigService sysDeskConfigService) {
        this.sysDeskConfigService = sysDeskConfigService;
    }

    private SysMonitorService sysMonitorService;

    @Autowired
    public void setSysMonitorService(SysMonitorService sysMonitorService) {
        this.sysMonitorService = sysMonitorService;
    }

    private SysLogService sysLogService;

    @Autowired
    public void setSysLogService(SysLogService sysLogService) {
        this.sysLogService = sysLogService;
    }

    private SysOrgConfigService sysOrgConfigService;

    @Autowired
    public void setSysOrgConfigService(SysOrgConfigService sysOrgConfigService) {
        this.sysOrgConfigService = sysOrgConfigService;
    }

    private SysInterfaceService sysInterfaceService;

    @Autowired
    public void setSysInterfaceService(SysInterfaceService sysInterfaceService) {
        this.sysInterfaceService = sysInterfaceService;
    }

    private AppConfigService appConfigService;

    @Autowired
    public void setAppConfigService(AppConfigService appConfigService) {
        this.appConfigService = appConfigService;
    }

    private UserPrivService userPrivService;

    @Autowired
    public void setUserPrivService(UserPrivService userPrivService) {
        this.userPrivService = userPrivService;
    }

    private SysMenuService sysMenuService;

    @Autowired
    public void setSysMenuService(SysMenuService sysMenuService) {
        this.sysMenuService = sysMenuService;
    }

    private ShortcutService shortcutService;

    @Autowired
    public void setShortcutService(ShortcutService shortcutService) {
        this.shortcutService = shortcutService;
    }

    private SysTimingTaskService sysTimingTaskService;

    @Autowired
    public void setSysTimingTaskService(SysTimingTaskService sysTimingTaskService) {
        this.sysTimingTaskService = sysTimingTaskService;
    }

    private SysProfileService sysProfileService;

    @Autowired
    public void setSysProfileService(SysProfileService sysProfileService) {
        this.sysProfileService = sysProfileService;
    }

    private SysWebSiteService sysWebSiteService;

    @Autowired
    public void setSysWebSiteService(SysWebSiteService sysWebSiteService) {
        this.sysWebSiteService = sysWebSiteService;
    }

    private SysApplicationService sysApplicationService;

    @Autowired
    public void setSysApplicationService(SysApplicationService sysApplicationService) {
        this.sysApplicationService = sysApplicationService;
    }

    private SysApplicationAccountService sysApplicationAccountService;

    @Autowired
    public void setSysApplicationAccountService(SysApplicationAccountService sysApplicationAccountService) {
        this.sysApplicationAccountService = sysApplicationAccountService;
    }

    private SysThreeMemberService sysThreeMemberService;

    @Autowired
    public void setSysThreeMemberService(SysThreeMemberService sysThreeMemberService) {
        this.sysThreeMemberService = sysThreeMemberService;
    }

    private ThreeEventRecordService threeEventRecordService;

    @Autowired
    public void setThreeEventRecordService(ThreeEventRecordService threeEventRecordService) {
        this.threeEventRecordService = threeEventRecordService;
    }

    private ThreeEventApprovalService threeEventApprovalService;

    @Autowired
    public void setThreeEventApprovalService(ThreeEventApprovalService threeEventApprovalService) {
        this.threeEventApprovalService = threeEventApprovalService;
    }


    private SysInitializationUnitService sysInitializationUnitService;

    @Autowired
    public void setSysInitializationUnitService(SysInitializationUnitService sysInitializationUnitService) {
        this.sysInitializationUnitService = sysInitializationUnitService;
    }

    private SysConfigInitService sysConfigInitService;

    @Autowired
    public void setSysConfigInitService(SysConfigInitService sysConfigInitService) {
        this.sysConfigInitService = sysConfigInitService;
    }

    private FullTextConfigService fullTextConfigService;

    @Autowired
    public void setFullTextConfigService(FullTextConfigService fullTextConfigService) {
        this.fullTextConfigService = fullTextConfigService;
    }


    /**
     * 停用全文检索
     *
     * @param module
     * @return
     */
    @RequestMapping(value = "/setStopFullTextConfig", method = RequestMethod.POST)
    public RetDataBean setStopFullTextConfig(String module) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            Map<String, String> map = new HashMap<String, String>();
            map.put(module, "0");
            FullTextConfig fullTextConfig = JSON.parseObject(JSON.toJSONString(map), FullTextConfig.class);
            Example example = new Example(FullTextConfig.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, fullTextConfigService.updateFullTextConfig(fullTextConfig, example));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 启用全文检索
     *
     * @param module
     * @return
     */
    @RequestMapping(value = "/setOpenFullTextConfig", method = RequestMethod.POST)
    public RetDataBean setOpenFullTextConfig(String module) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            Map<String, String> map = new HashMap<String, String>();
            map.put(module, "1");
            FullTextConfig fullTextConfig = JSON.parseObject(JSON.toJSONString(map), FullTextConfig.class);
            Example example = new Example(FullTextConfig.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, fullTextConfigService.updateFullTextConfig(fullTextConfig, example));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param recordId
     * @param status
     * @return RetDataBean
     * @Title: setThreeMemnerApproval
     * @Description:  安全保密员审批并执行相关操作
     */
    @RequestMapping(value = "/setThreeMemnerApproval", method = RequestMethod.POST)
    public RetDataBean setThreeMemnerApproval(String recordId, String status, String remark) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(recordId) || StringUtils.isBlank(status)) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            return threeEventRecordService.setThreeMemnerApproval(account, recordId, status, remark);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param sysThreeMember
     * @return RetDataBean
     * @Title: setSysThreeMember
     * @Description:  设置三员管理配置
     */
    @RequestMapping(value = "/setSysThreeMember", method = RequestMethod.POST)
    public RetDataBean setSysThreeMember(SysThreeMember sysThreeMember) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            sysThreeMember.setOrgId(account.getOrgId());
            if (StringUtils.isBlank(sysThreeMember.getOrgId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            return sysThreeMemberService.setSysThreeMember(sysThreeMember);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param userSignCode
     * @return RetDataBean
     * @Title: doInitialization
     * @Description:  初始化功能模块数据
     */
    @RequestMapping(value = "/doInitialization", method = RequestMethod.POST)
    public RetDataBean doInitialization(String userSignCode) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(userSignCode)) {
                return RetDataTools.NotOk(MessageCode.MSG_00022);
            }
            return sysInitializationUnitService.doInitialization(account, userSignCode);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param sysApplicationAccount
     * @return RetDataBean
     * @Title: insertSysApplicationAccount
     * @Description:  添加单点登陆账号
     */
    @RequestMapping(value = "/insertSysApplicationAccount", method = RequestMethod.POST)
    public RetDataBean insertSysApplicationAccount(SysApplicationAccount sysApplicationAccount) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            sysApplicationAccount.setRecordId(SysTools.getGUID());
            sysApplicationAccount.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            sysApplicationAccount.setCreateUser(account.getAccountId());
            sysApplicationAccount.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, sysApplicationAccountService.insertSysApplicationAccount(sysApplicationAccount));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param sysApplicationAccount
     * @return RetDataBean
     * @Title: SysApplicationAccount
     * @Description:  删除单点登陆账号
     */
    @RequestMapping(value = "/deleteSysApplicationAccount", method = RequestMethod.POST)
    public RetDataBean deleteSysApplicationAccount(SysApplicationAccount sysApplicationAccount) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(sysApplicationAccount.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            sysApplicationAccount.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, sysApplicationAccountService.deleteSysApplicationAccount(sysApplicationAccount));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param sysApplicationAccount
     * @return RetDataBean
     * @Title: updateSysApplicationAccount
     * @Description:  更新单点登陆账号
     */
    @RequestMapping(value = "/updateSysApplicationAccount", method = RequestMethod.POST)
    public RetDataBean updateSysApplicationAccount(SysApplicationAccount sysApplicationAccount) {
        try {
            if (StringUtils.isBlank(sysApplicationAccount.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(SysApplicationAccount.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", sysApplicationAccount.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, sysApplicationAccountService.updateSysApplicationAccount(example, sysApplicationAccount));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param sysApplication
     * @return RetDataBean
     * @Title: insertSysApplication
     * @Description:  添加单点登陆系统
     */
    @RequestMapping(value = "/insertSysApplication", method = RequestMethod.POST)
    public RetDataBean insertSysApplication(SysApplication sysApplication) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            sysApplication.setApplicationId(SysTools.getGUID());
            sysApplication.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            sysApplication.setCreateUser(account.getAccountId());
            sysApplication.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, sysApplicationService.insertSysApplication(sysApplication));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param sysApplication
     * @return RetDataBean
     * @Title: deleteSysApplication
     * @Description:  删除单点登陆系统
     */
    @RequestMapping(value = "/deleteSysApplication", method = RequestMethod.POST)
    public RetDataBean deleteSysApplication(SysApplication sysApplication) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(sysApplication.getApplicationId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            sysApplication.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, sysApplicationService.deleteSysApplication(sysApplication));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param sysApplication
     * @return RetDataBean
     * @Title: updateSysApplication
     * @Description:  更新单点登陆系统
     */
    @RequestMapping(value = "/updateSysApplication", method = RequestMethod.POST)
    public RetDataBean updateSysApplication(SysApplication sysApplication) {
        try {
            if (StringUtils.isBlank(sysApplication.getApplicationId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(SysApplication.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("applicationId", sysApplication.getApplicationId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, sysApplicationService.updateSysApplication(example, sysApplication));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @return RetDataBean
     * @Title deleteSysWebSite
     * @Description 删除常用网址
     */
    @RequestMapping(value = "/deleteSysWebSite", method = RequestMethod.POST)
    public RetDataBean deleteSysWebSite(SysWebSite sysWebSite) {
        try {
            if (StringUtils.isBlank(sysWebSite.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            sysWebSite.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, sysWebSiteService.deleteSysWebSite(sysWebSite));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param sysWebSiteArr
     * @param @return
     * @Title: deleteSysWebSiteBatch
     * @Description:  批量删除常用网址
     * @return: RetDataBean
     */
    @RequestMapping(value = "/deleteSysWebSiteBatch", method = RequestMethod.POST)
    public RetDataBean deleteSysWebSiteBatch(@RequestParam(value = "sysWebSiteArr[]") String[] sysWebSiteArr) {
        try {
            if (sysWebSiteArr.length == 0) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            }
            List<String> list = new ArrayList<String>(Arrays.asList(sysWebSiteArr));
            Example example = new Example(SysWebSite.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andIn("recordId", list);
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, sysWebSiteService.deleteSysWebSiteBatch(example));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title insertSysWebSite
     * @Description  添加常用网址
     */
    @RequestMapping(value = "/insertSysWebSite", method = RequestMethod.POST)
    public RetDataBean insertSysWebSite(SysWebSite sysWebSite) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            sysWebSite.setRecordId(SysTools.getGUID());
            sysWebSite.setAccountId(account.getAccountId());
            sysWebSite.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            sysWebSite.setCreateUser(account.getAccountId());
            sysWebSite.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, sysWebSiteService.insertSysWebSite(sysWebSite));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title updateSysWebSite
     * @Description  更新常用网址
     */
    @RequestMapping(value = "/updateSysWebSite", method = RequestMethod.POST)
    public RetDataBean updateSysWebSite(SysWebSite sysWebSite) {
        try {
            if (StringUtils.isBlank(sysWebSite.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(SysWebSite.class);
            example.createCriteria().andEqualTo("recordId", sysWebSite.getRecordId()).andEqualTo("orgId", account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, sysWebSiteService.updateSysWebSite(example, sysWebSite));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param appConfig
     * @param @return
     * @Title: updateAppConfig
     * @Description:  更新APP应用
     * @return: RetDataBean
     */
    @RequestMapping(value = "/updateAppConfig", method = RequestMethod.POST)
    public RetDataBean updateAppConfig(AppConfig appConfig) {
        try {
            if (StringUtils.isBlank(appConfig.getAppId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            }
            Example example = new Example(AppConfig.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("appId", appConfig.getAppId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, appConfigService.updateAppConfig(example, appConfig));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param appConfig
     * @param @return
     * @Title: deleteAppConfig
     * @Description:  删除APP应用
     * @return: RetDataBean
     */
    @RequestMapping(value = "/deleteAppConfig", method = RequestMethod.POST)
    public RetDataBean deleteAppConfig(AppConfig appConfig) {
        try {
            if (StringUtils.isBlank(appConfig.getAppId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            }
            appConfig.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, appConfigService.deleteAppConfig(appConfig));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param appConfig
     * @param @return
     * @Title: insertAppConfig
     * @Description:  添加APP应用
     * @return: RetDataBean
     */
    @RequestMapping(value = "/insertAppConfig", method = RequestMethod.POST)
    public RetDataBean insertAppConfig(AppConfig appConfig) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            appConfig.setAppId(SysTools.getGUID());
            appConfig.setCreateUser(account.getAccountId());
            appConfig.setStatus("0");
            appConfig.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            appConfig.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, appConfigService.insertAppConfig(appConfig));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param sysInterface
     * @param @return
     * @Title: setInterface
     * @Description:  设置系统登陆界面
     * @return: RetDataBean
     */
    @RequestMapping(value = "/setInterface", method = RequestMethod.POST)
    public RetDataBean setInterface(SysInterface sysInterface) {
        try {
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, sysInterfaceService.setSysInterface(sysInterface));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param sysOrgConfig
     * @param @return
     * @Title: addOrgConfig
     * @Description:  创建新机构
     * @return: RetDataBean
     */
    @RequestMapping(value = "/addOrgConfig", method = RequestMethod.POST)
    public RetDataBean addOrgConfig(SysOrgConfig sysOrgConfig, String orgName, String password) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(orgName)) {
                return RetDataTools.NotOk(MessageCode.MSG_00023);
            }
            if (StringUtils.isBlank(sysOrgConfig.getOrgAdmin())) {
                return RetDataTools.NotOk(MessageCode.MSG_00024);
            }
            if (StringUtils.isBlank(password)) {
                return RetDataTools.NotOk(MessageCode.MSG_00025);
            }
            sysOrgConfig.setConfigId(SysTools.getGUID());
            sysOrgConfig.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            sysOrgConfig.setCreateUser(account.getAccountId());
            sysOrgConfig.setOrgId(SysTools.getGUID());
            return sysOrgConfigService.addOrg(account.getOrgId(), sysOrgConfig, orgName, password);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param sysDbSource
     * @return RetDataBean
     * @Title insertDbSource
     * @Description  添加数据源
     */
    @RequestMapping(value = "/insertDbSource", method = RequestMethod.POST)
    public RetDataBean insertDbSource(SysDbSource sysDbSource) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            sysDbSource.setDbSourceId(SysTools.getGUID());
            sysDbSource.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            sysDbSource.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, sysDbSourceService.insertSysDbSource(sysDbSource));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param sysDbSource
     * @return RetDataBean
     * @Title updateDbSource
     * @Description  更新数据
     */
    @RequestMapping(value = "/updateDbSource", method = RequestMethod.POST)
    public RetDataBean updateDbSource(SysDbSource sysDbSource) {
        try {
            if (StringUtils.isBlank(sysDbSource.getDbSourceId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            sysDbSource.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            sysDbSource.setOrgId(account.getOrgId());
            Example example = new Example(SysDbSource.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("dbSourceId", sysDbSource.getDbSourceId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, sysDbSourceService.updateSysDbSource(sysDbSource, example));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param sysDbSource
     * @return RetDataBean
     * @Title deleteDbSource
     * @Description  删除数据源
     */
    @RequestMapping(value = "/deleteDbSource", method = RequestMethod.POST)
    public RetDataBean deleteDbSource(SysDbSource sysDbSource) {
        try {
            if (StringUtils.isBlank(sysDbSource.getDbSourceId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            }
            sysDbSource.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, sysDbSourceService.deleteSysDbSource(sysDbSource));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param sysConfigInit
     * @return RetDataBean
     * @Title insertSysConfig
     * @Description  添加系统配置
     */
    @RequestMapping(value = "/updateSysConfig", method = RequestMethod.POST)
    public RetDataBean updateSysConfig(SysConfigInit sysConfigInit) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            sysConfigInit.setOrgId(account.getOrgId());
            SysConfigInit newSysConfigInit = new SysConfigInit();
            newSysConfigInit.setOrgId(sysConfigInit.getOrgId());
            newSysConfigInit = sysConfigInitService.selectOneSysConfigInit(newSysConfigInit);
            if (newSysConfigInit != null) {
                Example example = new Example(SysConfigInit.class);
                example.createCriteria().andEqualTo("orgId", sysConfigInit.getOrgId());
                return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, sysConfigInitService.updateSysConfigInit(sysConfigInit, example));

            } else {
                sysConfigInit.setSysConfigId(SysTools.getGUID());
                return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, sysConfigInitService.insertSysConfigInit(sysConfigInit));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param codeClass
     * @return RetDataBean
     * @Title deleteCodeClass
     * @Description 删除系统分类
     */
    @RequestMapping(value = "/deleteCodeClass", method = RequestMethod.POST)
    public RetDataBean deleteCodeClass(CodeClass codeClass) {
        try {
            if (StringUtils.isBlank(codeClass.getCodeClassId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            codeClass.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, codeClassService.deleteCodeClass(codeClass));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param classCodeArr
     * @param @return
     * @Title: deleteCodeClassBatch
     * @Description:  批量删除分类码
     * @return: RetDataBean
     */
    @RequestMapping(value = "/deleteCodeClassBatch", method = RequestMethod.POST)
    public RetDataBean deleteCodeClassBatch(@RequestParam(value = "classCodeArr[]") String[] classCodeArr) {
        try {
            if (classCodeArr.length == 0) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            }
            List<String> list = new ArrayList<String>(Arrays.asList(classCodeArr));
            Example example = new Example(CodeClass.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andIn("classCodeId", list);
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, codeClassService.deleteCodeClass(example));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param codeClass
     * @return RetDataBean
     * @Title insertCodeClass
     * @Description  添加系统分类
     */
    @RequestMapping(value = "/insertCodeClass", method = RequestMethod.POST)
    public RetDataBean insertCodeClass(CodeClass codeClass) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            codeClass.setCodeClassId(SysTools.getGUID());
            codeClass.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            codeClass.setCreateUser(account.getAccountId());
            codeClass.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, codeClassService.insertCodeClass(codeClass));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param codeClass
     * @return RetDataBean
     * @Title updateCodeClass
     * @Description  更新系统编码
     */
    @RequestMapping(value = "/updateCodeClass", method = RequestMethod.POST)
    public RetDataBean updateCodeClass(CodeClass codeClass) {
        try {
            if (StringUtils.isBlank(codeClass.getCodeClassId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(CodeClass.class);
            example.createCriteria().andEqualTo("codeClassId", codeClass.getCodeClassId()).andEqualTo("orgId", account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, codeClassService.updateCodeClass(codeClass, example));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param config
     * @return RetDataBean
     * @Title updateSmsConfig
     * @Description  设置消息提醒权限
     */
    @RequestMapping(value = "/updateSmsConfig", method = RequestMethod.POST)
    public RetDataBean updateSmsConfig(String config, String mustChecked) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (account.getOpFlag().equals("1")) {
                SmsConfig smsConfig = new SmsConfig();
                smsConfig.setOrgId(account.getOrgId());
                if (smsConfigService.isExist(smsConfig) > 0) {
                    smsConfig.setConfig(config);
                    smsConfig.setMustChecked(mustChecked);
                    Example example = new Example(SmsConfig.class);
                    example.createCriteria().andEqualTo("orgId", account.getOrgId());
                    return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, smsConfigService.updateSmsConfig(smsConfig, example));
                } else {
                    smsConfig.setConfig(config);
                    smsConfig.setMustChecked(mustChecked);
                    return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, smsConfigService.insertSmsConfig(smsConfig));
                }
            } else {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  sysDeskConfig
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: createDeskConfig
     * @Description:  创建系统桌面模块
     */
    @RequestMapping(value = "/createDeskConfig", method = RequestMethod.POST)
    public RetDataBean createDeskConfig(SysDeskConfig sysDeskConfig) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            } else {
                sysDeskConfig.setDeskConfigId(SysTools.getGUID());
                sysDeskConfig.setUseUserPriv("");
                sysDeskConfig.setUseDeptPriv("");
                sysDeskConfig.setUseLevelPriv("");
                sysDeskConfig.setMustUseUserPriv("");
                sysDeskConfig.setMustUseDeptPriv("");
                sysDeskConfig.setMustUseLevelPriv("");
                sysDeskConfig.setUnUseUserPriv("");
                sysDeskConfig.setUnUseDeptPriv("");
                sysDeskConfig.setUnUseLevelPriv("");
                sysDeskConfig.setOrgId(account.getOrgId());
                return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, sysDeskConfigService.insertSysDeskConfig(sysDeskConfig));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  sysDeskConfig
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: updateDeskConfig
     * @Description:  更新系统桌面模块
     */
    @RequestMapping(value = "/updateDeskConfig", method = RequestMethod.POST)
    public RetDataBean updateDeskConfig(SysDeskConfig sysDeskConfig) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(sysDeskConfig.getDeskConfigId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            } else {
                sysDeskConfig.setOrgId(account.getOrgId());
                Example example = new Example(SysDeskConfig.class);
                example.createCriteria().andEqualTo("deskConfigId", sysDeskConfig.getDeskConfigId()).andEqualTo("orgId", sysDeskConfig.getOrgId());
                return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, sysDeskConfigService.updateSysDeskConfig(sysDeskConfig, example));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  sysDeskConfig
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: delDeskConfig
     * @Description:  删除系统桌面模块
     */
    @RequestMapping(value = "/delDeskConfig", method = RequestMethod.POST)
    public RetDataBean delDeskConfig(SysDeskConfig sysDeskConfig) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(sysDeskConfig.getDeskConfigId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            if (account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            } else {
                sysDeskConfig.setOrgId(account.getOrgId());
                return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, sysDeskConfigService.delSysDeskConfig(sysDeskConfig));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param shortcut
     * @param @return
     * @Title: setShortcut
     * @Description:  设置个人快捷方式
     * @return: RetDataBean
     */
    @RequestMapping(value = "/setShortcut", method = RequestMethod.POST)
    public RetDataBean setShortcut(Shortcut shortcut) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            Shortcut shortcut1 = new Shortcut();
            shortcut1.setCreateUser(account.getAccountId());
            shortcut1.setOrgId(account.getOrgId());
            shortcut1 = shortcutService.selectOneShortcut(shortcut1);
            if (shortcut1 == null) {
                shortcut.setConfigId(SysTools.getGUID());
                shortcut.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
                shortcut.setCreateUser(account.getAccountId());
                shortcut.setOrgId(account.getOrgId());
                return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, shortcutService.insertShortcut(shortcut));
            } else {
                Example example = new Example(Shortcut.class);
                example.createCriteria().andEqualTo("configId", shortcut1.getConfigId()).andEqualTo("createUser", account.getAccountId());
                return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, shortcutService.updateShortuct(example, shortcut));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param sysTimingTask
     * @param @return
     * @Title: insertSysTimingTask
     * @Description:  创建定时任务
     * @return: RetDataBean
     */
    @RequestMapping(value = "/insertSysTimingTask", method = RequestMethod.POST)
    public RetDataBean insertSysTimingTask(SysTimingTask sysTimingTask) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            sysTimingTask.setTaskId(SysTools.getGUID());
            sysTimingTask.setCreateUser(account.getAccountId());
            sysTimingTask.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            sysTimingTask.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, sysTimingTaskService.insertSysTimingTask(sysTimingTask));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param sysTimingTask
     * @param @return
     * @Title: delSysTimingTask
     * @Description:  删除定时任务
     * @return: RetDataBean
     */
    @RequestMapping(value = "/delSysTimingTask", method = RequestMethod.POST)
    public RetDataBean delSysTimingTask(SysTimingTask sysTimingTask) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(sysTimingTask.getTaskId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            if (account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            } else {
                sysTimingTask.setOrgId(account.getOrgId());
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, sysTimingTaskService.deleteSysTimingTask(sysTimingTask));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param sysTimingTask
     * @param @return
     * @Title: updateSysTimingTask
     * @Description:  更新任务
     * @return: RetDataBean
     */
    @RequestMapping(value = "/updateSysTimingTask", method = RequestMethod.POST)
    public RetDataBean updateSysTimingTask(SysTimingTask sysTimingTask) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(sysTimingTask.getTaskId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            } else {
                sysTimingTask.setOrgId(account.getOrgId());
                Example example = new Example(SysTimingTask.class);
                example.createCriteria().andEqualTo("taskId", sysTimingTask.getTaskId()).andEqualTo("orgId", sysTimingTask.getOrgId());
                return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, sysTimingTaskService.updateSysTimingTask(example, sysTimingTask));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param sysProfile
     * @return RetDataBean
     * @Title: insertSysProfile
     * @Description:  添加系统门户
     */
    @RequestMapping(value = "/insertSysProfile", method = RequestMethod.POST)
    public RetDataBean insertSysProfile(SysProfile sysProfile) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            sysProfile.setProfileId(SysTools.getGUID());
            sysProfile.setCreateUser(account.getAccountId());
            sysProfile.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            sysProfile.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, sysProfileService.insertSysProfile(sysProfile));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param sysProfile
     * @param @return
     * @Title: updateSysProfile
     * @Description:  更新门户权限
     * @return: RetDataBean
     */
    @RequestMapping(value = "/updateSysProfile", method = RequestMethod.POST)
    public RetDataBean updateSysProfile(SysProfile sysProfile) {
        try {
            if (StringUtils.isBlank(sysProfile.getProfileId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            }
            Example example = new Example(SysProfile.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("profileId", sysProfile.getProfileId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, sysProfileService.updateSysProfile(example, sysProfile));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param sysProfile
     * @return RetDataBean
     * @Title: deleteSysProfile
     * @Description:  删除系统门户
     */
    @RequestMapping(value = "/deleteSysProfile", method = RequestMethod.POST)
    public RetDataBean deleteSysProfile(SysProfile sysProfile) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(sysProfile.getProfileId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            } else {
                sysProfile.setOrgId(account.getOrgId());

                sysProfile = sysProfileService.selectOneSysProfile(sysProfile);
                if (sysProfile.getType().equals("1")) {
                    return RetDataTools.NotOk(MessageCode.MSG_00026);
                } else {

                    return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, sysProfileService.deleteSysProfile(sysProfile));
                }
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }
}
