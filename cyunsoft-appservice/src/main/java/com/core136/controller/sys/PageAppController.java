package com.core136.controller.sys;

import com.alibaba.fastjson.JSONObject;
import com.core136.bean.account.AUserInfo;
import com.core136.bean.account.Account;
import com.core136.bean.account.UserPriv;
import com.core136.bean.file.Attach;
import com.core136.bean.file.NetDisk;
import com.core136.bean.sys.SysConfigInit;
import com.core136.bean.sys.SysMenu;
import com.core136.bean.sys.SysProfile;
import com.core136.common.utils.StrTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.account.UserInfoService;
import com.core136.service.account.UserPrivService;
import com.core136.service.file.AttachService;
import com.core136.service.file.NetDiskService;
import com.core136.service.sys.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: AppPageController
 * @Description:系统管理的Controller
 * @author: 刘绍全
 * @date: 2018年10月18日 下午1:20:32
 * @Copyright: 2018 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Controller
@RequestMapping("/app/core")
public class PageAppController {
    private UserPrivService userPrivService;

    @Autowired
    public void setUserPrivService(UserPrivService userPrivService) {
        this.userPrivService = userPrivService;
    }

    private UserInfoService userInfoService;

    @Autowired
    public void setUserInfoService(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }

    private SysProfileService sysProfileService;

    @Autowired
    public void setSysProfileService(SysProfileService sysProfileService) {
        this.sysProfileService = sysProfileService;
    }

    private AppConfigService appConfigService;

    @Autowired
    public void setAppConfigService(AppConfigService appConfigService) {
        this.appConfigService = appConfigService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private AttachService attachService;

    @Autowired
    public void setAttachService(AttachService attachService) {
        this.attachService = attachService;
    }

    private NetDiskService netDiskService;

    @Autowired
    public void setNetDiskService(NetDiskService netDiskService) {
        this.netDiskService = netDiskService;
    }

    private SysMonitorService sysMonitorService;

    @Autowired
    public void setSysMonitorService(SysMonitorService sysMonitorService) {
        this.sysMonitorService = sysMonitorService;
    }

    private SysConfigInitService sysConfigInitService;

    @Autowired
    public void setSysConfigInitService(SysConfigInitService sysConfigInitService) {
        this.sysConfigInitService = sysConfigInitService;
    }

    private SysMenuService sysMenuService;

    @Autowired
    public void setSysMenuService(SysMenuService sysMenuService) {
        this.sysMenuService = sysMenuService;
    }


    @Value("${app.poi.tmppath}")
    private String tmppath;
    @Value("${app.msgtime}")
    private int msgtime;
    @Value("${app.theme}")
    private String theme;

    @RequestMapping("/previewonline")
    public ModelAndView goPreviewOnline(String isNetdisk, String netDiskId, String attachId) {
        String html = null;
        attachId = attachId.replace("\\", File.separator).replace("/", File.separator);
        ModelAndView mv = new ModelAndView("app/core/officeonline/previewonline");
        Account account = accountService.getRedisAUserInfoToAccount();
        try {
            if (StringUtils.isNotBlank(isNetdisk)) {
                NetDisk netDisk = new NetDisk();
                netDisk.setNetDiskId(netDiskId);
                netDisk.setRootPath(null);
                netDisk.setOrgId(account.getOrgId());
                netDisk = netDiskService.selectOneNetDisk(netDisk);
                String extName = attachId.substring(attachId.lastIndexOf("."), attachId.length());
                String extNameUP = extName.toUpperCase();
                if (extNameUP.equals(".PNG") || extNameUP.equals(".BMP") || extNameUP.equals(".JPG") || extNameUP.equals(".GIF") || extNameUP.equals(".TIFF") || extNameUP.equals(".JPEG")) {
                    html = "<img src='/sys/file/getImage?attachId=&netDiskId=" + netDiskId + "&path=" + attachId + "' width='100%'/>";
                } else {
                    Attach attach = new Attach();
                    attach.setPath(netDisk.getRootPath() + attachId);
                    attach.setExtName(extName);
                    html = attachService.previewOnLine(tmppath, attach,true);
                }
                mv.addObject("extName", extName);
                mv.addObject("filwName", attachId.substring(attachId.lastIndexOf(File.separator) + 1, attachId.length()));
            } else {
                Attach attach = new Attach();
                attach.setAttachId(attachId);
                attach = attachService.selectOne(attach);
                String extName = attach.getExtName();
                String extNameUP = extName.toUpperCase();
                if (extNameUP.equals(".PNG") || extNameUP.equals(".BMP") || extNameUP.equals(".JPG") || extNameUP.equals(".GIF") || extNameUP.equals(".TIFF") || extNameUP.equals(".JPEG")) {
                    html = "<img src='/sys/file/getImage?attachId="+ attachId + "' width='100%'/>";
                }else {
                    html = attachService.previewOnLine(tmppath, attach,false);
                }
                mv.addObject("filwName", attach.getOldName());
                mv.addObject("extName", attach.getExtName());
            }
            mv.addObject("content", html);
        } catch (Exception e) {
            //e.printStackTrace();
        }
        return mv;
    }

    /**
     * @param request
     * @return ModelAndView
     * @Title: goWebSiteseting
     * @Description:  常用网址
     */
    @RequestMapping("/oa/website")
    @RequiresPermissions("/app/core/oa/website")
    public ModelAndView goWebSite(HttpServletRequest request) {
        return new ModelAndView("app/core/sysset/website");
    }


    /**
     * @Title: goMobilesms
     * @Description:  平台手机短信
     * @param: request
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/oa/mobilesms")
    @RequiresPermissions("/app/core/oa/mobilesms")
    public ModelAndView goMobilesms(HttpServletRequest request) {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("app/core/sms/mobilesms");
        return mv;
    }

    /**
     * @Title: goFrame
     * @author:刘绍全
     * @Description: 登陆成功后的系统主页面
     * @param: request
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/frame")
    public ModelAndView goFrame(HttpServletRequest request,String view) {
        if(StringUtils.isBlank(view)) {
            AUserInfo aUserInfo = accountService.getRedisAUserInfo();
            ModelAndView mv = null;
            try {
                if (!aUserInfo.getAccountId().equals("admin")) {
                    String modeInfo = sysMonitorService.getMaintenanceModeStatus();
                    if (StringUtils.isNotBlank(modeInfo)) {
                        mv = new ModelAndView("maintenancemode");
                        mv.addObject("modeInfo", modeInfo);
                        return mv;
                    }
                }
                String[] privIdArr = aUserInfo.getUserPriv().split(",");
                String sysMenuIds = "";
                String mobilePrivIds = "";
                for (int i = 0; i < privIdArr.length; i++) {
                    if (StringUtils.isNotBlank(privIdArr[i])) {
                        UserPriv userPriv = userPrivService.getUserPrivByPrivId(privIdArr[i], aUserInfo.getOrgId());
                        sysMenuIds += userPriv.getUserPrivStr() + ",";
                        mobilePrivIds += userPriv.getMobilePriv() + ",";
                    }
                }
                if (SysTools.isMobileDevice(request)) {
                    List<String> appList = StrTools.strToList(mobilePrivIds);
                    JSONObject json = appConfigService.getMyAppListInfo(aUserInfo.getOrgId(), appList);
                    mv = new ModelAndView("app/mobile/main/index");
                    mv.addObject("r", System.currentTimeMillis());
                    mv.addObject("appMenuList", json);
                } else {
                    if (StringUtils.isBlank(theme)) {
                        mv = new ModelAndView("app/core/frame");
                    } else {
                        if (theme.equals("0")) {
                            //企业版主页
                            mv = new ModelAndView("app/core/frame");
                        } else if (theme.equals("1")) {
                            //政务版主页
                            mv = new ModelAndView("app/core/frame1");
                        } else if (theme.equals("2")) {
                            //教育版主页
                            mv = new ModelAndView("app/core/frame2");
                        }
                    }
                    List<String> sysMenuIdList = StrTools.strToList(sysMenuIds);
                    List<Map<String, String>> platformPrivList = userPrivService.getMyPlatformPrivList(aUserInfo.getOrgId(), aUserInfo.getAccountId());
                    String platformPrivTempStr = "";
                    if (platformPrivList.size() > 0) {
                        for (int i = 0; i < platformPrivList.size(); i++) {
                            platformPrivTempStr += platformPrivList.get(i).get("userPrivStr") + ",";
                        }
                    }
                    List<String> platformMenuIdList = StrTools.strToList(platformPrivTempStr);
                    List<SysMenu> sysMenuList = sysMenuService.getSysMenuByAccount(sysMenuIdList, platformMenuIdList, aUserInfo.getOrgId());
                    mv.addObject("sysMenuList", sysMenuList);
                    UserPriv userPriv = userPrivService.getUserPrivByPrivId(aUserInfo.getUserPriv(), aUserInfo.getOrgId());
                    mv.addObject("USER_INFO", userInfoService.getUserInfoByAccountId(aUserInfo.getAccountId(), aUserInfo.getOrgId()));
                    mv.addObject("USER_PRIV", userPriv);
                    mv.addObject("msgtime", msgtime);
                    mv.addObject("msgTip", aUserInfo.getMsgTip());
                    mv.addObject("msgAudio", aUserInfo.getMsgAudio());
                    mv.addObject("outLoginUrl", "/account/loginout");
                }
                return mv;
            } catch (Exception e) {
                return new ModelAndView("relogin");
            }
        }else if(view.equals("oa"))
        {
            AUserInfo aUserInfo = accountService.getRedisAUserInfo();
            ModelAndView mv = null;
            try {
                if (!aUserInfo.getAccountId().equals("admin")) {
                    String modeInfo = sysMonitorService.getMaintenanceModeStatus();
                    if (StringUtils.isNotBlank(modeInfo)) {
                        mv = new ModelAndView("maintenancemode");
                        mv.addObject("modeInfo", modeInfo);
                        return mv;
                    }
                }
                String[] privIdArr = aUserInfo.getUserPriv().split(",");
                String sysMenuIds = "";
                String mobilePrivIds = "";
                for (int i = 0; i < privIdArr.length; i++) {
                    if (StringUtils.isNotBlank(privIdArr[i])) {
                        UserPriv userPriv = userPrivService.getUserPrivByPrivId(privIdArr[i], aUserInfo.getOrgId());
                        sysMenuIds += userPriv.getUserPrivStr() + ",";
                        mobilePrivIds += userPriv.getMobilePriv() + ",";
                    }
                }
                if (SysTools.isMobileDevice(request)) {
                    List<String> appList = StrTools.strToList(mobilePrivIds);
                    JSONObject json = appConfigService.getMyAppListInfo(aUserInfo.getOrgId(), appList);
                    mv = new ModelAndView("app/mobile/main/index");
                    mv.addObject("r", System.currentTimeMillis());
                    mv.addObject("appMenuList", json);
                } else {
                    mv = new ModelAndView("app/core/oaframe");
                    List<String> sysMenuIdList = StrTools.strToList(sysMenuIds);
                    List<SysMenu> sysMenuList = sysMenuService.getOaSysMenuByAccount(sysMenuIdList, aUserInfo.getOrgId());
                    mv.addObject("sysMenuList", sysMenuList);
                    UserPriv userPriv = userPrivService.getUserPrivByPrivId(aUserInfo.getUserPriv(), aUserInfo.getOrgId());
                    mv.addObject("USER_INFO", userInfoService.getUserInfoByAccountId(aUserInfo.getAccountId(), aUserInfo.getOrgId()));
                    mv.addObject("USER_PRIV", userPriv);
                    mv.addObject("msgtime", msgtime);
                    mv.addObject("msgTip", aUserInfo.getMsgTip());
                    mv.addObject("msgAudio", aUserInfo.getMsgAudio());
                    mv.addObject("outLoginUrl", "/account/loginout");
                }
                return mv;
            } catch (Exception e) {
                e.printStackTrace();
                return new ModelAndView("relogin");
            }
        }else if(view.equals("wf"))
        {
            AUserInfo aUserInfo = accountService.getRedisAUserInfo();
            ModelAndView mv = null;
            try {
                if (!aUserInfo.getAccountId().equals("admin")) {
                    String modeInfo = sysMonitorService.getMaintenanceModeStatus();
                    if (StringUtils.isNotBlank(modeInfo)) {
                        mv = new ModelAndView("maintenancemode");
                        mv.addObject("modeInfo", modeInfo);
                        return mv;
                    }
                }
                String[] privIdArr = aUserInfo.getUserPriv().split(",");
                String sysMenuIds = "";
                String mobilePrivIds = "";
                for (int i = 0; i < privIdArr.length; i++) {
                    if (StringUtils.isNotBlank(privIdArr[i])) {
                        UserPriv userPriv = userPrivService.getUserPrivByPrivId(privIdArr[i], aUserInfo.getOrgId());
                        sysMenuIds += userPriv.getUserPrivStr() + ",";
                        mobilePrivIds += userPriv.getMobilePriv() + ",";
                    }
                }
                if (SysTools.isMobileDevice(request)) {
                    List<String> appList = StrTools.strToList(mobilePrivIds);
                    JSONObject json = appConfigService.getMyAppListInfo(aUserInfo.getOrgId(), appList);
                    mv = new ModelAndView("app/mobile/main/index");
                    mv.addObject("r", System.currentTimeMillis());
                    mv.addObject("appMenuList", json);
                } else {
                    mv = new ModelAndView("app/core/wfframe");
                    List<String> sysMenuIdList = StrTools.strToList(sysMenuIds);
                    List<SysMenu> sysMenuList = sysMenuService.getWfSysMenuByAccount(sysMenuIdList, aUserInfo.getOrgId());
                    mv.addObject("sysMenuList", sysMenuList);
                    UserPriv userPriv = userPrivService.getUserPrivByPrivId(aUserInfo.getUserPriv(), aUserInfo.getOrgId());
                    mv.addObject("USER_INFO", userInfoService.getUserInfoByAccountId(aUserInfo.getAccountId(), aUserInfo.getOrgId()));
                    mv.addObject("USER_PRIV", userPriv);
                    mv.addObject("msgtime", msgtime);
                    mv.addObject("msgTip", aUserInfo.getMsgTip());
                    mv.addObject("msgAudio", aUserInfo.getMsgAudio());
                    mv.addObject("outLoginUrl", "/account/loginout");
                }
                return mv;
            } catch (Exception e) {
                e.printStackTrace();
                return new ModelAndView("relogin");
            }
        }else
        {
            AUserInfo aUserInfo = accountService.getRedisAUserInfo();
            ModelAndView mv = null;
            try {
                if (!aUserInfo.getAccountId().equals("admin")) {
                    String modeInfo = sysMonitorService.getMaintenanceModeStatus();
                    if (StringUtils.isNotBlank(modeInfo)) {
                        mv = new ModelAndView("maintenancemode");
                        mv.addObject("modeInfo", modeInfo);
                        return mv;
                    }
                }
                String[] privIdArr = aUserInfo.getUserPriv().split(",");
                String sysMenuIds = "";
                String mobilePrivIds = "";
                for (int i = 0; i < privIdArr.length; i++) {
                    if (StringUtils.isNotBlank(privIdArr[i])) {
                        UserPriv userPriv = userPrivService.getUserPrivByPrivId(privIdArr[i], aUserInfo.getOrgId());
                        sysMenuIds += userPriv.getUserPrivStr() + ",";
                        mobilePrivIds += userPriv.getMobilePriv() + ",";
                    }
                }
                if (SysTools.isMobileDevice(request)) {
                    List<String> appList = StrTools.strToList(mobilePrivIds);
                    JSONObject json = appConfigService.getMyAppListInfo(aUserInfo.getOrgId(), appList);
                    mv = new ModelAndView("app/mobile/main/index");
                    mv.addObject("r", System.currentTimeMillis());
                    mv.addObject("appMenuList", json);
                } else {
                    if (StringUtils.isBlank(theme)) {
                        mv = new ModelAndView("app/core/frame");
                    } else {
                        if (theme.equals("0")) {
                            //企业版主页
                            mv = new ModelAndView("app/core/frame");
                        } else if (theme.equals("1")) {
                            //政务版主页
                            mv = new ModelAndView("app/core/frame1");
                        } else if (theme.equals("2")) {
                            //教育版主页
                            mv = new ModelAndView("app/core/frame2");
                        }
                    }
                    List<String> sysMenuIdList = StrTools.strToList(sysMenuIds);
                    List<Map<String, String>> platformPrivList = userPrivService.getMyPlatformPrivList(aUserInfo.getOrgId(), aUserInfo.getAccountId());
                    String platformPrivTempStr = "";
                    if (platformPrivList.size() > 0) {
                        for (int i = 0; i < platformPrivList.size(); i++) {
                            platformPrivTempStr += platformPrivList.get(i).get("userPrivStr") + ",";
                        }
                    }
                    List<String> platformMenuIdList = StrTools.strToList(platformPrivTempStr);
                    List<SysMenu> sysMenuList = sysMenuService.getSysMenuByAccount(sysMenuIdList, platformMenuIdList, aUserInfo.getOrgId());
                    mv.addObject("sysMenuList", sysMenuList);
                    UserPriv userPriv = userPrivService.getUserPrivByPrivId(aUserInfo.getUserPriv(), aUserInfo.getOrgId());
                    mv.addObject("USER_INFO", userInfoService.getUserInfoByAccountId(aUserInfo.getAccountId(), aUserInfo.getOrgId()));
                    mv.addObject("USER_PRIV", userPriv);
                    mv.addObject("msgtime", msgtime);
                    mv.addObject("msgTip", aUserInfo.getMsgTip());
                    mv.addObject("msgAudio", aUserInfo.getMsgAudio());
                    mv.addObject("outLoginUrl", "/account/loginout");
                }
                return mv;
            } catch (Exception e) {
                return new ModelAndView("relogin");
            }
        }
    }


    @RequestMapping("/frameim")
    public ModelAndView goFrameIm(HttpServletRequest request) {
        AUserInfo aUserInfo = accountService.getRedisAUserInfo();
        ModelAndView mv = null;
        try {
            if (!aUserInfo.getAccountId().equals("admin")) {
                String modeInfo = sysMonitorService.getMaintenanceModeStatus();
                if (StringUtils.isNotBlank(modeInfo)) {
                    mv = new ModelAndView("maintenancemode");
                    mv.addObject("modeInfo", modeInfo);
                    return mv;
                }
            }
            String[] privIdArr = aUserInfo.getUserPriv().split(",");
            String sysMenuIds = "";
            String mobilePrivIds = "";
            for (int i = 0; i < privIdArr.length; i++) {
                if (StringUtils.isNotBlank(privIdArr[i])) {
                    UserPriv userPriv = userPrivService.getUserPrivByPrivId(privIdArr[i], aUserInfo.getOrgId());
                    sysMenuIds += userPriv.getUserPrivStr() + ",";
                    mobilePrivIds += userPriv.getMobilePriv() + ",";
                }
            }
            if (SysTools.isMobileDevice(request)) {
                List<String> appList = StrTools.strToList(mobilePrivIds);
                JSONObject json = appConfigService.getMyAppListInfo(aUserInfo.getOrgId(), appList);
                mv = new ModelAndView("app/mobile/main/index");
                mv.addObject("r", System.currentTimeMillis());
                mv.addObject("appMenuList", json);
            } else {
                if (StringUtils.isBlank(theme)) {
                    mv = new ModelAndView("app/core/frameim");
                } else {
                    if (theme.equals("0")) {
                        //企业版主页
                        mv = new ModelAndView("app/core/frameim");
                    } else if (theme.equals("1")) {
                        //政务版主页
                        mv = new ModelAndView("app/core/frameim1");
                    } else if (theme.equals("2")) {
                        //教育版主页
                        mv = new ModelAndView("app/core/frameim2");
                    }
                }
                List<String> sysMenuIdList = StrTools.strToList(sysMenuIds);
                List<Map<String, String>> platformPrivList = userPrivService.getMyPlatformPrivList(aUserInfo.getOrgId(), aUserInfo.getAccountId());
                String platformPrivTempStr = "";
                if (platformPrivList.size() > 0) {
                    for (int i = 0; i < platformPrivList.size(); i++) {
                        platformPrivTempStr += platformPrivList.get(i).get("userPrivStr") + ",";
                    }
                }
                List<String> platformMenuIdList = StrTools.strToList(platformPrivTempStr);
                List<SysMenu> sysMenuList = sysMenuService.getSysMenuByAccount(sysMenuIdList, platformMenuIdList, aUserInfo.getOrgId());
                mv.addObject("sysMenuList", sysMenuList);
                UserPriv userPriv = userPrivService.getUserPrivByPrivId(aUserInfo.getUserPriv(), aUserInfo.getOrgId());
                mv.addObject("USER_INFO", userInfoService.getUserInfoByAccountId(aUserInfo.getAccountId(), aUserInfo.getOrgId()));
                mv.addObject("USER_PRIV", userPriv);
                mv.addObject("msgtime", msgtime);
                mv.addObject("msgTip", aUserInfo.getMsgTip());
                mv.addObject("msgAudio", aUserInfo.getMsgAudio());
                mv.addObject("outLoginUrl", "/account/loginoutim");
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("relogin");
        }
    }

    /**
     * @return ModelAndView
     * @Title goDesk
     * @Description  个人工作台
     */
    @RequestMapping("/desk")
    public ModelAndView goDesk(String view, String profileId) {
        ModelAndView mv = null;
        try {
            AUserInfo aUserInfo = accountService.getRedisAUserInfo();
            List<Map<String, String>> sysProfileList = sysProfileService.getMySysProfileList(aUserInfo.getOrgId(), aUserInfo.getOpFlag(), aUserInfo.getAccountId(), aUserInfo.getDeptId(), aUserInfo.getLeadLevel());
            if (StringUtils.isBlank(view)) {
                if (sysProfileList.size() > 0) {
                    view = sysProfileList.get(0).get("url");
                    profileId = sysProfileList.get(0).get("profileId");
                }
            }
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/portal/desk");
            } else {
                if (view.equals("auto")) {
                    mv = new ModelAndView("/app/core/portal/autodesk");
                    SysProfile sysProfile = new SysProfile();
                    sysProfile.setProfileId(profileId);
                    sysProfile.setOrgId(aUserInfo.getOrgId());
                    sysProfile = sysProfileService.selectOneSysProfile(sysProfile);
                    mv.addObject("bodyhtml", sysProfile.getLayout());
                } else if (view.equals("vehicle")) {
                    mv = new ModelAndView("/app/core/portal/deskvehicle");
                } else if (view.equals("finance")) {
                    mv = new ModelAndView("/app/core/portal/deskforfinance");
                } else if (view.equals("hr")) {
                    mv = new ModelAndView("/app/core/portal/deskforhr");
                } else if (view.equals("com")) {
                    mv = new ModelAndView("/app/core/portal/deskforcom");
                } else if (view.equals("bi")) {
                    mv = new ModelAndView("/app/core/portal/deskforbi");
                } else if (view.equals("project")) {
                    mv = new ModelAndView("/app/core/portal/deskforproject");
                } else if (view.equals("desk")) {
                    mv = new ModelAndView("/app/core/portal/desk");
                } else if (view.equals("bigscreen")) {
                    mv = new ModelAndView("/app/core/portal/bigscreen");
                } else if (view.equals("governmentinfo")) {
                    mv = new ModelAndView("/app/core/portal/governmentinfo");
                } else if (view.equals("superversion")) {
                    mv = new ModelAndView("/app/core/portal/superversion");
                }
            }
            mv.addObject("view", view);
            mv.addObject("profilelist", sysProfileList);
            mv.addObject("msgtime",msgtime);
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param @param  request
     * @param @return 设定文件
     * @return ModelAndView 返回类型
     * @Title: mydesk
     * @Description:  个人门户
     */
    @RequestMapping("/mydesk")
    @RequiresPermissions("/app/core/mydesk")
    public ModelAndView mydesk(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/deskmodule/mydesk");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


    /**
     * @param @param  request
     * @param @return 设定文件
     * @return ModelAndView 返回类型
     * @Title: goPublicfile
     * @Description:  公共文件柜
     */
    @RequestMapping("/file/publicfile")
    @RequiresPermissions("/app/core/file/publicfile")
    public ModelAndView goPublicfile(HttpServletRequest request) {
        return new ModelAndView("app/core/fileshare/public/index");
    }


    /**
     * @param request
     * @return ModelAndView
     * @Title gonetdiskset
     * @Description  网盘设置
     */
    @RequestMapping("/file/netdiskset")
    @RequiresPermissions("/app/core/file/netdiskset")
    public ModelAndView gonetdiskset(HttpServletRequest request) {
        return new ModelAndView("app/core/fileshare/netdisk/seting");
    }

    /**
     * @param request
     * @return ModelAndView
     * @Title netdisk
     * @Description  网盘
     */
    @RequestMapping("/file/netdisk")
    @RequiresPermissions("/app/core/file/netdisk")
    public ModelAndView gonetdisk(HttpServletRequest request) {
        return new ModelAndView("app/core/fileshare/netdisk/netdisk");
    }

    /**
     * @param request
     * @return ModelAndView
     * @Title goFolderset
     * @Description  公共文件柜设置
     */
    @RequestMapping("/folderset")
    @RequiresPermissions("/app/core/folderset")
    public ModelAndView goFolderset(HttpServletRequest request) {
        return new ModelAndView("app/core/fileshare/public/folderset");
    }

    @RequestMapping("/personalfile")
    @RequiresPermissions("/app/core/personalfile")
    public ModelAndView personalfile(HttpServletRequest request) {
        return new ModelAndView("app/core/fileshare/personal/myfile");
    }

    /**
     * @param @param  request
     * @param @return 设定文件
     * @return ModelAndView 返回类型
     * @Title: readphoto
     * @Description:  查看图片
     */
    @RequestMapping("/file/photo")
    @RequiresPermissions("/app/core/file/photo")
    public ModelAndView readphoto(HttpServletRequest request) {
        return new ModelAndView("app/core/fileshare/photo/index");
    }

    /**
     * @param @param  request
     * @param @return 设定文件
     * @return ModelAndView 返回类型
     * @Title: setphoto
     * @Description: 设置相册
     */
    @RequestMapping("/file/setphoto")
    @RequiresPermissions("/app/core/file/setphoto")
    public ModelAndView setphoto(HttpServletRequest request) {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("app/core/fileshare/photo/photoset");
        return mv;
    }

    /**
     * @param request
     * @return ModelAndView
     * @Title goPersonseting
     * @Description  个人信息设置
     */
    @RequestMapping("/personseting")
    @RequiresPermissions("/app/core/personseting")
    public ModelAndView goPersonseting(HttpServletRequest request, String view) {
        Account account = accountService.getRedisAUserInfoToAccount();
        SysConfigInit sysConfigInit = new SysConfigInit();
        sysConfigInit.setOrgId(account.getOrgId());
        sysConfigInit = sysConfigInitService.selectOneSysConfigInit(sysConfigInit);
        ModelAndView mv = new ModelAndView();
        mv.setViewName("app/core/personal/personseting");
        mv.addObject("sysConfig", sysConfigInit);
        mv.addObject("view", view);
        return mv;
    }

    /**
     * @param request
     * @return ModelAndView
     * @Title goMysms
     * @Description  我的站内消息
     */
    @RequestMapping("/oa/sms")
    @RequiresPermissions("/app/core/oa/sms")
    public ModelAndView goMysms(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/sms/sms");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goLeadScheduleMange
     * @Description:  领导行程管理
     */
    @RequestMapping("/leadactivity/manage")
    @RequiresPermissions("/app/core/leadactivity/manage")
    public ModelAndView goLeadActivityMange(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/leadactivity/manage");
            } else {
                mv = new ModelAndView("app/core/leadactivity/input");
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goLeadActivityDetails
     * @Description:  领导行程详情
     */
    @RequestMapping("/leadactivity/details")
    @RequiresPermissions("/app/core/leadactivity/details")
    public ModelAndView goLeadActivityDetails(HttpServletRequest request) {
        ModelAndView mv = null;
        try {
            if (SysTools.isMobileDevice(request)) {
                mv = new ModelAndView("/app/mobile/main/leadactivity/details");
            } else {
                mv = new ModelAndView("app/core/leadactivity/details");
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goLeadActivityquery
     * @Description:  领导行程查询
     */
    @RequestMapping("/leadactivity/query")
    @RequiresPermissions("/app/core/leadactivity/query")
    public ModelAndView goLeadActivityquery() {
        try {
            return new ModelAndView("app/core/leadactivity/query");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goMemo
     * @Description:  个人便签
     */
    @RequestMapping("/oa/memo")
    @RequiresPermissions("/app/core/oa/memo")
    public ModelAndView goMemo() {
        try {
            return new ModelAndView("app/core/sms/memo");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


}
