package com.core136.controller.sys;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.core136.bean.account.AUserInfo;
import com.core136.bean.account.Account;
import com.core136.bean.account.UserInfo;
import com.core136.bean.account.UserPriv;
import com.core136.bean.oa.Shortcut;
import com.core136.bean.sys.*;
import com.core136.common.enums.AppGobalConstant;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.account.UserPrivService;
import com.core136.service.oa.ShortcutService;
import com.core136.service.sys.*;
import com.core136.unit.dbunit.MysqlToOrcaleService;
import com.core136.unit.fileutils.FileTools;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: RouteGetSysController
 * @Description: 系统相关的接口
 * @author: 稠云信息
 * @date: 2019年1月3日 上午9:28:48
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@RestController
@RequestMapping("/ret/sysget")
public class RouteGetSysController {
    @Value("${logging.file.name}")
    private String logFilePath;
    @Value("${app.isdev}")
    private String isDev;
    @Value("${app.verifycode}")
    private String verifyCode;
    @Value("${app.orgid}")
    private String appOrgId;
    @Value("${app.login.errorcount}")
    private String errorcount;
    @Value("${app.login.resettime}")
    private String resettime;

    private SmsService smsService;

    @Autowired
    public void setSmsService(SmsService smsService) {
        this.smsService = smsService;
    }

    private SysDbSourceService sysDbSourceService;

    @Autowired
    public void setSysDbSourceService(SysDbSourceService sysDbSourceService) {
        this.sysDbSourceService = sysDbSourceService;
    }

    private CodeClassService codeClassService;

    @Autowired
    public void setCodeClassService(CodeClassService codeClassService) {
        this.codeClassService = codeClassService;
    }

    private SmsConfigService smsConfigService;

    @Autowired
    public void setSmsConfigService(SmsConfigService smsConfigService) {
        this.smsConfigService = smsConfigService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private SysDeskConfigService sysDeskConfigService;

    @Autowired
    public void setSysDeskConfigService(SysDeskConfigService sysDeskConfigService) {
        this.sysDeskConfigService = sysDeskConfigService;
    }

    private SysMonitorService sysMonitorService;

    @Autowired
    public void setSysMonitorService(SysMonitorService sysMonitorService) {
        this.sysMonitorService = sysMonitorService;
    }

    private SysLogService sysLogService;

    @Autowired
    public void setSysLogService(SysLogService sysLogService) {
        this.sysLogService = sysLogService;
    }

    private SysOrgConfigService sysOrgConfigService;

    @Autowired
    public void setSysOrgConfigService(SysOrgConfigService sysOrgConfigService) {
        this.sysOrgConfigService = sysOrgConfigService;
    }

    private SysInterfaceService sysInterfaceService;

    @Autowired
    public void setSysInterfaceService(SysInterfaceService sysInterfaceService) {
        this.sysInterfaceService = sysInterfaceService;
    }

    private AppConfigService appConfigService;

    @Autowired
    public void setAppConfigService(AppConfigService appConfigService) {
        this.appConfigService = appConfigService;
    }

    private UserPrivService userPrivService;

    @Autowired
    public void setUserPrivService(UserPrivService userPrivService) {
        this.userPrivService = userPrivService;
    }

    private SysMenuService sysMenuService;

    @Autowired
    public void setSysMenuService(SysMenuService sysMenuService) {
        this.sysMenuService = sysMenuService;
    }

    private ShortcutService shortcutService;

    @Autowired
    public void setShortcutService(ShortcutService shortcutService) {
        this.shortcutService = shortcutService;
    }

    private SysTimingTaskService sysTimingTaskService;

    @Autowired
    public void setSysTimingTaskService(SysTimingTaskService sysTimingTaskService) {
        this.sysTimingTaskService = sysTimingTaskService;
    }

    private SysProfileService sysProfileService;

    @Autowired
    public void setSysProfileService(SysProfileService sysProfileService) {
        this.sysProfileService = sysProfileService;
    }

    private SysWebSiteService sysWebSiteService;

    @Autowired
    public void setSysWebSiteService(SysWebSiteService sysWebSiteService) {
        this.sysWebSiteService = sysWebSiteService;
    }

    private SysApplicationService sysApplicationService;

    @Autowired
    public void setSysApplicationService(SysApplicationService sysApplicationService) {
        this.sysApplicationService = sysApplicationService;
    }

    private SysApplicationAccountService sysApplicationAccountService;

    @Autowired
    public void setSysApplicationAccountService(SysApplicationAccountService sysApplicationAccountService) {
        this.sysApplicationAccountService = sysApplicationAccountService;
    }

    private SysThreeMemberService sysThreeMemberService;

    @Autowired
    public void setSysThreeMemberService(SysThreeMemberService sysThreeMemberService) {
        this.sysThreeMemberService = sysThreeMemberService;
    }

    private ThreeEventRecordService threeEventRecordService;

    @Autowired
    public void setThreeEventRecordService(ThreeEventRecordService threeEventRecordService) {
        this.threeEventRecordService = threeEventRecordService;
    }

    private ThreeEventApprovalService threeEventApprovalService;

    @Autowired
    public void setThreeEventApprovalService(ThreeEventApprovalService threeEventApprovalService) {
        this.threeEventApprovalService = threeEventApprovalService;
    }

    private FullTextConfigService fullTextConfigService;

    @Autowired
    public void setFullTextConfigService(FullTextConfigService fullTextConfigService) {
        this.fullTextConfigService = fullTextConfigService;
    }

    private SysConfigInitService sysConfigInitService;

    @Autowired
    public void setSysConfigInitService(SysConfigInitService sysConfigInitService) {
        this.sysConfigInitService = sysConfigInitService;
    }

    private MysqlToOrcaleService mysqlToOrcaleService;

    @Autowired
    private void setMysqlToOrcaleService(MysqlToOrcaleService mysqlToOrcaleService) {
        this.mysqlToOrcaleService = mysqlToOrcaleService;
    }

    /**
     * 获取MYSQL转ORCALE的SQL修正脚本
     */
    @RequestMapping(value = "/getMySqlToOracleScript", method = RequestMethod.GET)
    public void getMySqlToOracleScript(String scriptSavePath, String oracleSchema, String tableSchema) {
        try {
            mysqlToOrcaleService.getColAlterStr(scriptSavePath, oracleSchema, tableSchema);
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    /**
     * 获取按权限获取菜单
     *
     * @return
     */
    @RequestMapping(value = "/getSysMenuByAccount", method = RequestMethod.POST)
    public RetDataBean getSysMenuByAccount() {
        try {
            AUserInfo aUserInfo = accountService.getRedisAUserInfo();
            String[] privIdArr = aUserInfo.getUserPriv().split(",");
            String sysMenuIds = "";
            for (int i = 0; i < privIdArr.length; i++) {
                if (StringUtils.isNotBlank(privIdArr[i])) {
                    UserPriv userPriv = userPrivService.getUserPrivByPrivId(privIdArr[i], aUserInfo.getOrgId());
                    sysMenuIds += userPriv.getUserPrivStr() + ",";
                }
            }
            List<String> sysMenuIdList = StrTools.strToList(sysMenuIds);
            List<Map<String, String>> platformPrivList = userPrivService.getMyPlatformPrivList(aUserInfo.getOrgId(), aUserInfo.getAccountId());
            String platformPrivTempStr = "";
            if (platformPrivList.size() > 0) {
                for (int i = 0; i < platformPrivList.size(); i++) {
                    platformPrivTempStr += platformPrivList.get(i).get("userPrivStr") + ",";
                }
            }
            List<String> platformMenuIdList = StrTools.strToList(platformPrivTempStr);
            List<SysMenu> sysMenuList = sysMenuService.getSysMenuByAccount(sysMenuIdList, platformMenuIdList, aUserInfo.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, sysMenuList);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 获取登陆界面信息
     *
     * @return
     */
    @RequestMapping(value = "/getInitInfo", method = RequestMethod.POST)
    public RetDataBean getInitInfo() {
        try {
            SysInterface sysInterface = new SysInterface();
            sysInterface = sysInterfaceService.selectOneSysInterface(sysInterface);
            SysConfigInit sysConfigInit = new SysConfigInit();
            sysConfigInit.setOrgId(appOrgId);
            sysConfigInit = sysConfigInitService.selectOneSysConfigInit(sysConfigInit);
            if (StringUtils.isBlank(sysInterface.getSysTitle())) {
                sysInterface.setSysTitle(AppGobalConstant.SOFT_NAME);
            }
            JSONArray jsonArray = new JSONArray();
            if (StringUtils.isNotBlank(sysInterface.getBackgroundImg())) {
                String backGroundStr = sysInterface.getBackgroundImg();
                String[] arr = backGroundStr.split("\\*");
                for (int i = 0; i < arr.length; i++) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("image", "/sys/file/getBackgroundImg?fileName=" + arr[i]);
                    jsonArray.add(jsonObject);
                }
            }
            Map<String, Object> returnMap = new HashMap<String, Object>();
            if (jsonArray.size() == 0) {
                returnMap.put("imArr", "");
            } else {
                returnMap.put("imgArr", jsonArray);
            }
            returnMap.put("SOFT_NAME", AppGobalConstant.SOFT_NAME);
            returnMap.put("sysInterface", sysInterface);
            returnMap.put("isDev", isDev);
            returnMap.put("verifyCode", verifyCode);
            returnMap.put("appClientFlag", sysConfigInit.getAppClientFlag());
            returnMap.put("langFlag", sysConfigInit.getLangFlag());
            returnMap.put("remUserPass",sysConfigInit.getRemUserPass());
            returnMap.put("findPass",sysConfigInit.getFindPass());
            returnMap.put("errorcount",errorcount);
            returnMap.put("resettime",resettime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, returnMap);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取corn表代式未来运行时机
     *
     * @param cron
     * @param resSize
     * @return
     */
    @RequestMapping(value = "/cronCalculation", method = RequestMethod.POST)
    public RetDataBean cronCalculation(String cron, int resSize) {
        try {
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, SysTools.getCronCalculation(cron,resSize));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取全文检索配置
     *
     * @return
     */
    @RequestMapping(value = "/getFullTextConfig", method = RequestMethod.POST)
    public RetDataBean getFullTextConfig() {
        try {
            FullTextConfig fullTextConfig = new FullTextConfig();
            Account account = accountService.getRedisAUserInfoToAccount();
            fullTextConfig.setOrgId(account.getOrgId());
            fullTextConfig = fullTextConfigService.selectOneFullTextConfig(fullTextConfig);
            if (fullTextConfig == null) {
                fullTextConfig = new FullTextConfig();
                fullTextConfig.setConfigId(SysTools.getGUID());
                fullTextConfig.setEmailFlag("0");
                fullTextConfig.setNewsFlag("0");
                fullTextConfig.setNoticeFlag("0");
                fullTextConfig.setArchivesFlag("0");
                fullTextConfig.setBpmFlag("0");
                fullTextConfig.setDocumentFlag("0");
                fullTextConfig.setTaskFlag("0");
                fullTextConfig.setArchivesFlag("0");
                fullTextConfig.setMeetingFlag("0");
                fullTextConfig.setHrFlag("0");
                fullTextConfig.setProjectFlag("0");
                fullTextConfig.setSuperversionFlag("0");
                fullTextConfig.setWorkplanFlag("0");
                fullTextConfig.setPublicfileFlag("0");
                fullTextConfig.setContractFlag("0");
                fullTextConfig.setFilemapFlag("0");
                fullTextConfig.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
                fullTextConfig.setCreateUser(account.getAccountId());
                fullTextConfig.setOrgId(account.getOrgId());
                fullTextConfigService.insertFullTextConfig(fullTextConfig);
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, fullTextConfig);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @param optType
     * @param module
     * @return RetDataBean
     * @Title: getThreeEventLogList
     * @Description:  系统三员审批事件
     */
    @RequestMapping(value = "/getThreeEventLogList", method = RequestMethod.POST)
    public RetDataBean getThreeEventLogList(PageParam pageParam, String beginTime, String endTime, String optType, String module) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("a.create_time");
            } else {
                pageParam.setSort("a." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = threeEventApprovalService.getThreeEventLogList(pageParam, beginTime, endTime, optType, module);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param status
     * @param beginTime
     * @param endTime
     * @param optType
     * @param module
     * @return RetDataBean
     * @Title: getThreeEventApprovalOldList
     * @Description:  获取所有三员审批事件列表
     */
    @RequestMapping(value = "/getThreeEventApprovalOldList", method = RequestMethod.POST)
    public RetDataBean getThreeEventApprovalOldList(PageParam pageParam, String status, String beginTime, String endTime, String optType, String module) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("r.create_time");
            } else {
                pageParam.setSort("r." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = threeEventRecordService.getThreeEventApprovalOldList(pageParam, status, beginTime, endTime, optType, module);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @return RetDataBean
     * @Title: getThreeEventApprovalList
     * @Description:  获取三员审批事件列表
     */
    @RequestMapping(value = "/getThreeEventApprovalList", method = RequestMethod.POST)
    public RetDataBean getThreeEventApprovalList(PageParam pageParam, String beginTime, String endTime, String optType, String module) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("r.create_time");
            } else {
                pageParam.setSort("r." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = threeEventRecordService.getThreeEventApprovalList(pageParam, beginTime, endTime, optType, module);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param sysThreeMember
     * @return RetDataBean
     * @Title: getSystemLogList
     * @Description:  获取系统运行日志文件列表
     */
    @RequestMapping(value = "/getSystemLogList", method = RequestMethod.POST)
    public RetDataBean getSystemLogList(SysThreeMember sysThreeMember) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            sysThreeMember.setOrgId(account.getOrgId());
            File file = new File(logFilePath);
            String path = file.getParentFile().getAbsolutePath();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, FileTools.getFileList(path));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param sysThreeMember
     * @return RetDataBean
     * @Title: getSysThreeMember
     * @Description:  获取当前三员管理配置信息
     */
    @RequestMapping(value = "/getSysThreeMember", method = RequestMethod.POST)
    public RetDataBean getSysThreeMember(SysThreeMember sysThreeMember) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            sysThreeMember.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, sysThreeMemberService.selectOneSysThreeMember(sysThreeMember));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取账号列表
     *
     * @param pageParam
     * @param oaAccountId
     * @param status
     * @param applicationId
     * @return
     */
    @RequestMapping(value = "/getApplicationAccountList", method = RequestMethod.POST)
    public RetDataBean getApplicationAccountList(PageParam pageParam, String oaAccountId, String status, String applicationId) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("a.sort_no");
            } else {
                pageParam.setSort("a." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = sysApplicationAccountService.getApplicationAccountList(pageParam, oaAccountId, applicationId, status);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getApplicationListForSelect
     * @Description:  获取第三方应用列表
     */
    @RequestMapping(value = "/getApplicationListForSelect", method = RequestMethod.POST)
    public RetDataBean getApplicationListForSelect() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, sysApplicationService.getApplicationListForSelect(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageNumber
     * @param pageSize
     * @param search
     * @param sort
     * @param sortOrder
     * @return RetDataBean
     * @Title: getSysApplicationList
     * @Description:  获取单点登陆系统列表
     */
    @RequestMapping(value = "/getSysApplicationList", method = RequestMethod.POST)
    public RetDataBean getSysApplicationList(
            Integer pageNumber,
            Integer pageSize,
            String search,
            String sort,
            String sortOrder
    ) {
        try {
            if (StringUtils.isBlank(sort)) {
                sort = "sort_no";
            } else {
                sort = StrTools.lowerCharToUnderLine(sort);
            }
            if (StringUtils.isBlank(sortOrder)) {
                sortOrder = "asc";
            }
            String orderBy = sort + " " + sortOrder;
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(SysDbSource.class);
            example.setOrderByClause(orderBy);
            Criteria criteria = example.createCriteria();
            if (!account.getOpFlag().equals("1")) {
                criteria.andEqualTo("createUser", account.getAccountId());
            }
            criteria.andEqualTo("orgId", account.getOrgId());
            if (StringUtils.isNotEmpty(search)) {
                Criteria criteria2 = example.createCriteria();
                criteria2.orLike("applicationId", "%" + search + "%").orLike("applicationName", "%" + search + "%").orLike("linkUrl", "%" + search + "%");
                example.and(criteria2);
            }
            PageInfo<SysApplication> pageInfo = sysApplicationService.getSysApplicationList(example, pageNumber, pageSize);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param sysApplicationAccount
     * @return RetDataBean
     * @Title: getSysApplicationAccountById
     * @Description:  获取单登陆账号详情
     */
    @RequestMapping(value = "/getSysApplicationAccountById", method = RequestMethod.POST)
    public RetDataBean getSysApplicationAccountById(SysApplicationAccount sysApplicationAccount) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            sysApplicationAccount.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, sysApplicationAccountService.selectOneSysApplicationAccount(sysApplicationAccount));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param sysApplication
     * @return RetDataBean
     * @Title: getSysApplicationById
     * @Description:  获取单点登陆系统详情
     */
    @RequestMapping(value = "/getSysApplicationById", method = RequestMethod.POST)
    public RetDataBean getSysApplicationById(SysApplication sysApplication) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            sysApplication.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, sysApplicationService.selectOneSysApplication(sysApplication));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getMySysWebSiteList
     * @Description:  获取我的常用网址
     */
    @RequestMapping(value = "/getMySysWebSiteList", method = RequestMethod.POST)
    public RetDataBean getMySysWebSiteList() {
        try {
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, sysWebSiteService.getMySysWebSiteList(userInfo.getOrgId(), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getLeadLevel()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageNumber
     * @param pageSize
     * @param search
     * @param sort
     * @param sortOrder
     * @return RetDataBean
     * @Title: getSysWebSite
     * @Description:  获取常用网址列表
     */
    @RequestMapping(value = "/getSysWebSite", method = RequestMethod.POST)
    public RetDataBean getSysWebSite(
            Integer pageNumber,
            Integer pageSize,
            String search,
            String sort,
            String sortOrder
    ) {
        try {
            if (StringUtils.isBlank(sort)) {
                sort = "sort_no";
            } else {
                sort = StrTools.lowerCharToUnderLine(sort);
            }
            if (StringUtils.isBlank(sortOrder)) {
                sortOrder = "asc";
            }
            String orderBy = sort + " " + sortOrder;
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(SysWebSite.class);
            example.setOrderByClause(orderBy);
            Criteria criteria = example.createCriteria();
            criteria.andEqualTo("orgId", account.getOrgId());
            if (StringUtils.isNotEmpty(search)) {
                Criteria criteria2 = example.createCriteria();
                criteria2.orLike("webTitle", "%" + search + "%").orLike("webSite", "%" + search + "%");
                example.and(criteria2);
            }
            PageInfo<SysWebSite> pageInfo = sysWebSiteService.getSysWebSite(example, pageNumber, pageSize);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param sysWebSite
     * @return RetDataBean
     * @Title: getSysWebSiteById
     * @Description:  获取常用网址详情
     */
    @RequestMapping(value = "/getSysWebSiteById", method = RequestMethod.POST)
    public RetDataBean getSysWebSiteById(SysWebSite sysWebSite) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            sysWebSite.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, sysWebSiteService.selectOneSysWebSite(sysWebSite));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @return
     * @Title: getAllAppList
     * @Description:   获取所有的APP应用
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getAllAppList", method = RequestMethod.POST)
    public RetDataBean getAllAppList() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, appConfigService.getAllAppList(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param appConfig
     * @param @return
     * @Title: getAppConfigById
     * @Description:  获取APP详情
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getAppConfigById", method = RequestMethod.POST)
    public RetDataBean getAppConfigById(AppConfig appConfig) {
        try {
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, appConfigService.selectOne(appConfig));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param pageNumber
     * @param pageSize
     * @param search
     * @param sort
     * @param sortOrder
     * @param @return
     * @Title: getAppConfigList
     * @Description:  获取APP应用列表
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getAppConfigList", method = RequestMethod.POST)
    public RetDataBean getAppConfigList(
            Integer pageNumber,
            Integer pageSize,
            String search,
            String sort,
            String sortOrder
    ) {
        try {
            if (StringUtils.isBlank(sort)) {
                sort = "sort_no";
            } else {
                sort = StrTools.lowerCharToUnderLine(sort);
            }
            if (StringUtils.isBlank(sortOrder)) {
                sortOrder = "asc";
            }
            String orderBy = sort + " " + sortOrder;
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(AppConfig.class);
            example.setOrderByClause(orderBy);
            Criteria criteria = example.createCriteria();
            if (!account.getOpFlag().equals("1")) {
                criteria.andEqualTo("createUser", account.getAccountId());
            }
            criteria.andEqualTo("orgId", account.getOrgId());
            if (StringUtils.isNotEmpty(search)) {
                Criteria criteria2 = example.createCriteria();
                criteria2.orLike("title", "%" + search + "%").orLike("module", "%" + search + "%");
                example.and(criteria2);
            }
            PageInfo<AppConfig> pageInfo = appConfigService.getAppConfigList(example, pageNumber, pageSize);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param @return
     * @Title: getSysInterface
     * @Description:  获取系统界面设置
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getSysInterface", method = RequestMethod.POST)
    public RetDataBean getSysInterface() {
        try {
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, sysInterfaceService.selectOneSysInterface(null));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param pageParam
     * @param @return
     * @Title: getOrgConfigLsit
     * @Description:  获取机构列表
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getOrgConfigLsit", method = RequestMethod.POST)
    public RetDataBean getOrgConfigLsit(
            PageParam pageParam
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("sort_no");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = sysOrgConfigService.getOrgConfigLsit(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param pageParam
     * @param eventType
     * @param beginTime
     * @param endTime
     * @param @return
     * @Title: getAllSysLogList
     * @Description:  获取日志列表
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getAllSysLogList", method = RequestMethod.POST)
    public RetDataBean getAllSysLogList(
            PageParam pageParam,
            String eventType,
            String beginTime,
            String endTime,
            String createUser
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("s.create_time");
            } else {
                pageParam.setSort("s." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(createUser);
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = sysLogService.getSysLogList(pageParam, eventType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param eventType
     * @param beginTime
     * @param endTime
     * @param @return
     * @Title: getMySysLogList
     * @Description:  获取个人的系统日志
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getMySysLogList", method = RequestMethod.POST)
    public RetDataBean getMySysLogList(
            PageParam pageParam,
            String eventType,
            String beginTime,
            String endTime
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("s.create_time");
            } else {
                pageParam.setSort("s." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = sysLogService.getSysLogList(pageParam, eventType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @return RetDataBean
     * @Title getNoReadSms
     * @Description  获取前10未读消息
     */
    @RequestMapping(value = "/getNoReadSms", method = RequestMethod.POST)
    public RetDataBean getNoReadSms() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, smsService.getNoReadSms(account.getAccountId(), account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title getMySubordinates
     * @Description  获取本人下属列表
     */
    @RequestMapping(value = "/getMySubordinates", method = RequestMethod.POST)
    public RetDataBean getMySubordinates() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, accountService.getMySubordinates(account.getOrgId(), account.getAccountId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title getNoReadSmsCount
     * @Description  获取个人未读消息的总数
     */
    @RequestMapping(value = "/getNoReadSmsCount", method = RequestMethod.POST)
    public RetDataBean getNoReadSmsCount() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, smsService.getNoReadSmsCount(account.getAccountId(), account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title getDbSourceList
     * @Description  获取下拉菜单的数据源列表
     */
    @RequestMapping(value = "/getDbSourceList", method = RequestMethod.POST)
    public RetDataBean getDbSourceList() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, sysDbSourceService.getDbSourceList(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取系统所有的门户列表
     * @param pageParam
     * @return
     */
    @RequestMapping(value = "/getAllSysProfileList", method = RequestMethod.POST)
    public RetDataBean getAllSysProfileList(PageParam pageParam) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("sort_no");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            pageParam.setOrgId(account.getOrgId());
            PageInfo<Map<String, String>> pageInfo = sysProfileService.getAllSysProfileList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取当数据源列表
     * @param pageNumber
     * @param pageSize
     * @param search
     * @param sort
     * @param sortOrder
     * @return
     */
    @RequestMapping(value = "/getSysDbSourctList", method = RequestMethod.POST)
    public RetDataBean getSysDbSourctList(
            Integer pageNumber,
            Integer pageSize,
            String search,
            String sort,
            String sortOrder
    ) {
        try {
            if (StringUtils.isBlank(sort)) {
                sort = "sort_no";
            } else {
                sort = StrTools.lowerCharToUnderLine(sort);
            }
            if (StringUtils.isBlank(sortOrder)) {
                sortOrder = "asc";
            }
            String orderBy = sort + " " + sortOrder;
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(SysDbSource.class);
            example.setOrderByClause(orderBy);
            Criteria criteria = example.createCriteria();
            if (!account.getOpFlag().equals("1")) {
                criteria.andEqualTo("createUser", account.getAccountId());
            }
            criteria.andEqualTo("orgId", account.getOrgId());
            if (StringUtils.isNotEmpty(search)) {
                Criteria criteria2 = example.createCriteria();
                criteria2.orLike("dbSourctName", "%" + search + "%").orLike("dbLink", "%" + search + "%");
                example.and(criteria2);
            }
            PageInfo<SysDbSource> pageInfo = sysDbSourceService.getSysDbSourctList(example, pageNumber, pageSize);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param sysDbSource
     * @return RetDataBean
     * @Title getDbSource
     * @Description  获取数据源信息
     */
    @RequestMapping(value = "/getDbSource", method = RequestMethod.POST)
    public RetDataBean getDbSource(SysDbSource sysDbSource) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            sysDbSource.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, sysDbSourceService.selectOne(sysDbSource));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param pageNumber
     * @param pageSize
     * @param search
     * @param sort
     * @param sortOrder
     * @return RetDataBean
     * @Title: getCodeClassList
     * @Description:  获取分类码列表
     */
    @RequestMapping(value = "/getCodeClassList", method = RequestMethod.POST)
    public RetDataBean getCodeClassList(
            Integer pageNumber,
            Integer pageSize,
            String search,
            String sort,
            String sortOrder
    ) {
        try {
            if (StringUtils.isBlank(sort)) {
                sort = "module";
            } else {
                sort = StrTools.lowerCharToUnderLine(sort);
            }
            if (StringUtils.isBlank(sortOrder)) {
                sortOrder = "asc";
            }
            String orderBy = sort + " " + sortOrder;
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(CodeClass.class);
            example.setOrderByClause(orderBy);
            Criteria criteria = example.createCriteria();
            criteria.andEqualTo("orgId", account.getOrgId());
            if (StringUtils.isNotEmpty(search)) {
                Criteria criteria2 = example.createCriteria();
                criteria2.orLike("codeName", "%" + search + "%").orLike("module", "%" + search + "%");
                example.and(criteria2);
            }
            PageInfo<CodeClass> pageInfo = codeClassService.getCodeClassList(example, pageNumber, pageSize);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param module
     * @return RetDataBean
     * @Title getCodeClassByModule
     * @Description  按模块获取分类列表
     */
    @RequestMapping(value = "/getCodeClassByModule", method = RequestMethod.POST)
    public RetDataBean getCodeClassByModule(String module) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, codeClassService.getCodeClassByModule(account.getOrgId(), module));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    @RequestMapping(value = "/getCodeClass", method = RequestMethod.POST)
    public RetDataBean getCodeClass(CodeClass codeClass) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            codeClass.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, codeClassService.selectOneCodeClass(codeClass));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title getSmsConfig
     * @Description  获取系统配置
     */
    @RequestMapping(value = "/getSmsConfig", method = RequestMethod.POST)
    public RetDataBean getSmsConfig() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            SmsConfig smsConfig = new SmsConfig();
            smsConfig.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, smsConfigService.selectOne(smsConfig));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  sysDeskConfig
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getSysDeskConfig
     * @Description:  获取桌面模块详情
     */
    @RequestMapping(value = "/getSysDeskConfig", method = RequestMethod.POST)
    public RetDataBean getSysDeskConfig(SysDeskConfig sysDeskConfig) {
        try {
            if (StringUtils.isBlank(sysDeskConfig.getDeskConfigId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            sysDeskConfig.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, sysDeskConfigService.selectOneSysDeskConfig(sysDeskConfig));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @return
     * @Title: getDeskConfigList
     * @Description:  获取可用桌面模块列表
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getDeskConfigList", method = RequestMethod.POST)
    public RetDataBean getDeskConfigList() {
        try {
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, sysDeskConfigService.getDeskConfigList(userInfo.getOrgId(), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getLeadId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param @param  request
     * @param @param  sortId
     * @param @param  pageNumber
     * @param @param  pageSize
     * @param @param  search
     * @param @param  sort
     * @param @param  sortOrder
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getAllSysDeskConfig
     * @Description:  获取桌面列表
     */
    @RequestMapping(value = "/getAllSysDeskConfig", method = RequestMethod.POST)
    public RetDataBean getAllSysDeskConfig(
            Integer pageNumber,
            Integer pageSize,
            String search,
            String sort,
            String sortOrder
    ) {
        try {
            if (StringUtils.isBlank(sort)) {
                sort = "sort_no";
            } else {
                sort = StrTools.lowerCharToUnderLine(sort);
            }
            if (StringUtils.isBlank(sortOrder)) {
                sortOrder = "asc";
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            }
            String orderBy = sort + " " + sortOrder;
            PageInfo<Map<String, String>> pageInfo = sysDeskConfigService.getAllSysDeskConfig(pageNumber, pageSize, orderBy, account.getOrgId(), "%" + search + "%");
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param @return
     * @Title: getMemoryStatus
     * @Description:  获取当前服务器内存使用情况
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getMemoryStatus", method = RequestMethod.POST)
    public RetDataBean getMemoryStatus() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, sysMonitorService.getMemoryStatus());
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @return
     * @Title: getDiskStatus
     * @Description:  获取服务器硬盘使用情况
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getDiskStatus", method = RequestMethod.POST)
    public RetDataBean getDiskStatus() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, sysMonitorService.getDiskStatus());
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @return
     * @Title: getCpuInfo
     * @Description:  获取服务器CPU的基本信息
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getCpuInfo", method = RequestMethod.POST)
    public RetDataBean getCpuInfo() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, sysMonitorService.getCpuInfo());
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @return
     * @Title: getCpuStatus
     * @Description:  获取服务器的CPU每个核心的使用情况
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getCpuStatus", method = RequestMethod.POST)
    public RetDataBean getCpuStatus() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, sysMonitorService.getCpuStatus());
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param @return
     * @Title: getMyMenuInPriv
     * @Description:  获取当用户权限内的菜单
     * @return: RetDataBean
     */
    @RequestMapping("/getMyMenuInPriv")
    public RetDataBean getMyMenuInPriv() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            String userPrivIds = account.getUserPriv();
            if (StringUtils.isNotBlank(userPrivIds)) {
                String[] privIdArr = userPrivIds.split(",");
                String sysMenuIds = "";
                for (int i = 0; i < privIdArr.length; i++) {
                    if (StringUtils.isNotBlank(privIdArr[i])) {
                        UserPriv userPriv = userPrivService.getUserPrivByPrivId(privIdArr[i], account.getOrgId());
                        sysMenuIds += userPriv.getUserPrivStr() + ",";
                    }
                }
                List<String> sysMenuIdList = StrTools.strToList(sysMenuIds);
                List<SysMenu> sysMenuList = sysMenuService.getSysMenuInPriv(sysMenuIdList, account.getOrgId());
                return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, sysMenuList);
            } else {
                return RetDataTools.NotOk(MessageCode.MSG_00021);
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @return
     * @Title: getMyShortcutMenu
     * @Description:  获取个人的快捷方式
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getMyShortcutMenu", method = RequestMethod.POST)
    public RetDataBean getMyShortcutMenu() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            Shortcut shortcut = new Shortcut();
            shortcut.setCreateUser(account.getAccountId());
            shortcut.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, shortcutService.selectOneShortcut(shortcut));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param @return
     * @Title: getSysTimingTaskList
     * @Description:   获取系统定时任务
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getSysTimingTaskList", method = RequestMethod.POST)
    public RetDataBean getSysTimingTaskList(
            PageParam pageParam
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("s.sort_no");
            } else {
                pageParam.setSort("s." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            pageParam.setOrgId(account.getOrgId());
            PageInfo<Map<String, String>> pageInfo = sysTimingTaskService.getSysTimingTaskList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param sysTimingTask
     * @param @return
     * @Title: getSysTimingTaskById
     * @Description:  获取定时任务详情
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getSysTimingTaskById", method = RequestMethod.POST)
    public RetDataBean getSysTimingTaskById(SysTimingTask sysTimingTask) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            sysTimingTask.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, sysTimingTaskService.selectOneSysTimingTask(sysTimingTask));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param request
     * @param sysProfile
     * @param @return
     * @Title: getSysProfileById
     * @Description:  获取门户详情
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getSysProfileById", method = RequestMethod.POST)
    public RetDataBean getSysProfileById(HttpServletRequest request, SysProfile sysProfile) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            sysProfile.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, sysProfileService.selectOneSysProfile(sysProfile));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

}
