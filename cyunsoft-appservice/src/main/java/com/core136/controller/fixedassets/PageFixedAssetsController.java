/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: FixedassetsPageController.java
 * @Package com.core136.controller.fixedassets
 * @Description: 描述
 * @author: lsq
 * @date: 2019年11月25日 下午5:18:00
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.controller.fixedassets;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author lsq
 *
 */
@Controller
@RequestMapping("/app/core/fixedassets")
public class PageFixedAssetsController {
    /**
     *
     * @Title: goRepairdetails
     * @Description:  维修详情
     * @return
     * ModelAndView
     */
    @RequestMapping("/repairdetails")
    @RequiresPermissions("/app/core/fixedassets/repairdetails")
    public ModelAndView goRepairdetails() {
        try {
            return new ModelAndView("app/core/fixedassets/repairdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     *
     * @Title: goRepairmange
     * @Description:  固定资产维修管理
     * @param: @return
     * @return: ModelAndView

     */
    @RequestMapping("/repairmange")
    @RequiresPermissions("/app/core/fixedassets/repairmange")
    public ModelAndView goRepairmange(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/fixedassets/repairmange");
            } else {
                mv = new ModelAndView("app/core/fixedassets/repair");
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     *
     * @Title: goApprove
     * @Description:  领用审批
     * @param: @return
     * @return: ModelAndView

     */
    @RequestMapping("/approve")
    @RequiresPermissions("/app/core/fixedassets/approve")
    public ModelAndView goApprove() {
        try {
            return new ModelAndView("app/core/fixedassets/approve");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     *
     * @Title: goAllocationfixedassets
     * @Description:  固定资产调拨
     * @param: @return
     * @return: ModelAndView

     */
    @RequestMapping("/allocation")
    @RequiresPermissions("/app/core/fixedassets/allocation")
    public ModelAndView goAllocation(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/fixedassets/allocationfixedassets");
            } else {
                mv = new ModelAndView("app/core/fixedassets/allocationfixedassetsmanage");
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     *
     * @Title: goApplyfixedassets
     * @Description:  固定资产申请
     * @param: @return
     * @return: ModelAndView

     */
    @RequestMapping("/applyfixedassets")
    @RequiresPermissions("/app/core/fixedassets/applyfixedassets")
    public ModelAndView goApplyfixedassets() {
        try {
            return new ModelAndView("app/core/fixedassets/applyfixedassets");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     *
     * @Title: goSetSort
     * @Description:  固定资产分类设置
     * @param: @return
     * @return: ModelAndView

     */
    @RequestMapping("/setsort")
    @RequiresPermissions("/app/core/fixedassets/setsort")
    public ModelAndView goSetSort() {
        try {
            return new ModelAndView("app/core/fixedassets/sort");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     *
     * @Title: goSetSort
     * @Description:  固定资产查询
     * @param: @return
     * @return: ModelAndView

     */
    @RequestMapping("/query")
    @RequiresPermissions("/app/core/fixedassets/query")
    public ModelAndView goQuery() {
        try {
            return new ModelAndView("app/core/fixedassets/query");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     *
     * @Title: goDetailsfixedassets
     * @Description:  固定资产详情
     * @param: @return
     * @return: ModelAndView

     */
    @RequestMapping("/detailsfixedassets")
    @RequiresPermissions("/app/core/fixedassets/detailsfixedassets")
    public ModelAndView goDetailsfixedassets() {
        try {
            return new ModelAndView("app/core/fixedassets/detailsfixedassets");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     *
     * @Title: goDetailsfixedassetsandapply
     * @Description:  调拨的产品与申请详情
     * @param: @return
     * @return: ModelAndView

     */
    @RequestMapping("/detailsfixedassetsandapply")
    @RequiresPermissions("/app/core/fixedassets/detailsfixedassetsandapply")
    public ModelAndView goDetailsfixedassetsandapply() {
        try {
            return new ModelAndView("app/core/fixedassets/detailsfixedassetsandapply");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     *
     * @Title: goSetstorage
     * @Description:  设置仓库
     * @param: @return
     * @return: ModelAndView

     */
    @RequestMapping("/setstorage")
    @RequiresPermissions("/app/core/fixedassets/setstorage")
    public ModelAndView goSetstorage() {
        try {
            return new ModelAndView("app/core/fixedassets/setstorage");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     *
     * @Title: goManage
     * @Description:  跳转到固定资产管理
     * @param: view
     * @param: @return
     * @return: ModelAndView

     */
    @RequestMapping("/manage")
    @RequiresPermissions("/app/core/fixedassets/manage")
    public ModelAndView goManage(String view) {
        try {
            ModelAndView mv = null;
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/fixedassets/addfixedassets");
            } else {
                if (view.equals("manage")) {
                    mv = new ModelAndView("app/core/fixedassets/managefixedassets");
                } else if (view.equals("edit")) {
                    mv = new ModelAndView("app/core/fixedassets/editfixedassets");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }
}
