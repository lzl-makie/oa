package com.core136.controller.echarts;

import com.core136.bean.account.Account;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.service.account.AccountService;
import com.core136.service.echarts.EchartsCrmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ret/echartscrmget")
public class RouteGetBiCrmController {
    private EchartsCrmService echartsCrmService;

    @Autowired
    public void setEchartsCrmService(EchartsCrmService echartsCrmService) {
        this.echartsCrmService = echartsCrmService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * @return RetDataBean
     * @Title: getBiQuotationByDeptPie
     * @Description:  部门报价单占比前10的占比
     */
    @RequestMapping(value = "/getBiQuotationByDeptPie", method = RequestMethod.POST)
    public RetDataBean getBiQuotationByDeptPie() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsCrmService.getBiQuotationByDeptPie(account));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getBiQuotationByProductPie
     * @Description:  获取产品分类前10的占比
     */
    @RequestMapping(value = "/getBiQuotationByProductPie", method = RequestMethod.POST)
    public RetDataBean getBiQuotationByProductPie() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsCrmService.getBiQuotationByProductPie(account));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getBiQuotationByMonthLine
     * @Description:  按月份统计报价单
     */
    @RequestMapping(value = "/getBiQuotationByMonthLine", method = RequestMethod.POST)
    public RetDataBean getBiQuotationByMonthLine() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsCrmService.getBiQuotationByMonthLine(account));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getBiQuotationByAccountPie
     * @Description:  获取报价单人员占比
     */
    @RequestMapping(value = "/getBiQuotationByAccountPie", method = RequestMethod.POST)
    public RetDataBean getBiQuotationByAccountPie() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsCrmService.getBiQuotationByAccountPie(account));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getBiInquiryByDeptPie
     * @Description:  部门询价单占比前10的占比
     */
    @RequestMapping(value = "/getBiInquiryByDeptPie", method = RequestMethod.POST)
    public RetDataBean getBiInquiryByDeptPie() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsCrmService.getBiInquiryByDeptPie(account));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getBiInquiryByProductPie
     * @Description:  获取产品分类前10的占比
     */
    @RequestMapping(value = "/getBiInquiryByProductPie", method = RequestMethod.POST)
    public RetDataBean getBiInquiryByProductPie() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsCrmService.getBiInquiryByProductPie(account));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getBiInquiryByMonthLine
     * @Description:  按月份统计工作量
     */
    @RequestMapping(value = "/getBiInquiryByMonthLine", method = RequestMethod.POST)
    public RetDataBean getBiInquiryByMonthLine() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsCrmService.getBiInquiryByMonthLine(account));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getBiInquiryByAccountPie
     * @Description:  获取询价单人员占比
     */
    @RequestMapping(value = "/getBiInquiryByAccountPie", method = RequestMethod.POST)
    public RetDataBean getBiInquiryByAccountPie() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsCrmService.getBiInquiryByAccountPie(account));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getBiCustomerIndustryPie
     * @Description:  获取crm的客户行业占比
     */
    @RequestMapping(value = "/getBiCustomerIndustryPie", method = RequestMethod.POST)
    public RetDataBean getBiCustomerIndustryPie() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsCrmService.getBiCustomerIndustryPie(account));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getBiCustomerKeepUserPie
     * @Description:  获取CRM销售人员的占比
     */
    @RequestMapping(value = "/getBiCustomerKeepUserPie", method = RequestMethod.POST)
    public RetDataBean getBiCustomerKeepUserPie() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsCrmService.getBiCustomerKeepUserPie(account));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getBiCustomerAreaPie
     * @Description:  获取CRM地区占比
     */
    @RequestMapping(value = "/getBiCustomerAreaPie", method = RequestMethod.POST)
    public RetDataBean getBiCustomerAreaPie() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsCrmService.getBiCustomerAreaPie(account));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getBiCustomerLevelPie
     * @Description:  获取CRM客户等级占比
     */
    @RequestMapping(value = "/getBiCustomerLevelPie", method = RequestMethod.POST)
    public RetDataBean getBiCustomerLevelPie() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsCrmService.getBiCustomerLevelPie(account));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }
}
