package com.core136.controller.mobile;

import com.alibaba.fastjson.JSONObject;
import com.core136.bean.account.AUserInfo;
import com.core136.bean.account.Account;
import com.core136.bean.account.UserPriv;
import com.core136.bean.sys.WxConfig;
import com.core136.common.utils.StrTools;
import com.core136.common.weixin.WxUtils;
import com.core136.common.weixin.entity.WUserIdInfo;
import com.core136.service.account.AccountService;
import com.core136.service.account.UserPrivService;
import com.core136.service.sys.AppConfigService;
import com.core136.service.sys.WxAndDdUtilsService;
import com.core136.service.sys.WxConfigService;
import com.core136.service.weixin.WeiXinLoginService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/weixin")
public class PageWxController {
    private WxAndDdUtilsService wxAndDdUtilsService;
    @Autowired
    public void setWxUtilsService(WxAndDdUtilsService wxAndDdUtilsService)
    {
        this.wxAndDdUtilsService = wxAndDdUtilsService;
    }
    @Value("${app.host}")
    private String host;
    private WxConfigService wxConfigService;

    @Autowired
    public void setWxConfigService(WxConfigService wxConfigService) {
        this.wxConfigService = wxConfigService;
    }

    private AppConfigService appConfigService;

    @Autowired
    public void setAppConfigService(AppConfigService appConfigService) {
        this.appConfigService = appConfigService;
    }

    private WeiXinLoginService weiXinLoginService;

    @Autowired
    public void setWeiXinLoginService(WeiXinLoginService weiXinLoginService) {
        this.weiXinLoginService = weiXinLoginService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private UserPrivService userPrivService;
    @Autowired
    public void setUserPrivService(UserPrivService userPrivService)
    {
        this.userPrivService = userPrivService;
    }

    /**
     * @Title: goWXIndex
     * @Description:  企业微信系统界面
     * @param: request
     * @param: ddConfig
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/index")
    public ModelAndView goWXIndex(WxConfig wxConfig) {
        wxConfig = wxConfigService.selectOneWxConfig(wxConfig);
        ModelAndView mv = new ModelAndView("app/mobile/weixin/index");
        mv.addObject("wxConfig", wxConfig);
        mv.addObject("host", host);
        return mv;
    }

    /**
     * @param wxConfig
     * @return ModelAndView
     * @Title: goWxAutoLogin
     * @Description:  微信自动登陆
     */
    @RequestMapping("/wxlogin")
    public ModelAndView goWxAutoLogin(WxConfig wxConfig) {
        wxConfig = wxConfigService.selectOneWxConfig(wxConfig);
        ModelAndView mv = new ModelAndView("app/mobile/weixin/wxlogin");
        mv.addObject("wxConfig", wxConfig);
        mv.addObject("host", host);
        return mv;
    }

    /**
     * @param request
     * @param code
     * @param orgId
     * @return ModelAndView
     * @Title: goToMobileIndex
     * @Description:  跳转到主页
     */
    @RequestMapping(value = "/main/index", method = RequestMethod.GET)
    public ModelAndView goToMobileIndex(HttpServletRequest request, String code, String orgId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (account == null) {
                WxConfig wxConfig = new WxConfig();
                wxConfig = wxConfigService.selectOneWxConfig(wxConfig);
                String accessToken = wxAndDdUtilsService.getWxSyncAccessToken(wxConfig.getWxCorpId(), wxConfig.getWxAppSecret());
                WUserIdInfo wUserIdInfo = WxUtils.getUserId(accessToken, code);
                weiXinLoginService.weiXinLogin(request, wUserIdInfo.getUserId(), orgId);
                account = accountService.getRedisAUserInfoToAccount();
            }
            AUserInfo aUserInfo = accountService.getRedisAUserInfo();
            String[] privIdArr = aUserInfo.getUserPriv().split(",");
            String mobilePrivIds = "";
            for (int i = 0; i < privIdArr.length; i++) {
                if (StringUtils.isNotBlank(privIdArr[i])) {
                    UserPriv userPriv = userPrivService.getUserPrivByPrivId(privIdArr[i], aUserInfo.getOrgId());
                    mobilePrivIds += userPriv.getMobilePriv() + ",";
                }
            }
            List<String> appList = StrTools.strToList(mobilePrivIds);
            JSONObject appMenuList = appConfigService.getMyAppListInfo(account.getOrgId(), appList);
            ModelAndView mv = new ModelAndView("app/mobile/main/index");
            mv.addObject("appMenuList", appMenuList);
            mv.addObject("r", System.currentTimeMillis());
            return mv;
        } catch (Exception e) {
            ModelAndView mv = new ModelAndView("titps");
            return mv;
        }
    }

    /**
     * @param request
     * @param code
     * @param orgId
     * @param detailsUrl
     * @return String
     * @Title: goDetails
     * @Description:  微信消息的详情页跳转
     */
    @RequestMapping(value = "/details", method = RequestMethod.GET)
    public String goDetails(HttpServletRequest request, String code, String orgId, String detailsUrl) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (account == null) {
                WxConfig wxConfig = new WxConfig();
                wxConfig = wxConfigService.selectOneWxConfig(wxConfig);
                String accessToken = wxAndDdUtilsService.getWxSyncAccessToken(wxConfig.getWxCorpId(), wxConfig.getWxAppSecret());
                WUserIdInfo wUserIdInfo = WxUtils.getUserId(accessToken, code);
                weiXinLoginService.weiXinLogin(request, wUserIdInfo.getUserId(), orgId);
            }
        } catch (Exception e) {

        }
        return detailsUrl;
    }
}
