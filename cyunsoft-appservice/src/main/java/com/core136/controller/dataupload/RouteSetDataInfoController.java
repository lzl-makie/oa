package com.core136.controller.dataupload;

import com.core136.bean.account.Account;
import com.core136.bean.account.UserInfo;
import com.core136.bean.dataupload.DataUploadHandle;
import com.core136.bean.dataupload.DataUploadInfo;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.dataupload.DataUploadHandleService;
import com.core136.service.dataupload.DataUploadInfoService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

@RestController
@RequestMapping("/set/datauploadset")
public class RouteSetDataInfoController {
    private DataUploadInfoService dataUploadInfoService;

    @Autowired
    public void setDataUploadInfoService(DataUploadInfoService dataUploadInfoService) {
        this.dataUploadInfoService = dataUploadInfoService;
    }

    private DataUploadHandleService dataUploadHandleService;

    @Autowired
    public void setDataUploadHandleService(DataUploadHandleService dataUploadHandleService) {
        this.dataUploadHandleService = dataUploadHandleService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * @param dataUploadHandle
     * @return RetDataBean
     * @Title: insertDataUploadHandle
     * @Description:  添加事件处理结果
     */
    @RequestMapping(value = "/insertDataUploadHandle", method = RequestMethod.POST)
    public RetDataBean insertDataUploadHandle(DataUploadHandle dataUploadHandle) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            dataUploadHandle.setProcessId(SysTools.getGUID());
            dataUploadHandle.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            dataUploadHandle.setCreateUser(account.getAccountId());
            dataUploadHandle.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, dataUploadHandleService.processDataInfo(dataUploadHandle));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param dataUploadHandle
     * @return RetDataBean
     * @Title: deletDataUploadHandle
     * @Description:  删除事件处理结果
     */
    @RequestMapping(value = "/deleteDataUploadHandle", method = RequestMethod.POST)
    public RetDataBean deletDataUploadHandle(DataUploadHandle dataUploadHandle) {
        try {
            if (StringUtils.isBlank(dataUploadHandle.getProcessId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            dataUploadHandle.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, dataUploadHandleService.deleteDataUploadHandle(dataUploadHandle));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param dataUploadHandle
     * @return RetDataBean
     * @Title: updateDataUploadHandle
     * @Description:  更新处理结果
     */
    @RequestMapping(value = "/updateDataUploadHandle", method = RequestMethod.POST)
    public RetDataBean updateDataUploadHandle(DataUploadHandle dataUploadHandle) {
        try {
            if (StringUtils.isBlank(dataUploadHandle.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(DataUploadHandle.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("processId", dataUploadHandle.getProcessId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, dataUploadHandleService.updateDataUploadHandle(example, dataUploadHandle));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param dataUploadInfo
     * @return RetDataBean
     * @Title: insertDataUploadInfo
     * @Description:  上报信息
     */
    @RequestMapping(value = "/insertDataUploadInfo", method = RequestMethod.POST)
    public RetDataBean insertDataUploadInfo(DataUploadInfo dataUploadInfo) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("dataupload:insert")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            dataUploadInfo.setRecordId(SysTools.getGUID());
            dataUploadInfo.setStatus("0");
            dataUploadInfo.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            dataUploadInfo.setCreateUser(account.getAccountId());
            dataUploadInfo.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, dataUploadInfoService.dataUploadInfo(account, userInfo, dataUploadInfo));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param dataUploadInfo
     * @return RetDataBean
     * @Title: deleteDataUploadInfo
     * @Description:  删除上报信息
     */
    @RequestMapping(value = "/deleteDataUploadInfo", method = RequestMethod.POST)
    public RetDataBean deleteDataUploadInfo(DataUploadInfo dataUploadInfo) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("dataupload:delete")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
            }
            if (StringUtils.isBlank(dataUploadInfo.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            dataUploadInfo.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, dataUploadInfoService.deleteDataUploadInfo(dataUploadInfo));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param dataUploadInfo
     * @return RetDataBean
     * @Title: updateDataUploadInfo
     * @Description:  更新上报信息
     */
    @RequestMapping(value = "/updateDataUploadInfo", method = RequestMethod.POST)
    public RetDataBean updateDataUploadInfo(DataUploadInfo dataUploadInfo) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("dataupload:update")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
            if (StringUtils.isBlank(dataUploadInfo.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            Example example = new Example(DataUploadInfo.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", dataUploadInfo.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, dataUploadInfoService.updateDataUploadInfo(account, userInfo, example, dataUploadInfo));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

}
