package com.core136.controller.diary;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/app/core")
public class PageDiaryController {
    /**
     * 他人分享的工作日志
     * @return
     */
    @RequestMapping("/diary/sharediarylist")
    @RequiresPermissions("/app/core/diary/sharediarylist")
    public ModelAndView sharediarylist() {
        try {
            return new ModelAndView("app/core/diary/sharediarylist");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: leaddiary
     * @Description:  领导查看下属的工作日志
     * @param: @return
     * @return: ModelAndView
     */
    @RequestMapping("/diary/leaddiary")
    @RequiresPermissions("/app/core/diary/leaddiary")
    public ModelAndView leaddiary(String view) {
        try {
            return new ModelAndView("app/core/diary/leaddiary");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


    /**
     * @param @return 设定文件
     * @return ModelAndView 返回类型
     * @Title: readdiary
     * @Description:  查看日志详情
     */
    @RequestMapping("/diary/readdiary")
    @RequiresPermissions("/app/core/diary/readdiary")
    public ModelAndView readdiary(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/diary/readdiary");
            } else {
                if (view.equals("mdetails")) {
                    mv = new ModelAndView("app/mobile/main/diary/details");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param request
     * @return ModelAndView
     * @Title goMydiary
     * @Description  工作日志
     */
    @RequestMapping("/diary/mydiary")
    @RequiresPermissions("/app/core/diary/mydiary")
    public ModelAndView goMydiary(HttpServletRequest request) {
        return new ModelAndView("app/core/diary/mydiary");
    }

    /**
     * @param @param  request
     * @param @param  accountId
     * @param @return 设定文件
     * @return ModelAndView 返回类型
     * @Title: goOtherdiarylist
     * @Description:  他人的工作日志
     */
    @RequestMapping("/diary/otherdiarylist")
    @RequiresPermissions("/app/core/diary/otherdiarylist")
    public ModelAndView goOtherdiarylist(HttpServletRequest request) {
        return new ModelAndView("app/core/diary/otherdiarylist");
    }

    /**
     * @param @param  request
     * @param @return 设定文件
     * @return ModelAndView 返回类型
     * @Title: goDiarypriv
     * @Description:  设置工作日志
     */
    @RequestMapping("/diary/diarypriv")
    @RequiresPermissions("/app/core/diary/diarypriv")
    public ModelAndView goDiarypriv(HttpServletRequest request) {
        return new ModelAndView("app/core/diary/diarypriv");
    }
}
