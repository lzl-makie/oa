package com.core136.controller.diary;

import com.core136.bean.account.Account;
import com.core136.bean.account.UserInfo;
import com.core136.bean.oa.Diary;
import com.core136.bean.oa.DiaryComments;
import com.core136.bean.oa.DiaryPriv;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.oa.DiaryCommentsService;
import com.core136.service.oa.DiaryPrivService;
import com.core136.service.oa.DiaryService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

@RestController
@RequestMapping("/set/oaset")
public class RouteSetDiaryController {
    private DiaryPrivService diaryPrivService;

    @Autowired
    public void setDiaryPrivService(DiaryPrivService diaryPrivService) {
        this.diaryPrivService = diaryPrivService;
    }

    private DiaryService diaryService;

    @Autowired
    public void setDiaryService(DiaryService diaryService) {
        this.diaryService = diaryService;
    }

    private DiaryCommentsService diaryCommentsService;

    @Autowired
    public void setDiaryCommentsService(DiaryCommentsService diaryCommentsService) {
        this.diaryCommentsService = diaryCommentsService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * @Title: insertDiaryComments
     * @Description:  添加日志评论
     * @param: request
     * @param: diaryComments
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/insertDiaryComments", method = RequestMethod.POST)
    public RetDataBean insertDiaryComments(DiaryComments diaryComments) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            diaryComments.setCommId(SysTools.getGUID());
            diaryComments.setCreateUser(account.getAccountId());
            diaryComments.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            diaryComments.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, diaryCommentsService.insertDiaryComments(diaryComments));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: delDiary
     * @Description:  删除日志
     * @param: request
     * @param: diary
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/delDiary", method = RequestMethod.POST)
    public RetDataBean delDiary(Diary diary) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("diary:delete")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
            }
            if (StringUtils.isBlank(diary.getDiaryId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            diary.setCreateUser(account.getAccountId());
            diary.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, diaryService.delDiary(diary));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  diaryPriv
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: setDiaryPriv
     * @Description:  设置工作日志权限
     */
    @RequestMapping(value = "/setDiaryPriv", method = RequestMethod.POST)
    public RetDataBean setDiaryPriv(DiaryPriv diaryPriv) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            }
            diaryPriv.setCreateUser(account.getAccountId());
            diaryPriv.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            diaryPriv.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, diaryPrivService.setDiaryPriv(diaryPriv));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  diary
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: sendDiary
     * @Description:  创建工作日志
     */
    @RequestMapping(value = "/sendDiary", method = RequestMethod.POST)
    public RetDataBean sendDiary(Diary diary) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("diary:insert")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            DiaryPriv diaryPriv = new DiaryPriv();
            diaryPriv.setOrgId(account.getOrgId());
            diaryPriv = diaryPrivService.selectOneDiaryPriv(diaryPriv);
            diary.setDiaryId(SysTools.getGUID());
            if (StringUtils.isBlank(diary.getTitle())) {
                diary.setTitle(MessageCode.MESSAGE_TITLE_IS_EMPTY);
            }
            if (StringUtils.isBlank(diary.getDiaryDay())) {
                diary.setDiaryDay(SysTools.getTime("yyyy-MM-dd"));
            }
            diary.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            diary.setCreateUser(account.getAccountId());
            Document htmlDoc = Jsoup.parse(diary.getContent());
            String subheading = htmlDoc.text();
            if (subheading.length() > 50) {
                subheading = subheading.substring(0, 50) + "...";
            }
            diary.setSubheading(subheading);
            diary.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, diaryService.createDiary(account, userInfo, diary, diaryPriv));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: updateDiary
     * @Description:  工作日志更新
     * @param: request
     * @param: diary
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/updateDiary", method = RequestMethod.POST)
    public RetDataBean updateDiary(Diary diary) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("diary:update")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(diary.getDiaryId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            diary.setCreateUser(account.getAccountId());
            diary.setOrgId(account.getOrgId());
            Document htmlDoc = Jsoup.parse(diary.getContent());
            String subheading = htmlDoc.text();
            if (subheading.length() > 50) {
                subheading = subheading.substring(0, 50) + "...";
            }
            diary.setSubheading(subheading);
            DiaryPriv diaryPriv = new DiaryPriv();
            diaryPriv.setOrgId(account.getOrgId());
            diaryPriv = diaryPrivService.selectOneDiaryPriv(diaryPriv);
            Example example = new Example(Diary.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("diaryId", diary.getDiaryId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, diaryService.updateDiary(account, userInfo, example, diary, diaryPriv));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

}
