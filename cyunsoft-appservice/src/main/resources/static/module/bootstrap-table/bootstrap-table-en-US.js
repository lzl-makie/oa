/**
 * Bootstrap Table Chinese translation
 * Author: Zhixin Wen<wenzhixin2010@gmail.com>
 */
(function ($) {
    'use strict';

    $.fn.bootstrapTable.locales['en-US'] = {
        formatLoadingMessage: function () {
            return 'Trying to load data……';
        },
        formatRecordsPerPage: function (pageNumber) {
            return pageNumber + ' Records per page';
        },
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Display from ' + pageFrom + ' to ' + pageTo + ' record，Total ' + totalRows + ' records';
        },
        formatSearch: function () {
            return 'Search';
        },
        formatNoMatches: function () {
            return 'No matching records were found';
        },
        formatPaginationSwitch: function () {
            return 'Hide/show Pagination';
        },
        formatRefresh: function () {
            return 'Refresh';
        },
        formatToggle: function () {
            return 'Switch';
        },
        formatColumns: function () {
            return 'Column';
        },
        formatExport: function () {
            return 'Export data';
        },
        formatClearFilters: function () {
            return 'Empty filter';
        }
    };

    $.extend($.fn.bootstrapTable.defaults, $.fn.bootstrapTable.locales['en-US']);

})(jQuery);
