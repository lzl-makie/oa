function singleFileUpLoad(module, fileId) {
    $.ajaxFileUpload({
        url: '/sys/file/upload?module=' + module, //上传文件的服务端
        secureuri: false,  //是否启用安全提交
        async: false,
        dataType: 'json',   //数据类型
        fileElementId: fileId, //表示文件域ID
        success: function (data, status) {
            if (data.status == "200") {
                let attachIds = data.list;
                let attachArr = [];
                for (let i = 0; i < attachIds.length; i++) {
                    attachArr.push(attachIds[i].attachId);
                }
                $("#show_" + fileId).empty();
                createAttachDiv(fileId, attachIds, 4);
                $("#" + fileId).attr("data_value", attachArr.join(","));
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                layer.msg(sysmsg[data.msg]);
            }
        },
        //提交失败处理函数
        error: function (data, status, e) {
            layer.msg("文件上传出错!请检查文件格式!");
            console.log(data.msg);
        }
    });
}

function fileUpLoad(module, fileId) {
    $.ajaxFileUpload({
        url: '/sys/file/upload?module=' + module, //上传文件的服务端
        secureuri: false,  //是否启用安全提交
        async: false,
        dataType: 'json',   //数据类型
        fileElementId: fileId, //表示文件域ID
        success: function (data, status) {
            if (data.status == "200") {
                var datalist = data.list;
                var attachIds = $("#" + fileId).attr("data_value");
                var attachArr = [];
                if (attachIds) {
                    attachArr = attachIds.split(",");
                }
                for (var i = 0; i < datalist.length; i++) {
                    attachArr.push(datalist[i].attachId);
                }
                createAttachDiv(fileId, datalist, 4);
                $("#" + fileId).attr("data_value", attachArr.join(","));
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                layer.msg(sysmsg[data.msg]);
            }
        },
        //提交失败处理函数
        error: function (data, status, e) {
            layer.msg("文件上传出错!请检查文件格式!");
            console.log(data.msg);
        }
    });
}

function fileDocumentUpLoad(module, fileId) {
    $.ajaxFileUpload({
        url: '/sys/file/upload?module=' + module, //上传文件的服务端
        secureuri: false,  //是否启用安全提交
        async: false,
        dataType: 'json',   //数据类型
        fileElementId: fileId, //表示文件域ID
        success: function (data, status) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                var datalist = data.list;
                var attachIds = $("#" + fileId).attr("data_value");
                var attachArr = [];
                if (attachIds) {
                    attachArr = attachIds.split(",");
                }
                for (var i = 0; i < datalist.length; i++) {
                    attachArr.push(datalist[i].attachId);
                }
                createAttachDocumentDiv(fileId, datalist, 4);
                $("#" + fileId).attr("data_value", attachArr.join(","));
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                layer.msg(sysmsg[data.msg]);
            }
        },
        //提交失败处理函数
        error: function (data, status, e) {
            layer.msg("文件上传出错!请检查文件格式!");
            console.log(data.msg);
        }
    });
}

function uploadheadimg(fileId) {
    $.ajaxFileUpload({
        url: '/sys/file/uploadHeadImg', //上传文件的服务端
        secureuri: false,  //是否启用安全提交
        async: false,
        dataType: 'json',   //数据类型
        fileElementId: fileId, //表示文件域ID
        success: function (data, status) {
            layer.msg(sysmsg[data.msg]);
            if (data.status == 200) {
                $(".js-accountHeadImg").each(function () {
                    $(this).attr("src", "/sys/file/getHeadImg?r=" + Math.random());
                })
            }
        },
        //提交失败处理函数
        error: function (data, status, e) {
            console.log(data.msg);
        }
    });
}

function uploadimg(fileId, module) {
    $.ajaxFileUpload({
        url: '/sys/file/uploadimg?module=' + module, //上传文件的服务端
        secureuri: false,  //是否启用安全提交
        async: false,
        dataType: 'json',   //数据类型
        fileElementId: fileId, //表示文件域ID
        success: function (data, status) {
            if (data.status == "200") {
                $("#" + fileId).attr("data-value", data.redirect);
                $("#" + fileId + "_img").attr("src", "/sys/file/getStaticImg?r=" + Math.random() + "&module=" + module + "&fileName=" + data.redirect);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        },
        error: function (data, status, e) {
            console.log(data.msg);
        }
    });
}


function uploadimglogo(fileId) {
    $.ajaxFileUpload({
        url: '/sys/file/uploadimglogo', //上传文件的服务端
        secureuri: false,  //是否启用安全提交
        async: false,
        dataType: 'json',   //数据类型
        fileElementId: fileId, //表示文件域ID
        success: function (data, status) {
            if (data.status == "200") {
                $("#" + fileId).attr("data-value", data.redirect);
                $("#" + fileId + "_img").attr("src", "/sys/file/getBackgroundImg?fileName=" + data.redirect);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        },
        error: function (data, status, e) {
            console.log(data.msg);
        }
    });
}


//priv:1只读权限
//priv:2可以打印
//priv:3可以下载,打印
//priv:4 编辑权限
function createAttach(eId, priv) {
    var attachIds = $("#" + eId).attr("data_value");
    $("#show_" + eId).empty();
    if (attachIds != "" && attachIds != undefined) {
        $.ajax({
            url: "/sys/file/getAttachList",
            type: "post",
            dataType: "json",
            data: {
                attachIds: attachIds
            },
            success: function (data) {
                if (data.status == 200) {
                    var datalist = data.list;
                    if (datalist != null) {
                        createAttachDiv(eId, datalist, priv);
                    }
                } else if (data.status == 100) {
                    console.log(data.msg);
                } else {
                    console.log(data.msg);
                }
            }
        });
    }
}

function createAttachDocument(eId, priv) {
    var attachIds = $("#" + eId).attr("data_value");
    $("#show_" + eId).empty();
    if (attachIds != "" && attachIds != undefined) {
        $.ajax({
            url: "/sys/file/getAttachList",
            type: "post",
            dataType: "json",
            data: {
                attachIds: attachIds
            },
            success: function (data) {
                if (data.status == 200) {
                    var datalist = data.list;
                    if (datalist != null) {
                        createAttachDocumentDiv(eId, datalist, priv);
                    }

                } else if (data.status == 100) {
                    console.log(data.msg);
                } else {
                    console.log(data.msg);
                }
            }
        });
    }
}


function createAttachDiv(eId, attachlist, priv) {
    var htmlattach = "";
    var htmlimg = "";
    //在线查看
    if (priv == "1") {
        for (var i = 0; i < attachlist.length; i++) {
            if (attachlist[i].extName != '.jpg' && attachlist[i].extName != '.jpeg' && attachlist[i].extName != '.png' && attachlist[i].extName != '.bmp' && attachlist[i].extName != '.tit' && attachlist[i].extName != '.gif') {
                htmlattach += "<span class='btn-group' style=\"margin-right: 10px;margin-bottom: 10px;\" id='attachdiv_" + attachlist[i].attachId + "'>" +
                    "<a class=\"btn btn-success\" href=\"javascript:void(0);\" title='"+attachlist[i].oldName+"' style='white-space: nowrap;overflow: hidden;text-overflow: ellipsis;width: 200px;'>" + attachlist[i].oldName + "</a>" +
                    "<a class=\"btn btn-success  dropdown-toggle\" data-toggle=\"dropdown\" href=\"javascript:void(0);\" aria-expanded=\"false\"><i class=\"fa fa-angle-down\"></i></a>" +
                    "<ul class=\"dropdown-menu dropdown-success\">";
                if(officetype!="3") {
                    htmlattach +="<li>" +
                    "<a href=\"javascript:window.open('/app/core/previewonline?attachId=" + attachlist[i].attachId + "');\">预览 </a>" +
                    "</li>";
                }
                htmlattach +="<li>" +
                    "<a href=\"javascript:openFileOnLine('" + attachlist[i].extName + "','" + attachlist[i].attachId + "',1);\">查看</a>" +
                    "</li>" +
                    "</ul>" +
                    "</span>";
            } else {
                htmlimg += "<span class='attachDiv' onclick=\"showImg('" + attachlist[i].attachId + "')\";>" +
                    "<img class='attachImg' src='/sys/file/getImage?attachId=" + attachlist[i].attachId + "'/>" +
                    "<div style='text-align:center;'>" + attachlist[i].oldName + "</div>" +
                    "</span>";
            }
        }
    } else if (priv == "2")//可以打印
    {
        for (var i = 0; i < attachlist.length; i++) {
            if (attachlist[i].extName != '.jpg' && attachlist[i].extName != '.jpeg' && attachlist[i].extName != '.png' && attachlist[i].extName != '.bmp' && attachlist[i].extName != '.tit' && attachlist[i].extName != '.gif') {
                htmlattach += "<span class='btn-group' style=\"margin-right: 10px;margin-bottom: 10px;\" id='attachdiv_" + attachlist[i].attachId + "'>" +
                    "<a class=\"btn btn-success\" href=\"javascript:void(0);\" title='"+attachlist[i].oldName+"' style='white-space: nowrap;overflow: hidden;text-overflow: ellipsis;width: 200px;'>" + attachlist[i].oldName + "</a>" +
                    "<a class=\"btn btn-success  dropdown-toggle\" data-toggle=\"dropdown\" href=\"javascript:void(0);\" aria-expanded=\"false\"><i class=\"fa fa-angle-down\"></i></a>" +
                    "<ul class=\"dropdown-menu dropdown-success\">" +
                    "<li>" +
                    "<a href=\"javascript:openFileOnLine('" + attachlist[i].extName + "','" + attachlist[i].attachId + "',3);\">打印 </a>" +
                    " </li>";
                if(officetype!="3") {
                    htmlattach += "<li>" +
                        "<a href=\"javascript:window.open('/app/core/previewonline?attachId=" + attachlist[i].attachId + "');\">预览 </a>" +
                        "</li>";
                }
                htmlattach +="<li>" +
                    "<a href=\"javascript:openFileOnLine('" + attachlist[i].extName + "','" + attachlist[i].attachId + "',1);\">查看 </a>" +
                    "</li>" +
                    "</ul>" +
                    "</span>";
            } else {
                htmlimg += "<span class='attachDiv' onclick=\"showImg('" + attachlist[i].attachId + "')\";>" +
                    "<img class='attachImg' src='/sys/file/getImage?attachId=" + attachlist[i].attachId + "'/>" +
                    "<div style='text-align:center;'>" + attachlist[i].oldName + "</div>" +
                    "</span>";
            }
        }
    } else if (priv == "3")//可以下载,打印
    {
        for (var i = 0; i < attachlist.length; i++) {
            if (attachlist[i].extName != '.jpg' && attachlist[i].extName != '.jpeg' && attachlist[i].extName != '.png' && attachlist[i].extName != '.bmp' && attachlist[i].extName != '.tit' && attachlist[i].extName != '.gif') {
                htmlattach += "<span class='btn-group' style=\"margin-right: 10px;margin-bottom: 10px;\" id='attachdiv_" + attachlist[i].attachId + "'>" +
                    "<a class=\"btn btn-success\" href=\"javascript:void(0);\" title='"+attachlist[i].oldName+"' style='white-space: nowrap;overflow: hidden;text-overflow: ellipsis;width: 200px;'>" + attachlist[i].oldName + "</a>" +
                    "<a class=\"btn btn-success  dropdown-toggle\" data-toggle=\"dropdown\" href=\"javascript:void(0);\" aria-expanded=\"false\"><i class=\"fa fa-angle-down\"></i></a>" +
                    "<ul class=\"dropdown-menu dropdown-success\">" +
                    "<li>" +
                    "<a href=\"/sys/file/getFileDown?attachId=" + attachlist[i].attachId + "\">下载</a>" +
                    " </li>"+
                    "<li>" +
                    "<a href=\"javascript:openFileOnLine('" + attachlist[i].extName + "','" + attachlist[i].attachId + "',2);\">打印 </a>" +
                    "</li>";
                if(officetype!="3") {
                    htmlattach += "<li>" +
                        "<a href=\"javascript:window.open('/app/core/previewonline?attachId=" + attachlist[i].attachId + "');\">预览 </a>" +
                        "</li>";
                }
                htmlattach +="<li>" +
                    "<a href=\"javascript:openFileOnLine('" + attachlist[i].extName + "','" + attachlist[i].attachId + "',1);\">查看 </a>" +
                    "</li>" +
                    "</ul>" +
                    "</span>";
            } else {
                htmlimg += "<span class='attachDiv' onclick=\"showImg('" + attachlist[i].attachId + "')\";>" +
                    "<img class='attachImg' src='/sys/file/getImage?attachId=" + attachlist[i].attachId + "'/>" +
                    "<div style='text-align:center;'>" + attachlist[i].oldName + "</div>" +
                    "</span>";
            }
        }
    } else if (priv == "4") {
        for (var i = 0; i < attachlist.length; i++) {
            if (attachlist[i].extName != '.jpg' && attachlist[i].extName != '.jpeg' && attachlist[i].extName != '.png' && attachlist[i].extName != '.bmp' && attachlist[i].extName != '.tit' && attachlist[i].extName != '.gif') {
                htmlattach += "<span class='btn-group' style=\"margin-right: 10px;margin-bottom: 10px;\" id='attachdiv_" + attachlist[i].attachId + "'>" +
                    "<a class=\"btn btn-success\" href=\"javascript:void(0);\" title='"+attachlist[i].oldName+"' style='white-space: nowrap;overflow: hidden;text-overflow: ellipsis;width: 200px;'>" + attachlist[i].oldName + "</a>" +
                    "<a class=\"btn btn-success  dropdown-toggle\" data-toggle=\"dropdown\" href=\"javascript:void(0);\" aria-expanded=\"false\"><i class=\"fa fa-angle-down\"></i></a>" +
                    "<ul class=\"dropdown-menu dropdown-success\">" +
                    "<li>" +
                    "<a href=\"javascript:openFileOnLine('" + attachlist[i].extName + "','" + attachlist[i].attachId + "',2);\">打印 </a>" +
                    "</li>";
                if (attachlist[i].extName != '.txt') {
                    htmlattach += "<li>" +
                        "<a href=\"javascript:openFileOnLine('" + attachlist[i].extName + "','" + attachlist[i].attachId + "',4);\">编辑 </a>" +
                        "</li>";
                }
                htmlattach += "<li>" +
                    "<a href=\"javascript:showFileVersionList('" + attachlist[i].attachId + "');\">历史版本 </a>" +
                    "</li>" +
                    "<li>" +
                    "<a href=\"javascript:void(0);delattach('" + eId + "','" + attachlist[i].attachId + "')\">删除</a>" +
                    "</li>" +
                    "<li>" +
                    "<a href=\"/sys/file/getFileDown?attachId=" + attachlist[i].attachId + "\">下载</a>" +
                    " </li>";
                if(officetype!="3") {
                    htmlattach +="<li>" +
                    "<a href=\"javascript:window.open('/app/core/previewonline?attachId=" + attachlist[i].attachId + "');\">预览 </a>" +
                    "</li>";
                }
                htmlattach +="<li>" +
                    "<a href=\"javascript:openFileOnLine('" + attachlist[i].extName + "','" + attachlist[i].attachId + "',1);\">查看 </a>" +
                    "</li>" +
                    "</ul>" +
                    "</span>";
            } else {
                htmlimg += "<span class='attachDiv' style='text-align: center;' id='span_" + attachlist[i].attachId + "'>" +
                    "<img onclick=\"showImg('" + attachlist[i].attachId + "');\" class='attachImg' src='/sys/file/getImage?attachId=" + attachlist[i].attachId + "'/>" +
                    "<div style='text-align:center;'>" + attachlist[i].oldName + "</div><div style='text-align:center;'></div>" +
                    "<a style='cursor: pointer;' href=\"javascript:void(0);delattach('" + eId + "','" + attachlist[i].attachId + "')\">删除</a>" +
                    "</span>";
            }
        }
    } else {
        for (var i = 0; i < attachlist.length; i++) {
            if (attachlist[i].extName != '.jpg' && attachlist[i].extName != '.jpeg' && attachlist[i].extName != '.png' && attachlist[i].extName != '.bmp' && attachlist[i].extName != '.tit' && attachlist[i].extName != '.gif') {
                htmlattach += "<span class='btn-group' style=\"margin-right: 10px;margin-bottom: 10px;\" id='attachdiv_" + attachlist[i].attachId + "'>" +
                    "<a class=\"btn btn-success\" href=\"javascript:void(0);\" title='"+attachlist[i].oldName+"' style='white-space: nowrap;overflow: hidden;text-overflow: ellipsis;width: 200px;'>" + attachlist[i].oldName + "</a>" +
                    "<a class=\"btn btn-success  dropdown-toggle\" data-toggle=\"dropdown\" href=\"javascript:void(0);\" aria-expanded=\"false\"><i class=\"fa fa-angle-down\"></i></a>" +
                    "<ul class=\"dropdown-menu dropdown-success\">" +
                    "<li>" +
                    "<a onclick=\"readfile('" + attachlist[i].extName + "','" + attachlist[i].attachId + "')\">查看</a>" +
                    "</li>" +
                    "<li>" +
                    "<a href=\"javascript:void(0);delattach('" + eId + "','" + attachlist[i].attachId + "')\">删除</a>" +
                    "</li>" +
                    "<li>" +
                    "<a href=\"/sys/file/getFileDown?attachId=" + attachlist[i].attachId + "\">下载</a>" +
                    " </li>";
                if(officetype!="3") {
                    htmlattach += "<li>" +
                    "<a href=\"javascript:window.open('/app/core/previewonline?attachId=" + attachlist[i].attachId + "');\">预览 </a>" +
                    "</li>";
                }
                htmlattach += "</ul>" +
                    "</span>";
            } else {
                htmlimg += "<span class='attachDiv' id='attachdiv_" + attachlist[i].attachId + "' onclick=\"delattach('" + eId + "','" + attachlist[i].attachId + "')\";><p class=\"diyControl\"><span class=\"diyCancel\"><i></i></span></p><img class='attachImg' src='/sys/file/getImage?attachId=" + attachlist[i].attachId + "'/></span>";
            }
        }
    }
    $("#show_" + eId).append("<div>" + htmlimg + "</div><div>" + htmlattach + "</div>");
}

function createAttachDocumentDiv(eId, attachlist, priv) {
    var attachTable = "";
    if (priv == "" || priv == null) {
        priv = "1";
    }
    //在线查看
    if (priv == "1") {
        for (var i = 0; i < attachlist.length; i++) {
            var extName = attachlist[i].extName.toUpperCase();
            attachTable += "<tr id=\"d_file_" + attachlist[i].attachId + "\"><td>" + attachlist[i].oldName + "</td><td>" + getUserNameByStr(attachlist[i].createAccount) + "</td><td>" + attachlist[i].upTime + "</td><td>" + attachlist[i].extName + "</td><td>" + (attachlist[i].fileSize / 1024).toFixed(2) + "K</td><td>" + attachlist[i].version + "</td>";
            if (extName != '.JPG' && extName != '.JPEG' && extName != '.PNG' && extName != '.BPM' && extName != '.TIT' && extName != '.GIF') {
                attachTable += "<td>";
                    if(officetype!="3")
                    {
                        attachTable +="<a class=\"btn btn-purple btn-sm\" href=\"javascript:window.open('/app/core/previewonline?attachId=" + attachlist[i].attachId + "');\">预览 </a>&nbsp&nbsp";
                    }
                attachTable +="<a class=\"btn btn-purple btn-sm\" href=\"javascript:openFileOnLine('" + attachlist[i].extName + "','" + attachlist[i].attachId + "',1);\">查看 </a></td>";
            } else {
                attachTable += "<td><a class=\"btn btn-purple btn-sm\" href=\"#\" onclick=\"showImg('" + attachlist[i].attachId + "')\";>查看 </a></td>";
            }
            attachTable += "</tr>"
        }
    } else if (priv == "2")//可以打印
    {
        for (var i = 0; i < attachlist.length; i++) {
            var extName = attachlist[i].extName.toUpperCase();
            attachTable += "<tr id=\"d_file_" + attachlist[i].attachId + "\"><td>" + attachlist[i].oldName + "</td><td>" + getUserNameByStr(attachlist[i].createAccount) + "</td><td>" + attachlist[i].upTime + "</td><td>" + attachlist[i].extName + "</td><td>" + (attachlist[i].fileSize / 1024).toFixed(2) + "K</td><td>" + attachlist[i].version + "</td>";
            if (extName != '.JPG' && extName != '.JPEG' && extName != '.PNG' && extName != '.BPM' && extName != '.TIT' && extName != '.GIF') {
                attachTable += "<td><a class=\"btn btn-primary btn-sm\" href=\"javascript:openFileOnLine('" + attachlist[i].extName + "','" + attachlist[i].attachId + "',2);\">打印 </a>&nbsp&nbsp";
                if(officetype!="3")
                {
                    attachTable += "<a class=\"btn btn-purple btn-sm\" href=\"javascript:window.open('/app/core/previewonline?attachId=" + attachlist[i].attachId + "');\">预览 </a>&nbsp&nbsp";
                }
                attachTable +="<a class=\"btn btn-purple btn-sm\" href=\"javascript:openFileOnLine('" + attachlist[i].extName + "','" + attachlist[i].attachId + "',1);\">查看 </a>";
            } else {
                attachTable += "<td><a class=\"btn btn-purple btn-sm\" href=\"#\" onclick=\"showImg('" + attachlist[i].attachId + "')\";>查看 </a></td>";
            }
            attachTable += "</tr>"
        }
    } else if (priv == "3")//可以下载,打印
    {
        for (var i = 0; i < attachlist.length; i++) {
            var extName = attachlist[i].extName.toUpperCase();
            attachTable += "<tr id=\"d_file_" + attachlist[i].attachId + "\"><td>" + attachlist[i].oldName + "</td><td>" + getUserNameByStr(attachlist[i].createAccount) + "</td><td>" + attachlist[i].upTime + "</td><td>" + attachlist[i].extName + "</td><td>" + (attachlist[i].fileSize / 1024).toFixed(2) + "K</td><td>" + attachlist[i].version + "</td>";
            if (extName != '.JPG' && extName != '.JPEG' && extName != '.PNG' && extName != '.BMP' && extName != '.TIT' && extName != '.GIF') {
                attachTable += "<td><a class=\"btn btn-primary btn-sm\" href=\"javascript:openFileOnLine('" + attachlist[i].extName + "','" + attachlist[i].attachId + "',2);\">打印 </a>&nbsp&nbsp" +
                    "<a class=\"btn btn-maroon btn-sm\" href=\"/sys/file/getFileDown?attachId=" + attachlist[i].attachId + "\">下载</a>&nbsp&nbsp";
                if(officetype!="3")
                {
                    attachTable +="<a class=\"btn btn-purple btn-sm\" href=\"javascript:window.open('/app/core/previewonline?attachId=" + attachlist[i].attachId + "');\">预览 </a>&nbsp&nbsp";
                }
                attachTable +="<a class=\"btn btn-purple btn-sm\" href=\"javascript:openFileOnLine('" + attachlist[i].extName + "','" + attachlist[i].attachId + "',1);\">查看 </a>";
            } else {
                attachTable += "<td><a class=\"btn btn-purple btn-sm\" href=\"#\" onclick=\"showImg('" + attachlist[i].attachId + "')\";>查看 </a></td>";
            }
            attachTable += "</tr>"
        }
    } else if (priv == "4") {
        for (var i = 0; i < attachlist.length; i++) {
            var extName = attachlist[i].extName.toUpperCase();
            attachTable += "<tr id=\"d_file_" + attachlist[i].attachId + "\"><td>" + attachlist[i].oldName + "</td><td>" + getUserNameByStr(attachlist[i].createAccount) + "</td><td>" + attachlist[i].upTime + "</td><td>" + attachlist[i].extName + "</td><td>" + (attachlist[i].fileSize / 1024).toFixed(2) + "K</td><td>" + attachlist[i].version + "</td>";
            if (extName != '.JPG' && extName != '.JPEG' && extName != '.PNG' && extName != '.BMP' && extName != '.TIT' && extName != '.GIF' && extName != '.TXT') {
                attachTable += "<td><a class=\"btn btn-primary btn-sm\" href=\"javascript:openFileOnLine('" + attachlist[i].extName + "','" + attachlist[i].attachId + "',2);\">打印 </a>&nbsp&nbsp" +
                    "<a class=\"btn btn-success btn-sm\" href=\"javascript:openFileOnLineTaoHong('" + attachlist[i].extName + "','" + attachlist[i].attachId + "',4);\">编辑 </a>&nbsp&nbsp" +
                    "<a class=\"btn btn-blue btn-sm\" href=\"javascript:showFileVersionList('" + attachlist[i].attachId + "');\">历史版本 </a>&nbsp&nbsp" +
                    "<a class=\"btn btn-darkorange btn-sm\" href=\"javascript:void(0);delDocumentattach('" + eId + "','" + attachlist[i].attachId + "')\">删除</a>&nbsp&nbsp" +
                    "<a class=\"btn btn-maroon btn-sm\" href=\"/sys/file/getFileDown?attachId=" + attachlist[i].attachId + "\">下载</a>&nbsp&nbsp";
                if(officetype!="3")
                {
                    attachTable +="<a class=\"btn btn-purple btn-sm\" href=\"javascript:window.open('/app/core/previewonline?attachId=" + attachlist[i].attachId + "');\">预览 </a>&nbsp&nbsp";
                }
                    attachTable+="<a class=\"btn btn-purple btn-sm\" href=\"javascript:openFileOnLine('" + attachlist[i].extName + "','" + attachlist[i].attachId + "',1);\">查看 </a></td>";
            } else {
                attachTable += "<td>";
                if (extName == '.TXT') {
                    attachTable += "<a class=\"btn btn-purple btn-sm\" href=\"javascript:window.open('/app/core/previewonline?attachId=" + attachlist[i].attachId + "');\">预览 </a>&nbsp&nbsp";
                }
                attachTable += "<a class=\"btn btn-darkorange btn-sm\" href=\"javascript:void(0);delDocumentattach('" + eId + "','" + attachlist[i].attachId + "')\">删除</a>&nbsp&nbsp" +
                    "<a class=\"btn btn-maroon btn-sm\" href=\"/sys/file/getFileDown?attachId=" + attachlist[i].attachId + "\">下载</a>&nbsp&nbsp" +
                    "<a class=\"btn btn-purple btn-sm\" href=\"javascript:openFileOnLine('" + attachlist[i].extName + "','" + attachlist[i].attachId + "',1);\">查看 </a></td>";
            }
            attachTable += "</tr>"
        }
    } else if (priv == "5") {
        for (var i = 0; i < attachlist.length; i++) {
            var extName = attachlist[i].extName.toUpperCase();
            attachTable += "<tr id=\"d_file_" + attachlist[i].attachId + "\"><td>" + attachlist[i].oldName + "</td><td>" + getUserNameByStr(attachlist[i].createAccount) + "</td><td>" + attachlist[i].upTime + "</td><td>" + attachlist[i].extName + "</td><td>" + (attachlist[i].fileSize / 1024).toFixed(2) + "K</td><td>" + attachlist[i].version + "</td>";
            if (extName != '.JPG' && extName != '.JPEG' && extName != '.PNG' && extName != '.BMP' && extName != '.TIT' && extName != '.GIF') {
                attachTable += "<td><a class=\"btn btn-primary btn-sm\" href=\"javascript:openFileOnLine('" + attachlist[i].extName + "','" + attachlist[i].attachId + "',2);\">打印 </a>&nbsp&nbsp";
                if (extName == '.DOC' || extName == '.DOT' || extName == '.DOCX' || extName == '.DOTX' || extName == ".UOF") {
                    attachTable += "<a class=\"btn btn-primary btn-sm\" href=\"javascript:openFileOnLineTaoHong('" + attachlist[i].extName + "','" + attachlist[i].attachId + "',5);\">套红 </a>&nbsp&nbsp";
                }
                if (extName == ".XLS" || extName == ".XLSX" || extName == ".CSV" || extName == ".ET" || extName == ".ETT"
                    || extName == ".PPT" || extName == ".PPTX" || extName == ".DPS" || extName == ".DPT" || extName == ".OFD") {
                    attachTable += "<a class=\"btn btn-success btn-sm\" href=\"javascript:openFileOnLineTaoHong('" + attachlist[i].extName + "','" + attachlist[i].attachId + "',4);\">编辑 </a>&nbsp&nbsp";
                }
                attachTable += "<a class=\"btn btn-blue btn-sm\" href=\"javascript:showFileVersionList('" + attachlist[i].attachId + "');\">历史版本 </a>&nbsp&nbsp" +
                    "<a class=\"btn btn-darkorange btn-sm\" href=\"javascript:void(0);delDocumentattach('" + eId + "','" + attachlist[i].attachId + "')\">删除</a>&nbsp&nbsp" +
                    "<a class=\"btn btn-maroon btn-sm\" href=\"/sys/file/getFileDown?attachId=" + attachlist[i].attachId + "\">下载</a>&nbsp&nbsp";
                if(officetype!="3") {
                    attachTable +="<a class=\"btn btn-purple btn-sm\" href=\"javascript:window.open('/app/core/previewonline?attachId=" + attachlist[i].attachId + "');\">预览 </a>&nbsp&nbsp";
                }
                attachTable +="<a class=\"btn btn-purple btn-sm\" href=\"javascript:openFileOnLine('" + attachlist[i].extName + "','" + attachlist[i].attachId + "',1);\">查看 </a></td>";
            } else {
                attachTable += "<td><a class=\"btn btn-darkorange btn-sm\" href=\"javascript:void(0);delDocumentattach('" + eId + "','" + attachlist[i].attachId + "')\">删除</a>&nbsp&nbsp" +
                    "<a class=\"btn btn-maroon btn-sm\" href=\"/sys/file/getFileDown?attachId=" + attachlist[i].attachId + "\">下载</a>&nbsp&nbsp" +
                    "<a class=\"btn btn-purple btn-sm\" href=\"#\" onclick=\"showImg('" + attachlist[i].attachId + "')\";>查看 </a></td>";
            }
            attachTable += "</tr>"
        }
    }
    $("#show_" + eId).append(attachTable);
}

function showImg(attachId) {
    window.open("/sys/file/getImage?attachId=" + attachId, "_blank");
}

function readfile(extName, attachId) {
    if (extName == ".txt" || extName == ".html") {
        window.open("/sys/file/readFile?attachId=" + attachId, "_blank");
    } else {

    }
}

/**
 *
 * @param eId
 * @param attachId
 * @returns
 */
function delattach(eId, attachId) {
    if (confirm("确定删除当前文件吗？")) {
        var v = $("#" + eId).attr("data_value");
        v = (v + ",").replace(attachId + ",", "");
        $("#" + eId).attr("data_value", v);
        $("#attachdiv_" + attachId).remove();
        $("#span_" + attachId).remove();
    }
}

function delDocumentattach(eId, attachId) {
    if (confirm("确定删除当前公文文件吗？")) {
        var v = $("#" + eId).attr("data_value");
        v = (v + ",").replace(attachId + ",", "");
        $("#" + eId).attr("data_value", v);
        $("#d_file_" + attachId).remove();
    }
}

function getFileType(extName) {
    extName = extName.toUpperCase();
    if (extName == '.JPG' || extName == '.PNG' || extName == '.JPEG' || extName == '.BMP' || extName == '.TIT' || extName == '.GIF') {
        return "img";
    } else {
        return "doc";
    }
}

function createTableAttach(attachIds) {
    var html = "";
    if (attachIds != "" && attachIds != undefined) {
        $.ajax({
            url: "/sys/file/getAttachList",
            type: "post",
            dataType: "json",
            async: false,
            data: {
                attachIds: attachIds
            },
            success: function (data) {
                if (data.status == 200) {
                    var datalist = data.list;
                    for (var i = 0; i < datalist.length; i++) {
                        html += "<a onclick=\"openFileOnLine('" + datalist[i].extName + "','" + datalist[i].attachId + "','1')\" href=\"javascript:void(0);\" class=\"btn btn-palegreen btn-xs\">" + datalist[i].oldName + "</a>";
                    }
                } else if (data.status == 100) {
                    console.log(data.msg);
                } else {
                    console.log(data.msg);
                }
            }
        });
    }
    return html;
}


function openFileOnLineTaoHong(extName, attachId, priv) {
    var extName = extName.toUpperCase();
    if (extName == ".TXT" || extName == ".HTML") {
        window.open("/sys/file/readFile?attachId=" + attachId, "_blank");
    } else if (extName == ".DOC" || extName == ".DOT" || extName == ".DOCX" || extName == ".DOTX" || extName == ".WPS" || extName == ".UOF")//
    {
        if (officetype == "0") {
            POBrowser.openWindowModeless("/app/core/document/taohongword?runId=" + runId + "&attachId=" + attachId + "&openModeType=" + priv, "width=1200px;height=800px;");
        } else if (officetype == "1") {
            window.open("/office/goldgrid/openword?runId=" + runId + "&attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "2") {
            window.open("/office/wps/openword?runId=" + runId + "&attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "3") {
            window.open("/v1/weboffice/index?attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "4") {
            window.open("/app/core/onlyoffice?attachId=" + attachId + "&openModeType=" + priv + "&fileId=" + fileId);
        }
    } else if (extName == ".XLS" || extName == ".XLSX" || extName == ".CSV" || extName == ".ET" || extName == ".ETT") {
        if (officetype == "0") {
            POBrowser.openWindowModeless("/office/openexcel?attachId=" + attachId + "&openModeType=" + priv, "width=1200px;height=800px;");
        } else if (officetype == "1") {
            window.open("/office/goldgrid/openexcel?runId=" + runId + "&attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "2") {
            window.open("/office/wps/openexcel?runId=" + runId + "&attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "3") {
            window.open("/v1/weboffice/index?attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "4") {
            window.open("/app/core/onlyoffice?attachId=" + attachId + "&openModeType=" + priv + "&fileId=" + fileId);
        }
    } else if (extName == ".PPT" || extName == ".PPTX" || extName == ".DPS" || extName == ".DPT") {
        if (officetype == "0") {
            POBrowser.openWindowModeless("/office/openppt?attachId=" + attachId + "&openModeType=" + priv, "width=1200px;height=800px;");
        } else if (officetype == "1") {
            window.open("/office/goldgrid/openword?runId=" + runId + "&attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "2") {
            window.open("/office/wps/openppt?runId=" + runId + "&attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "3") {
            window.open("/v1/weboffice/index?attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "4") {
            window.open("/app/core/onlyoffice?attachId=" + attachId + "&openModeType=" + priv + "&fileId=" + fileId);
        }
    } else if (extName == ".PDF") {
        if (officetype == "0") {
            POBrowser.openWindowModeless("/office/openpdf?attachId=" + attachId + "&openModeType=" + priv, "width=1200px;height=800px;");
        } else if (officetype == "1") {
            window.open("/office/goldgrid/openword?attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "2") {
            if (ofdtype == "1") {
                window.open("/office/ofd/openfxofd?attachId=" + attachId + "&openModeType=" + priv);
            } else if (ofdtype == "2") {
                window.open("/office/ofd/openswofd?attachId=" + attachId + "&openModeType=" + priv);
            } else {
                window.open("/module/pdfjs/web/viewer.html?file=" + encodeURIComponent("/sys/file/openFileOnLine?attachId=" + attachId), "_blank");
            }
        } else if (officetype == "3") {
            window.open("/v1/weboffice/index?attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "4") {
            window.open("/app/core/onlyoffice?attachId=" + attachId + "&openModeType=" + priv + "&fileId=" + fileId);
        }
    } else if (extName == '.JPG' || extName == '.PNG' || extName == '.JPEG' || extName == '.BMP' || extName == '.TIT' || extName == '.GIF') {
        window.open("/sys/file/getImage?attachId=" + attachId, "_blank");
    } else if (extName == ".OFD") {
        if (ofdtype == "1") {
            window.open("/office/ofd/openfxofd?attachId=" + attachId + "&openModeType=" + priv);
        } else if (ofdtype == "2") {
            window.open("/office/ofd/openswofd?attachId=" + attachId + "&openModeType=" + priv);
        } else {
            window.open("/module/ofd/index.html?ofdFile=" + encodeURIComponent("/sys/file/openFileOnLine?attachId=" + attachId), "_blank");
        }

    } else {
        layer.msg("文件格式不支持在线打开！");
    }
}

/**
 * 在线打开文件
 * @param extName
 * @param attachId
 * @param priv
 * @returns
 */
function openFileOnLine(extName, attachId, priv, fileId) {
    fileId = fileId || '';
    var extName = extName.toUpperCase();
    if (extName == ".TXT" || extName == ".HTML") {
        window.open("/sys/file/readFile?attachId=" + attachId, "_blank");
    } else if (extName == ".DOC" || extName == ".DOT" || extName == ".DOCX" || extName == ".DOTX" || extName == ".WPS" || extName == ".UOF")//
    {
        if (officetype == "0") {
            POBrowser.openWindowModeless("/office/openword?attachId=" + attachId + "&openModeType=" + priv, "width=1200px;height=800px;");
        } else if (officetype == "1") {
            window.open("/office/goldgrid/openword?attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "2") {
            window.open("/office/wps/openword?attachId=" + attachId + "&openModeType=" + priv + "&fileId=" + fileId);
        }else if (officetype == "3") {
            window.open("/v1/weboffice/index?attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "4") {
            window.open("/app/core/onlyoffice?attachId=" + attachId + "&openModeType=" + priv + "&fileId=" + fileId);
        }
    } else if (extName == ".XLS" || extName == ".XLSX" || extName == ".CSV" || extName == ".ET" || extName == ".ETT") {
        if (officetype == "0") {
            POBrowser.openWindowModeless("/office/openexcel?attachId=" + attachId + "&openModeType=" + priv, "width=1200px;height=800px;");
        } else if (officetype == "1") {
            window.open("/office/goldgrid/openexcel?attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "2") {
            window.open("/office/wps/openexcel?attachId=" + attachId + "&openModeType=" + priv + "&fileId=" + fileId);
        }else if (officetype == "3") {
            window.open("/v1/weboffice/index?attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "4") {
            window.open("/app/core/onlyoffice?attachId=" + attachId + "&openModeType=" + priv + "&fileId=" + fileId);
        }
    } else if (extName == ".PPT" || extName == ".PPTX" || extName == ".DPS" || extName == ".DPT") {
        if (officetype == "0") {
            POBrowser.openWindowModeless("/office/openppt?attachId=" + attachId + "&openModeType=" + priv, "width=1200px;height=800px;");
        } else if (officetype == "1") {
            window.open("/office/goldgrid/openword?attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "2") {
            window.open("/office/wps/openppt?attachId=" + attachId + "&openModeType=" + priv + "&fileId=" + fileId);
        }else if (officetype == "3") {
            window.open("/v1/weboffice/index?attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "4") {
            window.open("/app/core/onlyoffice?attachId=" + attachId + "&openModeType=" + priv + "&fileId=" + fileId);
        }
    } else if (extName == ".PDF") {
        if (officetype == "0") {
            POBrowser.openWindowModeless("/office/openpdf?attachId=" + attachId + "&openModeType=" + priv, "width=1200px;height=800px;");
        } else if (officetype == "1") {
            window.open("/office/goldgrid/openword?attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "2") {
            if (ofdtype == "1") {
                window.open("/office/ofd/openfxofd?attachId=" + attachId + "&openModeType=" + priv);
            } else {
                window.open("/module/pdfjs/web/viewer.html?file=" + encodeURIComponent("/sys/file/getFileDown?attachId=" + attachId), "_blank");
            }
        } else if (officetype == "3") {
            window.open("/v1/weboffice/index?attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "4") {
            window.open("/app/core/onlyoffice?attachId=" + attachId + "&openModeType=" + priv + "&fileId=" + fileId);
        }
    } else if (extName == '.JPG' || extName == '.PNG' || extName == '.JPEG' || extName == '.BMP' || extName == '.TIT' || extName == '.GIF') {
        window.open("/sys/file/getImage?attachId=" + attachId, "_blank", "location=no");
    } else if (extName == ".OFD") {
        if (ofdtype == "1") {
            window.open("/office/ofd/openfxofd?attachId=" + attachId + "&openModeType=" + priv, "_blank", "location=no");
        } else if (ofdtype == "2") {
            window.open("/office/ofd/openswofd?attachId=" + attachId + "&openModeType=" + priv, "_blank", "location=no");
        } else {
            window.open("/module/ofd/index.html?ofdFile=" + encodeURIComponent("/sys/file/getFileDown?attachId=" + attachId), "_blank", "location=no");
        }
    } else {
        layer.msg("文件格式不支持在线打开！");
    }
}


function openNetDiskFileOnLine(extName, netDiskId, path) {
    var priv = 1;
    var attachId = "";
    var extName = extName.toUpperCase();
    if (extName == ".TXT" || extName == ".HTML") {
        window.open("/sys/file/readFile?attachId=" + attachId + "&netDiskId=" + netDiskId + "&path=" + encodeURIComponent(path), "_blank");
    } else if (extName == ".DOC" || extName == ".DOT" || extName == ".DOCX" || extName == ".DOTX") {
        if (officetype == "0") {
            POBrowser.openWindowModeless("/office/openword?attachId=" + attachId + "&openModeType=" + priv + "&netDiskId=" + netDiskId + "&path=" + path, "width=1200px;height=800px;");
        }else if(officetype=="1")
        {
            //金格
        }else if(officetype=="2")
        {
            window.open("/office/wps/openword?attachId=&openModeType=" + priv + "&fileId=&netDiskId=" + netDiskId + "&path=" + encodeURIComponent(path));
        }else if (officetype == "3") {
            window.open("/v1/weboffice/index?attachId=" + encodeURIComponent(path) + "&openModeType=" + priv + "&netDiskId=" + netDiskId + "&isNetDisk=1");
        } else if (officetype == "4") {
            window.open("/app/core/onlyoffice?attachId=" + encodeURIComponent(path) + "&openModeType=" + priv + "&netDiskId=" + netDiskId + "&isNetDisk=1");
        }
    } else if (extName == ".XLS" || extName == ".XLSX" || extName == ".CSV") {
        if (officetype == "0") {
            POBrowser.openWindowModeless("/office/openexcel?attachId=" + attachId + "&openModeType=" + priv + "&netDiskId=" + netDiskId + "&path=" + path, "width=1200px;height=800px;");
        }else if(officetype=="1")
        {
        //金格
        }else if(officetype=="2")
        {
            window.open("/office/wps/openexcel?attachId=" + attachId + "&openModeType=" + priv + "&fileId=&netDiskId=" + netDiskId + "&path=" + encodeURIComponent(path));
        }else if (officetype == "3") {
            window.open("/v1/weboffice/index?attachId=" + encodeURIComponent(path) + "&openModeType=" + priv + "&netDiskId=" + netDiskId + "&isNetDisk=1");
        } else if (officetype == "4") {
            window.open("/app/core/onlyoffice?attachId=" + encodeURIComponent(path) + "&openModeType=" + priv + "&netDiskId=" + netDiskId + "&isNetDisk=1");
        }
    } else if (extName == ".PPT" || extName == ".PPTX") {
        if (officetype == "0") {
            POBrowser.openWindowModeless("/office/openppt?attachId=" + attachId + "&openModeType=" + priv + "&netDiskId=" + netDiskId + "&path=" + path, "width=1200px;height=800px;");
        }else if(officetype=="1")
        {
            //金格
        }else if(officetype=="2")
        {
            window.open("/office/wps/openppt?attachId=" + attachId + "&openModeType=" + priv + "&fileId=&netDiskId=" + netDiskId + "&path=" + encodeURIComponent(path));
        }else if (officetype == "3") {
            window.open("/v1/weboffice/index?attachId=" + encodeURIComponent(path) + "&openModeType=" + priv + "&netDiskId=" + netDiskId + "&isNetDisk=1");
        } else if (officetype == "4") {
            window.open("/app/core/onlyoffice?attachId=" + encodeURIComponent(path) + "&openModeType=" + priv + "&netDiskId=" + netDiskId + "&isNetDisk=1");
        }
    } else if (extName == ".PDF") {
        if (officetype == "0") {
            POBrowser.openWindowModeless("/office/openpdf?attachId=" + attachId + "&openModeType=" + priv + "&netDiskId=" + netDiskId + "&path=" + path, "width=1200px;height=800px;");
        }else if(officetype=="1")
        {
            //金格
        }else if(officetype=="2")
        {
            window.open("/module/pdfjs/web/viewer.html?file=" + encodeURIComponent("/sys/file/getFileDown?attachId=" + attachId+"&netDiskId=" + netDiskId + "&path=" + encodeURIComponent(path)), "_blank");
        }else if (officetype == "3") {
            window.open("/v1/weboffice/index?attachId=" + encodeURIComponent(path) + "&openModeType=" + priv + "&netDiskId=" + netDiskId + "&isNetDisk=1");
        } else if (officetype == "4") {
            window.open("/app/core/onlyoffice?attachId=" + encodeURIComponent(path) + "&openModeType=" + priv + "&netDiskId=" + netDiskId + "&isNetDisk=1");
        }
    } else if (extName == '.JPG' || extName == '.PNG' || extName == '.JPEG' || extName == '.BMP' || extName == '.TIT' || extName == '.GIF') {
        window.open("/sys/file/getImage?attachId=" + attachId + "&netDiskId=" + netDiskId + "&path=" + encodeURIComponent(path), "_blank");
    } else {
        layer.msg("文件格式不支持在线打开！");
    }
}

function showFileVersionList(attachId) {
    $("#fileVersionListModal").remove();
    $("body").append(fileVersionHtml);
    $.ajax({
        url: "/ret/fileget/getAttachVersionList",
        type: "post",
        dataType: "json",
        data: {
            attachId: attachId
        },
        success: function (data) {
            if (data.status == "200") {
                var fileList = data.list;
                for (var i = 0; i < fileList.length; i++) {
                    var html = "";
                    html += "<tr>";
                    html += "<td>" + (i + 1) + "</td>";
                    html += "<td>" + fileList[i].newName + "</td>";
                    html += "<td>" + fileList[i].version + "</td>";
                    html += "<td>" + fileList[i].createUserName + "</td>";
                    html += "<td>" + fileList[i].upTime + "</td>";
                    html += "<td>";
                    if (officetype != "3" && officetype != "4") {
                        html += "<a href=\"javascript:window.open('/app/core/previewonline?attachId=" + fileList[i].attachId + "');\">预览 </a>";
                    } else {
                        html += "<a href=\"javascript:openFileOnLine('" + fileList[i].extName + "','" + fileList[i].attachId + "',1);\">查看 </a>";
                    }
                    html += "<a href=\"/sys/file/getFileDown?attachId=" + fileList[i].attachId + "\">下载</a></td>";
                    html += "</tr>";
                    $("#fileversiontbody").append(html);
                }
                $("#fileVersionListModal").modal("show");
            } else if (data.status == "100") {
                layer.msg(data.msg)
            } else {
                console.log(data.msg)
            }
        }
    })
}

var fileVersionHtml = ['<div class="modal fade bs-example-modal-lg" id="fileVersionListModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">',
    '		<div class="modal-dialog modal-lg">',
    '			<div class="modal-content">',
    '				<div class="modal-header">',
    '					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>',
    '					<h4 class="modal-title" id="myLargeModalLabel">文件历史版本列表</h4>',
    '				</div>',
    '				<div class="modal-body" style="padding:0px">',
    '						<table class="table table-hover table-striped table-bordered">',
    '						<thead>',
    '							<tr>',
    '							<th>序号</th>',
    '							<th>文件名</th>',
    '							<th>版本号</th>',
    '							<th>创建人</th>',
    '							<th>创建时间</th>',
    '							<th>操作</th>',
    '							</tr>',
    '						</thead>',
    '						<tbody id="fileversiontbody">',
    '						</tbody>',
    '						</table>',
    '				</div>',
    '				<div class="modal-footer">',
    '					<button type="button" class="btn btn-warning" data-dismiss="modal">取消</button>',
    '				</div>',
    '			</div>',
    '		</div>',
    '	</div>',
    '	'].join("");
