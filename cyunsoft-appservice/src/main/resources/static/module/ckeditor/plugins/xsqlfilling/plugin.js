CKEDITOR.plugins.add('xsqlfilling', {
    init: function (editor) {
        editor.addCommand('xsqlfilling', {
            exec: function () {
                $("#xplugin").html(xsqlfillingmodal);
                if (bpmfield == "auto") {
                    $(".autodiv").each(function () {
                        $(this).remove();
                    });
                }
                getDbSource();
                let selection = top.editor.getSelection();
                let element = selection.getSelectedElement();
                if (element) {
                    let xtype = element.getAttribute('xtype');
                    if (xtype != "xsqlfilling") {
                        return;
                    }
                    $("#name").val(element.getAttribute('name'));
                    $("#name").attr("readonly", "readonly");
                    $("#dataType").val(element.getAttribute('dataType'));
                    $("#style").val(element.getAttribute('style'));
                    $("#defaultValue").val(element.getAttribute('defaultValue'));
                    $("#title").val(element.getAttribute('title'));
                    let model = element.getAttribute('model');
                    let json = JSON.parse(model);
                    $("#dbSource").val(json.dbSource);
                    $("#sql").val(json.sql);
                } else {
                    if (bpmfield == "auto") {
                        let content = editor.getData();
                        $("#name").attr("readonly", "readonly");
                        let maxName = 0;
                        $(content).find('*[xtype]').each(function () {
                            let thisName = $(this).attr("name");
                            let thisCount = thisName.substring(7, thisName.length);
                            if (parseInt(thisCount) > maxName) {
                                maxName = parseInt(thisCount);
                            }
                        })
                        $("#name").val(getFieldNameAuto(maxName));
                    }
                }
                $(".js-saveinputbtn").unbind('click').click(function () {
                    let name = $("#name").val();
                    let dataType = $("#dataType").val();
                    let style = $("#style").val();
                    let defaultValue = $("#defaultValue").val();
                    let title = $("#title").val();
                    let dbSource = $("#dbSource").val();
                    let sql = $("#sql").val().replace(/\'/g, "&#39;");
                    let json = {};
                    json.dbSource = dbSource;
                    json.sql = sql;
                    let modelStr = JSON.stringify(json)
                    let html = "<input type='text' " +
                        "xtype='xsqlfilling' " +
                        "title='" + title + "' " +
                        "name='" + name + "' " +
                        "dataType='" + dataType + "' " +
                        "model='" + modelStr + "' " +
                        "defaultValue='" + defaultValue + "' " +
                        "value='" + defaultValue + "' " +
                        "style='" + style + "' " +
                        "class='form-control' " +
                        "placeholder='自动回填|" + title + "'/>";
                    if (title == "") {
                        layer.msg("字段标题不能为空!");
                        return false;
                    }
                    if ($("#name").attr("readonly") == "readonly") {
                        editor.insertHtml(html);
                        $("#xmodal").modal("hide");
                    } else {
                        let content = editor.getData();
                        $(content).find('*[xtype]').each(function () {
                            fieldList[$(this).attr("name")] = $(this).attr("title");
                        });
                        if (fieldList.hasOwnProperty(name)) {
                            layer.msg(title + "字段已存在,请检查!");
                            return false;
                        }
                        editor.insertHtml(html);
                        $("#xmodal").modal("hide");
                    }
                });
                $('#xmodal').modal("show");
            }
        });
        if (editor.addMenuItems) {
            editor.addMenuItems({
                xsqlfilling: {
                    label: "自动回填",
                    command: 'xsqlfilling',
                    group: 'xsqlfilling',
                    order: 1
                }
            });
        }
        if (editor.contextMenu) {
            editor.contextMenu.addListener(function (element, selection) {
                if (!element || element.isReadOnly()) return null;
                let isInput = element.hasAscendant('input', 1);
                if (isInput && element.getAttribute('xtype') == "xsqlfilling") {
                    return {
                        xsqlfilling: CKEDITOR.TRISTATE_OFF
                    };
                }
                return null;
            });
        }
    }
});

function getDbSource() {
    $.ajax({
        url: "/ret/sysget/getDbSourceList",
        type: "POST",
        dataType: "json",
        async: false,
        success: function (data) {
            if (data.status == 200) {
                let html = "<option value='SYS_DB'>当前系统</option>";
                for (let i = 0; i < data.list.length; i++) {
                    html += "<option value='" + data.list[i].dbSourceId + "'>" + data.list[i].dbSourceName + "</option>";
                }
                $("#dbSource").html(html);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}


var xsqlfillingmodal = ['	 <div class="modal fade bs-example-modal-lg" id="xmodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">',
    '        <div class="modal-dialog">',
    '            <div class="modal-content">',
    '                <div class="modal-header">',
    '                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>',
    '                    <h4 class="modal-title" id="myLargeModalLabel">自动回填组件</h4>',
    '                </div>',
    '                <div class="modal-body">',
    '                <form class="form-horizontal" role="form" id="xsqlfillingform">',
    '                	<div class="form-group">',
    '	                    <label class="col-sm-2 control-label no-padding-right">控件标题</label>',
    '	                    <div class="col-sm-10">',
    '	                    <input type="text" class="form-control" id="title" name="title" placeholder="控件标题">',
    '	                    </div>',
    '	                </div>',
    '	                <div class="form-group">',
    '	                    <label class="col-sm-2 control-label no-padding-right">字段名称</label>',
    '	                    <div class="col-sm-10">',
    '	                    <input type="text" class="form-control" id="name" name="name" placeholder="字段名称">',
    '	                </div>',
    '	                </div>',
    '	                <div class="form-group autodiv">',
    '	                    <label class="col-sm-2 control-label no-padding-right">字段类型</label>',
    '	                    <div class="col-sm-10">',
    '	                    <select class="form-control" id="dataType" name="dataType" placeholder="字段类型">',
    '							<option>请选择</option>',
    '							<option value="int">数值型</option>',
    '							<option value="varchar">字符型</option>',
    '							<option value="text">文本型</option>',
    '						</select>',
    '	                	</div>',
    '	                </div>',
    '	                <div class="form-group">',
    '	                    <label class="col-sm-2 control-label no-padding-right">初始值</label>',
    '	                    <div class="col-sm-10">',
    '	                    <input type="text" class="form-control" id="defaultValue" name="defaultValue" placeholder="初始值">',
    '	               		</div>',
    '	                </div>',
    '	                <div class="form-group">',
    '	                    <label class="col-sm-2 control-label no-padding-right">数据源</label>',
    '	                    <div class="col-sm-10">',
    '	                    <select id="dbSource" name="dbSource" style="border-radius: 0px"></select>',
    '	                </div>',
    '	                </div>',
    '	                <div class="form-group">',
    '	                    <label class="col-sm-2 control-label no-padding-right">执行SQL</label>',
    '	                    <div class="col-sm-10">',
    '	                    <textarea rows="5" class="form-control" id="sql" name="sql"></textarea>',
    '	                </div>',
    '	                </div>',
    '	                <div class="form-group">',
    '	                    <label class="col-sm-2 control-label no-padding-right">控件样式</label>',
    '	                    <div class="col-sm-10">',
    '	                    <textarea rows="5" class="form-control" id="style" name="style"></textarea>',
    '	                </div>',
    '	                </div>',
    '                </form>',
    '                </div>',
    '                <div class="modal-footer">',
    '                  <button type="button" class="btn btn-warning" data-dismiss="modal">取消</button>',
    '                 <button type="button" class="btn btn-primary js-saveinputbtn">确定</button>',
    '                </div>',
    '            </div>',
    '        </div>',
    '    </div>'].join("");
