CKEDITOR.plugins.add('xapproval', {
    init: function (editor) {
        editor.addCommand('xapproval', {
            exec: function () {
                $("#xplugin").html(xapprovalmodal);
                if (bpmfield == "auto") {
                    $(".autodiv").each(function () {
                        $(this).remove();
                    });
                }
                var selection = top.editor.getSelection();
                var element = selection.getSelectedElement();
                if (element) {
                    var xtype = element.getAttribute('xtype');
                    if (xtype != "xapproval") {
                        return;
                    }
                    $("#name").val(element.getAttribute('name'));
                    $("#name").attr("readonly", "readonly");
                    $("#dataType").val(element.getAttribute('dataType'));
                    $("#title").val(element.getAttribute('title'));
                } else {
                    if (bpmfield == "auto") {
                        var content = editor.getData();
                        $("#name").attr("readonly", "readonly");
                        var maxName = 0;
                        $(content).find('*[xtype]').each(function () {
                            var thisName = $(this).attr("name");
                            var thisCount = thisName.substring(7, thisName.length);
                            if (parseInt(thisCount) > maxName) {
                                maxName = parseInt(thisCount);
                            }
                        })
                        $("#name").val(getFieldNameAuto(maxName));
                    }
                }
                $(".js-saveinputbtn").unbind('click').click(function () {
                    var name = $("#name").val();
                    var dataType = $("#dataType").val();
                    var title = $("#title").val();
                    var html = "<textarea " +
                        "xtype='xtextarea' " +
                        "title='" + title + "' " +
                        "name='" + name + "' " +
                        "dataType='" + dataType + "' " +
                        "defaultValue='' " +
                        "style='width:100%;height:100px' " +
                        "class='form-control' " +
                        "placeholder='多行输入|" + title + "'></textarea>";
                    html += "<input type='text' " +
                        "xtype='xmacro' " +
                        "title='" + title + "签名' " +
                        "name='" + name + "A' " +
                        "dataType='" + dataType + "' " +
                        "defaultValue='' " +
                        "style='width:200px;text-align:center;float:right' " +
                        "model='{&quot;type&quot;:&quot;20&quot;,&quot;format&quot;:null}' " +
                        "class='form-control' " +
                        "placeholder='自动控件|" + title + "签名'/>";

                    if (title == "") {
                        layer.msg("字段标题不能为空!");
                        return false;
                    }
                    if ($("#name").attr("readonly") == "readonly") {
                        editor.insertHtml(html);
                        $("#xmodal").modal("hide");
                    } else {
                        var content = editor.getData();
                        $(content).find('*[xtype]').each(function () {
                            fieldList[$(this).attr("name")] = $(this).attr("title");
                        });
                        if (fieldList.hasOwnProperty(name)) {
                            layer.msg(title + "字段已存在,请检查!");
                            return false;
                        }
                        editor.insertHtml(html);
                        $("#xmodal").modal("hide");
                    }
                });
                $('#xmodal').modal("show");
            }
        });
        if (editor.addMenuItems) {
            editor.addMenuItems({
                xapproval: {
                    label: "审批控件属性",
                    command: 'xapproval',
                    group: 'xapproval',
                    order: 1
                }
            });
        }
        if (editor.contextMenu) {
            editor.contextMenu.addListener(function (element, selection) {
                if (!element || element.isReadOnly()) return null;
                var isInput = element.hasAscendant('textarea', 1);
                if (isInput && element.getAttribute('xtype') == "xapproval") {
                    return {
                        xapproval: CKEDITOR.TRISTATE_OFF
                    };
                }
                return null;
            });
        }
    }
});

var xapprovalmodal = ['	 <div class="modal fade bs-example-modal-lg" id="xmodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">',
    '        <div class="modal-dialog">',
    '            <div class="modal-content">',
    '                <div class="modal-header">',
    '                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>',
    '                    <h4 class="modal-title" id="myLargeModalLabel">审批控件</h4>',
    '                </div>',
    '                <div class="modal-body">',
    '                <form class="form-horizontal" role="form" id="xapprovalform">',
    '                	<div class="form-group">',
    '	                    <label class="col-sm-2 control-label no-padding-right">控件标题</label>',
    '	                    <div class="col-sm-10">',
    '	                    <input type="text" class="form-control" id="title" name="title" placeholder="控件标题">',
    '	                    </div>',
    '	                </div>',
    '	                <div class="form-group">',
    '	                    <label class="col-sm-2 control-label no-padding-right">字段名称</label>',
    '	                    <div class="col-sm-10">',
    '	                    <input type="text" class="form-control" id="name" name="name" placeholder="字段名称">',
    '	                </div>',
    '	                </div>',
    '	                <div class="form-group autodiv">',
    '	                    <label class="col-sm-2 control-label no-padding-right">字段类型</label>',
    '	                    <div class="col-sm-10">',
    '	                    <select class="form-control" id="dataType" name="dataType" placeholder="字段类型">',
    '							<option>请选择</option>',
    '							<option value="int">数值型</option>',
    '							<option value="varchar">字符型</option>',
    '							<option value="text">文本型</option>',
    '						</select>',
    '	                	</div>',
    '	                </div>',
    '                </form>',
    '                </div>',
    '                <div class="modal-footer">',
    '                  <button type="button" class="btn btn-warning" data-dismiss="modal">取消</button>',
    '                 <button type="button" class="btn btn-primary js-saveinputbtn">确定</button>',
    '                </div>',
    '            </div>',
    '        </div>',
    '    </div>'].join("");
