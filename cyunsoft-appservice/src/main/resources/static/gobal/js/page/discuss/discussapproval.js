$(function () {
    query();
})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/oaget/getTopDiscussRecordApprovalList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        search: true,//启用搜索
        sortOrder: "desc",
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'recordId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',//标题  可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'title',
            title: '标题',
            sortable: true,
            width: '150px'
        }, {
            field: 'subheading',
            title: '副标题',
            sortable: true,
            width: '200px'
        }, {
            field: 'editorUserName',
            width: '50px',
            title: '版主'
        }, {
            field: 'userName',
            width: '50px',
            title: '发帖人'
        }, {
            field: 'status',
            width: '50px',
            title: '审批状态',
            formatter: function (value, row, index) {
                if (value == "0") {
                    return "待审批";
                } else if (value == "1") {
                    return "通过";
                } else if (value == "2") {
                    return "未通过";
                } else if (value == "3") {
                    return "已暂停";
                } else {
                    return "未通过";
                }
            }
        }, {
            field: 'createTime',
            width: '100px',
            title: '发帖时间'
        }, {
            field: 'opt',
            width: '100px',
            align: 'center',
            title: '操作',
            formatter: function (value, row, index) {
                return createOptBtn(row.recordId, row.status);
            }
        }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list
                    //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order
    };
    return temp;
};

function createOptBtn(recordId, status) {
    var html = "<a href=\"javascript:void(0);readdeatils('" + recordId + "')\" class=\"btn btn-sky btn-xs\" >详情</a>&nbsp;&nbsp;";
    if (status == "0") {
        html += "<a href=\"javascript:void(0);approval('" + recordId + "')\" class=\"btn btn-success btn-xs\" >审批</a>"
    } else if (status == "1") {
        html += "<a href=\"javascript:void(0);setApprovalStatus('" + recordId + "','3')\" class=\"btn btn-darkorange btn-xs\" >关闭</a>";
    } else if (status == "3") {
        html += "<a href=\"javascript:void(0);setApprovalStatus('" + recordId + "','1')\" class=\"btn btn-success btn-xs\" >启用</a>"
    }
    return html;
}


function approval(recordId) {
    $("#approvermodal").modal("show");
    $(".js-app-not-pass").unbind("click").click(function () {
        setApprovalStatus(recordId, "0")
    })
    $(".js-app-pass").unbind("click").click(function () {
        setApprovalStatus(recordId, "1")
    })
}


function setApprovalStatus(recordId, status) {
    $.ajax({
        url: "/set/oaset/setDiscussRecordStatus",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId,
            status: status
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                layer.msg(sysmsg[data.msg]);
                $("#myTable").bootstrapTable('refresh');
                $("#approvermodal").modal("hide");
            }
        }
    });
}
