var zTree;
var setting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/fileget/getPublicFileFolderTree",// Ajax 获取数据的 URL 地址
        autoParam: ["folderId"],// 异步加载时需要自动提交父节点属性的参数
    },
    callback: {
        onClick: zTreeOnClick
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "folderId",
            pIdKey: "folderLevel",
            rootPId: "0"
        },
        key: {
            name: "folderName"
        }
    }
};

function zTreeOnClick(event, treeId, treeNode) {
    document.getElementById("form2").reset();
    document.getElementById("form3").reset();
    document.getElementById("form4").reset();
    document.getElementById("form5").reset();
    document.getElementById("form6").reset();
    $("#titdiv").hide();
    $("#privdiv").show();
    $("#accessUser").attr("data-value", "");
    $("#accessDept").attr("data-value", "");
    $("#accessLevel").attr("data-value", "");

    $("#downUser").attr("data-value", "");
    $("#downDept").attr("data-value", "");
    $("#downLevel").attr("data-value", "");

    $("#manageUser").attr("data-value", "");
    $("#manageDept").attr("data-value", "");
    $("#manageLevel").attr("data-value", "");

    $("#createUser").attr("data-value", "");
    $("#createDept").attr("data-value", "");
    $("#createLevel").attr("data-value", "");

    $("#folderId").val(treeNode.folderId);
    $("#folderNamespan").html("正在为：" + treeNode.folderName + " 设置权限");
    $.ajax({
        url: "/ret/fileget/getPublicFileFolderById",
        type: "post",
        dataType: "json",
        data: {folderId: treeNode.folderId},
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                console.log(data.msg);
            } else {
                if (data.list) {
                    $("#accessUser").attr("data-value", data.list.accessUserPriv);
                    $("#accessUser").val(getUserNameByStr(data.list.accessUserPriv));
                    $("#accessDept").attr("data-value", data.list.accessDeptPriv);
                    $("#accessDept").val(getDeptNameByDeptIds(data.list.accessDeptPriv));
                    $("#accessLevel").attr("data-value", data.list.accessLevelPriv);
                    $("#accessLevel").val(getUserLevelStr(data.list.accessLevelPriv));

                    $("#downUser").attr("data-value", data.list.downUserPriv);
                    $("#downUser").val(getUserNameByStr(data.list.downUserPriv));
                    $("#downDept").attr("data-value", data.list.downDeptPriv);
                    $("#downDept").val(getDeptNameByDeptIds(data.list.downDeptPriv));
                    $("#downLevel").attr("data-value", data.list.downLevelPriv);
                    $("#downLevel").val(getUserLevelStr(data.list.downLevelPriv));


                    $("#manageUser").attr("data-value", data.list.manageUserPriv);
                    $("#manageUser").val(getUserNameByStr(data.list.manageUserPriv));
                    $("#manageDept").attr("data-value", data.list.manageDeptPriv);
                    $("#manageDept").val(getDeptNameByDeptIds(data.list.manageDeptPriv));
                    $("#manageLevel").attr("data-value", data.list.manageLevelPriv);
                    $("#manageLevel").val(getUserLevelStr(data.list.manageLevelPriv));


                    $("#createUser").attr("data-value", data.list.createUserPriv);
                    $("#createUser").val(getUserNameByStr(data.list.createUserPriv));
                    $("#createDept").attr("data-value", data.list.createDeptPriv);
                    $("#createDept").val(getDeptNameByDeptIds(data.list.createDeptPriv));
                    $("#createLevel").attr("data-value", data.list.createLevelPriv);
                    $("#createLevel").val(getUserLevelStr(data.list.createLevelPriv));
                }
            }
        }
    });

}

$(function () {
    $.ajax({
        url: "/ret/fileget/getPublicFileFolderTree",
        type: "post",
        dataType: "json",
        data: {folderId: "0"},
        success: function (data) {
            zTree = $.fn.zTree.init($("#tree"), setting, data);// 初始化树节点时，添加同步获取的数据
//			var nodes = zTree.transformToArray(zTree.getNodes())
//			zTree.expandNode(nodes[0], true);
        }
    });
    $("#cbut").unbind("click").click(function () {
        document.getElementById("form1").reset();
        $("#createPublicFolder").modal("show");
        $(".js-save").unbind("click").click(function () {
            if ($("#folderName").val() == "") {
                layer.msg("文件夹名称不能为空！");
            } else {
                $.ajax({
                    url: "/set/fileset/createPublicFileFolder",
                    type: "post",
                    dataType: "json",
                    data: {
                        sortNo: $("#sortNo").val(),
                        folderName: $("#folderName").val(),
                        owner: $("#owner").attr("data-value"),
                        spaceLimit: $("#spaceLimit").val()
                    },
                    success: function (data) {
                        if (data.status == "500") {
                            console.log(data.msg);
                        } else if (data.status == "100") {
                            console.log(data.msg);
                        } else {
                            layer.msg(sysmsg[data.msg]);
                            location.reload();
                        }
                    }
                });
                $("#createPublicFolder").modal("hide");
            }
        });
    });
    $(".js-setpriv1").unbind("click").click(function () {
        var folderId = $("#folderId").val();
        if (folderId == "") {
            layer.msg("请选择一个需设置权限的文件夹！");
            return;
        }
        addPriv1(folderId);
    });
    $(".js-setpriv2").unbind("click").click(function () {
        var folderId = $("#folderId").val();
        if (folderId == "") {
            layer.msg("请选择一个需设置权限的文件夹！");
            return;
        }
        addPriv2(folderId);
    });
    $(".js-setpriv3").unbind("click").click(function () {
        var folderId = $("#folderId").val();
        if (folderId == "") {
            layer.msg("请选择一个需设置权限的文件夹！");
            return;
        }
        addPriv3(folderId);
    });
    $(".js-setpriv4").unbind("click").click(function () {
        var folderId = $("#folderId").val();
        if (folderId == "") {
            layer.msg("请选择一个需设置权限的文件夹！");
            return;
        }
        addPriv4(folderId);
    });
    $(".js-setpriv5").unbind("click").click(function () {
        var folderId = $("#folderId").val();
        if (folderId == "") {
            layer.msg("请选择一个需设置权限的文件夹！");
            return;
        }
        addPriv5(folderId);
    });
});

function addPriv1(folderId) {
    $.ajax({
        url: "/set/fileset/setPublicFilePriv",
        type: "post",
        dataType: "json",
        data: {
            folderId: folderId,
            accessUserPriv: $("#accessUser").attr("data-value"),
            accessDeptPriv: $("#accessDept").attr("data-value"),
            accessLevelPriv: $("#accessLevel").attr("data-value")
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                layer.msg(sysmsg[data.msg]);
            }
        }
    });
}

function addPriv2(folderId) {
    $.ajax({
        url: "/set/fileset/setPublicFilePriv",
        type: "post",
        dataType: "json",
        data: {
            folderId: folderId,
            downUserPriv: $("#downUser").attr("data-value"),
            downDeptPriv: $("#downDept").attr("data-value"),
            downLevelPriv: $("#downLevel").attr("data-value")
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                layer.msg(sysmsg[data.msg]);
            }
        }
    });
}

function addPriv3(folderId) {
    $.ajax({
        url: "/set/fileset/setPublicFilePriv",
        type: "post",
        dataType: "json",
        data: {
            folderId: folderId,
            manageUserPriv: $("#manageUser").attr("data-value"),
            manageDeptPriv: $("#manageDept").attr("data-value"),
            manageLevelPriv: $("#manageLevel").attr("data-value")
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                layer.msg(sysmsg[data.msg]);
            }
        }
    });
}

function addPriv4(folderId) {
    $.ajax({
        url: "/set/fileset/setPublicFilePriv",
        type: "post",
        dataType: "json",
        data: {
            folderId: folderId,
            createUserPriv: $("#createUser").attr("data-value"),
            createDeptPriv: $("#createDept").attr("data-value"),
            createLevelPriv: $("#createLevel").attr("data-value")
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                layer.msg(sysmsg[data.msg]);
            }
        }
    });
}


function addPriv5(folderId) {
    var optType = $('input:radio[name="optType"]:checked').val();
    if (optType == "1") {
        addpriv(folderId)
    } else if (optType == "2") {
        removepriv(folderId);
    }
}

function addpriv(folderId) {
    $.ajax({
        url: "/set/fileset/addPublicFolderPriv",
        type: "post",
        dataType: "json",
        data: {
            folderId: folderId,
            user: $("#user").attr("data-value"),
            dept: $("#dept").attr("data-value"),
            level: $("#level").attr("data-value"),
            range: getCheckBoxValue("range")
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                layer.msg(sysmsg[data.msg]);
            }
        }
    });
}

function removepriv(folderId) {
    $.ajax({
        url: "/set/fileset/removePublicFolderPriv",
        type: "post",
        dataType: "json",
        data: {
            folderId: folderId,
            user: $("#user").attr("data-value"),
            dept: $("#dept").attr("data-value"),
            level: $("#level").attr("data-value"),
            range: getCheckBoxValue("range")
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                layer.msg(sysmsg[data.msg]);
            }
        }
    });
}
