$(function () {
    getNextDayList()
})

function getNextDayList() {
    $.ajax({
        url: "/ret/vehicleget/getNextDayList",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data);
            }
        }
    });
}
