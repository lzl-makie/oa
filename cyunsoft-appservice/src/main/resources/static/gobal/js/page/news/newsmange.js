$(function () {
    query();
    $(".js-btn").unbind("click").click(function () {
        top.goUrl("/app/core/news");
    });
    $(".js-query-but").unbind("click").click(function () {
        $('#myTable').bootstrapTable('refresh');
    });
})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/oaget/getNewsManageList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        sortOrder: "desc",
        search: true,//启用搜索
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'newsId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        },
            {
                field: 'num',
                title: sysmsg['SORT_NO'],//标题  可不加
                width: '50px',
                formatter: function (value, row, index) {
                    return index + 1;
                }
            },
            {
                field: 'newsTitle',
                title: newsmsg['NEWS_TITLE'],
                sortable: true,
                width: '200px',
                formatter: function (value, row, index) {
                    return "<a href=\"javascript:void(0);readNews('" + row.newsId + "')\">" + value + "</a>";
                }
            },
            {
                field: 'subheading',
                title: '新闻摘要',
                sortable: true,
                width: '300px'
            },

            {
                field: 'newsType',
                width: '50px',
                title: newsmsg['NEWS_TYPE'],
                formatter: function (value, row, index) {
                    return getCodeClassName(value, "news");
                }
            },
            {
                field: 'delFlag',
                title: '是否删除',
                width: '50px',
                visible: false,
                formatter: function (value, row, index) {
                    if (value == "1") {
                        return "已删除";
                    } else {
                        return "未删除";
                    }

                }
            },
            {
                field: 'createTime',
                width: '100px',
                title: '创建时间'
            },
            {
                field: 'endTime',
                width: '100px',
                title: '终止时间'
            },
            {
                field: 'onclickCount',
                width: '50px',
                visible: false,
                title: '查看次数'
            },
            {
                field: 'createUser',
                width: '50px',
                title: '创建人'
            },
            {
                field: 'opt',
                title: '操作',
                align: 'center',
                width: '150px',
                formatter: function (value, row, index) {
                    return createOptBtn(row.newsId, row.status);
                }
            }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        status: $("#status").val()
    };
    return temp;
}

function createOptBtn(newsId, status) {
    let html = "<a href=\"javascript:void(0);edit('" + newsId + "')\" class=\"btn btn-primary btn-xs\" >" + sysmsg['OPT_EDIT'] + "</a>&nbsp;&nbsp;";
    if (status == "0") {
        html += "<a href=\"javascript:void(0);stopNews('" + newsId + "')\" class=\"btn btn-purple btn-xs\">终止</a>&nbsp;&nbsp;";
    } else {
        html += "<a href=\"javascript:void(0);startNews('" + newsId + "')\" class=\"btn btn-success btn-xs\">生效</a>&nbsp;&nbsp;";
    }
    html += "<a href=\"javascript:void(0);readStatus('" + newsId + "')\" class=\"btn btn-darkorange btn-xs\" >查阅情况</a>&nbsp;&nbsp;";
    html += "<a href=\"javascript:void(0);del('" + newsId + "')\" class=\"btn btn-darkorange btn-xs\" >" + sysmsg['OPT_DELETE'] + "</a>";
    return html;
}

function edit(newsId) {
    open("/app/core/news?view=edit&newsId=" + newsId, "_self");
}

function readStatus(newsId) {
    window.open("/app/core/news/readstatus?newsId=" + newsId);
}

function stopNews(newsId) {
    if (confirm("确定终止当前新闻吗？")) {
        $.ajax({
            url: "/set/oaset/stopNews",
            type: "post",
            dataType: "json",
            data: {newsId: newsId},
            success: function (data) {
                if (data.status == "500") {
                    console.log(data.msg);
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    layer.msg(sysmsg[data.msg]);
                    $('#myTable').bootstrapTable('refresh');
                }
            }
        })
    }
}

function startNews(newsId) {
    if (confirm("确定生效当前新闻吗？")) {
        $.ajax({
            url: "/set/oaset/startNews",
            type: "post",
            dataType: "json",
            data: {newsId: newsId},
            success: function (data) {
                if (data.status == "500") {
                    console.log(data.msg);
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    layer.msg(sysmsg[data.msg]);
                    $('#myTable').bootstrapTable('refresh');
                }
            }
        })
    }
}

function readNews(newsId) {
    window.open("/app/core/news/readnews?newsId=" + newsId);
}

function del(newsId) {
    if (confirm(sysmsg['OPT_CONFIRM_DELETE'])) {
        $.ajax({
            url: "/set/oaset/delNews",
            type: "post",
            dataType: "json",
            data: {newsId: newsId},
            success: function (data) {
                if (data.status == "500") {
                    console.log(data.msg);
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    layer.msg(sysmsg[data.msg]);
                    $('#myTable').bootstrapTable('refresh');
                }
            }
        })
    }
}
