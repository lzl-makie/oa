$(function () {
    jeDate("#beginTime", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTime", {
        format: "YYYY-MM-DD"
    });
    $("#myTable").bootstrapTable({
        url: '/ret/filemapget/searchMyFileMapFileList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        search: true,//启用搜索
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'recordId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [
            {
                field: 'num',
                title: '序号',//标题  可不加
                width: '50px',
                formatter: function (value, row, index) {
                    return index + 1;
                }
            },
            {
                field: 'titleName',
                title: '文档分类',
                width: '100px'
            },
            {
                field: 'fileName',
                width: '150px',
                title: '文件名称'
            },
            {
                field: 'extName',
                title: '文档格式',
                width: '50px'
            },
            {
                field: 'version',
                title: '当前版本',
                width: '100px'
            },
            {
                field: 'clickCount',
                width: '50px',
                title: '查阅次数'
            },
            {
                field: 'readStatus',
                width: '50px',
                title: '查阅状态',
                formatter: function (value, row, index) {
                    if (value == 0) {
                        return "未阅";
                    } else if (value > 0) {
                        return "已阅";
                    } else {
                        return "未阅";
                    }
                }
            },
            {
                field: 'createTime',
                width: '100px',
                title: '创建时间'
            },
            {
                field: 'fileStatus',
                width: '50px',
                title: '当前状态',
                formatter: function (value, row, index) {
                    if (value == "0") {
                        return "生效中";
                    } else if (value == "1") {
                        return "已作废";
                    }
                }
            },
            {
                field: 'createUserName',
                title: '创建人',
                width: '100px'
            },
            {
                field: 'opt',
                title: '操作',
                align: 'center',
                width: '100px',
                formatter: function (value, row, index) {
                    return createOptBtn(row.extName, row.attachId, row.recordId);
                }
            }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
})

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        itemId: itemId,
        readStatus: $("#readStatus").val(),
        beginTime: $("#beginTime").val(),
        endTime: $("#endTime").val()
    };
    return temp;
};

function createOptBtn(extName, attachId, recordId) {
    let html = "<a href=\"javascript:void(0);openFileMapOnLine('" + extName + "','" + attachId + "','1','','" + recordId + "')\" class=\"btn btn-sky btn-xs\" >查看</a>&nbsp;&nbsp;" +
        "<a href=\"javascript:window.open('/app/core/previewonline?attachId=" + attachId + "');\" class=\"btn btn-sky btn-xs\">预览 </a>";
    return html;
}

function openFileMapOnLine(extName, attachId, priv, fileId, recordId) {
    $.ajax({
        url: "/set/filemapset/setReadStatus",
        type: "post",
        dataType: "json",
        data: {recordId: recordId},
        success: function (data) {
            if (data.status == "200") {
                //layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
    fileId = fileId || '';
    var extName = extName.toUpperCase();
    if (extName == ".TXT" || extName == ".HTML") {
        window.open("/sys/file/readFile?attachId=" + attachId, "_blank");
    } else if (extName == ".DOC" || extName == ".DOT" || extName == ".DOCX" || extName == ".DOTX" || extName == ".WPS" || extName == ".UOF")//
    {
        if (officetype == "0") {
            POBrowser.openWindowModeless("/office/openword?attachId=" + attachId + "&openModeType=" + priv, "width=1200px;height=800px;");
        } else if (officetype == "1") {
            window.open("/office/goldgrid/openword?attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "2") {
            window.open("/office/wps/openword?attachId=" + attachId + "&openModeType=" + priv + "&fileId=" + fileId);
        } else if (officetype == "3") {
            window.open("/v1/weboffice/index?attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "4") {
            window.open("/app/core/onlyoffice?attachId=" + attachId + "&openModeType=" + priv + "&fileId=" + fileId);
        }
    } else if (extName == ".XLS" || extName == ".XLSX" || extName == ".CSV" || extName == ".ET" || extName == ".ETT") {
        if (officetype == "0") {
            POBrowser.openWindowModeless("/office/openexcel?attachId=" + attachId + "&openModeType=" + priv, "width=1200px;height=800px;");
        } else if (officetype == "1") {
            window.open("/office/goldgrid/openexcel?attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "2") {
            window.open("/office/wps/openexcel?attachId=" + attachId + "&openModeType=" + priv + "&fileId=" + fileId);
        } else if (officetype == "3") {
            window.open("/v1/weboffice/index?attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "4") {
            window.open("/app/core/onlyoffice?attachId=" + attachId + "&openModeType=" + priv + "&fileId=" + fileId);
        }
    } else if (extName == ".PPT" || extName == ".PPTX" || extName == ".DPS" || extName == ".DPT") {
        if (officetype == "0") {
            POBrowser.openWindowModeless("/office/openppt?attachId=" + attachId + "&openModeType=" + priv, "width=1200px;height=800px;");
        } else if (officetype == "1") {
            window.open("/office/goldgrid/openword?attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "2") {
            window.open("/office/wps/openppt?attachId=" + attachId + "&openModeType=" + priv + "&fileId=" + fileId);
        } else if (officetype == "3") {
            window.open("/v1/weboffice/index?attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "4") {
            window.open("/app/core/onlyoffice?attachId=" + attachId + "&openModeType=" + priv + "&fileId=" + fileId);
        }
    } else if (extName == ".PDF") {
        if (officetype == "0") {
            POBrowser.openWindowModeless("/office/openpdf?attachId=" + attachId + "&openModeType=" + priv, "width=1200px;height=800px;");
        } else if (officetype == "1") {
            window.open("/office/goldgrid/openword?attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "2") {
            if (ofdtype == "1") {
                window.open("/office/ofd/openfxofd?attachId=" + attachId + "&openModeType=" + priv);
            } else {
                window.open("/module/pdfjs/web/viewer.html?file=" + encodeURIComponent("/sys/file/getFileDown?attachId=" + attachId), "_blank");
            }
        } else if (officetype == "3") {
            window.open("/v1/weboffice/index?attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "4") {
            window.open("/app/core/onlyoffice?attachId=" + attachId + "&openModeType=" + priv + "&fileId=" + fileId);
        }
    } else if (extName == '.JPG' || extName == '.PNG' || extName == '.JPEG' || extName == '.BMP' || extName == '.TIT' || extName == '.GIF') {
        window.open("/sys/file/getImage?attachId=" + attachId, "_blank", "location=no");
    } else if (extName == ".OFD") {
        if (ofdtype == "1") {
            window.open("/office/ofd/openfxofd?attachId=" + attachId + "&openModeType=" + priv, "_blank", "location=no");
        } else if (ofdtype == "2") {
            window.open("/office/ofd/openswofd?attachId=" + attachId + "&openModeType=" + priv, "_blank", "location=no");
        } else {
            window.open("/module/ofd/index.html?ofdFile=" + encodeURIComponent("/sys/file/getFileDown?attachId=" + attachId), "_blank", "location=no");
        }
    } else {
        layer.msg("文件格式不支持在线打开！");
    }
}
