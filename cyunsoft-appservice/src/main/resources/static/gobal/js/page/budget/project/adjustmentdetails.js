$(function () {
    $.ajax({
        url: "/ret/budgetget/getBudgetAdjustmentById",
        type: "post",
        dataType: "json",
        data: {recordId: recordId},
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", recordInfo.attach);
                        createAttach("attach", 1);
                    } else if (id == "approvalUser" || id == "applyUser") {
                        $("#" + id).html(getUserNameByStr(recordInfo[id]));
                    } else if (id == "adjustType") {
                        if (recordInfo[id] == "1") {
                            $("#" + id).html("削减预算");
                        } else if (recordInfo[id] == "2") {
                            $("#" + id).html("增加预算");
                        }
                    } else if (id == "projectId") {
                        $.ajax({
                            url: "/ret/budgetget/getBudgetAdjustmentById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {projectId: recordInfo[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    $("#projectId").html(res.list.title);
                                } else if (res.status == "100") {
                                    layer.msg(sysmsg[res.msg]);
                                } else {
                                    console.log(res.msg);
                                }
                            }
                        });
                    } else if (id == "status") {
                        if (data.list.status == "0") {
                            $("#" + id).html("审批中");
                        } else if (data.list.status == "1") {
                            $("#" + id).html("通过");
                        } else if (data.list.status == "2") {
                            $("#" + id).html("未通过");
                        }

                    } else {
                        $("#" + id).html(recordInfo[id]);
                    }
                    if (data.list.adjustType == "1") {
                        $("#resTotalCost").html(parseFloat(data.list.oldTotalCost) - parseFloat(data.list.newTotalCost));
                    } else if (data.list.adjustType == "2") {
                        $("#resTotalCost").html(parseFloat(data.list.oldTotalCost) + parseFloat(data.list.newTotalCost));
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
});
