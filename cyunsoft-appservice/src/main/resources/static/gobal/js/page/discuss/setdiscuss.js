$(function () {
    getCodeClass("sortId", "discuss_type");
    $(".js-add").unbind("click").click(function () {
        $("#setdiscuss").modal("show");
        document.getElementById("form1").reset();
        $("#levelPriv").attr("data-value", "");
        $("#deptPriv").attr("data-value", "");
        $("#userPriv").attr("data-value", "");
        $("#edition").attr("data-value", "");
        $(".js-save").unbind("click").click(function () {
            insertDiscuess();
        })
    })
    query();
});

function query() {
    $("#myTable")
        .bootstrapTable(
            {
                url: '/ret/oaget/getDiscussList',
                method: 'post',
                contentType: 'application/x-www-form-urlencoded',
                toolbar: '#toobar',// 工具列
                striped: true,// 隔行换色
                cache: false,// 禁用缓存
                pagination: true,// 启动分页
                sidePagination: 'server',// 分页方式
                pageNumber: 1,// 初始化table时显示的页码
                pageSize: 10,// 每页条目
                showFooter: false,// 是否显示列脚
                showPaginationSwitch: true,// 是否显示 数据条数选择框
                sortable: true,// 排序
                search: false,// 启用搜索
                showColumns: true,// 是否显示 内容列下拉框
                showRefresh: true,// 显示刷新按钮
                idField: 'recordId',// key值栏位
                clickToSelect: true,// 点击选中checkbox
                pageList: [10, 20, 30, 50],// 可选择单页记录数
                queryParams: queryParams,
                columns: [
                    {
                        field: 'num',
                        title: '序号',// 标题 可不加
                        width: '50px',
                        formatter: function (value, row, index) {
                            return index + 1;
                        }
                    },
                    {
                        field: 'title',
                        width: '200px',
                        title: '版块名称'
                    },
                    {
                        field: 'sortId',
                        title: '版块类型',
                        width: '50px',
                        formatter: function (value, row, index) {
                            return getCodeClassName(value,
                                "discuss_type");
                        }
                    },
                    {
                        field: 'userName',
                        width: '100px',
                        title: '版主'
                    },
                    {
                        field: 'needApproval',
                        title: '是否需要审批',
                        width: '100px',
                        formatter: function (value, row, index) {
                            if (value == "1") {
                                return "<a href=\"javascript:void(0);\" class=\"btn btn-palegreen btn-xs\">需要</a>";
                            } else {
                                return "<a href=\"javascript:void(0);\" class=\"btn btn-darkorange btn-xs\">不需要</a>";
                            }
                        }
                    },
                    {
                        field: 'status',
                        title: '版块状态',
                        width: '100px',
                        formatter: function (value, row, index) {
                            if (value == "1") {
                                return "<a href=\"javascript:void(0);\" class=\"btn btn-palegreen btn-xs\">应用中</a>";
                            } else {
                                return "<a href=\"javascript:void(0);\" class=\"btn btn-darkorange btn-xs\">已停用</a>";
                            }
                        }
                    },
                    {
                        field: 'createTime',
                        width: '100px',
                        title: '创建时间'
                    },
                    {
                        field: 'opt',
                        title: '操作',
                        align: 'center',
                        width: '150px',
                        formatter: function (value, row, index) {
                            return createOptBtn(row.recordId,
                                row.status);
                        }
                    }],
                onClickCell: function (field, value, row, $element) {
                    // alert(row.SystemDesc);
                },
                responseHandler: function (res) {
                    if (res.status == "500") {
                        console.log(res.msg);
                    } else if (res.status == "100") {
                        layer.msg(sysmsg[res.msg]);
                    } else {
                        return {
                            total: res.list.total, // 总页数,前面的key必须为"total"
                            rows: res.list.list
                            // 行数据，前面的key要与之前设置的dataField的值一致.
                        };
                    }
                }
            });
}

function createOptBtn(recordId, status) {
    var html = "<a href=\"javascript:void(0);edit('" + recordId
        + "')\" class=\"btn btn-sky btn-xs\" >修改</a>&nbsp;&nbsp;"
        + "<a href=\"javascript:void(0);del('" + recordId
        + "')\" class=\"btn btn-darkorange btn-xs\" >删除</a>";
    if (status == "1") {
        html += "&nbsp;&nbsp;<a href=\"javascript:void(0);setstatus('"
            + recordId + "','0')\" class=\"btn btn-purple btn-xs\" >停用</a>";
    } else {
        html += "&nbsp;&nbsp;<a href=\"javascript:void(0);setstatus('"
            + recordId
            + "','1')\" class=\"btn btn-success btn-xs\" >启用</a>";
    }
    return html;
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order
    };
    return temp;
};

function setstatus(recordId, status) {
    $.ajax({
        url: "/set/oaset/updateDiscuss",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId,
            status: status
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                layer.msg(sysmsg[data.msg]);
                $("#myTable").bootstrapTable('refresh');
            }
        }
    });
}

function edit(recordId) {
    document.getElementById("form1").reset();
    $("#levelPriv").attr("data-value", "");
    $("#deptPriv").attr("data-value", "");
    $("#userPriv").attr("data-value", "");
    $("#edition").attr("data-value", "");
    $.ajax({
        url: "/ret/oaget/getDiscussById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                for (var name in data.list) {
                    if (name == "edition") {
                        $("#edition").attr("data-value", data.list[name]);
                        $("#edition").val(getUserNameByStr(data.list[name]));
                    } else if (name == "userPriv") {
                        $("#userPriv").attr("data-value", data.list[name]);
                        $("#userPriv").val(getUserNameByStr(data.list[name]));
                    } else if (name == "deptPriv") {
                        $("#deptPriv").attr("data-value", data.list[name]);
                        $("#deptPriv").val(
                            getDeptNameByDeptIds(data.list[name]));
                    } else if (name == "levelPriv") {
                        $("#levelPriv").attr("data-value", data.list[name]);
                        $("#levelPriv").val(getUserLevelStr(data.list[name]));
                    } else if (name == "needApproval") {
                        $(
                            "input:radio[name='needApproval'][value='"
                            + data.list[name] + "']").attr(
                            "checked", "checked");
                    } else if (name == "readPriv") {
                        $(
                            "input:radio[name='readPriv'][value='"
                            + data.list[name] + "']").attr(
                            "checked", "checked");
                    } else {
                        $("#" + name).val(data.list[name]);
                    }
                }
            }
        }
    });
    $("#setdiscuss").modal("show");
    $(".js-save").unbind("click").click(
        function () {
            $.ajax({
                url: "/set/oaset/updateDiscuss",
                type: "post",
                dataType: "json",
                data: {
                    recordId: recordId,
                    sortNo: $("#sortNo").val(),
                    title: $("#title").val(),
                    sortId: $("#sortId").val(),
                    edition: $("#edition").attr("data-value"),
                    userPriv: $("#userPriv").attr("data-value"),
                    deptPriv: $("#deptPriv").attr("data-value"),
                    levelPriv: $("#levelPriv").attr("data-value"),
                    needApproval: $(
                        "input:checkbox[name='needApproval']:checked")
                        .val(),
                    readPriv: $("input:radio[name='readPriv']:checked")
                        .val(),
                    remark: $("#remark").val()
                },
                success: function (data) {
                    if (data.status == "500") {
                        console.log(data.msg);
                    } else if (data.status == "100") {
                        layer.msg(sysmsg[data.msg]);
                    } else {
                        layer.msg(sysmsg[data.msg]);
                        $("#myTable").bootstrapTable('refresh');
                        $("#setdiscuss").modal("hide");
                    }
                }
            });
        });

}

function del(recordId) {
    if (confirm("确定删除当前版块吗？")) {
        $.ajax({
            url: "/set/oaset/deleteDiscuss",
            type: "post",
            dataType: "json",
            data: {
                recordId: recordId
            },
            success: function (data) {
                if (data.status == "500") {
                    console.log(data.msg);
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable('refresh');
                }
            }
        });
    } else {
        return;
    }
}

function insertDiscuess() {
    $.ajax({
        url: "/set/oaset/insertDiscuss",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            title: $("#title").val(),
            sortId: $("#sortId").val(),
            edition: $("#edition").attr("data-value"),
            userPriv: $("#userPriv").attr("data-value"),
            deptPriv: $("#deptPriv").attr("data-value"),
            levelPriv: $("#levelPriv").attr("data-value"),
            needApproval: $("input:checkbox[name='needApproval']:checked")
                .val(),
            readPriv: $("input:radio[name='readPriv']:checked").val(),
            remark: $("#remark").val()
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                layer.msg(sysmsg[data.msg]);
                $("#myTable").bootstrapTable('refresh');
                $("#setdiscuss").modal("hide");
            }
        }
    })
}
