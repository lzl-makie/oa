$(function () {
    $.ajax({
        url: "/ret/officesuppliesget/getofficeSuppliesById",
        type: "post",
        dataType: "json",
        data: {
            suppliesId: suppliesId
        },
        success: function (data) {
            if (data.status == 200) {
                for (name in data.list) {
                    if (name == "sortId") {
                        $("#" + name).html(getOfficeSupplesSortNameById(data.list[name]));
                    } else if (name == "ownDept") {
                        $("#" + name).html(getDeptNameByDeptIds(data.list[name]));
                    } else if (name == "unit") {
                        $("#" + name).html(getofficesuppliesunitbyid(data.list[name]));
                    } else {
                        $("#" + name).html(data.list[name]);
                    }
                }
            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
})


function getofficesuppliesunitbyid(untiId) {
    var returnStr = "";
    $.ajax({
        url: "/ret/officesuppliesget/getofficeSuppliesUnitById",
        type: "post",
        dataType: "json",
        async: false,
        data: {
            unitId: untiId

        },
        success: function (data) {
            if (data.status == 200) {
                if (data.list.znName != null && data.list.znName != "") {
                    returnStr = data.list.cnName + "|" + data.list.znName;
                } else {
                    returnStr = data.list.cnName;
                }
            } else {
                console.log(data.msg);
            }
        }
    });
    return returnStr;
}

function getOfficeSupplesSortNameById(sortId) {
    var returnStr = "";
    $.ajax({
        url: "/ret/officesuppliesget/getofficeSuppliesSortById",
        type: "post",
        dataType: "json",
        async: false,
        data: {
            sortId: sortId

        },
        success: function (data) {
            if (data.status == 200) {
                returnStr = data.list.sortName;
            } else {
                console.log(data.msg);
            }
        }
    });
    return returnStr;
}
