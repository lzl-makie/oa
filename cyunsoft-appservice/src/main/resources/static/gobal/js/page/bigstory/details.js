$(function () {
    $.ajax({
        url: "/ret/oaget/getBigStoryById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", recordInfo.attach);
                        createAttach("attach", 1);
                    } else if (id == "type") {
                        $("#type").html(getCodeClassName(recordInfo[id], "bigStory"));
                    } else if (id == "mainPic") {
                        $("#file_img").attr("src", "/sys/file/getStaticImg?r=" + Math.random() + "&module=bigstory&fileName=" + recordInfo[id] + "")
                    } else {
                        $("#" + id).html(recordInfo[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})
