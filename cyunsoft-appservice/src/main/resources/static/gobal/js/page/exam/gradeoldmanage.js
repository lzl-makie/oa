$(function () {
    query();
    jeDate("#beginTime", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTime", {
        format: "YYYY-MM-DD"
    });
    $(".js-query-but").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    });
    getExamTestListForSelect();
})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/examget/getExamGradeOldList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        search: true,//启用搜索
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'recordId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        },
            {
                field: 'num',
                title: '序号',//标题  可不加
                width: '50px',
                formatter: function (value, row, index) {
                    return index + 1;
                }
            },
            {
                field: 'examTestTitle',
                title: '人员测试标题',
                sortable: true,
                width: '200px'
            },
            {
                field: 'testUserNmae',
                width: '50px',
                title: '参考人'
            },
            {
                field: 'beginTime',
                width: '100px',
                title: '考试时间'
            }, {
                field: 'endTime',
                width: '100px',
                title: '交卷时间'
            }, {
                field: 'passMark',
                width: '100px',
                title: '合格线'
            },
            {
                field: 'grade',
                width: '50px',
                title: '当前成绩',
                align: 'center',
                formatter: function (value, row, index) {
                    if (value >= row.passMark) {
                        return "<a href=\"javascript:void(0);\" class=\"btn btn-success btn-xs\">" + value + "分</a>"
                    } else {
                        return "<a href=\"javascript:void(0);\" class=\"btn btn-darkorange btn-xs\">" + value + "分</a>"
                    }
                }
            }],
        onClickCell: function (field, value, row, $element) {
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        examTestId: $("#examTestId").val(),
        accountId: $("#accountId").attr("data-value"),
        beginTime: $("#beginTime").val(),
        endTime: $("#endTime").val()
    };
    return temp;
};

function getExamTestListForSelect() {
    $.ajax({
        url: "/ret/examget/getExamTestListForSelect",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                let infoList = data.list;
                $("#examTestId").append("<option value=''>全部</option>");
                for (let i = 0; i < infoList.length; i++) {
                    $("#examTestId").append("<option value='" + infoList[i].recordId + "'>" + infoList[i].title + "</option>");
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}
