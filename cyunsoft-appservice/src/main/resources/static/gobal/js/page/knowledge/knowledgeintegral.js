var onlineuserlist;
var zTree;
var setting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/unitget/getUnitDeptForUserInfoTree",// Ajax 获取数据的 URL 地址
        autoParam: ["deptId"],// 异步加载时需要自动提交父节点属性的参数
    },
    callback: {
        onExpand: function (event, treeId, treeNode) {
            var deptId = treeNode.deptId;
            if (treeNode.isParent) {
                $.ajax({
                    url: "/ret/unitget/getSelectUserByDeptId",
                    type: "post",
                    data: {
                        deptId: deptId
                    },
                    dataType: "json",
                    success: function (data) {
                        var appNode = [];
                        if (data.list.length > 0) {
                            for (var i = 0; i < data.list.length; i++) {
                                var newnode = {};
                                newnode.deptId = data.list[i].accountId;
                                newnode.deptName = data.list[i].userName;
                                newnode.isParent = false;
                                if (data.list[i].sex == '男') {
                                    var result = $.inArray(data.list[i].accountId, onlineuserlist);
                                    if (result > -1) {
                                        newnode.icon = '/gobal/img/org/U01.png';
                                    } else {
                                        newnode.icon = '/gobal/img/org/U00.png';
                                    }
                                } else if (data.list[i].sex == '女') {
                                    var result = $.inArray(data.list[i].accountId, onlineuserlist);
                                    if (result > -1) {
                                        newnode.icon = '/gobal/img/org/U11.png';
                                    } else {
                                        newnode.icon = '/gobal/img/org/U10.png';
                                    }
                                }
                                appNode.push(newnode);

                            }
                            zTree.reAsyncChildNodes(treeNode, "refresh");
                            zTree.addNodes(treeNode, appNode);
                        }
                    }
                });
            }
        },
        onClick: zTreeOnClick
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "deptId",
            pIdKey: "orgLevelId",
            rootPId: "0"
        },
        key: {
            name: "deptName"
        }
    }
};

function zTreeOnClick(event, treeId, treeNode) {
    if (treeNode.isParent == false) {
        var accountId = treeNode.deptId;
        $("#myTable").bootstrapTable('destroy');
        querytable(accountId);
    }
}

$(document).ready(function () {
    var topNode = [{deptName: orgName, orgLevelId: '', isParent: "true", deptId: "0", icon: "/gobal/img/org/org.png"}];
    zTree = $.fn.zTree.init($("#tree"), setting, topNode);
    var nodes = zTree.transformToArray(zTree.getNodes());
    zTree.expandNode(nodes[0], true);
    querytable("");
});
$(function () {
    jeDate("#beginTime", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTime", {
        format: "YYYY-MM-DD"
    });
    $("#allrecord").unbind("click").click(function () {
        $("#myTable").bootstrapTable('destroy');
        querytable("");
    })
    $(".js-query-but").unbind("click").click(function () {
        $('#myTable').bootstrapTable('refresh');
    })
});

function querytable(createUser) {
    $("#myTable").bootstrapTable({
        url: '/ret/knowledgeget/getKnowledgeLearnList?createUser=' + createUser,
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        search: true,//启用搜索
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'learnId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        },
            {
                field: 'num',
                title: '序号',//标题  可不加
                width: '50px',
                formatter: function (value, row, index) {
                    return index + 1;
                }
            },
            {
                field: 'title',
                title: '知识文档标题',
                sortable: true,
                width: '150px'
            },
            {
                field: 'sortName',
                width: '100px',
                title: '文档分类'
            },
            {
                field: 'levelStar',
                width: '50px',
                title: '文档星级',
                formatter: function (value, row, index) {
                    return value + "星";
                }
            },
            {
                field: 'fileCreateName',
                width: '50px',
                title: '文档创建人'
            },
            {
                field: 'fileCreateTime',
                width: '100px',
                title: '文档创建时间'
            },
            {
                field: 'createUserName',
                width: '50px',
                title: '学习人'
            },
            {
                field: 'createTime',
                width: '100px',
                title: '学习时间'
            }
        ],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        beginTime: $("#beginTime").val(),
        endTime: $("#endTime").val()
    };
    return temp;
};
