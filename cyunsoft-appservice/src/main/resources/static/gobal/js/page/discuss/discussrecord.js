$(function () {
    $(".js-sendrecordbtn").unbind("click").click(function () {
        $("#setRecordModal").modal("show");
        $(".js-save").unbind("click").click(function () {
            sendDiscussRecord();
        })
    })
    $('#content').summernote({height: 300});
    query();
})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/oaget/getTopDiscussRecordList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        search: true,//启用搜索
        sortOrder: "desc",
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'recordId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        },
            {
                field: 'num',
                title: '序号',//标题  可不加
                width: '50px',
                formatter: function (value, row, index) {
                    return index + 1;
                }
            },
            {
                field: 'title',
                title: '标题',
                sortable: true,
                width: '150px'
            },
            {
                field: 'subheading',
                title: '副标题',
                sortable: true,
                width: '200px'
            },
            {
                field: 'editorUserName',
                width: '50px',
                title: '版主'
            },
            {
                field: 'userName',
                width: '50px',
                title: '发帖人'
            },
            {
                field: 'createTime',
                width: '100px',
                title: '发帖时间'
            },
            {
                field: 'opt',
                width: '150px',
                align: 'center',
                title: '操作',
                formatter: function (value, row, index) {
                    return createOptBtn(row.recordId, row.isMe);
                }
            }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        discussId: discussId
    };
    return temp;
};

function createOptBtn(recordId, isMe) {
    let html = "<a href=\"javascript:void(0);readdeatils('" + recordId + "')\" class=\"btn btn-sky btn-xs\" >浏览</a>&nbsp;&nbsp;";
    if (isMe) {
        html += "<a href=\"javascript:void(0);edit('" + recordId + "')\" class=\"btn btn-success btn-xs\" >编辑</a>&nbsp;&nbsp;" +
            "<a href=\"javascript:void(0);del('" + recordId + "')\" class=\"btn btn-darkorange btn-xs\" >删除</a>";
    }
    return html;
}

function readdeatils(recordId) {
    window.location = "/app/core/discuss/revdiscuss?recordId=" + recordId;
}

function del(recordId) {
    if (confirm(sysmsg['OPT_CONFIRM_DELETE'])) {
        $.ajax({
            url: "/set/oaset/deleteDiscussRecord",
            type: "post",
            dataType: "json",
            data: {
                recordId: recordId
            },
            success: function (data) {
                if (data.status == "500") {
                    console.log(data.msg);
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    layer.msg(sysmsg[data.msg]);1
                    $("#myTable").bootstrapTable("refresh");
                }
            }
        });
    }
}

function edit(recordId) {
    $.ajax({
        url: "/ret/oaget/getDiscussRecordById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                $("#title").val(data.list.title);
                $("#content").code(data.list.content);
                $("#setRecordModal").modal("show");
                $(".js-save").unbind("click").click(function () {
                    updateDiscussRecord(recordId);
                })
            }
        }
    });
}

function updateDiscussRecord(recordId) {
    $.ajax({
        url: "/set/oaset/updateDiscussRecord",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId,
            title: $("#title").val(),
            content: $("#content").code()
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                layer.msg(sysmsg[data.msg]);
                $("#setRecordModal").modal("hide");
                $("#myTable").bootstrapTable("refresh");
            }
        }
    });
}


function sendDiscussRecord() {
    $.ajax({
        url: "/set/oaset/insertDiscussRecord",
        type: "post",
        dataType: "json",
        data: {
            discussId: discussId,
            title: $("#title").val(),
            content: $("#content").code()
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                layer.msg(sysmsg[data.msg]);
                $("#setRecordModal").modal("hide");
                $("#myTable").bootstrapTable("refresh");
            }
        }
    });
}
