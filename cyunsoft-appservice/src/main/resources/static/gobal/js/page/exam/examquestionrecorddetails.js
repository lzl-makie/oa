$(function () {
    $.ajax({
        url: "/ret/examget/getExamQuestionRecordById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                let info = data.list;
                for (let id in info) {
                    if (id == "questionIds") {
                        getExamQuestionListByIds(info[id])
                    } else if (id == "remark") {
                        $("#remark").html(info[id]);
                    } else {
                        $("#" + id).html(info[id]);
                    }
                }
            }
        }
    })
})

function getExamQuestionListByIds(recordIds) {
    $.ajax({
        url: "/ret/examget/getExamQuestionListByIds",
        type: "post",
        dataType: "json",
        data: {
            recordIds: recordIds
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                let listInfo = data.list;
                for (let i = 0; i < listInfo.length; i++) {
                    let info = listInfo[i];
                    let index = $("#questionlist").find("tr").length;
                    let examType = "";
                    if (info.examType == "0") {
                        examType = "单选";
                    } else if (info.examType == "1") {
                        examType = "多选";
                    } else if (info.examType == "2") {
                        examType = "简述";
                    }
                    if (info.examType == "2") {
                        $("#questionlist").append("<tr data-value='" + info.recordId + "'><td>" + index + "</td><td>" + examType + "</td><td colspan='2'>" + info.title + "</td>" +
                            "</tr>");
                    } else {
                        let html = "";
                        let childItemArr = JSON.parse(info.content);
                        for (let i = 0; i < childItemArr.length; i++) {
                            html += childItemArr[i].optValue + "." + childItemArr[i].childTitle + "</br>";
                        }
                        $("#questionlist").append("<tr data-value='" + info.recordId + "'><td>" + index + "</td><td>" + examType + "</td><td>" + info.title + "</td>" +
                            "<td>" + html + "</td></tr>");

                    }
                }
            }
        }
    })
}
