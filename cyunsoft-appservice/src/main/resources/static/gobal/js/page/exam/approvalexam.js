$(function () {
    getExamGradeById();
    $(".js-approval").unbind("click").click(function () {
        approvalExamTest();
    })
})
let result;

function getExamGradeById() {
    $.ajax({
        url: "/ret/examget/getExamGradeById",
        type: "post",
        dataType: "json",
        data: {recordId: recordId},
        success: function (data) {
            if (data.status == "200") {
                result = data.list.result;
                getExamTestById(data.list.examTestId);
                $("#accountId").html(getUserNameByStr(data.list.accountId));
                $("#endTime").html(data.list.endTime);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function getExamTestById(examTestId) {
    $.ajax({
        url: "/ret/examget/getExamTestById",
        type: "post",
        dataType: "json",
        data: {recordId: examTestId},
        success: function (data) {
            if (data.status == "200") {
                getExamQuestionRecordById(data.list.questionRecordId);
                $("#title").html("《" + data.list.title + "》");
                $("#passMark").html("合格分数：" + data.list.passMark);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function getExamQuestionRecordById(questionRecordId) {
    $.ajax({
        url: "/ret/examget/getExamQuestionRecordById",
        type: "post",
        dataType: "json",
        data: {recordId: questionRecordId},
        success: function (data) {
            if (data.status == "200") {
                for (let id in data.list) {
                    if (id == "questionIds") {
                        getExamQuestionListByIds(data.list.questionIds);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function getExamQuestionListByIds(questionRecordId) {
    $.ajax({
        url: "/ret/examget/getExamQuestionListByIds",
        type: "post",
        dataType: "json",
        data: {
            recordIds: questionRecordId
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                let listInfo = data.list;
                for (let i = 0; i < listInfo.length; i++) {
                    let info = listInfo[i];
                    let index = $("#questionlist").find("tr").length;
                    let examType = "";
                    if (info.examType == "0") {
                        examType = "单选";
                    } else if (info.examType == "1") {
                        examType = "多选";
                    } else if (info.examType == "2") {
                        examType = "简述";
                    }
                    let childItemArr = [];
                    let html = "";
                    let html1 = "<div style='display: inline-block;font-size: 16px;font-weight: 600'>解答：</div>";
                    if (info.examType == "0" || info.examType == "1") {
                        childItemArr = JSON.parse(info.content);
                        html1 += "<div style='display: inline-block' id='id_" + info.recordId + "'></div><div style='float: right'>正确答案：" + info.answer + "</div>";
                    } else {
                        html1 += "<div class='js-answer' id='id_" + info.recordId + "'></div><div style='float: right'><span style='font-size: 16px;font-weight: 600'>得分：</span><input maxlength='3' style='display:" +
                            " inline-block;width:" +
                            " 100px'" +
                            " class='js-grade" +
                            " form-control'" +
                            " type='number'" +
                            " id='grade_" + info.recordId + "' value='0'></div>";
                    }
                    for (let i = 0; i < childItemArr.length; i++) {
                        html += "<div style='margin-top:5px;font-size: 14px;'>" + childItemArr[i].optValue + "、&nbsp;&nbsp;" + childItemArr[i].childTitle + "</div>";
                    }
                    $("#questionlist").append("<tr data-value='" + info.recordId + "'><td><div style='font-size: 16px;font-weight: 600;'>" + (index + 1) + "、【" + examType + "】" + info.title + "<span style='float: right;margin-right: 10px;'>分值：" + info.grade + "分</span></div>" +
                        "<div>" + html + "</div>" +
                        "<div>" + html1 + "</div></tr>");
                }
                let jsonRes = JSON.parse(result);
                for (let k = 0; k < jsonRes.length; k++) {
                    console.log(jsonRes[k]);
                    $("#id_" + jsonRes[k].recordId).html(jsonRes[k].answer);
                }
            }
        }
    })
}

function approvalExamTest() {

    let totalGrade = 0;
    $(".js-grade").each(function () {
        let temp = $(this).val();
        totalGrade += parseFloat(temp);
    })
    alert("本次主观题判卷总得分为：" + totalGrade);
    if (confirm("确定提交本次判卷结果吗？")) {
        $.ajax({
            url: "/set/examset/approvalExamTest",
            type: "post",
            dataType: "json",
            data: {
                recordId: recordId,
                totalGrade: totalGrade
            },
            success: function (data) {
                if (data.status == "200") {
                    alert("判断完成");
                    window.close();
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    }


}
