$(function () {
    getDiscussList();
    $(".js-noticebtn").unbind("click").click(function () {
        $("#noticeModal").modal("show");
        $(".js-save").unbind("click").click(function () {
            sendNotice();
        })
    })
    getDiscussNotice();
})

function sendNotice() {
    $.ajax({
        url: "/set/oaset/insertDiscussNotice",
        type: "post",
        dataType: "json",
        data: {
            title: $("#title").val(),
            noticeType: $("#noticeType").val(),
            remark: $("#remark").val()
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                layer.msg(sysmsg[data.msg]);
                $("#noticeModal").modal("hide");
            }
        }
    });
}

function getDiscussNotice() {
    $.ajax({
        url: "/ret/oaget/getDiscussNoticeList",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                var infoList = data.list;
                for (var i = 0; i < infoList.length; i++) {
                    $(".js-alert").append("<div id='" + infoList[i].recordId + "' class='alert alert-info fade in'><button class='close' data-dismiss='alert'>×</button><i class='fa-fw fa fa-info'></i> <strong>通知!</strong><a style='margin-left:20px;color:#fff;' href='#' onclick='readnotice(\"" + infoList[i].recordId + "\")';>" + infoList[i].title + "</a></div>")
                }
            }
        }
    });
}

function readnotice(recordId) {
    $("#noticeReadModal").modal("show");
    $.ajax({
        url: "/ret/oaget/getDiscussNoticeById",
        type: "post",
        dataType: "json",
        data: {recordId: recordId},
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                var infoList = data.list;
                for (var id in infoList) {
                    if (id == "noticeType") {
                        if (infoList.noticeType == "1") {
                            $("#readNoticeType").html("系统公告");
                        } else if (infoList.noticeType == "2") {
                            $("#readNoticeType").html("版主公告");
                        }
                    } else if (id == "title") {
                        $("#readTitle").html(infoList.title);
                    } else if (id == "remark") {
                        $("#readRemark").html(infoList.remark);
                    }
                }
            }
            $("#" + infoList.recordId).remove();
        }
    });
}

function getDiscussList() {
    $.ajax({
        url: "/ret/oaget/getMyDiscussList",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                var recordList = data.list;
                for (var i = 0; i < recordList.length; i++) {
                    var html = "<tr>" +
                        "<td>" + getCodeClassName(recordList[i].sortId, "discuss_type") + "</td>" +
                        "<td><a href='/app/core/discuss/index?view=record&discussId=" + recordList[i].recordId + "'>" + recordList[i].title + "</a></td>" +
                        "<td>" + recordList[i].userName + "</td>" +
                        "<td class='center'>" + getDiscussRecordCount(recordList[i].recordId) + "</td>" +
                        "<td>" + getLastDiscussRecordTime(recordList[i].recordId) + "</td>" +
                        "</tr>";
                    $("#disusslist").append(html);
                }
            }
        }
    });
}

function getDiscussRecordCount(discussId) {
    var returnStr = "";
    $.ajax({
        url: "/ret/oaget/getDiscussRecordCount",
        type: "post",
        dataType: "json",
        async: false,
        data: {discussId: discussId},
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                returnStr = data.list;
            }
        }
    });
    return returnStr;
}

function getLastDiscussRecordTime(discussId) {
    var returnStr = "";
    $.ajax({
        url: "/ret/oaget/getLastDiscussRecordTime",
        type: "post",
        dataType: "json",
        async: false,
        data: {discussId: discussId},
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                if (data.list) {
                    returnStr = data.list.lastSendTime;
                } else {
                    returnStr = "暂无";
                }
            }
        }
    });
    return returnStr;
}
