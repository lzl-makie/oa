let ue=UE.getEditor('remark');
$(function () {
    getSmsConfig("msgType", "meeting");
    getNotNotesMeetingList();
    $("#updatebut").unbind("click").click(function () {
        updateMeetingNotes();
    });
    ue.addListener("ready", function () {
        $.ajax({
            url: "/ret/meetingget/getMeetingNotesById",
            type: "post",
            dataType: "json",
            data: {
                notesId: notesId
            },
            success: function (data) {
                if (data.status == "500") {
                    console.log(data.msg);
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    var info = data.list;
                    for (var id in info) {
                        if (id == "attach") {
                            $("#meetingattach").attr("data_value", info.attach);
                            createAttach("meetingattach", 4);
                        } else if (id == "userPriv") {
                            $("#userPriv").attr("data-value", info[id]);
                            $("#userPriv").val(getUserNameByStr(info[id]));
                        } else if (id == "deptPriv") {
                            $("#deptPriv").attr("data-value", info[id]);
                            $("#deptPriv").val(getDeptNameByDeptIds(info[id]));
                        } else if (id == "levelPriv") {
                            $("#levelPriv").attr("data-value", info[id]);
                            $("#levelPriv").val(getUserLevelStr(info[id]));
                        } else if (id == "remark") {
                            ue.setContent(info[id]);
                        } else if (id == "attachPriv") {
                            $("input:radio[name='attachPriv'][value='" + info[id] + "']").prop("checked", "checked");
                        } else if (id == "meetingId") {
                            $.ajax({
                                url: "/ret/meetingget/getMeetingById",
                                type: "post",
                                dataType: "json",
                                async: false,
                                data: {
                                    meetingId: info[id]
                                },
                                success: function (res) {
                                    $("#subject").html(res.list.subject);
                                }
                            });
                        } else {
                            $("#" + id).val(info[id]);
                        }
                    }
                }
            }
        });
    });
})

function updateMeetingNotes() {
    $.ajax({
        url: "/set/meetingset/updateMeetingNotes",
        type: "post",
        dataType: "json",
        data: {
            notesId: notesId,
            notesTitle: $("#notesTitle").val(),
            userPriv: $("#userPriv").attr("data-value"),
            deptPriv: $("#deptPriv").attr("data-value"),
            levelPriv: $("#levelPriv").attr("data-value"),
            attach: $("#meetingattach").attr("data_value"),
            attachPriv: $("input:radio[name='attachPriv']:checked").val(),
            remark: ue.getContent(),
            msgType: getCheckBoxValue("msgType")
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                window.location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function getNotNotesMeetingList() {
    $.ajax({
        url: "/ret/meetingget/getNotNotesMeetingList",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                for (var i = 0; i < data.list.length; i++) {
                    $("#meetingId").append("<option value='" + data.list[i].meetingId + "'>" + data.list[i].subject + "</option>")
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}
