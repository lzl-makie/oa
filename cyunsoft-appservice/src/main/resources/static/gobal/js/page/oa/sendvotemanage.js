let ue = UE.getEditor('remark');
$(function () {
    getCodeClass("voteType", "vote");
    getCodeClass("voteTypeQuery", "vote");
    getSmsConfig("msgType", "vote");
    jeDate("#startTime", {
        format: "YYYY-MM-DD",
        minDate: getSysDate(),
        isinitVal: true
    });
    jeDate("#endTime", {
        format: "YYYY-MM-DD",
        minDate: getSysDate()
    });
    jeDate("#beginTimeQuery", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTimeQuery", {
        format: "YYYY-MM-DD"
    });
    $(".js-add-save").unbind("click").click(function () {
        sendVote();
    })
    query();
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    });
    $(".js-back-btn").unbind("click").click(function () {
        $("#votediv").hide();
        $("#votelistdiv").show();
    })
})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/oaget/getVoteListForManage',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        sortOrder: "desc",
        search: true,//启用搜索
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'voteId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        },
            {
                field: 'num',
                title: '序号',//标题  可不加
                width: '50px',
                formatter: function (value, row, index) {
                    return index + 1;
                }
            },
            {
                field: 'title',
                title: '投票标题',
                width: '150px',
                formatter: function (value, row, index) {
                    return "<a href=\"javascript:void(0);details('" + row.voteId + "')\">" + value + "</a>";
                }
            },
            {
                field: 'voteType',
                title: '投票类型',
                sortable: true,
                width: '50px',
                formatter: function (value, row, index) {
                    return getCodeClassName(value, "vote");
                }
            },
            {
                field: 'status',
                width: '50px',
                title: '状态',
                formatter: function (value, row, index) {
                    if (value == "0") {
                        return "未生效";
                    } else if (value == "1") {
                        return "已生效";
                    }
                }
            },
            {
                field: 'startTime',
                title: '起始日期',
                width: '50px'
            },
            {
                field: 'endTime',
                width: '100px',
                title: '终止时间'
            },
            {
                field: 'createTime',
                width: '100px',
                title: '创建时间'
            },
            {
                field: 'createUserName',
                width: '50px',
                title: '创建人'
            },
            {
                field: 'opt',
                title: '操作',
                align: 'center',
                width: '180px',
                formatter: function (value, row, index) {
                    return createOptBtn(row.voteId, row.status);
                }
            }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        status: $("#statusQuery").val(),
        beginTime: $("#beginTimeQuery").val(),
        endTime: $("#endTimeQuery").val(),
        voteType: $("#voteTypeQuery").val()
    };
    return temp;
};

function createOptBtn(voteId, status) {
    let html = "";
    if (status == "0") {
        html += "<a href=\"javascript:void(0);edit('" + voteId + "')\" class=\"btn btn-palegreen btn-xs\" >编辑</a>&nbsp;&nbsp;" +
            "<a href=\"javascript:void(0);startVote('" + voteId + "')\" class=\"btn btn-success btn-xs\">发布</a>&nbsp;&nbsp;" +
            "<a href=\"javascript:void(0);del('" + voteId + "')\" class=\"btn btn-darkorange btn-xs\" >删除</a>&nbsp;&nbsp;" +
            "<a href=\"javascript:void(0);setItem('" + voteId + "')\" class=\"btn btn-azure shiny btn-xs\">投票项</a>";
    } else if (status == "1") {
        html += "<a href=\"javascript:void(0);stopVote('" + voteId + "')\" class=\"btn btn-primary btn-xs\">终止</a>&nbsp;&nbsp;";
    }
    return html;
}

function details(voteId) {
    window.open("/app/core/vote/sendvote?view=details&voteId=" + voteId);
}

function startVote(voteId) {
    if (confirm("确定发布当前投票吗？")) {
        $.ajax({
            url: "/set/oaset/updateVote",
            type: "post",
            dataType: "json",
            data: {
                voteId: voteId,
                status: "1"
            },
            success: function (data) {
                if (data.status == "500") {
                    console.log(data.msg);
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable('refresh');
                }
            }
        });
    }
}

function stopVote(voteId) {
    if (confirm("确定终止当前投票吗？")) {
        $.ajax({
            url: "/set/oaset/updateVote",
            type: "post",
            dataType: "json",
            data: {
                voteId: voteId,
                status: "0"
            },
            success: function (data) {
                if (data.status == "500") {
                    console.log(data.msg);
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable('refresh');
                }
            }
        });
    }
}

function setItem(voteId) {
    window.location.href = "/app/core/vote/sendvote?view=item&voteId=" + voteId;
}

function edit(voteId) {
    document.getElementById("form1").reset();
    $("#votelistdiv").hide();
    $("#votediv").show();
    $("#userPriv").attr("data-value", "");
    $("#deptPriv").attr("data-value", "");
    $("#levelPriv").attr("data-value", "");
    ue.setContent("");
    $("#show_attach").empty();
    $("#attach").attr("data_value", "");
    $.ajax({
        url: "/ret/oaget/getVoteById",
        type: "post",
        dataType: "json",
        data: {
            voteId: voteId
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", recordInfo.attach);
                        createAttach("attach", 4);
                    } else if (id == "userPriv") {
                        $("#userPriv").attr("data-value", data.list[id]);
                        $("#userPriv").val(getUserNameByStr(data.list[id]));
                    } else if (id == "deptPriv") {
                        $("#deptPriv").attr("data-value", data.list[id]);
                        $("#deptPriv").val(getDeptNameByDeptIds(data.list[id]));
                    } else if (id == "levelPriv") {
                        $("#levelPriv").attr("data-value", data.list[id]);
                        $("#levelPriv").val(getUserLevelStr(data.list[id]));
                    } else if (id == "isTop") {
                        $("input:radio[name='isTop'][value='" + data.list[id] + "']").prop("checked", "checked");
                    } else if (id == "readRes") {
                        $("input:radio[name='readRes'][value='" + data.list[id] + "']").prop("checked", "checked");
                    } else if (id == "remark") {
                        ue.setContent(data.list[id]);
                    } else {
                        $("#" + id).val(recordInfo[id]);
                    }
                }
            }
            $(".js-update-save").unbind("click").click(function () {
                updateVote(voteId);
            })
        }
    });
}


function del(voteId) {
    if (confirm("确定删除当前投票吗？")) {
        $.ajax({
            url: "/set/oaset/deleteVote",
            type: "post",
            dataType: "json",
            data: {
                voteId: voteId
            },
            success: function (data) {
                if (data.status == "500") {
                    console.log(data.msg);
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable('refresh');
                }
            }
        });
    }
}


function updateVote(voteId) {
    if ($("#title").val() == "") {
        layer.msg("投票标题不能为空！");
        return;
    }
    if ($("#startTime").val() == "") {
        layer.msg("投票开始时间不能为空！");
        return;
    }
    if ($("#endTime").val() == "") {
        layer.msg("投票结束时间不能为空！");
        return;
    }
    $.ajax({
        url: "/set/oaset/updateVote",
        type: "post",
        dataType: "json",
        data: {
            voteId: voteId,
            title: $("#title").val(),
            voteType: $("#voteType").val(),
            isAnonymous: $("#isAnonymous").val(),
            userPriv: $("#userPriv").attr("data-value"),
            deptPriv: $("#deptPriv").attr("data-value"),
            levelPriv: $("#levelPriv").attr("data-value"),
            remark: $("#remark").code(),
            startTime: $("#startTime").val(),
            endTime: $("#endTime").val(),
            attach: $("#attach").attr("data_value"),
            isTop: $("input:radio[name='isTop']:checked").val(),
            readRes: $("input:radio[name='readRes']:checked").val(),
            attachPriv: $("input:radio[name='attachPriv']:checked").val(),
            msgType: getCheckBoxValue("msgType")
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                window.location.reload();
                layer.msg(sysmsg[data.msg]);
            }
        }
    })
}
