let ue=UE.getEditor('remark');
$(function () {
    jeDate("#beginTime", {
        format: "YYYY-MM-DD"
    });
    getSmsConfig("msgType", "task");
    getCodeClass("taskType", "task");
    $("#createbut").unbind("click").click(function () {
        addTask();
    })
});

function addTask() {
    if ($("#taskName").val() == "") {
        layer.msg("任务标题不能为空！");
        return;
    }
    $.ajax({
        url: "/set/taskset/addTask",
        type: "POST",
        dataType: "json",
        data: {
            taskName: $("#taskName").val(),
            taskType: $("#taskType").val(),
            isTop: $('input:radio[name="isTop"]:checked').val(),
            deptPriv: $("#deptPriv").attr("data-value"),
            userPriv: $("#userPriv").attr("data-value"),
            levelPriv: $("#levelPriv").attr("data-value"),
            chargeAccountId: $("#chargeAccountId").attr("data-value"),
            participantAccountId: $("#participantAccountId").attr("data-value"),
            supervisorAccountId: $("#supervisorAccountId").attr("data-value"),
            attach: $("#taskattach").attr("data_value"),
            attachPriv: $("input:radio[name='attachPriv']:checked").val(),
            beginTime: $("#beginTime").val(),
            endTime: $("#endTime").val(),
            msgType: getCheckBoxValue("msgType"),
            duration: $("#duration").val(),
            remark: ue.getContent()
        },
        async: false,
        error: function (e) {
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}
