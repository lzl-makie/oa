let vm = new Vue({
    el: '#app',  //实例化对象
    data:{
        ue:UE.getEditor('content')
    },
    mounted: function () {
        getCodeClass("newsType", "news");
        getSmsConfig("msgType", "news");
        $("#createbut").unbind("click").click(function () {
            vm.sendNews();
        });
        jeDate("#sendTime", {
            format: "YYYY-MM-DD",
            minDate: getSysDate(),
            isinitVal: true
        });
        jeDate("#endTime", {
            format: "YYYY-MM-DD",
            minDate: getSysDate()
        });
    },
    methods: {
        sendNews: function () {
            if ($("#newsTitle").val() == "") {
                layer.msg("新闻标题不能为空");
                return;
            }
            $.ajax({
                url: "/set/oaset/sendNews",
                type: "post",
                dataType: "json",
                data: {
                    newsTitle: $("#newsTitle").val(),
                    newsType: $("#newsType").val(),
                    userPriv: $("#userPriv").attr("data-value"),
                    deptPriv: $("#deptPriv").attr("data-value"),
                    levelPriv: $("#levelPriv").attr("data-value"),
                    content: vm.ue.getContent(),
                    sendTime: $("#sendTime").val(),
                    endTime: $("#endTime").val(),
                    attach: $("#newsattach").attr("data_value"),
                    isTop: $("input:radio[name='isTop']:checked").val(),
                    attachPriv: $("input:radio[name='attachPriv']:checked").val(),
                    msgType: getCheckBoxValue("msgType")
                },
                success: function (data) {
                    if (data.status == "500") {
                        console.log(data.msg);
                    } else if (data.status == "100") {
                        layer.msg(sysmsg[data.msg]);
                    } else {
                        window.location.reload();
                        layer.msg(sysmsg[data.msg]);
                    }
                }
            })
        }
    },
})


