var vm = new Vue({
    el: '#app',  //实例化对象
    data: {
        itemList: []  //要存放的数据
    },
    methods: {
        //存放实例方法
    }
})
var setting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/filemapget/getFileMapSortTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    callback: {
        onClick: zTreeOnClick
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    }
};
$(function () {
    getItemList("");
    $.ajax({
        url: "/ret/filemapget/getFileMapSortTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            zTree = $.fn.zTree.init($("#tree"), setting, data);// 初始化树节点时，添加同步获取的数据
        }
    });
})

function getItemList(sortId) {
    $.ajax({
        url: "/ret/filemapget/getFileItemList",
        type: "post",
        dataType: "json",
        data: {
            sortId: sortId,
        },
        success: function (data) {
            if (data.status == "200") {
                vm.itemList = data.list;
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function zTreeOnClick(event, treeId, treeNode) {
    getItemList(treeNode.sortId);
}

function openFileList(Obj) {
    let itemId = $(Obj).attr("value");
    window.open("/app/core/filemap/filemapsearch?itemId=" + itemId);
}
