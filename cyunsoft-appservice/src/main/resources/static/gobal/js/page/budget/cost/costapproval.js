$(function () {
    $("#remark").summernote({height: 300});
    $.ajax({
        url: "/ret/budgetget/getProjectTreeList",
        type: "post",
        dataType: "json",
        success: function (data) {
            var topNode = [{
                sortName: "全部分类",
                isParent: "false",
                sortId: ""
            }];
            var newTreeNodes = topNode.concat(data);
            $.fn.zTree.init($("#tree1"), setting, newTreeNodes);
            var topNode = [{
                sortName: "全部",
                isParent: "false",
                sortId: ""
            }];
        }
    });
    jeDate("#beginTimeQuery", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTimeQuery", {
        format: "YYYY-MM-DD",
    });
    $("#projectIdQuery").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#menuContent1").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });
    $("body").unbind("click").click(function () {
        $(".menuContent").hide();
    });
    $(".menuContent").unbind("click").click(function (e) {
        e.stopPropagation();
    });
    query();
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })
});
var setting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/budgetget/getProjectTreeList",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("tree1"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.id;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].sortName + ",";
                vid += nodes[i].sortId + ",";
            }

            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#projectIdQuery");
            idem.val(v);
            idem.attr("data-value", vid);
        }
    }
};

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/budgetget/getBudgetCostApprpvalList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',// 工具列
        striped: true,// 隔行换色
        cache: false,// 禁用缓存
        pagination: true,// 启动分页
        sidePagination: 'server',// 分页方式
        pageNumber: 1,// 初始化table时显示的页码
        pageSize: 10,// 每页条目
        showFooter: false,// 是否显示列脚
        showPaginationSwitch: true,// 是否显示 数据条数选择框
        sortable: true,// 排序
        search: true,// 启用搜索
        sortOrder: "asc",
        showColumns: true,// 是否显示 内容列下拉框
        showRefresh: true,// 显示刷新按钮
        idField: 'recordId',// key值栏位
        clickToSelect: true,// 点击选中checkbox
        pageList: [10, 20, 30, 50],// 可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',// 标题 可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'pTitle',
            width: '150px',
            title: '项目名称'
        }, {
            field: 'title',
            width: '150px',
            title: '预算申请标题'
        }, {
            field: 'pTotalCost',
            width: '100px',
            title: '剩余预算'
        },
            {
                field: 'cTotalCost',
                title: '申请金额',
                width: '100px'
            },
            {
                field: 'budgetAccount',
                width: '100px',
                title: '预算科目',
                formatter: function (value, row, index) {
                    return getBudgetAccountName(value);
                }
            }, {
                field: 'applyUser',
                width: '50px',
                title: '审批人',
                formatter: function (value, row, index) {
                    return getUserNameByStr(value);
                }
            }, {
                field: 'payTime',
                title: '支出日期',
                width: '50px'
            }, {
                field: 'opt',
                title: '操作',
                align: 'center',
                width: '120px',
                formatter: function (value, row, index) {
                    return createOptBtn(row.recordId, row.status);
                }
            }],
        onClickCell: function (field, value, row, $element) {
            // alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, // 总页数,前面的key必须为"total"
                    rows: res.list.list
                    // 行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        projectId: $("#projectIdQuery").attr("data-value"),
        beginTime: $("#beginTimeQuery").val(),
        endTime: $("#endTimeQuery").val()
    };
    return temp;
};

function createOptBtn(recordId, status) {
    var html = "";
    if (status == "0") {
        html += "<a href=\"javascript:void(0);approval('" + recordId + "')\" class=\"btn btn-primary btn-xs\">审批</a>&nbsp;&nbsp;";
    }
    html += "<a href=\"javascript:void(0);details('" + recordId + "')\" class=\"btn btn-sky btn-xs\" >详情</a>";
    return html;
}

function approval(recordId) {
    $("#approvermodal").modal("show");
    $(".js-app-not-pass").unbind("click").click(function () {
        $.ajax({
            url: "/set/budgetset/setCostApprovalStatus",
            type: "post",
            dataType: "json",
            data: {recordId: recordId, status: 2},
            success: function (data) {
                if (data.status == "500") {
                    console.log(data.msg);
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    layer.msg(sysmsg[data.msg]);
                    $('#myTable').bootstrapTable('refresh');
                    $("#approvermodal").modal("hide");
                }
            }
        })
    });
    $(".js-app-pass").unbind("click").click(function () {
        $.ajax({
            url: "/set/budgetset/setCostApprovalStatus",
            type: "post",
            dataType: "json",
            data: {recordId: recordId, status: 1},
            success: function (data) {
                if (data.status == "500") {
                    console.log(data.msg);
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    layer.msg(sysmsg[data.msg]);
                    $('#myTable').bootstrapTable('refresh');
                    $("#approvermodal").modal("hide");
                }
            }
        })
    });
}

function details(recordId) {
    window.open("/app/core/budget/costapplydetails?recordId=" + recordId);
}

function getBudgetAccountName(budgetAccountId) {
    var returnStr = "";
    $.ajax({
        url: "/ret/budgetget/getBudgetAccountById",
        type: "post",
        dataType: "json",
        async: false,
        data: {
            budgetAccountId: budgetAccountId
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                returnStr = data.list.title;
            }
        }
    });
    return returnStr;
}
