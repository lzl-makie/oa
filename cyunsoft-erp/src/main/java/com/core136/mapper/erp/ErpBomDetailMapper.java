package com.core136.mapper.erp;

import com.core136.bean.erp.ErpBomDetail;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ErpBomDetailMapper extends MyMapper<ErpBomDetail> {
    /**
     * @param bomId
     * @param materielCode
     * @param orgId
     * @return List<Map < String, Object>>
     * @Title getBomDetailList
     * @Description  查询Bom下的所有物料
     */
    public List<Map<String, Object>> getBomDetailList(@Param(value = "bomId") String bomId, @Param(value = "materielCode") String materielCode, @Param(value = "orgId") String orgId);

    /**
     * @param bomId
     * @param materielCode
     * @param orgId
     * @return int
     * @Title isExistMaterielCode
     * @Description  判断当前BOM下是否有相同的物料编码
     */
    public int isExistMaterielCode(@Param(value = "bomId") String bomId, @Param(value = "materielCode") String materielCode, @Param(value = "orgId") String orgId);

    /**
     * @param bomId
     * @param bomDetailId
     * @param orgId
     * @return int
     * @Title getBomDetailByDetailId
     * @Description  按bomDetailId获取BOM清单中的物料
     */
    public Map<String, Object> getBomDetailByDetailId(@Param(value = "bomId") String bomId, @Param(value = "bomDetailId") String bomDetailId, @Param(value = "orgId") String orgId);

    /**
     * @param bomId
     * @param materielCode
     * @param orgId
     * @return List<Map < String, Object>>
     * @Title getErpBomByBomIdList
     * @Description  获取BOM清单中的子BOM清单
     */
    public List<Map<String, Object>> getErpBomByBomIdList(@Param(value = "bomId") String bomId, @Param(value = "bomName") String bomName, @Param(value = "orgId") String orgId);


    /**
     * @param bomId
     * @param childBomId
     * @param orgId
     * @return int
     * @Title isExistChildBomIdCode
     * @Description  查询子BOM是否存于现有的BOM中
     */
    public int isExistChildBomIdCode(@Param(value = "bomId") String bomId, @Param(value = "childBomId") String childBomId, @Param(value = "orgId") String orgId);


    /**
     * @param bomId
     * @param orgId
     * @return List<Map < String, Object>>
     * @Title getProductMaterielList
     * @Description  产品物料详情
     */
    public List<Map<String, Object>> getProductMaterielList(@Param(value = "bomId") String bomId, @Param(value = "orgId") String orgId);


}
