package com.core136.mapper.erp;

import com.core136.bean.erp.ErpOrderDetail;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ErpOrderDetailMapper extends MyMapper<ErpOrderDetail> {

    /**
     * @param orderId
     * @param search
     * @param orgId
     * @return List<Map < String, String>>
     * @Title getErpOrderDetail
     * @Description  获取订单详情
     */
    public List<Map<String, Object>> getErpOrderDetail(@Param(value = "orderId") String orderId, @Param(value = "search") String search, @Param(value = "orgId") String orgId);
}
