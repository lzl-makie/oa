package com.core136.mapper.erp;

import com.core136.bean.erp.ErpBom;
import com.core136.bean.erp.ErpMateriel;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ErpBomMapper extends MyMapper<ErpBom> {

    /**
     * @param sortId
     * @param orgId
     * @return List<Map < String, Object>>
     * @Title getErpBomListBySortId
     * @Description  按分类获取BOM树结构
     */
    public List<Map<String, Object>> getErpBomTreeBySortId(@Param(value = "sortId") String sortId, @Param(value = "orgId") String orgId);

    /**
     * @param bomName
     * @param orgId
     * @return List<ErpMateriel>
     * @Title selectBomList2ById
     * @Description  获取BOM清单用于SELECT2插件
     */
    public List<ErpMateriel> selectBomList2ById(@Param(value = "bomName") String bomName, @Param(value = "orgId") String orgId);

}
