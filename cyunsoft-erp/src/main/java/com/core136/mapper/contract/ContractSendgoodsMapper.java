package com.core136.mapper.contract;

import com.core136.bean.contract.ContractSendgoods;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ContractSendgoodsMapper extends MyMapper<ContractSendgoods> {

    /**
     * @param orgId
     * @param contractType
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getContractSendgoodsList
     * @Description:  获取发货列表
     */
    public List<Map<String, String>> getContractSendgoodsList(
            @Param(value = "orgId") String orgId, @Param(value = "contractType") String contractType,
            @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime,
            @Param(value = "search") String search
    );
}
