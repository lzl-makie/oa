package com.core136.service.erp;

import com.core136.bean.erp.ErpOrder;
import com.core136.mapper.erp.ErpOrderMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service
public class ErpOrderService {
    private ErpOrderMapper erpOrderMapper;

    @Autowired
    public void setErpOrderMapper(ErpOrderMapper erpOrderMapper) {
        this.erpOrderMapper = erpOrderMapper;
    }

    /**
     * @param erpOrder
     * @return int
     * @Title insertErpOrder
     * @Description  添加订单
     */
    public int insertErpOrder(ErpOrder erpOrder) {
        return erpOrderMapper.insert(erpOrder);
    }

    /**
     * @param erpOrder
     * @return ErpOrder
     * @Title select
     * @Description  控条件获取ErpOrder
     */
    public ErpOrder selectOne(ErpOrder erpOrder) {
        return erpOrderMapper.selectOne(erpOrder);
    }

    /**
     * @param example
     * @param pageNumber
     * @param pageSize
     * @return PageInfo<ErpOrder>
     * @Title getErpOrderList
     * @Description  获取订单列表
     */

    public PageInfo<ErpOrder> getErpOrderList(Example example, int pageNumber, int pageSize) {
        PageHelper.startPage(pageNumber, pageSize);
        List<ErpOrder> datalist = erpOrderMapper.selectByExample(example);
        PageInfo<ErpOrder> pageInfo = new PageInfo<ErpOrder>(datalist);
        return pageInfo;
    }

    /**
     * @param erpOrder
     * @return int
     * @Title delErpOrder
     * @Description  删除订单
     */
    public int deleteErpOrder(ErpOrder erpOrder) {
        return erpOrderMapper.delete(erpOrder);
    }

    /**
     * @param erpOrder
     * @param example
     * @return int
     * @Title updateErpOrder
     * @Description  更新订单信息
     */
    public int updateErpOrder(ErpOrder erpOrder, Example example) {
        return erpOrderMapper.updateByExampleSelective(erpOrder, example);
    }

}
