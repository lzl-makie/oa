/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: CrmInquiryService.java
 * @Package com.core136.service.crm
 * @Description: (用一句话描述该文件做什么)
 * @author: 稠云信息
 * @date: 2019年5月5日 上午10:27:34
 * @version V1.0
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
package com.core136.service.crm;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.core136.bean.crm.CrmInquiry;
import com.core136.bean.crm.CrmInquiryDetail;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.crm.CrmInquiryMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 * @ClassName: CrmInquiryService
 * @Description: 询价单主表
 * @author: 稠云信息
 * @date: 2019年5月5日 上午10:27:34
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Service
public class CrmInquiryService {
    private CrmInquiryMapper crmInquiryMapper;

    @Autowired
    public void setCrmInquiryMapper(CrmInquiryMapper crmInquiryMapper) {
        this.crmInquiryMapper = crmInquiryMapper;
    }

    private CrmInquiryDetailService crmInquiryDetailService;

    @Autowired
    public void setCrmInquiryDetailService(CrmInquiryDetailService crmInquiryDetailService) {
        this.crmInquiryDetailService = crmInquiryDetailService;
    }

    public CrmInquiry selectOneCrmInquiry(CrmInquiry crmInquiry) {
        return crmInquiryMapper.selectOne(crmInquiry);
    }

    public int insertCrmInquiry(CrmInquiry crmInquiry) {
        return crmInquiryMapper.insert(crmInquiry);
    }

    public int deleteCrmInquiry(CrmInquiry crmInquiry) {
        return crmInquiryMapper.delete(crmInquiry);
    }

    public int updateCrmInquiry(CrmInquiry crmInquiry, Example example) {
        return crmInquiryMapper.updateByExampleSelective(crmInquiry, example);
    }

    /**
     * @param crmInquiry
     * @param example
     * @param jsonArr
     * @return int
     * @Title: updateInquiryAndDetail
     * @Description:  更新询价单
     */
    @Transactional(value = "generalTM")
    public int updateInquiryAndDetail(CrmInquiry crmInquiry, Example example, JSONArray jsonArr) {
        CrmInquiryDetail crmInquiryDetaildel = new CrmInquiryDetail();
        crmInquiryDetaildel.setInquiryId(crmInquiry.getInquiryId());
        crmInquiryDetaildel.setOrgId(crmInquiry.getOrgId());
        crmInquiryDetailService.deleteCrmInquiryDetail(crmInquiryDetaildel);
        for (int i = 0; i < jsonArr.size(); i++) {
            CrmInquiryDetail crmInquiryDetail = JSONObject.parseObject(jsonArr.get(i).toString(), CrmInquiryDetail.class);
            crmInquiryDetail.setDetailId(SysTools.getGUID());
            crmInquiryDetail.setInquiryId(crmInquiry.getInquiryId());
            crmInquiryDetail.setOrgId(crmInquiry.getOrgId());
            crmInquiryDetail.setCreateTime(crmInquiry.getCreateTime());
            crmInquiryDetail.setCreateUser(crmInquiry.getCreateUser());
            crmInquiryDetailService.insertCrmInquiryDetail(crmInquiryDetail);
        }
        return updateCrmInquiry(crmInquiry, example);
    }


    /**
     * @param @param  crmInquiry
     * @param @param  detailList
     * @param @return 设定文件
     * @return int 返回类型
     * @Title: saveCrmInquiry
     * @Description:  保存询价单
     */
    @Transactional(value = "generalTM")
    public int saveCrmInquiry(CrmInquiry crmInquiry, JSONArray jsonArr) {
        for (int i = 0; i < jsonArr.size(); i++) {
            CrmInquiryDetail crmInquiryDetail = JSONObject.parseObject(jsonArr.get(i).toString(), CrmInquiryDetail.class);
            crmInquiryDetail.setDetailId(SysTools.getGUID());
            crmInquiryDetail.setInquiryId(crmInquiry.getInquiryId());
            crmInquiryDetail.setOrgId(crmInquiry.getOrgId());
            crmInquiryDetail.setCreateTime(crmInquiry.getCreateTime());
            crmInquiryDetail.setCreateUser(crmInquiry.getCreateUser());
            crmInquiryDetailService.insertCrmInquiryDetail(crmInquiryDetail);
        }
        return insertCrmInquiry(crmInquiry);
    }

    /**
     * @param @param  orgId
     * @param @param  opFlag
     * @param @param  accountId
     * @param @param  search
     * @param @return 设定文件
     * @Title: getCrmInquiryList
     * @Description: 获限权限内的询价单列表
     */

    public List<Map<String, String>> getCrmInquiryList(String orgId, String opFlag, String accountId, String beginTime, String endTime, String customerType, String status, String search) {
        //  Auto-generated method stub
        return crmInquiryMapper.getCrmInquiryList(orgId, opFlag, accountId, beginTime, endTime, customerType, status, "%" + search + "%");
    }

    /**
     * @param @param  pageNumber
     * @param @param  pageSize
     * @param @param  orderBy
     * @param @param  orgId
     * @param @param  opFlag
     * @param @param  accountId
     * @param @param  search
     * @param @return 设定文件
     * @return PageInfo<Map < String, String>> 返回类型
     * @throws Exception
     * @Title: getCrmInquiryList
     * @Description: 获限权限内的询价单列表
     */
    public PageInfo<Map<String, String>> getCrmInquiryList(PageParam pageParam, String beginTime, String endTime, String customerType, String status) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSql(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getCrmInquiryList(pageParam.getOrgId(), pageParam.getOpFlag(), pageParam.getAccountId(), beginTime, endTime, customerType, status, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getCrmInquiryListForSelect
     * @Description:  获取询价单列表
     */
    public List<Map<String, String>> getCrmInquiryListForSelect(String orgId, String accountId) {
        return crmInquiryMapper.getCrmInquiryListForSelect(orgId, accountId);
    }

}
