package com.core136.service.crm;

import com.core136.bean.calendar.Calendar;
import com.core136.bean.crm.CrmContractRecord;
import com.core136.bean.crm.CrmCustomer;
import com.core136.common.utils.SysTools;
import com.core136.mapper.calendar.CalendarMapper;
import com.core136.mapper.crm.CrmContractRecordMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * @ClassName: CrmContactRecordService
 * @Description: 客户联系记录
 * @author: 稠云信息
 * @date: 2019年2月12日 下午5:10:37
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Service
public class CrmContactRecordService {
    private CrmContractRecordMapper crmContractRecordMapper;

    @Autowired
    public void setCrmContractRecordMapper(CrmContractRecordMapper crmContractRecordMapper) {
        this.crmContractRecordMapper = crmContractRecordMapper;
    }

    private CalendarMapper calendarMapper;

    @Autowired
    public void setCalendarMapper(CalendarMapper calendarMapper) {
        this.calendarMapper = calendarMapper;
    }

    private CrmCustomerService crmCustomerService;

    @Autowired
    public void setCrmCustomerService(CrmCustomerService crmCustomerService) {
        this.crmCustomerService = crmCustomerService;
    }

    public CrmContractRecord selectOneCrmContractRecord(CrmContractRecord crmContractRecord) {
        return crmContractRecordMapper.selectOne(crmContractRecord);
    }

    public int delectCrmContractRecord(CrmContractRecord crmContractRecord) {
        return crmContractRecordMapper.delete(crmContractRecord);
    }

    public int updateCrmContractRecord(CrmContractRecord crmContractRecord, Example example) {
        return crmContractRecordMapper.updateByExampleSelective(crmContractRecord, example);
    }

    public int insertCrmContractRecord(CrmContractRecord crmContractRecord) {
        return crmContractRecordMapper.insert(crmContractRecord);
    }

    /**
     * @return int
     * @Title addRecordAndCalendar
     * @Description  添加联系记录并处理下次联系加入个人日程
     */
    @Transactional(value = "generalTM")
    public int addRecordAndCalendar(CrmContractRecord crmContractRecord) {
        if (StringUtils.isNotBlank(crmContractRecord.getNextVisit())) {
            CrmCustomer crmCustomer = new CrmCustomer();
            crmCustomer.setCustomerId(crmContractRecord.getCustomerId());
            crmCustomer.setOrgId(crmContractRecord.getOrgId());
            crmCustomer = crmCustomerService.selectOne(crmCustomer);
            Calendar calendar = new Calendar();
            calendar.setCalendarId(SysTools.getGUID());
            if (StringUtils.isNotBlank(crmCustomer.getCnName())) {
                calendar.setContent("回访" + crmCustomer.getEnName());
            } else {
                calendar.setContent("回访" + crmCustomer.getCnName());
            }
            calendar.setStartTime(crmContractRecord.getNextVisit() + " 08:00:00");
            calendar.setEndTime(crmContractRecord.getNextVisit() + " 23:59:59");
            calendar.setType("1");
            calendar.setAccountId(crmContractRecord.getCreateUser());
            calendar.setUrl("/app/core/crm/customerdetails?customerId=" + crmContractRecord.getCustomerId());
            calendar.setOrgId(crmContractRecord.getOrgId());
            calendarMapper.insert(calendar);
        }
        return crmContractRecordMapper.insert(crmContractRecord);
    }


    /**
     * @param example
     * @return List<CrmContactRecord>
     * @Title getRecordByCustomerId
     * @Description  获把指定企业的联系记录
     */
    public List<CrmContractRecord> getRecordByCustomerId(Example example) {
        return crmContractRecordMapper.selectByExample(example);
    }

    /**
     * 获把指定企业的联系记录
     */

    public List<Map<String, Object>> getRecordListByCustomerId(String orgId, String customerId) {
        //  Auto-generated method stub
        return crmContractRecordMapper.getRecordListByCustomerId(orgId, customerId);
    }

    public PageInfo<Map<String, Object>> getRecordListByCustomerId(int pageNumber, int pageSize, String orgId, String customerId) {
        PageHelper.startPage(pageNumber, pageSize);
        List<Map<String, Object>> datalist = crmContractRecordMapper.getRecordListByCustomerId(orgId, customerId);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<Map<String, Object>>(datalist);
        return pageInfo;
    }

}
