package com.core136.controller.crm;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.core136.bean.account.Account;
import com.core136.bean.account.UserInfo;
import com.core136.bean.crm.*;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.crm.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

/**
 * @ClassName: RoutSetCrmController
 * @Description: CRM管理数据接口
 * @author: 稠云信息
 * @date: 2019年2月12日 上午9:25:07
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@RestController
@RequestMapping("/set/crmset")
public class RouteSetCrmController {
    private CrmCustomerService crmCustomerService;

    @Autowired
    public void setCrmCustomerService(CrmCustomerService crmCustomerService) {
        this.crmCustomerService = crmCustomerService;
    }

    private CrmLinkManService crmLinkManService;

    @Autowired
    public void setCrmLinkManService(CrmLinkManService crmLinkManService) {
        this.crmLinkManService = crmLinkManService;
    }

    private CrmContactRecordService crmContactRecordService;

    @Autowired
    public void setCrmContactRecordService(CrmContactRecordService crmContactRecordService) {
        this.crmContactRecordService = crmContactRecordService;
    }

    private CrmPrivService crmPrivService;

    @Autowired
    public void setCrmPrivService(CrmPrivService crmPrivService) {
        this.crmPrivService = crmPrivService;
    }

    private CrmIndustryService crmIndustryService;

    @Autowired
    public void setCrmIndustryService(CrmIndustryService crmIndustryService) {
        this.crmIndustryService = crmIndustryService;
    }

    private CrmTagsService crmTagsService;

    @Autowired
    public void setCrmTagsService(CrmTagsService crmTagsService) {
        this.crmTagsService = crmTagsService;
    }

    private CrmMyProductService crmMyProductService;

    @Autowired
    public void setCrmMyProductService(CrmMyProductService crmMyProductService) {
        this.crmMyProductService = crmMyProductService;
    }

    private CrmContractInfoService crmContractInfoService;

    @Autowired
    public void setCrmContractInfoService(CrmContractInfoService crmContractInfoService) {
        this.crmContractInfoService = crmContractInfoService;
    }

    private CrmInquiryService crmInquiryService;

    @Autowired
    public void setCrmInquiryService(CrmInquiryService crmInquiryService) {
        this.crmInquiryService = crmInquiryService;
    }

    private CrmInquiryDetailService crmInquiryDetailService;

    @Autowired
    public void setCrmInquiryDetailService(CrmInquiryDetailService crmInquiryDetailService) {
        this.crmInquiryDetailService = crmInquiryDetailService;
    }

    private CrmQuotationService crmQuotationService;

    @Autowired
    public void setCrmQuotationService(CrmQuotationService crmQuotationService) {
        this.crmQuotationService = crmQuotationService;
    }

    private CrmQuotationMxService crmQuotationMxService;

    @Autowired
    public void setCrmQuotationMxService(CrmQuotationMxService crmQuotationMxService) {
        this.crmQuotationMxService = crmQuotationMxService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * @param crmQuotation
     * @return RetDataBean
     * @Title: approvedCrmQuotation
     * @Description:  报价单
     */
    @RequestMapping(value = "/approvedCrmQuotation", method = RequestMethod.POST)
    public RetDataBean approvedCrmQuotation(CrmQuotation crmQuotation) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            crmQuotation.setApprovedTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            crmQuotation.setOrgId(account.getOrgId());
            Example example = new Example(CrmQuotation.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("quotationId", crmQuotation.getQuotationId());
            return RetDataTools.Ok(MessageCode.MESSAGE_APPROVAL_SUCCESS, crmQuotationService.updateCrmQuotation(example, crmQuotation));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param @param  crmQuotation
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: insertCrmQuotation
     * @Description:  创建报价单
     */
    @RequestMapping(value = "/insertCrmQuotation", method = RequestMethod.POST)
    public RetDataBean insertCrmQuotation(CrmQuotation crmQuotation, String detail) {
        try {
            JSONArray jsonArr = JSONObject.parseArray(detail);
            Account account = accountService.getRedisAUserInfoToAccount();
            crmQuotation.setQuotationId(SysTools.getGUID());
            crmQuotation.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            crmQuotation.setCreateUser(account.getAccountId());
            crmQuotation.setStatus("0");
            crmQuotation.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, crmQuotationService.saveCrmQuotation(crmQuotation, jsonArr));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  crmQuotation
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: deleteCrmQuotation
     * @Description:  删除报价单
     */
    @RequestMapping(value = "/deleteCrmQuotation", method = RequestMethod.POST)
    public RetDataBean deleteCrmQuotation(CrmQuotation crmQuotation) {
        try {
            if (StringUtils.isBlank(crmQuotation.getQuotationId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                Account account = accountService.getRedisAUserInfoToAccount();
                crmQuotation.setOrgId(account.getOrgId());
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, crmQuotationService.deleteCrmQuotation(crmQuotation));
            }
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param @param  request
     * @param @param  crmQuotation
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: updateCrmQuotation
     * @Description:  更新产品信息
     */
    @RequestMapping(value = "/updateCrmQuotation", method = RequestMethod.POST)
    public RetDataBean updateCrmQuotation(CrmQuotation crmQuotation, String detail) {
        try {
            JSONArray jsonArr = JSONObject.parseArray(detail);
            if (StringUtils.isBlank(crmQuotation.getQuotationId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                Account account = accountService.getRedisAUserInfoToAccount();
                crmQuotation.setOrgId(account.getOrgId());
                Example example = new Example(CrmQuotation.class);
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("quotationId", crmQuotation.getQuotationId());
                return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, crmQuotationService.updateQuotationAndDetail(crmQuotation, example, jsonArr));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param crmCustomer
     * @return RetDataBean
     * @Title createCrmCustomer
     * @Description  创建客户
     */
    @RequestMapping(value = "/createCrmCustomer", method = RequestMethod.POST)
    public RetDataBean createCrmCustomer(CrmCustomer crmCustomer) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            crmCustomer.setCustomerId(SysTools.getGUID());
            crmCustomer.setCreateUser(account.getAccountId());
            crmCustomer.setKeepUser(account.getAccountId());
            crmCustomer.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            crmCustomer.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, crmCustomerService.insertCrmCustomer(crmCustomer));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param crmCustomer
     * @return RetDataBean
     * @Title deleteCrmCustomer
     * @Description  删除客户
     */
    @RequestMapping(value = "/deleteCrmCustomer", method = RequestMethod.POST)
    public RetDataBean deleteCrmCustomer(CrmCustomer crmCustomer) {
        try {
            if (StringUtils.isBlank(crmCustomer.getCustomerId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            } else {
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, crmCustomerService.deleteCrmCustomer(crmCustomer));
            }
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 客户信息分配
     *
     * @param customerIdArr
     * @param toKeepUser
     * @return
     */
    @RequestMapping(value = "/toSetKeeepUser", method = RequestMethod.POST)
    public RetDataBean toSetKeeepUser(@RequestParam(value = "customerIdArr[]") String[] customerIdArr, String toKeepUser) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                CrmPriv crmPriv = new CrmPriv();
                crmPriv.setOrgId(account.getOrgId());
                crmPriv = crmPrivService.selectOneCrmPriv(crmPriv);
                if (("," + crmPriv.getSender() + "," + crmPriv.getManager() + ",").indexOf("," + account.getAccountId() + ",") >= 0) {
                    for (int i = 0; i < customerIdArr.length; i++) {
                        CrmCustomer crmCustomer = new CrmCustomer();
                        crmCustomer.setCustomerId(customerIdArr[i]);
                        crmCustomer.setKeepUser(toKeepUser);
                        crmCustomer.setOrgId(account.getOrgId());
                        Example example = new Example(CrmCustomer.class);
                        example.createCriteria().andEqualTo("orgId", crmCustomer.getOrgId()).andEqualTo("customerId", crmCustomer.getCustomerId());
                        crmCustomerService.UpdateCrmCustomer(crmCustomer, example);
                    }
                    return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
                } else {
                    return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
                }
            } else {
                for (int i = 0; i < customerIdArr.length; i++) {
                    CrmCustomer crmCustomer = new CrmCustomer();
                    crmCustomer.setCustomerId(customerIdArr[i]);
                    crmCustomer.setKeepUser(toKeepUser);
                    crmCustomer.setOrgId(account.getOrgId());
                    Example example = new Example(CrmCustomer.class);
                    example.createCriteria().andEqualTo("orgId", crmCustomer.getOrgId()).andEqualTo("customerId", crmCustomer.getCustomerId());
                    crmCustomerService.UpdateCrmCustomer(crmCustomer, example);
                }
                return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
            }
        } catch (Exception e) {


            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param crmLinkMan
     * @return RetDataBean
     * @Title createCrmLinkMan
     * @Description  添加客户联系人
     */
    @RequestMapping(value = "/createCrmLinkMan", method = RequestMethod.POST)
    public RetDataBean createCrmLinkMan(CrmLinkMan crmLinkMan) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            crmLinkMan.setLinkManId(SysTools.getGUID());
            crmLinkMan.setCreateUser(account.getAccountId());
            crmLinkMan.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            crmLinkMan.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, crmLinkManService.insertCrmLinkMan(crmLinkMan));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param crmLinkMan
     * @return RetDataBean
     * @Title deleteCrmLinkMan
     * @Description  删除联系人
     */
    @RequestMapping(value = "/deleteCrmLinkMan", method = RequestMethod.POST)
    public RetDataBean deleteCrmLinkMan(CrmLinkMan crmLinkMan) {
        try {
            if (StringUtils.isBlank(crmLinkMan.getLinkManId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            crmLinkMan.setOrgId(account.getOrgId());
            if (account.getOpFlag().equals("1")) {
                Example example = new Example(CrmLinkMan.class);
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("linkManId", crmLinkMan.getLinkManId());
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, crmLinkManService.deleteCrmLinkMan(crmLinkMan));
            } else {
                CrmPriv crmPriv = new CrmPriv();
                crmPriv.setOrgId(account.getOrgId());
                crmPriv = crmPrivService.selectOneCrmPriv(crmPriv);
                if (("," + crmPriv.getManager() + ",").indexOf("," + account.getAccountId() + ",") >= 0) {
                    return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, crmLinkManService.deleteCrmLinkMan(crmLinkMan));
                } else {
                    CrmLinkMan newCrmLinkMan = new CrmLinkMan();
                    newCrmLinkMan.setOrgId(account.getOrgId());
                    newCrmLinkMan.setLinkManId(crmLinkMan.getLinkManId());
                    newCrmLinkMan = crmLinkManService.selectOne(newCrmLinkMan);
                    CrmCustomer crmCustomer = new CrmCustomer();
                    crmCustomer.setOrgId(account.getOrgId());
                    crmCustomer.setCustomerId(newCrmLinkMan.getCustomerId());
                    crmCustomer = crmCustomerService.selectOne(crmCustomer);
                    if (crmCustomer.getKeepUser().equals(account.getAccountId())) {
                        return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, crmLinkManService.deleteCrmLinkMan(crmLinkMan));
                    } else {
                        return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
                    }
                }
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param crmLinkMan
     * @return RetDataBean
     * @Title updateCrmLinkMan
     * @Description  更新联系人
     */
    @RequestMapping(value = "/updateCrmLinkMan", method = RequestMethod.POST)
    public RetDataBean updateCrmLinkMan(CrmLinkMan crmLinkMan) {
        try {
            if (StringUtils.isBlank(crmLinkMan.getLinkManId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            crmLinkMan.setOrgId(account.getOrgId());
            if (account.getOpFlag().equals("1")) {
                Example example = new Example(CrmLinkMan.class);
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("linkManId", crmLinkMan.getLinkManId());
                return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, crmLinkManService.updateCrmLinkMan(crmLinkMan, example));
            } else {
                CrmPriv crmPriv = new CrmPriv();
                crmPriv.setOrgId(account.getOrgId());
                crmPriv = crmPrivService.selectOneCrmPriv(crmPriv);
                if (("," + crmPriv.getManager() + ",").indexOf("," + account.getAccountId() + ",") >= 0) {
                    Example example = new Example(CrmLinkMan.class);
                    example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("linkManId", crmLinkMan.getLinkManId());
                    return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, crmLinkManService.updateCrmLinkMan(crmLinkMan, example));
                } else {
                    CrmLinkMan newCrmLinkMan = new CrmLinkMan();
                    newCrmLinkMan.setOrgId(account.getOrgId());
                    newCrmLinkMan.setLinkManId(crmLinkMan.getLinkManId());
                    newCrmLinkMan = crmLinkManService.selectOne(newCrmLinkMan);
                    CrmCustomer crmCustomer = new CrmCustomer();
                    crmCustomer.setOrgId(account.getOrgId());
                    crmCustomer.setCustomerId(newCrmLinkMan.getCustomerId());
                    crmCustomer = crmCustomerService.selectOne(crmCustomer);
                    if (crmCustomer.getKeepUser().equals(account.getAccountId())) {
                        Example example = new Example(CrmLinkMan.class);
                        example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("linkManId", crmLinkMan.getLinkManId());
                        return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, crmLinkManService.updateCrmLinkMan(crmLinkMan, example));
                    } else {
                        return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
                    }
                }
            }
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param crmContractRecord
     * @return RetDataBean
     * @Title createCrmContactRecord
     * @Description  创建联系记录
     */
    @RequestMapping(value = "/createCrmContactRecord", method = RequestMethod.POST)
    public RetDataBean createCrmContactRecord(CrmContractRecord crmContractRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            crmContractRecord.setRecordId(SysTools.getGUID());
            crmContractRecord.setCreateUser(account.getAccountId());
            crmContractRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            crmContractRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, crmContactRecordService.addRecordAndCalendar(crmContractRecord));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param crmCustomer
     * @return RetDataBean
     * @Title updateCrmCustomer
     * @Description  更新客户信息
     */
    @RequestMapping(value = "/updateCrmCustomer", method = RequestMethod.POST)
    public RetDataBean updateCrmCustomer(CrmCustomer crmCustomer) {
        try {
            if (StringUtils.isBlank(crmCustomer.getCustomerId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            crmCustomer.setOrgId(account.getOrgId());
            Example example = new Example(CrmCustomer.class);
            example.createCriteria().andEqualTo("customerId", crmCustomer.getCustomerId()).andEqualTo("orgId", crmCustomer.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, crmCustomerService.UpdateCrmCustomer(crmCustomer, example));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param crmPriv
     * @return RetDataBean
     * @Title setCrmPriv
     * @Description  设置CRM权限
     */
    @RequestMapping(value = "/setCrmPriv", method = RequestMethod.POST)
    public RetDataBean setCrmPriv(CrmPriv crmPriv) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (account.getOpFlag().equals("1")) {
                CrmPriv newCrmPriv = new CrmPriv();
                newCrmPriv.setOrgId(account.getOrgId());
                crmPriv.setOrgId(account.getOrgId());
                int count = crmPrivService.selectCount(newCrmPriv);
                if (count <= 0) {
                    crmPriv.setPrivId(SysTools.getGUID());
                    return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, crmPrivService.insertCrmPriv(crmPriv));
                } else {
                    Example example = new Example(CrmPriv.class);
                    example.createCriteria().andEqualTo("orgId", account.getOrgId());
                    return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, crmPrivService.updateCrmPriv(crmPriv, example));
                }
            } else {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            }
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 发送电子邮件
     *
     * @param to
     * @param subject
     * @param content
     * @param attachId
     * @param sendServiceType
     * @return
     */
    @RequestMapping(value = "/sendWebMail", method = RequestMethod.POST)
    public RetDataBean sendWebMail(String to, String subject, String content, String attachId, String sendServiceType) {
        Account account = accountService.getRedisAUserInfoToAccount();
        UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
        return crmLinkManService.sendWebMail(account, to, subject, content, attachId, sendServiceType, userInfo);
    }

    /**
     * @param @param  request
     * @param @param  crmMyProduct
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: addProduct
     * @Description:  添加企业可供产品
     */
    @RequestMapping(value = "/addProduct", method = RequestMethod.POST)
    public RetDataBean addProduct(CrmMyProduct crmMyProduct) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            crmMyProduct.setProductId(SysTools.getGUID());
            crmMyProduct.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, crmMyProductService.insertCrmMyProduct(crmMyProduct));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  crmMyProduct
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: delProduct
     * @Description:  删除产品
     */
    @RequestMapping(value = "/delProduct", method = RequestMethod.POST)
    public RetDataBean delProduct(CrmMyProduct crmMyProduct) {
        try {
            if (StringUtils.isBlank(crmMyProduct.getProductId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                Account account = accountService.getRedisAUserInfoToAccount();
                crmMyProduct.setOrgId(account.getOrgId());
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, crmMyProductService.deleteCrmMyProduct(crmMyProduct));
            }
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param @param  request
     * @param @param  crmMyProduct
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: updateMyProduct
     * @Description:  更新产品信息
     */
    @RequestMapping(value = "/updateMyProduct", method = RequestMethod.POST)
    public RetDataBean updateMyProduct(CrmMyProduct crmMyProduct) {
        try {
            if (StringUtils.isBlank(crmMyProduct.getProductId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                Account account = accountService.getRedisAUserInfoToAccount();
                Example example = new Example(CrmMyProduct.class);
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("productId", crmMyProduct.getProductId());
                return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, crmMyProductService.updateCrmMyProduct(crmMyProduct, example));
            }
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  crmTags
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: addTags
     * @Description:  添加企来标签
     */
    @RequestMapping(value = "/addTags", method = RequestMethod.POST)
    public RetDataBean addTags(CrmTags crmTags) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            crmTags.setTagsId(SysTools.getGUID());
            crmTags.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, crmTagsService.insertCrmTags(crmTags));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  crmTags
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: delTags
     * @Description:  删除企来标签
     */
    @RequestMapping(value = "/delTags", method = RequestMethod.POST)
    public RetDataBean delTags(CrmTags crmTags) {
        try {
            if (StringUtils.isBlank(crmTags.getTagsId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                Account account = accountService.getRedisAUserInfoToAccount();
                crmTags.setOrgId(account.getOrgId());
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, crmTagsService.deleteCrmTags(crmTags));
            }
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  crmTags
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: updateTags
     * @Description:  更新企业标签
     */
    @RequestMapping(value = "/updateTags", method = RequestMethod.POST)
    public RetDataBean updateTags(CrmTags crmTags) {
        try {
            if (StringUtils.isBlank(crmTags.getTagsId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                Account account = accountService.getRedisAUserInfoToAccount();
                Example example = new Example(CrmTags.class);
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("tagsId", crmTags.getTagsId());
                return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, crmTagsService.updateCrmTags(crmTags, example));
            }
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  crmIndustry
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: addIndustry
     * @Description:  添加行业分类
     */
    @RequestMapping(value = "/addIndustry", method = RequestMethod.POST)
    public RetDataBean addIndustry(CrmIndustry crmIndustry) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            crmIndustry.setIndustryId(SysTools.getGUID());
            crmIndustry.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, crmIndustryService.insertCrmIndustry(crmIndustry));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  crmIndustry
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: delIndustry
     * @Description:  删除企业分类
     */
    @RequestMapping(value = "/delIndustry", method = RequestMethod.POST)
    public RetDataBean delIndustry(CrmIndustry crmIndustry) {
        try {
            if (StringUtils.isBlank(crmIndustry.getIndustryId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                Account account = accountService.getRedisAUserInfoToAccount();
                crmIndustry.setOrgId(account.getOrgId());
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, crmIndustryService.deleteCrmIndustry(crmIndustry));
            }
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  crmIndustry
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: updateTags
     * @Description:  更新行业分类
     */
    @RequestMapping(value = "/updateIndustry", method = RequestMethod.POST)
    public RetDataBean updateIndustry(CrmIndustry crmIndustry) {
        try {
            if (StringUtils.isBlank(crmIndustry.getIndustryId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                Account account = accountService.getRedisAUserInfoToAccount();
                Example example = new Example(CrmIndustry.class);
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("industryId", crmIndustry.getIndustryId());
                return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, crmIndustryService.updateCrmIndustry(crmIndustry, example));
            }
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  crmcontractInfo
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: addCrmContractInfo
     * @Description:  添加客户银行信息
     */
    @RequestMapping(value = "/addCrmContractInfo", method = RequestMethod.POST)
    public RetDataBean addCrmContractInfo(CrmContractInfo crmContractInfo) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            crmContractInfo.setContractInfoId(SysTools.getGUID());
            crmContractInfo.setCreateUser(account.getAccountId());
            crmContractInfo.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            crmContractInfo.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, crmContractInfoService.insertCrmContractInfoMapper(crmContractInfo));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  crmContractInfo
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: delCrmContractInfo
     * @Description:  删除银行信息
     */
    @RequestMapping(value = "/delCrmContractInfo", method = RequestMethod.POST)
    public RetDataBean delCrmContractInfo(CrmContractInfo crmContractInfo) {
        try {
            if (StringUtils.isBlank(crmContractInfo.getContractInfoId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                Account account = accountService.getRedisAUserInfoToAccount();
                crmContractInfo.setOrgId(account.getOrgId());
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, crmContractInfoService.deleteCrmContractInfo(crmContractInfo));
            }
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  crmContractInfo
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: updateCrmContractInfo
     * @Description:  更新银行信息
     */
    @RequestMapping(value = "/updateCrmContractInfo", method = RequestMethod.POST)
    public RetDataBean updateCrmContractInfo(CrmContractInfo crmContractInfo) {
        try {
            if (StringUtils.isBlank(crmContractInfo.getContractInfoId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                Account account = accountService.getRedisAUserInfoToAccount();
                crmContractInfo.setOrgId(account.getOrgId());
                Example example = new Example(CrmContractInfo.class);
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("contractInfoId", crmContractInfo.getContractInfoId());
                return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, crmContractInfoService.updateCrmContractInfo(crmContractInfo, example));
            }
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  crmInquiry
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: createCrmInquiry
     * @Description:  创建询价单
     */
    @RequestMapping(value = "/createCrmInquiry", method = RequestMethod.POST)
    public RetDataBean createCrmInquiry(CrmInquiry crmInquiry, String detail) {
        try {
            JSONArray jsonArr = JSONObject.parseArray(detail);
            Account account = accountService.getRedisAUserInfoToAccount();
            crmInquiry.setInquiryId(SysTools.getGUID());
            crmInquiry.setCreateUser(account.getAccountId());
            crmInquiry.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            crmInquiry.setOrgId(account.getOrgId());
            crmInquiry.setStatus("0");
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, crmInquiryService.saveCrmInquiry(crmInquiry, jsonArr));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param crmInquiry
     * @return RetDataBean
     * @Title: deleteCrmInquiry
     * @Description:  删除询价单
     */
    @RequestMapping(value = "/deleteCrmInquiry", method = RequestMethod.POST)
    public RetDataBean deleteCrmInquiry(CrmInquiry crmInquiry) {
        try {
            if (StringUtils.isBlank(crmInquiry.getInquiryId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                Account account = accountService.getRedisAUserInfoToAccount();
                crmInquiry.setOrgId(account.getOrgId());
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, crmInquiryService.deleteCrmInquiry(crmInquiry));
            }
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param crmInquiry
     * @return RetDataBean
     * @Title: updateInquiryStatus
     * @Description:  更改询价单状态
     */
    @RequestMapping(value = "/updateInquiryStatus", method = RequestMethod.POST)
    public RetDataBean updateInquiryStatus(CrmInquiry crmInquiry) {
        try {
            if (StringUtils.isBlank(crmInquiry.getInquiryId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                Account account = accountService.getRedisAUserInfoToAccount();
                crmInquiry.setOrgId(account.getOrgId());
                Example example = new Example(CrmInquiry.class);
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("inquiryId", crmInquiry.getInquiryId());
                return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, crmInquiryService.updateCrmInquiry(crmInquiry, example));
            }
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param crmInquiry
     * @return RetDataBean
     * @Title: updateCrmInquiry
     * @Description:  更新询价单
     */
    @RequestMapping(value = "/updateCrmInquiry", method = RequestMethod.POST)
    public RetDataBean updateCrmInquiry(CrmInquiry crmInquiry, String detail) {
        try {
            JSONArray jsonArr = JSONObject.parseArray(detail);
            if (StringUtils.isBlank(crmInquiry.getInquiryId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                Account account = accountService.getRedisAUserInfoToAccount();
                crmInquiry.setOrgId(account.getOrgId());
                Example example = new Example(CrmInquiry.class);
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("inquiryId", crmInquiry.getInquiryId());
                return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, crmInquiryService.updateInquiryAndDetail(crmInquiry, example, jsonArr));
            }
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }
}
