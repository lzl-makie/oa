package com.core136.controller.crm;

import com.core136.bean.account.Account;
import com.core136.bean.crm.*;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.service.account.AccountService;
import com.core136.service.crm.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: RoutSetCrmController
 * @Description: CRM管理数据接口
 * @author: 稠云信息
 * @date: 2019年2月12日 上午9:25:07
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@RestController
@RequestMapping("/ret/crmget")
public class RouteGetCrmController {
    private CrmCustomerService crmCustomerService;

    @Autowired
    public void setCrmCustomerService(CrmCustomerService crmCustomerService) {
        this.crmCustomerService = crmCustomerService;
    }

    private CrmLinkManService crmLinkManService;

    @Autowired
    public void setCrmLinkManService(CrmLinkManService crmLinkManService) {
        this.crmLinkManService = crmLinkManService;
    }

    private CrmContactRecordService crmContactRecordService;

    @Autowired
    public void setCrmContactRecordService(CrmContactRecordService crmContactRecordService) {
        this.crmContactRecordService = crmContactRecordService;
    }

    private CrmPrivService crmPrivService;

    @Autowired
    public void setCrmPrivService(CrmPrivService crmPrivService) {
        this.crmPrivService = crmPrivService;
    }

    private CrmIndustryService crmIndustryService;

    @Autowired
    public void setCrmIndustryService(CrmIndustryService crmIndustryService) {
        this.crmIndustryService = crmIndustryService;
    }

    private CrmTagsService crmTagsService;

    @Autowired
    public void setCrmTagsService(CrmTagsService crmTagsService) {
        this.crmTagsService = crmTagsService;
    }

    private CrmMyProductService crmMyProductService;

    @Autowired
    public void setCrmMyProductService(CrmMyProductService crmMyProductService) {
        this.crmMyProductService = crmMyProductService;
    }

    private CrmContractInfoService crmContractInfoService;

    @Autowired
    public void setCrmContractInfoService(CrmContractInfoService crmContractInfoService) {
        this.crmContractInfoService = crmContractInfoService;
    }

    private CrmInquiryService crmInquiryService;

    @Autowired
    public void setCrmInquiryService(CrmInquiryService crmInquiryService) {
        this.crmInquiryService = crmInquiryService;
    }

    private CrmInquiryDetailService crmInquiryDetailService;

    @Autowired
    public void setCrmInquiryDetailService(CrmInquiryDetailService crmInquiryDetailService) {
        this.crmInquiryDetailService = crmInquiryDetailService;
    }

    private CrmQuotationService crmQuotationService;

    @Autowired
    public void setCrmQuotationService(CrmQuotationService crmQuotationService) {
        this.crmQuotationService = crmQuotationService;
    }

    private CrmQuotationMxService crmQuotationMxService;

    @Autowired
    public void setCrmQuotationMxService(CrmQuotationMxService crmQuotationMxService) {
        this.crmQuotationMxService = crmQuotationMxService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * @return RetDataBean
     * @Title: getCrmApprovedUserList
     * @Description:  获取审批人员列表
     */
    @RequestMapping(value = "/getCrmApprovedUserList", method = RequestMethod.POST)
    public RetDataBean getCrmApprovedUserList() {
        try {
            List<Map<String, String>> teMaps = new ArrayList<Map<String, String>>();
            Account account = accountService.getRedisAUserInfoToAccount();
            CrmPriv crmPriv = new CrmPriv();
            crmPriv.setOrgId(account.getOrgId());
            crmPriv = crmPrivService.selectOneCrmPriv(crmPriv);
            String manageStr = crmPriv.getManager();
            if (StringUtils.isNotBlank(manageStr)) {
                String[] accountArr = manageStr.split(",");
                List<String> accountList = Arrays.asList(accountArr);
                teMaps = accountService.getUserNamesByAccountIds(accountList, account.getOrgId());
            }

            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, teMaps);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param crmQuotationMx
     * @return RetDataBean
     * @Title: getCrmQuotationMxById
     * @Description:  报价明细详情
     */
    @RequestMapping(value = "/getCrmQuotationMxById", method = RequestMethod.POST)
    public RetDataBean getCrmQuotationMxById(CrmQuotationMx crmQuotationMx) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            crmQuotationMx.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmQuotationMxService.selectOneCrmQuotationMx(crmQuotationMx));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getCrmInquiryListForSelect
     * @Description:  获取询价单列表
     */
    @RequestMapping(value = "/getCrmInquiryListForSelect", method = RequestMethod.POST)
    public RetDataBean getCrmInquiryListForSelect() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmInquiryService.getCrmInquiryListForSelect(account.getOrgId(), account.getAccountId()));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param crmQuotation
     * @return RetDataBean
     * @Title: getCrmQuotationById
     * @Description:  获取报价单详情
     */
    @RequestMapping(value = "/getCrmQuotationById", method = RequestMethod.POST)
    public RetDataBean getCrmQuotationById(CrmQuotation crmQuotation) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            crmQuotation.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmQuotationService.selectOneCrmQuotation(crmQuotation));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageNumber
     * @param pageSize
     * @param search
     * @param sort
     * @param sortOrder
     * @return RetDataBean
     * @Title getCrmCustomerList
     * @Description  获取客户列表
     */
    @RequestMapping(value = "/getCrmCustomerList", method = RequestMethod.POST)
    public RetDataBean getCrmCustomerList(
            Integer pageNumber,
            Integer pageSize,
            String search,
            String sort,
            String sortOrder,
            String industry,
            String model,
            String level,
            String intention,
            String country,
            String province,
            String city,
            String tags
    ) {
        try {
            if (StringUtils.isBlank(sort)) {
                sort = "create_time";
            } else {
                sort = StrTools.lowerCharToUnderLine(sort);
            }
            if (StringUtils.isBlank(sortOrder)) {
                sortOrder = "asc";
            }
            String orderBy = sort + " " + sortOrder;
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(CrmCustomer.class);
            example.setOrderByClause(orderBy);
            Criteria criteria = example.createCriteria();
            if (!account.getOpFlag().equals("1")) {
                criteria.andEqualTo("keepUser", account.getAccountId());
            }
            criteria.andEqualTo("orgId", account.getOrgId());
            if (StringUtils.isNotBlank(search)) {
                Criteria criteria2 = example.createCriteria();
                criteria2.orLike("cnName", "%" + search + "%").orLike("enName", "%" + search + "%").orLike("focusProduct", "%" + search + "%");
                if (StringUtils.isNotBlank(industry)) {
                    criteria.andLike("industry", "%" + industry + "%");
                }

                if (StringUtils.isNotBlank(tags)) {
                    List<String> arrayList = new ArrayList<String>();
                    if (StringUtils.isNotBlank(tags)) {
                        String[] tagsArr = null;
                        if (tags.indexOf(",") > 0) {
                            tagsArr = tags.split(",");
                        } else {
                            tagsArr = new String[]{tags};
                        }
                        arrayList = Arrays.asList(tagsArr);
                    }

                    Criteria criteria3 = example.createCriteria();
                    for (int i = 0; i < arrayList.size(); i++) {
                        criteria3.orLike("tags", "%" + arrayList.get(i) + "%");
                    }
                    example.and(criteria3);
                }
                if (StringUtils.isNotBlank(model)) {
                    criteria.andLike("model", "%" + model + "%");
                }
                if (StringUtils.isNotBlank(level)) {
                    criteria.andLike("level", "%" + level + "%");
                }
                if (StringUtils.isNotBlank(intention)) {
                    criteria.andLike("intention", "%" + intention + "%");
                }
                if (StringUtils.isNotBlank(country)) {
                    if (!country.equals("0")) {
                        criteria.andLike("country", "%" + country + "%");
                    }
                }
                if (StringUtils.isNotBlank(province)) {
                    if (!province.equals("0")) {
                        criteria.andLike("province", "%" + province + "%");
                    }
                }
                if (StringUtils.isNotBlank(city)) {
                    if (!city.equals("0")) {
                        criteria.andLike("city", "%" + city + "%");
                    }
                }
                example.and(criteria2);
            } else {
                Criteria criteria2 = example.createCriteria();
                if (StringUtils.isNotBlank(industry)) {
                    criteria.andLike("industry", "%" + industry + "%");
                }
                if (StringUtils.isNotBlank(tags)) {
                    //criteria.andLike("tags", "%"+tags+"%");
                    List<String> arrayList = new ArrayList<String>();
                    if (StringUtils.isNotBlank(tags)) {
                        String[] tagsArr = null;
                        if (tags.indexOf(",") > 0) {
                            tagsArr = tags.split(",");
                        } else {
                            tagsArr = new String[]{tags};
                        }
                        arrayList = Arrays.asList(tagsArr);
                    }

                    Criteria criteria3 = example.createCriteria();
                    for (int i = 0; i < arrayList.size(); i++) {
                        criteria3.orLike("tags", "%" + arrayList.get(i) + "%");
                    }
                    example.and(criteria3);
                }
                if (StringUtils.isNotBlank(model)) {
                    criteria.andLike("model", "%" + model + "%");
                }
                if (StringUtils.isNotBlank(level)) {
                    criteria.andLike("level", "%" + level + "%");
                }
                if (StringUtils.isNotBlank(intention)) {
                    criteria.andLike("intention", "%" + intention + "%");
                }
                if (StringUtils.isNotBlank(country)) {
                    if (!country.equals("0")) {
                        criteria.andLike("country", "%" + country + "%");
                    }
                }
                if (StringUtils.isNotBlank(province)) {
                    if (!province.equals("0")) {
                        criteria.andLike("province", "%" + province + "%");
                    }
                }
                if (StringUtils.isNotBlank(city)) {
                    if (!city.equals("0")) {
                        criteria.andLike("city", "%" + city + "%");
                    }
                }
                example.and(criteria2);
            }
            PageInfo<CrmCustomer> pageInfo = crmCustomerService.getCrmCustomerList(example, pageNumber, pageSize);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param crmCustomer
     * @return RetDataBean
     * @Title getCrmCustomerById
     * @Description  获取客户基本信息
     */
    @RequestMapping(value = "/getCrmCustomerById", method = RequestMethod.POST)
    public RetDataBean getCrmCustomerById(CrmCustomer crmCustomer) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            crmCustomer.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmCustomerService.selectOne(crmCustomer));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param customerId
     * @return RetDataBean
     * @Title getCrmLinkManList
     * @Description  获取客户联系人
     */
    @RequestMapping(value = "/getCrmLinkManList", method = RequestMethod.POST)
    public RetDataBean getCrmLinkManList(String customerId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmLinkManService.getCrmLinkManList(account.getOrgId(), customerId));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param crmLinkMan
     * @return RetDataBean
     * @Title getCrmLinkMan
     * @Description  获取联系人详情
     */
    @RequestMapping(value = "/getCrmLinkMan", method = RequestMethod.POST)
    public RetDataBean getCrmLinkMan(CrmLinkMan crmLinkMan) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            crmLinkMan.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmLinkManService.selectOne(crmLinkMan));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param crmLinkMan
     * @return RetDataBean
     * @Title getCrmLinkManInfo
     * @Description  获取联系人信息
     */
    @RequestMapping(value = "/getCrmLinkManInfo", method = RequestMethod.POST)
    public RetDataBean getCrmLinkManInfo(CrmLinkMan crmLinkMan) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            crmLinkMan.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmLinkManService.getCrmLinkManInfo(crmLinkMan.getOrgId(), crmLinkMan.getLinkManId()));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param crmLinkMan
     * @return RetDataBean
     * @Title getCrmLinkManByCustomerId
     * @Description  获取企业下所有联系人
     */
    @RequestMapping(value = "/getCrmLinkManByCustomerId", method = RequestMethod.POST)
    public RetDataBean getCrmLinkManByCustomerId(CrmLinkMan crmLinkMan) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            crmLinkMan.setOrgId(account.getOrgId());
            Example example = new Example(CrmLinkMan.class);
            example.createCriteria().andEqualTo("orgId", crmLinkMan.getOrgId()).andEqualTo("customerId", crmLinkMan.getCustomerId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmLinkManService.getCrmLinkManByCustomerId(example));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageNumber
     * @param pageSize
     * @param customerId
     * @return RetDataBean
     * @Title getRecordByCustomerId
     * @Description  获取企业联系记录
     */
    @RequestMapping(value = "/getRecordByCustomerId", method = RequestMethod.POST)
    public RetDataBean getRecordByCustomerId(
            Integer pageNumber,
            Integer pageSize,
            String customerId
    ) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            PageInfo<Map<String, Object>> pageInfo = crmContactRecordService.getRecordListByCustomerId(pageNumber, pageSize, account.getOrgId(), customerId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param inquiryId
     * @return RetDataBean
     * @Title: getCrmInquiryDetailList
     * @Description:  询价单产品详情
     */
    @RequestMapping(value = "/getCrmInquiryDetailList", method = RequestMethod.POST)
    public RetDataBean getCrmInquiryDetailList(
            PageParam pageParam,
            String inquiryId
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("d.create_time");
            } else {
                pageParam.setSort("d." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOpFlag(account.getOpFlag());
            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            pageParam.setOrderBy(orderBy);
            pageParam.setOrgId(account.getOrgId());
            PageInfo<Map<String, String>> pageInfo = crmInquiryDetailService.getCrmInquiryDetailList(pageParam, inquiryId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param inquiryId
     * @param quotationId
     * @return RetDataBean
     * @Title: getCrmInquiryDetailListForQuotation
     * @Description:  获取报价单明细
     */
    @RequestMapping(value = "/getCrmInquiryDetailListForQuotation", method = RequestMethod.POST)
    public RetDataBean getCrmInquiryDetailListForQuotation(
            PageParam pageParam,
            String inquiryId,
            String quotationId
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("d.create_time");
            } else {
                pageParam.setSort("d." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOpFlag(account.getOpFlag());
            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            pageParam.setOrderBy(orderBy);
            pageParam.setOrgId(account.getOrgId());
            PageInfo<Map<String, String>> pageInfo = crmQuotationMxService.getCrmInquiryDetailListForQuotation(pageParam, inquiryId, quotationId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 获取CRM联系人列表
     *
     * @param pageParam
     * @return
     */
    @RequestMapping(value = "/getCrmLinkManAllList", method = RequestMethod.POST)
    public RetDataBean getCrmLinkManAllList(
            PageParam pageParam
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("customer_id");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrderBy(orderBy);
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrgId(account.getOrgId());
            PageInfo<Map<String, Object>> pageInfo = new PageInfo<Map<String, Object>>();
            if (account.getOpFlag().equals("1")) {
                pageInfo = crmLinkManService.getCrmLinkManAllList(pageParam);
            } else {
                CrmPriv crmPriv = new CrmPriv();
                crmPriv.setOrgId(account.getOrgId());
                crmPriv = crmPrivService.selectOneCrmPriv(crmPriv);
                if (StringUtils.isNotBlank(crmPriv.getManager())) {
                    if (("," + crmPriv.getManager() + ",").indexOf("," + account.getAccountId() + ",") >= 0) {
                        pageInfo = crmLinkManService.getCrmLinkManAllList(pageParam);
                    }
                }
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * ]
     *
     * @param @param  sortId
     * @param @param  pageNumber
     * @param @param  pageSize
     * @param @param  search
     * @param @param  sort
     * @param @param  sortOrder
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getMyCrmLinkManAllList
     * @Description:  获取个人客户联系人列表
     */
    @RequestMapping(value = "/getMyCrmLinkManAllList", method = RequestMethod.POST)
    public RetDataBean getMyCrmLinkManAllList(
            Integer pageNumber,
            Integer pageSize,
            String search,
            String sort,
            String sortOrder
    ) {
        try {
            if (StringUtils.isBlank(sort)) {
                sort = "customer_id";
            } else {
                sort = StrTools.lowerCharToUnderLine(sort);
            }
            if (StringUtils.isBlank(sortOrder)) {
                sortOrder = "asc";
            }

            String orderBy = sort + " " + sortOrder;
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmLinkManService.getMyCrmLinkManAllList(pageNumber, pageSize, orderBy, account.getOrgId(), account.getAccountId(), search));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title getCrmPriv
     * @Description  获取CRM管理权限
     */
    @RequestMapping(value = "/getCrmPriv", method = RequestMethod.POST)
    public RetDataBean getCrmPriv() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            CrmPriv crmPriv = new CrmPriv();
            crmPriv.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmPrivService.selectOneCrmPriv(crmPriv));
        } catch (Exception e) {


            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageNumber
     * @param pageSize
     * @param search
     * @param sort
     * @param sortOrder
     * @return RetDataBean
     * @Title getAllCrmCustomerList
     * @Description 获取权限内所有客户
     */
    @RequestMapping(value = "/getAllCrmCustomerList", method = RequestMethod.POST)
    public RetDataBean getAllCrmCustomerList(
            Integer pageNumber,
            Integer pageSize,
            String search,
            String sort,
            String sortOrder,
            String source,
            String model,
            String roles,
            String industry,
            String keepUser,
            String country,
            String province,
            String city,
            String level,
            String intention,
            String opponent,
            String tags
    ) {
        try {
            if (StringUtils.isBlank(sort)) {
                sort = "create_time";
            } else {
                sort = StrTools.lowerCharToUnderLine(sort);
            }
            if (StringUtils.isBlank(sortOrder)) {
                sortOrder = "asc";
            }
            List<String> list = new ArrayList<String>();
            if (StringUtils.isNotBlank(tags)) {
                String[] tagsArr = null;
                if (tags.indexOf(",") > 0) {
                    tagsArr = tags.split(",");
                } else {
                    tagsArr = new String[]{tags};
                }
                list = Arrays.asList(tagsArr);
            }
            String orderBy = sort + " " + sortOrder;
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                CrmPriv crmPriv = new CrmPriv();
                crmPriv.setOrgId(account.getOrgId());
                crmPriv = crmPrivService.selectOneCrmPriv(crmPriv);
                if (("," + crmPriv.getManager() + ",").indexOf("," + account.getAccountId() + ",") >= 0) {
                    PageInfo<Map<String, Object>> pageInfo = crmCustomerService.getAllCrmCustomerList(pageNumber, pageSize, account.getOrgId(), "%" + source + "%", "%" + model + "%", "%" + roles + "%", "%" + industry + "%", "%" + keepUser + "%", "%" + search + "%",
                            "%" + country + "%", "%" + province + "%", "%" + city + "%", "%" + level + "%", "%" + intention + "%", "%" + opponent + "%", list, orderBy);
                    return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
                } else {
                    return RetDataTools.NotOk(MessageCode.MESSAGE_FAILED);
                }

            } else {
                PageInfo<Map<String, Object>> pageInfo = crmCustomerService.getAllCrmCustomerList(pageNumber, pageSize, account.getOrgId(), "%" + source + "%", "%" + model + "%", "%" + roles + "%", "%" + industry + "%", "%" + keepUser + "%", "%" + search + "%",
                        "%" + country + "%", "%" + province + "%", "%" + city + "%", "%" + level + "%", "%" + intention + "%", "%" + opponent + "%", list, orderBy);
                return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
            }
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param search
     * @return RetDataBean
     * @Title getSelect2CustomerList
     * @Description 获取SELECT2客户列表
     */
    @RequestMapping(value = "/getSelect2CustomerList", method = RequestMethod.POST)
    public RetDataBean getSelect2CustomerList(String search) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            CrmPriv crmPriv = new CrmPriv();
            crmPriv.setOrgId(account.getOrgId());
            crmPriv = crmPrivService.selectOneCrmPriv(crmPriv);
            if (account.getOpFlag().equals("1")) {
                return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmCustomerService.getSelect2CustomerList(account.getOrgId(), "", "%" + search + "%"));
            } else {
                if (crmPriv.getManager().equals(account.getAccountId())) {
                    return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmCustomerService.getSelect2CustomerList(account.getOrgId(), "", "%" + search + "%"));
                } else {
                    return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmCustomerService.getSelect2CustomerList(account.getOrgId(), account.getAccountId(), "%" + search + "%"));
                }
            }
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title getCrmPriv
     * @Description  获取业务列表
     */
    @RequestMapping(value = "/getCrmSaleList", method = RequestMethod.POST)
    public RetDataBean getCrmPriv(String search) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            CrmPriv crmPriv = new CrmPriv();
            crmPriv.setOrgId(account.getOrgId());
            crmPriv = crmPrivService.selectOneCrmPriv(crmPriv);
            String sales = crmPriv.getSale();
            if (StringUtils.isNotBlank(sales)) {
                String[] accountArr;
                if (sales.indexOf(",") >= 0) {
                    accountArr = sales.split(",");
                } else {
                    accountArr = new String[]{sales};
                }
                List<String> list = new ArrayList<String>();
                for (int i = 0; i < accountArr.length; i++) {
                    list.add(accountArr[i]);
                }
                return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, accountService.getCrmSaleList(list, account.getOrgId(), "%" + search + "%"));
            } else {
                return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, null);
            }
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  industryId
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getCrmTagsbyIndustryList
     * @Description: 按行业分类获取企业标签
     */
    @RequestMapping(value = "/getCrmTagsbyIndustryList", method = RequestMethod.POST)
    public RetDataBean getCrmTagsbyIndustryList(String industryId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmTagsService.getAllTags(account.getOrgId(), industryId));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  industryId
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getCrmTagsList
     * @Description:  获取所有企业分类
     */
    @RequestMapping(value = "/getCrmTagsList", method = RequestMethod.POST)
    public RetDataBean getCrmTagsList(String industryId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmTagsService.getAllTags(account.getOrgId(), industryId));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  levelId
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getCrmIndustryList
     * @Description:  获取行业分类列表
     */
    @RequestMapping(value = "/getCrmIndustryList", method = RequestMethod.POST)
    public RetDataBean getCrmIndustryList() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(CrmIndustry.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmIndustryService.selectCrmIndustry(example));
        } catch (Exception e) {


            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  levelId
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getCrmMyProductList
     * @Description:  获取公司可供产品列表
     */
    @RequestMapping(value = "/getCrmMyProductList", method = RequestMethod.POST)
    public RetDataBean getCrmMyProductList() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmMyProductService.getAllMyProduct(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  productIds
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getMyProductNameStr
     * @Description:  按productId获取参应的产品名称
     */
    @RequestMapping(value = "/getMyProductNameStr", method = RequestMethod.POST)
    public RetDataBean getMyProductNameStr(String productIds) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            String[] productIdArr;
            if (StringUtils.isNotBlank(productIds)) {
                if (productIds.indexOf(",") > -1) {
                    productIdArr = productIds.split(",");
                } else {
                    productIdArr = new String[]{productIds};
                }
                List<String> list = Arrays.asList(productIdArr);
                return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmMyProductService.getMyProductNameStr(account.getOrgId(), list));
            } else {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }

        } catch (Exception e) {


            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  sortId
     * @param @param  pageNumber
     * @param @param  pageSize
     * @param @param  search
     * @param @param  sort
     * @param @param  sortOrder
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getAllProduct
     * @Description:  获取所有产品列表
     */
    @RequestMapping(value = "/getAllProduct", method = RequestMethod.POST)
    public RetDataBean getAllProduct(
            Integer pageNumber,
            Integer pageSize,
            String search,
            String sort,
            String sortOrder
    ) {
        try {
            if (StringUtils.isBlank(sort)) {
                sort = "sort_no";
            } else {
                sort = StrTools.lowerCharToUnderLine(sort);
            }
            if (StringUtils.isBlank(sortOrder)) {
                sortOrder = "asc";
            }
            String orderBy = sort + " " + sortOrder;
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmMyProductService.getAllProductList(pageNumber, pageSize, orderBy, account.getOrgId(), "%" + search + "%"));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  crmMyProduct
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getMyProduct
     * @Description:  获取产品详情
     */
    @RequestMapping(value = "/getMyProduct", method = RequestMethod.POST)
    public RetDataBean getMyProduct(CrmMyProduct crmMyProduct) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            crmMyProduct.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmMyProductService.selectOneCrmMyProdcut(crmMyProduct));
        } catch (Exception e) {


            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  crmTags
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getTags
     * @Description:  获取标签详情
     */
    @RequestMapping(value = "/getCrmTags", method = RequestMethod.POST)
    public RetDataBean getTags(CrmTags crmTags) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            crmTags.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmTagsService.selectOneCrmTags(crmTags));
        } catch (Exception e) {


            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param @param  sortId
     * @param @param  pageNumber
     * @param @param  pageSize
     * @param @param  search
     * @param @param  sort
     * @param @param  sortOrder
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getAllTags
     * @Description:  获取企业标签列表
     */
    @RequestMapping(value = "/getAllTags", method = RequestMethod.POST)
    public RetDataBean getAllTags(
            Integer pageNumber,
            Integer pageSize,
            String search,
            String sort,
            String sortOrder
    ) {
        try {
            if (StringUtils.isBlank(sort)) {
                sort = "b.industry_id";
            } else {
                sort = "b." + StrTools.lowerCharToUnderLine(sort);
            }
            if (StringUtils.isBlank(sortOrder)) {
                sortOrder = "DESC";
            }
            String orderBy = sort + " " + sortOrder;
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmTagsService.getAllTagsList(pageNumber, pageSize, orderBy, account.getOrgId(), "%" + search + "%"));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  crmIndustry
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getCrmIndustry
     * @Description:  获取企业分类详情
     */
    @RequestMapping(value = "/getCrmIndustry", method = RequestMethod.POST)
    public RetDataBean getCrmIndustry(CrmIndustry crmIndustry) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            crmIndustry.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmIndustryService.selectOneCrmIndustry(crmIndustry));
        } catch (Exception e) {


            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  sortId
     * @param @param  pageNumber
     * @param @param  pageSize
     * @param @param  search
     * @param @param  sort
     * @param @param  sortOrder
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getAllIndustryList
     * @Description:  获取行业分类列表
     */
    @RequestMapping(value = "/getAllIndustryList", method = RequestMethod.POST)
    public RetDataBean getAllIndustryList(
            Integer pageNumber,
            Integer pageSize,
            String search,
            String sort,
            String sortOrder
    ) {
        try {
            if (StringUtils.isBlank(sort)) {
                sort = "sort_no";
            } else {
                sort = StrTools.lowerCharToUnderLine(sort);
            }
            if (StringUtils.isBlank(sortOrder)) {
                sortOrder = "desc";
            }
            String orderBy = sort + " " + sortOrder;
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmIndustryService.getAllIndustryList(pageNumber, pageSize, orderBy, account.getOrgId(), "%" + search + "%"));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param @param  sortId
     * @param @param  pageNumber
     * @param @param  pageSize
     * @param @param  search
     * @param @param  sort
     * @param @param  sortOrder
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getCrmContractInfoList
     * @Description:  获取银行信息列表
     */
    @RequestMapping(value = "/getCrmContractInfoList", method = RequestMethod.POST)
    public RetDataBean getCrmContractInfoList(
            Integer pageNumber,
            Integer pageSize,
            String search,
            String sort,
            String sortOrder
    ) {
        try {
            if (StringUtils.isBlank(sort)) {
                sort = "c.sort_no";
            } else {
                sort = "c." + StrTools.lowerCharToUnderLine(sort);
            }
            if (StringUtils.isBlank(sortOrder)) {
                sortOrder = "asc";
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            String orderBy = sort + " " + sortOrder;
            PageInfo<Map<String, Object>> pageInfo = crmContractInfoService.getCrmContractInfoList(pageNumber, pageSize, orderBy, account.getOrgId(), "%" + search + "%");
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  crmContractInfo
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getCrmContractInfo
     * @Description:  获取对银行信息详情
     */
    @RequestMapping(value = "/getCrmContractInfo", method = RequestMethod.POST)
    public RetDataBean getCrmContractInfo(CrmContractInfo crmContractInfo) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            crmContractInfo.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmContractInfoService.selectOneCrmContractInfo(crmContractInfo));
        } catch (Exception e) {


            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  crmContractInfo
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: selectCrmContractInfoList
     * @Description:  获取客户银行信息列表
     */
    @RequestMapping(value = "/selectCrmContractInfoList", method = RequestMethod.POST)
    public RetDataBean selectCrmContractInfoList(CrmContractInfo crmContractInfo) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            crmContractInfo.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmContractInfoService.selectCrmContractInfoList(crmContractInfo));
        } catch (Exception e) {


            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param @param  contractInfoId
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getContractInfoById
     * @Description:  获取银行信息详情
     */
    @RequestMapping(value = "/getContractInfoById", method = RequestMethod.POST)
    public RetDataBean getContractInfoById(String contractInfoId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmContractInfoService.getContractInfoById(account.getOrgId(), contractInfoId));
        } catch (Exception e) {


            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  contractInfoId
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getAllSender
     * @Description:  获取所有销售人员列表
     */
    @RequestMapping(value = "/getAllSender", method = RequestMethod.POST)
    public RetDataBean getAllSender(String search) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmPrivService.getAllSender(account.getOrgId(), "%" + search + "%"));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  sortId
     * @param @param  pageNumber
     * @param @param  pageSize
     * @param @param  search
     * @param @param  sort
     * @param @param  sortOrder
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getCrmInquiryList
     * @Description:  获限权限内的询价单列表
     */
    @RequestMapping(value = "/getCrmInquiryList", method = RequestMethod.POST)
    public RetDataBean getCrmInquiryList(
            PageParam pageParam,
            String beginTime,
            String endTime,
            String customerType,
            String status
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("i.create_time");
            } else {
                pageParam.setSort("i." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrgId(account.getOrgId());
            CrmPriv crmPriv = new CrmPriv();
            crmPriv.setOrgId(account.getOrgId());
            crmPriv = crmPrivService.selectOneCrmPriv(crmPriv);
            if (crmPriv == null) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            if (("," + crmPriv.getSender() + ",").indexOf("," + account.getAccountId() + ",") >= 0) {
                pageParam.setOpFlag("1");
            }
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = crmInquiryService.getCrmInquiryList(pageParam, beginTime, endTime, customerType, status);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  crmInquiry
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getCrmInquiry
     * @Description:  获取询价单基本信息
     */
    @RequestMapping(value = "/getCrmInquiry", method = RequestMethod.POST)
    public RetDataBean getCrmInquiry(CrmInquiry crmInquiry) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            crmInquiry.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, crmInquiryService.selectOneCrmInquiry(crmInquiry));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取报价列表
     *
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @param approvedUser
     * @param status
     * @return
     */
    @RequestMapping(value = "/getMyCrmQuotationList", method = RequestMethod.POST)
    public RetDataBean getMyCrmQuotationList(
            PageParam pageParam,
            String beginTime,
            String endTime,
            String approvedUser,
            String status
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("i.create_time");
            } else {
                pageParam.setSort("i." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = crmQuotationService.getMyCrmQuotationList(pageParam, beginTime, endTime, status, approvedUser);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @return RetDataBean
     * @Title: getMyApprovedList
     * @Description:  获取审批列表
     */
    @RequestMapping(value = "/getMyApprovedList", method = RequestMethod.POST)
    public RetDataBean getMyApprovedList(
            PageParam pageParam
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("i.create_time");
            } else {
                pageParam.setSort("i" + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = crmQuotationService.getMyApprovedList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @param createUser
     * @param status
     * @return RetDataBean
     * @Title: getApprovedQueryList
     * @Description:  审批历史记录查询
     */
    @RequestMapping(value = "/getApprovedQueryList", method = RequestMethod.POST)
    public RetDataBean getApprovedQueryList(
            PageParam pageParam,
            String beginTime,
            String endTime,
            String createUser,
            String status
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("i.create_time");
            } else {
                pageParam.setSort("i." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = crmQuotationService.getApprovedQueryList(pageParam, createUser, beginTime, endTime, status);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @param createUser
     * @param approvedUser
     * @param status
     * @return RetDataBean
     * @Title: getQuotationQueryList
     * @Description:  报价单查询列表
     */
    @RequestMapping(value = "/getQuotationQueryList", method = RequestMethod.POST)
    public RetDataBean getQuotationQueryList(
            PageParam pageParam,
            String beginTime,
            String endTime,
            String createUser,
            String approvedUser,
            String status
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("i.create_time");
            } else {
                pageParam.setSort("i." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = crmQuotationService.getQuotationQueryList(pageParam, approvedUser, createUser, beginTime, endTime, status);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }
}
