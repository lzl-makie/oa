/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: RoutGetContractController.java
 * @Package com.core136.controller.contract
 * @Description: (用一句话描述该文件做什么)
 * @author: 稠云信息
 * @date: 2019年4月23日 下午1:57:12
 * @version V1.0
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
package com.core136.controller.contract;

import com.core136.bean.account.Account;
import com.core136.bean.contract.*;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.contract.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author lsq
 * @ClassName: RoutGetContractController
 * @Description:(这里用一句话描述这个类的作用)
 * @author: 稠云信息
 * @date: 2019年4月23日 下午1:57:12
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@RestController
@RequestMapping("/ret/contractget")
public class RouteGetContractController {
    private ContractSortService contractSortService;

    @Autowired
    public void setContractSortService(ContractSortService contractSortService) {
        this.contractSortService = contractSortService;
    }

    private ContractService contractService;

    @Autowired
    public void setContractService(ContractService contractService) {
        this.contractService = contractService;
    }

    private ContractDetailsService contractDetailsService;

    @Autowired
    public void setContractDetailsService(ContractDetailsService contractDetailsService) {
        this.contractDetailsService = contractDetailsService;
    }

    private ContractPrivService contractPrivService;

    @Autowired
    public void setContractPrivService(ContractPrivService contractPrivService) {
        this.contractPrivService = contractPrivService;
    }

    private ContractReceivablesService contractReceivablesService;

    @Autowired
    public void setContractReceivablesService(ContractReceivablesService contractReceivablesService) {
        this.contractReceivablesService = contractReceivablesService;
    }

    private ContractPayableService contractPayableService;

    @Autowired
    public void setContractPayableService(ContractPayableService contractPayableService) {
        this.contractPayableService = contractPayableService;
    }

    private ContractBillService contractBillService;

    @Autowired
    public void setContractBillService(ContractBillService contractBillService) {
        this.contractBillService = contractBillService;
    }

    private ContractSendgoodsService contractSendgoodsService;

    @Autowired
    public void setContractSendgoodsService(ContractSendgoodsService contractSendgoodsService) {
        this.contractSendgoodsService = contractSendgoodsService;
    }

    private ContractReceivablesRecordService contractReceivablesRecordService;

    @Autowired
    public void setContractReceivablesRecordService(ContractReceivablesRecordService contractReceivablesRecordService) {
        this.contractReceivablesRecordService = contractReceivablesRecordService;
    }

    private ContractPayableRecordService contractPayableRecordService;

    @Autowired
    public void setContractPayableRecordService(ContractPayableRecordService contractPayableRecordService) {
        this.contractPayableRecordService = contractPayableRecordService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * @param request
     * @return RetDataBean
     * @Title: getContractTop
     * @Description:  获取近期的合同列表
     */
    @RequestMapping(value = "/getContractTop", method = RequestMethod.POST)
    public RetDataBean getContractTop(HttpServletRequest request) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, contractService.getContractTop(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param request
     * @return RetDataBean
     * @Title: getContractBillTop
     * @Description:  获取近期发票列表
     */
    @RequestMapping(value = "/getContractBillTop", method = RequestMethod.POST)
    public RetDataBean getContractBillTop(HttpServletRequest request) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, contractBillService.getContractBillTop(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param request
     * @return RetDataBean
     * @Title: getPayableRecordTop
     * @Description:  近期付款记录
     */
    @RequestMapping(value = "/getPayableRecordTop", method = RequestMethod.POST)
    public RetDataBean getPayableRecordTop(HttpServletRequest request) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, contractPayableRecordService.getPayableRecordTop(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param request
     * @return RetDataBean
     * @Title: getReceivRecordTop
     * @Description:  收款记录
     */
    @RequestMapping(value = "/getReceivRecordTop", method = RequestMethod.POST)
    public RetDataBean getReceivRecordTop(HttpServletRequest request) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, contractReceivablesRecordService.getReceivRecordTop(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param request
     * @return RetDataBean
     * @Title: getDeskBillList
     * @Description:  获取财务门户发票列表
     */
    @RequestMapping(value = "/getDeskBillList", method = RequestMethod.POST)
    public RetDataBean getDeskBillList(HttpServletRequest request) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, contractBillService.getDeskBillList(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param request
     * @return RetDataBean
     * @Title: getDeskPayableList
     * @Description:  获取财务门户近期付款记录
     */
    @RequestMapping(value = "/getDeskPayableList", method = RequestMethod.POST)
    public RetDataBean getDeskPayableList(HttpServletRequest request) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            String beginTime = SysTools.getTime("yyyy-MM-dd HH:mm:ss");
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, contractPayableService.getDeskPayableList(account.getOrgId(), beginTime, account.getOpFlag(), account.getAccountId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param request
     * @return RetDataBean
     * @Title: getDeskReceivablesList
     * @Description:  获取财务门户近期收款情况
     */
    @RequestMapping(value = "/getDeskReceivablesList", method = RequestMethod.POST)
    public RetDataBean getDeskReceivablesList(HttpServletRequest request) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            String beginTime = SysTools.getTime("yyyy-MM-dd HH:mm:ss");
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, contractReceivablesService.getDeskReceivablesList(account.getOrgId(), beginTime, account.getOpFlag(), account.getAccountId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param request
     * @param contractPayableRecord
     * @return RetDataBean
     * @Title: getContractPayableRecordById
     * @Description:  获取付款详情
     */
    @RequestMapping(value = "/getContractPayableRecordById", method = RequestMethod.POST)
    public RetDataBean getContractPayableRecordById(HttpServletRequest request, ContractPayableRecord contractPayableRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            contractPayableRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, contractPayableRecordService.selectOneContractPayableRecord(contractPayableRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param request
     * @param contractReceivablesRecord
     * @return RetDataBean
     * @Title: getContractReceivablesRecordById
     * @Description:  获取收款详情
     */
    @RequestMapping(value = "/getContractReceivablesRecordById", method = RequestMethod.POST)
    public RetDataBean getContractReceivablesRecordById(HttpServletRequest request, ContractReceivablesRecord contractReceivablesRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            contractReceivablesRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, contractReceivablesRecordService.selectOneContractReceivablesRecord(contractReceivablesRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param request
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @param contractType
     * @return RetDataBean
     * @Title: getContractSendgoodsList
     * @Description:  获取发货列表
     */
    @RequestMapping(value = "/getContractSendgoodsList", method = RequestMethod.POST)
    public RetDataBean getContractSendgoodsList(
            HttpServletRequest request,
            PageParam pageParam,
            String beginTime,
            String endTime,
            String contractType
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("g.create_time");
            } else {
                pageParam.setSort("g." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrderBy(orderBy);
            pageParam.setOrgId(account.getOrgId());
            PageInfo<Map<String, String>> pageInfo = contractSendgoodsService.getContractSendgoodsList(pageParam, contractType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param request
     * @param pageParam
     * @param receivablesId
     * @return RetDataBean
     * @Title: getContractReceivablesRecordList
     * @Description:  获取收款记录
     */
    @RequestMapping(value = "/getContractReceivablesRecordList", method = RequestMethod.POST)
    public RetDataBean getContractReceivablesRecordList(
            HttpServletRequest request,
            PageParam pageParam,
            String receivablesId
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrderBy(orderBy);
            pageParam.setOrgId(account.getOrgId());
            PageInfo<Map<String, String>> pageInfo = contractReceivablesRecordService.getContractReceivablesRecordList(pageParam, receivablesId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param payableId
     * @return RetDataBean
     * @Title: getContractPayableRecordList
     * @Description:  获取付款记录
     */
    @RequestMapping(value = "/getContractPayableRecordList", method = RequestMethod.POST)
    public RetDataBean getContractPayableRecordList(
            PageParam pageParam,
            String payableId
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrderBy(orderBy);
            pageParam.setOrgId(account.getOrgId());
            PageInfo<Map<String, String>> pageInfo = contractPayableRecordService.getContractPayableRecordList(pageParam, payableId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param request
     * @param contractSendgoods
     * @return RetDataBean
     * @Title: getContractSendgoodsById
     * @Description:  获取发货详情
     */
    @RequestMapping(value = "/getContractSendgoodsById", method = RequestMethod.POST)
    public RetDataBean getContractSendgoodsById(HttpServletRequest request, ContractSendgoods contractSendgoods) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            contractSendgoods.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, contractSendgoodsService.selectOneContractSendgoods(contractSendgoods));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getContractBillList
     * @Description:  获取票据列表
     * @param: request
     * @param: pageParam
     * @param: beginTime
     * @param: endTime
     * @param: isOpen
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getContractBillList", method = RequestMethod.POST)
    public RetDataBean getContractBillList(
            HttpServletRequest request,
            PageParam pageParam,
            String beginTime,
            String endTime,
            String isOpen,
            String billType,
            String status
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("b.create_time");
            } else {
                pageParam.setSort("b." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrderBy(orderBy);
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrgId(account.getOrgId());
            PageInfo<Map<String, String>> pageInfo = contractBillService.getContractBillList(pageParam, isOpen, status, billType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getContractBillById
     * @Description:  获取票据记录详情
     * @param: request
     * @param: contractBill
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getContractBillById", method = RequestMethod.POST)
    public RetDataBean getContractBillById(HttpServletRequest request, ContractBill contractBill) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            contractBill.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, contractBillService.selectOneContractBill(contractBill));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: getContractPayableList
     * @Description:  获取应付款列表
     * @param: request
     * @param: pageParam
     * @param: beginTime
     * @param: endTime
     * @param: status
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getContractPayableList", method = RequestMethod.POST)
    public RetDataBean getContractPayableList(
            PageParam pageParam,
            String userPriv,
            String beginTime,
            String endTime,
            String status
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("sort_no");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrderBy(orderBy);
            pageParam.setAccountId(userPriv);
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrgId(account.getOrgId());
            PageInfo<Map<String, String>> pageInfo = contractPayableService.getContractPayableList(pageParam, beginTime, endTime, status);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getContractPayableById
     * @Description:  获取应付款详情
     * @param: request
     * @param: contractPayable
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getContractPayableById", method = RequestMethod.POST)
    public RetDataBean getContractPayableById(HttpServletRequest request, ContractPayable contractPayable) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            contractPayable.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, contractPayableService.selectOneContractPayable(contractPayable));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: getContractReceivablesList
     * @Description:  获取应收款列表
     * @param: request
     * @param: pageParam
     * @param: beginTime
     * @param: endTime
     * @param: status
     * @param: userPriv
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getContractReceivablesList", method = RequestMethod.POST)
    public RetDataBean getContractReceivablesList(
            HttpServletRequest request,
            PageParam pageParam,
            String beginTime,
            String endTime,
            String status,
            String userPriv
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("sort_no");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrderBy(orderBy);
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrgId(account.getOrgId());
            PageInfo<Map<String, Object>> pageInfo = contractReceivablesService.getContractReceivablesList(pageParam, userPriv, beginTime, endTime, status);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getSelect2ContractList
     * @Description:  SELECT2的列表
     * @param: request
     * @param: search
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getSelect2ContractList", method = RequestMethod.POST)
    public RetDataBean getSelect2ContractList(HttpServletRequest request, String search) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, contractService.getSelect2ContractList(account.getOrgId(), search));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getContractReceivablesById
     * @Description:  应收款详情
     * @param: request
     * @param: contractReceivables
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getContractReceivablesById", method = RequestMethod.POST)
    public RetDataBean getContractReceivablesById(HttpServletRequest request, ContractReceivables contractReceivables) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            contractReceivables.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, contractReceivablesService.selectOneContractReceivables(contractReceivables));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: getContractManageList
     * @Description:  获取合同管理列表
     * @param: request
     * @param: pageParam
     * @param: beginTime
     * @param: endTime
     * @param: contractType
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getContractManageList", method = RequestMethod.POST)
    public RetDataBean getContractManageList(
            HttpServletRequest request,
            PageParam pageParam,
            String beginTime,
            String endTime,
            String contractType
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("c.sort_no");
            } else {
                pageParam.setSort("c." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrderBy(orderBy);
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrgId(account.getOrgId());
            PageInfo<Map<String, String>> pageInfo = contractService.getContractManageList(pageParam, beginTime, endTime, contractType);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: queryContract
     * @Description:  合同查询列表
     * @param: request
     * @param: pageParam
     * @param: customerName
     * @param: beginTime
     * @param: endTime
     * @param: contractType
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/queryContract", method = RequestMethod.POST)
    public RetDataBean queryContract(
            HttpServletRequest request,
            PageParam pageParam,
            String beginTime,
            String endTime,
            String contractType,
            String mySignUser
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("c.sort_no");
            } else {
                pageParam.setSort("c." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrderBy(orderBy);
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrgId(account.getOrgId());
            PageInfo<Map<String, String>> pageInfo = contractService.queryContract(pageParam, beginTime, endTime, contractType, mySignUser);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param request
     * @param sortId
     * @return List<Map < String, Object>>
     * @Title getErpBomSortTree
     * @Description  获取BOM 分类树结构
     */
    @RequestMapping(value = "/getContractSortTree", method = RequestMethod.POST)
    public List<Map<String, String>> getContractSortTree(HttpServletRequest request, String sortId) {
        try {
            String sortLevel = "0";
            if (StringUtils.isNotBlank(sortId)) {
                sortLevel = sortId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return contractSortService.getContractSortTree(account.getOrgId(), sortLevel);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @param @param  request
     * @param @param  contractSort
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getContractById
     * @Description:  获取合同分类的详情信息
     */
    @RequestMapping(value = "/getContractSortById", method = RequestMethod.POST)
    public RetDataBean getContractSortById(HttpServletRequest request, ContractSort contractSort) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            contractSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, contractSortService.selectOneContractSort(contractSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getContractDetailsList
     * @Description:  获取指定的合同明细
     * @param: request
     * @param: contractId
     * @param: pageParam
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getContractDetailsList", method = RequestMethod.POST)
    public RetDataBean getContractDetailsList(
            HttpServletRequest request,
            String contractId,
            PageParam pageParam
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("c.sort_no");
            } else {
                pageParam.setSort("c." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrderBy(orderBy);
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrgId(account.getOrgId());
            PageInfo<Map<String, Object>> pageInfo = contractDetailsService.getContractDetailsList(pageParam, contractId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: createCode
     * @Description:  获取合同编号
     */
    @RequestMapping(value = "/createCode", method = RequestMethod.POST)
    public RetDataBean createCode(HttpServletRequest request) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            String contractCode = "AIP-" + SysTools.getTime("yyyy") + SysTools.autoGenericCode((contractService.getContractCount(account.getOrgId()) + ""), 3);
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, contractCode);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 根据合同id获取合同对象
     *
     * @param contract
     * @return
     */
    @RequestMapping("/getContractById")
    public RetDataBean getContractById(Contract contract) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            contract.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, contractService.selectOneContract(contract));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getContractPriv
     * @Description:  获取合同管理的权限详情
     */
    @RequestMapping(value = "/getContractPriv", method = RequestMethod.POST)
    public RetDataBean getContractPriv(HttpServletRequest request) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            ContractPriv contractPriv = new ContractPriv();
            contractPriv.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, contractPrivService.selectOneContractPriv(contractPriv));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }
}
