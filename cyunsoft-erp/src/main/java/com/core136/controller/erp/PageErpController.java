package com.core136.controller.erp;

import com.core136.bean.account.Account;
import com.core136.bean.erp.ErpBom;
import com.core136.bean.erp.ErpOrder;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.erp.ErpBomService;
import com.core136.service.erp.ErpOrderService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/app/core/erp")
public class PageErpController {
    private ErpBomService erpBomService;

    @Autowired
    public void setErpBomService(ErpBomService erpBomService) {
        this.erpBomService = erpBomService;
    }

    private ErpOrderService erpOrderService;

    @Autowired
    public void setErpOrderService(ErpOrderService erpOrderService) {
        this.erpOrderService = erpOrderService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * @return ModelAndView
     * @Title goBommange
     * @Description  跳转物料分类管理页面
     */
    @RequestMapping("/cost/materielsort")
    @RequiresPermissions("/app/core/erp/cost/materielsort")
    public ModelAndView goCostBaseSortSet() {
        try {
            return new ModelAndView("app/core/erp/cost/baseset/materielsort");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title goBommange
     * @Description  跳转产品分类管理页面
     */
    @RequestMapping("/cost/productsort")
    @RequiresPermissions("/app/core/erp/cost/productsort")
    public ModelAndView goCostProductSortSet() {
        try {
            return new ModelAndView("app/core/erp/cost/baseset/productsort");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title goBommange
     * @Description  跳转物料管理页面
     */
    @RequestMapping("/cost/materielmagage")
    @RequiresPermissions("/app/core/erp/cost/materielmagage")
    public ModelAndView goMaterielMagage() {
        try {
            return new ModelAndView("app/core/erp/cost/baseset/materielmanage");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title goBommange
     * @Description  跳转Bom管理页面
     */
    @RequestMapping("/cost/bommange")
    @RequiresPermissions("/app/core/erp/cost/bommange")
    public ModelAndView goBommange() {
        try {
            return new ModelAndView("app/core/erp/cost/bom/bommanage");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title goBomsort
     * @Description  跳转Bom分类管理页面
     */
    @RequestMapping("/cost/bomsort")
    @RequiresPermissions("/app/core/erp/cost/bomsort")
    public ModelAndView goBomsort() {
        try {
            return new ModelAndView("app/core/erp/cost/bom/bomsort");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param erpBom
     * @return ModelAndView
     * @Title goBomdetail
     * @Description  路转BOM详情页面
     */
    @RequestMapping("/cost/bomdetail")
    @RequiresPermissions("/app/core/erp/cost/bomdetail")
    public ModelAndView goBomdetail(ErpBom erpBom) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            erpBom.setOrgId(account.getOrgId());
            ModelAndView mv = new ModelAndView("app/core/erp/cost/bom/bomdetail");
            mv.addObject("erpBom", erpBomService.selectOne(erpBom));
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title goPmanage
     * @Description  产品管理
     */
    @RequestMapping("/cost/productmanage")
    @RequiresPermissions("/app/core/erp/cost/productmanage")
    public ModelAndView goPmanage() {
        try {
            return new ModelAndView("app/core/erp/cost/baseset/pmanage");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title goOrder
     * @Description  跳转订单管理
     */
    @RequestMapping("/cost/order")
    @RequiresPermissions("/app/core/erp/cost/order")
    public ModelAndView goOrder( ErpOrder erpOrder) {
        String orderCode = "";
        ErpOrder erpOrder1 = new ErpOrder();
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            ModelAndView mv = new ModelAndView("app/core/erp/cost/order/index");
            if (StringUtils.isNotEmpty(erpOrder.getOrderId())) {
                erpOrder.setOrgId(account.getOrgId());
                erpOrder.setCreateUser(account.getAccountId());
                erpOrder1 = erpOrderService.selectOne(erpOrder);
            } else {
                orderCode = SysTools.getCode(account, "[yyyy][MM][dd]-[HH][mm][ss]-[R]");
                erpOrder1.setOrderCode(orderCode);
            }
            mv.addObject("erpOrder", erpOrder1);
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param request
     * @param erpOrder
     * @return ModelAndView
     * @Title goOrdermanage
     * @Description  订单管理
     */
    @RequestMapping("/cost/ordermanage")
    @RequiresPermissions("/app/core/erp/cost/ordermanage")
    public ModelAndView goOrdermanage(HttpServletRequest request, ErpOrder erpOrder) {
        try {
            return new ModelAndView("app/core/erp/cost/order/ordermanage");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param request
     * @param erpOrder
     * @return ModelAndView
     * @Title goOrderproduct
     * @Description  为订单产品管理
     */
    @RequestMapping("/cost/orderproduct")
    @RequiresPermissions("/app/core/erp/cost/orderproduct")
    public ModelAndView goOrderproduct(HttpServletRequest request, ErpOrder erpOrder) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            erpOrder.setOrgId(account.getOrgId());
            erpOrder = erpOrderService.selectOne(erpOrder);
            ModelAndView mv = new ModelAndView("app/core/erp/cost/order/orderproduct");
            mv.addObject("erpOrder", erpOrder);
            return mv;
        } catch (Exception e) {
            ModelAndView mv = new ModelAndView("titps");
            return mv;
        }
    }

    /**
     * @return ModelAndView
     * @Title goOrder
     * @Description  跳转订单管理
     */
    @RequestMapping("/cost/docost")
    @RequiresPermissions("/app/core/erp/cost/docost")
    public ModelAndView goDocost() {
        try {
            return new ModelAndView("app/core/erp/cost/order/docost");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    @RequestMapping("/cost/costinfo")
    @RequiresPermissions("/app/core/erp/cost/costinfo")
    public ModelAndView costinfo(HttpServletRequest request, String orderId) {
        try {
            ModelAndView mv = new ModelAndView("app/core/erp/cost/order/costinfo");
            mv.addObject("orderId", orderId);
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


    /**
     * @param productId
     * @return ModelAndView
     * @Title goPircinfo
     * @Description  跳转成本组成页面
     */
    @RequestMapping("/cost/pirceinfo")
    @RequiresPermissions("/app/core/erp/cost/pirceinfo")
    public ModelAndView goPirceinfo(String productId) {
        try {
            ModelAndView mv = new ModelAndView("app/core/erp/cost/order/pirceinfo");
            mv.addObject("productId", productId);
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param bomId
     * @return ModelAndView
     * @Title goBompirce
     * @Description  总成成本详情
     */
    @RequestMapping("/cost/bompirce")
    @RequiresPermissions("/app/core/erp/cost/bompirce")
    public ModelAndView goBompirce(String bomId) {
        try {
            ModelAndView mv = new ModelAndView("app/core/erp/cost/order/bompirce");
            mv.addObject("bomId", bomId);
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param request
     * @return ModelAndView
     * @Title goEquipmentsort
     * @Description  设备分类
     */

    @RequestMapping("/cost/equipmentsort")
    @RequiresPermissions("/app/core/erp/cost/equipmentsort")
    public ModelAndView goEquipmentsort(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/erp/cost/equipment/sort");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param request
     * @return ModelAndView
     * @Title goEquipment
     * @Description  设备管理
     */
    @RequestMapping("/cost/equipment")
    @RequiresPermissions("/app/core/erp/cost/equipment")
    public ModelAndView goEquipment(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/erp/cost/equipment/manage");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param request
     * @return ModelAndView
     * @Title goErpbi
     * @Description  Erp图形报表
     */
    @RequestMapping("/cost/erpbi")
    @RequiresPermissions("/app/core/erp/cost/erpbi")
    public ModelAndView goErpbi(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/erp/cost/query/erpbi");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }
}
