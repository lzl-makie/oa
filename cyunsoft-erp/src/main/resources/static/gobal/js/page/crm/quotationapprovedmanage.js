$(function () {
    jeDate("#beginTimeQuery", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTimeQuery", {
        format: "YYYY-MM-DD"
    });
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    });
    query();
})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/crmget/getApprovedQueryList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        search: true,//启用搜索
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'quotationId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        },
            {
                field: 'num',
                title: '序号',//标题  可不加
                width: '50px',
                formatter: function (value, row, index) {
                    return index + 1;
                }
            },
            {
                field: 'quotationCode',
                title: '报价单号',
                sortable: true,
                width: '100px'
            },
            {
                field: 'title',
                title: '报价单标题',
                sortable: true,
                width: '100px'
            },
            {
                field: 'status',
                title: '审批状态',
                width: '50px',
                formatter: function (value, row, index) {
                    if (value == "0") {
                        return "审批中";
                    } else if (value == "1") {
                        return "通过";
                    } else if (value == "2") {
                        return "未通过";
                    }

                }
            },
            {
                field: 'inquiryTitle',
                width: '100px',
                title: '对应询价单',
                formatter: function (value, row, index) {
                    return "<a style='cursor: pointer;' href='javascript:void(0);window.open(\"/app/core/crm/inquirydetails?inquiryId=" + row.inquiryId + "\");'>" + value + "</a>"
                }
            },
            {
                field: 'colsingTime',
                width: '100px',
                title: '报价有效期'
            },
            {
                field: 'tax',
                width: '50px',
                title: '税费'
            },
            {
                field: 'totalPrice',
                width: '80px',
                title: '总价'
            },
            {
                field: 'approvedTime',
                width: '100px',
                title: '审批时间'
            },
            {
                field: 'approvedUserName',
                width: '80px',
                title: '审批人'
            },
            {
                field: 'opt',
                width: '50px',
                align: 'center',
                title: '操作',
                formatter: function (value, row, index) {
                    return createOptBtn(row.quotationId);
                }
            }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        beginTime: $("#beginTimeQuery").val(),
        endTime: $("#endTimeQuery").val(),
        createUser: $("#createUserQuery").attr("data-value"),
        status: $("#status").val()
    };
    return temp;
};

function createOptBtn(quotationId) {
    var html = "<a href=\"javascript:void(0);details('" + quotationId + "')\" class=\"btn btn-sky btn-xs\" >详情</a>";
    return html;
}

function details(quotationId) {
    window.open("/app/core/crm/quotationdetails?quotationId=" + quotationId);
}

function getApprovedUserList() {
    $.ajax({
        url: "/ret/crmget/getCrmApprovedUserList",
        type: "post",
        dataType: "json",
        async: false,
        success: function (data) {
            if (data.status == "200") {
                var recordList = data.list;
                var html = "";
                for (var i = 0; i < recordList.length; i++) {
                    html += "<option value=\"" + recordList[i].accountId + "\">" + recordList[i].userName + "</option>";
                }
                $("#approvedUser").html(html);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    })
}
